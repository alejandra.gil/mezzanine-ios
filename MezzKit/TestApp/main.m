//
//  main.m
//  TestApp
//
//  Created by miguel on 8/2/17.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
  }
}
