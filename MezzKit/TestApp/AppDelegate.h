//
//  AppDelegate.h
//  TestApp
//
//  Created by miguel on 8/2/17.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

