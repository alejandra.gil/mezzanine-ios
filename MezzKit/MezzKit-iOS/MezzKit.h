//
//  MezzKit.h
//  MezzKit
//
//  Created by Miguel Sanchez Valdes on 14/03/2019.
//  Copyright © 2019 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for MezzKit.
FOUNDATION_EXPORT double MezzKitVersionNumber;

//! Project version string for MezzKit.
FOUNDATION_EXPORT const unsigned char MezzKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MezzKit/PublicHeader.h>

#import <MezzKit/MezzKit.h>
