//
//  MZExportInfo.h
//  MezzKit
//
//  Created by Zai Chang on 1/13/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef NS_ENUM(NSUInteger, MZExportState)
{
  MZExportStateNone,
  MZExportStateRequested,
  MZExportStateDownloading,
  MZExportStateReady,
  MZExportStateFailed
};

typedef NS_ENUM(NSUInteger, MZExportSourceType)
{
  MZExportSourceTypePortfolio
};


@interface MZExportInfo : NSObject <NSCopying>

@property (nonatomic, assign) MZExportState state;
@property (nonatomic, strong) NSURL* url;
@property (nonatomic, assign) CGFloat progress;
@property (nonatomic, strong) NSString* filepath;
@property (nonatomic, strong) NSString* errorMessage;
@property (nonatomic, strong) NSString* errorTitle;
@property (nonatomic, assign) NSUInteger filesizeInBytes;
@property (nonatomic, assign) MZExportSourceType source;
@property (nonatomic, strong) NSString* workspaceName;

-(void) reset;
-(NSString *) sourceString;

@end
