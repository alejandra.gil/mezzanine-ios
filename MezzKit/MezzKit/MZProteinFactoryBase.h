//
//  MZProteinFactoryBase.h
//  MezzKit
//
//  Created by Ivan Bella Lopez on 07/04/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MZProteinFactoryBase : NSObject

@property (nonatomic, strong) NSString *provenance;
@property (nonatomic, assign) NSInteger transactionId;

- (NSArray*)getNextProvenanceAndTransactionId;
- (NSArray*)requestDescripsWithCommand:(NSString*)command;

@end
