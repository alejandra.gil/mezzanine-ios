//
//  MZDownloadManager.h
//  Mezzanine
//
//  Created by miguel on 26/11/13.
//  Copyright (c) 2013 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kMZDownloadManagerErrorDomain @"MZDownloadManager"
#define kMZDownloadManagerErrorCode_IncorrectDownload -5000

typedef NS_ENUM(NSInteger, MZDownloadCacheType) {
  MZDownloadCacheTypeDefault, // Both Memory and Disk
  MZDownloadCacheTypeOnlyInMemory,
  MZDownloadCacheTypeOnlyInDisk,
  MZDownloadCacheTypeNone
};

@interface MZDownloadManager : NSObject

+ (MZDownloadManager *)sharedLoader;

- (MZImage *)cachedImageForURL:(NSURL *)url;
- (MZImage *)cachedImageInDiskForURL:(NSURL *)url;


- (void)loadImageWithURL:(NSURL *)aUrl atTarget:(id)target success:(void (^)(MZImage *image))successBlock error:(void (^)(NSError *error))errorBlock;
- (void)loadImageWithURL:(NSURL *)aUrl atTarget:(id)target success:(void (^)(MZImage *image))successBlock error:(void (^)(NSError *error))errorBlock canceled:(void (^)(void))canceledBlock;
- (void)loadImageWithURL:(NSURL *)aUrl atTarget:(id)target success:(void (^)(MZImage *image))successBlock error:(void (^)(NSError *error))errorBlock canceled:(void (^)(void))canceledBlock cachingOption:(MZDownloadCacheType)option;

- (void)cancelLoadingURLForTarget:(id)target;
- (void)cancelAllOperations;

- (void)unloadImageFromMemoryCacheWithURL:(NSURL *)url;
- (void)unloadImageFromDiskCacheWithURL:(NSURL *)url;

@property (nonatomic, assign) BOOL pauseDownloads;

@end

