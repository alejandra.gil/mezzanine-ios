//
//  OBProtein+Mezzanine.m
//  Mezzanine
//
//  Created by Zai Chang on 1/25/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import "OBProtein+Mezzanine.h"
#import "MZProteins.h"
@implementation OBProtein (Mezzanine)

-(NSString*) extractProvenance
{
  NSString *(^extractProvenanceFromDecripAtIndex)(NSInteger index) = ^NSString *(NSInteger index){
    if (index != NSNotFound && index+1 < [descrips count])
    {
      // transaction id is the second in the array of [provenance, id]
      id provAndId = descrips[index+1];
      if ([provAndId isKindOfClass:[NSArray class]] && [provAndId count] > 0)
        return provAndId[0];
    }
    return nil;
  };
  
  NSInteger index = [descrips indexOfObject:@"to:"];
  NSString *aProvenance = extractProvenanceFromDecripAtIndex(index);
  if (aProvenance)
    return aProvenance;
  
  index = [descrips indexOfObject:@"pat:"];
  aProvenance = extractProvenanceFromDecripAtIndex(index);
  if (aProvenance)
    return aProvenance;
  
  return nil;
}

-(BOOL) provenanceIsEqual:(NSString*)aProvenance
{
  return [[self extractProvenance] isEqual:aProvenance];
}

-(NSString*) proteinVersion
{
  // Protein spec descrip should be the one after mezzanine
  NSInteger index = [descrips indexOfObject:@"mezzanine"];
  if (index != NSNotFound && [descrips count] > index+1)
  {
    NSString *proteinSpecs = descrips[index+1];
    if ([proteinSpecs hasPrefix:@"prot-spec v"])
    {
      NSString *version = [proteinSpecs stringByReplacingOccurrencesOfString:@"prot-spec v" withString:@""];
      return version;
    }
  }
  return nil;
}

-(NSString*) transactionId
{
  NSString *(^extractTransactionIdFromDecripAtIndex)(NSInteger index) = ^NSString *(NSInteger index){
    if (index != NSNotFound && index+1 < [descrips count])
    {
      id provAndId = descrips[index+1];
      // In Pat class, 'y' is the transaction id
      if ([provAndId isKindOfClass:[Pat class]])
      {
        NSInteger transactionId = (long)((Pat *)provAndId).transactionId;
        return [NSString stringWithFormat:@"%li", (long)transactionId];
      }
      // transaction id is the second in the array of [provenance, id]
      else if ([provAndId isKindOfClass:[NSArray class]] && [provAndId count] > 1)
      {
        id prov = provAndId[1];
        if ([prov respondsToSelector:@selector(stringValue)])
          return [prov stringValue];
      }
    }
    return nil;
  };
  
  NSString *anId = nil;
  // In the case of inbound response proteins, transactionId is after
  // the to: descrip
  NSInteger index = [descrips indexOfObject:@"to:"];
  anId = extractTransactionIdFromDecripAtIndex(index);
  if (anId)
    return anId;
  
  // In the case of psa that is the result of a request by this client
  // the transaction id is after the pat: descrip ... so many cases!
  index = [descrips indexOfObject:@"pat:"];
  anId = extractTransactionIdFromDecripAtIndex(index);
  if (anId)
    return anId;
  
  // In the case of outbound proteins, id is after the from: descrip
  index = [descrips indexOfObject:@"from:"];
  anId = extractTransactionIdFromDecripAtIndex(index);
  if (anId)
    return anId;
  
  return nil;
}

@end
