//
//  MZHandlerFactory.m
//  Mezzanine
//
//  Created by Zai Chang on 1/25/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import "MZHandlerFactoryBase.h"
#import "MZRemoteMezz.h"


@implementation MZHandlerFactoryBase

@synthesize communicator;
@synthesize systemModel;

@end
