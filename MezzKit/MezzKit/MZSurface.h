//
//  MZSurface.h
//  MezzKit
//
//  Created by miguel on 25/2/15.
//  Copyright (c) 2015 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MZFeld.h"

@interface MZSurface : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) MZFeld *boundingFeld;
@property (nonatomic, strong) NSMutableArray<MZFeld *> *felds;

-(instancetype) initWithName:(NSString*)aName;

-(void) updateWithDictionary:(NSDictionary *)dictionary;

-(MZFeld *) feldWithName:(NSString*)aName;

@end
