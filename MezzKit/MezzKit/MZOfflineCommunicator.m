//
//  MZOfflineCommunicator.m
//  MezzKit
//
//  Created by Zai Chang on 1/15/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import "MZOfflineCommunicator.h"


@implementation MZOfflineCommunicator


#pragma mark - Passphrase

-(MZTransaction *) requestPassphraseEnable
{
  systemModel.passphrase = @"ASD";
  // Uncomment the following to test the passphrase view
//  systemModel.passphraseRequested = YES;
  return nil;
}

-(MZTransaction *) requestPassphraseDisable
{
  systemModel.passphraseRequested = NO;
  systemModel.passphrase = nil;
  return nil;
}

-(MZTransaction *) requestPassphraseDetails
{
  systemModel.passphrase = nil;
  return nil;
}


#pragma mark - Workspace


-(MZTransaction*) requestWorkspaceCreate:(NSString*)name
{
  static NSInteger counter = 1;
  MZWorkspace *newWorkspace = [[MZWorkspace alloc] init];
  newWorkspace.uid = [NSString stringWithFormat:@"ws-000%ld", (long)counter];
  newWorkspace.isSaved = NO;
  
  [systemModel insertObject:newWorkspace inWorkspacesAtIndex:systemModel.workspaces.count];
  return nil;
}


-(MZTransaction*) requestWorkspaceClose:(NSString*)uid
{
  if (![systemModel.currentWorkspace.uid isEqual:uid])
    return nil;
  
  MZWorkspace *emptyWorkspace = [[MZWorkspace alloc] init];
  emptyWorkspace.uid = @"ws-0002";
  emptyWorkspace.isSaved = NO;
  systemModel.currentWorkspace = emptyWorkspace;
  return nil;
}


-(MZTransaction*) requestWorkspaceDiscard:(NSString*)uid
{
  if (![systemModel.currentWorkspace.uid isEqual:uid])
    return nil;
  
  MZWorkspace *emptyWorkspace = [[MZWorkspace alloc] init];
  emptyWorkspace.uid = @"ws-0002";
  emptyWorkspace.isSaved = NO;
  systemModel.currentWorkspace = emptyWorkspace;
  return nil;
}


-(MZTransaction*) requestWorkspaceSave:(NSString*)uid name:(NSString*)name
{
  if (![systemModel.currentWorkspace.uid isEqual:uid])
    return nil;
  
  systemModel.currentWorkspace.isSaved = YES;
  systemModel.currentWorkspace.name = name;
  
  return nil;
}


-(MZTransaction*) requestWorkspaceRename:(NSString*)uid newName:(NSString*)newName
{
  if (![systemModel.currentWorkspace.uid isEqual:uid])
    return nil;

  systemModel.currentWorkspace.isSaved = YES;
  systemModel.currentWorkspace.name = newName;

  return nil;
}


#pragma mark - Uploads

-(MZImageUploadRequestTransaction *) requestImageUpload:(MZImage*)image format:(NSString*)format target:(NSString*)target success:(void (^)(NSArray *assetUids, NSArray *slideInstanceUids))successBlock error:(void (^)(NSError *))errorBlock
{
  MZImageUploadRequestTransaction *transaction = [[MZImageUploadRequestTransaction alloc] init];
  
  [[NSOperationQueue mainQueue] addOperationWithBlock:^{
    MZSlide *slide = [[MZSlide alloc] init];
    slide.image = image;
    slide.imageAvailable = YES;
    slide.thumbURL = @"/blah";
    slide.fullURL = @"/blah";

    if (systemModel.currentWorkspace.presentation)
    {
      NSInteger index = systemModel.currentWorkspace.presentation.slides.count;
      slide.uid = [NSString stringWithFormat:@"sl-000%ld", (long)index];
      [systemModel.currentWorkspace.presentation insertObject:slide inSlidesAtIndex:index];
    }
  }];
  
  return transaction;
}


-(MZImageUploadRequestTransaction *) requestImageUploadTo:(NSString*)target numberOfImages:(NSInteger)numberOfImages successHandler:(void (^)(MZImageUploadRequestTransaction *, NSArray *))successHandler errorHandler:(void (^)(NSError *))errorBlock
{
  MZImageUploadRequestTransaction *transaction = [[MZImageUploadRequestTransaction alloc] init];
  
  NSMutableArray *uids = [NSMutableArray array];
  NSInteger startIndex = 0;
  
  if (systemModel.currentWorkspace.presentation)
    startIndex = systemModel.currentWorkspace.presentation.slides.count;
  
  for (NSInteger i=startIndex; i<startIndex+numberOfImages; i++)
  {
    [uids addObject:[NSNumber numberWithInteger:i]];
  }
  
  [[NSOperationQueue mainQueue] addOperationWithBlock:^{
    successHandler(transaction, uids);
  }];
  
  return transaction;
}


-(MZImageUploadTransaction *) beginImageUploadTransaction:(MZImage*)image thumbnail:(MZImage*)thumbnail format:(NSString*)format uid:(NSString*)uid dossierUid:(NSString*)dossierUid
{
  MZImageUploadTransaction *transaction = [[MZImageUploadTransaction alloc] init];
  
  [[NSOperationQueue mainQueue] addOperationWithBlock:^{
    MZSlide *slide = [[MZSlide alloc] init];
    slide.image = image;
    slide.imageAvailable = YES;
    slide.thumbURL = @"/blah";
    slide.fullURL = @"/blah";
    
    if (systemModel.currentWorkspace.presentation)
    {
      NSInteger index = systemModel.currentWorkspace.presentation.slides.count;
      slide.uid = [NSString stringWithFormat:@"sl-000%ld", (long)index];
      [systemModel.currentWorkspace.presentation insertObject:slide inSlidesAtIndex:index];
    }
  }];
  
  return transaction;
}



#pragma mark - Deck / Presentation

-(MZTransaction*) requestNextSlide
{
  MZPresentation *presentation = systemModel.currentWorkspace.presentation;
  if (presentation)
    presentation.currentSlideIndex = MIN(presentation.currentSlideIndex + 1, presentation.numberOfSlides - 1);
  
  return nil;
}


-(MZTransaction*) requestPreviousSlide
{
  MZPresentation *presentation = systemModel.currentWorkspace.presentation;
  if (presentation)
    presentation.currentSlideIndex = MAX(presentation.currentSlideIndex - 1, 0);

  return nil;
}


-(MZTransaction *) requestScrollDeckToSlide:(NSUInteger)slideIndex
{
  MZPresentation *presentation = systemModel.currentWorkspace.presentation;
  if (presentation)
    presentation.currentSlideIndex = MIN(MAX(slideIndex, 0), presentation.numberOfSlides - 1);

  return nil;
}


-(MZTransaction *) requestSlideDelete:(NSString*)uid
{
  MZPresentation *presentation = systemModel.currentWorkspace.presentation;
  if (presentation)
  {
    MZPortfolioItem *slide = [presentation slideWithUid:uid];
    NSInteger index = [presentation.slides indexOfObject:slide];
    if (index != NSNotFound)
    {
      [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [presentation removeSlidesAtIndexes:[NSIndexSet indexSetWithIndex:index]];
      }];
    }
  }

  return nil;
}


-(MZTransaction *) requestClearDeck
{
  return nil;
}


-(MZTransaction *) requestSlideInsert:(NSString*)assetUid ofType:(NSString*)type insertionIndex:(NSInteger)insertionIndex leftOf:(NSString*)leftOfSlideUid rightOf:(NSString*)rightOfSlideUid
{
  MZTransaction *transaction = [[MZTransaction alloc] init];

  MZSlide *slide = [[MZSlide alloc] init];
  slide.uid = [NSString stringWithFormat:@"sl-000%ld", (long)insertionIndex];

  MZPresentation *presentation = systemModel.currentWorkspace.presentation;
  [[NSOperationQueue mainQueue] addOperationWithBlock:^{
    if (presentation)
      [presentation insertObject:slide inSlidesAtIndex:insertionIndex];
    
    if (transaction.successBlock)
      transaction.successBlock();
    if (transaction.endedBlock)
      transaction.endedBlock();
  }];
  
  return transaction;
}


-(MZTransaction *) requestSlideReorder:(NSString*)slideUid insertionIndex:(NSInteger)insertionIndex leftOf:(NSString*)leftOfSlideUid rightOf:(NSString*)rightOfSlideUid
{
  MZTransaction *transaction = [[MZTransaction alloc] init];
  
  MZPresentation *presentation = systemModel.currentWorkspace.presentation;
  [[NSOperationQueue mainQueue] addOperationWithBlock:^{
    if (presentation)
    {
      MZPortfolioItem *slide = [presentation slideWithUid:slideUid];
      [presentation moveSlide:slide toIndex:insertionIndex];
    }

    if (transaction.successBlock)
      transaction.successBlock();
    if (transaction.endedBlock)
      transaction.endedBlock();
  }];
  
  return transaction;
}


#pragma mark - Portfolio

- (MZTransaction*)requestPortfolioItemInsert:(NSString*)contentUid atIndex:(NSInteger)insertionIndex leftOf:(NSString*)leftOf rightOf:(NSString*)rightOf workspaceUid:(NSString*)workspaceUid
{
  MZTransaction *transaction = [[MZTransaction alloc] init];
  
  MZPortfolioItem *item = [[MZPortfolioItem alloc] init];
  item.uid = [NSString stringWithFormat:@"sl-000%ld", (long)insertionIndex];
  
  MZPresentation *presentation = systemModel.currentWorkspace.presentation;
  [[NSOperationQueue mainQueue] addOperationWithBlock:^{
    if (presentation)
      [presentation insertObject:item inSlidesAtIndex:insertionIndex];
    
    if (transaction.successBlock)
      transaction.successBlock();
    if (transaction.endedBlock)
      transaction.endedBlock();
  }];
  
  return transaction;
}


- (MZTransaction*)requestPortfolioItemReorder:(NSString*)uid toIndex:(NSInteger)index leftOf:(NSString*)leftOf rightOf:(NSString*)rightOf workspaceUid:(NSString*)workspaceUid
{
  MZTransaction *transaction = [[MZTransaction alloc] init];
  
  MZPortfolioItem *item = [systemModel.currentWorkspace.presentation slideWithUid:uid];
  
  MZPresentation *presentation = systemModel.currentWorkspace.presentation;
  [[NSOperationQueue mainQueue] addOperationWithBlock:^{
    if (presentation)
      [presentation moveSlide:item toIndex:index];
    
    if (transaction.successBlock)
      transaction.successBlock();
    if (transaction.endedBlock)
      transaction.endedBlock();
  }];
  
  return transaction;
}


- (MZTransaction*)requestPortfolioClear:(NSString*)workspaceUid
{
  MZTransaction *transaction = [[MZTransaction alloc] init];
  
  MZPresentation *presentation = systemModel.currentWorkspace.presentation;
  [[NSOperationQueue mainQueue] addOperationWithBlock:^{
    if (presentation)
      [presentation removeAllSlides];
    
    if (transaction.successBlock)
      transaction.successBlock();
    if (transaction.endedBlock)
      transaction.endedBlock();
  }];
  
  return transaction;
}


- (MZTransaction*)requestPortfolioItemDelete:(NSString*)uid workspaceUid:(NSString*)workspaceUid
{
  MZTransaction *transaction = [[MZTransaction alloc] init];
  
  MZPresentation *presentation = systemModel.currentWorkspace.presentation;
  [[NSOperationQueue mainQueue] addOperationWithBlock:^{
    if (presentation)
    {
      NSInteger index = [presentation.slides objectAtIndex:[presentation slideWithUid:uid]];
      [presentation removeObjectFromSlidesAtIndex:index];
    }
    
    if (transaction.successBlock)
      transaction.successBlock();
    if (transaction.endedBlock)
      transaction.endedBlock();
  }];
  
  return transaction;
}



#pragma mark - Windshield

-(MZWindshield *) currentWindshield
{
  if (systemModel.currentWorkspace)
    return systemModel.currentWorkspace.windshield;
  
  return nil;
}

-(MZTransaction *) requestWindshieldAddItem:(NSString*)uid withFrame:(CGRect)frame
{
  MZTransaction *transaction = [[MZTransaction alloc] init];

  MZWindshield *windshield = [self currentWindshield];

  MZWindshieldItem *item = [[MZWindshieldItem alloc] init];
  item.uid = [NSString stringWithFormat:@"as-000%lu", (unsigned long)windshield.items.count];
  item.assetUid = uid;
  item.frame = frame;
  
  if (windshield)
  {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
      [windshield insertObject:item inItemsAtIndex:windshield.items.count];

      if (transaction.successBlock)
        transaction.successBlock();
      if (transaction.endedBlock)
        transaction.endedBlock();
    }];
  }

  return transaction;
}


-(MZTransaction *) requestWindshieldTransformItem:(NSString*)uid withFrame:(CGRect)frame
{
  MZTransaction *transaction = [[MZTransaction alloc] init];
  
  MZWindshield *windshield = [self currentWindshield];
  if (windshield)
  {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
      MZWindshieldItem *item = [windshield itemWithUid:uid];
      item.frame = frame;

      if (transaction.successBlock)
        transaction.successBlock();
      if (transaction.endedBlock)
        transaction.endedBlock();
    }];
  }
  
  return transaction;
}


-(MZTransaction *) requestWindshieldDeleteItem:(NSString*)uid
{
  MZTransaction *transaction = [[MZTransaction alloc] init];
  
  MZWindshield *windshield = [self currentWindshield];
  if (windshield)
  {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
      NSInteger index = [windshield indexOfItemWithUid:uid];
      if (index != NSNotFound)
        [windshield removeObjectFromItemsAtIndex:index];

      if (transaction.successBlock)
        transaction.successBlock();
      if (transaction.endedBlock)
        transaction.endedBlock();
    }];
  }
  
  return transaction;
}


-(MZTransaction *) requestClearWindshield
{
  MZWindshield *windshield = [self currentWindshield];
  if (windshield)
  {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
      [windshield clear];
    }];
  }
  return nil;
}


#pragma mark - Infopresence

// Makes no sense for offline browsing, but is here for interface testing
-(MZTransaction*) requestInfopresenceJoinMezzanine:(NSString*)mezzUid
{
  MZRemoteMezz *remoteMezz = [systemModel remoteMezzWithUid:mezzUid];
  
  // Fake a call in progress then success
  remoteMezz.collaborationState = MZRemoteMezzCollaborationStateJoining;
  
  [[NSOperationQueue mainQueue] addOperationWithBlock:^{
    remoteMezz.collaborationState = MZRemoteMezzCollaborationStateInCollaboration;
    systemModel.infopresence.status = MZInfopresenceStatusActive;
  }];

  return nil;
}


-(MZTransaction*) requestInfopresenceLeave:(BOOL)fromInterrupted
{
  for (MZRemoteMezz *remoteMezz in systemModel.remoteMezzes)
  {
    remoteMezz.collaborationState = MZRemoteMezzCollaborationStateNotInCollaboration;
  }
  
  systemModel.infopresence.status = MZInfopresenceStatusInactive;
  return nil;
}


#pragma mark - Connection

-(void) disconnect
{
  systemModel.state = MZSystemStateNotConnected;
}


#pragma mark - Image Resources

-(NSURL*) urlForResource:(NSString*)resource
{
  if (resource.length == 0)
    return nil;
  
  if ([resource hasPrefix:@"http://"])
  {
    DAssert(NO, @"Implement loading of resource from file cache");
    return [NSURL URLWithString:resource];
  }
  
  return [NSURL fileURLWithPath:resource];  // Temporary solution, to load offline test dossier/workspace
}

@end
