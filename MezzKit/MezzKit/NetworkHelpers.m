//
//  NetworkHelpers.m
//  MezzKit
//
//  Created by Miguel Sánchez Valdés on 22/9/18.
//  Copyright © 2018 Oblong Industries. All rights reserved.
//

#import "NetworkHelpers.h"

#include <ifaddrs.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <ifaddrs.h>
#include <net/if.h>
#include <netdb.h>

@implementation NetworkHelpers

+ (NSString*)lookupHostIPAddressForURL:(NSURL*)url
{
  if (!url)
    return nil;

  // Ask the unix subsytem to query the DNS
  struct hostent *remoteHostEnt = gethostbyname([[url host] UTF8String]);
  if (!remoteHostEnt)
    return nil;

  // Get address info from host entry
  struct in_addr *remoteInAddr = (struct in_addr *) remoteHostEnt->h_addr_list[0];
  if (!remoteInAddr)
    return nil;

  // Convert numeric addr to ASCII string
  char *sRemoteInAddr = inet_ntoa(*remoteInAddr);
  if (!sRemoteInAddr)
    return nil;

  // hostIP
  NSString* hostIP = [NSString stringWithUTF8String:sRemoteInAddr];
  return hostIP;
}


@end
