//
//  MZInfopresenceCall.h
//  MezzKit
//
//  Created by Zai Chang on 8/17/15.
//  Copyright (c) 2015 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MZRemoteMezz.h"


extern NSString* const kMZInfopresenceCallTypeInvite;
extern NSString* const kMZInfopresenceCallTypeJoinRequest;

extern NSString* const kMZInfopresenceCallResolutionDeclined;
extern NSString* const kMZInfopresenceCallResolutionAccepted;
extern NSString* const kMZInfopresenceCallResolutionTimeOut;
extern NSString* const kMZInfopresenceCallResolutionTime_Out;
extern NSString* const kMZInfopresenceCallResolutionCanceled;


@interface MZInfopresenceCall : NSObject

@property (nonatomic, copy) NSString *uid;
@property (nonatomic, assign) MZInfopresenceCallType type;
@property (nonatomic, assign) MZInfopresenceCallResolution resolution;

@property (nonatomic, copy) NSDate *sentDate;				// Used by invite
@property (nonatomic, copy) NSDate *receivedDate;		// Used by join request

- (instancetype)initWithDictionary:(NSDictionary*)dictionary;
- (void)updateWithDictionary:(NSDictionary*)dictionary;

- (BOOL)isInvite;
- (BOOL)isJoinRequest;
- (BOOL)didNotSuceed;	// If the call did not go through whether declined, canceled or timedout

@end
