//
//  MZMezzanine.h
//  Mezzanine
//
//  Created by Miguel Sánchez Valdés on 11/6/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MZParticipant;

typedef NS_ENUM(NSInteger, MZMezzanineRoomType) {
  MZMezzanineRoomTypeLocal,
  MZMezzanineRoomTypeRemote,
  MZMezzanineRoomTypeCloud,
  MZMezzanineRoomTypeUnknown
};

@interface MZMezzanine : NSObject
{
  NSString *uid;
  NSString *name;
  NSString *location;
  NSString *company;
  NSString *version;
}

@property (nonatomic, strong) NSString *uid;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *location;
@property (nonatomic, strong) NSString *company;
@property (nonatomic, strong) NSString *version;
@property (nonatomic, strong) NSString *model;
@property (nonatomic, strong) NSMutableArray<MZParticipant *> *participants;
@property (nonatomic, assign) MZMezzanineRoomType roomType;

-(void) updateWithDictionary:(NSDictionary*)dictionary;
-(void) reset;


#pragma mark - Participants

KVO_MUTABLEARRAY_ACCESSOR_INTERFACE(Participants, participants)

-(NSArray*) participantsWithUid:(NSString*)uid;

@end
