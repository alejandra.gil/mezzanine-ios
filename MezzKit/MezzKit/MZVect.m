//
//  MZVect.m
//  MezzKit
//
//  Created by Miguel Sánchez Valdés on 26/03/2019.
//  Copyright © 2019 Oblong Industries. All rights reserved.
//

#import "MZVect.h"

@implementation MZVect

- (instancetype)initWithX:(NSNumber * _Nonnull)x y:(NSNumber * _Nonnull)y
{
  self = [super init];
  if (nil == self) return nil;

  _components = [[NSArray alloc] initWithObjects:x, y, nil];

  return self;
}

- (instancetype)initWithX:(NSNumber * _Nonnull)x y:(NSNumber * _Nonnull)y z:(NSNumber * _Nonnull)z
{
  self = [super init];
  if (nil == self) return nil;

  _components = [[NSArray alloc] initWithObjects:x, y, z, nil];

  return self;
}


+ (instancetype)vectWithX:(NSNumber * _Nonnull)x y:(NSNumber * _Nonnull)y
{
  return [[MZVect alloc] initWithX:x y:y];
}

+ (instancetype)vectWithX:(NSNumber * _Nonnull)x y:(NSNumber * _Nonnull)y z:(NSNumber * _Nonnull)z
{
  return [[MZVect alloc] initWithX:x y:y z:z];
}


+ (instancetype)integerVectWithX:(NSInteger)x y:(NSInteger)y
{
  return [MZVect vectWithX:@(x) y:@(y)];
}

+ (instancetype)integerVectWithX:(NSInteger)x y:(NSInteger)y z:(NSInteger)z
{
  return [MZVect vectWithX:@(x) y:@(y) z:@(z)];
}


+ (instancetype)doubleVectWithX:(double)x y:(double)y
{
  return [MZVect vectWithX:@(x) y:@(y)];
}

+ (instancetype)doubleVectWithX:(double)x y:(double)y z:(double)z
{
  return [MZVect vectWithX:@(x) y:@(y) z:@(z)];
}

- (NSNumber * _Nonnull)x
{
  return _components[0];
}

- (NSNumber * _Nonnull)y
{
  return _components[1];
}

- (NSNumber *)z
{
  if (_components.count > 2) {
    return _components[2];
  }
  return nil;
}

-(NSString *)description
{
  if (self.z)
  {
    return [NSString stringWithFormat:@"{%@, %@, %@}", self.x, self.y, self.z];
  }
  else
  {
    return [NSString stringWithFormat:@"{%@, %@}", self.x, self.y];
  }
}

- (BOOL)isIntegerVect
{
  return strcmp([self.x objCType], @encode(NSInteger)) == 0;
}

- (BOOL)isDoubleVect
{
  return strcmp([self.x objCType], @encode(double)) == 0;
}

- (MZVect *)cross:(MZVect *)v
{
  if (self.z)
  {
    if ([self isDoubleVect] || [v isDoubleVect])
    {
      return [MZVect doubleVectWithX:self.y.doubleValue * v.z.doubleValue - self.z.doubleValue * v.y.doubleValue
                                   y:self.z.doubleValue * v.x.doubleValue - self.x.doubleValue * v.z.doubleValue
                                   z:self.x.doubleValue * v.y.doubleValue - self.y.doubleValue * v.x.doubleValue];
    }
    if ([self isIntegerVect] && [v isIntegerVect])
    {
      return [MZVect integerVectWithX:self.y.integerValue * v.z.integerValue - self.z.integerValue * v.y.integerValue
                                    y:self.z.integerValue * v.x.integerValue - self.x.integerValue * v.z.integerValue
                                    z:self.x.integerValue * v.y.integerValue - self.y.integerValue * v.x.integerValue];
    }
    else
    {
      return nil;
    }
  }
  else
  {
    return nil;
  }
}

- (NSInteger)integerVectDot:(MZVect *)v
{
  return self.x.integerValue * v.x.integerValue + self.y.integerValue * v.y.integerValue + self.z.integerValue * v.z.integerValue;
}

- (double)doubleVectDot:(MZVect *)v
{
  return self.x.doubleValue * v.x.doubleValue + self.y.doubleValue * v.y.doubleValue + self.z.doubleValue * v.z.doubleValue;
}

- (BOOL)equalToVect:(MZVect *)v
{
  if ([self isIntegerVect] && [v isIntegerVect])
  {
    return (self.x.integerValue == v.x.integerValue)
      && (self.y.integerValue == v.y.integerValue)
      && (self.z.integerValue == v.z.integerValue);
  }
  else if ([self isDoubleVect] && [v isDoubleVect])
  {
    return (self.x.doubleValue == v.x.doubleValue)
    && (self.y.doubleValue == v.y.doubleValue)
    && (self.z.doubleValue == v.z.doubleValue);
  }

  return false;
}

@end
