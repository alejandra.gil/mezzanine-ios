//
//  SystemStateResponse.h
//  MezzKit
//
//  Created by Miguel Sánchez Valdés on 25/02/2019.
//  Copyright © 2019 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SystemStateResponse : NSObject

@property (nonatomic, strong) NSDictionary *metadata;
@property (nonatomic, strong) NSString *mezzanineVersion;
@property (nonatomic, assign) NSInteger passkeyLength;
@property (nonatomic, assign) BOOL passkeyRequired;
@property (nonatomic, strong) NSString* errorDescription;
@property (nonatomic, strong) NSString* errorSummary;
@property (nonatomic, assign) NSInteger errorCode;

-(instancetype) initWithData:(id)data;

@end

NS_ASSUME_NONNULL_END
