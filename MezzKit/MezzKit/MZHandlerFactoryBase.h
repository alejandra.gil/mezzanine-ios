//
//  MZHandlerFactoryBase.h
//  Mezzanine
//
//  Created by Zai Chang on 1/25/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MZCommunicator.h"


@interface MZHandlerFactoryBase : NSObject
{
@protected
  MZCommunicator *communicator;
  MZSystemModel *systemModel;
}

@property (nonatomic, strong) MZCommunicator *communicator;
@property (nonatomic, strong) MZSystemModel *systemModel;

@end
