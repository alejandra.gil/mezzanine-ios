//
//  MZLiveStream.m
//  Mezzanine
//
//  Created by Miguel Sánchez Valdés on 12/4/12.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import "MZLiveStream.h"
#import "NSObject+KVCAdditions.h"

@implementation MZLiveStream

- (void)updateWithDictionary:(NSDictionary *)dictionary
{
  [self setValueIfNotNullAndChanged:dictionary[@"source-id"] forKey:@"uid"];

  NSNumber *activeNumber = dictionary[@"active"];
  if (activeNumber)
    self.active = [activeNumber boolValue]; // Don't set this value unless the key is present, otherwise logic in setUid: could be overridden

  self.displayInfo = dictionary[@"display-info"];

  // For remote videos, we don't get display-name and origin information from live-stream-details
  if (!_local && self.displayInfo)
  {
    self.displayName = [self.displayInfo valueForKeyPath:@"display-name"];
    self.remoteMezzName = [self.displayInfo valueForKeyPath:@"mezz-info.name"];
  }

  NSString *displayName = dictionary[@"display-name"];
  if (displayName)
    self.displayName = displayName;
}


- (void)setUid:(NSString *)aUid
{
  [super setUid:aUid];
  
  // Origin of video according to its content-source extracted from its uid:
  //
  //  local: vi-bmagic-0/_local_
  //  VTC: vi-teleconf/_vtc_
  //  remote: vi-bmagic-2/8vW5f_VlRv68dzVrYp7ibQ
  
  _local = ([self.uid hasSuffix:@"_local_"] || [self.uid hasSuffix:@"_vtc_"]);

  // If not a local video, assume active because that property depends on the live-stream-details protein which does not contain remote sources
  if (!_local)
    _active = YES;
}

@end
