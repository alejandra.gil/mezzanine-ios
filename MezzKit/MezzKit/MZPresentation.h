//
//  MZPresentation.h
//  MezzKit
//
//  Created by Zai Chang on 1/16/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MZPortfolioItem.h"
#import "KVOHelpers.h"


@class MZWorkspace;


@interface MZPresentation : NSObject

@property (nonatomic, assign) BOOL active;

@property (nonatomic, assign) NSInteger currentSlideIndex;
@property (nonatomic, assign, readonly) NSInteger numberOfSlides;
@property (nonatomic, strong) NSMutableArray *slides;

KVO_MUTABLEARRAY_ACCESSOR_INTERFACE(Slides, slides)

-(MZPortfolioItem*) slideWithUid:(NSString*)aUid;
-(NSString*) slideUidAtIndex:(NSInteger)index;
-(void) updateSlideIndexes;
-(void) moveSlide:(MZPortfolioItem*)slide toIndex:(NSInteger)index;
-(void) moveSlideWithUid:(NSString*)aUid toIndex:(NSInteger)index;
-(void) swapSlide:(MZPortfolioItem*)slidea withSlide:(MZPortfolioItem*)slideb;
-(void) removeAllSlides;

-(NSArray*) slidesWithContentSource:(NSString*)contentSource;

@property (nonatomic, assign) MZWorkspace *workspace; // Workspace to which this presentation belong
- (void)updateWithDictionary:(NSDictionary*)dictionary;
- (void)updateWithPortfolioItems:(NSArray*)portfolioItems;

@end
