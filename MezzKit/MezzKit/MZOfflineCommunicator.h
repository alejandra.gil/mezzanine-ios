//
//  MZOfflineCommunicator.h
//  MezzKit
//
//  Created by Zai Chang on 1/15/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import "MZCommunicator.h"


/// An communicator class that replaces MZCommunicator if the user is viewing
/// a workspace offline, or for interface testing via a mock MZSystemModel
///
@interface MZOfflineCommunicator : MZCommunicator

@end
