//
//  MZSurface.m
//  MezzKit
//
//  Created by miguel on 25/2/15.
//  Copyright (c) 2015 Oblong Industries. All rights reserved.
//

#import "MZSurface.h"

@implementation MZSurface

-(instancetype) initWithName:(NSString*)aName
{
  self = [super init];
  if (self)
  {
    self.name = aName;
    self.felds = [NSMutableArray new];
  }
  return self;
}


-(void) updateWithDictionary:(NSDictionary *)dictionary
{
  NSDictionary *boundingFeldDictionary = dictionary[@"bounding-feld"];
  if (boundingFeldDictionary)
  {
    MZFeld *feld = [MZFeld feldWithName:boundingFeldDictionary[@"id"]];
    [feld updateWithDictionary:boundingFeldDictionary];
    self.boundingFeld = feld;
  }

  NSArray *feldsArray = dictionary[@"felds"];
  NSMutableArray *feldsInSurface = [NSMutableArray new];
  if (feldsArray)
  {
    for (NSDictionary *feldDictionary in feldsArray)
    {
      NSString *feldName = feldDictionary[@"id"];
      MZFeld *feld = [self feldWithName:feldName];
      if (!feld)
      {
        feld = [MZFeld feldWithName:feldName];
        [feld updateWithDictionary:feldDictionary];
      }
      [feldsInSurface addObject:feld];
    }
    self.felds = feldsInSurface;
  }
}

-(MZFeld *) feldWithName:(NSString*)aName
{
  for (MZFeld *feld in self.felds)
  {
    if ([feld.name isEqual:aName])
      return feld;
  }
  return nil;
}


@end
