//
//  MZSlide.h
//  Mezzanine
//
//  Created by Zai Chang on 2/15/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import "MZItem.h"

/// Model object for a slide within the deck of a dossier.
/// Used when connected to Mezzanine v1.6 to v2.10
///
@interface MZSlide : MZItem

@end

