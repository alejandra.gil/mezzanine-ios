//
//  MZDiskImageOperation.h
//  MezzKit
//
//  Created by Ivan Bella Lopez on 09/06/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MZDiskImageOperation : NSBlockOperation

typedef void (^URLImageFromDiskDoneBlock)(MZImage *image);

@property (nonatomic, strong, readonly, nonnull) id target;
@property (nonatomic, strong, readonly, nullable) NSString *cacheKey;

+ (MZDiskImageOperation*)operationWithTarget:(id)aTarget cacheKey:(NSString *)key completion:(URLImageFromDiskDoneBlock)completionBlock;

NS_ASSUME_NONNULL_END

@end

