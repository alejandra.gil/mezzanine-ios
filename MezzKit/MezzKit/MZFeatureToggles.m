//
//  MZFeatureToggles.m
//  MezzKit
//
//  Created by miguel on 2/2/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

#import "MZFeatureToggles.h"

@implementation MZFeatureToggles

- (instancetype)init
{
  self = [super init];
  if (self)
  {
    _infopresenceInvitesEnabled = NO;
    _remoteAccessEnabled = NO;
    _disableDownloadsEnabled = NO;
    _largeImagesEnabled = NO;
    _immenseImagesEnabled = NO;
    _accessWarning = NO;
    _roomKey = NO;
    _participantRoster = NO;
  }
  return self;
}

@end
