//
//  MZInfopresence.h
//  MezzKit
//
//  Created by miguel on 5/6/15.
//  Copyright (c) 2015 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MZRemoteParticipant.h"
#import "MZRemoteMezz.h"
#import "MZInfopresenceCall.h"


extern NSString* const kMZInfopresenceStatusInactive;
extern NSString* const kMZInfopresenceStatusPending;
extern NSString* const kMZInfopresenceStatusActive;
extern NSString* const kMZInfopresenceStatusInterrupted;

extern NSString* const kMZInfopresenceRemoteAccessStatusActive;
extern NSString* const kMZInfopresenceRemoteAccessStatusInactive;
extern NSString* const kMZInfopresenceRemoteAccessStatusStarting;


typedef NS_ENUM(NSUInteger, MZInfopresenceRemoteAccessStatus)
{
  MZInfopresenceRemoteAccessStatusActive,
  MZInfopresenceRemoteAccessStatusInactive,
  MZInfopresenceRemoteAccessStatusStarting
};

typedef NS_ENUM(NSUInteger, MZInfopresenceStatus)
{
  MZInfopresenceStatusInactive,
  MZInfopresenceStatusPending,
  MZInfopresenceStatusActive,
  MZInfopresenceStatusInterrupted
};


@interface MZInfopresence : NSObject

@property (nonatomic, assign) MZInfopresenceStatus status;

@property (nonatomic, strong) NSMutableArray<MZMezzanine *> *rooms;
@property (nonatomic, strong) NSMutableArray<MZRemoteParticipant *> *remoteParticipants;

@property (nonatomic, strong) NSMutableArray *incomingCalls;
@property (nonatomic, strong) NSMutableArray *outgoingCalls;

@property (nonatomic, assign) BOOL remoteParticipantsEnabled;	// This property is in the client-join protein under "remote-access-enabled"
@property (nonatomic, assign) MZInfopresenceRemoteAccessStatus remoteAccessStatus;
@property (nonatomic, strong) NSString *remoteAccessUrl;

@property (nonatomic, assign) BOOL infopresenceInstalled;

@property (nonatomic, strong) NSString *videoChatService;
@property (nonatomic, strong) NSString *pexipNode;
@property (nonatomic, strong) NSString *pexipConference;


- (void)reset;


#pragma mark - Incoming Calls

KVO_MUTABLEARRAY_ACCESSOR_INTERFACE(IncomingCalls, incomingCalls)

- (MZInfopresenceCall*) incomingCallWithUid:(NSString*)uid;
- (void)updateIncomingCallsWithDictionary:(NSDictionary*)dictionary;	// Expects dictionary with key 'incoming-calls', which is a list of calls


#pragma mark - Outgoing Calls

KVO_MUTABLEARRAY_ACCESSOR_INTERFACE(OutgoingCalls, outgoingCalls)

- (MZInfopresenceCall*) outgoingCallWithUid:(NSString*)uid;
- (void)updateOutgoingCallsWithDictionary:(NSDictionary*)dictionary;	// Expects dictionary with key 'outgoing-calls', which is a list of calls

@property (nonatomic, strong, readonly) NSArray *pendingInvites;
@property (nonatomic, assign, readonly) NSInteger pendingInvitesCount;

#pragma mark - Rooms

KVO_MUTABLEARRAY_ACCESSOR_INTERFACE(Rooms, rooms)


#pragma mark - RemoteParticipants

KVO_MUTABLEARRAY_ACCESSOR_INTERFACE(RemoteParticipants, remoteParticipants)

- (MZRemoteParticipant *)remoteParticipantWithUid:(NSString *)uid;
- (MZMezzanine *)mezzanineRoomWithUid:(NSString *)uid;


#pragma mark - VideoChat

- (void)updateWithVideoChatDictionary:(NSDictionary *)dictionary;

@end
