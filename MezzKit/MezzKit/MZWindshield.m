//
//  MZWindshield.m
//  Mezzanine
//
//  Created by Zai Chang on 5/8/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import "MZWindshield.h"


@implementation MZWindshield

@synthesize opacity;
@synthesize items;

KVO_MUTABLEARRAY_ACCESSOR_IMPLEMENTATION(Items, items, items)

-(id) init
{
  if ((self = [super init]))
  {
    items = [[NSMutableArray alloc] init];
  }
  return self;
}

-(void) updateWithDictionary:(NSDictionary *)dictionary
{
  // TODO
}

-(MZWindshieldItem*) itemWithUid:(NSString*)aUid
{
  for (MZWindshieldItem *item in items)
  {
    if ([item.uid isEqual:aUid])
      return item;
  }
  return nil;
}

-(NSInteger) indexOfItemWithUid:(NSString*)aUid
{
  for (NSInteger i=0; i<items.count; i++)
  {
    MZWindshieldItem *item = items[i];
    if ([item.uid isEqual:aUid])
      return i;
  }
  return NSNotFound;
}


-(NSArray*) itemsWithContentSource:(NSString*)contentSource
{
  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"assetUid == %@", contentSource];
  return [items filteredArrayUsingPredicate:predicate];
}


-(void) clear
{
  if (items.count > 0)
    [self removeItemsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, items.count)]];
}


- (void)updateWithWindshieldItems:(NSArray *)windshieldItemsArray
{
  if (windshieldItemsArray == nil)
    return;

  // Do sanity check of duplicated items to be safe
  NSArray *uniqueWindshieldItems = [self filterDuplicatedItems:windshieldItemsArray];
  NSArray *uids = [uniqueWindshieldItems valueForKey:@"uid"];
  
  // Remove items if they no longer exists
  NSMutableIndexSet *indexSet = [NSMutableIndexSet indexSet];
  for (NSInteger i=0; i<self.countOfItems; i++)
  {
    MZWindshieldItem *item = items[i];
    if (item.uid && ![uids containsObject:item.uid])
    {
      [indexSet addIndex:i];
    }
  }
  if ([indexSet count] > 0)
    [self removeItemsAtIndexes:indexSet];


  NSMutableArray *itemsToInsert = [[NSMutableArray alloc] init];
  NSMutableIndexSet *indexSetForItems = [NSMutableIndexSet indexSet];
  
  for (NSInteger i=0; i<[uids count]; i++)
  {
    NSString *uid = uids[i];
    MZWindshieldItem *item = [self itemWithUid:uid];
    NSDictionary *windshieldInfo = uniqueWindshieldItems[i];
    
    if (item)
    {
      [item updateWithDictionary:windshieldInfo];
    }
    else
    {
      item = [[MZWindshieldItem alloc] init];
      item.uid = uid;
      [item updateWithDictionary:windshieldInfo];
      [itemsToInsert addObject:item];
      [indexSetForItems addIndex:i];
    }
  }
  
  if (itemsToInsert.count)
    [self insertItems:itemsToInsert atIndexes:indexSetForItems];
}

// This helper method filters out a list of windshield items and keeps
// the first appearance of duplicated items
- (NSArray *)filterDuplicatedItems:(NSArray *)allItems
{
  NSMutableArray *returningArray = [NSMutableArray new];
  NSMutableArray *uids = [NSMutableArray new];

  for (NSDictionary *item in allItems)
  {
    if (![uids containsObject:[item objectForKey:@"uid"]])
    {
      [returningArray addObject:item];
      [uids addObject:[item objectForKey:@"uid"]];
    }
  }

  if (allItems.count != returningArray.count)
    DLog(@"WARNING: This windshield item array contains duplicated items.");
  return returningArray;
}

- (NSArray *)itemsInPrimaryWindshield
{
  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"surface.name == %@", @"main"];
  return [items filteredArrayUsingPredicate:predicate];
}


- (NSArray *)itemsInExtendedWindshield
{
  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"surface.name != %@", @"main"];
  return [items filteredArrayUsingPredicate:predicate];
}

- (NSArray *)itemsInSurface:(MZSurface *)surface
{
  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"surface.name == %@", surface.name];
  return [items filteredArrayUsingPredicate:predicate];
}

@end
