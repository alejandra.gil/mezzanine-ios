//
//  MZCommunicator+Handlers.h
//  MezzKit
//
//  Created by Zai Chang on 6/4/15.
//  Copyright (c) 2015 Oblong Industries. All rights reserved.
//

#import "MZCommunicator.h"

@interface MZCommunicator (Handlers)

- (void)addMessageHandlers;

@end
