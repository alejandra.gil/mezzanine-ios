// Automatically generated code, do not edit.
#import "MZSlaws.h"



@implementation Pat


-(instancetype) initWithArray:(NSArray*)array
{
	self = [super init];
	if (self)
	{
		_provenance = array[0];
		_transactionId = [array[1] integerValue];
	}
	return self;
}

-(NSArray*)arrayRepresentation
{
	NSMutableArray *array = [NSMutableArray new];
	[array addObject:_provenance];
	[array addObject:@(_transactionId)];
	return array;
}

@end


@implementation Vec2


-(instancetype) initWithArray:(NSArray*)array
{
	self = [super init];
	if (self)
	{
		_x = [array[0] doubleValue];
		_y = [array[1] doubleValue];
	}
	return self;
}

-(NSArray*)arrayRepresentation
{
	NSMutableArray *array = [NSMutableArray new];
	[array addObject:@(_x)];
	[array addObject:@(_y)];
	return array;
}

@end


@implementation Vec2i


-(instancetype) initWithArray:(NSArray*)array
{
	self = [super init];
	if (self)
	{
		_x = [array[0] integerValue];
		_y = [array[1] integerValue];
	}
	return self;
}

-(NSArray*)arrayRepresentation
{
	NSMutableArray *array = [NSMutableArray new];
	[array addObject:@(_x)];
	[array addObject:@(_y)];
	return array;
}

@end


@implementation Vec3


-(instancetype) initWithArray:(NSArray*)array
{
	self = [super init];
	if (self)
	{
		_x = [array[0] doubleValue];
		_y = [array[1] doubleValue];
		_z = [array[2] doubleValue];
	}
	return self;
}

-(NSArray*)arrayRepresentation
{
	NSMutableArray *array = [NSMutableArray new];
	[array addObject:@(_x)];
	[array addObject:@(_y)];
	[array addObject:@(_z)];
	return array;
}

@end


@implementation Vec4


-(instancetype) initWithArray:(NSArray*)array
{
	self = [super init];
	if (self)
	{
		_x = [array[0] doubleValue];
		_y = [array[1] doubleValue];
		_z = [array[2] doubleValue];
		_w = [array[3] doubleValue];
	}
	return self;
}

-(NSArray*)arrayRepresentation
{
	NSMutableArray *array = [NSMutableArray new];
	[array addObject:@(_x)];
	[array addObject:@(_y)];
	[array addObject:@(_z)];
	[array addObject:@(_w)];
	return array;
}

@end


@implementation FeatureToggles


-(instancetype) initWithDictionary:(NSDictionary*)dictionary
{
	self = [super init];
	if (self)
	{
		_remoteAccess = [dictionary[@"remote-access"] boolValue];
		_m2mInvites = [dictionary[@"m2m-invites"] boolValue];
		_wand2 = [dictionary[@"wand-2"] boolValue];
		_disableDownloads = [dictionary[@"disable-downloads"] boolValue];
		_samlAuth = [dictionary[@"saml-auth"] boolValue];
		_diskEncryption = [dictionary[@"disk-encryption"] boolValue];
		_logForwarding = [dictionary[@"log-forwarding"] boolValue];
		_watchdog = [dictionary[@"watchdog"] boolValue];
		_webTeamworkUi = [dictionary[@"web-teamwork-ui"] boolValue];
		_accessWarning = [dictionary[@"access-warning"] boolValue];
		_webthing = [dictionary[@"webthing"] boolValue];
		_oblongMesaRadeon = [dictionary[@"oblong-mesa-radeon"] boolValue];
		_s4b = [dictionary[@"s4b"] boolValue];
		_enableHsts = [dictionary[@"enable-hsts"] boolValue];
		_enableCsrf = [dictionary[@"enable-csrf"] boolValue];
		_thirdPartyCerts = [dictionary[@"third-party-certs"] boolValue];
		_adminApi = [dictionary[@"admin-api"] boolValue];
		_vtcAutoplay = [dictionary[@"vtc-autoplay"] boolValue];
		_rtspB2b = [dictionary[@"rtsp-b2b"] boolValue];
		_advancedPip = [dictionary[@"advanced-pip"] boolValue];
		_setupWizard = [dictionary[@"setup-wizard"] boolValue];
		_obConman = [dictionary[@"ob-conman"] boolValue];
		_obConmanExperimental = [dictionary[@"ob-conman-experimental"] boolValue];
		_participantRoster = [dictionary[@"participant-roster"] boolValue];
		_roomKey = [dictionary[@"room-key"] boolValue];
		_privacyKey = [dictionary[@"privacy-key"] boolValue];
	}
	return self;
}

-(NSDictionary*)dictionaryRepresentation
{
	NSMutableDictionary *dictionary = [NSMutableDictionary new];
	if (_remoteAccess)
		dictionary[@"remote-access"] = @(_remoteAccess);
	if (_m2mInvites)
		dictionary[@"m2m-invites"] = @(_m2mInvites);
	if (_wand2)
		dictionary[@"wand-2"] = @(_wand2);
	if (_disableDownloads)
		dictionary[@"disable-downloads"] = @(_disableDownloads);
	if (_samlAuth)
		dictionary[@"saml-auth"] = @(_samlAuth);
	if (_diskEncryption)
		dictionary[@"disk-encryption"] = @(_diskEncryption);
	if (_logForwarding)
		dictionary[@"log-forwarding"] = @(_logForwarding);
	if (_watchdog)
		dictionary[@"watchdog"] = @(_watchdog);
	if (_webTeamworkUi)
		dictionary[@"web-teamwork-ui"] = @(_webTeamworkUi);
	if (_accessWarning)
		dictionary[@"access-warning"] = @(_accessWarning);
	if (_webthing)
		dictionary[@"webthing"] = @(_webthing);
	if (_oblongMesaRadeon)
		dictionary[@"oblong-mesa-radeon"] = @(_oblongMesaRadeon);
	if (_s4b)
		dictionary[@"s4b"] = @(_s4b);
	if (_enableHsts)
		dictionary[@"enable-hsts"] = @(_enableHsts);
	if (_enableCsrf)
		dictionary[@"enable-csrf"] = @(_enableCsrf);
	if (_thirdPartyCerts)
		dictionary[@"third-party-certs"] = @(_thirdPartyCerts);
	if (_adminApi)
		dictionary[@"admin-api"] = @(_adminApi);
	if (_vtcAutoplay)
		dictionary[@"vtc-autoplay"] = @(_vtcAutoplay);
	if (_rtspB2b)
		dictionary[@"rtsp-b2b"] = @(_rtspB2b);
	if (_advancedPip)
		dictionary[@"advanced-pip"] = @(_advancedPip);
	if (_setupWizard)
		dictionary[@"setup-wizard"] = @(_setupWizard);
	if (_obConman)
		dictionary[@"ob-conman"] = @(_obConman);
	if (_obConmanExperimental)
		dictionary[@"ob-conman-experimental"] = @(_obConmanExperimental);
	if (_participantRoster)
		dictionary[@"participant-roster"] = @(_participantRoster);
	if (_roomKey)
		dictionary[@"room-key"] = @(_roomKey);
	if (_privacyKey)
		dictionary[@"privacy-key"] = @(_privacyKey);
	return dictionary;
}
@end


@implementation Feld


-(instancetype) initWithDictionary:(NSDictionary*)dictionary
{
	self = [super init];
	if (self)
	{
		_id = dictionary[@"id"];
		_type = dictionary[@"type"];
		_cent = dictionary[@"cent"];
		_up = dictionary[@"up"];
		_over = dictionary[@"over"];
		_norm = dictionary[@"norm"];
		_physSize = dictionary[@"phys-size"];
		_pxSize = dictionary[@"px-size"];
		_viewDist = [dictionary[@"view-dist"] doubleValue];
		_visibleToAll = [dictionary[@"visible-to-all"] boolValue];
		_visibility = dictionary[@"visibility"];
	}
	return self;
}

-(NSDictionary*)dictionaryRepresentation
{
	NSMutableDictionary *dictionary = [NSMutableDictionary new];
	if (_id)
		dictionary[@"id"] = _id;
	dictionary[@"type"] = _type;
	dictionary[@"cent"] = _cent;
	dictionary[@"up"] = _up;
	dictionary[@"over"] = _over;
	dictionary[@"norm"] = _norm;
	dictionary[@"phys-size"] = _physSize;
	dictionary[@"px-size"] = _pxSize;
	dictionary[@"view-dist"] = @(_viewDist);
	dictionary[@"visible-to-all"] = @(_visibleToAll);
	if (_visibility)
		dictionary[@"visibility"] = _visibility;
	return dictionary;
}
@end


@implementation FeldRelativeCoords


-(instancetype) initWithDictionary:(NSDictionary*)dictionary
{
	self = [super init];
	if (self)
	{
		_feldId = dictionary[@"feld-id"];
		_over = [dictionary[@"over"] doubleValue];
		_up = [dictionary[@"up"] doubleValue];
		_norm = [dictionary[@"norm"] doubleValue];
		_valid = [dictionary[@"valid"] boolValue];
	}
	return self;
}

-(NSDictionary*)dictionaryRepresentation
{
	NSMutableDictionary *dictionary = [NSMutableDictionary new];
	dictionary[@"feld-id"] = _feldId;
	dictionary[@"over"] = @(_over);
	dictionary[@"up"] = @(_up);
	dictionary[@"norm"] = @(_norm);
	dictionary[@"valid"] = @(_valid);
	return dictionary;
}
@end


@implementation FeldRelativeSize


-(instancetype) initWithDictionary:(NSDictionary*)dictionary
{
	self = [super init];
	if (self)
	{
		_feldId = dictionary[@"feld-id"];
		_scale = [dictionary[@"scale"] doubleValue];
		_aspectRatio = [dictionary[@"aspect-ratio"] doubleValue];
	}
	return self;
}

-(NSDictionary*)dictionaryRepresentation
{
	NSMutableDictionary *dictionary = [NSMutableDictionary new];
	dictionary[@"feld-id"] = _feldId;
	dictionary[@"scale"] = @(_scale);
	dictionary[@"aspect-ratio"] = @(_aspectRatio);
	return dictionary;
}
@end


@implementation FileRequest


-(instancetype) initWithDictionary:(NSDictionary*)dictionary
{
	self = [super init];
	if (self)
	{
		_fileId = [dictionary[@"file-id"] integerValue];
		_fileName = dictionary[@"file-name"];
		_type = dictionary[@"type"];
	}
	return self;
}

-(NSDictionary*)dictionaryRepresentation
{
	NSMutableDictionary *dictionary = [NSMutableDictionary new];
	dictionary[@"file-id"] = @(_fileId);
	dictionary[@"file-name"] = _fileName;
	dictionary[@"type"] = _type;
	return dictionary;
}
@end


@implementation FileResponse


-(instancetype) initWithDictionary:(NSDictionary*)dictionary
{
	self = [super init];
	if (self)
	{
		_fileId = [dictionary[@"file-id"] integerValue];
		_fileName = dictionary[@"file-name"];
		_type = dictionary[@"type"];
		_uids = dictionary[@"uids"];
		_pages = [dictionary[@"pages"] integerValue];
	}
	return self;
}

-(NSDictionary*)dictionaryRepresentation
{
	NSMutableDictionary *dictionary = [NSMutableDictionary new];
	dictionary[@"file-id"] = @(_fileId);
	dictionary[@"file-name"] = _fileName;
	dictionary[@"type"] = _type;
	dictionary[@"uids"] = _uids;
	if (_pages)
		dictionary[@"pages"] = @(_pages);
	return dictionary;
}
@end


@implementation InfopresenceCall


-(instancetype) initWithDictionary:(NSDictionary*)dictionary
{
	self = [super init];
	if (self)
	{
		_uid = dictionary[@"uid"];
		_type = dictionary[@"type"];
		_sentUtc = [dictionary[@"sent-utc"] doubleValue];
		_receivedUtc = [dictionary[@"received-utc"] doubleValue];
	}
	return self;
}

-(NSDictionary*)dictionaryRepresentation
{
	NSMutableDictionary *dictionary = [NSMutableDictionary new];
	dictionary[@"uid"] = _uid;
	dictionary[@"type"] = _type;
	if (_sentUtc)
		dictionary[@"sent-utc"] = @(_sentUtc);
	if (_receivedUtc)
		dictionary[@"received-utc"] = @(_receivedUtc);
	return dictionary;
}
@end


@implementation Whiteboard


-(instancetype) initWithDictionary:(NSDictionary*)dictionary
{
	self = [super init];
	if (self)
	{
		_uid = dictionary[@"uid"];
		_name = dictionary[@"name"];
	}
	return self;
}

-(NSDictionary*)dictionaryRepresentation
{
	NSMutableDictionary *dictionary = [NSMutableDictionary new];
	dictionary[@"uid"] = _uid;
	if (_name)
		dictionary[@"name"] = _name;
	return dictionary;
}
@end


@implementation AssetUploadLimits


-(instancetype) initWithDictionary:(NSDictionary*)dictionary
{
	self = [super init];
	if (self)
	{
		_uploadMaxHeight = [dictionary[@"upload-max-height"] integerValue];
		_uploadMaxImageSizeMb = [dictionary[@"upload-max-image-size-mb"] integerValue];
		_uploadMaxPdfSizeMb = [dictionary[@"upload-max-pdf-size-mb"] integerValue];
		_uploadMaxSizeMb = [dictionary[@"upload-max-size-mb"] integerValue];
		_uploadMaxWidth = [dictionary[@"upload-max-width"] integerValue];
	}
	return self;
}

-(NSDictionary*)dictionaryRepresentation
{
	NSMutableDictionary *dictionary = [NSMutableDictionary new];
	dictionary[@"upload-max-height"] = @(_uploadMaxHeight);
	dictionary[@"upload-max-image-size-mb"] = @(_uploadMaxImageSizeMb);
	dictionary[@"upload-max-pdf-size-mb"] = @(_uploadMaxPdfSizeMb);
	dictionary[@"upload-max-size-mb"] = @(_uploadMaxSizeMb);
	dictionary[@"upload-max-width"] = @(_uploadMaxWidth);
	return dictionary;
}
@end


@implementation PresentationState


-(instancetype) initWithDictionary:(NSDictionary*)dictionary
{
	self = [super init];
	if (self)
	{
		_active = [dictionary[@"active"] boolValue];
		_currentIndex = [dictionary[@"current-index"] integerValue];
	}
	return self;
}

-(NSDictionary*)dictionaryRepresentation
{
	NSMutableDictionary *dictionary = [NSMutableDictionary new];
	dictionary[@"active"] = @(_active);
	dictionary[@"current-index"] = @(_currentIndex);
	return dictionary;
}
@end


@implementation WorkspaceStateClosed


-(instancetype) initWithDictionary:(NSDictionary*)dictionary
{
	self = [super init];
	if (self)
	{
		_uid = dictionary[@"uid"];
		_destroyed = [dictionary[@"destroyed"] boolValue];
	}
	return self;
}

-(NSDictionary*)dictionaryRepresentation
{
	NSMutableDictionary *dictionary = [NSMutableDictionary new];
	dictionary[@"uid"] = _uid;
	dictionary[@"destroyed"] = @(_destroyed);
	return dictionary;
}
@end


@implementation WorkspaceState


-(instancetype) initWithDictionary:(NSDictionary*)dictionary
{
	self = [super init];
	if (self)
	{
		_uid = dictionary[@"uid"];
		_name = dictionary[@"name"];
		_modDate = dictionary[@"mod-date"];
		_modDateUtc = [dictionary[@"mod-date-utc"] integerValue];
		_opening = [dictionary[@"opening"] boolValue];
		_public = [dictionary[@"public"] boolValue];
		_saved = [dictionary[@"saved"] boolValue];
		_thumbUri = dictionary[@"thumb-uri"];
		_presentation = dictionary[@"presentation"];
		_liveStreams = dictionary[@"live-streams"];
		_portfolioItems = dictionary[@"portfolio-items"];
		_windshieldItems = dictionary[@"windshield-items"];
	}
	return self;
}

-(NSDictionary*)dictionaryRepresentation
{
	NSMutableDictionary *dictionary = [NSMutableDictionary new];
	dictionary[@"uid"] = _uid;
	dictionary[@"name"] = _name;
	dictionary[@"mod-date"] = _modDate;
	dictionary[@"mod-date-utc"] = @(_modDateUtc);
	dictionary[@"opening"] = @(_opening);
	dictionary[@"public"] = @(_public);
	dictionary[@"saved"] = @(_saved);
	dictionary[@"thumb-uri"] = _thumbUri;
	dictionary[@"presentation"] = _presentation;
	dictionary[@"live-streams"] = _liveStreams;
	dictionary[@"portfolio-items"] = _portfolioItems;
	dictionary[@"windshield-items"] = _windshieldItems;
	return dictionary;
}
@end


@implementation Surface


-(instancetype) initWithDictionary:(NSDictionary*)dictionary
{
	self = [super init];
	if (self)
	{
		_name = dictionary[@"name"];
		_boundingFeld = dictionary[@"bounding-feld"];
		_felds = dictionary[@"felds"];
	}
	return self;
}

-(NSDictionary*)dictionaryRepresentation
{
	NSMutableDictionary *dictionary = [NSMutableDictionary new];
	dictionary[@"name"] = _name;
	dictionary[@"bounding-feld"] = _boundingFeld;
	dictionary[@"felds"] = _felds;
	return dictionary;
}
@end


@implementation ImageSpec


-(instancetype) initWithDictionary:(NSDictionary*)dictionary
{
	self = [super init];
	if (self)
	{
		_uri = dictionary[@"uri"];
		_width = [dictionary[@"width"] integerValue];
		_height = [dictionary[@"height"] integerValue];
	}
	return self;
}

-(NSDictionary*)dictionaryRepresentation
{
	NSMutableDictionary *dictionary = [NSMutableDictionary new];
	dictionary[@"uri"] = _uri;
	dictionary[@"width"] = @(_width);
	dictionary[@"height"] = @(_height);
	return dictionary;
}
@end


@implementation WorkspaceItem


-(instancetype) initWithDictionary:(NSDictionary*)dictionary
{
	self = [super init];
	if (self)
	{
		_uid = dictionary[@"uid"];
		_owners = dictionary[@"owners"];
		_name = dictionary[@"name"];
		_modDate = dictionary[@"mod-date"];
		_modDateUtc = [dictionary[@"mod-date-utc"] integerValue];
		_saved = [dictionary[@"saved"] boolValue];
		_thumbUri = dictionary[@"thumb-uri"];
	}
	return self;
}

-(NSDictionary*)dictionaryRepresentation
{
	NSMutableDictionary *dictionary = [NSMutableDictionary new];
	dictionary[@"uid"] = _uid;
	dictionary[@"owners"] = _owners;
	dictionary[@"name"] = _name;
	dictionary[@"mod-date"] = _modDate;
	dictionary[@"mod-date-utc"] = @(_modDateUtc);
	dictionary[@"saved"] = @(_saved);
	dictionary[@"thumb-uri"] = _thumbUri;
	return dictionary;
}
@end


@implementation WindshieldItem


-(instancetype) initWithDictionary:(NSDictionary*)dictionary
{
	self = [super init];
	if (self)
	{
		_uid = dictionary[@"uid"];
		_displayName = dictionary[@"display-name"];
		_surfaceName = dictionary[@"surface-name"];
		_contentSource = dictionary[@"content-source"];
		_thumbUri = dictionary[@"thumb-uri"];
		_largeUri = dictionary[@"large-uri"];
		_smallUri = dictionary[@"small-uri"];
		_mediumUri = dictionary[@"medium-uri"];
		_feldInscribedUri = dictionary[@"feld-inscribed-uri"];
		_fullUri = dictionary[@"full-uri"];
		_imageAvailable = [dictionary[@"image-available"] boolValue];
		_origImageAvailable = [dictionary[@"orig-image-available"] boolValue];
		_feldRelativeCoords = dictionary[@"feld-relative-coords"];
		_feldRelativeSize = dictionary[@"feld-relative-size"];
		_feldRelativeUri = dictionary[@"feld-relative-uri"];
	}
	return self;
}

-(NSDictionary*)dictionaryRepresentation
{
	NSMutableDictionary *dictionary = [NSMutableDictionary new];
	dictionary[@"uid"] = _uid;
	if (_displayName)
		dictionary[@"display-name"] = _displayName;
	dictionary[@"surface-name"] = _surfaceName;
	dictionary[@"content-source"] = _contentSource;
	if (_thumbUri)
		dictionary[@"thumb-uri"] = _thumbUri;
	if (_largeUri)
		dictionary[@"large-uri"] = _largeUri;
	if (_smallUri)
		dictionary[@"small-uri"] = _smallUri;
	if (_mediumUri)
		dictionary[@"medium-uri"] = _mediumUri;
	if (_feldInscribedUri)
		dictionary[@"feld-inscribed-uri"] = _feldInscribedUri;
	if (_fullUri)
		dictionary[@"full-uri"] = _fullUri;
	if (_imageAvailable)
		dictionary[@"image-available"] = @(_imageAvailable);
	if (_origImageAvailable)
		dictionary[@"orig-image-available"] = @(_origImageAvailable);
	dictionary[@"feld-relative-coords"] = _feldRelativeCoords;
	dictionary[@"feld-relative-size"] = _feldRelativeSize;
	if (_feldRelativeUri)
		dictionary[@"feld-relative-uri"] = _feldRelativeUri;
	return dictionary;
}
@end


@implementation PortfolioItem


-(instancetype) initWithDictionary:(NSDictionary*)dictionary
{
	self = [super init];
	if (self)
	{
		_uid = dictionary[@"uid"];
		_contentSource = dictionary[@"content-source"];
		_displayName = dictionary[@"display-name"];
		_thumbUri = dictionary[@"thumb-uri"];
		_largeUri = dictionary[@"large-uri"];
		_smallUri = dictionary[@"small-uri"];
		_mediumUri = dictionary[@"medium-uri"];
		_feldInscribedUri = dictionary[@"feld-inscribed-uri"];
		_fullUri = dictionary[@"full-uri"];
		_imageAvailable = [dictionary[@"image-available"] boolValue];
		_origImageAvailable = [dictionary[@"orig-image-available"] boolValue];
		_feldRelativeUri = dictionary[@"feld-relative-uri"];
	}
	return self;
}

-(NSDictionary*)dictionaryRepresentation
{
	NSMutableDictionary *dictionary = [NSMutableDictionary new];
	dictionary[@"uid"] = _uid;
	dictionary[@"content-source"] = _contentSource;
	if (_displayName)
		dictionary[@"display-name"] = _displayName;
	if (_thumbUri)
		dictionary[@"thumb-uri"] = _thumbUri;
	if (_largeUri)
		dictionary[@"large-uri"] = _largeUri;
	if (_smallUri)
		dictionary[@"small-uri"] = _smallUri;
	if (_mediumUri)
		dictionary[@"medium-uri"] = _mediumUri;
	if (_feldInscribedUri)
		dictionary[@"feld-inscribed-uri"] = _feldInscribedUri;
	if (_fullUri)
		dictionary[@"full-uri"] = _fullUri;
	if (_imageAvailable)
		dictionary[@"image-available"] = @(_imageAvailable);
	if (_origImageAvailable)
		dictionary[@"orig-image-available"] = @(_origImageAvailable);
	if (_feldRelativeUri)
		dictionary[@"feld-relative-uri"] = _feldRelativeUri;
	return dictionary;
}
@end


@implementation LivestreamItem


-(instancetype) initWithDictionary:(NSDictionary*)dictionary
{
	self = [super init];
	if (self)
	{
		_sourceId = dictionary[@"source-id"];
		_active = [dictionary[@"active"] boolValue];
		_displayName = dictionary[@"display-name"];
	}
	return self;
}

-(NSDictionary*)dictionaryRepresentation
{
	NSMutableDictionary *dictionary = [NSMutableDictionary new];
	dictionary[@"source-id"] = _sourceId;
	dictionary[@"active"] = @(_active);
	dictionary[@"display-name"] = _displayName;
	return dictionary;
}
@end


@implementation MezzDetail


-(instancetype) initWithDictionary:(NSDictionary*)dictionary
{
	self = [super init];
	if (self)
	{
		_uid = dictionary[@"uid"];
		_name = dictionary[@"name"];
		_active = [dictionary[@"active"] boolValue];
		_address = dictionary[@"address"];
		_company = dictionary[@"company"];
		_lastContactUtc = dictionary[@"last-contact-utc"];
		_location = dictionary[@"location"];
		_certificate = dictionary[@"certificate"];
	}
	return self;
}

-(NSDictionary*)dictionaryRepresentation
{
	NSMutableDictionary *dictionary = [NSMutableDictionary new];
	dictionary[@"uid"] = _uid;
	dictionary[@"name"] = _name;
	dictionary[@"active"] = @(_active);
	dictionary[@"address"] = _address;
	if (_company)
		dictionary[@"company"] = _company;
	if (_lastContactUtc)
		dictionary[@"last-contact-utc"] = _lastContactUtc;
	if (_location)
		dictionary[@"location"] = _location;
	if (_certificate)
		dictionary[@"certificate"] = _certificate;
	return dictionary;
}
@end


@implementation MezzMetadata


-(instancetype) initWithDictionary:(NSDictionary*)dictionary
{
	self = [super init];
	if (self)
	{
		_cloudInstance = [dictionary[@"cloud-instance"] boolValue];
		_featureToggles = dictionary[@"feature-toggles"];
		_apiVersion = dictionary[@"api-version"];
	}
	return self;
}

-(NSDictionary*)dictionaryRepresentation
{
	NSMutableDictionary *dictionary = [NSMutableDictionary new];
	dictionary[@"cloud-instance"] = @(_cloudInstance);
	if (_featureToggles)
		dictionary[@"feature-toggles"] = _featureToggles;
	dictionary[@"api-version"] = _apiVersion;
	return dictionary;
}
@end


@implementation RemoteParticipant


-(instancetype) initWithDictionary:(NSDictionary*)dictionary
{
	self = [super init];
	if (self)
	{
		_uid = dictionary[@"uid"];
		_displayName = dictionary[@"display-name"];
	}
	return self;
}

-(NSDictionary*)dictionaryRepresentation
{
	NSMutableDictionary *dictionary = [NSMutableDictionary new];
	dictionary[@"uid"] = _uid;
	dictionary[@"display-name"] = _displayName;
	return dictionary;
}
@end


@implementation Handi


-(instancetype) initWithDictionary:(NSDictionary*)dictionary
{
	self = [super init];
	if (self)
	{
		_color = dictionary[@"color"];
		_quadrant = dictionary[@"quadrant"];
		_ratchetState = dictionary[@"ratchet-state"];
	}
	return self;
}

-(NSDictionary*)dictionaryRepresentation
{
	NSMutableDictionary *dictionary = [NSMutableDictionary new];
	dictionary[@"color"] = _color;
	dictionary[@"quadrant"] = _quadrant;
	dictionary[@"ratchet-state"] = _ratchetState;
	return dictionary;
}
@end


@implementation PressureReport


-(instancetype) initWithDictionary:(NSDictionary*)dictionary
{
	self = [super init];
	if (self)
	{
		_main = dictionary[@"main"];
	}
	return self;
}

-(NSDictionary*)dictionaryRepresentation
{
	NSMutableDictionary *dictionary = [NSMutableDictionary new];
	dictionary[@"main"] = _main;
	return dictionary;
}
@end


@implementation PexipDetail


-(instancetype) initWithDictionary:(NSDictionary*)dictionary
{
	self = [super init];
	if (self)
	{
		_node = dictionary[@"node"];
		_conference = dictionary[@"conference"];
	}
	return self;
}

-(NSDictionary*)dictionaryRepresentation
{
	NSMutableDictionary *dictionary = [NSMutableDictionary new];
	dictionary[@"node"] = _node;
	dictionary[@"conference"] = _conference;
	return dictionary;
}
@end


@implementation VideoChat


-(instancetype) initWithDictionary:(NSDictionary*)dictionary
{
	self = [super init];
	if (self)
	{
		_service = dictionary[@"service"];
		_pexip = dictionary[@"pexip"];
	}
	return self;
}

-(NSDictionary*)dictionaryRepresentation
{
	NSMutableDictionary *dictionary = [NSMutableDictionary new];
	if (_service)
		dictionary[@"service"] = _service;
	if (_pexip)
		dictionary[@"pexip"] = _pexip;
	return dictionary;
}
@end


@implementation CalendarAttendee


-(instancetype) initWithDictionary:(NSDictionary*)dictionary
{
	self = [super init];
	if (self)
	{
		_email = dictionary[@"email"];
		_name = dictionary[@"name"];
	}
	return self;
}

-(NSDictionary*)dictionaryRepresentation
{
	NSMutableDictionary *dictionary = [NSMutableDictionary new];
	dictionary[@"email"] = _email;
	dictionary[@"name"] = _name;
	return dictionary;
}
@end


@implementation CalendarMeeting


-(instancetype) initWithDictionary:(NSDictionary*)dictionary
{
	self = [super init];
	if (self)
	{
		_meetingUid = dictionary[@"meeting-uid"];
		_title = dictionary[@"title"];
		_thirdPartyUrl = dictionary[@"third-party-url"];
		_mezzinUrl = dictionary[@"mezzin-url"];
		_startUtc = [dictionary[@"start-utc"] doubleValue];
		_endUtc = [dictionary[@"end-utc"] doubleValue];
		_meetingOrganizer = dictionary[@"meeting-organizer"];
		_attendees = dictionary[@"attendees"];
	}
	return self;
}

-(NSDictionary*)dictionaryRepresentation
{
	NSMutableDictionary *dictionary = [NSMutableDictionary new];
	dictionary[@"meeting-uid"] = _meetingUid;
	dictionary[@"title"] = _title;
	dictionary[@"third-party-url"] = _thirdPartyUrl;
	dictionary[@"mezzin-url"] = _mezzinUrl;
	dictionary[@"start-utc"] = @(_startUtc);
	dictionary[@"end-utc"] = @(_endUtc);
	dictionary[@"meeting-organizer"] = _meetingOrganizer;
	dictionary[@"attendees"] = _attendees;
	return dictionary;
}
@end


@implementation DialogItem


-(instancetype) initWithDictionary:(NSDictionary*)dictionary
{
	self = [super init];
	if (self)
	{
		_id = dictionary[@"id"];
		_modal = [dictionary[@"modal"] boolValue];
		_title = dictionary[@"title"];
		_message = dictionary[@"message"];
		_buttons = dictionary[@"buttons"];
	}
	return self;
}

-(NSDictionary*)dictionaryRepresentation
{
	NSMutableDictionary *dictionary = [NSMutableDictionary new];
	dictionary[@"id"] = _id;
	if (_modal)
		dictionary[@"modal"] = @(_modal);
	dictionary[@"title"] = _title;
	dictionary[@"message"] = _message;
	dictionary[@"buttons"] = _buttons;
	return dictionary;
}
@end


@implementation Button


-(instancetype) initWithDictionary:(NSDictionary*)dictionary
{
	self = [super init];
	if (self)
	{
		_type = dictionary[@"type"];
		_label = dictionary[@"label"];
		_value = dictionary[@"value"];
	}
	return self;
}

-(NSDictionary*)dictionaryRepresentation
{
	NSMutableDictionary *dictionary = [NSMutableDictionary new];
	if (_type)
		dictionary[@"type"] = _type;
	dictionary[@"label"] = _label;
	dictionary[@"value"] = _value;
	return dictionary;
}
@end


@implementation Certificate


-(instancetype) initWithDictionary:(NSDictionary*)dictionary
{
	self = [super init];
	if (self)
	{
		_issuedTo = dictionary[@"issued_to"];
	}
	return self;
}

-(NSDictionary*)dictionaryRepresentation
{
	NSMutableDictionary *dictionary = [NSMutableDictionary new];
	dictionary[@"issued_to"] = _issuedTo;
	return dictionary;
}
@end


@implementation SessionState


-(instancetype) initWithDictionary:(NSDictionary*)dictionary
{
	self = [super init];
	if (self)
	{
		_sessionId = dictionary[@"session-id"];
		_passkey = dictionary[@"passkey"];
		_displayPasskey = [dictionary[@"display-passkey"] boolValue];
		_startUtc = [dictionary[@"start-utc"] integerValue];
		_isResting = [dictionary[@"is-resting"] boolValue];
	}
	return self;
}

-(NSDictionary*)dictionaryRepresentation
{
	NSMutableDictionary *dictionary = [NSMutableDictionary new];
	dictionary[@"session-id"] = _sessionId;
	dictionary[@"passkey"] = _passkey;
	dictionary[@"display-passkey"] = @(_displayPasskey);
	dictionary[@"start-utc"] = @(_startUtc);
	dictionary[@"is-resting"] = @(_isResting);
	return dictionary;
}
@end


@implementation RoomParticipant


-(instancetype) initWithDictionary:(NSDictionary*)dictionary
{
	self = [super init];
	if (self)
	{
		_roomName = dictionary[@"room-name"];
		_uid = dictionary[@"uid"];
		_participants = dictionary[@"participants"];
	}
	return self;
}

-(NSDictionary*)dictionaryRepresentation
{
	NSMutableDictionary *dictionary = [NSMutableDictionary new];
	dictionary[@"room-name"] = _roomName;
	dictionary[@"uid"] = _uid;
	dictionary[@"participants"] = _participants;
	return dictionary;
}
@end


@implementation Participant


-(instancetype) initWithDictionary:(NSDictionary*)dictionary
{
	self = [super init];
	if (self)
	{
		_displayName = dictionary[@"display-name"];
		_type = dictionary[@"type"];
		_provenance = dictionary[@"provenance"];
	}
	return self;
}

-(NSDictionary*)dictionaryRepresentation
{
	NSMutableDictionary *dictionary = [NSMutableDictionary new];
	dictionary[@"display-name"] = _displayName;
	dictionary[@"type"] = _type;
	dictionary[@"provenance"] = _provenance;
	return dictionary;
}
@end


@implementation VtcStatusDataPsa


-(instancetype) initWithDictionary:(NSDictionary*)dictionary
{
	self = [super init];
	if (self)
	{
		_vtcOnline = [dictionary[@"vtc-online"] boolValue];
		_vtcInformation = dictionary[@"vtc-information"];
		_vtcOnCall = [dictionary[@"vtc-on-call"] boolValue];
		_vtcRinging = [dictionary[@"vtc-ringing"] boolValue];
		_vtcDialing = [dictionary[@"vtc-dialing"] boolValue];
		_vtcInMezzMeeting = [dictionary[@"vtc-in-mezz-meeting"] boolValue];
		_vtcMezzDialToAddress = dictionary[@"vtc-mezz-dial-to-address"];
		_vtcCalls = dictionary[@"vtc-calls"];
	}
	return self;
}

-(NSDictionary*)dictionaryRepresentation
{
	NSMutableDictionary *dictionary = [NSMutableDictionary new];
	dictionary[@"vtc-online"] = @(_vtcOnline);
	dictionary[@"vtc-information"] = _vtcInformation;
	dictionary[@"vtc-on-call"] = @(_vtcOnCall);
	dictionary[@"vtc-ringing"] = @(_vtcRinging);
	dictionary[@"vtc-dialing"] = @(_vtcDialing);
	dictionary[@"vtc-in-mezz-meeting"] = @(_vtcInMezzMeeting);
	dictionary[@"vtc-mezz-dial-to-address"] = _vtcMezzDialToAddress;
	dictionary[@"vtc-calls"] = _vtcCalls;
	return dictionary;
}
@end


@implementation VtcInformationPsa


-(instancetype) initWithDictionary:(NSDictionary*)dictionary
{
	self = [super init];
	if (self)
	{
		_vtcSystemName = dictionary[@"vtc-system-name"];
		_vtcSipName = dictionary[@"vtc-sip-name"];
		_vtcVendor = dictionary[@"vtc-vendor"];
		_vtcModel = dictionary[@"vtc-model"];
		_vtcVersion = dictionary[@"vtc-version"];
	}
	return self;
}

-(NSDictionary*)dictionaryRepresentation
{
	NSMutableDictionary *dictionary = [NSMutableDictionary new];
	dictionary[@"vtc-system-name"] = _vtcSystemName;
	dictionary[@"vtc-sip-name"] = _vtcSipName;
	dictionary[@"vtc-vendor"] = _vtcVendor;
	dictionary[@"vtc-model"] = _vtcModel;
	dictionary[@"vtc-version"] = _vtcVersion;
	return dictionary;
}
@end


@implementation VtcCallPsa


-(instancetype) initWithDictionary:(NSDictionary*)dictionary
{
	self = [super init];
	if (self)
	{
		_callId = dictionary[@"call-id"];
		_callDirection = dictionary[@"call-direction"];
		_remoteNumber = dictionary[@"remote-number"];
		_remoteName = dictionary[@"remote-name"];
		_callProtocol = dictionary[@"call-protocol"];
		_callState = dictionary[@"call-state"];
	}
	return self;
}

-(NSDictionary*)dictionaryRepresentation
{
	NSMutableDictionary *dictionary = [NSMutableDictionary new];
	dictionary[@"call-id"] = _callId;
	dictionary[@"call-direction"] = _callDirection;
	dictionary[@"remote-number"] = _remoteNumber;
	dictionary[@"remote-name"] = _remoteName;
	dictionary[@"call-protocol"] = _callProtocol;
	dictionary[@"call-state"] = _callState;
	return dictionary;
}
@end


@implementation VtcStatusData


-(instancetype) initWithDictionary:(NSDictionary*)dictionary
{
	self = [super init];
	if (self)
	{
		_vtcOnline = [dictionary[@"vtc-online"] boolValue];
		_vtcInformation = dictionary[@"vtc-information"];
		_vtcOnCall = [dictionary[@"vtc-on-call"] boolValue];
		_vtcRinging = [dictionary[@"vtc-ringing"] boolValue];
		_vtcDialing = [dictionary[@"vtc-dialing"] boolValue];
		_vtcInMezzMeeting = [dictionary[@"vtc-in-mezz-meeting"] boolValue];
		_vtcMezzDialToAddress = dictionary[@"vtc-mezz-dial-to-address"];
		_vtcCalls = dictionary[@"vtc-calls"];
		_vtcContent = dictionary[@"vtc-content"];
		_vtcBridgeStatus = dictionary[@"vtc-bridge-status"];
	}
	return self;
}

-(NSDictionary*)dictionaryRepresentation
{
	NSMutableDictionary *dictionary = [NSMutableDictionary new];
	dictionary[@"vtc-online"] = @(_vtcOnline);
	dictionary[@"vtc-information"] = _vtcInformation;
	dictionary[@"vtc-on-call"] = @(_vtcOnCall);
	dictionary[@"vtc-ringing"] = @(_vtcRinging);
	dictionary[@"vtc-dialing"] = @(_vtcDialing);
	dictionary[@"vtc-in-mezz-meeting"] = @(_vtcInMezzMeeting);
	dictionary[@"vtc-mezz-dial-to-address"] = _vtcMezzDialToAddress;
	dictionary[@"vtc-calls"] = _vtcCalls;
	dictionary[@"vtc-content"] = _vtcContent;
	dictionary[@"vtc-bridge-status"] = _vtcBridgeStatus;
	return dictionary;
}
@end


@implementation VtcBridgeStatus


-(instancetype) initWithDictionary:(NSDictionary*)dictionary
{
	self = [super init];
	if (self)
	{
		_skypeEnabled = [dictionary[@"skype-enabled"] boolValue];
		_skypeUnavailable = [dictionary[@"skype-unavailable"] boolValue];
	}
	return self;
}

-(NSDictionary*)dictionaryRepresentation
{
	NSMutableDictionary *dictionary = [NSMutableDictionary new];
	dictionary[@"skype-enabled"] = @(_skypeEnabled);
	dictionary[@"skype-unavailable"] = @(_skypeUnavailable);
	return dictionary;
}
@end


@implementation VtcInformation


-(instancetype) initWithDictionary:(NSDictionary*)dictionary
{
	self = [super init];
	if (self)
	{
		_vtcSystemName = dictionary[@"vtc-system-name"];
		_vtcSipName = dictionary[@"vtc-sip-name"];
		_vtcVendor = dictionary[@"vtc-vendor"];
		_vtcModel = dictionary[@"vtc-model"];
		_vtcVersion = dictionary[@"vtc-version"];
	}
	return self;
}

-(NSDictionary*)dictionaryRepresentation
{
	NSMutableDictionary *dictionary = [NSMutableDictionary new];
	dictionary[@"vtc-system-name"] = _vtcSystemName;
	dictionary[@"vtc-sip-name"] = _vtcSipName;
	dictionary[@"vtc-vendor"] = _vtcVendor;
	dictionary[@"vtc-model"] = _vtcModel;
	dictionary[@"vtc-version"] = _vtcVersion;
	return dictionary;
}
@end


@implementation VtcCall


-(instancetype) initWithDictionary:(NSDictionary*)dictionary
{
	self = [super init];
	if (self)
	{
		_callId = dictionary[@"call-id"];
		_callDirection = dictionary[@"call-direction"];
		_remoteNumber = dictionary[@"remote-number"];
		_remoteName = dictionary[@"remote-name"];
		_callProtocol = dictionary[@"call-protocol"];
		_callState = dictionary[@"call-state"];
	}
	return self;
}

-(NSDictionary*)dictionaryRepresentation
{
	NSMutableDictionary *dictionary = [NSMutableDictionary new];
	dictionary[@"call-id"] = _callId;
	dictionary[@"call-direction"] = _callDirection;
	dictionary[@"remote-number"] = _remoteNumber;
	dictionary[@"remote-name"] = _remoteName;
	dictionary[@"call-protocol"] = _callProtocol;
	dictionary[@"call-state"] = _callState;
	return dictionary;
}
@end


@implementation VtcContent


-(instancetype) initWithDictionary:(NSDictionary*)dictionary
{
	self = [super init];
	if (self)
	{
		_vtcDisplayingSafeContent = [dictionary[@"vtc-displaying-safe-content"] boolValue];
		_vtcDisplayingRiskyContent = [dictionary[@"vtc-displaying-risky-content"] boolValue];
		_isSendingMezzMainScreenInVtcCall = [dictionary[@"is-sending-mezz-main-screen-in-vtc-call"] boolValue];
	}
	return self;
}

-(NSDictionary*)dictionaryRepresentation
{
	NSMutableDictionary *dictionary = [NSMutableDictionary new];
	dictionary[@"vtc-displaying-safe-content"] = @(_vtcDisplayingSafeContent);
	dictionary[@"vtc-displaying-risky-content"] = @(_vtcDisplayingRiskyContent);
	dictionary[@"is-sending-mezz-main-screen-in-vtc-call"] = @(_isSendingMezzMainScreenInVtcCall);
	return dictionary;
}
@end


@implementation CallEndInfo


-(instancetype) initWithDictionary:(NSDictionary*)dictionary
{
	self = [super init];
	if (self)
	{
		_vtcCallEndReason = dictionary[@"vtc-call-end-reason"];
		_vtcInMezzMeeting = [dictionary[@"vtc-in-mezz-meeting"] boolValue];
		_pleaseLeaveMezzMeeting = [dictionary[@"please-leave-mezz-meeting"] boolValue];
		_otherCallsCount = [dictionary[@"other-calls-count"] integerValue];
	}
	return self;
}

-(NSDictionary*)dictionaryRepresentation
{
	NSMutableDictionary *dictionary = [NSMutableDictionary new];
	dictionary[@"vtc-call-end-reason"] = _vtcCallEndReason;
	dictionary[@"vtc-in-mezz-meeting"] = @(_vtcInMezzMeeting);
	dictionary[@"please-leave-mezz-meeting"] = @(_pleaseLeaveMezzMeeting);
	dictionary[@"other-calls-count"] = @(_otherCallsCount);
	return dictionary;
}
@end

