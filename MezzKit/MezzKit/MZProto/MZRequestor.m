// Automatically generated code, do not edit.
#import "MZRequestor.h"
#import "MZProtoFactory.h"




@interface MZRequestor()
{
	MZProteinFactory *_proteinFactory;
}

@property (nonatomic, weak) MZCommunicator *communicator;

@end


@implementation MZRequestor

- (instancetype)initWithCommunicator:(MZCommunicator*)communicator
{
	if (self = [super init])
	{
		_communicator = communicator;
		_proteinFactory = [MZProteinFactory new];
		_proteinFactory.provenance = communicator.provenance;
	}
	return self;
}

-(MZTransaction*) requestAssetDownloadRequest:(NSString*)contentSource
{
	return [_communicator beginTransactionWithName:@"asset-download" proteinBlock:^{
		return [self->_proteinFactory assetDownloadRequest:contentSource];
	}];
}

-(MZTransaction*) requestAssetUploadImageReadyRequest:(NSString*)workspaceUid uid:(NSString*)uid fileId:(NSInteger)fileId fileName:(NSString*)fileName data:(NSData*)data
{
	return [_communicator beginTransactionWithName:@"asset-upload-image-ready" proteinBlock:^{
		return [self->_proteinFactory assetUploadImageReadyRequest:workspaceUid uid:uid fileId:fileId fileName:fileName data:data];
	}];
}

-(MZTransaction*) requestAssetUploadPdfReadyRequest:(NSInteger)fileId data:(NSData*)data
{
	return [_communicator beginTransactionWithName:@"asset-upload-pdf-ready" proteinBlock:^{
		return [self->_proteinFactory assetUploadPdfReadyRequest:fileId data:data];
	}];
}

-(MZTransaction*) requestAssetUploadProvisionRequest:(NSString*)workspaceUid files:(NSArray*)files
{
	return [_communicator beginTransactionWithName:@"asset-upload-provision" proteinBlock:^{
		return [self->_proteinFactory assetUploadProvisionRequest:workspaceUid files:files];
	}];
}

-(MZTransaction*) requestClientJoinRequest
{
	return [_communicator beginTransactionWithName:@"client-join" proteinBlock:^{
		return [self->_proteinFactory clientJoinRequest];
	}];
}

-(MZTransaction*) requestClientJoinRequestWithPasskey:(NSString*)passkey
{
	return [_communicator beginTransactionWithName:@"client-join" proteinBlock:^{
		return [self->_proteinFactory clientJoinRequestWithPasskey:passkey];
	}];
}

-(MZTransaction*) requestClientJoinRequestWithName:(NSString*)displayName
{
	return [_communicator beginTransactionWithName:@"client-join" proteinBlock:^{
		return [self->_proteinFactory clientJoinRequestWithName:displayName];
	}];
}

-(MZTransaction*) requestClientJoinRequestWithNameAndPasskey:(NSString*)displayName passkey:(NSString*)passkey
{
	return [_communicator beginTransactionWithName:@"client-join" proteinBlock:^{
		return [self->_proteinFactory clientJoinRequestWithNameAndPasskey:displayName passkey:passkey];
	}];
}

-(MZTransaction*) requestClientLeaveRequest
{
	return [_communicator beginTransactionWithName:@"client-leave" proteinBlock:^{
		return [self->_proteinFactory clientLeaveRequest];
	}];
}

-(MZTransaction*) requestDialogDismissRequest:(NSString*)id value:(NSString*)value
{
	return [_communicator beginTransactionWithName:@"dialog-dismiss" proteinBlock:^{
		return [self->_proteinFactory dialogDismissRequest:id value:value];
	}];
}

-(MZTransaction*) requestHeartbeatRequest:(BOOL)joined
{
	return [_communicator beginTransactionWithName:@"heartbeat" proteinBlock:^{
		return [self->_proteinFactory heartbeatRequest:joined];
	}];
}

-(MZTransaction*) requestInfopresenceDetailRequest
{
	return [_communicator beginTransactionWithName:@"infopresence-detail" proteinBlock:^{
		return [self->_proteinFactory infopresenceDetailRequest];
	}];
}

-(MZTransaction*) requestInfopresenceIncomingResponse:(NSString*)uid accept:(BOOL)accept
{
	return [_communicator beginTransactionWithName:@"infopresence-incoming-request-resolve" proteinBlock:^{
		return [self->_proteinFactory infopresenceIncomingResponse:uid accept:accept];
	}];
}

-(MZTransaction*) requestInfopresenceLeaveRequest:(BOOL)interrupted
{
	return [_communicator beginTransactionWithName:@"infopresence-leave" proteinBlock:^{
		return [self->_proteinFactory infopresenceLeaveRequest:interrupted];
	}];
}

-(MZTransaction*) requestInfopresenceOutgoingCancelRequest
{
	return [_communicator beginTransactionWithName:@"infopresence-outgoing-request-cancel" proteinBlock:^{
		return [self->_proteinFactory infopresenceOutgoingCancelRequest];
	}];
}

-(MZTransaction*) requestInfopresenceOutgoingRequest:(NSString*)uid sentUtc:(NSInteger)sentUtc
{
	return [_communicator beginTransactionWithName:@"infopresence-outgoing-request" proteinBlock:^{
		return [self->_proteinFactory infopresenceOutgoingRequest:uid sentUtc:sentUtc];
	}];
}

-(MZTransaction*) requestInfopresenceOutgoingInviteRequest:(NSString*)uid
{
	return [_communicator beginTransactionWithName:@"infopresence-outgoing-invite" proteinBlock:^{
		return [self->_proteinFactory infopresenceOutgoingInviteRequest:uid];
	}];
}

-(MZTransaction*) requestInfopresenceOutgoingInviteCancelRequest:(NSString*)uid
{
	return [_communicator beginTransactionWithName:@"infopresence-outgoing-invite-cancel" proteinBlock:^{
		return [self->_proteinFactory infopresenceOutgoingInviteCancelRequest:uid];
	}];
}

-(MZTransaction*) requestInfopresenceIncomingInviteResolveRequest:(NSString*)uid accept:(BOOL)accept
{
	return [_communicator beginTransactionWithName:@"infopresence-incoming-invite-resolve" proteinBlock:^{
		return [self->_proteinFactory infopresenceIncomingInviteResolveRequest:uid accept:accept];
	}];
}

-(MZTransaction*) requestLiveStreamCreate:(NSString*)uri id:(NSString*)id
{
	return [_communicator beginTransactionWithName:@"live-stream-create" proteinBlock:^{
		return [self->_proteinFactory liveStreamCreate:uri id:id];
	}];
}

-(MZTransaction*) requestLiveStreamDelete:(NSString*)contentSource
{
	return [_communicator beginTransactionWithName:@"live-stream-delete" proteinBlock:^{
		return [self->_proteinFactory liveStreamDelete:contentSource];
	}];
}

-(MZTransaction*) requestLiveStreamDetailRequest
{
	return [_communicator beginTransactionWithName:@"live-streams-detail" proteinBlock:^{
		return [self->_proteinFactory liveStreamDetailRequest];
	}];
}

-(MZTransaction*) requestLiveStreamGrabRequest:(NSString*)workspaceUid uid:(NSString*)uid
{
	return [_communicator beginTransactionWithName:@"live-stream-grab" proteinBlock:^{
		return [self->_proteinFactory liveStreamGrabRequest:workspaceUid uid:uid];
	}];
}

-(MZTransaction*) requestLiveStreamRelinquishRequest:(NSString*)workspaceUid uid:(NSString*)uid
{
	return [_communicator beginTransactionWithName:@"live-stream-relinquish" proteinBlock:^{
		return [self->_proteinFactory liveStreamRelinquishRequest:workspaceUid uid:uid];
	}];
}

-(MZTransaction*) requestClientSignInRequest:(NSString*)username password:(NSString*)password
{
	return [_communicator beginTransactionWithName:@"client-sign-in" proteinBlock:^{
		return [self->_proteinFactory clientSignInRequest:username password:password];
	}];
}

-(MZTransaction*) requestClientSignOutRequest
{
	return [_communicator beginTransactionWithName:@"client-sign-out" proteinBlock:^{
		return [self->_proteinFactory clientSignOutRequest];
	}];
}

-(MZTransaction*) requestMeetingEndRequest:(NSString*)meetingUid
{
	return [_communicator beginTransactionWithName:@"meeting-end" proteinBlock:^{
		return [self->_proteinFactory meetingEndRequest:meetingUid];
	}];
}

-(MZTransaction*) requestMeetingJoinUrlRequest:(NSString*)mezzinUrl isForMagicM2m:(BOOL)isForMagicM2m
{
	return [_communicator beginTransactionWithName:@"meeting-join-url" proteinBlock:^{
		return [self->_proteinFactory meetingJoinUrlRequest:mezzinUrl isForMagicM2m:isForMagicM2m];
	}];
}

-(MZTransaction*) requestMeetingListRequest
{
	return [_communicator beginTransactionWithName:@"meeting-list" proteinBlock:^{
		return [self->_proteinFactory meetingListRequest];
	}];
}

-(MZTransaction*) requestMeetingStartRequest:(NSString*)meetingUid isForMagicM2m:(BOOL)isForMagicM2m
{
	return [_communicator beginTransactionWithName:@"meeting-start" proteinBlock:^{
		return [self->_proteinFactory meetingStartRequest:meetingUid isForMagicM2m:isForMagicM2m];
	}];
}

-(MZTransaction*) requestMezCapsRequest
{
	return [_communicator beginTransactionWithName:@"mez-caps" proteinBlock:^{
		return [self->_proteinFactory mezCapsRequest];
	}];
}

-(MZTransaction*) requestMezzMetadataRequest
{
	return [_communicator beginTransactionWithName:@"mezz-metadata" proteinBlock:^{
		return [self->_proteinFactory mezzMetadataRequest];
	}];
}

-(MZTransaction*) requestOutgoingInfopresenceSessionRequest
{
	return [_communicator beginTransactionWithName:@"outgoing-infopresence-session-request" proteinBlock:^{
		return [self->_proteinFactory outgoingInfopresenceSessionRequest];
	}];
}

-(MZTransaction*) requestPasskeyDetailRequest
{
	return [_communicator beginTransactionWithName:@"passkey-detail" proteinBlock:^{
		return [self->_proteinFactory passkeyDetailRequest];
	}];
}

-(MZTransaction*) requestPasskeyDisableRequest
{
	return [_communicator beginTransactionWithName:@"passkey-disable" proteinBlock:^{
		return [self->_proteinFactory passkeyDisableRequest];
	}];
}

-(MZTransaction*) requestPasskeyEnableRequest
{
	return [_communicator beginTransactionWithName:@"passkey-enable" proteinBlock:^{
		return [self->_proteinFactory passkeyEnableRequest];
	}];
}

-(MZTransaction*) requestPortfolioDetailRequest:(NSString*)workspaceUid
{
	return [_communicator beginTransactionWithName:@"portfolio-detail" proteinBlock:^{
		return [self->_proteinFactory portfolioDetailRequest:workspaceUid];
	}];
}

-(MZTransaction*) requestPortfolioDownloadRequest:(NSString*)workspaceUid
{
	return [_communicator beginTransactionWithName:@"portfolio-download" proteinBlock:^{
		return [self->_proteinFactory portfolioDownloadRequest:workspaceUid];
	}];
}

-(MZTransaction*) requestPortfolioItemDeleteRequest:(NSString*)workspaceUid uid:(NSString*)uid
{
	return [_communicator beginTransactionWithName:@"portfolio-item-delete" proteinBlock:^{
		return [self->_proteinFactory portfolioItemDeleteRequest:workspaceUid uid:uid];
	}];
}

-(MZTransaction*) requestPortfolioClearRequest:(NSString*)workspaceUid
{
	return [_communicator beginTransactionWithName:@"portfolio-clear" proteinBlock:^{
		return [self->_proteinFactory portfolioClearRequest:workspaceUid];
	}];
}

-(MZTransaction*) requestPortfolioItemGrabRequest:(NSString*)workspaceUid uid:(NSString*)uid
{
	return [_communicator beginTransactionWithName:@"portfolio-item-grab" proteinBlock:^{
		return [self->_proteinFactory portfolioItemGrabRequest:workspaceUid uid:uid];
	}];
}

-(MZTransaction*) requestPortfolioItemInsertRequest:(NSString*)workspaceUid contentSource:(NSString*)contentSource index:(NSInteger)index
{
	return [_communicator beginTransactionWithName:@"portfolio-item-insert" proteinBlock:^{
		return [self->_proteinFactory portfolioItemInsertRequest:workspaceUid contentSource:contentSource index:index];
	}];
}

-(MZTransaction*) requestPortfolioItemRelinquishRequest:(NSString*)workspaceUid uid:(NSString*)uid
{
	return [_communicator beginTransactionWithName:@"portfolio-item-relinquish" proteinBlock:^{
		return [self->_proteinFactory portfolioItemRelinquishRequest:workspaceUid uid:uid];
	}];
}

-(MZTransaction*) requestPortfolioItemReorderRequest:(NSString*)workspaceUid uid:(NSString*)uid index:(NSInteger)index
{
	return [_communicator beginTransactionWithName:@"portfolio-item-reorder" proteinBlock:^{
		return [self->_proteinFactory portfolioItemReorderRequest:workspaceUid uid:uid index:index];
	}];
}

-(MZTransaction*) requestPresentationScrollRequest:(NSString*)workspaceUid currentIndex:(NSInteger)currentIndex
{
	return [_communicator beginTransactionWithName:@"presentation-scroll" proteinBlock:^{
		return [self->_proteinFactory presentationScrollRequest:workspaceUid currentIndex:currentIndex];
	}];
}

-(MZTransaction*) requestPresentationStartRequest:(NSString*)workspaceUid currentIndex:(NSInteger)currentIndex
{
	return [_communicator beginTransactionWithName:@"presentation-start" proteinBlock:^{
		return [self->_proteinFactory presentationStartRequest:workspaceUid currentIndex:currentIndex];
	}];
}

-(MZTransaction*) requestPresentationStopRequest:(NSString*)workspaceUid
{
	return [_communicator beginTransactionWithName:@"presentation-stop" proteinBlock:^{
		return [self->_proteinFactory presentationStopRequest:workspaceUid];
	}];
}

-(MZTransaction*) requestProfileDetailRequest
{
	return [_communicator beginTransactionWithName:@"profile-detail" proteinBlock:^{
		return [self->_proteinFactory profileDetailRequest];
	}];
}

-(MZTransaction*) requestRatchetRequest:(NSString*)ratchetState
{
	return [_communicator beginTransactionWithName:@"ratchet" proteinBlock:^{
		return [self->_proteinFactory ratchetRequest:ratchetState];
	}];
}

-(MZTransaction*) requestInfopresenceEnableRemoteAccessRequest
{
	return [_communicator beginTransactionWithName:@"infopresence-enable-remote-access" proteinBlock:^{
		return [self->_proteinFactory infopresenceEnableRemoteAccessRequest];
	}];
}

-(MZTransaction*) requestInfopresenceDisableRemoteAccessRequest
{
	return [_communicator beginTransactionWithName:@"infopresence-disable-remote-access" proteinBlock:^{
		return [self->_proteinFactory infopresenceDisableRemoteAccessRequest];
	}];
}

-(MZTransaction*) requestPassforwardRequest:(NSString*)spaceCntx curAim:(MZVect*)curAim curOrigin:(MZVect*)curOrigin pressureReports:(PressureReport*)pressureReports
{
	return [_communicator beginTransactionWithName:@"passforward" proteinBlock:^{
		return [self->_proteinFactory passforwardRequest:spaceCntx curAim:curAim curOrigin:curOrigin pressureReports:pressureReports];
	}];
}

-(MZTransaction*) requestRemoteMezzaninesDetailRequest
{
	return [_communicator beginTransactionWithName:@"remote-mezzanines-detail" proteinBlock:^{
		return [self->_proteinFactory remoteMezzaninesDetailRequest];
	}];
}

-(MZTransaction*) requestStorageDetailRequest
{
	return [_communicator beginTransactionWithName:@"storage-detail" proteinBlock:^{
		return [self->_proteinFactory storageDetailRequest];
	}];
}

-(MZTransaction*) requestWhiteboardCaptureRequest:(NSString*)uid workspaceUid:(NSString*)workspaceUid
{
	return [_communicator beginTransactionWithName:@"whiteboard-capture" proteinBlock:^{
		return [self->_proteinFactory whiteboardCaptureRequest:uid workspaceUid:workspaceUid];
	}];
}

-(MZTransaction*) requestWindshieldArrangeRequest:(NSString*)workspaceUid surfaces:(NSArray*)surfaces
{
	return [_communicator beginTransactionWithName:@"windshield-arrange" proteinBlock:^{
		return [self->_proteinFactory windshieldArrangeRequest:workspaceUid surfaces:surfaces];
	}];
}

-(MZTransaction*) requestWindshieldClearRequest:(NSString*)workspaceUid surfaces:(NSArray*)surfaces
{
	return [_communicator beginTransactionWithName:@"windshield-clear" proteinBlock:^{
		return [self->_proteinFactory windshieldClearRequest:workspaceUid surfaces:surfaces];
	}];
}

-(MZTransaction*) requestWindshieldDetailRequest:(NSString*)workspaceUid
{
	return [_communicator beginTransactionWithName:@"windshield-detail" proteinBlock:^{
		return [self->_proteinFactory windshieldDetailRequest:workspaceUid];
	}];
}

-(MZTransaction*) requestWindshieldItemCreateRequest:(NSString*)workspaceUid contentSource:(NSString*)contentSource surfaceName:(NSString*)surfaceName feldRelativeCoords:(FeldRelativeCoords*)feldRelativeCoords feldRelativeSize:(FeldRelativeSize*)feldRelativeSize
{
	return [_communicator beginTransactionWithName:@"windshield-item-create" proteinBlock:^{
		return [self->_proteinFactory windshieldItemCreateRequest:workspaceUid contentSource:contentSource surfaceName:surfaceName feldRelativeCoords:feldRelativeCoords feldRelativeSize:feldRelativeSize];
	}];
}

-(MZTransaction*) requestWindshieldItemDeleteRequest:(NSString*)workspaceUid uid:(NSString*)uid
{
	return [_communicator beginTransactionWithName:@"windshield-item-delete" proteinBlock:^{
		return [self->_proteinFactory windshieldItemDeleteRequest:workspaceUid uid:uid];
	}];
}

-(MZTransaction*) requestWindshieldItemGrabRequest:(NSString*)workspaceUid uid:(NSString*)uid
{
	return [_communicator beginTransactionWithName:@"windshield-item-grab" proteinBlock:^{
		return [self->_proteinFactory windshieldItemGrabRequest:workspaceUid uid:uid];
	}];
}

-(MZTransaction*) requestWindshieldItemTransformRequest:(NSString*)workspaceUid uid:(NSString*)uid surfaceName:(NSString*)surfaceName feldRelativeCoords:(FeldRelativeCoords*)feldRelativeCoords feldRelativeSize:(FeldRelativeSize*)feldRelativeSize
{
	return [_communicator beginTransactionWithName:@"windshield-item-transform" proteinBlock:^{
		return [self->_proteinFactory windshieldItemTransformRequest:workspaceUid uid:uid surfaceName:surfaceName feldRelativeCoords:feldRelativeCoords feldRelativeSize:feldRelativeSize];
	}];
}

-(MZTransaction*) requestWindshieldItemRelinquishRequest:(NSString*)workspaceUid uid:(NSString*)uid
{
	return [_communicator beginTransactionWithName:@"windshield-item-relinquish" proteinBlock:^{
		return [self->_proteinFactory windshieldItemRelinquishRequest:workspaceUid uid:uid];
	}];
}

-(MZTransaction*) requestWorkspaceCloseRequest:(NSString*)uid
{
	return [_communicator beginTransactionWithName:@"workspace-close" proteinBlock:^{
		return [self->_proteinFactory workspaceCloseRequest:uid];
	}];
}

-(MZTransaction*) requestWorkspaceCreateRequest:(NSString*)name owners:(NSArray*)owners
{
	return [_communicator beginTransactionWithName:@"workspace-create" proteinBlock:^{
		return [self->_proteinFactory workspaceCreateRequest:name owners:owners];
	}];
}

-(MZTransaction*) requestWorkspaceDeleteRequest:(NSString*)uid
{
	return [_communicator beginTransactionWithName:@"workspace-delete" proteinBlock:^{
		return [self->_proteinFactory workspaceDeleteRequest:uid];
	}];
}

-(MZTransaction*) requestWorkspaceDiscardRequest:(NSString*)uid
{
	return [_communicator beginTransactionWithName:@"workspace-discard" proteinBlock:^{
		return [self->_proteinFactory workspaceDiscardRequest:uid];
	}];
}

-(MZTransaction*) requestWorkspaceDownloadRequest:(NSString*)uid
{
	return [_communicator beginTransactionWithName:@"workspace-download" proteinBlock:^{
		return [self->_proteinFactory workspaceDownloadRequest:uid];
	}];
}

-(MZTransaction*) requestWorkspaceDuplicateRequest:(NSString*)uid owners:(NSArray*)owners name:(NSString*)name
{
	return [_communicator beginTransactionWithName:@"workspace-duplicate" proteinBlock:^{
		return [self->_proteinFactory workspaceDuplicateRequest:uid owners:owners name:name];
	}];
}

-(MZTransaction*) requestWorkspaceListRequest
{
	return [_communicator beginTransactionWithName:@"workspace-list" proteinBlock:^{
		return [self->_proteinFactory workspaceListRequest];
	}];
}

-(MZTransaction*) requestWorkspaceOpenRequest:(NSString*)switchFromUid switchToUid:(NSString*)switchToUid
{
	return [_communicator beginTransactionWithName:@"workspace-open" proteinBlock:^{
		return [self->_proteinFactory workspaceOpenRequest:switchFromUid switchToUid:switchToUid];
	}];
}

-(MZTransaction*) requestWorkspaceRenameRequest:(NSString*)uid name:(NSString*)name
{
	return [_communicator beginTransactionWithName:@"workspace-rename" proteinBlock:^{
		return [self->_proteinFactory workspaceRenameRequest:uid name:name];
	}];
}

-(MZTransaction*) requestWorkspaceSaveRequest:(NSString*)uid owners:(NSArray*)owners name:(NSString*)name
{
	return [_communicator beginTransactionWithName:@"workspace-save" proteinBlock:^{
		return [self->_proteinFactory workspaceSaveRequest:uid owners:owners name:name];
	}];
}

-(MZTransaction*) requestWorkspaceUploadRequest:(NSArray*)owners
{
	return [_communicator beginTransactionWithName:@"workspace-upload" proteinBlock:^{
		return [self->_proteinFactory workspaceUploadRequest:owners];
	}];
}

-(MZTransaction*) requestWorkspaceUploadDoneRequest:(NSString*)uid owners:(NSArray*)owners fileName:(NSString*)fileName uploadUrl:(NSString*)uploadUrl
{
	return [_communicator beginTransactionWithName:@"workspace-upload-done" proteinBlock:^{
		return [self->_proteinFactory workspaceUploadDoneRequest:uid owners:owners fileName:fileName uploadUrl:uploadUrl];
	}];
}

-(MZTransaction*) requestRequestVtcStatus
{
	return [_communicator beginTransactionWithName:@"request-vtc-status" proteinBlock:^{
		return [self->_proteinFactory requestVtcStatus];
	}];
}

-(MZTransaction*) requestMeetingWorkspaceCleanupRequest
{
	return [_communicator beginTransactionWithName:@"meeting-workspace-cleanup" proteinBlock:^{
		return [self->_proteinFactory meetingWorkspaceCleanupRequest];
	}];
}

-(MZTransaction*) requestParticipantListRequest
{
	return [_communicator beginTransactionWithName:@"participant-list" proteinBlock:^{
		return [self->_proteinFactory participantListRequest];
	}];
}

@end
