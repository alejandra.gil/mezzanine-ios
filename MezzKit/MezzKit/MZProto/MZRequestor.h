// Automatically generated code, do not edit.

#import <Foundation/Foundation.h>
#import "MZCommunicator.h"
#import "MZSlaws.h"


/// Compatible with client api versions: 3.6, 4.0
@interface MZRequestor : NSObject

- (instancetype)initWithCommunicator:(MZCommunicator*)communicator;

-(MZTransaction*) requestAssetDownloadRequest:(NSString*)contentSource;

-(MZTransaction*) requestAssetUploadImageReadyRequest:(NSString*)workspaceUid uid:(NSString*)uid fileId:(NSInteger)fileId fileName:(NSString*)fileName data:(NSData*)data;

-(MZTransaction*) requestAssetUploadPdfReadyRequest:(NSInteger)fileId data:(NSData*)data;

-(MZTransaction*) requestAssetUploadProvisionRequest:(NSString*)workspaceUid files:(NSArray*)files;

-(MZTransaction*) requestClientJoinRequest;

-(MZTransaction*) requestClientJoinRequestWithPasskey:(NSString*)passkey;

-(MZTransaction*) requestClientJoinRequestWithName:(NSString*)displayName;

-(MZTransaction*) requestClientJoinRequestWithNameAndPasskey:(NSString*)displayName passkey:(NSString*)passkey;

-(MZTransaction*) requestClientLeaveRequest;

-(MZTransaction*) requestDialogDismissRequest:(NSString*)id value:(NSString*)value;

-(MZTransaction*) requestHeartbeatRequest:(BOOL)joined;

-(MZTransaction*) requestInfopresenceDetailRequest;

-(MZTransaction*) requestInfopresenceIncomingResponse:(NSString*)uid accept:(BOOL)accept;

-(MZTransaction*) requestInfopresenceLeaveRequest:(BOOL)interrupted;

-(MZTransaction*) requestInfopresenceOutgoingCancelRequest;

-(MZTransaction*) requestInfopresenceOutgoingRequest:(NSString*)uid sentUtc:(NSInteger)sentUtc;

-(MZTransaction*) requestInfopresenceOutgoingInviteRequest:(NSString*)uid;

-(MZTransaction*) requestInfopresenceOutgoingInviteCancelRequest:(NSString*)uid;

-(MZTransaction*) requestInfopresenceIncomingInviteResolveRequest:(NSString*)uid accept:(BOOL)accept;

-(MZTransaction*) requestLiveStreamCreate:(NSString*)uri id:(NSString*)id;

-(MZTransaction*) requestLiveStreamDelete:(NSString*)contentSource;

-(MZTransaction*) requestLiveStreamDetailRequest;

-(MZTransaction*) requestLiveStreamGrabRequest:(NSString*)workspaceUid uid:(NSString*)uid;

-(MZTransaction*) requestLiveStreamRelinquishRequest:(NSString*)workspaceUid uid:(NSString*)uid;

-(MZTransaction*) requestClientSignInRequest:(NSString*)username password:(NSString*)password;

-(MZTransaction*) requestClientSignOutRequest;

-(MZTransaction*) requestMeetingEndRequest:(NSString*)meetingUid;

-(MZTransaction*) requestMeetingJoinUrlRequest:(NSString*)mezzinUrl isForMagicM2m:(BOOL)isForMagicM2m;

-(MZTransaction*) requestMeetingListRequest;

-(MZTransaction*) requestMeetingStartRequest:(NSString*)meetingUid isForMagicM2m:(BOOL)isForMagicM2m;

-(MZTransaction*) requestMezCapsRequest;

-(MZTransaction*) requestMezzMetadataRequest;

-(MZTransaction*) requestOutgoingInfopresenceSessionRequest;

-(MZTransaction*) requestPasskeyDetailRequest;

-(MZTransaction*) requestPasskeyDisableRequest;

-(MZTransaction*) requestPasskeyEnableRequest;

-(MZTransaction*) requestPortfolioDetailRequest:(NSString*)workspaceUid;

-(MZTransaction*) requestPortfolioDownloadRequest:(NSString*)workspaceUid;

-(MZTransaction*) requestPortfolioItemDeleteRequest:(NSString*)workspaceUid uid:(NSString*)uid;

-(MZTransaction*) requestPortfolioClearRequest:(NSString*)workspaceUid;

-(MZTransaction*) requestPortfolioItemGrabRequest:(NSString*)workspaceUid uid:(NSString*)uid;

-(MZTransaction*) requestPortfolioItemInsertRequest:(NSString*)workspaceUid contentSource:(NSString*)contentSource index:(NSInteger)index;

-(MZTransaction*) requestPortfolioItemRelinquishRequest:(NSString*)workspaceUid uid:(NSString*)uid;

-(MZTransaction*) requestPortfolioItemReorderRequest:(NSString*)workspaceUid uid:(NSString*)uid index:(NSInteger)index;

-(MZTransaction*) requestPresentationScrollRequest:(NSString*)workspaceUid currentIndex:(NSInteger)currentIndex;

-(MZTransaction*) requestPresentationStartRequest:(NSString*)workspaceUid currentIndex:(NSInteger)currentIndex;

-(MZTransaction*) requestPresentationStopRequest:(NSString*)workspaceUid;

-(MZTransaction*) requestProfileDetailRequest;

-(MZTransaction*) requestRatchetRequest:(NSString*)ratchetState;

-(MZTransaction*) requestInfopresenceEnableRemoteAccessRequest;

-(MZTransaction*) requestInfopresenceDisableRemoteAccessRequest;

-(MZTransaction*) requestPassforwardRequest:(NSString*)spaceCntx curAim:(MZVect*)curAim curOrigin:(MZVect*)curOrigin pressureReports:(PressureReport*)pressureReports;

-(MZTransaction*) requestRemoteMezzaninesDetailRequest;

-(MZTransaction*) requestStorageDetailRequest;

-(MZTransaction*) requestWhiteboardCaptureRequest:(NSString*)uid workspaceUid:(NSString*)workspaceUid;

-(MZTransaction*) requestWindshieldArrangeRequest:(NSString*)workspaceUid surfaces:(NSArray*)surfaces;

-(MZTransaction*) requestWindshieldClearRequest:(NSString*)workspaceUid surfaces:(NSArray*)surfaces;

-(MZTransaction*) requestWindshieldDetailRequest:(NSString*)workspaceUid;

-(MZTransaction*) requestWindshieldItemCreateRequest:(NSString*)workspaceUid contentSource:(NSString*)contentSource surfaceName:(NSString*)surfaceName feldRelativeCoords:(FeldRelativeCoords*)feldRelativeCoords feldRelativeSize:(FeldRelativeSize*)feldRelativeSize;

-(MZTransaction*) requestWindshieldItemDeleteRequest:(NSString*)workspaceUid uid:(NSString*)uid;

-(MZTransaction*) requestWindshieldItemGrabRequest:(NSString*)workspaceUid uid:(NSString*)uid;

-(MZTransaction*) requestWindshieldItemTransformRequest:(NSString*)workspaceUid uid:(NSString*)uid surfaceName:(NSString*)surfaceName feldRelativeCoords:(FeldRelativeCoords*)feldRelativeCoords feldRelativeSize:(FeldRelativeSize*)feldRelativeSize;

-(MZTransaction*) requestWindshieldItemRelinquishRequest:(NSString*)workspaceUid uid:(NSString*)uid;

-(MZTransaction*) requestWorkspaceCloseRequest:(NSString*)uid;

-(MZTransaction*) requestWorkspaceCreateRequest:(NSString*)name owners:(NSArray*)owners;

-(MZTransaction*) requestWorkspaceDeleteRequest:(NSString*)uid;

-(MZTransaction*) requestWorkspaceDiscardRequest:(NSString*)uid;

-(MZTransaction*) requestWorkspaceDownloadRequest:(NSString*)uid;

-(MZTransaction*) requestWorkspaceDuplicateRequest:(NSString*)uid owners:(NSArray*)owners name:(NSString*)name;

-(MZTransaction*) requestWorkspaceListRequest;

-(MZTransaction*) requestWorkspaceOpenRequest:(NSString*)switchFromUid switchToUid:(NSString*)switchToUid;

-(MZTransaction*) requestWorkspaceRenameRequest:(NSString*)uid name:(NSString*)name;

-(MZTransaction*) requestWorkspaceSaveRequest:(NSString*)uid owners:(NSArray*)owners name:(NSString*)name;

-(MZTransaction*) requestWorkspaceUploadRequest:(NSArray*)owners;

-(MZTransaction*) requestWorkspaceUploadDoneRequest:(NSString*)uid owners:(NSArray*)owners fileName:(NSString*)fileName uploadUrl:(NSString*)uploadUrl;

-(MZTransaction*) requestRequestVtcStatus;

-(MZTransaction*) requestMeetingWorkspaceCleanupRequest;

-(MZTransaction*) requestParticipantListRequest;


@end
