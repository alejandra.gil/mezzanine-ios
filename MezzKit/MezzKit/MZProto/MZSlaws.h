// Automatically generated code, do not edit.

#import <Foundation/Foundation.h>
#import "MZVect.h"


@class Pat;
@class Vec2;
@class Vec2i;
@class Vec3;
@class Vec4;
@class FeatureToggles;
@class Feld;
@class FeldRelativeCoords;
@class FeldRelativeSize;
@class FileRequest;
@class FileResponse;
@class InfopresenceCall;
@class Whiteboard;
@class AssetUploadLimits;
@class PresentationState;
@class WorkspaceStateClosed;
@class WorkspaceState;
@class Surface;
@class ImageSpec;
@class WorkspaceItem;
@class WindshieldItem;
@class PortfolioItem;
@class LivestreamItem;
@class MezzDetail;
@class MezzMetadata;
@class RemoteParticipant;
@class Handi;
@class PressureReport;
@class PexipDetail;
@class VideoChat;
@class CalendarAttendee;
@class CalendarMeeting;
@class DialogItem;
@class Button;
@class Certificate;
@class SessionState;
@class RoomParticipant;
@class Participant;
@class VtcStatusDataPsa;
@class VtcInformationPsa;
@class VtcCallPsa;
@class VtcStatusData;
@class VtcBridgeStatus;
@class VtcInformation;
@class VtcCall;
@class VtcContent;
@class CallEndInfo;


@interface Pat : NSObject

@property (nonatomic, copy) NSString* provenance;
@property (nonatomic, assign) NSInteger transactionId;

-(instancetype) initWithArray:(NSArray*)array;
-(NSArray*)arrayRepresentation;

@end


@interface Vec2 : NSObject

@property (nonatomic, assign) double x;
@property (nonatomic, assign) double y;

-(instancetype) initWithArray:(NSArray*)array;
-(NSArray*)arrayRepresentation;

@end


@interface Vec2i : NSObject

@property (nonatomic, assign) NSInteger x;
@property (nonatomic, assign) NSInteger y;

-(instancetype) initWithArray:(NSArray*)array;
-(NSArray*)arrayRepresentation;

@end


@interface Vec3 : NSObject

@property (nonatomic, assign) double x;
@property (nonatomic, assign) double y;
@property (nonatomic, assign) double z;

-(instancetype) initWithArray:(NSArray*)array;
-(NSArray*)arrayRepresentation;

@end


@interface Vec4 : NSObject

@property (nonatomic, assign) double x;
@property (nonatomic, assign) double y;
@property (nonatomic, assign) double z;
@property (nonatomic, assign) double w;

-(instancetype) initWithArray:(NSArray*)array;
-(NSArray*)arrayRepresentation;

@end


@interface FeatureToggles : NSObject

@property (nonatomic, assign) BOOL remoteAccess;
@property (nonatomic, assign) BOOL m2mInvites;
@property (nonatomic, assign) BOOL wand2;
@property (nonatomic, assign) BOOL disableDownloads;
@property (nonatomic, assign) BOOL samlAuth;
@property (nonatomic, assign) BOOL diskEncryption;
@property (nonatomic, assign) BOOL logForwarding;
@property (nonatomic, assign) BOOL watchdog;
@property (nonatomic, assign) BOOL webTeamworkUi;
@property (nonatomic, assign) BOOL accessWarning;
@property (nonatomic, assign) BOOL webthing;
@property (nonatomic, assign) BOOL oblongMesaRadeon;
@property (nonatomic, assign) BOOL s4b;
@property (nonatomic, assign) BOOL enableHsts;
@property (nonatomic, assign) BOOL enableCsrf;
@property (nonatomic, assign) BOOL thirdPartyCerts;
@property (nonatomic, assign) BOOL adminApi;
@property (nonatomic, assign) BOOL vtcAutoplay;
@property (nonatomic, assign) BOOL rtspB2b;
@property (nonatomic, assign) BOOL advancedPip;
@property (nonatomic, assign) BOOL setupWizard;
@property (nonatomic, assign) BOOL obConman;
@property (nonatomic, assign) BOOL obConmanExperimental;
@property (nonatomic, assign) BOOL participantRoster;
@property (nonatomic, assign) BOOL roomKey;
@property (nonatomic, assign) BOOL privacyKey;

-(instancetype) initWithDictionary:(NSDictionary*)dictionary;
-(NSDictionary*) dictionaryRepresentation;

@end


@interface Feld : NSObject

@property (nonatomic, copy) NSString* id;
@property (nonatomic, copy) NSString* type;
@property (nonatomic, strong) MZVect* cent;
@property (nonatomic, strong) MZVect* up;
@property (nonatomic, strong) MZVect* over;
@property (nonatomic, strong) MZVect* norm;
@property (nonatomic, strong) MZVect* physSize;
@property (nonatomic, strong) MZVect* pxSize;
@property (nonatomic, assign) double viewDist;
@property (nonatomic, assign) BOOL visibleToAll;
@property (nonatomic, copy) NSString* visibility;

-(instancetype) initWithDictionary:(NSDictionary*)dictionary;
-(NSDictionary*) dictionaryRepresentation;

@end


@interface FeldRelativeCoords : NSObject

@property (nonatomic, copy) NSString* feldId;
@property (nonatomic, assign) double over;
@property (nonatomic, assign) double up;
@property (nonatomic, assign) double norm;
@property (nonatomic, assign) BOOL valid;

-(instancetype) initWithDictionary:(NSDictionary*)dictionary;
-(NSDictionary*) dictionaryRepresentation;

@end


@interface FeldRelativeSize : NSObject

@property (nonatomic, copy) NSString* feldId;
@property (nonatomic, assign) double scale;
@property (nonatomic, assign) double aspectRatio;

-(instancetype) initWithDictionary:(NSDictionary*)dictionary;
-(NSDictionary*) dictionaryRepresentation;

@end


@interface FileRequest : NSObject

@property (nonatomic, assign) NSInteger fileId;
@property (nonatomic, copy) NSString* fileName;
@property (nonatomic, copy) NSString* type;

-(instancetype) initWithDictionary:(NSDictionary*)dictionary;
-(NSDictionary*) dictionaryRepresentation;

@end


@interface FileResponse : NSObject

@property (nonatomic, assign) NSInteger fileId;
@property (nonatomic, copy) NSString* fileName;
@property (nonatomic, copy) NSString* type;
@property (nonatomic, strong) NSArray* uids;		// contains items of type NSString*
@property (nonatomic, assign) NSInteger pages;

-(instancetype) initWithDictionary:(NSDictionary*)dictionary;
-(NSDictionary*) dictionaryRepresentation;

@end


@interface InfopresenceCall : NSObject

@property (nonatomic, copy) NSString* uid;
@property (nonatomic, copy) NSString* type;
@property (nonatomic, assign) double sentUtc;
@property (nonatomic, assign) double receivedUtc;

-(instancetype) initWithDictionary:(NSDictionary*)dictionary;
-(NSDictionary*) dictionaryRepresentation;

@end


@interface Whiteboard : NSObject

@property (nonatomic, copy) NSString* uid;
@property (nonatomic, copy) NSString* name;

-(instancetype) initWithDictionary:(NSDictionary*)dictionary;
-(NSDictionary*) dictionaryRepresentation;

@end


@interface AssetUploadLimits : NSObject

@property (nonatomic, assign) NSInteger uploadMaxHeight;
@property (nonatomic, assign) NSInteger uploadMaxImageSizeMb;
@property (nonatomic, assign) NSInteger uploadMaxPdfSizeMb;
@property (nonatomic, assign) NSInteger uploadMaxSizeMb;
@property (nonatomic, assign) NSInteger uploadMaxWidth;

-(instancetype) initWithDictionary:(NSDictionary*)dictionary;
-(NSDictionary*) dictionaryRepresentation;

@end


@interface PresentationState : NSObject

@property (nonatomic, assign) BOOL active;
@property (nonatomic, assign) NSInteger currentIndex;

-(instancetype) initWithDictionary:(NSDictionary*)dictionary;
-(NSDictionary*) dictionaryRepresentation;

@end


@interface WorkspaceStateClosed : NSObject

@property (nonatomic, copy) NSString* uid;
@property (nonatomic, assign) BOOL destroyed;

-(instancetype) initWithDictionary:(NSDictionary*)dictionary;
-(NSDictionary*) dictionaryRepresentation;

@end


@interface WorkspaceState : NSObject

@property (nonatomic, copy) NSString* uid;
@property (nonatomic, copy) NSString* name;
@property (nonatomic, copy) NSString* modDate;
@property (nonatomic, assign) NSInteger modDateUtc;
@property (nonatomic, assign) BOOL opening;
@property (nonatomic, assign) BOOL public;
@property (nonatomic, assign) BOOL saved;
@property (nonatomic, copy) NSString* thumbUri;
@property (nonatomic, strong) PresentationState* presentation;
@property (nonatomic, strong) NSArray* liveStreams;		// contains items of type LivestreamItem*
@property (nonatomic, strong) NSArray* portfolioItems;		// contains items of type PortfolioItem*
@property (nonatomic, strong) NSArray* windshieldItems;		// contains items of type WindshieldItem*

-(instancetype) initWithDictionary:(NSDictionary*)dictionary;
-(NSDictionary*) dictionaryRepresentation;

@end


@interface Surface : NSObject

@property (nonatomic, copy) NSString* name;
@property (nonatomic, strong) Feld* boundingFeld;
@property (nonatomic, strong) NSArray* felds;		// contains items of type Feld*

-(instancetype) initWithDictionary:(NSDictionary*)dictionary;
-(NSDictionary*) dictionaryRepresentation;

@end


@interface ImageSpec : NSObject

@property (nonatomic, copy) NSString* uri;
@property (nonatomic, assign) NSInteger width;
@property (nonatomic, assign) NSInteger height;

-(instancetype) initWithDictionary:(NSDictionary*)dictionary;
-(NSDictionary*) dictionaryRepresentation;

@end


@interface WorkspaceItem : NSObject

@property (nonatomic, copy) NSString* uid;
@property (nonatomic, strong) NSArray* owners;		// contains items of type NSString*
@property (nonatomic, copy) NSString* name;
@property (nonatomic, copy) NSString* modDate;
@property (nonatomic, assign) NSInteger modDateUtc;
@property (nonatomic, assign) BOOL saved;
@property (nonatomic, copy) NSString* thumbUri;

-(instancetype) initWithDictionary:(NSDictionary*)dictionary;
-(NSDictionary*) dictionaryRepresentation;

@end


@interface WindshieldItem : NSObject

@property (nonatomic, copy) NSString* uid;
@property (nonatomic, copy) NSString* displayName;
@property (nonatomic, copy) NSString* surfaceName;
@property (nonatomic, copy) NSString* contentSource;
@property (nonatomic, copy) NSString* thumbUri;
@property (nonatomic, copy) NSString* largeUri;
@property (nonatomic, copy) NSString* smallUri;
@property (nonatomic, copy) NSString* mediumUri;
@property (nonatomic, copy) NSString* feldInscribedUri;
@property (nonatomic, copy) NSString* fullUri;
@property (nonatomic, assign) BOOL imageAvailable;
@property (nonatomic, assign) BOOL origImageAvailable;
@property (nonatomic, strong) FeldRelativeCoords* feldRelativeCoords;
@property (nonatomic, strong) FeldRelativeSize* feldRelativeSize;
@property (nonatomic, copy) NSString* feldRelativeUri;

-(instancetype) initWithDictionary:(NSDictionary*)dictionary;
-(NSDictionary*) dictionaryRepresentation;

@end


@interface PortfolioItem : NSObject

@property (nonatomic, copy) NSString* uid;
@property (nonatomic, copy) NSString* contentSource;
@property (nonatomic, copy) NSString* displayName;
@property (nonatomic, copy) NSString* thumbUri;
@property (nonatomic, copy) NSString* largeUri;
@property (nonatomic, copy) NSString* smallUri;
@property (nonatomic, copy) NSString* mediumUri;
@property (nonatomic, copy) NSString* feldInscribedUri;
@property (nonatomic, copy) NSString* fullUri;
@property (nonatomic, assign) BOOL imageAvailable;
@property (nonatomic, assign) BOOL origImageAvailable;
@property (nonatomic, copy) NSString* feldRelativeUri;

-(instancetype) initWithDictionary:(NSDictionary*)dictionary;
-(NSDictionary*) dictionaryRepresentation;

@end


@interface LivestreamItem : NSObject

@property (nonatomic, copy) NSString* sourceId;
@property (nonatomic, assign) BOOL active;
@property (nonatomic, copy) NSString* displayName;

-(instancetype) initWithDictionary:(NSDictionary*)dictionary;
-(NSDictionary*) dictionaryRepresentation;

@end


@interface MezzDetail : NSObject

@property (nonatomic, copy) NSString* uid;
@property (nonatomic, copy) NSString* name;
@property (nonatomic, assign) BOOL active;
@property (nonatomic, copy) NSString* address;
@property (nonatomic, copy) NSString* company;
@property (nonatomic, copy) NSString* lastContactUtc;
@property (nonatomic, copy) NSString* location;
@property (nonatomic, strong) Certificate* certificate;

-(instancetype) initWithDictionary:(NSDictionary*)dictionary;
-(NSDictionary*) dictionaryRepresentation;

@end


@interface MezzMetadata : NSObject

@property (nonatomic, assign) BOOL cloudInstance;
@property (nonatomic, strong) FeatureToggles* featureToggles;
@property (nonatomic, copy) NSString* apiVersion;

-(instancetype) initWithDictionary:(NSDictionary*)dictionary;
-(NSDictionary*) dictionaryRepresentation;

@end


@interface RemoteParticipant : NSObject

@property (nonatomic, copy) NSString* uid;
@property (nonatomic, copy) NSString* displayName;

-(instancetype) initWithDictionary:(NSDictionary*)dictionary;
-(NSDictionary*) dictionaryRepresentation;

@end


@interface Handi : NSObject

@property (nonatomic, strong) Vec4* color;
@property (nonatomic, strong) MZVect* quadrant;
@property (nonatomic, copy) NSString* ratchetState;

-(instancetype) initWithDictionary:(NSDictionary*)dictionary;
-(NSDictionary*) dictionaryRepresentation;

@end


@interface PressureReport : NSObject

@property (nonatomic, copy) NSString* main;

-(instancetype) initWithDictionary:(NSDictionary*)dictionary;
-(NSDictionary*) dictionaryRepresentation;

@end


@interface PexipDetail : NSObject

@property (nonatomic, copy) NSString* node;
@property (nonatomic, copy) NSString* conference;

-(instancetype) initWithDictionary:(NSDictionary*)dictionary;
-(NSDictionary*) dictionaryRepresentation;

@end


@interface VideoChat : NSObject

@property (nonatomic, copy) NSString* service;
@property (nonatomic, strong) PexipDetail* pexip;

-(instancetype) initWithDictionary:(NSDictionary*)dictionary;
-(NSDictionary*) dictionaryRepresentation;

@end


@interface CalendarAttendee : NSObject

@property (nonatomic, copy) NSString* email;
@property (nonatomic, copy) NSString* name;

-(instancetype) initWithDictionary:(NSDictionary*)dictionary;
-(NSDictionary*) dictionaryRepresentation;

@end


@interface CalendarMeeting : NSObject

@property (nonatomic, copy) NSString* meetingUid;
@property (nonatomic, copy) NSString* title;
@property (nonatomic, copy) NSString* thirdPartyUrl;
@property (nonatomic, copy) NSString* mezzinUrl;
@property (nonatomic, assign) double startUtc;
@property (nonatomic, assign) double endUtc;
@property (nonatomic, strong) CalendarAttendee* meetingOrganizer;
@property (nonatomic, strong) NSArray* attendees;		// contains items of type CalendarAttendee*

-(instancetype) initWithDictionary:(NSDictionary*)dictionary;
-(NSDictionary*) dictionaryRepresentation;

@end


@interface DialogItem : NSObject

@property (nonatomic, copy) NSString* id;
@property (nonatomic, assign) BOOL modal;
@property (nonatomic, copy) NSString* title;
@property (nonatomic, copy) NSString* message;
@property (nonatomic, strong) NSArray* buttons;		// contains items of type Button*

-(instancetype) initWithDictionary:(NSDictionary*)dictionary;
-(NSDictionary*) dictionaryRepresentation;

@end


@interface Button : NSObject

@property (nonatomic, copy) NSString* type;
@property (nonatomic, copy) NSString* label;
@property (nonatomic, copy) NSString* value;

-(instancetype) initWithDictionary:(NSDictionary*)dictionary;
-(NSDictionary*) dictionaryRepresentation;

@end


@interface Certificate : NSObject

@property (nonatomic, copy) NSString* issuedTo;

-(instancetype) initWithDictionary:(NSDictionary*)dictionary;
-(NSDictionary*) dictionaryRepresentation;

@end


@interface SessionState : NSObject

@property (nonatomic, copy) NSString* sessionId;
@property (nonatomic, copy) NSString* passkey;
@property (nonatomic, assign) BOOL displayPasskey;
@property (nonatomic, assign) NSInteger startUtc;
@property (nonatomic, assign) BOOL isResting;

-(instancetype) initWithDictionary:(NSDictionary*)dictionary;
-(NSDictionary*) dictionaryRepresentation;

@end


@interface RoomParticipant : NSObject

@property (nonatomic, copy) NSString* roomName;
@property (nonatomic, copy) NSString* uid;
@property (nonatomic, strong) NSArray* participants;		// contains items of type Participant*

-(instancetype) initWithDictionary:(NSDictionary*)dictionary;
-(NSDictionary*) dictionaryRepresentation;

@end


@interface Participant : NSObject

@property (nonatomic, copy) NSString* displayName;
@property (nonatomic, copy) NSString* type;
@property (nonatomic, copy) NSString* provenance;

-(instancetype) initWithDictionary:(NSDictionary*)dictionary;
-(NSDictionary*) dictionaryRepresentation;

@end


@interface VtcStatusDataPsa : NSObject

@property (nonatomic, assign) BOOL vtcOnline;
@property (nonatomic, strong) VtcInformationPsa* vtcInformation;
@property (nonatomic, assign) BOOL vtcOnCall;
@property (nonatomic, assign) BOOL vtcRinging;
@property (nonatomic, assign) BOOL vtcDialing;
@property (nonatomic, assign) BOOL vtcInMezzMeeting;
@property (nonatomic, copy) NSString* vtcMezzDialToAddress;
@property (nonatomic, strong) NSArray* vtcCalls;		// contains items of type VtcCallPsa*

-(instancetype) initWithDictionary:(NSDictionary*)dictionary;
-(NSDictionary*) dictionaryRepresentation;

@end


@interface VtcInformationPsa : NSObject

@property (nonatomic, copy) NSString* vtcSystemName;
@property (nonatomic, copy) NSString* vtcSipName;
@property (nonatomic, copy) NSString* vtcVendor;
@property (nonatomic, copy) NSString* vtcModel;
@property (nonatomic, copy) NSString* vtcVersion;

-(instancetype) initWithDictionary:(NSDictionary*)dictionary;
-(NSDictionary*) dictionaryRepresentation;

@end


@interface VtcCallPsa : NSObject

@property (nonatomic, copy) NSString* callId;
@property (nonatomic, copy) NSString* callDirection;
@property (nonatomic, copy) NSString* remoteNumber;
@property (nonatomic, copy) NSString* remoteName;
@property (nonatomic, copy) NSString* callProtocol;
@property (nonatomic, copy) NSString* callState;

-(instancetype) initWithDictionary:(NSDictionary*)dictionary;
-(NSDictionary*) dictionaryRepresentation;

@end


@interface VtcStatusData : NSObject

@property (nonatomic, assign) BOOL vtcOnline;
@property (nonatomic, strong) VtcInformation* vtcInformation;
@property (nonatomic, assign) BOOL vtcOnCall;
@property (nonatomic, assign) BOOL vtcRinging;
@property (nonatomic, assign) BOOL vtcDialing;
@property (nonatomic, assign) BOOL vtcInMezzMeeting;
@property (nonatomic, copy) NSString* vtcMezzDialToAddress;
@property (nonatomic, strong) NSArray* vtcCalls;		// contains items of type VtcCall*
@property (nonatomic, strong) VtcContent* vtcContent;
@property (nonatomic, strong) VtcBridgeStatus* vtcBridgeStatus;

-(instancetype) initWithDictionary:(NSDictionary*)dictionary;
-(NSDictionary*) dictionaryRepresentation;

@end


@interface VtcBridgeStatus : NSObject

@property (nonatomic, assign) BOOL skypeEnabled;
@property (nonatomic, assign) BOOL skypeUnavailable;

-(instancetype) initWithDictionary:(NSDictionary*)dictionary;
-(NSDictionary*) dictionaryRepresentation;

@end


@interface VtcInformation : NSObject

@property (nonatomic, copy) NSString* vtcSystemName;
@property (nonatomic, copy) NSString* vtcSipName;
@property (nonatomic, copy) NSString* vtcVendor;
@property (nonatomic, copy) NSString* vtcModel;
@property (nonatomic, copy) NSString* vtcVersion;

-(instancetype) initWithDictionary:(NSDictionary*)dictionary;
-(NSDictionary*) dictionaryRepresentation;

@end


@interface VtcCall : NSObject

@property (nonatomic, copy) NSString* callId;
@property (nonatomic, copy) NSString* callDirection;
@property (nonatomic, copy) NSString* remoteNumber;
@property (nonatomic, copy) NSString* remoteName;
@property (nonatomic, copy) NSString* callProtocol;
@property (nonatomic, copy) NSString* callState;

-(instancetype) initWithDictionary:(NSDictionary*)dictionary;
-(NSDictionary*) dictionaryRepresentation;

@end


@interface VtcContent : NSObject

@property (nonatomic, assign) BOOL vtcDisplayingSafeContent;
@property (nonatomic, assign) BOOL vtcDisplayingRiskyContent;
@property (nonatomic, assign) BOOL isSendingMezzMainScreenInVtcCall;

-(instancetype) initWithDictionary:(NSDictionary*)dictionary;
-(NSDictionary*) dictionaryRepresentation;

@end


@interface CallEndInfo : NSObject

@property (nonatomic, copy) NSString* vtcCallEndReason;
@property (nonatomic, assign) BOOL vtcInMezzMeeting;
@property (nonatomic, assign) BOOL pleaseLeaveMezzMeeting;
@property (nonatomic, assign) NSInteger otherCallsCount;

-(instancetype) initWithDictionary:(NSDictionary*)dictionary;
-(NSDictionary*) dictionaryRepresentation;

@end

