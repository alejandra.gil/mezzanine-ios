// Automatically generated code, do not edit.

#import <Foundation/Foundation.h>
#import "OBProtein.h"
#import "MZSlaws.h"



@interface AssetDownloadError : OBProtein


@end


@interface AssetDownloadRequest : OBProtein

-(instancetype) initWithContentSource:(NSString*)contentSource pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* contentSource;

@end


@interface AssetDownloadResponse : OBProtein

@property (nonatomic, readonly) NSString* contentSource;
@property (nonatomic, readonly) NSString* downloadUrl;

@end


@interface AssetRefreshPsa : OBProtein

@property (nonatomic, readonly) NSString* contentSource;
@property (nonatomic, readonly) BOOL origImageAvailable;

@end


@interface AssetUploadImageReadyError : OBProtein

@property (nonatomic, readonly) NSArray* uids;		// contains items of type NSString*
@property (nonatomic, readonly) NSString* errorDescription;
@property (nonatomic, readonly) NSString* errorCode;
@property (nonatomic, readonly) NSString* summary;

@end


@interface AssetUploadImageReadyRequest : OBProtein

-(instancetype) initWithWorkspaceUid:(NSString*)workspaceUid uid:(NSString*)uid fileId:(NSInteger)fileId fileName:(NSString*)fileName data:(NSData*)data pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSString* uid;
@property (nonatomic, readonly) NSInteger fileId;
@property (nonatomic, readonly) NSString* fileName;
@property (nonatomic, readonly) NSData* data;

@end


@interface AssetUploadPdfPm : OBProtein

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSArray* fileIds;		// contains items of type NSInteger

@end


@interface AssetUploadPdfReadyError : OBProtein

@property (nonatomic, readonly) NSInteger fileId;
@property (nonatomic, readonly) NSString* errorDescription;
@property (nonatomic, readonly) NSString* errorCode;
@property (nonatomic, readonly) NSString* summary;

@end


@interface AssetUploadPdfReadyRequest : OBProtein

-(instancetype) initWithFileId:(NSInteger)fileId data:(NSData*)data pat:(Pat*)pat;

@property (nonatomic, readonly) NSInteger fileId;
@property (nonatomic, readonly) NSData* data;

@end


@interface AssetUploadProvisionError : OBProtein

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSArray* uids;		// contains items of type NSString*
@property (nonatomic, readonly) NSString* errorDescription;
@property (nonatomic, readonly) NSString* errorCode;
@property (nonatomic, readonly) NSString* summary;

@end


@interface AssetUploadProvisionRequest : OBProtein

-(instancetype) initWithWorkspaceUid:(NSString*)workspaceUid files:(NSArray*)files pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSArray* files;		// contains items of type FileRequest*

@end


@interface AssetUploadProvisionResponse : OBProtein

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSArray* uids;		// contains items of type NSString*
@property (nonatomic, readonly) NSArray* files;		// contains items of type FileResponse*
@property (nonatomic, readonly) NSString* errorDescription;
@property (nonatomic, readonly) NSString* summary;

@end


@interface ClientEvictPm : OBProtein


@end


@interface ClientJoinRequest : OBProtein

-(instancetype) initWithPat:(Pat*)pat;


@end


@interface ClientJoinRequestWithPasskey : OBProtein

-(instancetype) initWithPasskey:(NSString*)passkey pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* passkey;

@end


@interface ClientJoinRequestWithName : OBProtein

-(instancetype) initWithDisplayName:(NSString*)displayName pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* displayName;

@end


@interface ClientJoinRequestWithNameAndPasskey : OBProtein

-(instancetype) initWithDisplayName:(NSString*)displayName passkey:(NSString*)passkey pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* displayName;
@property (nonatomic, readonly) NSString* passkey;

@end


@interface ClientJoinResponse : OBProtein

@property (nonatomic, readonly) NSString* mezzanineVersion;
@property (nonatomic, readonly) AssetUploadLimits* assetUploadLimits;
@property (nonatomic, readonly) NSArray* whiteboards;		// contains items of type Whiteboard*
@property (nonatomic, readonly) NSArray* surfaces;		// contains items of type Surface*
@property (nonatomic, readonly) WorkspaceState* currentWorkspace;
@property (nonatomic, readonly) NSDictionary* felds;		// contains items of type Feld*
@property (nonatomic, readonly) Handi* handipoint;
@property (nonatomic, readonly) BOOL m2mIsInstalled;
@property (nonatomic, readonly) BOOL permission;
@property (nonatomic, readonly) BOOL superuser;
@property (nonatomic, readonly) NSString* username;
@property (nonatomic, readonly) BOOL remoteAccessEnabled;
@property (nonatomic, readonly) MezzMetadata* mezzMetadata;
@property (nonatomic, readonly) NSArray* pendingDialogs;		// contains items of type DialogItem*

@end


@interface ClientJoinResponseDecline : OBProtein

@property (nonatomic, readonly) BOOL permission;
@property (nonatomic, readonly) NSString* reason;

@end


@interface ClientLeaveRequest : OBProtein

-(instancetype) initWithPat:(Pat*)pat;


@end


@interface DialogDisplayPsa : OBProtein

@property (nonatomic, readonly) DialogItem* dialog;

@end


@interface DialogDismissRequest : OBProtein

-(instancetype) initWithId:(NSString*)id value:(NSString*)value pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* id;
@property (nonatomic, readonly) NSString* value;

@end


@interface DialogDismissPsa : OBProtein

@property (nonatomic, readonly) NSString* id;
@property (nonatomic, readonly) NSString* value;

@end


@interface HeartbeatRequest : OBProtein

-(instancetype) initWithJoined:(BOOL)joined pat:(Pat*)pat;

@property (nonatomic, readonly) BOOL joined;

@end


@interface HeartbeatPsa : OBProtein


@end


@interface InfopresenceDetailPsa : OBProtein

@property (nonatomic, readonly) NSString* status;
@property (nonatomic, readonly) NSString* remoteAccessStatus;
@property (nonatomic, readonly) NSString* remoteAccessUrl;
@property (nonatomic, readonly) NSArray* rooms;		// contains items of type NSString*
@property (nonatomic, readonly) NSArray* remoteParticipants;		// contains items of type RemoteParticipant*
@property (nonatomic, readonly) NSArray* incomingCalls;		// contains items of type InfopresenceCall*
@property (nonatomic, readonly) NSArray* outgoingCalls;		// contains items of type InfopresenceCall*
@property (nonatomic, readonly) VideoChat* videoChat;
@property (nonatomic, readonly) BOOL isForMagicM2m;

@end


@interface InfopresenceDetailRequest : OBProtein

-(instancetype) initWithPat:(Pat*)pat;


@end


@interface InfopresenceIncomingPsa : OBProtein

@property (nonatomic, readonly) NSString* uid;
@property (nonatomic, readonly) NSString* resolution;

@end


@interface InfopresenceIncomingRequest : OBProtein

@property (nonatomic, readonly) NSString* uid;
@property (nonatomic, readonly) NSInteger receivedUtc;

@end


@interface InfopresenceIncomingResponse : OBProtein

-(instancetype) initWithUid:(NSString*)uid accept:(BOOL)accept pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* uid;
@property (nonatomic, readonly) BOOL accept;

@end


@interface InfopresenceLeaveRequest : OBProtein

-(instancetype) initWithInterrupted:(BOOL)interrupted pat:(Pat*)pat;

@property (nonatomic, readonly) BOOL interrupted;

@end


@interface InfopresenceOutgoingCancelRequest : OBProtein

-(instancetype) initWithPat:(Pat*)pat;


@end


@interface InfopresenceOutgoingPsa : OBProtein

@property (nonatomic, readonly) NSString* uid;
@property (nonatomic, readonly) NSInteger sentUtc;

@end


@interface InfopresenceOutgoingRequest : OBProtein

-(instancetype) initWithUid:(NSString*)uid sentUtc:(NSInteger)sentUtc pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* uid;
@property (nonatomic, readonly) NSInteger sentUtc;

@end


@interface InfopresenceOutgoingError : OBProtein

@property (nonatomic, readonly) NSString* uid;
@property (nonatomic, readonly) NSString* errorDescription;
@property (nonatomic, readonly) NSString* summary;

@end


@interface InfopresenceOutgoingResponsePsa : OBProtein

@property (nonatomic, readonly) NSString* uid;
@property (nonatomic, readonly) NSString* resolution;

@end


@interface InfopresenceOutgoingInviteRequest : OBProtein

-(instancetype) initWithUid:(NSString*)uid pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* uid;

@end


@interface InfopresenceOutgoingInviteErrorResponse : OBProtein

@property (nonatomic, readonly) NSString* uid;
@property (nonatomic, readonly) NSString* errorDescription;
@property (nonatomic, readonly) NSString* summary;

@end


@interface InfopresenceOutgoingInvitePsa : OBProtein

@property (nonatomic, readonly) NSString* uid;
@property (nonatomic, readonly) NSInteger sentUtc;

@end


@interface InfopresenceOutgoingInviteCancelRequest : OBProtein

-(instancetype) initWithUid:(NSString*)uid pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* uid;

@end


@interface InfopresenceOutgoingInviteResolvePsa : OBProtein

@property (nonatomic, readonly) NSString* uid;
@property (nonatomic, readonly) NSString* resolution;

@end


@interface InfopresenceIncomingInvitePsa : OBProtein

@property (nonatomic, readonly) NSString* uid;
@property (nonatomic, readonly) NSInteger receivedUtc;

@end


@interface InfopresenceIncomingInviteResolveRequest : OBProtein

-(instancetype) initWithUid:(NSString*)uid accept:(BOOL)accept pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* uid;
@property (nonatomic, readonly) BOOL accept;

@end


@interface InfopresenceIncomingInviteResolvePsa : OBProtein

@property (nonatomic, readonly) NSString* uid;
@property (nonatomic, readonly) NSString* resolution;

@end


@interface InfopresenceParticipantJoinPsa : OBProtein

@property (nonatomic, readonly) NSString* uid;
@property (nonatomic, readonly) NSString* displayName;
@property (nonatomic, readonly) NSString* type;

@end


@interface InfopresenceParticipantLeavePsa : OBProtein

@property (nonatomic, readonly) NSString* uid;
@property (nonatomic, readonly) NSString* displayName;
@property (nonatomic, readonly) NSString* type;

@end


@interface LiveStreamCreate : OBProtein

-(instancetype) initWithUri:(NSString*)uri id:(NSString*)id pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* uri;
@property (nonatomic, readonly) NSString* id;

@end


@interface LiveStreamCreateResponse : OBProtein

@property (nonatomic, readonly) NSString* contentSource;

@end


@interface LiveStreamCreateError : OBProtein

@property (nonatomic, readonly) NSString* errorDescription;

@end


@interface LiveStreamDelete : OBProtein

-(instancetype) initWithContentSource:(NSString*)contentSource pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* contentSource;

@end


@interface LiveStreamDeleteResponse : OBProtein

@property (nonatomic, readonly) NSString* message;

@end


@interface LiveStreamDeleteError : OBProtein

@property (nonatomic, readonly) NSString* errorDescription;

@end


@interface LiveStreamAppearPsa : OBProtein

@property (nonatomic, readonly) NSString* contentSource;
@property (nonatomic, readonly) NSString* mezzUid;

@end


@interface LiveStreamDetailPsa : OBProtein

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSArray* liveStreams;		// contains items of type LivestreamItem*

@end


@interface LiveStreamDetailRequest : OBProtein

-(instancetype) initWithPat:(Pat*)pat;


@end


@interface LiveStreamDetailResponse : OBProtein

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSArray* liveStreams;		// contains items of type LivestreamItem*

@end


@interface LiveStreamDissapearPsa : OBProtein

@property (nonatomic, readonly) NSString* contentSource;

@end


@interface LiveStreamGrabPsa : OBProtein

@property (nonatomic, readonly) NSString* uid;

@end


@interface LiveStreamGrabError : OBProtein

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSString* uid;
@property (nonatomic, readonly) NSString* errorDescription;
@property (nonatomic, readonly) NSString* errorCode;
@property (nonatomic, readonly) NSString* summary;

@end


@interface LiveStreamGrabRequest : OBProtein

-(instancetype) initWithWorkspaceUid:(NSString*)workspaceUid uid:(NSString*)uid pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSString* uid;

@end


@interface LiveStreamGrabResponse : OBProtein

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSString* uid;

@end


@interface LiveStreamRelinquishPsa : OBProtein

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSString* uid;

@end


@interface LiveStreamRelinquishError : OBProtein

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSString* uid;
@property (nonatomic, readonly) NSString* errorDescription;
@property (nonatomic, readonly) NSString* errorCode;
@property (nonatomic, readonly) NSString* summary;

@end


@interface LiveStreamRelinquishRequest : OBProtein

-(instancetype) initWithWorkspaceUid:(NSString*)workspaceUid uid:(NSString*)uid pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSString* uid;

@end


@interface LiveStreamRelinquishResponse : OBProtein

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSString* uid;

@end


@interface LiveStreamRefreshPsa : OBProtein

@property (nonatomic, readonly) NSString* contentSource;
@property (nonatomic, readonly) ImageSpec* image;
@property (nonatomic, readonly) ImageSpec* thumbnailImage;

@end


@interface ClientSignInError : OBProtein

@property (nonatomic, readonly) NSString* errorDescription;
@property (nonatomic, readonly) NSString* errorCode;
@property (nonatomic, readonly) NSString* summary;

@end


@interface ClientSignInRequest : OBProtein

-(instancetype) initWithUsername:(NSString*)username password:(NSString*)password pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* username;
@property (nonatomic, readonly) NSString* password;

@end


@interface ClientSignInResponse : OBProtein

@property (nonatomic, readonly) BOOL superuser;
@property (nonatomic, readonly) NSString* username;

@end


@interface ClientSignOutPm : OBProtein


@end


@interface ClientSignOutRequest : OBProtein

-(instancetype) initWithPat:(Pat*)pat;


@end


@interface ClientSignOutResponse : OBProtein


@end


@interface MeetingEndPsa : OBProtein

@property (nonatomic, readonly) NSString* meetingUid;

@end


@interface MeetingEndError : OBProtein

@property (nonatomic, readonly) NSString* meetingUid;
@property (nonatomic, readonly) NSString* errorDescription;
@property (nonatomic, readonly) NSString* errorCode;
@property (nonatomic, readonly) NSString* summary;

@end


@interface MeetingEndRequest : OBProtein

-(instancetype) initWithMeetingUid:(NSString*)meetingUid pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* meetingUid;

@end


@interface MeetingJoinUrlRequest : OBProtein

-(instancetype) initWithMezzinUrl:(NSString*)mezzinUrl isForMagicM2m:(BOOL)isForMagicM2m pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* mezzinUrl;
@property (nonatomic, readonly) BOOL isForMagicM2m;

@end


@interface MeetingListPsa : OBProtein

@property (nonatomic, readonly) NSArray* meetingList;		// contains items of type CalendarMeeting*
@property (nonatomic, readonly) CalendarMeeting* inProgressMeeting;

@end


@interface MeetingListRequest : OBProtein

-(instancetype) initWithPat:(Pat*)pat;


@end


@interface MeetingStartPsa : OBProtein

@property (nonatomic, readonly) NSString* meetingUid;
@property (nonatomic, readonly) CalendarMeeting* meeting;

@end


@interface MeetingStartError : OBProtein

@property (nonatomic, readonly) NSString* meetingUid;
@property (nonatomic, readonly) NSString* errorDescription;
@property (nonatomic, readonly) NSString* errorCode;
@property (nonatomic, readonly) NSString* summary;

@end


@interface MeetingStartRequest : OBProtein

-(instancetype) initWithMeetingUid:(NSString*)meetingUid isForMagicM2m:(BOOL)isForMagicM2m pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* meetingUid;
@property (nonatomic, readonly) BOOL isForMagicM2m;

@end


@interface MeetingReadyPsa : OBProtein

@property (nonatomic, readonly) NSString* meetingUid;
@property (nonatomic, readonly) CalendarMeeting* meeting;

@end


@interface MeetingAbortedPsa : OBProtein

@property (nonatomic, readonly) NSString* meetingUid;
@property (nonatomic, readonly) CalendarMeeting* meeting;

@end


@interface MezCapsPsa : OBProtein

@property (nonatomic, readonly) AssetUploadLimits* assetUploadLimits;
@property (nonatomic, readonly) NSArray* whiteboards;		// contains items of type Whiteboard*
@property (nonatomic, readonly) NSDictionary* felds;		// contains items of type Feld*
@property (nonatomic, readonly) NSArray* surfaces;		// contains items of type Surface*
@property (nonatomic, readonly) BOOL remoteAccessEnabled;

@end


@interface MezCapsRequest : OBProtein

-(instancetype) initWithPat:(Pat*)pat;


@end


@interface MezCapsResponse : OBProtein

@property (nonatomic, readonly) AssetUploadLimits* assetUploadLimits;
@property (nonatomic, readonly) NSArray* whiteboards;		// contains items of type Whiteboard*
@property (nonatomic, readonly) NSArray* surfaces;		// contains items of type Surface*

@end


@interface MezzMetadataRequest : OBProtein

-(instancetype) initWithPat:(Pat*)pat;


@end


@interface MezzMetadataResponse : OBProtein

@property (nonatomic, readonly) BOOL cloudInstance;
@property (nonatomic, readonly) FeatureToggles* featureToggles;
@property (nonatomic, readonly) NSString* apiVersion;
@property (nonatomic, readonly) BOOL systemUseNotification;
@property (nonatomic, readonly) NSString* systemUseNotificationText;
@property (nonatomic, readonly) NSString* model;
@property (nonatomic, readonly) BOOL ocelotSystem;

@end


@interface OhayoGoyaimasuPsa : OBProtein


@end


@interface OutgoingInfopresenceSessionRequest : OBProtein

-(instancetype) initWithPat:(Pat*)pat;


@end


@interface PasskeyDetailRequest : OBProtein

-(instancetype) initWithPat:(Pat*)pat;


@end


@interface PasskeyDetailResponse : OBProtein

@property (nonatomic, readonly) NSString* clientConnectionUrl;
@property (nonatomic, readonly) BOOL isPasskeyProtected;
@property (nonatomic, readonly) NSString* passkey;

@end


@interface PasskeyDisablePsa : OBProtein


@end


@interface PasskeyDisableRequest : OBProtein

-(instancetype) initWithPat:(Pat*)pat;


@end


@interface PasskeyEnablePsa : OBProtein

@property (nonatomic, readonly) NSString* exempt;

@end


@interface PasskeyEnableRequest : OBProtein

-(instancetype) initWithPat:(Pat*)pat;


@end


@interface PortfolioDetailPsa : OBProtein

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSArray* portfolioItems;		// contains items of type PortfolioItem*

@end


@interface PortfolioDetailRequest : OBProtein

-(instancetype) initWithWorkspaceUid:(NSString*)workspaceUid pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* workspaceUid;

@end


@interface PortfolioDetailResponse : OBProtein

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSArray* portfolioItems;		// contains items of type PortfolioItem*

@end


@interface PortfolioDownloadError : OBProtein

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSString* errorDescription;
@property (nonatomic, readonly) NSString* errorCode;
@property (nonatomic, readonly) NSString* summary;

@end


@interface PortfolioDownloadRequest : OBProtein

-(instancetype) initWithWorkspaceUid:(NSString*)workspaceUid pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* workspaceUid;

@end


@interface PortfolioDownloadResponse : OBProtein

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSString* downloadUrl;

@end


@interface PortfolioItemDeleteRequest : OBProtein

-(instancetype) initWithWorkspaceUid:(NSString*)workspaceUid uid:(NSString*)uid pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSString* uid;

@end


@interface PortfolioItemDeleteError : OBProtein

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSString* uid;
@property (nonatomic, readonly) NSString* errorDescription;
@property (nonatomic, readonly) NSString* errorCode;
@property (nonatomic, readonly) NSString* summary;

@end


@interface PortfolioItemDeletePsa : OBProtein

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSString* uid;

@end


@interface PortfolioClearPsa : OBProtein

@property (nonatomic, readonly) NSString* workspaceUid;

@end


@interface PortfolioClearRequest : OBProtein

-(instancetype) initWithWorkspaceUid:(NSString*)workspaceUid pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* workspaceUid;

@end


@interface PortfolioClearError : OBProtein

@property (nonatomic, readonly) NSString* errorDescription;
@property (nonatomic, readonly) NSString* summary;

@end


@interface PortfolioItemGrabPsa : OBProtein

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSString* uid;

@end


@interface PortfolioItemGrabError : OBProtein

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSString* uid;
@property (nonatomic, readonly) NSString* errorDescription;
@property (nonatomic, readonly) NSString* errorCode;
@property (nonatomic, readonly) NSString* summary;

@end


@interface PortfolioItemGrabRequest : OBProtein

-(instancetype) initWithWorkspaceUid:(NSString*)workspaceUid uid:(NSString*)uid pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSString* uid;

@end


@interface PortfolioItemGrabResponse : OBProtein

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSString* uid;

@end


@interface PortfolioItemInsertPsa : OBProtein

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSString* uid;
@property (nonatomic, readonly) NSString* contentSource;
@property (nonatomic, readonly) NSString* displayName;
@property (nonatomic, readonly) NSInteger index;
@property (nonatomic, readonly) NSString* thumbUri;
@property (nonatomic, readonly) NSString* largeUri;
@property (nonatomic, readonly) NSString* smallUri;
@property (nonatomic, readonly) NSString* mediumUri;
@property (nonatomic, readonly) NSString* feldRelativeUri;
@property (nonatomic, readonly) NSString* fullUri;
@property (nonatomic, readonly) BOOL imageAvailable;
@property (nonatomic, readonly) NSString* feldInscribedUri;
@property (nonatomic, readonly) BOOL origImageAvailable;

@end


@interface PortfolioItemInsertRequest : OBProtein

-(instancetype) initWithWorkspaceUid:(NSString*)workspaceUid contentSource:(NSString*)contentSource index:(NSInteger)index pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSString* contentSource;
@property (nonatomic, readonly) NSInteger index;

@end


@interface PortfolioItemInsertError : OBProtein

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSString* contentSource;
@property (nonatomic, readonly) NSString* errorDescription;
@property (nonatomic, readonly) NSInteger index;
@property (nonatomic, readonly) NSString* summary;

@end


@interface PortfolioItemRelinquishPsa : OBProtein

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSString* uid;

@end


@interface PortfolioItemRelinquishError : OBProtein

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSString* uid;
@property (nonatomic, readonly) NSString* errorDescription;
@property (nonatomic, readonly) NSString* errorCode;
@property (nonatomic, readonly) NSString* summary;

@end


@interface PortfolioItemRelinquishRequest : OBProtein

-(instancetype) initWithWorkspaceUid:(NSString*)workspaceUid uid:(NSString*)uid pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSString* uid;

@end


@interface PortfolioItemRelinquishResponse : OBProtein

@property (nonatomic, readonly) NSString* uid;

@end


@interface PortfolioItemReorderPsa : OBProtein

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSString* uid;
@property (nonatomic, readonly) NSInteger index;

@end


@interface PortfolioItemReorderError : OBProtein

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSString* uid;
@property (nonatomic, readonly) NSInteger index;
@property (nonatomic, readonly) NSString* errorDescription;
@property (nonatomic, readonly) NSString* summary;

@end


@interface PortfolioItemReorderRequest : OBProtein

-(instancetype) initWithWorkspaceUid:(NSString*)workspaceUid uid:(NSString*)uid index:(NSInteger)index pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSString* uid;
@property (nonatomic, readonly) NSInteger index;

@end


@interface PresentationDetailPsa : OBProtein

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) BOOL active;
@property (nonatomic, readonly) NSInteger currentIndex;

@end


@interface PresentationScrollRequest : OBProtein

-(instancetype) initWithWorkspaceUid:(NSString*)workspaceUid currentIndex:(NSInteger)currentIndex pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSInteger currentIndex;

@end


@interface PresentationStartRequest : OBProtein

-(instancetype) initWithWorkspaceUid:(NSString*)workspaceUid currentIndex:(NSInteger)currentIndex pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSInteger currentIndex;

@end


@interface PresentationStopRequest : OBProtein

-(instancetype) initWithWorkspaceUid:(NSString*)workspaceUid pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* workspaceUid;

@end


@interface ProfileDetailPsa : OBProtein

@property (nonatomic, readonly) MezzDetail* profile;

@end


@interface ProfileDetailRequest : OBProtein

-(instancetype) initWithPat:(Pat*)pat;


@end


@interface RatchetError : OBProtein

@property (nonatomic, readonly) NSString* errorDescription;
@property (nonatomic, readonly) NSString* errorCode;
@property (nonatomic, readonly) NSString* summary;

@end


@interface RatchetResponse : OBProtein

@property (nonatomic, readonly) NSString* ratchetState;

@end


@interface RatchetRequest : OBProtein

-(instancetype) initWithRatchetState:(NSString*)ratchetState pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* ratchetState;

@end


@interface InfopresenceEnableRemoteAccessRequest : OBProtein

-(instancetype) initWithPat:(Pat*)pat;


@end


@interface InfopresenceEnableRemoteAccessErrorResponse : OBProtein

@property (nonatomic, readonly) NSString* errorDescription;
@property (nonatomic, readonly) NSString* errorCode;
@property (nonatomic, readonly) NSString* summary;

@end


@interface InfopresenceDisableRemoteAccessRequest : OBProtein

-(instancetype) initWithPat:(Pat*)pat;


@end


@interface InfopresenceDisableRemoteAccessErrorResponse : OBProtein

@property (nonatomic, readonly) NSString* errorDescription;
@property (nonatomic, readonly) NSString* errorCode;
@property (nonatomic, readonly) NSString* summary;

@end


@interface PassforwardRequest : OBProtein

-(instancetype) initWithSpaceCntx:(NSString*)spaceCntx curAim:(MZVect*)curAim curOrigin:(MZVect*)curOrigin pressureReports:(PressureReport*)pressureReports pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* spaceCntx;
@property (nonatomic, readonly) MZVect* curAim;
@property (nonatomic, readonly) MZVect* curOrigin;
@property (nonatomic, readonly) PressureReport* pressureReports;

@end


@interface RemoteMezzaninesDetailPsa : OBProtein

@property (nonatomic, readonly) NSArray* remoteMezzanines;		// contains items of type MezzDetail*

@end


@interface RemoteMezzaninesDetailRequest : OBProtein

-(instancetype) initWithPat:(Pat*)pat;


@end


@interface RemoteSessionEndPsa : OBProtein


@end


@interface StorageDetailPsa : OBProtein

@property (nonatomic, readonly) BOOL lowStorage;

@end


@interface StorageDetailRequest : OBProtein

-(instancetype) initWithPat:(Pat*)pat;


@end


@interface WhiteboardCaptureError : OBProtein

@property (nonatomic, readonly) NSString* errorDescription;
@property (nonatomic, readonly) NSString* errorCode;
@property (nonatomic, readonly) NSString* summary;

@end


@interface WhiteboardCaptureRequest : OBProtein

-(instancetype) initWithUid:(NSString*)uid workspaceUid:(NSString*)workspaceUid pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* uid;
@property (nonatomic, readonly) NSString* workspaceUid;

@end


@interface WhiteboardCaptureResponse : OBProtein

@property (nonatomic, readonly) NSString* workspaceUid;

@end


@interface WindshieldArrangeError : OBProtein

@property (nonatomic, readonly) NSString* errorDescription;
@property (nonatomic, readonly) NSString* errorCode;
@property (nonatomic, readonly) NSString* summary;

@end


@interface WindshieldArrangeRequest : OBProtein

-(instancetype) initWithWorkspaceUid:(NSString*)workspaceUid surfaces:(NSArray*)surfaces pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSArray* surfaces;		// contains items of type NSString*

@end


@interface WindshieldClearError : OBProtein

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSString* errorDescription;
@property (nonatomic, readonly) NSString* errorCode;
@property (nonatomic, readonly) NSString* summary;

@end


@interface WindshieldClearPsa : OBProtein

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSArray* surfaces;		// contains items of type NSString*

@end


@interface WindshieldClearRequest : OBProtein

-(instancetype) initWithWorkspaceUid:(NSString*)workspaceUid surfaces:(NSArray*)surfaces pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSArray* surfaces;		// contains items of type NSString*

@end


@interface WindshieldDetailPsa : OBProtein

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSArray* windshieldItems;		// contains items of type WindshieldItem*

@end


@interface WindshieldDetailRequest : OBProtein

-(instancetype) initWithWorkspaceUid:(NSString*)workspaceUid pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* workspaceUid;

@end


@interface WindshieldItemCreateError : OBProtein

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSString* errorDescription;
@property (nonatomic, readonly) NSString* errorCode;
@property (nonatomic, readonly) NSString* summary;

@end


@interface WindshieldItemCreatePsa : OBProtein

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSString* uid;
@property (nonatomic, readonly) NSString* contentSource;
@property (nonatomic, readonly) NSString* displayName;
@property (nonatomic, readonly) NSString* surfaceName;
@property (nonatomic, readonly) FeldRelativeCoords* feldRelativeCoords;
@property (nonatomic, readonly) FeldRelativeSize* feldRelativeSize;
@property (nonatomic, readonly) NSString* thumbUri;
@property (nonatomic, readonly) NSString* largeUri;
@property (nonatomic, readonly) NSString* smallUri;
@property (nonatomic, readonly) NSString* mediumUri;
@property (nonatomic, readonly) NSString* feldRelativeUri;
@property (nonatomic, readonly) NSString* fullUri;
@property (nonatomic, readonly) BOOL imageAvailable;
@property (nonatomic, readonly) NSString* feldInscribedUri;
@property (nonatomic, readonly) BOOL origImageAvailable;

@end


@interface WindshieldItemCreateRequest : OBProtein

-(instancetype) initWithWorkspaceUid:(NSString*)workspaceUid contentSource:(NSString*)contentSource surfaceName:(NSString*)surfaceName feldRelativeCoords:(FeldRelativeCoords*)feldRelativeCoords feldRelativeSize:(FeldRelativeSize*)feldRelativeSize pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSString* contentSource;
@property (nonatomic, readonly) NSString* surfaceName;
@property (nonatomic, readonly) FeldRelativeCoords* feldRelativeCoords;
@property (nonatomic, readonly) FeldRelativeSize* feldRelativeSize;

@end


@interface WindshieldItemDeleteError : OBProtein

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSString* errorDescription;
@property (nonatomic, readonly) NSString* errorCode;
@property (nonatomic, readonly) NSString* summary;

@end


@interface WindshieldItemDeletePsa : OBProtein

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSString* uid;

@end


@interface WindshieldItemDeleteRequest : OBProtein

-(instancetype) initWithWorkspaceUid:(NSString*)workspaceUid uid:(NSString*)uid pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSString* uid;

@end


@interface WindshieldItemGrabError : OBProtein

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSString* uid;
@property (nonatomic, readonly) NSString* errorDescription;
@property (nonatomic, readonly) NSString* errorCode;
@property (nonatomic, readonly) NSString* summary;

@end


@interface WindshieldItemGrabPsa : OBProtein

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSString* uid;

@end


@interface WindshieldItemGrabRequest : OBProtein

-(instancetype) initWithWorkspaceUid:(NSString*)workspaceUid uid:(NSString*)uid pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSString* uid;

@end


@interface WindshieldItemGrabResponse : OBProtein

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSString* uid;

@end


@interface WindshieldItemTransformError : OBProtein

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSString* errorDescription;
@property (nonatomic, readonly) NSString* errorCode;
@property (nonatomic, readonly) NSString* summary;

@end


@interface WindshieldItemTransformPsa : OBProtein

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSString* uid;
@property (nonatomic, readonly) NSString* surfaceName;
@property (nonatomic, readonly) FeldRelativeCoords* feldRelativeCoords;
@property (nonatomic, readonly) FeldRelativeSize* feldRelativeSize;
@property (nonatomic, readonly) BOOL moveToFront;

@end


@interface WindshieldItemTransformRequest : OBProtein

-(instancetype) initWithWorkspaceUid:(NSString*)workspaceUid uid:(NSString*)uid surfaceName:(NSString*)surfaceName feldRelativeCoords:(FeldRelativeCoords*)feldRelativeCoords feldRelativeSize:(FeldRelativeSize*)feldRelativeSize pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSString* uid;
@property (nonatomic, readonly) NSString* surfaceName;
@property (nonatomic, readonly) FeldRelativeCoords* feldRelativeCoords;
@property (nonatomic, readonly) FeldRelativeSize* feldRelativeSize;

@end


@interface WindshieldItemRelinquishPsa : OBProtein

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSString* uid;

@end


@interface WindshieldItemRelinquishError : OBProtein

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSString* uid;
@property (nonatomic, readonly) NSString* errorDescription;
@property (nonatomic, readonly) NSString* errorCode;
@property (nonatomic, readonly) NSString* summary;

@end


@interface WindshieldItemRelinquishRequest : OBProtein

-(instancetype) initWithWorkspaceUid:(NSString*)workspaceUid uid:(NSString*)uid pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSString* uid;

@end


@interface WindshieldItemRelinquishResponse : OBProtein

@property (nonatomic, readonly) NSString* workspaceUid;
@property (nonatomic, readonly) NSString* uid;

@end


@interface WorkspaceCloseError : OBProtein

@property (nonatomic, readonly) NSString* errorDescription;
@property (nonatomic, readonly) NSString* errorCode;
@property (nonatomic, readonly) NSString* summary;

@end


@interface WorkspaceCloseRequest : OBProtein

-(instancetype) initWithUid:(NSString*)uid pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* uid;

@end


@interface WorkspaceCreateError : OBProtein

@property (nonatomic, readonly) NSString* errorDescription;
@property (nonatomic, readonly) NSString* errorCode;
@property (nonatomic, readonly) NSString* summary;

@end


@interface WorkspaceCreatePm : OBProtein

@property (nonatomic, readonly) NSString* uid;
@property (nonatomic, readonly) NSString* name;
@property (nonatomic, readonly) NSArray* owners;		// contains items of type NSString*
@property (nonatomic, readonly) NSString* modDate;
@property (nonatomic, readonly) NSInteger modDateUtc;
@property (nonatomic, readonly) BOOL saved;
@property (nonatomic, readonly) NSString* thumbUri;

@end


@interface WorkspaceCreatePsa : OBProtein

@property (nonatomic, readonly) NSString* uid;
@property (nonatomic, readonly) NSString* name;
@property (nonatomic, readonly) NSArray* owners;		// contains items of type NSString*
@property (nonatomic, readonly) NSString* modDate;
@property (nonatomic, readonly) NSInteger modDateUtc;
@property (nonatomic, readonly) BOOL saved;
@property (nonatomic, readonly) NSString* thumbUri;

@end


@interface WorkspaceCreateRequest : OBProtein

-(instancetype) initWithName:(NSString*)name owners:(NSArray*)owners pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* name;
@property (nonatomic, readonly) NSArray* owners;		// contains items of type NSString*

@end


@interface WorkspaceDeleteError : OBProtein

@property (nonatomic, readonly) NSString* errorDescription;
@property (nonatomic, readonly) NSString* errorCode;
@property (nonatomic, readonly) NSString* summary;

@end


@interface WorkspaceDeletePm : OBProtein

@property (nonatomic, readonly) NSString* uid;

@end


@interface WorkspaceDeletePsa : OBProtein

@property (nonatomic, readonly) NSString* uid;

@end


@interface WorkspaceModifiedPsa : OBProtein

@property (nonatomic, readonly) NSString* uid;
@property (nonatomic, readonly) NSString* modDate;
@property (nonatomic, readonly) NSInteger modDateUtc;
@property (nonatomic, readonly) NSString* thumbUri;

@end


@interface WorkspaceDeleteRequest : OBProtein

-(instancetype) initWithUid:(NSString*)uid pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* uid;

@end


@interface WorkspaceDiscardRequest : OBProtein

-(instancetype) initWithUid:(NSString*)uid pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* uid;

@end


@interface WorkspaceDownloadError : OBProtein

@property (nonatomic, readonly) NSString* errorDescription;
@property (nonatomic, readonly) NSString* errorCode;
@property (nonatomic, readonly) NSString* summary;

@end


@interface WorkspaceDownloadRequest : OBProtein

-(instancetype) initWithUid:(NSString*)uid pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* uid;

@end


@interface WorkspaceDownloadResponse : OBProtein

@property (nonatomic, readonly) NSString* uid;
@property (nonatomic, readonly) NSString* downloadUrl;

@end


@interface WorkspaceDuplicateError : OBProtein

@property (nonatomic, readonly) NSString* errorDescription;
@property (nonatomic, readonly) NSString* errorCode;
@property (nonatomic, readonly) NSString* summary;

@end


@interface WorkspaceDuplicateRequest : OBProtein

-(instancetype) initWithUid:(NSString*)uid owners:(NSArray*)owners name:(NSString*)name pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* uid;
@property (nonatomic, readonly) NSArray* owners;		// contains items of type NSString*
@property (nonatomic, readonly) NSString* name;

@end


@interface WorkspaceGeomChangedPsa : OBProtein

@property (nonatomic, readonly) NSDictionary* felds;		// contains items of type Feld*
@property (nonatomic, readonly) NSArray* surfaces;		// contains items of type Surface*

@end


@interface WorkspaceListRequest : OBProtein

-(instancetype) initWithPat:(Pat*)pat;


@end


@interface WorkspaceListResponse : OBProtein

@property (nonatomic, readonly) NSArray* workspaces;		// contains items of type WorkspaceItem*

@end


@interface WorkspaceOpenError : OBProtein

@property (nonatomic, readonly) NSString* errorDescription;
@property (nonatomic, readonly) NSString* errorCode;
@property (nonatomic, readonly) NSString* summary;

@end


@interface WorkspaceOpenRequest : OBProtein

-(instancetype) initWithSwitchFromUid:(NSString*)switchFromUid switchToUid:(NSString*)switchToUid pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* switchFromUid;
@property (nonatomic, readonly) NSString* switchToUid;

@end


@interface WorkspaceRenameError : OBProtein

@property (nonatomic, readonly) NSString* errorDescription;
@property (nonatomic, readonly) NSString* errorCode;
@property (nonatomic, readonly) NSString* summary;

@end


@interface WorkspaceRenamePm : OBProtein

@property (nonatomic, readonly) NSString* uid;
@property (nonatomic, readonly) NSString* name;
@property (nonatomic, readonly) NSString* modDate;
@property (nonatomic, readonly) NSInteger modDateUtc;

@end


@interface WorkspaceRenamePsa : OBProtein

@property (nonatomic, readonly) NSString* uid;
@property (nonatomic, readonly) NSString* name;
@property (nonatomic, readonly) NSString* modDate;
@property (nonatomic, readonly) NSInteger modDateUtc;

@end


@interface WorkspaceRenameRequest : OBProtein

-(instancetype) initWithUid:(NSString*)uid name:(NSString*)name pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* uid;
@property (nonatomic, readonly) NSString* name;

@end


@interface WorkspaceReassignPsa : OBProtein

@property (nonatomic, readonly) NSString* oldUid;
@property (nonatomic, readonly) NSString* newUid;
@property (nonatomic, readonly) NSString* name;
@property (nonatomic, readonly) NSString* thumbUri;

@end


@interface WorkspaceSavePsa : OBProtein

@property (nonatomic, readonly) NSString* uid;
@property (nonatomic, readonly) NSArray* owners;		// contains items of type NSString*
@property (nonatomic, readonly) NSString* name;

@end


@interface WorkspaceSaveRequest : OBProtein

-(instancetype) initWithUid:(NSString*)uid owners:(NSArray*)owners name:(NSString*)name pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* uid;
@property (nonatomic, readonly) NSArray* owners;		// contains items of type NSString*
@property (nonatomic, readonly) NSString* name;

@end


@interface WorkspaceSwitchPsa : OBProtein

@property (nonatomic, readonly) WorkspaceState* currentWorkspace;
@property (nonatomic, readonly) WorkspaceStateClosed* previousWorkspace;

@end


@interface WorkspaceUploadDoneError : OBProtein

@property (nonatomic, readonly) NSString* uid;
@property (nonatomic, readonly) NSArray* owners;		// contains items of type NSString*
@property (nonatomic, readonly) NSString* fileName;

@end


@interface WorkspaceUploadDoneResponse : OBProtein

@property (nonatomic, readonly) NSString* errorDescription;
@property (nonatomic, readonly) NSString* errorCode;
@property (nonatomic, readonly) NSString* summary;

@end


@interface WorkspaceUploadError : OBProtein

@property (nonatomic, readonly) NSString* errorDescription;
@property (nonatomic, readonly) NSString* errorCode;
@property (nonatomic, readonly) NSString* summary;

@end


@interface WorkspaceUploadResponse : OBProtein

@property (nonatomic, readonly) NSString* uid;
@property (nonatomic, readonly) NSArray* owners;		// contains items of type NSString*
@property (nonatomic, readonly) NSString* uploadUrl;

@end


@interface WorkspaceUploadRequest : OBProtein

-(instancetype) initWithOwners:(NSArray*)owners pat:(Pat*)pat;

@property (nonatomic, readonly) NSArray* owners;		// contains items of type NSString*

@end


@interface WorkspaceUploadDoneRequest : OBProtein

-(instancetype) initWithUid:(NSString*)uid owners:(NSArray*)owners fileName:(NSString*)fileName uploadUrl:(NSString*)uploadUrl pat:(Pat*)pat;

@property (nonatomic, readonly) NSString* uid;
@property (nonatomic, readonly) NSArray* owners;		// contains items of type NSString*
@property (nonatomic, readonly) NSString* fileName;
@property (nonatomic, readonly) NSString* uploadUrl;

@end


@interface RequestVtcStatus : OBProtein

-(instancetype) initWithPat:(Pat*)pat;


@end


@interface VtcStatus : OBProtein

@property (nonatomic, readonly) VtcStatusData* status;

@end


@interface VtcCallEndMessage : OBProtein

@property (nonatomic, readonly) CallEndInfo* callEndInfo;

@end


@interface SessionDetailPsa : OBProtein

@property (nonatomic, readonly) SessionState* session;

@end


@interface DialogDisplayPm : OBProtein

@property (nonatomic, readonly) DialogItem* dialog;

@end


@interface MeetingWorkspaceCleanupRequest : OBProtein

-(instancetype) initWithPat:(Pat*)pat;


@end


@interface MeetingWorkspaceCleanupResponse : OBProtein


@end


@interface ParticipantListRequest : OBProtein

-(instancetype) initWithPat:(Pat*)pat;


@end


@interface ParticipantListPsa : OBProtein

@property (nonatomic, readonly) NSArray* cloudParticipants;		// contains items of type Participant*
@property (nonatomic, readonly) RoomParticipant* localRoom;
@property (nonatomic, readonly) NSArray* remoteRooms;		// contains items of type RoomParticipant*

@end


@interface StatusFromVtcPsa : OBProtein

@property (nonatomic, readonly) VtcStatusDataPsa* status;

@end

