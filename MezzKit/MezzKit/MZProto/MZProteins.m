// Automatically generated code, do not edit.
#import "MZProteins.h"



@implementation AssetDownloadError

@end



@implementation AssetDownloadRequest

-(instancetype) initWithContentSource:(NSString*)contentSource pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"asset-download",
			@"from:",
			pat,
		];

		ingests = @{
			@"content-source": contentSource,
		};
	}
	return self;
}

-(NSString*) contentSource
{
	return [ingests objectForKey:@"content-source"];
}

@end



@implementation AssetDownloadResponse

-(NSString*) contentSource
{
	return [ingests objectForKey:@"content-source"];
}

-(NSString*) downloadUrl
{
	return [ingests objectForKey:@"download-url"];
}

@end



@implementation AssetRefreshPsa

-(NSString*) contentSource
{
	return [ingests objectForKey:@"content-source"];
}

-(BOOL) origImageAvailable
{
	return [[ingests objectForKey:@"orig-image-available"] boolValue];
}

@end



@implementation AssetUploadImageReadyError

-(NSArray*) uids
{
	return [ingests objectForKey:@"uids"];
}

-(NSString*) errorDescription
{
	return [ingests objectForKey:@"description"];
}

-(NSString*) errorCode
{
	return [ingests objectForKey:@"error-code"];
}

-(NSString*) summary
{
	return [ingests objectForKey:@"summary"];
}

@end



@implementation AssetUploadImageReadyRequest

-(instancetype) initWithWorkspaceUid:(NSString*)workspaceUid uid:(NSString*)uid fileId:(NSInteger)fileId fileName:(NSString*)fileName data:(NSData*)data pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"asset-upload-image-ready",
			@"from:",
			pat,
			@"to:",
			@"asset-manager",
		];

		ingests = @{
			@"workspace-uid": workspaceUid,
			@"uid": uid,
			@"file-id": @(fileId),
			@"file-name": fileName,
			@"data": data,
		};
	}
	return self;
}

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

-(NSInteger) fileId
{
	return [[ingests objectForKey:@"file-id"] integerValue];
}

-(NSString*) fileName
{
	return [ingests objectForKey:@"file-name"];
}

-(NSData*) data
{
	return [ingests objectForKey:@"data"];
}

@end



@implementation AssetUploadPdfPm

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSArray*) fileIds
{
	return [ingests objectForKey:@"file-ids"];
}

@end



@implementation AssetUploadPdfReadyError

-(NSInteger) fileId
{
	return [[ingests objectForKey:@"file-id"] integerValue];
}

-(NSString*) errorDescription
{
	return [ingests objectForKey:@"description"];
}

-(NSString*) errorCode
{
	return [ingests objectForKey:@"error-code"];
}

-(NSString*) summary
{
	return [ingests objectForKey:@"summary"];
}

@end



@implementation AssetUploadPdfReadyRequest

-(instancetype) initWithFileId:(NSInteger)fileId data:(NSData*)data pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"asset-upload-pdf-ready",
			@"from:",
			pat,
			@"to:",
			@"asset-manager",
		];

		ingests = @{
			@"file-id": @(fileId),
			@"data": data,
		};
	}
	return self;
}

-(NSInteger) fileId
{
	return [[ingests objectForKey:@"file-id"] integerValue];
}

-(NSData*) data
{
	return [ingests objectForKey:@"data"];
}

@end



@implementation AssetUploadProvisionError

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSArray*) uids
{
	return [ingests objectForKey:@"uids"];
}

-(NSString*) errorDescription
{
	return [ingests objectForKey:@"description"];
}

-(NSString*) errorCode
{
	return [ingests objectForKey:@"error-code"];
}

-(NSString*) summary
{
	return [ingests objectForKey:@"summary"];
}

@end



@implementation AssetUploadProvisionRequest

-(instancetype) initWithWorkspaceUid:(NSString*)workspaceUid files:(NSArray*)files pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"asset-upload-provision",
			@"from:",
			pat,
		];

		ingests = @{
			@"workspace-uid": workspaceUid,
			@"files": files,
		};
	}
	return self;
}

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSArray*) files
{
	return [ingests objectForKey:@"files"];
}

@end



@implementation AssetUploadProvisionResponse

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSArray*) uids
{
	return [ingests objectForKey:@"uids"];
}

-(NSArray*) files
{
	return [ingests objectForKey:@"files"];
}

-(NSString*) errorDescription
{
	return [ingests objectForKey:@"description"];
}

-(NSString*) summary
{
	return [ingests objectForKey:@"summary"];
}

@end



@implementation ClientEvictPm

@end



@implementation ClientJoinRequest

-(instancetype) initWithPat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"client-join",
			@"from:",
			pat,
		];
	}
	return self;
}

@end



@implementation ClientJoinRequestWithPasskey

-(instancetype) initWithPasskey:(NSString*)passkey pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"client-join",
			@"from:",
			pat,
		];

		ingests = @{
			@"passkey": passkey,
		};
	}
	return self;
}

-(NSString*) passkey
{
	return [ingests objectForKey:@"passkey"];
}

@end



@implementation ClientJoinRequestWithName

-(instancetype) initWithDisplayName:(NSString*)displayName pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"client-join",
			@"from:",
			pat,
		];

		ingests = @{
			@"display-name": displayName,
		};
	}
	return self;
}

-(NSString*) displayName
{
	return [ingests objectForKey:@"display-name"];
}

@end



@implementation ClientJoinRequestWithNameAndPasskey

-(instancetype) initWithDisplayName:(NSString*)displayName passkey:(NSString*)passkey pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"client-join",
			@"from:",
			pat,
		];

		ingests = @{
			@"display-name": displayName,
			@"passkey": passkey,
		};
	}
	return self;
}

-(NSString*) displayName
{
	return [ingests objectForKey:@"display-name"];
}

-(NSString*) passkey
{
	return [ingests objectForKey:@"passkey"];
}

@end



@implementation ClientJoinResponse

-(NSString*) mezzanineVersion
{
	return [ingests objectForKey:@"mezzanine-version"];
}

-(AssetUploadLimits*) assetUploadLimits
{
	return [ingests objectForKey:@"asset-upload-limits"];
}

-(NSArray*) whiteboards
{
	return [ingests objectForKey:@"whiteboards"];
}

-(NSArray*) surfaces
{
	return [ingests objectForKey:@"surfaces"];
}

-(WorkspaceState*) currentWorkspace
{
	return [ingests objectForKey:@"current-workspace"];
}

-(NSDictionary*) felds
{
	return [ingests objectForKey:@"felds"];
}

-(Handi*) handipoint
{
	return [ingests objectForKey:@"handipoint"];
}

-(BOOL) m2mIsInstalled
{
	return [[ingests objectForKey:@"m2m-is-installed"] boolValue];
}

-(BOOL) permission
{
	return [[ingests objectForKey:@"permission"] boolValue];
}

-(BOOL) superuser
{
	return [[ingests objectForKey:@"superuser"] boolValue];
}

-(NSString*) username
{
	return [ingests objectForKey:@"username"];
}

-(BOOL) remoteAccessEnabled
{
	return [[ingests objectForKey:@"remote-access-enabled"] boolValue];
}

-(MezzMetadata*) mezzMetadata
{
	return [ingests objectForKey:@"mezz-metadata"];
}

-(NSArray*) pendingDialogs
{
	return [ingests objectForKey:@"pending-dialogs"];
}

@end



@implementation ClientJoinResponseDecline

-(BOOL) permission
{
	return [[ingests objectForKey:@"permission"] boolValue];
}

-(NSString*) reason
{
	return [ingests objectForKey:@"reason"];
}

@end



@implementation ClientLeaveRequest

-(instancetype) initWithPat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"client-leave",
			@"from:",
			pat,
		];
	}
	return self;
}

@end



@implementation DialogDisplayPsa

-(DialogItem*) dialog
{
	return [ingests objectForKey:@"dialog"];
}

@end



@implementation DialogDismissRequest

-(instancetype) initWithId:(NSString*)id value:(NSString*)value pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"dialog-dismiss",
			@"from:",
			pat,
		];

		ingests = @{
			@"id": id,
			@"value": value,
		};
	}
	return self;
}

-(NSString*) id
{
	return [ingests objectForKey:@"id"];
}

-(NSString*) value
{
	return [ingests objectForKey:@"value"];
}

@end



@implementation DialogDismissPsa

-(NSString*) id
{
	return [ingests objectForKey:@"id"];
}

-(NSString*) value
{
	return [ingests objectForKey:@"value"];
}

@end



@implementation HeartbeatRequest

-(instancetype) initWithJoined:(BOOL)joined pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"heartbeat",
			@"from:",
			pat,
		];

		ingests = @{
			@"joined": @(joined),
		};
	}
	return self;
}

-(BOOL) joined
{
	return [[ingests objectForKey:@"joined"] boolValue];
}

@end



@implementation HeartbeatPsa

@end



@implementation InfopresenceDetailPsa

-(NSString*) status
{
	return [ingests objectForKey:@"status"];
}

-(NSString*) remoteAccessStatus
{
	return [ingests objectForKey:@"remote-access-status"];
}

-(NSString*) remoteAccessUrl
{
	return [ingests objectForKey:@"remote-access-url"];
}

-(NSArray*) rooms
{
	return [ingests objectForKey:@"rooms"];
}

-(NSArray*) remoteParticipants
{
	return [ingests objectForKey:@"remote-participants"];
}

-(NSArray*) incomingCalls
{
	return [ingests objectForKey:@"incoming-calls"];
}

-(NSArray*) outgoingCalls
{
	return [ingests objectForKey:@"outgoing-calls"];
}

-(VideoChat*) videoChat
{
	return [ingests objectForKey:@"video-chat"];
}

-(BOOL) isForMagicM2m
{
	return [[ingests objectForKey:@"is-for-magic-m2m"] boolValue];
}

@end



@implementation InfopresenceDetailRequest

-(instancetype) initWithPat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"infopresence-detail",
			@"from:",
			pat,
		];
	}
	return self;
}

@end



@implementation InfopresenceIncomingPsa

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

-(NSString*) resolution
{
	return [ingests objectForKey:@"resolution"];
}

@end



@implementation InfopresenceIncomingRequest

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

-(NSInteger) receivedUtc
{
	return [[ingests objectForKey:@"received-utc"] integerValue];
}

@end



@implementation InfopresenceIncomingResponse

-(instancetype) initWithUid:(NSString*)uid accept:(BOOL)accept pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"infopresence-incoming-request-resolve",
			@"from:",
			pat,
		];

		ingests = @{
			@"uid": uid,
			@"accept": @(accept),
		};
	}
	return self;
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

-(BOOL) accept
{
	return [[ingests objectForKey:@"accept"] boolValue];
}

@end



@implementation InfopresenceLeaveRequest

-(instancetype) initWithInterrupted:(BOOL)interrupted pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"infopresence-leave",
			@"from:",
			pat,
		];

		ingests = @{
			@"interrupted": @(interrupted),
		};
	}
	return self;
}

-(BOOL) interrupted
{
	return [[ingests objectForKey:@"interrupted"] boolValue];
}

@end



@implementation InfopresenceOutgoingCancelRequest

-(instancetype) initWithPat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"infopresence-outgoing-request-cancel",
			@"from:",
			pat,
		];
	}
	return self;
}

@end



@implementation InfopresenceOutgoingPsa

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

-(NSInteger) sentUtc
{
	return [[ingests objectForKey:@"sent-utc"] integerValue];
}

@end



@implementation InfopresenceOutgoingRequest

-(instancetype) initWithUid:(NSString*)uid sentUtc:(NSInteger)sentUtc pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"infopresence-outgoing-request",
			@"from:",
			pat,
		];

		ingests = @{
			@"uid": uid,
			@"sent-utc": @(sentUtc),
		};
	}
	return self;
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

-(NSInteger) sentUtc
{
	return [[ingests objectForKey:@"sent-utc"] integerValue];
}

@end



@implementation InfopresenceOutgoingError

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

-(NSString*) errorDescription
{
	return [ingests objectForKey:@"description"];
}

-(NSString*) summary
{
	return [ingests objectForKey:@"summary"];
}

@end



@implementation InfopresenceOutgoingResponsePsa

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

-(NSString*) resolution
{
	return [ingests objectForKey:@"resolution"];
}

@end



@implementation InfopresenceOutgoingInviteRequest

-(instancetype) initWithUid:(NSString*)uid pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"infopresence-outgoing-invite",
			@"from:",
			pat,
		];

		ingests = @{
			@"uid": uid,
		};
	}
	return self;
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

@end



@implementation InfopresenceOutgoingInviteErrorResponse

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

-(NSString*) errorDescription
{
	return [ingests objectForKey:@"description"];
}

-(NSString*) summary
{
	return [ingests objectForKey:@"summary"];
}

@end



@implementation InfopresenceOutgoingInvitePsa

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

-(NSInteger) sentUtc
{
	return [[ingests objectForKey:@"sent-utc"] integerValue];
}

@end



@implementation InfopresenceOutgoingInviteCancelRequest

-(instancetype) initWithUid:(NSString*)uid pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"infopresence-outgoing-invite-cancel",
			@"from:",
			pat,
		];

		ingests = @{
			@"uid": uid,
		};
	}
	return self;
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

@end



@implementation InfopresenceOutgoingInviteResolvePsa

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

-(NSString*) resolution
{
	return [ingests objectForKey:@"resolution"];
}

@end



@implementation InfopresenceIncomingInvitePsa

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

-(NSInteger) receivedUtc
{
	return [[ingests objectForKey:@"received-utc"] integerValue];
}

@end



@implementation InfopresenceIncomingInviteResolveRequest

-(instancetype) initWithUid:(NSString*)uid accept:(BOOL)accept pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"infopresence-incoming-invite-resolve",
			@"from:",
			pat,
		];

		ingests = @{
			@"uid": uid,
			@"accept": @(accept),
		};
	}
	return self;
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

-(BOOL) accept
{
	return [[ingests objectForKey:@"accept"] boolValue];
}

@end



@implementation InfopresenceIncomingInviteResolvePsa

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

-(NSString*) resolution
{
	return [ingests objectForKey:@"resolution"];
}

@end



@implementation InfopresenceParticipantJoinPsa

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

-(NSString*) displayName
{
	return [ingests objectForKey:@"display-name"];
}

-(NSString*) type
{
	return [ingests objectForKey:@"type"];
}

@end



@implementation InfopresenceParticipantLeavePsa

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

-(NSString*) displayName
{
	return [ingests objectForKey:@"display-name"];
}

-(NSString*) type
{
	return [ingests objectForKey:@"type"];
}

@end



@implementation LiveStreamCreate

-(instancetype) initWithUri:(NSString*)uri id:(NSString*)id pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"live-stream-create",
			@"from:",
			pat,
		];

		ingests = @{
			@"uri": uri,
			@"id": id,
		};
	}
	return self;
}

-(NSString*) uri
{
	return [ingests objectForKey:@"uri"];
}

-(NSString*) id
{
	return [ingests objectForKey:@"id"];
}

@end



@implementation LiveStreamCreateResponse

-(NSString*) contentSource
{
	return [ingests objectForKey:@"content-source"];
}

@end



@implementation LiveStreamCreateError

-(NSString*) errorDescription
{
	return [ingests objectForKey:@"description"];
}

@end



@implementation LiveStreamDelete

-(instancetype) initWithContentSource:(NSString*)contentSource pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"live-stream-delete",
			@"from:",
			pat,
		];

		ingests = @{
			@"content-source": contentSource,
		};
	}
	return self;
}

-(NSString*) contentSource
{
	return [ingests objectForKey:@"content-source"];
}

@end



@implementation LiveStreamDeleteResponse

-(NSString*) message
{
	return [ingests objectForKey:@"message"];
}

@end



@implementation LiveStreamDeleteError

-(NSString*) errorDescription
{
	return [ingests objectForKey:@"description"];
}

@end



@implementation LiveStreamAppearPsa

-(NSString*) contentSource
{
	return [ingests objectForKey:@"content-source"];
}

-(NSString*) mezzUid
{
	return [ingests objectForKey:@"mezz-uid"];
}

@end



@implementation LiveStreamDetailPsa

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSArray*) liveStreams
{
	return [ingests objectForKey:@"live-streams"];
}

@end



@implementation LiveStreamDetailRequest

-(instancetype) initWithPat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"live-streams-detail",
			@"from:",
			pat,
		];
	}
	return self;
}

@end



@implementation LiveStreamDetailResponse

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSArray*) liveStreams
{
	return [ingests objectForKey:@"live-streams"];
}

@end



@implementation LiveStreamDissapearPsa

-(NSString*) contentSource
{
	return [ingests objectForKey:@"content-source"];
}

@end



@implementation LiveStreamGrabPsa

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

@end



@implementation LiveStreamGrabError

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

-(NSString*) errorDescription
{
	return [ingests objectForKey:@"description"];
}

-(NSString*) errorCode
{
	return [ingests objectForKey:@"error-code"];
}

-(NSString*) summary
{
	return [ingests objectForKey:@"summary"];
}

@end



@implementation LiveStreamGrabRequest

-(instancetype) initWithWorkspaceUid:(NSString*)workspaceUid uid:(NSString*)uid pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"live-stream-grab",
			@"from:",
			pat,
		];

		ingests = @{
			@"workspace-uid": workspaceUid,
			@"uid": uid,
		};
	}
	return self;
}

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

@end



@implementation LiveStreamGrabResponse

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

@end



@implementation LiveStreamRelinquishPsa

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

@end



@implementation LiveStreamRelinquishError

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

-(NSString*) errorDescription
{
	return [ingests objectForKey:@"description"];
}

-(NSString*) errorCode
{
	return [ingests objectForKey:@"error-code"];
}

-(NSString*) summary
{
	return [ingests objectForKey:@"summary"];
}

@end



@implementation LiveStreamRelinquishRequest

-(instancetype) initWithWorkspaceUid:(NSString*)workspaceUid uid:(NSString*)uid pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"live-stream-relinquish",
			@"from:",
			pat,
		];

		ingests = @{
			@"workspace-uid": workspaceUid,
			@"uid": uid,
		};
	}
	return self;
}

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

@end



@implementation LiveStreamRelinquishResponse

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

@end



@implementation LiveStreamRefreshPsa

-(NSString*) contentSource
{
	return [ingests objectForKey:@"content-source"];
}

-(ImageSpec*) image
{
	return [ingests objectForKey:@"image"];
}

-(ImageSpec*) thumbnailImage
{
	return [ingests objectForKey:@"thumbnail-image"];
}

@end



@implementation ClientSignInError

-(NSString*) errorDescription
{
	return [ingests objectForKey:@"description"];
}

-(NSString*) errorCode
{
	return [ingests objectForKey:@"error-code"];
}

-(NSString*) summary
{
	return [ingests objectForKey:@"summary"];
}

@end



@implementation ClientSignInRequest

-(instancetype) initWithUsername:(NSString*)username password:(NSString*)password pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"client-sign-in",
			@"from:",
			pat,
		];

		ingests = @{
			@"username": username,
			@"password": password,
		};
	}
	return self;
}

-(NSString*) username
{
	return [ingests objectForKey:@"username"];
}

-(NSString*) password
{
	return [ingests objectForKey:@"password"];
}

@end



@implementation ClientSignInResponse

-(BOOL) superuser
{
	return [[ingests objectForKey:@"superuser"] boolValue];
}

-(NSString*) username
{
	return [ingests objectForKey:@"username"];
}

@end



@implementation ClientSignOutPm

@end



@implementation ClientSignOutRequest

-(instancetype) initWithPat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"client-sign-out",
			@"from:",
			pat,
		];
	}
	return self;
}

@end



@implementation ClientSignOutResponse

@end



@implementation MeetingEndPsa

-(NSString*) meetingUid
{
	return [ingests objectForKey:@"meeting-uid"];
}

@end



@implementation MeetingEndError

-(NSString*) meetingUid
{
	return [ingests objectForKey:@"meeting-uid"];
}

-(NSString*) errorDescription
{
	return [ingests objectForKey:@"description"];
}

-(NSString*) errorCode
{
	return [ingests objectForKey:@"error-code"];
}

-(NSString*) summary
{
	return [ingests objectForKey:@"summary"];
}

@end



@implementation MeetingEndRequest

-(instancetype) initWithMeetingUid:(NSString*)meetingUid pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"meeting-end",
			@"from:",
			pat,
		];

		ingests = @{
			@"meeting-uid": meetingUid,
		};
	}
	return self;
}

-(NSString*) meetingUid
{
	return [ingests objectForKey:@"meeting-uid"];
}

@end



@implementation MeetingJoinUrlRequest

-(instancetype) initWithMezzinUrl:(NSString*)mezzinUrl isForMagicM2m:(BOOL)isForMagicM2m pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"meeting-join-url",
			@"from:",
			pat,
		];

		ingests = @{
			@"mezzin-url": mezzinUrl,
			@"is-for-magic-m2m": @(isForMagicM2m),
		};
	}
	return self;
}

-(NSString*) mezzinUrl
{
	return [ingests objectForKey:@"mezzin-url"];
}

-(BOOL) isForMagicM2m
{
	return [[ingests objectForKey:@"is-for-magic-m2m"] boolValue];
}

@end



@implementation MeetingListPsa

-(NSArray*) meetingList
{
	return [ingests objectForKey:@"meeting-list"];
}

-(CalendarMeeting*) inProgressMeeting
{
	return [ingests objectForKey:@"in-progress-meeting"];
}

@end



@implementation MeetingListRequest

-(instancetype) initWithPat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"meeting-list",
			@"from:",
			pat,
		];
	}
	return self;
}

@end



@implementation MeetingStartPsa

-(NSString*) meetingUid
{
	return [ingests objectForKey:@"meeting-uid"];
}

-(CalendarMeeting*) meeting
{
	return [ingests objectForKey:@"meeting"];
}

@end



@implementation MeetingStartError

-(NSString*) meetingUid
{
	return [ingests objectForKey:@"meeting-uid"];
}

-(NSString*) errorDescription
{
	return [ingests objectForKey:@"description"];
}

-(NSString*) errorCode
{
	return [ingests objectForKey:@"error-code"];
}

-(NSString*) summary
{
	return [ingests objectForKey:@"summary"];
}

@end



@implementation MeetingStartRequest

-(instancetype) initWithMeetingUid:(NSString*)meetingUid isForMagicM2m:(BOOL)isForMagicM2m pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"meeting-start",
			@"from:",
			pat,
		];

		ingests = @{
			@"meeting-uid": meetingUid,
			@"is-for-magic-m2m": @(isForMagicM2m),
		};
	}
	return self;
}

-(NSString*) meetingUid
{
	return [ingests objectForKey:@"meeting-uid"];
}

-(BOOL) isForMagicM2m
{
	return [[ingests objectForKey:@"is-for-magic-m2m"] boolValue];
}

@end



@implementation MeetingReadyPsa

-(NSString*) meetingUid
{
	return [ingests objectForKey:@"meeting-uid"];
}

-(CalendarMeeting*) meeting
{
	return [ingests objectForKey:@"meeting"];
}

@end



@implementation MeetingAbortedPsa

-(NSString*) meetingUid
{
	return [ingests objectForKey:@"meeting-uid"];
}

-(CalendarMeeting*) meeting
{
	return [ingests objectForKey:@"meeting"];
}

@end



@implementation MezCapsPsa

-(AssetUploadLimits*) assetUploadLimits
{
	return [ingests objectForKey:@"asset-upload-limits"];
}

-(NSArray*) whiteboards
{
	return [ingests objectForKey:@"whiteboards"];
}

-(NSDictionary*) felds
{
	return [ingests objectForKey:@"felds"];
}

-(NSArray*) surfaces
{
	return [ingests objectForKey:@"surfaces"];
}

-(BOOL) remoteAccessEnabled
{
	return [[ingests objectForKey:@"remote-access-enabled"] boolValue];
}

@end



@implementation MezCapsRequest

-(instancetype) initWithPat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"mez-caps",
			@"from:",
			pat,
		];
	}
	return self;
}

@end



@implementation MezCapsResponse

-(AssetUploadLimits*) assetUploadLimits
{
	return [ingests objectForKey:@"asset-upload-limits"];
}

-(NSArray*) whiteboards
{
	return [ingests objectForKey:@"whiteboards"];
}

-(NSArray*) surfaces
{
	return [ingests objectForKey:@"surfaces"];
}

@end



@implementation MezzMetadataRequest

-(instancetype) initWithPat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"mezz-metadata",
			@"from:",
			pat,
		];
	}
	return self;
}

@end



@implementation MezzMetadataResponse

-(BOOL) cloudInstance
{
	return [[ingests objectForKey:@"cloud-instance"] boolValue];
}

-(FeatureToggles*) featureToggles
{
	return [ingests objectForKey:@"feature-toggles"];
}

-(NSString*) apiVersion
{
	return [ingests objectForKey:@"api-version"];
}

-(BOOL) systemUseNotification
{
	return [[ingests objectForKey:@"system-use-notification"] boolValue];
}

-(NSString*) systemUseNotificationText
{
	return [ingests objectForKey:@"system-use-notification-text"];
}

-(NSString*) model
{
	return [ingests objectForKey:@"model"];
}

-(BOOL) ocelotSystem
{
	return [[ingests objectForKey:@"ocelot-system"] boolValue];
}

@end



@implementation OhayoGoyaimasuPsa

@end



@implementation OutgoingInfopresenceSessionRequest

-(instancetype) initWithPat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"outgoing-infopresence-session-request",
			@"from:",
			pat,
		];
	}
	return self;
}

@end



@implementation PasskeyDetailRequest

-(instancetype) initWithPat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"passkey-detail",
			@"from:",
			pat,
		];
	}
	return self;
}

@end



@implementation PasskeyDetailResponse

-(NSString*) clientConnectionUrl
{
	return [ingests objectForKey:@"client-connection-url"];
}

-(BOOL) isPasskeyProtected
{
	return [[ingests objectForKey:@"is-passkey-protected"] boolValue];
}

-(NSString*) passkey
{
	return [ingests objectForKey:@"passkey"];
}

@end



@implementation PasskeyDisablePsa

@end



@implementation PasskeyDisableRequest

-(instancetype) initWithPat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"passkey-disable",
			@"from:",
			pat,
		];
	}
	return self;
}

@end



@implementation PasskeyEnablePsa

-(NSString*) exempt
{
	return [ingests objectForKey:@"exempt"];
}

@end



@implementation PasskeyEnableRequest

-(instancetype) initWithPat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"passkey-enable",
			@"from:",
			pat,
		];
	}
	return self;
}

@end



@implementation PortfolioDetailPsa

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSArray*) portfolioItems
{
	return [ingests objectForKey:@"portfolio-items"];
}

@end



@implementation PortfolioDetailRequest

-(instancetype) initWithWorkspaceUid:(NSString*)workspaceUid pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"portfolio-detail",
			@"from:",
			pat,
		];

		ingests = @{
			@"workspace-uid": workspaceUid,
		};
	}
	return self;
}

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

@end



@implementation PortfolioDetailResponse

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSArray*) portfolioItems
{
	return [ingests objectForKey:@"portfolio-items"];
}

@end



@implementation PortfolioDownloadError

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSString*) errorDescription
{
	return [ingests objectForKey:@"description"];
}

-(NSString*) errorCode
{
	return [ingests objectForKey:@"error-code"];
}

-(NSString*) summary
{
	return [ingests objectForKey:@"summary"];
}

@end



@implementation PortfolioDownloadRequest

-(instancetype) initWithWorkspaceUid:(NSString*)workspaceUid pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"portfolio-download",
			@"from:",
			pat,
		];

		ingests = @{
			@"workspace-uid": workspaceUid,
		};
	}
	return self;
}

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

@end



@implementation PortfolioDownloadResponse

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSString*) downloadUrl
{
	return [ingests objectForKey:@"download-url"];
}

@end



@implementation PortfolioItemDeleteRequest

-(instancetype) initWithWorkspaceUid:(NSString*)workspaceUid uid:(NSString*)uid pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"portfolio-item-delete",
			@"from:",
			pat,
		];

		ingests = @{
			@"workspace-uid": workspaceUid,
			@"uid": uid,
		};
	}
	return self;
}

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

@end



@implementation PortfolioItemDeleteError

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

-(NSString*) errorDescription
{
	return [ingests objectForKey:@"description"];
}

-(NSString*) errorCode
{
	return [ingests objectForKey:@"error-code"];
}

-(NSString*) summary
{
	return [ingests objectForKey:@"summary"];
}

@end



@implementation PortfolioItemDeletePsa

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

@end



@implementation PortfolioClearPsa

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

@end



@implementation PortfolioClearRequest

-(instancetype) initWithWorkspaceUid:(NSString*)workspaceUid pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"portfolio-clear",
			@"from:",
			pat,
		];

		ingests = @{
			@"workspace-uid": workspaceUid,
		};
	}
	return self;
}

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

@end



@implementation PortfolioClearError

-(NSString*) errorDescription
{
	return [ingests objectForKey:@"description"];
}

-(NSString*) summary
{
	return [ingests objectForKey:@"summary"];
}

@end



@implementation PortfolioItemGrabPsa

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

@end



@implementation PortfolioItemGrabError

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

-(NSString*) errorDescription
{
	return [ingests objectForKey:@"description"];
}

-(NSString*) errorCode
{
	return [ingests objectForKey:@"error-code"];
}

-(NSString*) summary
{
	return [ingests objectForKey:@"summary"];
}

@end



@implementation PortfolioItemGrabRequest

-(instancetype) initWithWorkspaceUid:(NSString*)workspaceUid uid:(NSString*)uid pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"portfolio-item-grab",
			@"from:",
			pat,
		];

		ingests = @{
			@"workspace-uid": workspaceUid,
			@"uid": uid,
		};
	}
	return self;
}

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

@end



@implementation PortfolioItemGrabResponse

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

@end



@implementation PortfolioItemInsertPsa

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

-(NSString*) contentSource
{
	return [ingests objectForKey:@"content-source"];
}

-(NSString*) displayName
{
	return [ingests objectForKey:@"display-name"];
}

-(NSInteger) index
{
	return [[ingests objectForKey:@"index"] integerValue];
}

-(NSString*) thumbUri
{
	return [ingests objectForKey:@"thumb-uri"];
}

-(NSString*) largeUri
{
	return [ingests objectForKey:@"large-uri"];
}

-(NSString*) smallUri
{
	return [ingests objectForKey:@"small-uri"];
}

-(NSString*) mediumUri
{
	return [ingests objectForKey:@"medium-uri"];
}

-(NSString*) feldRelativeUri
{
	return [ingests objectForKey:@"feld-relative-uri"];
}

-(NSString*) fullUri
{
	return [ingests objectForKey:@"full-uri"];
}

-(BOOL) imageAvailable
{
	return [[ingests objectForKey:@"image-available"] boolValue];
}

-(NSString*) feldInscribedUri
{
	return [ingests objectForKey:@"feld-inscribed-uri"];
}

-(BOOL) origImageAvailable
{
	return [[ingests objectForKey:@"orig-image-available"] boolValue];
}

@end



@implementation PortfolioItemInsertRequest

-(instancetype) initWithWorkspaceUid:(NSString*)workspaceUid contentSource:(NSString*)contentSource index:(NSInteger)index pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"portfolio-item-insert",
			@"from:",
			pat,
		];

		ingests = @{
			@"workspace-uid": workspaceUid,
			@"content-source": contentSource,
			@"index": @(index),
		};
	}
	return self;
}

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSString*) contentSource
{
	return [ingests objectForKey:@"content-source"];
}

-(NSInteger) index
{
	return [[ingests objectForKey:@"index"] integerValue];
}

@end



@implementation PortfolioItemInsertError

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSString*) contentSource
{
	return [ingests objectForKey:@"content-source"];
}

-(NSString*) errorDescription
{
	return [ingests objectForKey:@"description"];
}

-(NSInteger) index
{
	return [[ingests objectForKey:@"index"] integerValue];
}

-(NSString*) summary
{
	return [ingests objectForKey:@"summary"];
}

@end



@implementation PortfolioItemRelinquishPsa

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

@end



@implementation PortfolioItemRelinquishError

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

-(NSString*) errorDescription
{
	return [ingests objectForKey:@"description"];
}

-(NSString*) errorCode
{
	return [ingests objectForKey:@"error-code"];
}

-(NSString*) summary
{
	return [ingests objectForKey:@"summary"];
}

@end



@implementation PortfolioItemRelinquishRequest

-(instancetype) initWithWorkspaceUid:(NSString*)workspaceUid uid:(NSString*)uid pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"portfolio-item-relinquish",
			@"from:",
			pat,
		];

		ingests = @{
			@"workspace-uid": workspaceUid,
			@"uid": uid,
		};
	}
	return self;
}

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

@end



@implementation PortfolioItemRelinquishResponse

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

@end



@implementation PortfolioItemReorderPsa

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

-(NSInteger) index
{
	return [[ingests objectForKey:@"index"] integerValue];
}

@end



@implementation PortfolioItemReorderError

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

-(NSInteger) index
{
	return [[ingests objectForKey:@"index"] integerValue];
}

-(NSString*) errorDescription
{
	return [ingests objectForKey:@"description"];
}

-(NSString*) summary
{
	return [ingests objectForKey:@"summary"];
}

@end



@implementation PortfolioItemReorderRequest

-(instancetype) initWithWorkspaceUid:(NSString*)workspaceUid uid:(NSString*)uid index:(NSInteger)index pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"portfolio-item-reorder",
			@"from:",
			pat,
		];

		ingests = @{
			@"workspace-uid": workspaceUid,
			@"uid": uid,
			@"index": @(index),
		};
	}
	return self;
}

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

-(NSInteger) index
{
	return [[ingests objectForKey:@"index"] integerValue];
}

@end



@implementation PresentationDetailPsa

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(BOOL) active
{
	return [[ingests objectForKey:@"active"] boolValue];
}

-(NSInteger) currentIndex
{
	return [[ingests objectForKey:@"current-index"] integerValue];
}

@end



@implementation PresentationScrollRequest

-(instancetype) initWithWorkspaceUid:(NSString*)workspaceUid currentIndex:(NSInteger)currentIndex pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"presentation-scroll",
			@"from:",
			pat,
		];

		ingests = @{
			@"workspace-uid": workspaceUid,
			@"current-index": @(currentIndex),
		};
	}
	return self;
}

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSInteger) currentIndex
{
	return [[ingests objectForKey:@"current-index"] integerValue];
}

@end



@implementation PresentationStartRequest

-(instancetype) initWithWorkspaceUid:(NSString*)workspaceUid currentIndex:(NSInteger)currentIndex pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"presentation-start",
			@"from:",
			pat,
		];

		ingests = @{
			@"workspace-uid": workspaceUid,
			@"current-index": @(currentIndex),
		};
	}
	return self;
}

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSInteger) currentIndex
{
	return [[ingests objectForKey:@"current-index"] integerValue];
}

@end



@implementation PresentationStopRequest

-(instancetype) initWithWorkspaceUid:(NSString*)workspaceUid pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"presentation-stop",
			@"from:",
			pat,
		];

		ingests = @{
			@"workspace-uid": workspaceUid,
		};
	}
	return self;
}

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

@end



@implementation ProfileDetailPsa

-(MezzDetail*) profile
{
	return [ingests objectForKey:@"profile"];
}

@end



@implementation ProfileDetailRequest

-(instancetype) initWithPat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"profile-detail",
			@"from:",
			pat,
		];
	}
	return self;
}

@end



@implementation RatchetError

-(NSString*) errorDescription
{
	return [ingests objectForKey:@"description"];
}

-(NSString*) errorCode
{
	return [ingests objectForKey:@"error-code"];
}

-(NSString*) summary
{
	return [ingests objectForKey:@"summary"];
}

@end



@implementation RatchetResponse

-(NSString*) ratchetState
{
	return [ingests objectForKey:@"ratchet-state"];
}

@end



@implementation RatchetRequest

-(instancetype) initWithRatchetState:(NSString*)ratchetState pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"ratchet",
			@"from:",
			pat,
			@"to:",
			@"native-mezz",
		];

		ingests = @{
			@"ratchet-state": ratchetState,
		};
	}
	return self;
}

-(NSString*) ratchetState
{
	return [ingests objectForKey:@"ratchet-state"];
}

@end



@implementation InfopresenceEnableRemoteAccessRequest

-(instancetype) initWithPat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"infopresence-enable-remote-access",
			@"from:",
			pat,
			@"to:",
			@"native-mezz",
		];
	}
	return self;
}

@end



@implementation InfopresenceEnableRemoteAccessErrorResponse

-(NSString*) errorDescription
{
	return [ingests objectForKey:@"description"];
}

-(NSString*) errorCode
{
	return [ingests objectForKey:@"error-code"];
}

-(NSString*) summary
{
	return [ingests objectForKey:@"summary"];
}

@end



@implementation InfopresenceDisableRemoteAccessRequest

-(instancetype) initWithPat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"infopresence-disable-remote-access",
			@"from:",
			pat,
			@"to:",
			@"native-mezz",
		];
	}
	return self;
}

@end



@implementation InfopresenceDisableRemoteAccessErrorResponse

-(NSString*) errorDescription
{
	return [ingests objectForKey:@"description"];
}

-(NSString*) errorCode
{
	return [ingests objectForKey:@"error-code"];
}

-(NSString*) summary
{
	return [ingests objectForKey:@"summary"];
}

@end



@implementation PassforwardRequest

-(instancetype) initWithSpaceCntx:(NSString*)spaceCntx curAim:(MZVect*)curAim curOrigin:(MZVect*)curOrigin pressureReports:(PressureReport*)pressureReports pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"passforward",
			@"from:",
			pat.provenance,
		];

		ingests = @{
			@"space-cntx": spaceCntx,
			@"cur-aim": curAim,
			@"cur-origin": curOrigin,
			@"pressure-reports": pressureReports,
		};
	}
	return self;
}

-(NSString*) spaceCntx
{
	return [ingests objectForKey:@"space-cntx"];
}

-(MZVect*) curAim
{
	return [ingests objectForKey:@"cur-aim"];
}

-(MZVect*) curOrigin
{
	return [ingests objectForKey:@"cur-origin"];
}

-(PressureReport*) pressureReports
{
	return [ingests objectForKey:@"pressure-reports"];
}

@end



@implementation RemoteMezzaninesDetailPsa

-(NSArray*) remoteMezzanines
{
	return [ingests objectForKey:@"remote-mezzanines"];
}

@end



@implementation RemoteMezzaninesDetailRequest

-(instancetype) initWithPat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"remote-mezzanines-detail",
			@"from:",
			pat,
		];
	}
	return self;
}

@end



@implementation RemoteSessionEndPsa

@end



@implementation StorageDetailPsa

-(BOOL) lowStorage
{
	return [[ingests objectForKey:@"low-storage"] boolValue];
}

@end



@implementation StorageDetailRequest

-(instancetype) initWithPat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"storage-detail",
			@"from:",
			pat,
		];
	}
	return self;
}

@end



@implementation WhiteboardCaptureError

-(NSString*) errorDescription
{
	return [ingests objectForKey:@"description"];
}

-(NSString*) errorCode
{
	return [ingests objectForKey:@"error-code"];
}

-(NSString*) summary
{
	return [ingests objectForKey:@"summary"];
}

@end



@implementation WhiteboardCaptureRequest

-(instancetype) initWithUid:(NSString*)uid workspaceUid:(NSString*)workspaceUid pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"whiteboard-capture",
			@"from:",
			pat,
		];

		ingests = @{
			@"uid": uid,
			@"workspace-uid": workspaceUid,
		};
	}
	return self;
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

@end



@implementation WhiteboardCaptureResponse

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

@end



@implementation WindshieldArrangeError

-(NSString*) errorDescription
{
	return [ingests objectForKey:@"description"];
}

-(NSString*) errorCode
{
	return [ingests objectForKey:@"error-code"];
}

-(NSString*) summary
{
	return [ingests objectForKey:@"summary"];
}

@end



@implementation WindshieldArrangeRequest

-(instancetype) initWithWorkspaceUid:(NSString*)workspaceUid surfaces:(NSArray*)surfaces pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"windshield-arrange",
			@"from:",
			pat,
		];

		ingests = @{
			@"workspace-uid": workspaceUid,
			@"surfaces": surfaces,
		};
	}
	return self;
}

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSArray*) surfaces
{
	return [ingests objectForKey:@"surfaces"];
}

@end



@implementation WindshieldClearError

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSString*) errorDescription
{
	return [ingests objectForKey:@"description"];
}

-(NSString*) errorCode
{
	return [ingests objectForKey:@"error-code"];
}

-(NSString*) summary
{
	return [ingests objectForKey:@"summary"];
}

@end



@implementation WindshieldClearPsa

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSArray*) surfaces
{
	return [ingests objectForKey:@"surfaces"];
}

@end



@implementation WindshieldClearRequest

-(instancetype) initWithWorkspaceUid:(NSString*)workspaceUid surfaces:(NSArray*)surfaces pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"windshield-clear",
			@"from:",
			pat,
		];

		ingests = @{
			@"workspace-uid": workspaceUid,
			@"surfaces": surfaces,
		};
	}
	return self;
}

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSArray*) surfaces
{
	return [ingests objectForKey:@"surfaces"];
}

@end



@implementation WindshieldDetailPsa

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSArray*) windshieldItems
{
	return [ingests objectForKey:@"windshield-items"];
}

@end



@implementation WindshieldDetailRequest

-(instancetype) initWithWorkspaceUid:(NSString*)workspaceUid pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"windshield-detail",
			@"from:",
			pat,
		];

		ingests = @{
			@"workspace-uid": workspaceUid,
		};
	}
	return self;
}

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

@end



@implementation WindshieldItemCreateError

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSString*) errorDescription
{
	return [ingests objectForKey:@"description"];
}

-(NSString*) errorCode
{
	return [ingests objectForKey:@"error-code"];
}

-(NSString*) summary
{
	return [ingests objectForKey:@"summary"];
}

@end



@implementation WindshieldItemCreatePsa

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

-(NSString*) contentSource
{
	return [ingests objectForKey:@"content-source"];
}

-(NSString*) displayName
{
	return [ingests objectForKey:@"display-name"];
}

-(NSString*) surfaceName
{
	return [ingests objectForKey:@"surface-name"];
}

-(FeldRelativeCoords*) feldRelativeCoords
{
	return [ingests objectForKey:@"feld-relative-coords"];
}

-(FeldRelativeSize*) feldRelativeSize
{
	return [ingests objectForKey:@"feld-relative-size"];
}

-(NSString*) thumbUri
{
	return [ingests objectForKey:@"thumb-uri"];
}

-(NSString*) largeUri
{
	return [ingests objectForKey:@"large-uri"];
}

-(NSString*) smallUri
{
	return [ingests objectForKey:@"small-uri"];
}

-(NSString*) mediumUri
{
	return [ingests objectForKey:@"medium-uri"];
}

-(NSString*) feldRelativeUri
{
	return [ingests objectForKey:@"feld-relative-uri"];
}

-(NSString*) fullUri
{
	return [ingests objectForKey:@"full-uri"];
}

-(BOOL) imageAvailable
{
	return [[ingests objectForKey:@"image-available"] boolValue];
}

-(NSString*) feldInscribedUri
{
	return [ingests objectForKey:@"feld-inscribed-uri"];
}

-(BOOL) origImageAvailable
{
	return [[ingests objectForKey:@"orig-image-available"] boolValue];
}

@end



@implementation WindshieldItemCreateRequest

-(instancetype) initWithWorkspaceUid:(NSString*)workspaceUid contentSource:(NSString*)contentSource surfaceName:(NSString*)surfaceName feldRelativeCoords:(FeldRelativeCoords*)feldRelativeCoords feldRelativeSize:(FeldRelativeSize*)feldRelativeSize pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"windshield-item-create",
			@"from:",
			pat,
		];

		ingests = @{
			@"workspace-uid": workspaceUid,
			@"content-source": contentSource,
			@"surface-name": surfaceName,
			@"feld-relative-coords": feldRelativeCoords,
			@"feld-relative-size": feldRelativeSize,
		};
	}
	return self;
}

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSString*) contentSource
{
	return [ingests objectForKey:@"content-source"];
}

-(NSString*) surfaceName
{
	return [ingests objectForKey:@"surface-name"];
}

-(FeldRelativeCoords*) feldRelativeCoords
{
	return [ingests objectForKey:@"feld-relative-coords"];
}

-(FeldRelativeSize*) feldRelativeSize
{
	return [ingests objectForKey:@"feld-relative-size"];
}

@end



@implementation WindshieldItemDeleteError

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSString*) errorDescription
{
	return [ingests objectForKey:@"description"];
}

-(NSString*) errorCode
{
	return [ingests objectForKey:@"error-code"];
}

-(NSString*) summary
{
	return [ingests objectForKey:@"summary"];
}

@end



@implementation WindshieldItemDeletePsa

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

@end



@implementation WindshieldItemDeleteRequest

-(instancetype) initWithWorkspaceUid:(NSString*)workspaceUid uid:(NSString*)uid pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"windshield-item-delete",
			@"from:",
			pat,
		];

		ingests = @{
			@"workspace-uid": workspaceUid,
			@"uid": uid,
		};
	}
	return self;
}

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

@end



@implementation WindshieldItemGrabError

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

-(NSString*) errorDescription
{
	return [ingests objectForKey:@"description"];
}

-(NSString*) errorCode
{
	return [ingests objectForKey:@"error-code"];
}

-(NSString*) summary
{
	return [ingests objectForKey:@"summary"];
}

@end



@implementation WindshieldItemGrabPsa

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

@end



@implementation WindshieldItemGrabRequest

-(instancetype) initWithWorkspaceUid:(NSString*)workspaceUid uid:(NSString*)uid pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"windshield-item-grab",
			@"from:",
			pat,
		];

		ingests = @{
			@"workspace-uid": workspaceUid,
			@"uid": uid,
		};
	}
	return self;
}

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

@end



@implementation WindshieldItemGrabResponse

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

@end



@implementation WindshieldItemTransformError

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSString*) errorDescription
{
	return [ingests objectForKey:@"description"];
}

-(NSString*) errorCode
{
	return [ingests objectForKey:@"error-code"];
}

-(NSString*) summary
{
	return [ingests objectForKey:@"summary"];
}

@end



@implementation WindshieldItemTransformPsa

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

-(NSString*) surfaceName
{
	return [ingests objectForKey:@"surface-name"];
}

-(FeldRelativeCoords*) feldRelativeCoords
{
	return [ingests objectForKey:@"feld-relative-coords"];
}

-(FeldRelativeSize*) feldRelativeSize
{
	return [ingests objectForKey:@"feld-relative-size"];
}

-(BOOL) moveToFront
{
	return [[ingests objectForKey:@"move-to-front"] boolValue];
}

@end



@implementation WindshieldItemTransformRequest

-(instancetype) initWithWorkspaceUid:(NSString*)workspaceUid uid:(NSString*)uid surfaceName:(NSString*)surfaceName feldRelativeCoords:(FeldRelativeCoords*)feldRelativeCoords feldRelativeSize:(FeldRelativeSize*)feldRelativeSize pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"windshield-item-transform",
			@"from:",
			pat,
		];

		ingests = @{
			@"workspace-uid": workspaceUid,
			@"uid": uid,
			@"surface-name": surfaceName,
			@"feld-relative-coords": feldRelativeCoords,
			@"feld-relative-size": feldRelativeSize,
		};
	}
	return self;
}

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

-(NSString*) surfaceName
{
	return [ingests objectForKey:@"surface-name"];
}

-(FeldRelativeCoords*) feldRelativeCoords
{
	return [ingests objectForKey:@"feld-relative-coords"];
}

-(FeldRelativeSize*) feldRelativeSize
{
	return [ingests objectForKey:@"feld-relative-size"];
}

@end



@implementation WindshieldItemRelinquishPsa

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

@end



@implementation WindshieldItemRelinquishError

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

-(NSString*) errorDescription
{
	return [ingests objectForKey:@"description"];
}

-(NSString*) errorCode
{
	return [ingests objectForKey:@"error-code"];
}

-(NSString*) summary
{
	return [ingests objectForKey:@"summary"];
}

@end



@implementation WindshieldItemRelinquishRequest

-(instancetype) initWithWorkspaceUid:(NSString*)workspaceUid uid:(NSString*)uid pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"windshield-item-relinquish",
			@"from:",
			pat,
		];

		ingests = @{
			@"workspace-uid": workspaceUid,
			@"uid": uid,
		};
	}
	return self;
}

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

@end



@implementation WindshieldItemRelinquishResponse

-(NSString*) workspaceUid
{
	return [ingests objectForKey:@"workspace-uid"];
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

@end



@implementation WorkspaceCloseError

-(NSString*) errorDescription
{
	return [ingests objectForKey:@"description"];
}

-(NSString*) errorCode
{
	return [ingests objectForKey:@"error-code"];
}

-(NSString*) summary
{
	return [ingests objectForKey:@"summary"];
}

@end



@implementation WorkspaceCloseRequest

-(instancetype) initWithUid:(NSString*)uid pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"workspace-close",
			@"from:",
			pat,
		];

		ingests = @{
			@"uid": uid,
		};
	}
	return self;
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

@end



@implementation WorkspaceCreateError

-(NSString*) errorDescription
{
	return [ingests objectForKey:@"description"];
}

-(NSString*) errorCode
{
	return [ingests objectForKey:@"error-code"];
}

-(NSString*) summary
{
	return [ingests objectForKey:@"summary"];
}

@end



@implementation WorkspaceCreatePm

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

-(NSString*) name
{
	return [ingests objectForKey:@"name"];
}

-(NSArray*) owners
{
	return [ingests objectForKey:@"owners"];
}

-(NSString*) modDate
{
	return [ingests objectForKey:@"mod-date"];
}

-(NSInteger) modDateUtc
{
	return [[ingests objectForKey:@"mod-date-utc"] integerValue];
}

-(BOOL) saved
{
	return [[ingests objectForKey:@"saved"] boolValue];
}

-(NSString*) thumbUri
{
	return [ingests objectForKey:@"thumb-uri"];
}

@end



@implementation WorkspaceCreatePsa

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

-(NSString*) name
{
	return [ingests objectForKey:@"name"];
}

-(NSArray*) owners
{
	return [ingests objectForKey:@"owners"];
}

-(NSString*) modDate
{
	return [ingests objectForKey:@"mod-date"];
}

-(NSInteger) modDateUtc
{
	return [[ingests objectForKey:@"mod-date-utc"] integerValue];
}

-(BOOL) saved
{
	return [[ingests objectForKey:@"saved"] boolValue];
}

-(NSString*) thumbUri
{
	return [ingests objectForKey:@"thumb-uri"];
}

@end



@implementation WorkspaceCreateRequest

-(instancetype) initWithName:(NSString*)name owners:(NSArray*)owners pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"workspace-create",
			@"from:",
			pat,
		];

		ingests = @{
			@"name": name,
			@"owners": owners,
		};
	}
	return self;
}

-(NSString*) name
{
	return [ingests objectForKey:@"name"];
}

-(NSArray*) owners
{
	return [ingests objectForKey:@"owners"];
}

@end



@implementation WorkspaceDeleteError

-(NSString*) errorDescription
{
	return [ingests objectForKey:@"description"];
}

-(NSString*) errorCode
{
	return [ingests objectForKey:@"error-code"];
}

-(NSString*) summary
{
	return [ingests objectForKey:@"summary"];
}

@end



@implementation WorkspaceDeletePm

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

@end



@implementation WorkspaceDeletePsa

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

@end



@implementation WorkspaceModifiedPsa

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

-(NSString*) modDate
{
	return [ingests objectForKey:@"mod-date"];
}

-(NSInteger) modDateUtc
{
	return [[ingests objectForKey:@"mod-date-utc"] integerValue];
}

-(NSString*) thumbUri
{
	return [ingests objectForKey:@"thumb-uri"];
}

@end



@implementation WorkspaceDeleteRequest

-(instancetype) initWithUid:(NSString*)uid pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"workspace-delete",
			@"from:",
			pat,
		];

		ingests = @{
			@"uid": uid,
		};
	}
	return self;
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

@end



@implementation WorkspaceDiscardRequest

-(instancetype) initWithUid:(NSString*)uid pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"workspace-discard",
			@"from:",
			pat,
		];

		ingests = @{
			@"uid": uid,
		};
	}
	return self;
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

@end



@implementation WorkspaceDownloadError

-(NSString*) errorDescription
{
	return [ingests objectForKey:@"description"];
}

-(NSString*) errorCode
{
	return [ingests objectForKey:@"error-code"];
}

-(NSString*) summary
{
	return [ingests objectForKey:@"summary"];
}

@end



@implementation WorkspaceDownloadRequest

-(instancetype) initWithUid:(NSString*)uid pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"workspace-download",
			@"from:",
			pat,
		];

		ingests = @{
			@"uid": uid,
		};
	}
	return self;
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

@end



@implementation WorkspaceDownloadResponse

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

-(NSString*) downloadUrl
{
	return [ingests objectForKey:@"download-url"];
}

@end



@implementation WorkspaceDuplicateError

-(NSString*) errorDescription
{
	return [ingests objectForKey:@"description"];
}

-(NSString*) errorCode
{
	return [ingests objectForKey:@"error-code"];
}

-(NSString*) summary
{
	return [ingests objectForKey:@"summary"];
}

@end



@implementation WorkspaceDuplicateRequest

-(instancetype) initWithUid:(NSString*)uid owners:(NSArray*)owners name:(NSString*)name pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"workspace-duplicate",
			@"from:",
			pat,
		];

		ingests = @{
			@"uid": uid,
			@"owners": owners,
			@"name": name,
		};
	}
	return self;
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

-(NSArray*) owners
{
	return [ingests objectForKey:@"owners"];
}

-(NSString*) name
{
	return [ingests objectForKey:@"name"];
}

@end



@implementation WorkspaceGeomChangedPsa

-(NSDictionary*) felds
{
	return [ingests objectForKey:@"felds"];
}

-(NSArray*) surfaces
{
	return [ingests objectForKey:@"surfaces"];
}

@end



@implementation WorkspaceListRequest

-(instancetype) initWithPat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"workspace-list",
			@"from:",
			pat,
		];
	}
	return self;
}

@end



@implementation WorkspaceListResponse

-(NSArray*) workspaces
{
	return [ingests objectForKey:@"workspaces"];
}

@end



@implementation WorkspaceOpenError

-(NSString*) errorDescription
{
	return [ingests objectForKey:@"description"];
}

-(NSString*) errorCode
{
	return [ingests objectForKey:@"error-code"];
}

-(NSString*) summary
{
	return [ingests objectForKey:@"summary"];
}

@end



@implementation WorkspaceOpenRequest

-(instancetype) initWithSwitchFromUid:(NSString*)switchFromUid switchToUid:(NSString*)switchToUid pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"workspace-open",
			@"from:",
			pat,
		];

		ingests = @{
			@"switch-from-uid": switchFromUid,
			@"switch-to-uid": switchToUid,
		};
	}
	return self;
}

-(NSString*) switchFromUid
{
	return [ingests objectForKey:@"switch-from-uid"];
}

-(NSString*) switchToUid
{
	return [ingests objectForKey:@"switch-to-uid"];
}

@end



@implementation WorkspaceRenameError

-(NSString*) errorDescription
{
	return [ingests objectForKey:@"description"];
}

-(NSString*) errorCode
{
	return [ingests objectForKey:@"error-code"];
}

-(NSString*) summary
{
	return [ingests objectForKey:@"summary"];
}

@end



@implementation WorkspaceRenamePm

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

-(NSString*) name
{
	return [ingests objectForKey:@"name"];
}

-(NSString*) modDate
{
	return [ingests objectForKey:@"mod-date"];
}

-(NSInteger) modDateUtc
{
	return [[ingests objectForKey:@"mod-date-utc"] integerValue];
}

@end



@implementation WorkspaceRenamePsa

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

-(NSString*) name
{
	return [ingests objectForKey:@"name"];
}

-(NSString*) modDate
{
	return [ingests objectForKey:@"mod-date"];
}

-(NSInteger) modDateUtc
{
	return [[ingests objectForKey:@"mod-date-utc"] integerValue];
}

@end



@implementation WorkspaceRenameRequest

-(instancetype) initWithUid:(NSString*)uid name:(NSString*)name pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"workspace-rename",
			@"from:",
			pat,
		];

		ingests = @{
			@"uid": uid,
			@"name": name,
		};
	}
	return self;
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

-(NSString*) name
{
	return [ingests objectForKey:@"name"];
}

@end



@implementation WorkspaceReassignPsa

-(NSString*) oldUid
{
	return [ingests objectForKey:@"old-uid"];
}

-(NSString*) newUid
{
	return [ingests objectForKey:@"new-uid"];
}

-(NSString*) name
{
	return [ingests objectForKey:@"name"];
}

-(NSString*) thumbUri
{
	return [ingests objectForKey:@"thumb-uri"];
}

@end



@implementation WorkspaceSavePsa

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

-(NSArray*) owners
{
	return [ingests objectForKey:@"owners"];
}

-(NSString*) name
{
	return [ingests objectForKey:@"name"];
}

@end



@implementation WorkspaceSaveRequest

-(instancetype) initWithUid:(NSString*)uid owners:(NSArray*)owners name:(NSString*)name pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"workspace-save",
			@"from:",
			pat,
		];

		ingests = @{
			@"uid": uid,
			@"owners": owners,
			@"name": name,
		};
	}
	return self;
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

-(NSArray*) owners
{
	return [ingests objectForKey:@"owners"];
}

-(NSString*) name
{
	return [ingests objectForKey:@"name"];
}

@end



@implementation WorkspaceSwitchPsa

-(WorkspaceState*) currentWorkspace
{
	return [ingests objectForKey:@"current-workspace"];
}

-(WorkspaceStateClosed*) previousWorkspace
{
	return [ingests objectForKey:@"previous-workspace"];
}

@end



@implementation WorkspaceUploadDoneError

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

-(NSArray*) owners
{
	return [ingests objectForKey:@"owners"];
}

-(NSString*) fileName
{
	return [ingests objectForKey:@"file-name"];
}

@end



@implementation WorkspaceUploadDoneResponse

-(NSString*) errorDescription
{
	return [ingests objectForKey:@"description"];
}

-(NSString*) errorCode
{
	return [ingests objectForKey:@"error-code"];
}

-(NSString*) summary
{
	return [ingests objectForKey:@"summary"];
}

@end



@implementation WorkspaceUploadError

-(NSString*) errorDescription
{
	return [ingests objectForKey:@"description"];
}

-(NSString*) errorCode
{
	return [ingests objectForKey:@"error-code"];
}

-(NSString*) summary
{
	return [ingests objectForKey:@"summary"];
}

@end



@implementation WorkspaceUploadResponse

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

-(NSArray*) owners
{
	return [ingests objectForKey:@"owners"];
}

-(NSString*) uploadUrl
{
	return [ingests objectForKey:@"upload-url"];
}

@end



@implementation WorkspaceUploadRequest

-(instancetype) initWithOwners:(NSArray*)owners pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"workspace-upload",
			@"from:",
			pat,
			@"to:",
			@"native-mezz",
		];

		ingests = @{
			@"owners": owners,
		};
	}
	return self;
}

-(NSArray*) owners
{
	return [ingests objectForKey:@"owners"];
}

@end



@implementation WorkspaceUploadDoneRequest

-(instancetype) initWithUid:(NSString*)uid owners:(NSArray*)owners fileName:(NSString*)fileName uploadUrl:(NSString*)uploadUrl pat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v3.6",
			@"request",
			@"workspace-upload-done",
			@"from:",
			pat,
		];

		ingests = @{
			@"uid": uid,
			@"owners": owners,
			@"file-name": fileName,
			@"upload-url": uploadUrl,
		};
	}
	return self;
}

-(NSString*) uid
{
	return [ingests objectForKey:@"uid"];
}

-(NSArray*) owners
{
	return [ingests objectForKey:@"owners"];
}

-(NSString*) fileName
{
	return [ingests objectForKey:@"file-name"];
}

-(NSString*) uploadUrl
{
	return [ingests objectForKey:@"upload-url"];
}

@end



@implementation RequestVtcStatus

-(instancetype) initWithPat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v4.0",
			@"request",
			@"request-vtc-status",
			@"from:",
			@"native-mezz",
			@"to:",
			@"vtci",
		];
	}
	return self;
}

@end



@implementation VtcStatus

-(VtcStatusData*) status
{
	return [ingests objectForKey:@"status"];
}

@end



@implementation VtcCallEndMessage

-(CallEndInfo*) callEndInfo
{
	return [ingests objectForKey:@"call-end-info"];
}

@end



@implementation SessionDetailPsa

-(SessionState*) session
{
	return [ingests objectForKey:@"session"];
}

@end



@implementation DialogDisplayPm

-(DialogItem*) dialog
{
	return [ingests objectForKey:@"dialog"];
}

@end



@implementation MeetingWorkspaceCleanupRequest

-(instancetype) initWithPat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v4.0",
			@"request",
			@"meeting-workspace-cleanup",
			@"from:",
			pat,
		];
	}
	return self;
}

@end



@implementation MeetingWorkspaceCleanupResponse

@end



@implementation ParticipantListRequest

-(instancetype) initWithPat:(Pat*)pat
{
	self = [super init];
	if (self)
	{
		descrips = @[
			@"mezzanine",
			@"prot-spec v4.0",
			@"request",
			@"participant-list",
			@"from:",
			pat,
		];
	}
	return self;
}

@end



@implementation ParticipantListPsa

-(NSArray*) cloudParticipants
{
	return [ingests objectForKey:@"cloud-participants"];
}

-(RoomParticipant*) localRoom
{
	return [ingests objectForKey:@"local-room"];
}

-(NSArray*) remoteRooms
{
	return [ingests objectForKey:@"remote-rooms"];
}

@end



@implementation StatusFromVtcPsa

-(VtcStatusDataPsa*) status
{
	return [ingests objectForKey:@"status"];
}

@end


