// Automatically generated code, do not edit.

#import <Foundation/Foundation.h>
#import "OBProtein.h"
#import "MZProteinFactoryBase.h"
#import "MZProteins.h"
#import "MZSlaws.h"


/*
@protocol MZProteinFactory

-(OBProtein*) assetDownloadError;

-(OBProtein*) assetDownloadRequest:(NSString*)contentSource;

-(OBProtein*) assetDownloadResponse:(NSString*)contentSource downloadUrl:(NSString*)downloadUrl;

-(OBProtein*) assetRefreshPsa:(NSString*)contentSource origImageAvailable:(BOOL)origImageAvailable;

-(OBProtein*) assetUploadImageReadyError:(NSArray*)uids errorDescription:(NSString*)errorDescription errorCode:(NSString*)errorCode summary:(NSString*)summary;

-(OBProtein*) assetUploadImageReadyRequest:(NSString*)workspaceUid uid:(NSString*)uid fileId:(NSInteger)fileId fileName:(NSString*)fileName data:(NSData*)data;

-(OBProtein*) assetUploadPdfPm:(NSString*)workspaceUid fileIds:(NSArray*)fileIds;

-(OBProtein*) assetUploadPdfReadyError:(NSInteger)fileId errorDescription:(NSString*)errorDescription errorCode:(NSString*)errorCode summary:(NSString*)summary;

-(OBProtein*) assetUploadPdfReadyRequest:(NSInteger)fileId data:(NSData*)data;

-(OBProtein*) assetUploadProvisionError:(NSString*)workspaceUid uids:(NSArray*)uids errorDescription:(NSString*)errorDescription errorCode:(NSString*)errorCode summary:(NSString*)summary;

-(OBProtein*) assetUploadProvisionRequest:(NSString*)workspaceUid files:(NSArray*)files;

-(OBProtein*) assetUploadProvisionResponse:(NSString*)workspaceUid uids:(NSArray*)uids files:(NSArray*)files errorDescription:(NSString*)errorDescription summary:(NSString*)summary;

-(OBProtein*) clientEvictPm;

-(OBProtein*) clientJoinRequest;

-(OBProtein*) clientJoinRequestWithPasskey:(NSString*)passkey;

-(OBProtein*) clientJoinRequestWithName:(NSString*)displayName;

-(OBProtein*) clientJoinRequestWithNameAndPasskey:(NSString*)displayName passkey:(NSString*)passkey;

-(OBProtein*) clientJoinResponse:(NSString*)mezzanineVersion assetUploadLimits:(AssetUploadLimits*)assetUploadLimits whiteboards:(NSArray*)whiteboards surfaces:(NSArray*)surfaces currentWorkspace:(WorkspaceState*)currentWorkspace felds:(NSDictionary*)felds handipoint:(Handi*)handipoint m2mIsInstalled:(BOOL)m2mIsInstalled permission:(BOOL)permission superuser:(BOOL)superuser username:(NSString*)username remoteAccessEnabled:(BOOL)remoteAccessEnabled mezzMetadata:(MezzMetadata*)mezzMetadata pendingDialogs:(NSArray*)pendingDialogs;

-(OBProtein*) clientJoinResponseDecline:(BOOL)permission reason:(NSString*)reason;

-(OBProtein*) clientLeaveRequest;

-(OBProtein*) dialogDisplayPsa:(DialogItem*)dialog;

-(OBProtein*) dialogDismissRequest:(NSString*)id value:(NSString*)value;

-(OBProtein*) dialogDismissPsa:(NSString*)id value:(NSString*)value;

-(OBProtein*) heartbeatRequest:(BOOL)joined;

-(OBProtein*) heartbeatPsa;

-(OBProtein*) infopresenceDetailPsa:(NSString*)status remoteAccessStatus:(NSString*)remoteAccessStatus remoteAccessUrl:(NSString*)remoteAccessUrl rooms:(NSArray*)rooms remoteParticipants:(NSArray*)remoteParticipants incomingCalls:(NSArray*)incomingCalls outgoingCalls:(NSArray*)outgoingCalls videoChat:(VideoChat*)videoChat isForMagicM2m:(BOOL)isForMagicM2m;

-(OBProtein*) infopresenceDetailRequest;

-(OBProtein*) infopresenceIncomingPsa:(NSString*)uid resolution:(NSString*)resolution;

-(OBProtein*) infopresenceIncomingRequest:(NSString*)uid receivedUtc:(NSInteger)receivedUtc;

-(OBProtein*) infopresenceIncomingResponse:(NSString*)uid accept:(BOOL)accept;

-(OBProtein*) infopresenceLeaveRequest:(BOOL)interrupted;

-(OBProtein*) infopresenceOutgoingCancelRequest;

-(OBProtein*) infopresenceOutgoingPsa:(NSString*)uid sentUtc:(NSInteger)sentUtc;

-(OBProtein*) infopresenceOutgoingRequest:(NSString*)uid sentUtc:(NSInteger)sentUtc;

-(OBProtein*) infopresenceOutgoingError:(NSString*)uid errorDescription:(NSString*)errorDescription summary:(NSString*)summary;

-(OBProtein*) infopresenceOutgoingResponsePsa:(NSString*)uid resolution:(NSString*)resolution;

-(OBProtein*) infopresenceOutgoingInviteRequest:(NSString*)uid;

-(OBProtein*) infopresenceOutgoingInviteErrorResponse:(NSString*)uid errorDescription:(NSString*)errorDescription summary:(NSString*)summary;

-(OBProtein*) infopresenceOutgoingInvitePsa:(NSString*)uid sentUtc:(NSInteger)sentUtc;

-(OBProtein*) infopresenceOutgoingInviteCancelRequest:(NSString*)uid;

-(OBProtein*) infopresenceOutgoingInviteResolvePsa:(NSString*)uid resolution:(NSString*)resolution;

-(OBProtein*) infopresenceIncomingInvitePsa:(NSString*)uid receivedUtc:(NSInteger)receivedUtc;

-(OBProtein*) infopresenceIncomingInviteResolveRequest:(NSString*)uid accept:(BOOL)accept;

-(OBProtein*) infopresenceIncomingInviteResolvePsa:(NSString*)uid resolution:(NSString*)resolution;

-(OBProtein*) infopresenceParticipantJoinPsa:(NSString*)uid displayName:(NSString*)displayName type:(NSString*)type;

-(OBProtein*) infopresenceParticipantLeavePsa:(NSString*)uid displayName:(NSString*)displayName type:(NSString*)type;

-(OBProtein*) liveStreamCreate:(NSString*)uri id:(NSString*)id;

-(OBProtein*) liveStreamCreateResponse:(NSString*)contentSource;

-(OBProtein*) liveStreamCreateError:(NSString*)errorDescription;

-(OBProtein*) liveStreamDelete:(NSString*)contentSource;

-(OBProtein*) liveStreamDeleteResponse:(NSString*)message;

-(OBProtein*) liveStreamDeleteError:(NSString*)errorDescription;

-(OBProtein*) liveStreamAppearPsa:(NSString*)contentSource mezzUid:(NSString*)mezzUid;

-(OBProtein*) liveStreamDetailPsa:(NSString*)workspaceUid liveStreams:(NSArray*)liveStreams;

-(OBProtein*) liveStreamDetailRequest;

-(OBProtein*) liveStreamDetailResponse:(NSString*)workspaceUid liveStreams:(NSArray*)liveStreams;

-(OBProtein*) liveStreamDissapearPsa:(NSString*)contentSource;

-(OBProtein*) liveStreamGrabPsa:(NSString*)uid;

-(OBProtein*) liveStreamGrabError:(NSString*)workspaceUid uid:(NSString*)uid errorDescription:(NSString*)errorDescription errorCode:(NSString*)errorCode summary:(NSString*)summary;

-(OBProtein*) liveStreamGrabRequest:(NSString*)workspaceUid uid:(NSString*)uid;

-(OBProtein*) liveStreamGrabResponse:(NSString*)workspaceUid uid:(NSString*)uid;

-(OBProtein*) liveStreamRelinquishPsa:(NSString*)workspaceUid uid:(NSString*)uid;

-(OBProtein*) liveStreamRelinquishError:(NSString*)workspaceUid uid:(NSString*)uid errorDescription:(NSString*)errorDescription errorCode:(NSString*)errorCode summary:(NSString*)summary;

-(OBProtein*) liveStreamRelinquishRequest:(NSString*)workspaceUid uid:(NSString*)uid;

-(OBProtein*) liveStreamRelinquishResponse:(NSString*)workspaceUid uid:(NSString*)uid;

-(OBProtein*) liveStreamRefreshPsa:(NSString*)contentSource image:(ImageSpec*)image thumbnailImage:(ImageSpec*)thumbnailImage;

-(OBProtein*) clientSignInError:(NSString*)errorDescription errorCode:(NSString*)errorCode summary:(NSString*)summary;

-(OBProtein*) clientSignInRequest:(NSString*)username password:(NSString*)password;

-(OBProtein*) clientSignInResponse:(BOOL)superuser username:(NSString*)username;

-(OBProtein*) clientSignOutPm;

-(OBProtein*) clientSignOutRequest;

-(OBProtein*) clientSignOutResponse;

-(OBProtein*) meetingEndPsa:(NSString*)meetingUid;

-(OBProtein*) meetingEndError:(NSString*)meetingUid errorDescription:(NSString*)errorDescription errorCode:(NSString*)errorCode summary:(NSString*)summary;

-(OBProtein*) meetingEndRequest:(NSString*)meetingUid;

-(OBProtein*) meetingJoinUrlRequest:(NSString*)mezzinUrl isForMagicM2m:(BOOL)isForMagicM2m;

-(OBProtein*) meetingListPsa:(NSArray*)meetingList inProgressMeeting:(CalendarMeeting*)inProgressMeeting;

-(OBProtein*) meetingListRequest;

-(OBProtein*) meetingStartPsa:(NSString*)meetingUid meeting:(CalendarMeeting*)meeting;

-(OBProtein*) meetingStartError:(NSString*)meetingUid errorDescription:(NSString*)errorDescription errorCode:(NSString*)errorCode summary:(NSString*)summary;

-(OBProtein*) meetingStartRequest:(NSString*)meetingUid isForMagicM2m:(BOOL)isForMagicM2m;

-(OBProtein*) meetingReadyPsa:(NSString*)meetingUid meeting:(CalendarMeeting*)meeting;

-(OBProtein*) meetingAbortedPsa:(NSString*)meetingUid meeting:(CalendarMeeting*)meeting;

-(OBProtein*) mezCapsPsa:(AssetUploadLimits*)assetUploadLimits whiteboards:(NSArray*)whiteboards felds:(NSDictionary*)felds surfaces:(NSArray*)surfaces remoteAccessEnabled:(BOOL)remoteAccessEnabled;

-(OBProtein*) mezCapsRequest;

-(OBProtein*) mezCapsResponse:(AssetUploadLimits*)assetUploadLimits whiteboards:(NSArray*)whiteboards surfaces:(NSArray*)surfaces;

-(OBProtein*) mezzMetadataRequest;

-(OBProtein*) mezzMetadataResponse:(BOOL)cloudInstance featureToggles:(FeatureToggles*)featureToggles apiVersion:(NSString*)apiVersion systemUseNotification:(BOOL)systemUseNotification systemUseNotificationText:(NSString*)systemUseNotificationText model:(NSString*)model ocelotSystem:(BOOL)ocelotSystem;

-(OBProtein*) ohayoGoyaimasuPsa;

-(OBProtein*) outgoingInfopresenceSessionRequest;

-(OBProtein*) passkeyDetailRequest;

-(OBProtein*) passkeyDetailResponse:(NSString*)clientConnectionUrl isPasskeyProtected:(BOOL)isPasskeyProtected passkey:(NSString*)passkey;

-(OBProtein*) passkeyDisablePsa;

-(OBProtein*) passkeyDisableRequest;

-(OBProtein*) passkeyEnablePsa:(NSString*)exempt;

-(OBProtein*) passkeyEnableRequest;

-(OBProtein*) portfolioDetailPsa:(NSString*)workspaceUid portfolioItems:(NSArray*)portfolioItems;

-(OBProtein*) portfolioDetailRequest:(NSString*)workspaceUid;

-(OBProtein*) portfolioDetailResponse:(NSString*)workspaceUid portfolioItems:(NSArray*)portfolioItems;

-(OBProtein*) portfolioDownloadError:(NSString*)workspaceUid errorDescription:(NSString*)errorDescription errorCode:(NSString*)errorCode summary:(NSString*)summary;

-(OBProtein*) portfolioDownloadRequest:(NSString*)workspaceUid;

-(OBProtein*) portfolioDownloadResponse:(NSString*)workspaceUid downloadUrl:(NSString*)downloadUrl;

-(OBProtein*) portfolioItemDeleteRequest:(NSString*)workspaceUid uid:(NSString*)uid;

-(OBProtein*) portfolioItemDeleteError:(NSString*)workspaceUid uid:(NSString*)uid errorDescription:(NSString*)errorDescription errorCode:(NSString*)errorCode summary:(NSString*)summary;

-(OBProtein*) portfolioItemDeletePsa:(NSString*)workspaceUid uid:(NSString*)uid;

-(OBProtein*) portfolioClearPsa:(NSString*)workspaceUid;

-(OBProtein*) portfolioClearRequest:(NSString*)workspaceUid;

-(OBProtein*) portfolioClearError:(NSString*)errorDescription summary:(NSString*)summary;

-(OBProtein*) portfolioItemGrabPsa:(NSString*)workspaceUid uid:(NSString*)uid;

-(OBProtein*) portfolioItemGrabError:(NSString*)workspaceUid uid:(NSString*)uid errorDescription:(NSString*)errorDescription errorCode:(NSString*)errorCode summary:(NSString*)summary;

-(OBProtein*) portfolioItemGrabRequest:(NSString*)workspaceUid uid:(NSString*)uid;

-(OBProtein*) portfolioItemGrabResponse:(NSString*)workspaceUid uid:(NSString*)uid;

-(OBProtein*) portfolioItemInsertPsa:(NSString*)workspaceUid uid:(NSString*)uid contentSource:(NSString*)contentSource displayName:(NSString*)displayName index:(NSInteger)index thumbUri:(NSString*)thumbUri largeUri:(NSString*)largeUri smallUri:(NSString*)smallUri mediumUri:(NSString*)mediumUri feldRelativeUri:(NSString*)feldRelativeUri fullUri:(NSString*)fullUri imageAvailable:(BOOL)imageAvailable feldInscribedUri:(NSString*)feldInscribedUri origImageAvailable:(BOOL)origImageAvailable;

-(OBProtein*) portfolioItemInsertRequest:(NSString*)workspaceUid contentSource:(NSString*)contentSource index:(NSInteger)index;

-(OBProtein*) portfolioItemInsertError:(NSString*)workspaceUid contentSource:(NSString*)contentSource errorDescription:(NSString*)errorDescription index:(NSInteger)index summary:(NSString*)summary;

-(OBProtein*) portfolioItemRelinquishPsa:(NSString*)workspaceUid uid:(NSString*)uid;

-(OBProtein*) portfolioItemRelinquishError:(NSString*)workspaceUid uid:(NSString*)uid errorDescription:(NSString*)errorDescription errorCode:(NSString*)errorCode summary:(NSString*)summary;

-(OBProtein*) portfolioItemRelinquishRequest:(NSString*)workspaceUid uid:(NSString*)uid;

-(OBProtein*) portfolioItemRelinquishResponse:(NSString*)uid;

-(OBProtein*) portfolioItemReorderPsa:(NSString*)workspaceUid uid:(NSString*)uid index:(NSInteger)index;

-(OBProtein*) portfolioItemReorderError:(NSString*)workspaceUid uid:(NSString*)uid index:(NSInteger)index errorDescription:(NSString*)errorDescription summary:(NSString*)summary;

-(OBProtein*) portfolioItemReorderRequest:(NSString*)workspaceUid uid:(NSString*)uid index:(NSInteger)index;

-(OBProtein*) presentationDetailPsa:(NSString*)workspaceUid active:(BOOL)active currentIndex:(NSInteger)currentIndex;

-(OBProtein*) presentationScrollRequest:(NSString*)workspaceUid currentIndex:(NSInteger)currentIndex;

-(OBProtein*) presentationStartRequest:(NSString*)workspaceUid currentIndex:(NSInteger)currentIndex;

-(OBProtein*) presentationStopRequest:(NSString*)workspaceUid;

-(OBProtein*) profileDetailPsa:(MezzDetail*)profile;

-(OBProtein*) profileDetailRequest;

-(OBProtein*) ratchetError:(NSString*)errorDescription errorCode:(NSString*)errorCode summary:(NSString*)summary;

-(OBProtein*) ratchetResponse:(NSString*)ratchetState;

-(OBProtein*) ratchetRequest:(NSString*)ratchetState;

-(OBProtein*) infopresenceEnableRemoteAccessRequest;

-(OBProtein*) infopresenceEnableRemoteAccessErrorResponse:(NSString*)errorDescription errorCode:(NSString*)errorCode summary:(NSString*)summary;

-(OBProtein*) infopresenceDisableRemoteAccessRequest;

-(OBProtein*) infopresenceDisableRemoteAccessErrorResponse:(NSString*)errorDescription errorCode:(NSString*)errorCode summary:(NSString*)summary;

-(OBProtein*) passforwardRequest:(NSString*)spaceCntx curAim:(MZVect*)curAim curOrigin:(MZVect*)curOrigin pressureReports:(PressureReport*)pressureReports;

-(OBProtein*) remoteMezzaninesDetailPsa:(NSArray*)remoteMezzanines;

-(OBProtein*) remoteMezzaninesDetailRequest;

-(OBProtein*) remoteSessionEndPsa;

-(OBProtein*) storageDetailPsa:(BOOL)lowStorage;

-(OBProtein*) storageDetailRequest;

-(OBProtein*) whiteboardCaptureError:(NSString*)errorDescription errorCode:(NSString*)errorCode summary:(NSString*)summary;

-(OBProtein*) whiteboardCaptureRequest:(NSString*)uid workspaceUid:(NSString*)workspaceUid;

-(OBProtein*) whiteboardCaptureResponse:(NSString*)workspaceUid;

-(OBProtein*) windshieldArrangeError:(NSString*)errorDescription errorCode:(NSString*)errorCode summary:(NSString*)summary;

-(OBProtein*) windshieldArrangeRequest:(NSString*)workspaceUid surfaces:(NSArray*)surfaces;

-(OBProtein*) windshieldClearError:(NSString*)workspaceUid errorDescription:(NSString*)errorDescription errorCode:(NSString*)errorCode summary:(NSString*)summary;

-(OBProtein*) windshieldClearPsa:(NSString*)workspaceUid surfaces:(NSArray*)surfaces;

-(OBProtein*) windshieldClearRequest:(NSString*)workspaceUid surfaces:(NSArray*)surfaces;

-(OBProtein*) windshieldDetailPsa:(NSString*)workspaceUid windshieldItems:(NSArray*)windshieldItems;

-(OBProtein*) windshieldDetailRequest:(NSString*)workspaceUid;

-(OBProtein*) windshieldItemCreateError:(NSString*)workspaceUid errorDescription:(NSString*)errorDescription errorCode:(NSString*)errorCode summary:(NSString*)summary;

-(OBProtein*) windshieldItemCreatePsa:(NSString*)workspaceUid uid:(NSString*)uid contentSource:(NSString*)contentSource displayName:(NSString*)displayName surfaceName:(NSString*)surfaceName feldRelativeCoords:(FeldRelativeCoords*)feldRelativeCoords feldRelativeSize:(FeldRelativeSize*)feldRelativeSize thumbUri:(NSString*)thumbUri largeUri:(NSString*)largeUri smallUri:(NSString*)smallUri mediumUri:(NSString*)mediumUri feldRelativeUri:(NSString*)feldRelativeUri fullUri:(NSString*)fullUri imageAvailable:(BOOL)imageAvailable feldInscribedUri:(NSString*)feldInscribedUri origImageAvailable:(BOOL)origImageAvailable;

-(OBProtein*) windshieldItemCreateRequest:(NSString*)workspaceUid contentSource:(NSString*)contentSource surfaceName:(NSString*)surfaceName feldRelativeCoords:(FeldRelativeCoords*)feldRelativeCoords feldRelativeSize:(FeldRelativeSize*)feldRelativeSize;

-(OBProtein*) windshieldItemDeleteError:(NSString*)workspaceUid errorDescription:(NSString*)errorDescription errorCode:(NSString*)errorCode summary:(NSString*)summary;

-(OBProtein*) windshieldItemDeletePsa:(NSString*)workspaceUid uid:(NSString*)uid;

-(OBProtein*) windshieldItemDeleteRequest:(NSString*)workspaceUid uid:(NSString*)uid;

-(OBProtein*) windshieldItemGrabError:(NSString*)workspaceUid uid:(NSString*)uid errorDescription:(NSString*)errorDescription errorCode:(NSString*)errorCode summary:(NSString*)summary;

-(OBProtein*) windshieldItemGrabPsa:(NSString*)workspaceUid uid:(NSString*)uid;

-(OBProtein*) windshieldItemGrabRequest:(NSString*)workspaceUid uid:(NSString*)uid;

-(OBProtein*) windshieldItemGrabResponse:(NSString*)workspaceUid uid:(NSString*)uid;

-(OBProtein*) windshieldItemTransformError:(NSString*)workspaceUid errorDescription:(NSString*)errorDescription errorCode:(NSString*)errorCode summary:(NSString*)summary;

-(OBProtein*) windshieldItemTransformPsa:(NSString*)workspaceUid uid:(NSString*)uid surfaceName:(NSString*)surfaceName feldRelativeCoords:(FeldRelativeCoords*)feldRelativeCoords feldRelativeSize:(FeldRelativeSize*)feldRelativeSize moveToFront:(BOOL)moveToFront;

-(OBProtein*) windshieldItemTransformRequest:(NSString*)workspaceUid uid:(NSString*)uid surfaceName:(NSString*)surfaceName feldRelativeCoords:(FeldRelativeCoords*)feldRelativeCoords feldRelativeSize:(FeldRelativeSize*)feldRelativeSize;

-(OBProtein*) windshieldItemRelinquishPsa:(NSString*)workspaceUid uid:(NSString*)uid;

-(OBProtein*) windshieldItemRelinquishError:(NSString*)workspaceUid uid:(NSString*)uid errorDescription:(NSString*)errorDescription errorCode:(NSString*)errorCode summary:(NSString*)summary;

-(OBProtein*) windshieldItemRelinquishRequest:(NSString*)workspaceUid uid:(NSString*)uid;

-(OBProtein*) windshieldItemRelinquishResponse:(NSString*)workspaceUid uid:(NSString*)uid;

-(OBProtein*) workspaceCloseError:(NSString*)errorDescription errorCode:(NSString*)errorCode summary:(NSString*)summary;

-(OBProtein*) workspaceCloseRequest:(NSString*)uid;

-(OBProtein*) workspaceCreateError:(NSString*)errorDescription errorCode:(NSString*)errorCode summary:(NSString*)summary;

-(OBProtein*) workspaceCreatePm:(NSString*)uid name:(NSString*)name owners:(NSArray*)owners modDate:(NSString*)modDate modDateUtc:(NSInteger)modDateUtc saved:(BOOL)saved thumbUri:(NSString*)thumbUri;

-(OBProtein*) workspaceCreatePsa:(NSString*)uid name:(NSString*)name owners:(NSArray*)owners modDate:(NSString*)modDate modDateUtc:(NSInteger)modDateUtc saved:(BOOL)saved thumbUri:(NSString*)thumbUri;

-(OBProtein*) workspaceCreateRequest:(NSString*)name owners:(NSArray*)owners;

-(OBProtein*) workspaceDeleteError:(NSString*)errorDescription errorCode:(NSString*)errorCode summary:(NSString*)summary;

-(OBProtein*) workspaceDeletePm:(NSString*)uid;

-(OBProtein*) workspaceDeletePsa:(NSString*)uid;

-(OBProtein*) workspaceModifiedPsa:(NSString*)uid modDate:(NSString*)modDate modDateUtc:(NSInteger)modDateUtc thumbUri:(NSString*)thumbUri;

-(OBProtein*) workspaceDeleteRequest:(NSString*)uid;

-(OBProtein*) workspaceDiscardRequest:(NSString*)uid;

-(OBProtein*) workspaceDownloadError:(NSString*)errorDescription errorCode:(NSString*)errorCode summary:(NSString*)summary;

-(OBProtein*) workspaceDownloadRequest:(NSString*)uid;

-(OBProtein*) workspaceDownloadResponse:(NSString*)uid downloadUrl:(NSString*)downloadUrl;

-(OBProtein*) workspaceDuplicateError:(NSString*)errorDescription errorCode:(NSString*)errorCode summary:(NSString*)summary;

-(OBProtein*) workspaceDuplicateRequest:(NSString*)uid owners:(NSArray*)owners name:(NSString*)name;

-(OBProtein*) workspaceGeomChangedPsa:(NSDictionary*)felds surfaces:(NSArray*)surfaces;

-(OBProtein*) workspaceListRequest;

-(OBProtein*) workspaceListResponse:(NSArray*)workspaces;

-(OBProtein*) workspaceOpenError:(NSString*)errorDescription errorCode:(NSString*)errorCode summary:(NSString*)summary;

-(OBProtein*) workspaceOpenRequest:(NSString*)switchFromUid switchToUid:(NSString*)switchToUid;

-(OBProtein*) workspaceRenameError:(NSString*)errorDescription errorCode:(NSString*)errorCode summary:(NSString*)summary;

-(OBProtein*) workspaceRenamePm:(NSString*)uid name:(NSString*)name modDate:(NSString*)modDate modDateUtc:(NSInteger)modDateUtc;

-(OBProtein*) workspaceRenamePsa:(NSString*)uid name:(NSString*)name modDate:(NSString*)modDate modDateUtc:(NSInteger)modDateUtc;

-(OBProtein*) workspaceRenameRequest:(NSString*)uid name:(NSString*)name;

-(OBProtein*) workspaceReassignPsa:(NSString*)oldUid newUid:(NSString*)newUid name:(NSString*)name thumbUri:(NSString*)thumbUri;

-(OBProtein*) workspaceSavePsa:(NSString*)uid owners:(NSArray*)owners name:(NSString*)name;

-(OBProtein*) workspaceSaveRequest:(NSString*)uid owners:(NSArray*)owners name:(NSString*)name;

-(OBProtein*) workspaceSwitchPsa:(WorkspaceState*)currentWorkspace previousWorkspace:(WorkspaceStateClosed*)previousWorkspace;

-(OBProtein*) workspaceUploadDoneError:(NSString*)uid owners:(NSArray*)owners fileName:(NSString*)fileName;

-(OBProtein*) workspaceUploadDoneResponse:(NSString*)errorDescription errorCode:(NSString*)errorCode summary:(NSString*)summary;

-(OBProtein*) workspaceUploadError:(NSString*)errorDescription errorCode:(NSString*)errorCode summary:(NSString*)summary;

-(OBProtein*) workspaceUploadResponse:(NSString*)uid owners:(NSArray*)owners uploadUrl:(NSString*)uploadUrl;

-(OBProtein*) workspaceUploadRequest:(NSArray*)owners;

-(OBProtein*) workspaceUploadDoneRequest:(NSString*)uid owners:(NSArray*)owners fileName:(NSString*)fileName uploadUrl:(NSString*)uploadUrl;

-(OBProtein*) requestVtcStatus;

-(OBProtein*) vtcStatus:(VtcStatusData*)status;

-(OBProtein*) vtcCallEndMessage:(CallEndInfo*)callEndInfo;

-(OBProtein*) sessionDetailPsa:(SessionState*)session;

-(OBProtein*) dialogDisplayPm:(DialogItem*)dialog;

-(OBProtein*) meetingWorkspaceCleanupRequest;

-(OBProtein*) meetingWorkspaceCleanupResponse;

-(OBProtein*) participantListRequest;

-(OBProtein*) participantListPsa:(NSArray*)cloudParticipants localRoom:(RoomParticipant*)localRoom remoteRooms:(NSArray*)remoteRooms;

-(OBProtein*) statusFromVtcPsa:(VtcStatusDataPsa*)status;


@end
*/

/// Compatible with client api versions: 3.6, 4.0
@interface MZProteinFactory : MZProteinFactoryBase

-(OBProtein*) assetDownloadRequest:(NSString*)contentSource;

-(OBProtein*) assetUploadImageReadyRequest:(NSString*)workspaceUid uid:(NSString*)uid fileId:(NSInteger)fileId fileName:(NSString*)fileName data:(NSData*)data;

-(OBProtein*) assetUploadPdfReadyRequest:(NSInteger)fileId data:(NSData*)data;

-(OBProtein*) assetUploadProvisionRequest:(NSString*)workspaceUid files:(NSArray*)files;

-(OBProtein*) clientJoinRequest;

-(OBProtein*) clientJoinRequestWithPasskey:(NSString*)passkey;

-(OBProtein*) clientJoinRequestWithName:(NSString*)displayName;

-(OBProtein*) clientJoinRequestWithNameAndPasskey:(NSString*)displayName passkey:(NSString*)passkey;

-(OBProtein*) clientLeaveRequest;

-(OBProtein*) dialogDismissRequest:(NSString*)id value:(NSString*)value;

-(OBProtein*) heartbeatRequest:(BOOL)joined;

-(OBProtein*) infopresenceDetailRequest;

-(OBProtein*) infopresenceIncomingResponse:(NSString*)uid accept:(BOOL)accept;

-(OBProtein*) infopresenceLeaveRequest:(BOOL)interrupted;

-(OBProtein*) infopresenceOutgoingCancelRequest;

-(OBProtein*) infopresenceOutgoingRequest:(NSString*)uid sentUtc:(NSInteger)sentUtc;

-(OBProtein*) infopresenceOutgoingInviteRequest:(NSString*)uid;

-(OBProtein*) infopresenceOutgoingInviteCancelRequest:(NSString*)uid;

-(OBProtein*) infopresenceIncomingInviteResolveRequest:(NSString*)uid accept:(BOOL)accept;

-(OBProtein*) liveStreamCreate:(NSString*)uri id:(NSString*)id;

-(OBProtein*) liveStreamDelete:(NSString*)contentSource;

-(OBProtein*) liveStreamDetailRequest;

-(OBProtein*) liveStreamGrabRequest:(NSString*)workspaceUid uid:(NSString*)uid;

-(OBProtein*) liveStreamRelinquishRequest:(NSString*)workspaceUid uid:(NSString*)uid;

-(OBProtein*) clientSignInRequest:(NSString*)username password:(NSString*)password;

-(OBProtein*) clientSignOutRequest;

-(OBProtein*) meetingEndRequest:(NSString*)meetingUid;

-(OBProtein*) meetingJoinUrlRequest:(NSString*)mezzinUrl isForMagicM2m:(BOOL)isForMagicM2m;

-(OBProtein*) meetingListRequest;

-(OBProtein*) meetingStartRequest:(NSString*)meetingUid isForMagicM2m:(BOOL)isForMagicM2m;

-(OBProtein*) mezCapsRequest;

-(OBProtein*) mezzMetadataRequest;

-(OBProtein*) outgoingInfopresenceSessionRequest;

-(OBProtein*) passkeyDetailRequest;

-(OBProtein*) passkeyDisableRequest;

-(OBProtein*) passkeyEnableRequest;

-(OBProtein*) portfolioDetailRequest:(NSString*)workspaceUid;

-(OBProtein*) portfolioDownloadRequest:(NSString*)workspaceUid;

-(OBProtein*) portfolioItemDeleteRequest:(NSString*)workspaceUid uid:(NSString*)uid;

-(OBProtein*) portfolioClearRequest:(NSString*)workspaceUid;

-(OBProtein*) portfolioItemGrabRequest:(NSString*)workspaceUid uid:(NSString*)uid;

-(OBProtein*) portfolioItemInsertRequest:(NSString*)workspaceUid contentSource:(NSString*)contentSource index:(NSInteger)index;

-(OBProtein*) portfolioItemRelinquishRequest:(NSString*)workspaceUid uid:(NSString*)uid;

-(OBProtein*) portfolioItemReorderRequest:(NSString*)workspaceUid uid:(NSString*)uid index:(NSInteger)index;

-(OBProtein*) presentationScrollRequest:(NSString*)workspaceUid currentIndex:(NSInteger)currentIndex;

-(OBProtein*) presentationStartRequest:(NSString*)workspaceUid currentIndex:(NSInteger)currentIndex;

-(OBProtein*) presentationStopRequest:(NSString*)workspaceUid;

-(OBProtein*) profileDetailRequest;

-(OBProtein*) ratchetRequest:(NSString*)ratchetState;

-(OBProtein*) infopresenceEnableRemoteAccessRequest;

-(OBProtein*) infopresenceDisableRemoteAccessRequest;

-(OBProtein*) passforwardRequest:(NSString*)spaceCntx curAim:(MZVect*)curAim curOrigin:(MZVect*)curOrigin pressureReports:(PressureReport*)pressureReports;

-(OBProtein*) remoteMezzaninesDetailRequest;

-(OBProtein*) storageDetailRequest;

-(OBProtein*) whiteboardCaptureRequest:(NSString*)uid workspaceUid:(NSString*)workspaceUid;

-(OBProtein*) windshieldArrangeRequest:(NSString*)workspaceUid surfaces:(NSArray*)surfaces;

-(OBProtein*) windshieldClearRequest:(NSString*)workspaceUid surfaces:(NSArray*)surfaces;

-(OBProtein*) windshieldDetailRequest:(NSString*)workspaceUid;

-(OBProtein*) windshieldItemCreateRequest:(NSString*)workspaceUid contentSource:(NSString*)contentSource surfaceName:(NSString*)surfaceName feldRelativeCoords:(FeldRelativeCoords*)feldRelativeCoords feldRelativeSize:(FeldRelativeSize*)feldRelativeSize;

-(OBProtein*) windshieldItemDeleteRequest:(NSString*)workspaceUid uid:(NSString*)uid;

-(OBProtein*) windshieldItemGrabRequest:(NSString*)workspaceUid uid:(NSString*)uid;

-(OBProtein*) windshieldItemTransformRequest:(NSString*)workspaceUid uid:(NSString*)uid surfaceName:(NSString*)surfaceName feldRelativeCoords:(FeldRelativeCoords*)feldRelativeCoords feldRelativeSize:(FeldRelativeSize*)feldRelativeSize;

-(OBProtein*) windshieldItemRelinquishRequest:(NSString*)workspaceUid uid:(NSString*)uid;

-(OBProtein*) workspaceCloseRequest:(NSString*)uid;

-(OBProtein*) workspaceCreateRequest:(NSString*)name owners:(NSArray*)owners;

-(OBProtein*) workspaceDeleteRequest:(NSString*)uid;

-(OBProtein*) workspaceDiscardRequest:(NSString*)uid;

-(OBProtein*) workspaceDownloadRequest:(NSString*)uid;

-(OBProtein*) workspaceDuplicateRequest:(NSString*)uid owners:(NSArray*)owners name:(NSString*)name;

-(OBProtein*) workspaceListRequest;

-(OBProtein*) workspaceOpenRequest:(NSString*)switchFromUid switchToUid:(NSString*)switchToUid;

-(OBProtein*) workspaceRenameRequest:(NSString*)uid name:(NSString*)name;

-(OBProtein*) workspaceSaveRequest:(NSString*)uid owners:(NSArray*)owners name:(NSString*)name;

-(OBProtein*) workspaceUploadRequest:(NSArray*)owners;

-(OBProtein*) workspaceUploadDoneRequest:(NSString*)uid owners:(NSArray*)owners fileName:(NSString*)fileName uploadUrl:(NSString*)uploadUrl;

-(OBProtein*) requestVtcStatus;

-(OBProtein*) meetingWorkspaceCleanupRequest;

-(OBProtein*) participantListRequest;


@end
