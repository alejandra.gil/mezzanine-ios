// Automatically generated code, do not edit.
#import "MZProtoFactory.h"


@implementation MZProteinFactory

-(OBProtein*) assetDownloadRequest:(NSString*)contentSource
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	AssetDownloadRequest *assetDownloadRequest = [[AssetDownloadRequest alloc] initWithContentSource:contentSource pat:pat];
	return assetDownloadRequest;
}

-(OBProtein*) assetUploadImageReadyRequest:(NSString*)workspaceUid uid:(NSString*)uid fileId:(NSInteger)fileId fileName:(NSString*)fileName data:(NSData*)data
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	AssetUploadImageReadyRequest *assetUploadImageReadyRequest = [[AssetUploadImageReadyRequest alloc] initWithWorkspaceUid:workspaceUid uid:uid fileId:fileId fileName:fileName data:data pat:pat];
	return assetUploadImageReadyRequest;
}

-(OBProtein*) assetUploadPdfReadyRequest:(NSInteger)fileId data:(NSData*)data
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	AssetUploadPdfReadyRequest *assetUploadPdfReadyRequest = [[AssetUploadPdfReadyRequest alloc] initWithFileId:fileId data:data pat:pat];
	return assetUploadPdfReadyRequest;
}

-(OBProtein*) assetUploadProvisionRequest:(NSString*)workspaceUid files:(NSArray*)files
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	AssetUploadProvisionRequest *assetUploadProvisionRequest = [[AssetUploadProvisionRequest alloc] initWithWorkspaceUid:workspaceUid files:files pat:pat];
	return assetUploadProvisionRequest;
}

-(OBProtein*) clientJoinRequest
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	ClientJoinRequest *clientJoinRequest = [[ClientJoinRequest alloc] initWithPat:pat];
	return clientJoinRequest;
}

-(OBProtein*) clientJoinRequestWithPasskey:(NSString*)passkey
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	ClientJoinRequestWithPasskey *clientJoinRequestWithPasskey = [[ClientJoinRequestWithPasskey alloc] initWithPasskey:passkey pat:pat];
	return clientJoinRequestWithPasskey;
}

-(OBProtein*) clientJoinRequestWithName:(NSString*)displayName
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	ClientJoinRequestWithName *clientJoinRequestWithName = [[ClientJoinRequestWithName alloc] initWithDisplayName:displayName pat:pat];
	return clientJoinRequestWithName;
}

-(OBProtein*) clientJoinRequestWithNameAndPasskey:(NSString*)displayName passkey:(NSString*)passkey
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	ClientJoinRequestWithNameAndPasskey *clientJoinRequestWithNameAndPasskey = [[ClientJoinRequestWithNameAndPasskey alloc] initWithDisplayName:displayName passkey:passkey pat:pat];
	return clientJoinRequestWithNameAndPasskey;
}

-(OBProtein*) clientLeaveRequest
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	ClientLeaveRequest *clientLeaveRequest = [[ClientLeaveRequest alloc] initWithPat:pat];
	return clientLeaveRequest;
}

-(OBProtein*) dialogDismissRequest:(NSString*)id value:(NSString*)value
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	DialogDismissRequest *dialogDismissRequest = [[DialogDismissRequest alloc] initWithId:id value:value pat:pat];
	return dialogDismissRequest;
}

-(OBProtein*) heartbeatRequest:(BOOL)joined
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	HeartbeatRequest *heartbeatRequest = [[HeartbeatRequest alloc] initWithJoined:joined pat:pat];
	return heartbeatRequest;
}

-(OBProtein*) infopresenceDetailRequest
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	InfopresenceDetailRequest *infopresenceDetailRequest = [[InfopresenceDetailRequest alloc] initWithPat:pat];
	return infopresenceDetailRequest;
}

-(OBProtein*) infopresenceIncomingResponse:(NSString*)uid accept:(BOOL)accept
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	InfopresenceIncomingResponse *infopresenceIncomingResponse = [[InfopresenceIncomingResponse alloc] initWithUid:uid accept:accept pat:pat];
	return infopresenceIncomingResponse;
}

-(OBProtein*) infopresenceLeaveRequest:(BOOL)interrupted
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	InfopresenceLeaveRequest *infopresenceLeaveRequest = [[InfopresenceLeaveRequest alloc] initWithInterrupted:interrupted pat:pat];
	return infopresenceLeaveRequest;
}

-(OBProtein*) infopresenceOutgoingCancelRequest
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	InfopresenceOutgoingCancelRequest *infopresenceOutgoingCancelRequest = [[InfopresenceOutgoingCancelRequest alloc] initWithPat:pat];
	return infopresenceOutgoingCancelRequest;
}

-(OBProtein*) infopresenceOutgoingRequest:(NSString*)uid sentUtc:(NSInteger)sentUtc
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	InfopresenceOutgoingRequest *infopresenceOutgoingRequest = [[InfopresenceOutgoingRequest alloc] initWithUid:uid sentUtc:sentUtc pat:pat];
	return infopresenceOutgoingRequest;
}

-(OBProtein*) infopresenceOutgoingInviteRequest:(NSString*)uid
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	InfopresenceOutgoingInviteRequest *infopresenceOutgoingInviteRequest = [[InfopresenceOutgoingInviteRequest alloc] initWithUid:uid pat:pat];
	return infopresenceOutgoingInviteRequest;
}

-(OBProtein*) infopresenceOutgoingInviteCancelRequest:(NSString*)uid
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	InfopresenceOutgoingInviteCancelRequest *infopresenceOutgoingInviteCancelRequest = [[InfopresenceOutgoingInviteCancelRequest alloc] initWithUid:uid pat:pat];
	return infopresenceOutgoingInviteCancelRequest;
}

-(OBProtein*) infopresenceIncomingInviteResolveRequest:(NSString*)uid accept:(BOOL)accept
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	InfopresenceIncomingInviteResolveRequest *infopresenceIncomingInviteResolveRequest = [[InfopresenceIncomingInviteResolveRequest alloc] initWithUid:uid accept:accept pat:pat];
	return infopresenceIncomingInviteResolveRequest;
}

-(OBProtein*) liveStreamCreate:(NSString*)uri id:(NSString*)id
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	LiveStreamCreate *liveStreamCreate = [[LiveStreamCreate alloc] initWithUri:uri id:id pat:pat];
	return liveStreamCreate;
}

-(OBProtein*) liveStreamDelete:(NSString*)contentSource
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	LiveStreamDelete *liveStreamDelete = [[LiveStreamDelete alloc] initWithContentSource:contentSource pat:pat];
	return liveStreamDelete;
}

-(OBProtein*) liveStreamDetailRequest
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	LiveStreamDetailRequest *liveStreamDetailRequest = [[LiveStreamDetailRequest alloc] initWithPat:pat];
	return liveStreamDetailRequest;
}

-(OBProtein*) liveStreamGrabRequest:(NSString*)workspaceUid uid:(NSString*)uid
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	LiveStreamGrabRequest *liveStreamGrabRequest = [[LiveStreamGrabRequest alloc] initWithWorkspaceUid:workspaceUid uid:uid pat:pat];
	return liveStreamGrabRequest;
}

-(OBProtein*) liveStreamRelinquishRequest:(NSString*)workspaceUid uid:(NSString*)uid
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	LiveStreamRelinquishRequest *liveStreamRelinquishRequest = [[LiveStreamRelinquishRequest alloc] initWithWorkspaceUid:workspaceUid uid:uid pat:pat];
	return liveStreamRelinquishRequest;
}

-(OBProtein*) clientSignInRequest:(NSString*)username password:(NSString*)password
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	ClientSignInRequest *clientSignInRequest = [[ClientSignInRequest alloc] initWithUsername:username password:password pat:pat];
	return clientSignInRequest;
}

-(OBProtein*) clientSignOutRequest
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	ClientSignOutRequest *clientSignOutRequest = [[ClientSignOutRequest alloc] initWithPat:pat];
	return clientSignOutRequest;
}

-(OBProtein*) meetingEndRequest:(NSString*)meetingUid
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	MeetingEndRequest *meetingEndRequest = [[MeetingEndRequest alloc] initWithMeetingUid:meetingUid pat:pat];
	return meetingEndRequest;
}

-(OBProtein*) meetingJoinUrlRequest:(NSString*)mezzinUrl isForMagicM2m:(BOOL)isForMagicM2m
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	MeetingJoinUrlRequest *meetingJoinUrlRequest = [[MeetingJoinUrlRequest alloc] initWithMezzinUrl:mezzinUrl isForMagicM2m:isForMagicM2m pat:pat];
	return meetingJoinUrlRequest;
}

-(OBProtein*) meetingListRequest
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	MeetingListRequest *meetingListRequest = [[MeetingListRequest alloc] initWithPat:pat];
	return meetingListRequest;
}

-(OBProtein*) meetingStartRequest:(NSString*)meetingUid isForMagicM2m:(BOOL)isForMagicM2m
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	MeetingStartRequest *meetingStartRequest = [[MeetingStartRequest alloc] initWithMeetingUid:meetingUid isForMagicM2m:isForMagicM2m pat:pat];
	return meetingStartRequest;
}

-(OBProtein*) mezCapsRequest
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	MezCapsRequest *mezCapsRequest = [[MezCapsRequest alloc] initWithPat:pat];
	return mezCapsRequest;
}

-(OBProtein*) mezzMetadataRequest
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	MezzMetadataRequest *mezzMetadataRequest = [[MezzMetadataRequest alloc] initWithPat:pat];
	return mezzMetadataRequest;
}

-(OBProtein*) outgoingInfopresenceSessionRequest
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	OutgoingInfopresenceSessionRequest *outgoingInfopresenceSessionRequest = [[OutgoingInfopresenceSessionRequest alloc] initWithPat:pat];
	return outgoingInfopresenceSessionRequest;
}

-(OBProtein*) passkeyDetailRequest
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	PasskeyDetailRequest *passkeyDetailRequest = [[PasskeyDetailRequest alloc] initWithPat:pat];
	return passkeyDetailRequest;
}

-(OBProtein*) passkeyDisableRequest
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	PasskeyDisableRequest *passkeyDisableRequest = [[PasskeyDisableRequest alloc] initWithPat:pat];
	return passkeyDisableRequest;
}

-(OBProtein*) passkeyEnableRequest
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	PasskeyEnableRequest *passkeyEnableRequest = [[PasskeyEnableRequest alloc] initWithPat:pat];
	return passkeyEnableRequest;
}

-(OBProtein*) portfolioDetailRequest:(NSString*)workspaceUid
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	PortfolioDetailRequest *portfolioDetailRequest = [[PortfolioDetailRequest alloc] initWithWorkspaceUid:workspaceUid pat:pat];
	return portfolioDetailRequest;
}

-(OBProtein*) portfolioDownloadRequest:(NSString*)workspaceUid
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	PortfolioDownloadRequest *portfolioDownloadRequest = [[PortfolioDownloadRequest alloc] initWithWorkspaceUid:workspaceUid pat:pat];
	return portfolioDownloadRequest;
}

-(OBProtein*) portfolioItemDeleteRequest:(NSString*)workspaceUid uid:(NSString*)uid
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	PortfolioItemDeleteRequest *portfolioItemDeleteRequest = [[PortfolioItemDeleteRequest alloc] initWithWorkspaceUid:workspaceUid uid:uid pat:pat];
	return portfolioItemDeleteRequest;
}

-(OBProtein*) portfolioClearRequest:(NSString*)workspaceUid
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	PortfolioClearRequest *portfolioClearRequest = [[PortfolioClearRequest alloc] initWithWorkspaceUid:workspaceUid pat:pat];
	return portfolioClearRequest;
}

-(OBProtein*) portfolioItemGrabRequest:(NSString*)workspaceUid uid:(NSString*)uid
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	PortfolioItemGrabRequest *portfolioItemGrabRequest = [[PortfolioItemGrabRequest alloc] initWithWorkspaceUid:workspaceUid uid:uid pat:pat];
	return portfolioItemGrabRequest;
}

-(OBProtein*) portfolioItemInsertRequest:(NSString*)workspaceUid contentSource:(NSString*)contentSource index:(NSInteger)index
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	PortfolioItemInsertRequest *portfolioItemInsertRequest = [[PortfolioItemInsertRequest alloc] initWithWorkspaceUid:workspaceUid contentSource:contentSource index:index pat:pat];
	return portfolioItemInsertRequest;
}

-(OBProtein*) portfolioItemRelinquishRequest:(NSString*)workspaceUid uid:(NSString*)uid
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	PortfolioItemRelinquishRequest *portfolioItemRelinquishRequest = [[PortfolioItemRelinquishRequest alloc] initWithWorkspaceUid:workspaceUid uid:uid pat:pat];
	return portfolioItemRelinquishRequest;
}

-(OBProtein*) portfolioItemReorderRequest:(NSString*)workspaceUid uid:(NSString*)uid index:(NSInteger)index
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	PortfolioItemReorderRequest *portfolioItemReorderRequest = [[PortfolioItemReorderRequest alloc] initWithWorkspaceUid:workspaceUid uid:uid index:index pat:pat];
	return portfolioItemReorderRequest;
}

-(OBProtein*) presentationScrollRequest:(NSString*)workspaceUid currentIndex:(NSInteger)currentIndex
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	PresentationScrollRequest *presentationScrollRequest = [[PresentationScrollRequest alloc] initWithWorkspaceUid:workspaceUid currentIndex:currentIndex pat:pat];
	return presentationScrollRequest;
}

-(OBProtein*) presentationStartRequest:(NSString*)workspaceUid currentIndex:(NSInteger)currentIndex
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	PresentationStartRequest *presentationStartRequest = [[PresentationStartRequest alloc] initWithWorkspaceUid:workspaceUid currentIndex:currentIndex pat:pat];
	return presentationStartRequest;
}

-(OBProtein*) presentationStopRequest:(NSString*)workspaceUid
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	PresentationStopRequest *presentationStopRequest = [[PresentationStopRequest alloc] initWithWorkspaceUid:workspaceUid pat:pat];
	return presentationStopRequest;
}

-(OBProtein*) profileDetailRequest
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	ProfileDetailRequest *profileDetailRequest = [[ProfileDetailRequest alloc] initWithPat:pat];
	return profileDetailRequest;
}

-(OBProtein*) ratchetRequest:(NSString*)ratchetState
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	RatchetRequest *ratchetRequest = [[RatchetRequest alloc] initWithRatchetState:ratchetState pat:pat];
	return ratchetRequest;
}

-(OBProtein*) infopresenceEnableRemoteAccessRequest
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	InfopresenceEnableRemoteAccessRequest *infopresenceEnableRemoteAccessRequest = [[InfopresenceEnableRemoteAccessRequest alloc] initWithPat:pat];
	return infopresenceEnableRemoteAccessRequest;
}

-(OBProtein*) infopresenceDisableRemoteAccessRequest
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	InfopresenceDisableRemoteAccessRequest *infopresenceDisableRemoteAccessRequest = [[InfopresenceDisableRemoteAccessRequest alloc] initWithPat:pat];
	return infopresenceDisableRemoteAccessRequest;
}

-(OBProtein*) passforwardRequest:(NSString*)spaceCntx curAim:(MZVect*)curAim curOrigin:(MZVect*)curOrigin pressureReports:(PressureReport*)pressureReports
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	PassforwardRequest *passforwardRequest = [[PassforwardRequest alloc] initWithSpaceCntx:spaceCntx curAim:curAim curOrigin:curOrigin pressureReports:pressureReports pat:pat];
	return passforwardRequest;
}

-(OBProtein*) remoteMezzaninesDetailRequest
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	RemoteMezzaninesDetailRequest *remoteMezzaninesDetailRequest = [[RemoteMezzaninesDetailRequest alloc] initWithPat:pat];
	return remoteMezzaninesDetailRequest;
}

-(OBProtein*) storageDetailRequest
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	StorageDetailRequest *storageDetailRequest = [[StorageDetailRequest alloc] initWithPat:pat];
	return storageDetailRequest;
}

-(OBProtein*) whiteboardCaptureRequest:(NSString*)uid workspaceUid:(NSString*)workspaceUid
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	WhiteboardCaptureRequest *whiteboardCaptureRequest = [[WhiteboardCaptureRequest alloc] initWithUid:uid workspaceUid:workspaceUid pat:pat];
	return whiteboardCaptureRequest;
}

-(OBProtein*) windshieldArrangeRequest:(NSString*)workspaceUid surfaces:(NSArray*)surfaces
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	WindshieldArrangeRequest *windshieldArrangeRequest = [[WindshieldArrangeRequest alloc] initWithWorkspaceUid:workspaceUid surfaces:surfaces pat:pat];
	return windshieldArrangeRequest;
}

-(OBProtein*) windshieldClearRequest:(NSString*)workspaceUid surfaces:(NSArray*)surfaces
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	WindshieldClearRequest *windshieldClearRequest = [[WindshieldClearRequest alloc] initWithWorkspaceUid:workspaceUid surfaces:surfaces pat:pat];
	return windshieldClearRequest;
}

-(OBProtein*) windshieldDetailRequest:(NSString*)workspaceUid
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	WindshieldDetailRequest *windshieldDetailRequest = [[WindshieldDetailRequest alloc] initWithWorkspaceUid:workspaceUid pat:pat];
	return windshieldDetailRequest;
}

-(OBProtein*) windshieldItemCreateRequest:(NSString*)workspaceUid contentSource:(NSString*)contentSource surfaceName:(NSString*)surfaceName feldRelativeCoords:(FeldRelativeCoords*)feldRelativeCoords feldRelativeSize:(FeldRelativeSize*)feldRelativeSize
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	WindshieldItemCreateRequest *windshieldItemCreateRequest = [[WindshieldItemCreateRequest alloc] initWithWorkspaceUid:workspaceUid contentSource:contentSource surfaceName:surfaceName feldRelativeCoords:feldRelativeCoords feldRelativeSize:feldRelativeSize pat:pat];
	return windshieldItemCreateRequest;
}

-(OBProtein*) windshieldItemDeleteRequest:(NSString*)workspaceUid uid:(NSString*)uid
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	WindshieldItemDeleteRequest *windshieldItemDeleteRequest = [[WindshieldItemDeleteRequest alloc] initWithWorkspaceUid:workspaceUid uid:uid pat:pat];
	return windshieldItemDeleteRequest;
}

-(OBProtein*) windshieldItemGrabRequest:(NSString*)workspaceUid uid:(NSString*)uid
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	WindshieldItemGrabRequest *windshieldItemGrabRequest = [[WindshieldItemGrabRequest alloc] initWithWorkspaceUid:workspaceUid uid:uid pat:pat];
	return windshieldItemGrabRequest;
}

-(OBProtein*) windshieldItemTransformRequest:(NSString*)workspaceUid uid:(NSString*)uid surfaceName:(NSString*)surfaceName feldRelativeCoords:(FeldRelativeCoords*)feldRelativeCoords feldRelativeSize:(FeldRelativeSize*)feldRelativeSize
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	WindshieldItemTransformRequest *windshieldItemTransformRequest = [[WindshieldItemTransformRequest alloc] initWithWorkspaceUid:workspaceUid uid:uid surfaceName:surfaceName feldRelativeCoords:feldRelativeCoords feldRelativeSize:feldRelativeSize pat:pat];
	return windshieldItemTransformRequest;
}

-(OBProtein*) windshieldItemRelinquishRequest:(NSString*)workspaceUid uid:(NSString*)uid
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	WindshieldItemRelinquishRequest *windshieldItemRelinquishRequest = [[WindshieldItemRelinquishRequest alloc] initWithWorkspaceUid:workspaceUid uid:uid pat:pat];
	return windshieldItemRelinquishRequest;
}

-(OBProtein*) workspaceCloseRequest:(NSString*)uid
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	WorkspaceCloseRequest *workspaceCloseRequest = [[WorkspaceCloseRequest alloc] initWithUid:uid pat:pat];
	return workspaceCloseRequest;
}

-(OBProtein*) workspaceCreateRequest:(NSString*)name owners:(NSArray*)owners
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	WorkspaceCreateRequest *workspaceCreateRequest = [[WorkspaceCreateRequest alloc] initWithName:name owners:owners pat:pat];
	return workspaceCreateRequest;
}

-(OBProtein*) workspaceDeleteRequest:(NSString*)uid
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	WorkspaceDeleteRequest *workspaceDeleteRequest = [[WorkspaceDeleteRequest alloc] initWithUid:uid pat:pat];
	return workspaceDeleteRequest;
}

-(OBProtein*) workspaceDiscardRequest:(NSString*)uid
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	WorkspaceDiscardRequest *workspaceDiscardRequest = [[WorkspaceDiscardRequest alloc] initWithUid:uid pat:pat];
	return workspaceDiscardRequest;
}

-(OBProtein*) workspaceDownloadRequest:(NSString*)uid
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	WorkspaceDownloadRequest *workspaceDownloadRequest = [[WorkspaceDownloadRequest alloc] initWithUid:uid pat:pat];
	return workspaceDownloadRequest;
}

-(OBProtein*) workspaceDuplicateRequest:(NSString*)uid owners:(NSArray*)owners name:(NSString*)name
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	WorkspaceDuplicateRequest *workspaceDuplicateRequest = [[WorkspaceDuplicateRequest alloc] initWithUid:uid owners:owners name:name pat:pat];
	return workspaceDuplicateRequest;
}

-(OBProtein*) workspaceListRequest
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	WorkspaceListRequest *workspaceListRequest = [[WorkspaceListRequest alloc] initWithPat:pat];
	return workspaceListRequest;
}

-(OBProtein*) workspaceOpenRequest:(NSString*)switchFromUid switchToUid:(NSString*)switchToUid
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	WorkspaceOpenRequest *workspaceOpenRequest = [[WorkspaceOpenRequest alloc] initWithSwitchFromUid:switchFromUid switchToUid:switchToUid pat:pat];
	return workspaceOpenRequest;
}

-(OBProtein*) workspaceRenameRequest:(NSString*)uid name:(NSString*)name
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	WorkspaceRenameRequest *workspaceRenameRequest = [[WorkspaceRenameRequest alloc] initWithUid:uid name:name pat:pat];
	return workspaceRenameRequest;
}

-(OBProtein*) workspaceSaveRequest:(NSString*)uid owners:(NSArray*)owners name:(NSString*)name
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	WorkspaceSaveRequest *workspaceSaveRequest = [[WorkspaceSaveRequest alloc] initWithUid:uid owners:owners name:name pat:pat];
	return workspaceSaveRequest;
}

-(OBProtein*) workspaceUploadRequest:(NSArray*)owners
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	WorkspaceUploadRequest *workspaceUploadRequest = [[WorkspaceUploadRequest alloc] initWithOwners:owners pat:pat];
	return workspaceUploadRequest;
}

-(OBProtein*) workspaceUploadDoneRequest:(NSString*)uid owners:(NSArray*)owners fileName:(NSString*)fileName uploadUrl:(NSString*)uploadUrl
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	WorkspaceUploadDoneRequest *workspaceUploadDoneRequest = [[WorkspaceUploadDoneRequest alloc] initWithUid:uid owners:owners fileName:fileName uploadUrl:uploadUrl pat:pat];
	return workspaceUploadDoneRequest;
}

-(OBProtein*) requestVtcStatus
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	RequestVtcStatus *requestVtcStatus = [[RequestVtcStatus alloc] initWithPat:pat];
	return requestVtcStatus;
}

-(OBProtein*) meetingWorkspaceCleanupRequest
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	MeetingWorkspaceCleanupRequest *meetingWorkspaceCleanupRequest = [[MeetingWorkspaceCleanupRequest alloc] initWithPat:pat];
	return meetingWorkspaceCleanupRequest;
}

-(OBProtein*) participantListRequest
{
	Pat *pat = [[Pat alloc] initWithArray:[self getNextProvenanceAndTransactionId]];
	ParticipantListRequest *participantListRequest = [[ParticipantListRequest alloc] initWithPat:pat];
	return participantListRequest;
}

@end
