//
//  NSString+Localization.h
//  MezzKit
//
//  Created by Miguel Sánchez Valdés on 17/11/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Localization)

NSString *MZLocalizedString(NSString* key, NSString* comment);

@end
