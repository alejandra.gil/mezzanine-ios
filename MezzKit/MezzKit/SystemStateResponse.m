//
//  SystemStateResponse.m
//  MezzKit
//
//  Created by Miguel Sánchez Valdés on 25/02/2019.
//  Copyright © 2019 Oblong Industries. All rights reserved.
//

#import "SystemStateResponse.h"

@implementation SystemStateResponse

-(instancetype) initWithData:(id)data {

  if (!data)
    return nil;

  if (self = [super init])
  {
    id jsonData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];

    if ([jsonData isKindOfClass:[NSDictionary class]])
    {
      NSDictionary *jsonDict = (NSDictionary *)jsonData;
      if ([jsonDict[@"response"] isKindOfClass:[NSDictionary class]])
      {
        NSDictionary *responseDict = (NSDictionary *)jsonDict[@"response"];

        if ([responseDict[@"metadata"] isKindOfClass:[NSDictionary class]])
          self.metadata = responseDict[@"metadata"];
        if ([responseDict[@"mezzanine-version"] isKindOfClass:[NSString class]])
          self.mezzanineVersion = responseDict[@"mezzanine-version"];
        if ([responseDict[@"passkey-length"] isKindOfClass:[NSNumber class]])
          self.passkeyLength = [responseDict[@"passkey-length"] integerValue];
        if ([responseDict[@"passkey-required"] isKindOfClass:[NSNumber class]])
          self.passkeyRequired = [responseDict[@"passkey-required"] boolValue];
        if ([responseDict[@"error"] isKindOfClass:[NSDictionary class]])
        {
          NSDictionary *errorDict = (NSDictionary *)responseDict[@"error"];
          if ([errorDict[@"summary"] isKindOfClass:[NSString class]])
            self.errorSummary = errorDict[@"summary"];
          if ([errorDict[@"description"] isKindOfClass:[NSString class]])
            self.errorDescription = errorDict[@"description"];
          if ([errorDict[@"error-code"] isKindOfClass:[NSNumber class]])
            self.errorCode = [errorDict[@"error-code"] integerValue];
        }
      }
      else
      {
        return nil;
      }
    }
    else
    {
      return nil;
    }
  }

  return self;
}
@end
