//
//  MZSystemModel.m
//  Mezzanine
//
//  Created by Zai Chang on 2/22/11.
//  Copyright 2011 Oblong Industries. All rights reserved.
//

#import "MZSystemModel.h"

@implementation MZSystemModel

@synthesize state;
@synthesize felds;
@synthesize passphraseRequested;
@synthesize remoteMezzes;
@synthesize myMezzanine;
@synthesize currentUsername;
@synthesize isCurrentUserSuperuser;
@synthesize toBeDisconnected;
@synthesize currentWorkspace;
@synthesize ratchetState;
@synthesize pendingTransactions;
@synthesize whiteboards;
@synthesize surfaces;
@synthesize popupMessage;
@synthesize passphrase;
@synthesize uploadLimits;

-(id) init
{
  if (self = [super init])
  {
    state = MZSystemStateNotConnected;
    felds = [[NSMutableArray alloc] init];
    myMezzanine = [[MZMezzanine alloc] init];
    _workspaces = [[NSMutableArray alloc] init];
    pendingTransactions = [[NSMutableArray alloc] init];
    whiteboards = [[NSMutableArray alloc] init];
    remoteMezzes = [[NSMutableArray alloc] init];
    surfaces = [[NSMutableArray alloc] init];

    _portfolioExport = [MZExport new];

    uploadLimits = [[MZUploadLimits alloc] init];

    _infopresence = [MZInfopresence new];
    _featureToggles = [MZFeatureToggles new];
    
    isCurrentUserSuperuser = NO;
    _systemType = MZSystemTypeMezzanine;
  }
  return self;
}


- (void)dealloc
{
  DLog(@"%@ dealloc", [self class]);
}



#pragma mark - Felds

KVO_MUTABLEARRAY_ACCESSOR_IMPLEMENTATION(Felds, felds, felds)

-(NSString*) feldNameAtIndex:(NSInteger)index
{
  if (index < 0 || index >= felds.count)
    return nil;
  MZFeld *feld = felds[index];
  return feld.name;
}


-(MZFeld *) feldWithName:(NSString*)aName
{
  for (MZFeld *feld in felds)
  {
    if ([feld.name isEqual:aName])
      return feld;
  }
  return nil;
}


-(NSDictionary*) feldAtIndex:(NSInteger)index
{
  if (index < 0 || index >= felds.count)
    return nil;
  return felds[index];
}


- (MZFeld*)mainFeld
{
  if (felds.count == 3)
  {
    return felds[1];
  }
  else if (felds.count > 0)
  {
    return felds[0];
  }
  return nil;
}


-(CGRect) mainFeldspace
{
  MZFeld *feld = [self mainFeld];
  
  if (feld)
  {
    CGRect feldRect = feld.feldRect;
    return feldRect;
  }
  
  return CGRectZero;
}


-(CGFloat) feldsZDistance
{
  MZFeld *centerFeld = [self centerFeld];
  if (centerFeld)
    return centerFeld.viewDistance;
  return 0.0;
}


-(MZFeld *) centerFeld
{
  NSInteger index = self.indexOfCenterFeld;
  if (index >= 0 && index < felds.count)
  {
    MZFeld *centerFeld = felds[index];
    return centerFeld;
  }
  return nil;
}


-(NSInteger) indexOfCenterFeld
{
  return (NSInteger) round((felds.count - 1.0) / 2.0);
}


#pragma mark - Workspaces

KVO_MUTABLEARRAY_ACCESSOR_IMPLEMENTATION(Workspaces, workspaces, _workspaces)

-(MZWorkspace*) workspaceWithUid:(NSString*)uid
{
  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uid == %@", uid];
  NSArray *results = [_workspaces filteredArrayUsingPredicate:predicate];
  if (results.count > 0)
    return results[0];
  
  return nil;
}


-(NSArray*) workspacesWithOwner:(NSString*)owner
{
  NSPredicate *predicate;
  if (owner.length > 0)
    predicate = [NSPredicate predicateWithFormat:@"owner == %@", owner];
  else
    predicate = [NSPredicate predicateWithFormat:@"owner == NIL"];
  return [_workspaces filteredArrayUsingPredicate:predicate];
}


-(NSArray*) allOwnersOfWorkspaces
{
  NSMutableArray *owners = [[NSMutableArray alloc] init];
  for (MZWorkspace *workspace in _workspaces)
  {
    if (workspace.owner)
      if (![owners containsObject:[workspace owner]])
      {
        [owners addObject:[workspace owner]];
      }
  }
  NSArray *sortedOwners = [owners sortedArrayUsingSelector: @selector(caseInsensitiveCompare:)];
  return sortedOwners;
}


-(void) removeAllWorkspaces
{
  NSInteger count = [_workspaces count];
  if (count > 0)
    [self removeWorkspacesAtIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, count)]];
}


-(void) openWorkspace:(NSDictionary*)workspaceDict
{
  NSString *uid = workspaceDict[@"uid"];
  if (!uid)
    return;
  
  MZWorkspace *workspace = [self workspaceWithUid:uid];
  if (!workspace)
  {
    workspace = [[MZWorkspace alloc] init];;
    workspace.uid = uid;
  }
  
  // This is a measure to prevent owner information being wiped out because open-dossier
  // and close-dosser proteins don't include the owner field
  // TODO: Review of this code is still needed in 2.12 with the workspace change
  NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:workspaceDict];
  [dict setValue:workspace.owner forKey:@"owner"];
  [workspace updateWithDictionary:dict];

  // Add the surface and feld info to the windshield items. This process needs access to
  // MZSystemModel and cannot be done during the previous MZWorksace::updateWithDictionary
  NSArray *windshieldItemsArray = dict[@"windshield-items"];
  if (windshieldItemsArray)
    for (NSDictionary *itemDictionary in windshieldItemsArray)
      if ([itemDictionary valueForKey:@"surface-name"])
      {
        MZWindshieldItem *item = [workspace.windshield itemWithUid:itemDictionary[@"uid"]];
        [item updateSurfaceAndFeldWithDictionary:itemDictionary usingSystemModel:self];
      }

  if (currentWorkspace != workspace)
    self.currentWorkspace = workspace;
}



#pragma mark - Sign In

+(NSSet*) keyPathsForValuesAffectingIsSignedIn
{
  return [NSSet setWithObjects:@"currentUsername", nil];
}

-(BOOL) isSignedIn
{
  return currentUsername.length > 0;
}


#pragma mark - Whiteboards

KVO_MUTABLEARRAY_ACCESSOR_IMPLEMENTATION(Whiteboards, whiteboards, whiteboards)

-(NSDictionary*) whiteboardWithUid:(NSString*)uid
{
  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uid == %@", uid];
  NSArray *results = [whiteboards filteredArrayUsingPredicate:predicate];
  if (results.count > 0)
    return results[0];
  return nil;
}


#pragma mark - Surfaces

KVO_MUTABLEARRAY_ACCESSOR_IMPLEMENTATION(Surfaces, surfaces, surfaces)

-(MZSurface *) surfaceWithName:(NSString *)aName
{
  for (MZSurface *surface in surfaces)
  {
    if ([surface.name isEqual:aName])
      return surface;
  }
  return nil;
}


-(MZSurface *) surfaceWithFeld:(MZFeld *)aFeld
{
  for (MZSurface *surface in surfaces)
    if ([surface.felds containsObject:aFeld])
      return surface;

  // Check felds in independent feld array. Those felds belong to the main surface -- Can be removed when dropping 2.12 support
  for (MZFeld *feld in felds)
    if ([feld isEqual:aFeld])
      return self.surfacesInPrimaryWindshield.firstObject;

  return nil;
}


-(NSArray *) surfacesInPrimaryWindshield
{
  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name == %@", @"main"];
  return [surfaces filteredArrayUsingPredicate:predicate];
}


-(NSArray *) surfacesInExtendedWindshield
{
  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name != %@", @"main"];
  return [surfaces filteredArrayUsingPredicate:predicate];
}


-(NSArray *) surfaceNames
{
  NSMutableArray *names = [NSMutableArray new];
  for (MZSurface *surface in surfaces)
    [names addObject:surface.name];

  return names;
}


-(NSArray *) surfaceNamesInPrimaryWindshield
{
  NSArray *allNames = [self surfaceNames];
  NSMutableArray *names = [NSMutableArray new];

  for (NSString *surfaceName in @[@"main"])
    if ([allNames containsObject:surfaceName])
      [names addObject:surfaceName];

  return names;
}


-(NSArray *) surfaceNamesInExtendedWindshield
{
  NSMutableArray *names = [NSMutableArray arrayWithArray:[self surfaceNames]];
  [names removeObjectsInArray:@[@"main"]];

  if (!names.count)
    return nil;
  else
    return names;
}


-(NSInteger) sideWallScreenCount
{
  return self.surfacesInExtendedWindshield.count;
}


#pragma mark - Ratcheting

-(NSString*) ratchetStateAsString
{
  if (ratchetState == MZWandRachetModePointing)
    return @"pointing";
  if (ratchetState == MZWandRachetModeCapture)
    return @"demarcating";
  if (ratchetState == MZWandRachetModePassthrough)
    return @"passthrough";
  return nil;
}


#pragma mark - Transaction Support

KVO_MUTABLEARRAY_ACCESSOR_IMPLEMENTATION(PendingTransactions, pendingTransactions, pendingTransactions)

-(void) clearPendingTransactions
{
  NSInteger count = [pendingTransactions count];
  if (count > 0)
    [self removePendingTransactionsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, count)]];
}

//-(NSArray*) pendingTransactionsForSlide:(MZItem*)slide
//{
//  NSMutableArray *results = [[NSMutableArray alloc] init];
//  for (MZTransaction *transaction in pendingTransactions)
//  {
//    if ([transaction isKindOfClass:[MZSlideReorderTransaction class]] &&
//        [((MZSlideReorderTransaction*)transaction).slideUid isEqual:slide.uid])
//      [results addObject:transaction];
//  }
//  return results;
//}



#pragma mark - Upload Transactions

-(void) cancelPendingUploadsForTarget:(NSString*)target workspaceUid:(NSString*)workspaceUid
{
  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"transactionName == %@", MZImageUploadTransactionName];
  NSArray *uploadTransactions = [pendingTransactions filteredArrayUsingPredicate:predicate];
  for (MZImageUploadTransaction *transaction in uploadTransactions)
  {
    if (workspaceUid && ![transaction.workspaceUid isEqual:workspaceUid])
      continue;
    
    if ([target isEqual:MZUploadTransactionTargetAsset])
      transaction.uploadAsAsset = NO;
    
    if ([target isEqual:MZUploadTransactionTargetSlide])
      transaction.uploadAsSlide = NO;

    // When workspaceUid is not nil, this is a 2.12 upload and thus always cancel as there are no more multiple targets
    if (workspaceUid || (!transaction.uploadAsAsset && !transaction.uploadAsSlide))
    {
      DLog(@"MZCommunicator: Canceling upload transaction %@", transaction);
      
      [transaction ended];
      
      NSInteger index = [pendingTransactions indexOfObject:transaction];
      [self removeObjectFromPendingTransactionsAtIndex:index];
    }
  }
}



#pragma mark - Remote Mezzanines

KVO_MUTABLEARRAY_ACCESSOR_IMPLEMENTATION(RemoteMezzes, remoteMezzes, remoteMezzes)


-(MZRemoteMezz*) remoteMezzWithUid:(NSString*)uid
{
  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uid == %@", uid];
  NSArray *results = [remoteMezzes filteredArrayUsingPredicate:predicate];
  if (results.count > 0)
    return results[0];
  
  return nil;
}


-(NSArray*) remoteMezzesWithCollaborationState:(MZRemoteMezzCollaborationState)collaborationState
{
  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"collaborationState == %d", collaborationState];
  NSArray *results = [remoteMezzes filteredArrayUsingPredicate:predicate];
  return results;
}


-(void) removeAllRemoteMezzanines
{
  NSInteger count = [remoteMezzes count];
  if (count > 0)
    [self removeRemoteMezzesAtIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, count)]];
}

-(void) updateRemoteMezzanineInfopresenceState
{
  NSMutableSet *updatedMezzanines = [NSMutableSet new];

	for (MZInfopresenceCall *call in _infopresence.outgoingCalls)
	{
		if (call.type == MZInfopresenceCallTypeInvite)
		{
			MZRemoteMezz *invitedMezz = [self remoteMezzWithUid:call.uid];
      
      if (!invitedMezz)
        break;
      
			MZRemoteMezzCollaborationState collaborationState = call.didNotSuceed ? MZRemoteMezzCollaborationStateNotInCollaboration : MZRemoteMezzCollaborationStateInvited;
			
			if (invitedMezz.collaborationState != collaborationState)
				invitedMezz.collaborationState = collaborationState;

			[updatedMezzanines addObject:invitedMezz];
		}
		else if (call.type == MZInfopresenceCallTypeJoinRequest)
		{
			MZRemoteMezz *calledMezz = [self remoteMezzWithUid:call.uid];
      
      if (!calledMezz)
        break;
      
			MZRemoteMezzCollaborationState collaborationState = call.didNotSuceed ? MZRemoteMezzCollaborationStateNotInCollaboration : MZRemoteMezzCollaborationStateJoining;

			if (calledMezz.collaborationState != collaborationState)
				calledMezz.collaborationState = collaborationState;
			
			[updatedMezzanines addObject:calledMezz];
		}
	}
	
	for (MZInfopresenceCall *call in _infopresence.incomingCalls)
	{
		if (call.type == MZInfopresenceCallTypeInvite)
		{
			// Do nothing
		}
		else if (call.type == MZInfopresenceCallTypeJoinRequest)
		{
			MZRemoteMezz *callingMezz = [self remoteMezzWithUid:call.uid];
      
      if (!callingMezz)
        break;
      
			MZRemoteMezzCollaborationState collaborationState = call.didNotSuceed ? MZRemoteMezzCollaborationStateNotInCollaboration : MZRemoteMezzCollaborationStateJoining;
			
			if (callingMezz.collaborationState != collaborationState)
				callingMezz.collaborationState = collaborationState;
			
			[updatedMezzanines addObject:callingMezz];
		}
	}

  for (MZRemoteMezz *roomInInfopresence in _infopresence.rooms)
  {
    // the local mezzanine is always contained even when not in an active session, but is not a remote mezz.
    if (roomInInfopresence == self.myMezzanine)
      continue;

    // All remote mezzanines contained in rooms are in an active session...
    MZRemoteMezzCollaborationState newState = MZRemoteMezzCollaborationStateInCollaboration;
    if (roomInInfopresence.collaborationState != newState)
      roomInInfopresence.collaborationState = newState;
    [updatedMezzanines addObject:roomInInfopresence];
  }

  NSArray *notUpdatedItems = [remoteMezzes filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"NOT (self IN %@)", updatedMezzanines]];
  for (MZRemoteMezz *remoteMezz in notUpdatedItems)
  {
    if (remoteMezz.collaborationState != MZRemoteMezzCollaborationStateNotInCollaboration)
      remoteMezz.collaborationState = MZRemoteMezzCollaborationStateNotInCollaboration;
  }
}



#pragma mark - Model Reset

-(void) reset
{
  [self removeAllWorkspaces];
  [self removeAllRemoteMezzanines];
  [self clearPendingTransactions];

  self.currentUsername = nil;
  self.isCurrentUserSuperuser = NO;
  self.currentWorkspace = nil;
  self.felds = nil;
  self.ratchetState = MZWandRachetModePointing;
  self.passphrase = nil;
  [self.myMezzanine reset];
  [self.infopresence reset];
  [self.portfolioExport reset];
  self.apiVersion = nil;
  self.systemType = MZSystemTypeMezzanine;

  [surfaces removeAllObjects];
  [whiteboards removeAllObjects];
  uploadLimits = [[MZUploadLimits alloc] init];
}


@end


MZSystemState MZSystemStateFromString(NSString *s)
{
  // Unknown
  return -1;
}


@implementation MZSystemModel (Protein)

-(void) updateWithDictionary:(NSDictionary*)dictionary
{
  NSDictionary *feldsDictionary = dictionary[@"felds"];
  if (feldsDictionary)
  {
    NSMutableArray *feldsIncludingInvisibles = [MZFeld orderedFeldsArrayFromDictionary:feldsDictionary includeInvisible:YES];

    if (!self.felds) {
      self.felds = [@[] mutableCopy];
    }
    
    self.felds = feldsIncludingInvisibles;
  }

  NSArray *surfacesArray = dictionary[@"surfaces"];
  if (surfacesArray)
  {
    NSMutableArray *surfaceNames = [NSMutableArray new];
    NSMutableIndexSet *indexesOfSurfacesToInsert = [NSMutableIndexSet indexSet];
    NSMutableArray *surfacesToInsert = [NSMutableArray new];
    for (NSDictionary *surfaceDictionary in surfacesArray)
    {
      NSString *surfaceName = surfaceDictionary[@"name"];
      [surfaceNames addObject:surfaceName];

      MZSurface *surface = [self surfaceWithName:surfaceName];
      if (!surface)
      {
        surface = [[MZSurface alloc] initWithName:surfaceName];
        [indexesOfSurfacesToInsert addIndex:self.surfaces.count + surfacesToInsert.count];

        if ([surface.name isEqualToString:@"main"])
          [surfacesToInsert insertObject:surface atIndex:0];
        else
          [surfacesToInsert addObject:surface];
      }
      [surface updateWithDictionary:surfaceDictionary];

      // Bug 14365 - iOS: User can't interact with felds properly in mixed geometry
      // This is a native error that has been corrected by web client... should be fixed by native
      // Web reference: Bug 13716 - web app: Mixed geometry doesn't occur on web
      if ([surface.name isEqualToString:@"main"] && ((self.felds.count != surface.felds.count) ||
          ([self.felds[0] visibility] != nil && [surface.felds[0] visibility] == nil)))
      {
        NSMutableArray *feldsInSurface = [NSMutableArray new];
        if (self.felds.count)
        {
          for (MZFeld *systemModelFeld in self.felds)
          {
            MZFeld *feld = [self feldWithName:systemModelFeld.name];
            [feld updateWithFeld:systemModelFeld];
            [feldsInSurface addObject:feld];
          }
          surface.felds = feldsInSurface;
        }
        
        [surface willChangeValueForKey:@"felds"];
        [surface didChangeValueForKey:@"felds"];
      }
    }
    [self insertSurfaces:surfacesToInsert atIndexes:indexesOfSurfacesToInsert];

    // Remove surfaces that no longer exist
    NSMutableIndexSet *surfacesToRemove = [NSMutableIndexSet indexSet];
    for (MZSurface *surface in self.surfaces)
    {
      if (![surfaceNames containsObject:surface.name])
      {
        [surface.felds removeAllObjects];
        [surfacesToRemove addIndex:[self.surfaces indexOfObject:surface]];
      }
    }
    [self removeSurfacesAtIndexes:surfacesToRemove];
  }
  else
  {
    // This allows 2.12 Mezzanine to work in 2.14 clients as it had a "Main" surface
    MZSurface *surface = [self surfaceWithName:@"main"];
    if (!surface)
    {
      surface = [[MZSurface alloc] initWithName:@"main"];
      [self insertObject:surface inSurfacesAtIndex:self.surfaces.count];
      surface.felds = self.felds;
    }
  }


  NSDictionary *handipoint = dictionary[@"handipoint"];
  if (handipoint)
  {
    // Do something later
  }

	// Infopresence
    id m2mIsInstalledValue = dictionary[@"m2m-is-installed"];
    if (m2mIsInstalledValue)
      self.infopresence.infopresenceInstalled = [m2mIsInstalledValue boolValue];
	
	NSNumber *remoteAccessEnabled = dictionary[@"remote-access-enabled"];
	if (remoteAccessEnabled)
		_infopresence.remoteParticipantsEnabled = [remoteAccessEnabled boolValue];
  
  
  // Whiteboards
  NSArray *whiteboardDictionary = dictionary[@"whiteboards"];
  if (whiteboardDictionary)
  {
    [self.whiteboards removeAllObjects];
    [self.whiteboards addObjectsFromArray:whiteboardDictionary];
  }
  

  // Upload limits
  //
  NSDictionary *assetUploadLimits = dictionary[@"asset-upload-limits"];
  if (assetUploadLimits)
    [self.uploadLimits updateWithDictionary:assetUploadLimits];
  
  
  // Current workspace
  //
  NSDictionary *currentWorkspaceDictionary = dictionary[@"current-workspace"];
  if (currentWorkspaceDictionary)
  {
    [self updateCurrentWorkspaceWithDictionary:currentWorkspaceDictionary];
  }
  

  // Workspace-List For 2.12 or later Mezzanines
  //
  NSArray *workspacesDict = dictionary[@"workspaces"];
  if (workspacesDict)
  {
    NSArray *uids = [workspacesDict valueForKey:@"uid"];
    
    // Remove workspaces if they no longer exists
    NSMutableIndexSet *indexSet = [NSMutableIndexSet indexSet];
    for (NSInteger i=0; i<self.countOfWorkspaces; i++)
    {
      MZWorkspace *workspace = _workspaces[i];
      if (workspace.uid && ![uids containsObject:workspace.uid])
      {
        [indexSet addIndex:i];
      }
    }
    if ([indexSet count] > 0)
      [self removeWorkspacesAtIndexes:indexSet];
    
    NSMutableArray *workspacesToInsert = [[NSMutableArray alloc] init];
    NSMutableIndexSet *indexForWorkspaces = [NSMutableIndexSet indexSet];
    
    for (NSInteger i=0; i<[uids count]; i++)
    {
      NSString *uid = uids[i];
      // It seems sometimes uids are sent as numbers?
      //if ([uid respondsToSelector:@selector(stringValue)])
      //    uid = [uid stringValue];
      
      MZWorkspace *workspace = [self workspaceWithUid:uid];
      NSDictionary *workspaceInfo = workspacesDict[i];
      
      // If an MZWorkspace instance is already created for the current workspace, and that the workspace isn't
      // yet part of the workspaces list, just re-use the same object
      if ((workspace == nil) && (self.currentWorkspace && [uid isEqual:self.currentWorkspace.uid]))
      {
        // If the workspace in the list is the same as the current workspace, insert that object
        // in into the list instead of a new duplicated object
        [workspacesToInsert addObject:self.currentWorkspace];
        [indexForWorkspaces addIndex:i];
        workspace = self.currentWorkspace;
      }
      
      if (workspace)
      {
        [workspace updateWithDictionary:workspaceInfo];
      }
      else
      {
        workspace = [[MZWorkspace alloc] init];
        workspace.uid = uid;
        [workspace updateWithDictionary:workspaceInfo];
        [workspacesToInsert addObject:workspace];
        [indexForWorkspaces addIndex:i];
      }
    }
    
    if ([workspacesToInsert count])
      [self insertWorkspaces:workspacesToInsert atIndexes:indexForWorkspaces];
  }
}


// This is called in the following situations:
//  when encountering 'current-workspace' in client-join
//  when encountering 'workspace' in workspace-switch
-(void) updateCurrentWorkspaceWithDictionary:(NSDictionary*)workspaceDictionary
{
  NSString *workspaceUid = workspaceDictionary[@"uid"];
  if (self.currentWorkspace && [self.currentWorkspace.uid isEqualToString:workspaceUid])
  {
    // Existing workspace matches workspace-state
    [self.currentWorkspace updateWithDictionary:workspaceDictionary];
  }
  else
  {
    // Existing workspace is different or no current workspace
    // So open the new workspace
    [self openWorkspace:workspaceDictionary];
  }
  
  // Slightly ugly, but works to prevent the necessity of passing in MZSystemModel through all the udpate calls
  MZWindshield *windshield = self.currentWorkspace.windshield;
  if (windshield)
  {
    NSArray *windshieldItemsArray = workspaceDictionary[@"windshield-items"];
    [self updateLiveStreamsWithVideoShielders:windshieldItemsArray];
    for (NSDictionary *windshieldDict in windshieldItemsArray)
    {
      MZWindshieldItem *item = [windshield itemWithUid:windshieldDict[@"uid"]];
      [item updateWithDictionary:windshieldDict];
      [item updateSurfaceAndFeldWithDictionary:windshieldDict usingSystemModel:self];
    }
  }
}

-(void) updateLiveStreamsWithVideoShielders:(NSArray *)items
{
  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.%K BEGINSWITH[c] %@", @"content-source", @"vi-"];
  NSArray *videoItems = [items filteredArrayUsingPredicate:predicate];
  for (NSDictionary *videoItem in videoItems)
  {
    [self.currentWorkspace updateLiveStreamsWithDictionary:videoItem andRemoteMezzes:self.remoteMezzes];
  }
}



@end
