//
//  MZCommunicator+Constants.h
//  MezzKit
//
//  Created by Ivan Bella Lopez on 07/04/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

#import "MZCommunicator.h"

#define MZTransactionNameAssetRefresh              @"asset-refresh"
#define MZTransactionNameAssetUploadProvision      @"asset-upload-provision"
#define MZTransactionNameAssetUploadImageReady     @"asset-upload-image-ready"
#define MZTransactionNameAssetUploadPDFReady       @"asset-upload-pdf-ready"

#define MZTransactionNameClientJoin                @"client-join"
#define MZTransactionNameClientLeave                @"client-leave"

#define MZTransactionNameClientEvict               @"client-evict"
#define MZTransactionNameClientHeartbeat           @"heartbeat"

#define MZTransactionNameInfopresenceSessionDetail @"infopresence-detail"
#define MZTransactionNameInfopresenceSessionLeave  @"infopresence-leave"
#define MZTransactionNameInfopresenceSessionIncomingRequest          @"infopresence-incoming-request"
#define MZTransactionNameInfopresenceSessionIncomingRequestResolve   @"infopresence-incoming-request-resolve"
#define MZTransactionNameInfopresenceSessionOutgoingRequest          @"infopresence-outgoing-request"
#define MZTransactionNameInfopresenceSessionOutgoingRequestCancel    @"infopresence-outgoing-request-cancel"
#define MZTransactionNameInfopresenceSessionOutgoingRequestResolve   @"infopresence-outgoing-request-resolve"

#define MZTransactionNameLiveStreamGrab            @"live-stream-grab"
#define MZTransactionNameLiveStreamRelinquish      @"live-stream-relinquish"
#define MZTransactionNameLiveStreamsDetail         @"live-streams-detail"

#define MZTransactionNameMezzanineState            @"mez-state"
#define MZTransactionNameMezzanineCapabilities     @"mez-caps"

#define MZTransactionNamePasskeyDetails            @"passkey-detail"
#define MZTransactionNamePasskeyEnable             @"passkey-enable"
#define MZTransactionNamePasskeyDisable            @"passkey-disable"

#define MZTransactionNamePortfolioClear            @"portfolio-clear"
#define MZTransactionNamePortfolioItemDelete       @"portfolio-item-delete"
#define MZTransactionNamePortfolioItemInsert       @"portfolio-item-insert"
#define MZTransactionNamePortfolioItemReorder      @"portfolio-item-reorder"

#define MZTransactionNamePresentationDetail        @"presentation-detail"
#define MZTransactionNamePresentationScroll        @"presentation-scroll"
#define MZTransactionNamePresentationStart         @"presentation-start"
#define MZTransactionNamePresentationStop          @"presentation-stop"

#define MZTransactionNameProfileDetail             @"profile-detail"

#define MZTransactionNamePortfolioDownload         @"portfolio-download"

#define MZTransactionNameWhiteboardCapture         @"whiteboard-capture"

#define MZTransactionNameWindshieldArrange         @"windshield-arrange"
#define MZTransactionNameWindshieldClear           @"windshield-clear"
#define MZTransactionNameWindshieldDetail          @"windshield-detail"

#define MZTransactionNameWindshieldItemCreate      @"windshield-item-create"
#define MZTransactionNameWindshieldItemDelete      @"windshield-item-delete"
#define MZTransactionNameWindshieldItemGrab        @"windshield-item-grab"
#define MZTransactionNameWindshieldItemRelinquish  @"windshield-item-relinquish"
#define MZTransactionNameWindshieldItemTransform   @"windshield-item-transform"

#define MZTransactionNameWorkspaceClose            @"workspace-close"
#define MZTransactionNameWorkspaceCreate           @"workspace-create"
#define MZTransactionNameWorkspaceDelete           @"workspace-delete"
#define MZTransactionNameWorkspaceDiscard          @"workspace-discard"
#define MZTransactionNameWorkspaceDuplicate        @"workspace-duplicate"
#define MZTransactionNameWorkspaceOpen             @"workspace-open"
#define MZTransactionNameWorkspaceModified         @"workspace-modified"
#define MZTransactionNameWorkspaceReassign         @"workspace-reassign"
#define MZTransactionNameWorkspaceRename           @"workspace-rename"
#define MZTransactionNameWorkspaceSave             @"workspace-save"
#define MZTransactionNameWorkspaceSwitch           @"workspace-switch" // Response to workspace-close, workspace-open, workspace-discard
#define MZTransactionNameWorkspaceList             @"workspace-list"

#define MZTransactionNameClientSignIn              @"client-sign-in"
#define MZTransactionNameClientSignOut             @"client-sign-out"



//3.0+
#define MZTransactionNameInfopresenceEnableRemoteAccess             @"infopresence-enable-remote-access"
#define MZTransactionNameInfopresenceDisableRemoteAccess            @"infopresence-disable-remote-access"

#define MZTransactionNameInfopresenceOutgoingInvite                 @"infopresence-outgoing-invite"
#define MZTransactionNameInfopresenceOutgoingInviteCancel						@"infopresence-outgoing-invite-cancel"
#define MZTransactionNameInfopresenceOutgoingInviteResolve					@"infopresence-outgoing-invite-resolve"
#define MZTransactionNameInfopresenceIncomingInvite									@"infopresence-incoming-invite"
#define MZTransactionNameInfopresenceIncomingInviteResolve					@"infopresence-incoming-invite-resolve"

#define MZTransactionNameInfopresenceParticipantJoin								@"infopresence-participant-join"
#define MZTransactionNameInfopresenceParticipantLeave								@"infopresence-participant-leave"

#define MZTransactionNameMezzMetadata                               @"mezz-metadata"
#define MZTransactionNameParticipantList                            @"participant-list"
