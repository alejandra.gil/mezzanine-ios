//
//  MZUploader.m
//  MezzKit
//
//  Created by miguel on 1/2/19.
//  Copyright © 2019 Oblong Industries. All rights reserved.
//

#import "MZUploader.h"

#import "MZCommunicatorRequestor.h"
#import "MZCommunicator+Constants.h"
#import "MZProtoFactory.h"

#import "MZSecurityManager.h"
#import "AFNetworking.h"

#import "NSString+VersionChecking.h"
#import "NSString+Localization.h"

#import "OBProtein+Mezzanine.h"
#import "OBJSONSerialization.h"

@interface MZUploader ()

@property (nonatomic, weak) MZCommunicator* communicator;

@property (nonatomic, strong) AFURLSessionManager *sessionManager;

// Upload files management
@property (nonatomic, strong) NSMutableArray *pendingFileUploads;
@property (nonatomic, assign) NSInteger numberOfUploadsInProgress;
@property (nonatomic, assign) NSInteger lastFileIdForUpload;
@property (nonatomic, strong) NSOperationQueue *imageUploadQueue;

// A list of transactions of individiual upload transactions
@property (nonatomic, strong) NSMutableArray *uploadTransactions;

@end


@implementation MZUploader


- (instancetype)initWithCommunicator:(MZCommunicator*)communicator
{
  if (self = [super init])
  {
    self.communicator = communicator;
    self.uploadTransactions = [NSMutableArray new];
    self.pendingFileUploads = [NSMutableArray new];
    self.lastFileIdForUpload = 1;
  }
  return self;
}

#pragma mark - Upload

- (MZTransaction*)requestFilesUpload:(NSArray *)fileUploadInfos workspaceUid:(NSString*)workspaceUid limitReached:(void (^)(MZFileBatchUploadRequestTransaction *, NSArray *, MZFileBatchUploadRequestContinuationBlock))limitReachedBlock singleUploadCompleted:(void (^)(void))singleUploadBlock errorHandler:(void (^)(NSError *))errorBlock
{
  // Assign upload ids
  // Create FileRequest for the provision
  NSMutableArray *fileRequestsArray = [[NSMutableArray alloc] init];
  for (MZFileUploadInfo *uploadInfo in fileUploadInfos)
  {
    uploadInfo.fileUid = self.lastFileIdForUpload++;
    uploadInfo.workspaceUid = workspaceUid;

    FileRequest *fileRequest = [FileRequest new];
    fileRequest.fileId = uploadInfo.fileUid;
    fileRequest.fileName = uploadInfo.fileName;
    fileRequest.type = uploadInfo.format;

    [fileRequestsArray addObject:fileRequest];
  }

  OBProtein *protein = [_communicator.proteinFactory assetUploadProvisionRequest:(NSString*)workspaceUid files:fileRequestsArray];
  MZFileBatchUploadRequestTransaction *transaction = [MZFileBatchUploadRequestTransaction transactionWithProtein:protein];
  transaction.descrips = @[MZTransactionNameAssetUploadProvision];
  transaction.workspaceUid = workspaceUid;
  transaction.fileUploadInfos = fileUploadInfos;
  transaction.completedUploads = 0;
  transaction.transactionName = MZFilesUploadRequestTransactionName;

  // Move blocks to heap
  limitReachedBlock = [limitReachedBlock copy];
  errorBlock = [errorBlock copy];
  singleUploadBlock = [singleUploadBlock copy];

  __weak MZFileBatchUploadRequestTransaction *__transaction = transaction;
  __weak typeof (self) weakSelf = self;
  __weak typeof (MZCommunicator *) weakCommunicator = _communicator;

  transaction.handler = ^(OBProtein *protein){

    NSString *(^extractTidFromDecripAtIndex)(NSInteger index) = ^NSString *(NSInteger index){
      if (index != NSNotFound && index+1 < [[protein descrips] count])
      {
        // transaction id is the second in the array of [provenance, id]
        id provAndId = [protein descrips][index+1];
        if ([provAndId isKindOfClass:[NSArray class]] && [provAndId count] > 1)
          return provAndId[1];
      }
      return nil;
    };

    NSInteger ix = [[protein descrips] indexOfObject:@"to:"];
    NSString *aTid = extractTidFromDecripAtIndex(ix);
    if (aTid)
      DLog(@"requestFileUpload Handler TID %@", aTid);

    NSString *errorCode = [protein ingestForKey:@"error-code"];
    NSArray *uids = [protein ingestForKey:@"uids"];

    if (errorCode || [uids count] == 0)
    {
      NSString *description = [protein ingestForKey:@"description"];
      NSError *error = [NSError errorWithDomain:kMZCommunicatorErrorDomain
                                           code:-1033
                                       userInfo:@{NSLocalizedDescriptionKey: description}];
      NSString *message = error.localizedDescription;
      weakCommunicator.systemModel.popupMessage = message;

      if (errorBlock)
        errorBlock(error);
    }
    else
    {
      NSArray *files = [protein ingestForKey:@"files"];
      NSMutableArray *uploadInfoForFilesToBeUploaded = [NSMutableArray array];

      for (NSDictionary *dictionaryFile in files) {
        FileResponse *fileResponse = [[FileResponse alloc] initWithDictionary:dictionaryFile];

        [fileUploadInfos enumerateObjectsUsingBlock:^(MZFileUploadInfo *uploadInfo, NSUInteger idx, BOOL * _Nonnull stop) {
          if (uploadInfo.fileUid == fileResponse.fileId) {
            // Add the uids needed for the upload mechanism
            uploadInfo.assetUids = fileResponse.uids;
            [uploadInfoForFilesToBeUploaded addObject:uploadInfo];
            *stop = YES;
          }
        }];
      }

      // PDF has been uploaded when pm#asset-upload-pdf has been handled
      if ([uploadInfoForFilesToBeUploaded count] && [(MZFileUploadInfo*)uploadInfoForFilesToBeUploaded[0] isPDF])
      {
        // If it is a PDF just inform if it has only been partially accepted
        NSDictionary *file = files[0];
        if (file[@"pages"])
        {
          NSInteger pages = [file[@"pages"] intValue];
          NSInteger uids = [file[@"uids"] count];
          if (!uids)
          {
            NSString *description = [protein ingestForKey:@"description"];
            NSError *error = [NSError errorWithDomain:kMZCommunicatorErrorDomain
                                                 code:-1033
                                             userInfo:@{NSLocalizedDescriptionKey: description}];
            NSString *message = error.localizedDescription;
            weakCommunicator.systemModel.popupMessage = message;
          }
          else if (uids < pages)
          {
            weakCommunicator.systemModel.popupMessage = [NSString stringWithFormat:MZLocalizedString(@"MZCommunicator Upload Error Portofolio Full", nil), (long)uids, (long)pages];
          }
        }
      }
      else
      {
        if (uids.count < files.count)
        {
          // Not all files can be uploaded
          if (limitReachedBlock)
          {
            limitReachedBlock(__transaction, uids, ^(MZTransaction *aProgressTransaction, NSArray *assetUids, BOOL accepted) {
              if (accepted)
              {
                MZFileBatchUploadRequestTransaction *progressTransaction = (MZFileBatchUploadRequestTransaction*)aProgressTransaction;

                // Update the total amount of images that will be uploaded and the uploadInfo array items
                [uploadInfoForFilesToBeUploaded removeObjectsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange([assetUids count], uploadInfoForFilesToBeUploaded.count - [assetUids count])]];
                progressTransaction.numberOfUploads = [assetUids count];
                progressTransaction.fileUploadInfos = uploadInfoForFilesToBeUploaded;

                // Add the transaction to the progressive ones.
                [weakSelf.uploadTransactions addObject:progressTransaction];
                [weakSelf beginFilesUpload:uploadInfoForFilesToBeUploaded requestTransaction:progressTransaction success:singleUploadBlock];
              }
              else
              {
                for (NSString *uid in assetUids)
                {
                  NSArray *items = [weakCommunicator.systemModel.currentWorkspace.presentation slidesWithContentSource:uid];
                  for (MZPortfolioItem *item in items)
                    [weakCommunicator.requestor requestPortfolioItemDeleteRequest:weakCommunicator.systemModel.currentWorkspace.uid uid:item.uid];
                }
              }
            });
          }
        }
        else
        {
          [weakSelf.uploadTransactions addObject:__transaction];
          __transaction.numberOfUploads = uids.count;
          [weakSelf beginFilesUpload:uploadInfoForFilesToBeUploaded requestTransaction:__transaction success:singleUploadBlock];
        }
      }
    }

    // We are no longer interested in that information.
    [self.pendingFileUploads removeObjectsInArray:fileUploadInfos];
  };

  transaction.transactionId = protein.transactionId;
  [_communicator beginTransaction:transaction];

  [self.pendingFileUploads addObjectsFromArray:fileUploadInfos];

  return transaction;
}


- (void)beginFilesUpload:(NSArray*)fileUploadInfos requestTransaction:(MZFileBatchUploadRequestTransaction*)requestTransaction success:(void (^)(void))successBlock
{
  for (MZFileUploadInfo *fileUploadInfo in fileUploadInfos)
  {
    // Queueing up a mass of uploads seems to cause some problems, the following is the attempt to limit
    // number of concurrent uploads. If anything this saves memory usage
    self.numberOfUploadsInProgress = 0;
    static NSInteger kMaximumNumberOfConcurrentUploads = 3;

    NSBlockOperation *operation = [[NSBlockOperation alloc] init];
    __weak NSBlockOperation *weakOperation = operation;
    __weak typeof (self) weakSelf = self;

    [operation addExecutionBlock:^{

      typeof (self) strongSelf = weakSelf;

      if (weakOperation.isCancelled)
        return;

      strongSelf.numberOfUploadsInProgress++;
      DLog(@"numberOfUploadsInProgress = %ld", (long)strongSelf.numberOfUploadsInProgress);

      while (strongSelf.numberOfUploadsInProgress > kMaximumNumberOfConcurrentUploads)
      {
        if (weakOperation.isCancelled)
          return;

        [NSThread sleepForTimeInterval:0.05];
      }

      if (fileUploadInfo.isPDF)
      {
        DLog(@"PDF has already been sent. We shouldn't be here");
        return;
      }

      if (weakOperation.isCancelled)
        return;

      NSString *format = fileUploadInfo.format;

      // This should theoretically always be the case, where an image should only have one uid, so I'm electing not to
      NSString *uid = fileUploadInfo.assetUids[0];
      MZTransaction *transaction = nil;

      if (fileUploadInfo.imageData)
      {
        transaction = [weakSelf beginImageDataUpload:fileUploadInfo.imageData format:format fileUid:fileUploadInfo.fileUid assetUid:uid workspaceUid:requestTransaction.workspaceUid];
      }
      else if (fileUploadInfo.imageDataBlock)
      {
        NSData *imageData = fileUploadInfo.imageDataBlock ();
        transaction = [weakSelf beginImageDataUpload:imageData format:format fileUid:fileUploadInfo.fileUid assetUid:uid workspaceUid:requestTransaction.workspaceUid];
      }
      else if (fileUploadInfo.imageBlock)
      {
        MZImage *image = fileUploadInfo.imageBlock ();
        transaction = [weakSelf beginImageUpload:image format:format fileUid:fileUploadInfo.fileUid assetUid:uid workspaceUid:requestTransaction.workspaceUid];
      }
      else
      {
        DAssert(NO, @"fileUploadInfo is neither PDF or image, something went wrong!");
      }

      [requestTransaction.transactionsInProgress addObject:transaction];

      __weak MZTransaction *weakTransaction = transaction;
      transaction.endedBlock = ^{
        strongSelf.numberOfUploadsInProgress--;
        requestTransaction.completedUploads++;
        [requestTransaction.transactionsInProgress removeObject:weakTransaction];
        if (requestTransaction.completedUploads == requestTransaction.numberOfUploads)
          [weakSelf.uploadTransactions removeObject:requestTransaction];

        if (successBlock)
          successBlock();
      };

    }];

    if (!self.imageUploadQueue)
    {
      self.imageUploadQueue = [[NSOperationQueue alloc] init];
      [self.imageUploadQueue setName:@"com.ob.mezzkit.imageUpload"];
      [self.imageUploadQueue setMaxConcurrentOperationCount:1];
    }

    [self.imageUploadQueue addOperation:operation];

  }
}

// Manual clean up of failed image uploads in a batch upload request transactions
- (void)removeTransactionForFailedUploadAssetUID:(NSString *)uid
{
  MZFileBatchUploadRequestTransaction *completedRequestTransaction;
  for (MZFileBatchUploadRequestTransaction *requestTransaction in self.uploadTransactions)
  {
    MZImageUploadTransaction *transactionForAssetUid;
    for (MZImageUploadTransaction *transactionInProgress in requestTransaction.transactionsInProgress)
    {
      if ([transactionInProgress.uid isEqualToString:uid])
      {
        transactionForAssetUid = transactionInProgress;
        self.numberOfUploadsInProgress--;
        requestTransaction.completedUploads++;
        break;
      }
    }

    if (transactionForAssetUid)
    {
      [requestTransaction.transactionsInProgress removeObject:transactionForAssetUid];
      if (requestTransaction.completedUploads == requestTransaction.numberOfUploads)
        completedRequestTransaction = requestTransaction;
    }
  }

  [self.uploadTransactions removeObject:completedRequestTransaction];
}


- (MZImageUploadTransaction *)beginImageUpload:(MZImage*)image format:(NSString*)format fileUid:(NSInteger)fileUid assetUid:(NSString*)assetUid workspaceUid:(NSString*)workspaceUid
{
  NSData *imageData = nil;

#if TARGET_OS_IPHONE
  if ([format isEqual:@"jpg"] || [format isEqual:@"public.jpeg"])
    imageData = UIImageJPEGRepresentation(image, 0.9);
  else if ([format isEqual:@"png"] || [format isEqual:@"public.png"])
    imageData = UIImagePNGRepresentation(image);
#else
  if ([format isEqual:@"jpg"] || [format isEqual:@"public.jpeg"])
    imageData = [NSBitmapImageRep representationOfImageRepsInArray:[image representations] usingType:NSJPEGFileType properties:[NSDictionary dictionaryWithObject:[NSDecimalNumber numberWithFloat:0.9] forKey:NSImageCompressionFactor]];
  else if ([format isEqual:@"png"] || [format isEqual:@"public.png"])
    imageData = [NSBitmapImageRep representationOfImageRepsInArray:[image representations] usingType:NSPNGFileType properties:@{}];
#endif

  return [self beginImageDataUpload:imageData format:format fileUid:fileUid assetUid:assetUid workspaceUid:workspaceUid];
}


- (MZImageUploadTransaction *)beginImageDataUpload:(NSData*)imageData format:(NSString*)format fileUid:(NSInteger)fileUid assetUid:(NSString*)assetUid workspaceUid:(NSString*)workspaceUid
{
  MZImageUploadTransaction *transaction = [MZImageUploadTransaction transactionWithProtein:nil];
  transaction.uid = assetUid;
  transaction.workspaceUid = workspaceUid;
  transaction.format = format;

  NSAssert([format length] > 0, @"format can't be nil");

  static NSInteger filenameCounter = 1;
  NSString *pretendFilename = [NSString stringWithFormat:@"%@-%ld.%@", _communicator.machineName, (long)filenameCounter++, format];

  if (imageData)
  {
    DLog(@"Uploading %@ image with size %lu", format, (unsigned long)imageData.length);

    // Data is sent as part of a POST request, not the protein anymore.
    OBProtein *protein = [_communicator.proteinFactory assetUploadImageReadyRequest:workspaceUid uid:assetUid fileId:fileUid fileName:pretendFilename data:[NSData new]];

    // Store the transaction ID, but not the actual protein because that'll occupy a lot of memory
    transaction.transactionId = protein.transactionId;

    [self httpsUploadFiledata:imageData protein:protein mimeType:[NSString stringWithFormat:@"image/%@", format] filename:pretendFilename];

    [_communicator.pendingTransactions addObject:transaction];
  }

  return transaction;
}

- (void)cancelAllPendingFileUploads
{
  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"transactionName == %@ OR transactionName == %@",
                            MZImageUploadRequestTransactionName,
                            MZImageUploadTransactionName];
  NSArray *allUploadTransactions = [_communicator.systemModel.pendingTransactions filteredArrayUsingPredicate:predicate];
  for (MZImageUploadTransaction *transaction in allUploadTransactions)
  {
    [_communicator cancelTransaction:transaction];
  }

  [_imageUploadQueue cancelAllOperations];
  [_imageUploadQueue setSuspended:NO];
  [_uploadTransactions removeAllObjects];
}

- (void)clearPendingUploadTransactionsForAssetUid:(NSString *)assetUid
{
  MZSystemModel *systemModel = _communicator.systemModel;

  NSArray *uploadTransactionsInPendingTransactions = [systemModel.pendingTransactions filteredArrayUsingPredicate:
                                                      [NSPredicate predicateWithFormat:@"transactionName == %@", MZFilesUploadRequestTransactionName]];
  NSArray *uploadTransactionsInProgress = [self.uploadTransactions filteredArrayUsingPredicate:
                                           [NSPredicate predicateWithFormat:@"transactionName == %@", MZFilesUploadRequestTransactionName]];
  NSArray *totalUploadTransactions = [uploadTransactionsInPendingTransactions arrayByAddingObjectsFromArray:uploadTransactionsInProgress];

  BOOL transactionFound = NO;
  for (MZFileBatchUploadRequestTransaction *transaction in totalUploadTransactions)
  {
    for (MZFileUploadInfo *uploadInfo in transaction.fileUploadInfos)
    {
      for (NSString *uid in uploadInfo.assetUids)
      {
        if ([uid isEqualToString:assetUid])
        {
          uploadInfo.completed = YES;
          transactionFound = YES;
          break;
        }
      }

      if (transactionFound)
        break;
    }

    if (transaction.completed)
    {
      [transaction ended];

      NSInteger index = [systemModel.pendingTransactions indexOfObject:transaction];
      if (index != NSNotFound)
        [systemModel removeObjectFromPendingTransactionsAtIndex:index];

      index = [self.uploadTransactions indexOfObject:transaction];
      if (index != NSNotFound)
        [self.uploadTransactions removeObjectAtIndex:index];
    }
  }


  uploadTransactionsInPendingTransactions = [systemModel.pendingTransactions filteredArrayUsingPredicate:
                                             [NSPredicate predicateWithFormat:@"transactionName == %@", MZImageUploadTransactionName]];
  for (MZImageUploadTransaction *transaction in uploadTransactionsInPendingTransactions)
  {
    if ([transaction.uid isEqual:assetUid])
    {
      [transaction ended];

      NSInteger index = [systemModel.pendingTransactions indexOfObject:transaction];
      [systemModel removeObjectFromPendingTransactionsAtIndex:index];
    }
  }
}


#pragma mark PDF

- (MZTransaction*)requestPDFUpload:(NSData *)data fileid:(NSInteger)fileid filename:(NSString *)filename
{
  // Data is sent as part of a POST request, not the protein anymore.
  OBProtein *protein = [_communicator.proteinFactory assetUploadPdfReadyRequest:fileid data:[NSData new]];
  protein.freeIngestsDuringDeposit = YES; // To release as much memory as early as possible (bug 9275)

  MZTransaction *transaction = [MZTransaction transactionWithProtein:protein];
  transaction.requestPoolName = MEZZ_IMAGE_DEPOSIT_POOL;
  transaction.transactionName = MZTransactionNameAssetUploadPDFReady;

  transaction.transactionId = protein.transactionId;
  transaction.dataSize = [data length];

  [self httpsUploadFiledata:data protein:protein mimeType:@"application/pdf" filename:filename];

  // Theoretically this should be part of our pending transactions list
  // but there's currently no way to pair this with any responses
  // in order to clear it, and thus memory will persist and leak
  //[pendingTransactions addObject:transaction];

  return transaction;
}


- (void)beginPDFUploadForFiles:(NSArray *)fileids
{
  NSMutableArray *uploadedFiles = [NSMutableArray array];
  for (NSNumber *fileidNumber in fileids)
  {
    for (MZFileUploadInfo *uploadInfo in self.pendingFileUploads)
    {
      NSInteger fileId = [fileidNumber integerValue];
      if (uploadInfo.fileUid == fileId)
      {
        NSURL *fileURL = uploadInfo.fileURL;
        NSData *pdfData = [NSData dataWithContentsOfURL:fileURL];
        [self requestPDFUpload:pdfData fileid:fileId filename:uploadInfo.fileName];
        [uploadedFiles addObject:uploadInfo];
        break;
      }
    }
  }
  [self.pendingFileUploads removeObjectsInArray:uploadedFiles];
}


#pragma mark HTTPS Upload

- (void)httpsUploadFiledata:(NSData *)imageData protein:(OBProtein *)protein mimeType:(NSString *)format filename:(NSString *)filename
{
  if (!_sessionManager)
    [self configureSessionManager];

  __block NSString *filenameBlock = filename;
  if (!filenameBlock || [filenameBlock length] == 0)
    filenameBlock = protein.ingests[@"file-name"] ? protein.ingests[@"file-name"] : @"untitledFile";

  // The new JSON protein must NOT contain the file data. It is sent as the
  // filedata part of the POST request instead of part of the JSON message.
  // MZProto should be revised in order to avoid this small adjustment of the protein.
  NSMutableDictionary *ingests = [NSMutableDictionary dictionaryWithDictionary:protein.ingests];
  [ingests removeObjectForKey:@"data"];
  OBJSONProtein *lightProtein = [OBJSONProtein proteinWithDescrips:protein.descrips ingests:ingests];

  __block NSData *lightProteinData = lightProtein.jsonData;
  __block NSData *imageDataBlock = imageData;

  // If we can't describe what we are sending, don't waste bandwidth
  if (!lightProteinData)
    return;

  void (^formDataBlock)(id<AFMultipartFormData>) = ^(id<AFMultipartFormData> formData) {
    // Message
    [formData appendPartWithFormData:lightProteinData name:@"protein"];

    // Image data
    [formData appendPartWithFileData:imageDataBlock name:@"Filedata" fileName:filenameBlock mimeType:format];

    // Pool name
    [formData appendPartWithFormData:[@"mz-incoming-assets" dataUsingEncoding:NSUTF8StringEncoding] name:@"pool"];
  };

  AFHTTPRequestSerializer *serializer = [AFHTTPRequestSerializer serializer];
  NSMutableURLRequest *request = [serializer multipartFormRequestWithMethod:@"POST"
                                                                  URLString:[_communicator httpImageUploadURL].absoluteString
                                                                 parameters:nil
                                                  constructingBodyWithBlock:formDataBlock
                                                                      error:nil];

  NSURLSessionUploadTask *uploadTask = [_sessionManager
                                        uploadTaskWithStreamedRequest:request
                                        progress:nil
                                        completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                                          if (error) {
                                            DLog(@"Error: %@", error);
                                          } else {
                                            // Do nothing for now
                                          }
                                        }];

  [uploadTask resume];
}


#pragma mark Pending Upload Items

- (NSInteger)numberOfImagesBeingUploaded
{
  NSInteger numberOfImageUploads = 0;
  NSMutableArray *pendingTransactiosNotInProgress = [[NSMutableArray alloc] initWithArray:_communicator.pendingTransactions];

  for (MZTransaction *transaction in self.uploadTransactions)
  {
    if ([transaction isKindOfClass:[MZImageBatchUploadRequestTransaction class]])
    {
      MZImageBatchUploadRequestTransaction *progressTransaction = (MZImageBatchUploadRequestTransaction *) transaction;
      numberOfImageUploads += progressTransaction.numberOfImages - progressTransaction.completedImageUploads;
      [pendingTransactiosNotInProgress removeObjectsInArray:progressTransaction.transactionsInProgress];
    }
    else if ([transaction.transactionName isEqualToString:MZFilesUploadRequestTransactionName])
    {
      MZFileBatchUploadRequestTransaction *progressTransaction = (MZFileBatchUploadRequestTransaction *) transaction;

      // Add up only the batch uploads that contain images. Since all items in a batch are the same we can just check the first one of them.
      NSArray *uploadInfos = progressTransaction.fileUploadInfos;
      if ([uploadInfos count] && ![((MZFileUploadInfo *)uploadInfos[0]) isPDF])
        numberOfImageUploads += progressTransaction.numberOfUploads - progressTransaction.completedUploads;

      [pendingTransactiosNotInProgress removeObjectsInArray:progressTransaction.transactionsInProgress];
    }
  }

  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"transactionName == %@ OR transactionName == %@",
                            MZTransactionNameAssetUploadProvision,
                            MZTransactionNameAssetUploadImageReady];
  NSArray *pendingImageTransactionsNotInProgress = [pendingTransactiosNotInProgress filteredArrayUsingPredicate:predicate];

  numberOfImageUploads += [pendingImageTransactionsNotInProgress count];

  return numberOfImageUploads;
}


- (NSInteger)numberOfFilesBeingUploaded
{
  NSInteger numberOfFileUploads = 0;
  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"transactionName == %@", MZFilesUploadRequestTransactionName];
  NSArray *pendingFilesToBeUploaded = [_communicator.pendingTransactions filteredArrayUsingPredicate:predicate];

  for (MZFileBatchUploadRequestTransaction *transaction in pendingFilesToBeUploaded)
  {
    NSArray *uploadInfos = transaction.fileUploadInfos;
    if ([uploadInfos count] && [((MZFileUploadInfo *)uploadInfos[0]) isPDF])
      numberOfFileUploads++;
  }

  return numberOfFileUploads;
}

- (NSInteger)numberOfUploadTransactions
{
  return self.uploadTransactions.count;
}


#pragma mark - Private

- (void)configureSessionManager
{
  NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
  _sessionManager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];

  _sessionManager.securityPolicy.allowInvalidCertificates = NO;

  // If we don't accept those content types we'll have an error (even if the request it's a success)
  AFHTTPResponseSerializer *responseSerializer = [AFHTTPResponseSerializer serializer];
  [responseSerializer setAcceptableContentTypes:[NSSet setWithObjects:@"text/html", @"text/plain", nil]];
  _sessionManager.responseSerializer = responseSerializer;

  [_sessionManager setSessionDidReceiveAuthenticationChallengeBlock:^NSURLSessionAuthChallengeDisposition(NSURLSession * _Nonnull session, NSURLAuthenticationChallenge * _Nonnull challenge, NSURLCredential *__autoreleasing  _Nullable * _Nullable credential) {

    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
    {
      MZSecurityManager *securityManager = [MZSecurityManager new];
      BOOL protected = [securityManager authenticateAgainstProtectionSpace:challenge.protectionSpace];

      if (protected)
      {
        *credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
        return NSURLSessionAuthChallengeUseCredential;
      }
      else
      {
        return NSURLSessionAuthChallengeCancelAuthenticationChallenge;
      }
    }

    return NSURLSessionAuthChallengePerformDefaultHandling;
  }];
}


@end
