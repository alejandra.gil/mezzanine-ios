//
//  MZWorkspace.m
//  Mezzanine
//
//  Created by Zai Chang on 10/20/10.
//  Copyright 2014 Oblong Industries. All rights reserved.
//

#import "MZWorkspace.h"
#import "NSObject+KVCAdditions.h"
#import "NSMutableArray+Additions.h"

@implementation MZWorkspace

@synthesize uid;
@synthesize name;
@synthesize thumbURL;
@synthesize modifiedDate;
@synthesize modifiedDateString;
@synthesize owner;

@synthesize windshield;
@synthesize liveStreams;

@synthesize isSingleFeld;

@synthesize isLoading;
@synthesize isPendingDeletion;
@synthesize isBeingEdited;

-(id) init
{
  if ((self = [super init]))
  {
    _presentation = [MZPresentation new];
    liveStreams = [NSMutableArray new];
    windshield = [MZWindshield new];
    isPendingDeletion = NO;
    isBeingEdited = NO;
  }
  return self;
}

-(void) dealloc
{
  modifiedDate = nil;
}

-(NSString*) description
{
  return [NSString stringWithFormat:@"MZWorkspace %p: name=%@", self, name];
}


+ (NSString*)createDefaultWorkspaceName
{
  static NSDateFormatter *dateFormatter = nil;
  if (dateFormatter == nil)
  {
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
  }
  return [NSString stringWithFormat:@"{ workspace %@ }",[dateFormatter stringFromDate:[NSDate date]]];
}


#pragma mark - Presentation

- (void)setPresentation:(MZPresentation *)presentation
{
  if (_presentation != presentation)
  {
    _presentation = presentation;
    _presentation.workspace = self;
  }
}



#pragma mark - Live Streams

KVO_MUTABLEARRAY_ACCESSOR_IMPLEMENTATION(LiveStreams, liveStreams, liveStreams)

-(MZLiveStream*) liveStreamWithUid:(NSString*)anUid
{
  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uid == %@", anUid];
  NSArray *results = [liveStreams filteredArrayUsingPredicate:predicate];
  if (results.count > 0)
    return results[0];
  return nil;
}

-(void) removeAllLiveStreams
{
  NSInteger count = [liveStreams count];
  if (count > 0)
    [self removeLiveStreamsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, count)]];
}


#pragma mark - Modified Date

-(void) updateModifiedDateString
{
  if (!self.modifiedDate)
    return;
  
  static NSDateFormatter *formatter = nil;
  if (formatter == nil)
  {
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yy hh:mm"];
  }

  self.modifiedDateString = [formatter stringFromDate:self.modifiedDate];
}

-(void) setModifiedDate:(NSDate *)date
{
  modifiedDate = date;
  [self updateModifiedDateString];
}


#pragma mark - Thumbnail

-(NSString*) dynamicThumbURL
{
  if (thumbURL == nil || [thumbURL isEqualToString:@""])
    return nil;
  
  if (self.modifiedDate)
  {
    return [thumbURL stringByAppendingFormat:@"?q=%ld", (long)[self.modifiedDate timeIntervalSinceReferenceDate]];
  }
  else if (self.modifiedDateString)
  {
    NSString *query = [self.modifiedDateString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    return [thumbURL stringByAppendingFormat:@"?q=%@", query];
  }
  else 
  {
    return thumbURL;
  }
}


#pragma mark - Protein Handling

- (void)updateWithDictionary:(NSDictionary*)dictionary
{
  if (!dictionary)
    return;
  
  if (self.uid && ![self.uid isEqualToString:dictionary[@"uid"]])
  {
    DLog(@"MZWorkspace updateWithDictionary: uid %@ does not match existing uid %@", dictionary[@"uid"], self.uid);
    return;
  }

  // 'owner' ingest can be a list of owners or only one.
  id owners = dictionary[@"owners"];
  NSString *anOwner = nil;
  if ([owners isKindOfClass:[NSString class]])
    anOwner = dictionary[@"owners"];
  else if ([owners isKindOfClass:[NSArray class]] && [owners count])
    anOwner = owners[0];
  if (anOwner)
    [self setValueIfChanged:anOwner forKey:@"owner"];

  NSString *aName = dictionary[@"name"];
  if (aName && aName != (id)[NSNull null] && ![self.name isEqual:aName])
    self.name = aName;
  
  NSNumber *modDateUTC = dictionary[@"mod-date-utc"];
  if (modDateUTC)
  {
    NSDate *newDate = [NSDate dateWithTimeIntervalSince1970:[modDateUTC integerValue]];
    if (![self.modifiedDate isEqual:newDate])
      self.modifiedDate = newDate;
  }
  else
  {
    // Only try the mod-date string if timestamp isn't available
    NSString *modDateString = dictionary[@"mod-date"];
    if (modDateString && ![self.modifiedDateString isEqual:modDateString])
    {
      // NSString for now, but should really be NSDate
      self.modifiedDateString = modDateString;
    }
  }
  
  NSString *url = dictionary[@"thumb-uri"];
  if (url && url != (id)[NSNull null] && ![self.thumbURL isEqual:url])
  {
    self.thumbURL = url;
  }

  NSNumber *openingNumber = dictionary[@"opening"];
  if (openingNumber && self.isLoading != openingNumber.boolValue)
    self.isLoading = openingNumber.boolValue;
  
  NSNumber *savedNumber = dictionary[@"saved"];
  if (savedNumber && self.isSaved != savedNumber.boolValue)
    self.isSaved = savedNumber.boolValue;

  // The following is used only in the case of a workspace being discarded
  NSNumber *destroyedNumber = dictionary[@"destroyed"];
  if (destroyedNumber && self.wasDestroyed != destroyedNumber.boolValue)
    self.wasDestroyed = destroyedNumber.boolValue;


  // Live Streams
  [self updateLiveStreamsWithDictionary:dictionary];


  // Presentation
  NSArray *portfolioItems = dictionary[@"portfolio-items"];
  if (portfolioItems)
    [self.presentation updateWithPortfolioItems:portfolioItems];

  // Update presentation state after updating porfolio items, because some logic below depends on numberOfSlides
  [self.presentation updateWithDictionary:dictionary[@"presentation"]];

  
  // Windshield
  NSArray *windshieldItemsArray = dictionary[@"windshield-items"];
  if (windshieldItemsArray)
    [self.windshield updateWithWindshieldItems:windshieldItemsArray];

}


- (void)updateLiveStreamsWithDictionary:(NSDictionary*)dictionary
{
  NSArray *liveStreamsArray = dictionary[@"live-streams"];
  if (liveStreamsArray)
  {
    for (NSDictionary *liveStreamDict in liveStreamsArray)
    {
      NSString *aUid = liveStreamDict[@"source-id"];
      MZLiveStream *liveStreamItem = [self liveStreamWithUid:aUid];
      // Use the live stream index in the protein to sync the order of local
      // live streams shown in the the native UI with the one used in the client bin
      NSInteger liveStreamIndex = [liveStreamsArray indexOfObject:liveStreamDict];
      if (!liveStreamItem)
      {
        liveStreamItem = [[MZLiveStream alloc] init];
        liveStreamItem.uid = aUid;
        [liveStreamItem updateWithDictionary:liveStreamDict];
        [self insertObject:liveStreamItem inLiveStreamsAtIndex:liveStreamIndex];
      }
      else
      {
        [liveStreams moveObjectAtIndex:[liveStreams indexOfObject:liveStreamItem] toIndex:liveStreamIndex];
        [liveStreamItem updateWithDictionary:liveStreamDict];
      }
    }
    
    NSMutableIndexSet *itemsToRemove = [NSMutableIndexSet indexSet];
    for (MZLiveStream *stream in self.liveStreams)
    {
      BOOL found = NO;
      
      for (NSDictionary *liveStreamDict in liveStreamsArray)
      {
        NSString *sourceId = liveStreamDict[@"source-id"];
        if ([sourceId isEqualToString:stream.uid])
        {
          found = YES;
          break;
        }
      }

      // Remote LiveStreams used by windshield or portfolio items
      // are not included in live-stream-detail protein so this removal is only
      // valid for local liveStreams
      // The remote ones are removed from the list in:
      // - (void)removeRemoteLiveStreamsWithContentSource:(NSString*)contentSource
      if (!found && stream.local)
      {
        // Instead of removing MZLiveStreams below, we need to keep that instance around because those streams that get removed
        // are MZReach streams, and they can disconnect and then later reconnect. We need to prevent various views from being
        // left with orphaned model objects. See bug 11920
        /*
        NSUInteger index = [self.liveStreams indexOfObject:stream];
        [itemsToRemove addIndex:index];
         */
        stream.active = NO; // Removes it from being visible in LiveStreamsViewController
      }
    }
    
    if (itemsToRemove.count > 0)
      [self removeLiveStreamsAtIndexes:itemsToRemove];
  }

  NSArray *windshieldItemsArray = dictionary[@"windshield-items"];
  if (windshieldItemsArray)
  {
    for (NSDictionary *windshieldItem in windshieldItemsArray)
    {
      NSString *sourceId = windshieldItem[@"content-source"];

      if (![sourceId hasPrefix:@"vi-"])
        continue;

      NSArray *liveStreamsArray = [self.liveStreams filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.uid == %@", sourceId]];
      if (!liveStreamsArray || [liveStreamsArray count] == 0)
      {
        MZLiveStream *liveStreamItem = [[MZLiveStream alloc] init];
        liveStreamItem.uid = sourceId;
        [liveStreamItem updateWithDictionary:windshieldItem];
        [self insertObject:liveStreamItem inLiveStreamsAtIndex:self.liveStreams.count];
      }
    }
  }

  NSArray *portfolioItemsArray = dictionary[@"portfolio-items"];
  if (portfolioItemsArray)
  {
    for (NSDictionary *portfolioItem in portfolioItemsArray)
    {
      NSString *sourceId = portfolioItem[@"content-source"];

      if (![sourceId hasPrefix:@"vi-"])
        continue;

      NSArray *liveStreamsArray = [self.liveStreams filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.uid == %@", sourceId]];
      if (!liveStreamsArray || [liveStreamsArray count] == 0)
      {
        MZLiveStream *liveStreamItem = [[MZLiveStream alloc] init];
        liveStreamItem.uid = sourceId;
        [liveStreamItem updateWithDictionary:portfolioItem];
        [self insertObject:liveStreamItem inLiveStreamsAtIndex:self.liveStreams.count];
      }
    }
  }
}

// Updates the live streams information with information stored in windshield or portfolio items.
- (void)updateLiveStreamsWithDictionary:(NSDictionary*)dictionary andRemoteMezzes:(NSArray *)remoteMezzes
{
  NSString *contentSource = dictionary[@"content-source"];

  // Examples of video content-source:
  //
  //  local: vi-bmagic-0/_local_
  //  VTC: vi-teleconf/_vtc_
  //  remote: vi-bmagic-2/8vW5f_VlRv68dzVrYp7ibQ

  if ([contentSource hasPrefix:@"vi-"])
  {
    NSArray *liveStreamsArray = [self.liveStreams filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.uid == %@", contentSource]];

    if (!liveStreamsArray || [liveStreamsArray count] == 0)
    {
      MZLiveStream *liveStream = [[MZLiveStream alloc] init];
      liveStream.uid = contentSource;
      liveStream.displayName = dictionary[@"display-name"];

      // With a remote owner, we look for the name of the remote Mezz
      [self updateLiveStreamRemoteMezzName:liveStream usingRemoteMezzes:remoteMezzes];

      [self insertObject:liveStream inLiveStreamsAtIndex:self.liveStreams.count];

    }
  }
}


- (void)updateLiveStreamRemoteMezzName:(MZLiveStream*)liveStream usingRemoteMezzes:(NSArray*)remoteMezzes
{
  if (!liveStream.local)
  {
    NSArray *split = [liveStream.uid componentsSeparatedByString:@"/"];
    NSString *streamOwner = [split lastObject];
    NSArray *validRemoteMezz = [remoteMezzes filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.uid == %@", streamOwner]];
    liveStream.remoteMezzName = [validRemoteMezz count] > 0 ? [validRemoteMezz[0] name] : nil;
  }
}


- (void)removeRemoteLiveStreamsWithContentSource:(NSString*)contentSource
{
  if (![contentSource hasPrefix:@"vi-"])
    return;

  NSArray *split = [contentSource componentsSeparatedByString:@"/"];
  NSString *streamOwner = [split lastObject];

  // With a local owner
  if ([streamOwner isEqualToString:@"_local_"] || [streamOwner isEqualToString:@"_vtc_"])
    return;

  NSArray *shieldersWithRemoteLiveStream = [self.windshield.items filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.assetUid == %@", contentSource]];

  NSArray *portfolioItemsWithRemoteLiveStream = [self.presentation.slides filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.contentSource == %@", contentSource]];

  if (!shieldersWithRemoteLiveStream.count && !portfolioItemsWithRemoteLiveStream.count)
  {
    NSArray *liveStreamsArray = [self.liveStreams filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.uid == %@", contentSource]];

    if (liveStreamsArray.count > 0)
    {
      NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:[self.liveStreams indexOfObject:liveStreamsArray[0]]];
      [self removeLiveStreamsAtIndexes:indexSet];
    }
  }
}


- (void)removeUnusedRemoteLiveStreams
{
  NSArray *remoteLiveStreams = [self.liveStreams filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.local == NO"]];
  for (MZLiveStream *liveStream in remoteLiveStreams)
  {
    [self removeRemoteLiveStreamsWithContentSource:liveStream.uid];
  }
}


#pragma mark - Volatile States

- (BOOL)hasContent
{
  return (self.windshield.items.count > 0 || self.presentation.slides.count > 0);
}


+ (NSSet*)keyPathsForValuesAffectingHasContent
{
  // Such that hasContent fires off KVO notifications
  return [NSSet setWithObjects:@"windshield.items", @"presentation.slides", nil];
}


@end
