//
//  MZWhiteboard.m
//  MezzKit
//
//  Created by Zai Chang on 3/5/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import "MZWhiteboard.h"

@implementation MZWhiteboard

-(instancetype) initWithUid:(NSString*)aUid
{
  self = [super init];
  if (self)
  {
    self.uid = aUid;
  }
  return self;
}


@end
