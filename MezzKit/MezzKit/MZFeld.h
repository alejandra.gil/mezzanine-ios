//
//  MZFeld.h
//  Mezzanine
//
//  Created by Zai Chang on 5/18/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MZVect.h"


@interface MZFeld : NSObject
{
  NSString *name;
  NSString *type;
  MZVect *center;
  MZVect *norm;
  MZVect *over;
  MZVect *up;
  MZVect *physicalSize;
  MZVect *pixelSize;
  CGFloat viewDistance;
  
  BOOL visibleToAll;
}
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, strong) MZVect *center;
@property (nonatomic, strong) MZVect *norm;
@property (nonatomic, strong) MZVect *over;
@property (nonatomic, strong) MZVect *up;
@property (nonatomic, strong) MZVect *physicalSize;
@property (nonatomic, strong) MZVect *pixelSize;
@property (nonatomic, assign) CGFloat viewDistance;

// Collaboration properties
@property (nonatomic, assign) BOOL visibleToAll;

// Dynamic properties
@property (nonatomic, assign, readonly) CGFloat aspectRatio;

// 3.20 Visiblity
@property (nonatomic, assign) NSString *visibility;


+(MZFeld *) feld;
+(MZFeld *) feldWithName:(NSString*)aName;

+(NSMutableArray*) orderedFeldsArrayFromDictionary:(NSDictionary *)dictionary;
+(NSMutableArray*) orderedFeldsArrayFromDictionary:(NSDictionary *)dictionary includeInvisible:(BOOL)includeInvisible;

-(void) updateWithDictionary:(NSDictionary *)dictionary;
-(void) updateWithFeld:(MZFeld *)feld;

-(double) aspectRatio;
-(CGRect) feldRect;

@end
