//
//  MZWindshieldItem.m
//  Mezzanine
//
//  Created by Zai Chang on 5/8/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import "MZWindshieldItem.h"
#import "NSObject+KVCAdditions.h"
#import "MZSystemModel.h"


@implementation MZWindshieldItem

@synthesize assetUid;
@synthesize frame;
@synthesize ordinal;
@synthesize isBeingGrabbed;

@dynamic isImage;
@dynamic isVideo;

@synthesize displayInfo;


-(NSString*) description
{
  return [NSString stringWithFormat:@"MZWindshieldItem %p: uid=%@", self, uid];
}


-(void) updateWithDictionary:(NSDictionary*)dictionary
{
  // Properties like uid, thumb-uri, full-uri are handled by MZAsset
  [super updateWithDictionary:dictionary];

  // assetUid = content-source (>=2.12) or asset-uid (<=2.10)
  [self setValueIfNotNullAndChanged:dictionary[@"asset-uid"] forKey:@"assetUid"];
  [self setValueIfNotNullAndChanged:dictionary[@"content-source"] forKey:@"assetUid"];

  [self setValueIfNotNullAndChanged:dictionary[@"ordinal"] forKey:@"ordinal"];
  
  [self setValueIfNotNullAndChanged:dictionary[@"display-info"] forKey:@"displayInfo"];
  
  [self updateWithTransformDictionary:dictionary];
}


-(void) updateWithTransformDictionary:(NSDictionary*)dictionary
{
  NSDictionary *checkedRelativeCoordinatesDictionary = [self replaceMullionsWithMainFeld:dictionary[@"feld-relative-coords"]];

  _feldRelativeCoordinates = checkedRelativeCoordinatesDictionary;
  _feldRelativeSize = dictionary[@"feld-relative-size"];

  _feld = [_surface feldWithName:checkedRelativeCoordinatesDictionary[@"feld-id"]];

  if (_feldRelativeCoordinates && _feldRelativeSize)
    [self updateFrame];
}


- (void)updateFrame
{
  if (_feld == nil)
    return; // Feld information hasn't arrived yet, ignore
  
  // 2.12
  NSNumber *x = _feldRelativeCoordinates[@"over"];
  NSNumber *y = _feldRelativeCoordinates[@"up"];
  NSNumber *width;
  NSNumber *height;
  
  CGFloat scale = [_feldRelativeSize[@"scale"] floatValue];
  _aspectRatio = [_feldRelativeSize[@"aspect-ratio"] floatValue];

  CGFloat feldPhysicalWidth = self.feld.feldRect.size.width;
  CGFloat feldPhysicalHeight = self.feld.feldRect.size.height;

  // Get width and height of image using scale (from feld) and image aspect ratio
  CGFloat w = 0;
  CGFloat h = 0;
  if (_aspectRatio > _feld.aspectRatio)
  {
    w = feldPhysicalWidth * scale;
    h = feldPhysicalWidth * scale / _aspectRatio;
  }
  else
  {
    w = feldPhysicalHeight * scale * _aspectRatio;
    h = feldPhysicalHeight * scale;
  }

  // Set its relative value (0..1) to keep the relativeness of the model used in <2.10
  width = @(w / feldPhysicalWidth);
  height = @(h / feldPhysicalHeight);

  if (x && y && width && height)
  {
    // Coelescing into one KVO
    self.frame = CGRectMake([x doubleValue], [y doubleValue], [width doubleValue], [height doubleValue]);
  }
}


-(BOOL) isImage
{
  return [assetUid hasPrefix:@"as-"];
}


-(BOOL) isVideo
{
  return [assetUid hasPrefix:@"vi-"];
}


- (void)updateSurfaceAndFeldWithDictionary:(NSDictionary*)dictionary usingSystemModel:(MZSystemModel*)systemModel
{
  _feldRelativeCoordinates = [self replaceMullionsWithMainFeld:dictionary[@"feld-relative-coords"]];
  _feldRelativeSize = dictionary[@"feld-relative-size"];

  BOOL changed = NO;
  // Setting main as default surface to help backward-compatibility with 2.12
  NSString *surfaceName = dictionary[@"surface-name"] ? dictionary[@"surface-name"] : @"main";

  MZSurface *surfaceFromSystemModel = [systemModel surfaceWithName:surfaceName];
  if (surfaceFromSystemModel != _surface)
  {
    self.surface = surfaceFromSystemModel;
    changed = YES;
  }

  MZFeld *feldFromSystemModel = [surfaceFromSystemModel feldWithName:[self replaceMullionsWithMainFeld:dictionary[@"feld-relative-coords"]][@"feld-id"]];
  if (feldFromSystemModel != _feld)
  {
    self.feld = feldFromSystemModel;
    changed = YES;
  }

  if (changed)
    [self updateFrame];
}


- (NSDictionary*)dictionaryRepresentation
{
  // N.B. currently only used for tests, not production ready serialization code
  
  NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
  if (self.uid)
    dictionary[@"uid"] = self.uid;
  if (self.assetUid)
    dictionary[@"asset-uid"] = self.assetUid;
  if (self.contentSource)
    dictionary[@"content-source"] = self.contentSource;

  dictionary[@"ordinal"] = @(self.ordinal);
  return dictionary;
}

-(CGFloat) scale
{
  return [_feldRelativeSize[@"scale"] floatValue];
}


#pragma mark - Mullion Replacement Helpers

// This hack is necessary because mobile clients do not represent or even consider mullions for their
// UI layout, or positions, but native sends coordinates that can be based on left and right mullions
// in 2.12 and 3.0 Mezz Systems.
- (NSDictionary *)replaceMullionsWithMainFeld:(NSDictionary *)originalFeldRelativeCoordinates
{
  if ([originalFeldRelativeCoordinates[@"feld-id"] isEqualToString:@"left-mullion"])
  {
    NSMutableDictionary *newCoordinates = originalFeldRelativeCoordinates.mutableCopy;
    newCoordinates[@"feld-id"] = @"main";
    newCoordinates[@"over"] = @(-0.5);
    return newCoordinates;
  }
  else if ([originalFeldRelativeCoordinates[@"feld-id"] isEqualToString:@"right-mullion"])
  {
    NSMutableDictionary *newCoordinates = originalFeldRelativeCoordinates.mutableCopy;
    newCoordinates[@"feld-id"] = @"main";
    newCoordinates[@"over"] = @(0.5);
    return newCoordinates;
  }

  return originalFeldRelativeCoordinates;
}

- (void)setFeldRelativeCoordinates:(NSDictionary *)feldRelativeCoordinates
{
  _feldRelativeCoordinates = [self replaceMullionsWithMainFeld:feldRelativeCoordinates];
}


@end
