//
//  MZHandlerFactory.h
//  MezzKit
//
//  Created by Zai Chang on 6/4/15.
//  Copyright (c) 2015 Oblong Industries. All rights reserved.
//

#import "MZHandlerFactoryBase.h"


@interface MZHandlerFactory : MZHandlerFactoryBase

-(OBMessageHandler*) createClientJoinHandler;
-(OBMessageHandler*) createClientLeaveHandler;
-(OBMessageHandler*) createClientEvictHandler;

-(OBMessageHandler*) createMezzanineCapabilitiesHandler;

-(OBMessageHandler*) createHeartbeatHandler;
-(OBMessageHandler*) createMezzanineAwakeHandler;

// Workspace Handlers
-(OBMessageHandler*) createWorkspaceStateHandler;
-(OBMessageHandler*) createWorkspaceOpenHandler;
-(OBMessageHandler*) createWorkspaceCloseHandler;
-(OBMessageHandler*) createWorkspaceCreateHandler;
-(OBMessageHandler*) createWorkspaceDeleteHandler;
-(OBMessageHandler*) createWorkspaceModifiedHandler;
-(OBMessageHandler*) createWorkspaceRenameHandler;
-(OBMessageHandler*) createWorkspaceReassignHandler;
-(OBMessageHandler*) createWorkspaceSaveHandler;
-(OBMessageHandler*) createWorkspaceListHandler;

// Sign In Handlers
-(OBMessageHandler*) createClientSignInHandler;
-(OBMessageHandler*) createClientSignOutHandler;

// Portfolio Handlers
-(OBMessageHandler*) createPortfolioItemInsertHandler;
-(OBMessageHandler*) createPortfolioItemReorderHandler;
-(OBMessageHandler*) createPortfolioItemDeleteHandler;
-(OBMessageHandler*) createPortfolioClearHandler;
-(OBMessageHandler*) createPortfolioDownloadHandler;

// Presentation Handlers
- (OBMessageHandler*) createPresentationDetailHandler;
- (OBMessageHandler*) createPresentationScrollHandler;

// Windshield handlers
-(OBMessageHandler*) createWindshieldItemCreateHandler;
-(OBMessageHandler*) createWindshieldItemGrabHandler;
-(OBMessageHandler*) createWindshieldItemTransformHandler;
-(OBMessageHandler*) createWindshieldItemDeleteHandler;
-(OBMessageHandler*) createWindshieldDetailsHandler;
-(OBMessageHandler*) createWindshieldClearHandler;

// Live Streams handlers
-(OBMessageHandler*) createLiveStreamsDetailHandler;
-(OBMessageHandler*) createLiveStreamAppearHandler;
-(OBMessageHandler*) createLiveStreamRefreshHandler;
-(OBMessageHandler*) createLiveStreamDisappearHandler;

// Infopresence
-(OBMessageHandler*) createInfopresenceSessionOutgoingRequestHandler;
-(OBMessageHandler*) createInfopresenceSessionOutgoingRequestResolveHandler;
-(OBMessageHandler*) createInfopresenceSessionIncomingRequestHandler;
-(OBMessageHandler*) createInfopresenceSessionIncomingRequestResolveHandler;
-(OBMessageHandler*) createInfopresenceRemoteMezzesStateHandler;
-(OBMessageHandler*) createInfopresenceSessionDetailHandler;
-(OBMessageHandler*) createInfopresenceFeldGeometryHandler;
-(OBMessageHandler*) createProfileDetailsHandler;
-(OBMessageHandler*) createInfopresenceOutgoingInviteHandler;
-(OBMessageHandler*) createInfopresenceOutgoingInviteResolveHandler;
-(OBMessageHandler*) createInfopresenceIncomingInviteHandler;
-(OBMessageHandler*) createInfopresenceIncomingInviteResolveHandler;
-(OBMessageHandler*) createInfopresenceParticipantJoinHandler;
-(OBMessageHandler*) createInfopresenceParticipantLeaveHandler;

// Participant list
-(OBMessageHandler*) createParticipantListHandler;

// Grabbing
-(OBMessageHandler*) createSlideGrabbedHandler;
-(OBMessageHandler*) createShielderGrabHandler;
-(OBMessageHandler*) createShielderGrabbedHandler;

// Refresh
-(OBMessageHandler*) createAssetRefreshHandler;

// Upload
-(OBMessageHandler*) createAssetUploadProvisionHandler;
-(OBMessageHandler*) createAssetUploadImageReadyHandler;

// Passkey
-(OBMessageHandler*) createPasskeyEnabledHandler;
-(OBMessageHandler*) createPasskeyDisabledHandler;
-(OBMessageHandler*) createPasskeyDetailsHandler;

// PDF
-(OBMessageHandler*) createAssetUploadPDFHandler;

// General Error
-(OBMessageHandler*) createGeneralErrorHandler;

@end
