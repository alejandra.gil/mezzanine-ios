//
//  MZParticipant.m
//  MezzKit
//
//  Created by miguel on 3/10/18.
//  Copyright © 2018 Oblong Industries. All rights reserved.
//

#import "MZParticipant.h"
#import "NSObject+KVCAdditions.h"

@implementation MZParticipant


- (NSString *)description
{
  return [NSString stringWithFormat:@"Participant: %@. Name: %@ Type: %@", _uid, _displayName, _type];
}

-(void) updateWithDictionary:(NSDictionary *)dictionary
{
  if (dictionary[@"uid"]) {
    [self setValueIfNotNullAndChanged:dictionary[@"uid"] forKey:@"uid"];
  } else if (dictionary[@"provenance"]) {
    [self setValueIfNotNullAndChanged:dictionary[@"provenance"] forKey:@"uid"];
  }
  [self setValueIfNotNullAndChanged:dictionary[@"display-name"] forKey:@"displayName"];
  [self setValueIfNotNullAndChanged:dictionary[@"type"] forKey:@"type"];
}

@end
