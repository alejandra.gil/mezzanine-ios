//
//  NSObject+KVCAdditions.h
//  Mezzanine
//
//  Created by Zai Chang on 2/17/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSObject (KVCAdditions)

-(void) setValueIfChanged:(id)value forKey:(NSString *)key;
-(void) setValueIfNotNullAndChanged:(id)value forKey:(NSString *)key;

@end
