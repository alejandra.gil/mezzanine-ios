//
//  MZURLImageOperation.h
//  MezzKit
//
//  Created by Ivan Bella Lopez on 08/06/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AsynchronousOperation.h"

@class AFURLSessionManager;

NS_ASSUME_NONNULL_BEGIN

@interface MZURLImageOperation : AsynchronousOperation

@property (nonatomic, strong, readwrite, nullable) NSMutableDictionary *actionsPerTargets;
@property (nonatomic, strong, readonly, nullable) NSURLSessionTask *task;

- (instancetype)initDownloadOperationWithManager:(AFURLSessionManager *)manager
                                         request:(NSURLRequest *)request
                                        progress:(nullable void (^)(NSProgress *downloadProgress)) downloadProgressBlock
                                     destination:(nullable NSURL * (^)(NSURL *targetPath, NSURLResponse *response))destination
                               completionHandler:(nullable void (^)(MZURLImageOperation *operation, NSURLResponse *response, NSURL * _Nullable filePath, NSError * _Nullable error))completionHandler;
@end

NS_ASSUME_NONNULL_END

