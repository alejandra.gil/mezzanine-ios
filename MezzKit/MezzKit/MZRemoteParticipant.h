//
//  MZRemoteParticipant.h
//  MezzKit
//
//  Created by miguel on 5/6/15.
//  Copyright (c) 2015 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MZParticipant.h"

@interface MZRemoteParticipant : MZParticipant

@end
