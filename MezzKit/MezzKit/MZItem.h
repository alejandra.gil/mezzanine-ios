//
//  MZItem.h
//  MezzKit
//
//  Created by miguel on 29/05/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MZAnnotation.h"
#import "KVOHelpers.h"


/// Base class for portfolio items ( 2.12)
/// For items that live inside a deck or presentation.
///
@interface MZItem : NSObject
{
  NSUInteger index;
  NSString *uid;
  NSString *thumbURL;
  NSString *largeURL;
  NSString *fullURL;
  NSString *contentSource;

  // In the case of a video slide
  NSString *viddleName;

  // Locally cached image
  MZImage *image;
}
@property (nonatomic, assign) NSUInteger index;
@property (nonatomic, copy) NSString *uid;
@property (nonatomic, copy) NSString *thumbURL;
@property (nonatomic, copy) NSString *largeURL;
@property (nonatomic, copy) NSString *fullURL;
@property (nonatomic, strong, readonly) NSString *convertedOriginalURL;
@property (nonatomic, copy) NSString *contentSource;
@property (nonatomic, assign) BOOL imageAvailable;
@property (nonatomic, copy) NSString *viddleName;
@property (nonatomic, strong) MZImage *image;
@property (nonatomic, strong) NSDictionary *displayInfo;

// Convenience
@property (nonatomic, assign, readonly) BOOL isVideo;


// Annotations
@property (nonatomic, strong) NSMutableArray *annotations;
KVO_MUTABLEARRAY_ACCESSOR_INTERFACE(Annotations, annotations)

-(void) addAnnotation:(MZAnnotation*)annotation;
-(void) addAnnotationsArray:(NSArray *)objects;
-(void) removeAnnotation:(MZAnnotation*)annotation;
-(void) removeAnnotationsArray:(NSArray *)objects;
-(void) clearAnnotations;


@property (nonatomic, strong) NSUndoManager *undoManager;


/// Volatile properties
// pendingReorder is a flag that is only set when an item is in the middle of
// being reordered. Currently this is necessary because there's no KVO mechanism
// for reordering within an NSMutableArray, and the workaround involve deleting
// then re-adding the object which is not ideal for smooth UI transition
@property (nonatomic, assign) BOOL pendingReorder;


-(void) updateWithDictionary:(NSDictionary*)dictionary;

@end
