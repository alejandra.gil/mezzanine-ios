//
//  MZWindshield.h
//  Mezzanine
//
//  Created by Zai Chang on 5/8/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MZWindshieldItem.h"
#import "KVOHelpers.h"


@interface MZWindshield : NSObject
{
  CGFloat opacity;
  NSMutableArray *items;
}

@property (nonatomic, assign) CGFloat opacity;  // Hopefully soon native will report on opacity status

@property (nonatomic, strong) NSMutableArray *items;  // These should be of the class MZWindshieldItem

KVO_MUTABLEARRAY_ACCESSOR_INTERFACE(Items, items)

-(MZWindshieldItem*) itemWithUid:(NSString*)aUid;
-(NSInteger) indexOfItemWithUid:(NSString*)aUid;

-(NSArray*) itemsWithContentSource:(NSString*)contentSource;
-(void) clear;

- (void)updateWithWindshieldItems:(NSArray *)windshieldItemsArray;

- (NSArray *)itemsInPrimaryWindshield;
- (NSArray *)itemsInExtendedWindshield;
- (NSArray *)itemsInSurface:(MZSurface *)surface;

@end
