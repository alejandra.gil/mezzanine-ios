//
//  MZFeatureToggles.h
//  MezzKit
//
//  Created by miguel on 2/2/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MZFeatureToggles : NSObject

@property (nonatomic, assign) BOOL infopresenceInvitesEnabled;
@property (nonatomic, assign) BOOL remoteAccessEnabled;
@property (nonatomic, assign) BOOL disableDownloadsEnabled;
@property (nonatomic, assign) BOOL largeImagesEnabled;
@property (nonatomic, assign) BOOL immenseImagesEnabled;
@property (nonatomic, assign) BOOL accessWarning;
@property (nonatomic, assign) BOOL roomKey;
@property (nonatomic, assign) BOOL participantRoster;

@end
