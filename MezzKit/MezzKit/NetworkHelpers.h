//
//  NetworkHelpers.h
//  MezzKit
//
//  Created by Miguel Sánchez Valdés on 22/9/18.
//  Copyright © 2018 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NetworkHelpers : NSObject

+ (NSString*)lookupHostIPAddressForURL:(NSURL*)url;

@end

NS_ASSUME_NONNULL_END
