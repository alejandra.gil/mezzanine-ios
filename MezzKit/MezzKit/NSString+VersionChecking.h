//
//  NSString+VersionChecking.h
//  MezzKit
//
//  Created by Ivan Bella Lopez on 06/11/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (VersionChecking)

- (BOOL)isVersionOrGreater:(NSString*)versionString;

@end
