//
//  MZPortfolioItem.m
//  MezzKit
//
//  Created by Miguel Sanchez on 28/05/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import "MZPortfolioItem.h"


@implementation MZPortfolioItem

-(NSString*) description
{
  return [NSString stringWithFormat:@"MZPortfolioItem %p: uid=%@, index=%lu", self, uid, (unsigned long)index];
}


#pragma mark - Content


-(NSString*) convertedOriginalURL
{
  if (fullURL)
  {
    if ([fullURL hasSuffix:@"img-local-slide.png"])
      return [fullURL stringByReplacingOccurrencesOfString:@"img-local-slide.png" withString:@"img-conf.png"];
    return fullURL;
  }
  return nil;
}


-(void) updateWithDictionary:(NSDictionary*)dictionary
{
  if (!dictionary)
    return;

  NSString *aUid = dictionary[@"uid"];
  if (self.uid != aUid && ![self.uid isEqual:aUid])
    self.uid = aUid;

  NSString *thumbUrl = dictionary[@"thumb-uri"];
  if (self.thumbURL != thumbUrl && ![self.thumbURL isEqual:thumbUrl])
  {
    self.thumbURL = thumbUrl;

    // This is a hack, because native side somehow changed full-uri to point instead at the origin image
    self.fullURL = [[thumbUrl stringByDeletingLastPathComponent] stringByAppendingPathComponent:@"img-local-slide.png"];
  }

  // Instead of using the original image (which full-uri) points to
  // we're using img-slide above because the full image can be as big as 6k
  // which is much too big for iOS to handle well without tiling
  //  NSString *fullUrl = [dictionary objectForKey:@"full-uri"];
  //  if (self.fullURL != fullUrl && ![self.fullURL isEqual:fullUrl])
  //    self.fullURL = fullUrl;


  BOOL anImageAvailable = [dictionary[@"image-available"] boolValue];
  if (self.imageAvailable != anImageAvailable)
    self.imageAvailable = anImageAvailable;


  NSString *source = dictionary[@"content-source"];
  if (source && ![self.contentSource isEqual:source])
    self.contentSource = source;

  // Contains information on video sources
  NSDictionary *someDisplayInfo = dictionary[@"display-info"];
  if (someDisplayInfo)
    self.displayInfo = someDisplayInfo;
}

@end