//
//  MZSessionManager.m
//  MezzKit
//
//  Created by Ivan Bella Lopez on 04/02/2019.
//  Copyright © 2019 Oblong Industries. All rights reserved.
//

#import "MZSessionManager.h"
#import "MZSecurityManager.h"
#import "NSURL+Additions.h"
#import "SystemStateResponse.h"

#define JOIN_HEARTBEAT_TIMEOUT  60.0
#define JOIN_HEARTBEAT_INTERVAL 10.0

typedef void (^MZSessionManagerJoiningHeartbeatCompletionBlock) (NSError *error);
typedef void (^MZSessionManagerAuthorizeCompletionBlock) (NSError * _Nullable error, NSString * _Nullable token);
typedef void (^PasskeyZiggyCompleteBlock)(NSString *passkey);

@interface MZSessionManager ()
{
  BOOL authenticationSucceeded;
  NSURL *serverURL;
  NSTimer *joinHeartbeatTimer;
  NSInteger __block joinHeartbeatTimeout;
}

@property (nonatomic, strong) NSURLSession* urlSession;

- (NSString *)extractSessionTokenFromResponseHeaders:(NSURLResponse *)response;
- (void)requestJoiningHeartbeatWithCompletion:(MZSessionManagerJoiningHeartbeatCompletionBlock)completionBlock;
- (void)requestAuthorizeWithPasskey:(NSString *)passkey completion:(MZSessionManagerAuthorizeCompletionBlock)completionBlock;
- (void)checkSSLHandshakeResultWithSystemStateJSON:(SystemStateResponse *)systemStateJSON completion:(MZSessionManagerHandshakeCompletionBlock)completionBlock;
- (void)continueWebTokenSessionAuthentication:(SystemStateResponse *)systemStateJSON completion:(MZSessionManagerHandshakeCompletionBlock)completionBlock;
- (void)showPasskeyViewForJWT:(SystemStateResponse *)systemStateJSON completion:(MZSessionManagerHandshakeCompletionBlock)completionBlock;
- (void)handleJoiningHeartbeatRequestResponse:(NSURLResponse * _Nullable)response data:(NSData * _Nullable)data error:(NSError * _Nullable)error completion:(MZSessionManagerJoiningHeartbeatCompletionBlock)completionBlock;
- (void)handleAuthorizeRequestResponse:(NSURLResponse * _Nullable)response data:(NSData * _Nullable)data error:(NSError * _Nullable)error completion:(MZSessionManagerAuthorizeCompletionBlock)completionBlock;
- (void)handleCleanUpRequestResponse:(NSURLResponse * _Nullable)response data:(NSData * _Nullable)data error:(NSError * _Nullable)error completion:(MZSessionManagerCleanupCompletionBlock)completionBlock;
- (void)handleEndSessionRequestResponse:(NSURLResponse * _Nullable)response data:(NSData * _Nullable)data error:(NSError * _Nullable)error;

@end

@implementation MZSessionManager

- (instancetype)init
{
  if (self = [super init])
  {
    joinHeartbeatTimeout = JOIN_HEARTBEAT_TIMEOUT;
    _sessionType = MZSessionTypeUnknown;
    _sessionStartTime = nil;

    NSURLSessionConfiguration* ephemeralConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    ephemeralConfiguration.timeoutIntervalForRequest = 10.0;
    self.urlSession = [NSURLSession sessionWithConfiguration:ephemeralConfiguration
                                                                delegate:self
                                                           delegateQueue:[NSOperationQueue mainQueue]];

    [self addPasskeyObservers];
  }

  return self;
}

-(void) dealloc
{
  DLog(@"%@ dealloc", [self class]);
}

#pragma mark - Observation

- (void)addPasskeyObservers
{
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(renewJoiningHeartbeatTimeout) name:MZSessionManagerUpdatedPasskeyViewNotification object:nil];
}

- (void)removePasskeyObservers
{
  [[NSNotificationCenter defaultCenter] removeObserver:self name:MZSessionManagerUpdatedPasskeyViewNotification object:nil];
}

#pragma mark - Requests

- (void)requestStartSessionWithServer:(NSURL *)aServerURL completion:(MZSessionManagerHandshakeCompletionBlock)completionBlock
{
  serverURL = aServerURL;
  NSURL *url = [self urlWithEndPoint:@"system-state"];

  __weak typeof(self) __weakSelf = self;
  MZSessionManagerHandshakeCompletionBlock __block blockCompletionBlock = completionBlock;
  NSURLSessionDataTask *task = [self.urlSession dataTaskWithURL:url
                                              completionHandler:^(NSData * _Nullable data,
                                                                  NSURLResponse * _Nullable response,
                                                                  NSError * _Nullable error) {
                                                typeof(self) strongSelf = __weakSelf;
                                                [strongSelf handleSystemStateRequestResponse:response
                                                                                        data:data
                                                                                       error:error
                                                                                  completion:blockCompletionBlock];
                                              }];
  [task resume];
}

- (void)requestJoiningHeartbeatWithCompletion:(MZSessionManagerJoiningHeartbeatCompletionBlock)completionBlock
{
  NSURL *url = [self urlWithEndPoint:@"joining-heartbeat"];
  NSMutableURLRequest *urlRequest = [NSMutableURLRequest new];
  [urlRequest setURL:url];
  [urlRequest setHTTPMethod:@"POST"];

  NSData *postData = [NSJSONSerialization dataWithJSONObject:@{} options:0 error:nil];
  [urlRequest setHTTPBody:postData];

  __weak typeof(self) __weakSelf = self;
  MZSessionManagerJoiningHeartbeatCompletionBlock __block blockCompletionBlock = completionBlock;
  NSURLSessionDataTask *task = [self.urlSession dataTaskWithRequest:urlRequest
                                                  completionHandler:^(NSData * _Nullable data,
                                                                      NSURLResponse * _Nullable response,
                                                                      NSError * _Nullable error) {
                                                    typeof(self) strongSelf = __weakSelf;
                                                    [strongSelf handleJoiningHeartbeatRequestResponse:response
                                                                                                 data:data
                                                                                                error:error
                                                                                           completion:blockCompletionBlock];
                                                  }];
  [task resume];
}

- (void)requestAuthorizeWithPasskey:(NSString *)passkey completion:(MZSessionManagerAuthorizeCompletionBlock)completionBlock
{
  NSURL *url = [self urlWithEndPoint:@"authorize"];
  NSMutableURLRequest *urlRequest = [NSMutableURLRequest new];
  [urlRequest setURL:url];
  [urlRequest setHTTPMethod:@"POST"];

  NSError *error = nil;
  NSDictionary *data = [[NSDictionary alloc] initWithObjectsAndKeys: self.provenance, @"provenance",
                               self.displayName, @"display-name", passkey, @"passkey", nil];
  NSData *postData = [NSJSONSerialization dataWithJSONObject:data options:0 error:&error];
  [urlRequest setHTTPBody:postData];

  __weak typeof(self) __weakSelf = self;
  MZSessionManagerAuthorizeCompletionBlock __block blockCompletionBlock = completionBlock;
  NSURLSessionDataTask *task = [self.urlSession dataTaskWithRequest:urlRequest
                                                  completionHandler:^(NSData * _Nullable data,
                                                                      NSURLResponse * _Nullable response,
                                                                      NSError * _Nullable error) {
                                                    typeof(self) strongSelf = __weakSelf;
                                                    [strongSelf handleAuthorizeRequestResponse:response
                                                                                          data:data
                                                                                         error:error
                                                                                    completion:blockCompletionBlock];
                                                  }];
  [task resume];
}

- (void)requestCleanUpSessionWithCompletion:(MZSessionManagerCleanupCompletionBlock)completionBlock;
{
  NSURL *url = [self urlWithEndPoint:@"cleanup-session"];
  NSMutableURLRequest *urlRequest = [NSMutableURLRequest new];
  [urlRequest setURL:url];
  [urlRequest setHTTPMethod:@"POST"];

  NSData *postData = [NSJSONSerialization dataWithJSONObject:@{} options:0 error:nil];
  [urlRequest setHTTPBody:postData];

  __weak typeof(self) __weakSelf = self;
  MZSessionManagerCleanupCompletionBlock __block blockCompletionBlock = completionBlock;
  NSURLSessionDataTask *task = [self.urlSession dataTaskWithRequest:urlRequest
                                                  completionHandler:^(NSData * _Nullable data,
                                                                      NSURLResponse * _Nullable response,
                                                                      NSError * _Nullable error) {
                                                    typeof(self) strongSelf = __weakSelf;
                                                    [strongSelf handleCleanUpRequestResponse:response
                                                                                        data:data
                                                                                       error:error
                                                                                  completion:blockCompletionBlock];
                                                  }];
  [task resume];
}

- (void)requestEndSession
{
  NSURL *url = [self urlWithEndPoint:@"end-session"];
  NSMutableURLRequest *urlRequest = [NSMutableURLRequest new];
  [urlRequest setURL:url];
  [urlRequest setHTTPMethod:@"POST"];

  NSData *postData = [NSJSONSerialization dataWithJSONObject:@{} options:0 error:nil];
  [urlRequest setHTTPBody:postData];

  __weak typeof(self) __weakSelf = self;
  NSURLSessionDataTask *task = [self.urlSession dataTaskWithRequest:urlRequest
                                                  completionHandler:^(NSData * _Nullable data,
                                                                      NSURLResponse * _Nullable response,
                                                                      NSError * _Nullable error) {
                                                    typeof(self) strongSelf = __weakSelf;
                                                    [strongSelf handleEndSessionRequestResponse:response
                                                                                           data:data
                                                                                          error:error];
                                                  }];
  [task resume];
}


#pragma mark - Request Helpers

- (NSURL *)urlWithEndPoint:(NSString *)endPoint
{
  return [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/session/v1/%@", [serverURL host], endPoint]];
}


- (NSString *)extractSessionTokenFromResponseHeaders:(NSURLResponse *)response
{
  NSDictionary *headerFields = [(NSHTTPURLResponse *)response allHeaderFields];
  NSArray *cookies = [NSHTTPCookie cookiesWithResponseHeaderFields:headerFields forURL:response.URL];
  NSString *token = nil;
  for (NSHTTPCookie *cookie in cookies)
  {
    if ([[cookie name] isEqualToString:@"Session-Token"])
    {
      token = [cookie value];
      break;
    }
  }

  return token;
}


- (void)reset
{
  [self.urlSession invalidateAndCancel];
  _sessionStartTime = nil;
}


#pragma mark - System State Response Handler Methods

- (void)handleSystemStateRequestResponse:(NSURLResponse * _Nullable)response data:(NSData * _Nullable)data error:(NSError * _Nullable)error completion:(MZSessionManagerHandshakeCompletionBlock)completionBlock
{
  // Check response type
  BOOL connectingToAZiggySystem = [(NSHTTPURLResponse *) response statusCode] == 200;

  // Serialize data from response
  SystemStateResponse *systemStateResponse = [[SystemStateResponse alloc] initWithData:data];
  if (!systemStateResponse && connectingToAZiggySystem)
  {
    // Error serializing system state response
    NSError *error = [NSError errorWithDomain:MZSessionManagerErrorDomain code:kMZSessionManagerErrorCode_SystemStateSerializationError userInfo:nil];
    completionBlock(error, nil);
    return;
  }

  // Complete handling of response managing different errors, messages and systems.
  if (error && authenticationSucceeded)
  {
    // ATS has become more restrictive after iOS 11 (still ocurring in iOS 12)
    // Our servers certficates chain are validated without problems but fail
    // to pass the final challenge if user try to connect using the hostname or
    // FQDN. It works fine using the IP.
    // TODO: this is a temporary hack until we figure out what changes are required
    // either in this client, the server or the certificate itself
    NSError * certHostError = [NSError errorWithDomain:MZSessionManagerErrorDomain
                                                  code:kMZSessionManagerErrorCode_ValidCertificateWithIncorrectHost
                                              userInfo:nil];
    completionBlock(certHostError, nil);
  }
  else if (error)
  {
    completionBlock(error, nil);
  }
  else
  {
    [self checkSSLHandshakeResultWithSystemStateJSON:systemStateResponse completion:completionBlock];
  }
}


#pragma mark - Session Passkey

- (void)checkSSLHandshakeResultWithSystemStateJSON:(SystemStateResponse *)systemStateJSON completion:(MZSessionManagerHandshakeCompletionBlock)completionBlock
{
  if (systemStateJSON)
  {
    // Connection via Ziggy, let's authenticate with a passkey
    _sessionType = MZSessionTypeZiggy;
    [self continueWebTokenSessionAuthentication:systemStateJSON completion:completionBlock];
  }
  else
  {
    // Connection via plasma-web-proxy, back to communicator
    _sessionType = MZSessionTypePlasmaWebProxy;
    completionBlock(nil, nil);
  }
}

- (void)continueWebTokenSessionAuthentication:(SystemStateResponse *)systemStateJSON completion:(MZSessionManagerHandshakeCompletionBlock)completionBlock
{
  __weak typeof(self) __weakSelf = self;

  // 1. Error found on Ziggy system-state response
  if (systemStateJSON.errorDescription)
  {
    NSError *error = [NSError errorWithDomain:MZSessionManagerErrorDomain
                                         code:systemStateJSON.errorCode
                                     userInfo:@{NSLocalizedDescriptionKey: systemStateJSON.errorDescription}];
    completionBlock(error, nil);
    return;
  }

  // 2. Success case: Ziggy system with an always-on passkey
  if (systemStateJSON.passkeyRequired)
  {
    BOOL __block passkeyShown = NO;
    joinHeartbeatTimer = [NSTimer scheduledTimerWithTimeInterval:JOIN_HEARTBEAT_INTERVAL repeats:YES block:^(NSTimer * _Nonnull timer) {
      typeof(self) strongSelf = __weakSelf;
      if (strongSelf->joinHeartbeatTimeout == 0)
      {
        [timer invalidate];
        [[NSNotificationCenter defaultCenter] postNotificationName:MZSessionManagerHidePasskeyViewNotification object:strongSelf userInfo:nil];
      }
      else
      {
        strongSelf->joinHeartbeatTimeout -= JOIN_HEARTBEAT_INTERVAL;
      }

      __weak typeof(self) __innerWeakSelf = strongSelf;
      [strongSelf requestJoiningHeartbeatWithCompletion:^(NSError * error) {
        typeof(self) innerStrongSelf = __innerWeakSelf;
        // 1. Error from joining-heartbeat request
        if (error)
        {
          completionBlock(error, nil);
          [timer invalidate];
        }
        else if (!passkeyShown)
        {
          [innerStrongSelf showPasskeyViewForJWT:systemStateJSON completion:completionBlock];
          passkeyShown = YES;
        }
      }];
    }];

    [joinHeartbeatTimer fire];
  }
  else
  {
    // 3. Success case: Ziggy systems not requiring passkeys
    [self requestAuthorizeWithPasskey:nil completion:^(NSError * _Nullable error, NSString * _Nullable token) {
      typeof(self) strongSelf = __weakSelf;
      if (error)
      {
        completionBlock(error, nil);
      }
      else if (!token)
      {
        // Give it a second chance of using a token if a previous one was stored
        // Authorize request is not sending the token on reconnections
        // for some unknown issue with session caching.
        NSString *oldToken = [strongSelf getCurrentSessionToken];
        if (oldToken)
        {
          completionBlock(nil, token);
        }
        else
        {
          NSError *error = [NSError errorWithDomain:MZSessionManagerErrorDomain
                                               code:kMZSessionManagerErrorCode_ErrorRetrievingSessionToken
                                           userInfo:@{NSLocalizedDescriptionKey: @"Error retreiving session token"}];
          completionBlock(error, nil);
        }
      }
      else if (token)
      {
        completionBlock(nil, token);
      }
    }];
  }
}

- (void)showPasskeyViewForJWT:(SystemStateResponse *)systemStateJSON completion:(MZSessionManagerHandshakeCompletionBlock)completionBlock
{
  __weak typeof(self) __weakSelf = self;
  PasskeyZiggyCompleteBlock __block passkeyOutput = ^(NSString *passkey)
  {
    typeof(self) strongSelf = __weakSelf;
    [strongSelf requestAuthorizeWithPasskey:passkey completion:^(NSError * _Nullable error, NSString * _Nullable token) {
      if (error)
      {
        // Wrong passkey, returns unauthorized (401)
        NSError *wrongPasskeyError = [NSError errorWithDomain:MZSessionManagerErrorDomain
                                                         code:kMZSessionManagerErrorCode_WrongPassword
                                                     userInfo:@{NSLocalizedDescriptionKey: @"Error retrieving session token"}];
        completionBlock(wrongPasskeyError, nil);
      }
      else if (!token)
      {
        // Give it a second chance of using a token if a previous one was stored
        // Authorize request is not sending the token on reconnections
        // for some unknown issue with session caching.
        NSString *oldToken = [strongSelf getCurrentSessionToken];
        if (oldToken)
        {
          [strongSelf stopJoiningHearbeat];
          completionBlock(nil, token);
        }
        else
        {
          NSError *error = [NSError errorWithDomain:MZSessionManagerErrorDomain
                                               code:kMZSessionManagerErrorCode_ErrorRetrievingSessionToken
                                           userInfo:@{NSLocalizedDescriptionKey: @"Error retrieving session token"}];

          completionBlock(error, nil);
        }
      }
      else if (token)
      {
        [strongSelf stopJoiningHearbeat];
        completionBlock(nil, token);
      }
    }];
  };


  // Try to used token. Otherwise, show passkey view
  // If valid will return a 200 but NO token. That's why we have it stored.
  // Passkey is not used to authorize but is used for backwards compatibility in MZCommunicator (joinPassphrase)
  [self requestAuthorizeWithPasskey:nil completion:^(NSError * _Nullable error, NSString * _Nullable token) {
    typeof(self) strongSelf = __weakSelf;
    if (!error)
    {
      NSString *token = [strongSelf getCurrentSessionToken];
      if (token)
      {
        [strongSelf stopJoiningHearbeat];
        completionBlock(nil, token);
      }
      else
      {
        NSDictionary *userInfo = @{@"completed":passkeyOutput, @"numberOfFields":[NSNumber numberWithInteger:systemStateJSON.passkeyLength]};
        [[NSNotificationCenter defaultCenter] postNotificationName:MZSessionManagerShowPasskeyViewNotification object:strongSelf userInfo:userInfo];
      }
    }
    else
    {
      NSDictionary *userInfo = @{@"completed":passkeyOutput, @"numberOfFields":[NSNumber numberWithInteger:systemStateJSON.passkeyLength]};
      [[NSNotificationCenter defaultCenter] postNotificationName:MZSessionManagerShowPasskeyViewNotification object:strongSelf userInfo:userInfo];
    }
  }];
}


- (void)stopJoiningHearbeat
{
  [joinHeartbeatTimer invalidate];
  joinHeartbeatTimer = nil;
}


- (void)renewJoiningHeartbeatTimeout
{
  joinHeartbeatTimeout = JOIN_HEARTBEAT_TIMEOUT;
}

#pragma mark - Joining Heartbeat Response Handler

- (void)handleJoiningHeartbeatRequestResponse:(NSURLResponse * _Nullable)response data:(NSData * _Nullable)data error:(NSError * _Nullable)error completion:(MZSessionManagerJoiningHeartbeatCompletionBlock)completionBlock
{
  NSInteger statusCode = [(NSHTTPURLResponse *) response statusCode];
  if (statusCode == 200 && !error)
  {
    completionBlock(nil);
  }
  else
  {
    DLog(@"Error: Joining heartbeat failed with error:%ld", (long)statusCode);
    NSError *sessionManagerError = [NSError errorWithDomain:MZSessionManagerErrorDomain code:kMZSessionManagerErrorCode_ErrorOnJoiningHeartbeatRequest userInfo:nil];
    completionBlock(sessionManagerError);
  }
}


#pragma mark - Authorize Response Handler

- (void)handleAuthorizeRequestResponse:(NSURLResponse * _Nullable)response data:(NSData * _Nullable)data error:(NSError * _Nullable)error completion:(MZSessionManagerAuthorizeCompletionBlock)completionBlock
{
  NSInteger statusCode = [(NSHTTPURLResponse *) response statusCode];
  if (statusCode == 200 && !error)
  {
    _sessionStartTime = [NSDate date];
    NSString *sessionToken = [self extractSessionTokenFromResponseHeaders:response];
    // TODO: This check shouldn't happen because the sessionToken shouldn't be nil.
    // Keeping it here because authorize request is not sending the token on
    // reconnections for some unknown issue with session caching.
    if (sessionToken)
      [self setCurrentSessionToken:sessionToken];
    completionBlock(nil, sessionToken);
  }
  else
  {
    DLog(@"Error: Authorize failed with error:%ld", (long)statusCode);
    NSError *error = [NSError errorWithDomain:MZSessionManagerErrorDomain code:kMZSessionManagerErrorCode_ErrorOnAuthorizeRequest userInfo:nil];
    completionBlock(error, nil);
  }
}

#pragma mark - Clean Up Response Handler

- (void)handleCleanUpRequestResponse:(NSURLResponse * _Nullable)response data:(NSData * _Nullable)data error:(NSError * _Nullable)error completion:(MZSessionManagerCleanupCompletionBlock)completionBlock
{
  NSInteger statusCode = [(NSHTTPURLResponse *) response statusCode];
  if (statusCode == 200 && !error)
  {
    NSString *sessionToken = [self extractSessionTokenFromResponseHeaders:response];
    [self setCurrentSessionToken:sessionToken];
    completionBlock(nil, sessionToken);
  }
  else
  {
    DLog(@"Error: Session clean up failed with error:%ld", (long)statusCode);
    NSError *error = [NSError errorWithDomain:MZSessionManagerErrorDomain code:kMZSessionManagerErrorCode_ErrorCleaningUpSession userInfo:nil];
    completionBlock(error, nil);
  }
}

#pragma mark - End Session Response Handler

- (void)handleEndSessionRequestResponse:(NSURLResponse * _Nullable)response data:(NSData * _Nullable)data error:(NSError * _Nullable)error
{
  NSInteger statusCode = [(NSHTTPURLResponse *) response statusCode];
  if (statusCode == 200 && !error)
  {
    DLog(@"Session Ended OK");
    [self setCurrentSessionToken:nil];
  }
  else
  {
    DLog(@"Error ending the session");
  }
}


#pragma mark - NSURLSession Delegate

- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential *))completionHandler
{
  if (!self.urlSession || session != self.urlSession) {
    completionHandler(NSURLSessionAuthChallengeCancelAuthenticationChallenge, nil);
    return;
  }

  if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
  {
    MZSecurityManager *securityManager = [MZSecurityManager new];
    NSURLCredential *successCredential = [securityManager credentialsFromAuthenticationChallenge:challenge];
    authenticationSucceeded = [securityManager authenticateAgainstProtectionSpace:challenge.protectionSpace];
    if (successCredential)
    {
      completionHandler(NSURLSessionAuthChallengeUseCredential, successCredential);
    }
    else
    {
      completionHandler(NSURLSessionAuthChallengeCancelAuthenticationChallenge, nil);
    }
  }
  else
  {
    completionHandler(NSURLSessionAuthChallengeCancelAuthenticationChallenge, nil);
  }
}

- (void)URLSession:(NSURLSession *)session didBecomeInvalidWithError:(nullable NSError *)error
{
  self.urlSession = nil;

  serverURL = nil;
  _sessionType = MZSessionTypeUnknown;

  // Joining heartbeat
  [self stopJoiningHearbeat];
  [self removePasskeyObservers];
}


#pragma mark - NSUserDefaults

- (void)setCurrentSessionToken:(NSString *)sessionToken
{
  [[NSUserDefaults standardUserDefaults] setObject:sessionToken forKey:@"session-token"];
}

- (NSString *)getCurrentSessionToken
{
  return [[NSUserDefaults standardUserDefaults] stringForKey:@"session-token"];
}

@end
