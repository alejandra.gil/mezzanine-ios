//
//  MZUploadLimits.m
//  MezzKit
//
//  Created by Zai Chang on 3/5/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import "MZUploadLimits.h"

@implementation MZUploadLimits

-(void) updateWithDictionary:(NSDictionary*)dictionary
{
  NSNumber *maxImageWidth = [dictionary objectForKey:@"upload-max-width"];
  NSNumber *maxImageHeight = [dictionary objectForKey:@"upload-max-height"];
  NSNumber *maxImageSizeInMB = [dictionary objectForKey:@"upload-max-image-size-mb"];
  NSNumber *maxPDFSizeInMB = [dictionary objectForKey:@"upload-max-pdf-size-mb"];
  NSNumber *maxGlobalSizeInMB = [dictionary objectForKey:@"upload-max-size-mb"];
  
  if (maxImageWidth)
    self.maxImageWidth = maxImageWidth;
  if (maxImageHeight)
    self.maxImageHeight = maxImageHeight;
  if (maxImageSizeInMB)
    self.maxImageSizeInMB = maxImageSizeInMB;
  if (maxPDFSizeInMB)
    self.maxPDFSizeInMB = maxPDFSizeInMB;
  if (maxGlobalSizeInMB)
    self.maxGlobalSizeInMB = maxGlobalSizeInMB;
  
  // TMP until bug 18035 is implemented in native and the value comes from the server
  self.maxImageSizeInMP = [NSNumber numberWithInteger:50];
}

@end
