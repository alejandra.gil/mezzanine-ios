//
//  NSURL+Additions.m
//  MezzKit
//
//  Created by Ivan Bella Lopez on 18/05/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

#import "NSURL+Additions.h"

#define kMEZZINURLPATH @"/m/"
#define kMEZZINPERSISTENTURLPATH @"/a/"

@implementation NSURL (Additions)

+ (nullable instancetype)defaultSchemeWithHostname:( NSString * _Nonnull)hostname;
{
  return  [self wssSchemeWithHostname:[hostname lowercaseString]];
}


+ (nullable instancetype)httpsSchemeWithHostname:(NSString * _Nonnull)hostname;
{
  NSString *urlString = [NSString stringWithFormat:@"https://%@", [hostname lowercaseString]];
  return [NSURL URLWithString:urlString];
}


+ (nullable instancetype)wssSchemeWithHostname:(NSString * _Nonnull)hostname;
{
  NSString *urlString = [NSString stringWithFormat:@"wss://%@", [hostname lowercaseString]];
  return [NSURL URLWithString:urlString];
}


+ (nullable instancetype)tcpsSchemeWithHostname:(NSString * _Nonnull)hostname;
{
  NSString *urlString = [NSString stringWithFormat:@"tcps://%@", [hostname lowercaseString]];
  return [NSURL URLWithString:urlString];
}


#pragma mark - URL manipulation

- (nullable instancetype)URLByReplacingSchemeWith:(NSString * _Nonnull)newScheme;
{
  NSURLComponents *components = [NSURLComponents componentsWithURL:self resolvingAgainstBaseURL:YES];
  components.scheme = [newScheme lowercaseString];
  return components.URL;
}


- (nullable instancetype)URLByReplacingSchemeWithWSS
{
  NSURLComponents *components = [NSURLComponents componentsWithURL:self resolvingAgainstBaseURL:YES];
  return [components.URL URLByReplacingSchemeWith:@"wss"];
}


- (nullable instancetype)URLByReplacingSchemeWithTCPS
{
  NSURLComponents *components = [NSURLComponents componentsWithURL:self resolvingAgainstBaseURL:YES];
  components.path = nil;
  
  return [components.URL URLByReplacingSchemeWith:@"tcps"];
}


- (nullable instancetype)URLByReplacingSchemeWithHTTPS
{
  return [self URLByReplacingSchemeWith:@"https"];
}


- (nullable instancetype)URLByDeletingFragment
{
  if (self.fragment) {
    NSURLComponents *components = [NSURLComponents componentsWithURL:self resolvingAgainstBaseURL:YES];
    components.fragment = nil;
    components.path = nil;
    
    return components.URL;
  }
  
  return self;
}


- (nullable instancetype)URLByDeletingPath
{
  NSURLComponents *components = [NSURLComponents componentsWithURL:self resolvingAgainstBaseURL:YES];
  components.path = nil;
  return components.URL;
}


- (nullable instancetype)lowCaseURL
{
  NSURLComponents *components = [NSURLComponents componentsWithURL:self resolvingAgainstBaseURL:YES];
  components.scheme = [self.scheme lowercaseString];
  components.user = [self.user lowercaseString];
  components.password = [self.password lowercaseString];
  components.host = [self.host lowercaseString];
  components.path = [self.path lowercaseString];
  components.query = [self.query lowercaseString];
  components.fragment = [self.fragment lowercaseString];
  
  return components.URL;
}


#pragma mark - Remote Participation Helpers

- (BOOL)isARemoteParticipationURL
{
  return [self.absoluteString containsString:kMEZZINURLPATH] || [self.absoluteString containsString:kMEZZINPERSISTENTURLPATH];
}


#pragma mark - Mezzanine scheme helper methods

- (NSURL *)sanitizeURLForMezzanineProtocol
{
  NSURLComponents *components = [NSURLComponents componentsWithURL:[self lowCaseURL] resolvingAgainstBaseURL:YES];

  if (![[components scheme] isEqualToString:@"mezzanine"])
  {
    return self;
  }
  else
  {
    if ([[components host] isEqualToString:@"http"]
        || [[components host] isEqualToString:@"https"]
        || [[components host] isEqualToString:@"wss"]
        || [[components host] isEqualToString:@"tcp"]
        || [[components host] isEqualToString:@"tcps"])
    {
      return [NSURL URLWithString:[NSString stringWithFormat:@"%@:%@", [components scheme], [components path]]];
    }
    else
    {
      return self;
    }
  }
  return self;
}
@end
