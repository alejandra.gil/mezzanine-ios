//
//  MZWorkspace.h
//  Mezzanine
//
//  Created by Zai Chang on 10/20/10.
//  Copyright 2014 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MZPresentation.h"
#import "MZSlide.h"
#import "MZAsset.h"
#import "MZWindshield.h"
#import "MZLiveStream.h"
#import "KVOHelpers.h"


// Notifications
#define MZWorkspaceSlidesClearedNotification @"MZWorkspaceSlidesCleared"


/// Model object for a workspace.
/// Used when connected to Mezzanine v2.12+
///
@interface MZWorkspace : NSObject

@property (nonatomic, copy) NSString *uid;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *thumbURL;
@property (nonatomic, strong) NSDate *modifiedDate;
@property (nonatomic, strong) NSString *modifiedDateString;
@property (nonatomic, strong) NSString *owner;

@property (nonatomic, assign) BOOL isSaved;
@property (nonatomic, assign) BOOL wasDestroyed;

@property (nonatomic, readonly, strong) NSString *dynamicThumbURL;

@property (nonatomic, strong) MZPresentation *presentation;
@property (nonatomic, strong) MZWindshield *windshield;
@property (nonatomic, strong) NSMutableArray *liveStreams;

// Mixed Geometry
@property (nonatomic, assign) BOOL isSingleFeld;

// Volatile states
@property (nonatomic, assign) BOOL isLoading;
@property (nonatomic, assign, readonly) BOOL hasContent;
@property (nonatomic, assign) BOOL isPendingDeletion;
@property (nonatomic, assign) BOOL isBeingEdited;


#pragma mark - LiveStreams

KVO_MUTABLEARRAY_ACCESSOR_INTERFACE(LiveStreams, liveStreams)


-(MZLiveStream*) liveStreamWithUid:(NSString*)uid;
-(void) removeAllLiveStreams;


- (void)updateWithDictionary:(NSDictionary*)dictionary;
- (void)updateLiveStreamsWithDictionary:(NSDictionary*)dictionary;
- (void)updateLiveStreamsWithDictionary:(NSDictionary*)dictionary andRemoteMezzes:(NSArray *)remoteMezzes;
- (void)updateLiveStreamRemoteMezzName:(MZLiveStream*)liveStream usingRemoteMezzes:(NSArray*)remoteMezzes;
- (void)removeRemoteLiveStreamsWithContentSource:(NSString*)contentSource;
- (void)removeUnusedRemoteLiveStreams;

+ (NSString*)createDefaultWorkspaceName;

@end
