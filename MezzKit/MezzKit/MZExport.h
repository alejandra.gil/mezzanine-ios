//
//  MZExport.h
//  Mezzanine
//
//  Created by miguel on 22/07/13.
//  Copyright (c) 2013 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MZExportInfo;

@interface MZExport : NSObject

@property (nonatomic, strong) MZExportInfo *info;

- (void)reset;
- (BOOL)isExporting;
- (void)eraseDownloadFolder;

@end
