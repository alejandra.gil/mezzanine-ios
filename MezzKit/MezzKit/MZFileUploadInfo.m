//
//  MZFileUploadInfo.m
//  MezzKit
//
//  Created by Zai Chang on 3/11/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import "MZFileUploadInfo.h"

@implementation MZFileUploadInfo

-(BOOL) isPDF
{
  return [self.format isEqual:@"pdf"];
}

@end
