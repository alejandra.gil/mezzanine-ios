//
//  NSURL+Additions.h
//  MezzKit
//
//  Created by Ivan Bella Lopez on 18/05/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURL (Additions)

+ (nullable instancetype)defaultSchemeWithHostname:( NSString * _Nonnull)hostname;
+ (nullable instancetype)httpsSchemeWithHostname:(NSString * _Nonnull)hostname;
+ (nullable instancetype)wssSchemeWithHostname:(NSString * _Nonnull)hostname;
+ (nullable instancetype)tcpsSchemeWithHostname:(NSString * _Nonnull)hostname;


- (nullable instancetype)URLByReplacingSchemeWith:(NSString * _Nonnull)newScheme;
- (nullable instancetype)URLByReplacingSchemeWithWSS;
- (nullable instancetype)URLByReplacingSchemeWithTCPS;
- (nullable instancetype)URLByReplacingSchemeWithHTTPS;
- (nullable instancetype)URLByDeletingFragment;
- (nullable instancetype)URLByDeletingPath;
- (nullable instancetype)lowCaseURL;

- (BOOL)isARemoteParticipationURL;

- (NSURL * _Nonnull)sanitizeURLForMezzanineProtocol;

@end
