//
//  MZFileUploadInfo.h
//  MezzKit
//
//  Created by Zai Chang on 3/11/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MZFileUploadInfo : NSObject

@property (nonatomic, copy) NSString *workspaceUid;
@property (nonatomic, assign) NSInteger fileUid;
@property (nonatomic, copy) NSString *fileName;
@property (nonatomic, copy) NSString *format;

-(BOOL) isPDF;

@property (nonatomic, copy) NSURL *fileURL;   // URL that points to the file's source
@property (nonatomic, copy) MZImage *(^imageBlock)();  // Should set only for images coming from the library
@property (nonatomic, copy) NSData *(^imageDataBlock)();  // Should set only for images in form of NSData coming from the library
@property (nonatomic, copy) NSData *imageData;

@property (nonatomic, copy) NSArray *assetUids; // Returned by native, a list of asset uids that this file is associated with. One if its an image but can be more than one in the case of a PDF

@property (nonatomic, assign) BOOL completed;   // Track of a particular file upload transaction has been completed (when new-slide or equivalent has been received)

@end
