//
//  MZExportInfo.m
//  MezzKit
//
//  Created by Zai Chang on 1/13/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import "MZExportInfo.h"


@implementation MZExportInfo

@synthesize state;
@synthesize url;
@synthesize progress;
@synthesize filepath;
@synthesize errorMessage;
@synthesize errorTitle;
@synthesize source;
@synthesize workspaceName;

- (id)copyWithZone:(NSZone *)zone
{
  MZExportInfo *copy = [[[self class] alloc] init];
  copy.state = self.state;
  copy.url = [self.url copyWithZone:zone];
  copy.progress = self.progress;
  copy.filepath = [self.filepath copyWithZone:zone];
  copy.errorMessage = [self.errorMessage copyWithZone:zone];
  copy.errorTitle = [self.errorTitle copyWithZone:zone];
  copy.filesizeInBytes = self.filesizeInBytes;
  copy.source = self.source;
  copy.workspaceName = [self.workspaceName copyWithZone:zone];

  return copy;
}

- (void)reset
{
  state = MZExportStateNone;
  progress = 0.0;
  url = nil;
  filepath = nil;
  errorMessage = nil;
  errorTitle = nil;
  workspaceName = nil;
}

- (NSString *)sourceString
{
  NSString *sourceStr = nil;
  switch (self.source)
  {
    case MZExportSourceTypePortfolio:
      sourceStr = @"Portfolio";
      break;
    default:
      break;
  }
  return sourceStr;
}

@end
