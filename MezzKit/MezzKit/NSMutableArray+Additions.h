//
//  NSMutableArray+Additions.h
//  MezzKit
//
//  Created by miguel on 28/9/15.
//  Copyright (c) 2015 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (Additions)

- (void) moveObjectAtIndex:(NSUInteger)atIndex toIndex:(NSUInteger)toIndex;

@end
