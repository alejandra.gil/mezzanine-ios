//
//  MZCommunicatorRequestor.m
//  MezzKit
//
//  Created by Ivan Bella Lopez on 14/04/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

#import "MZCommunicatorRequestor.h"
#import "MZCommunicator+Constants.h"
#import "MZProtoFactory.h"

#import "MZSecurityManager.h"
#import "AFNetworking.h"

#import "NSString+VersionChecking.h"
#import "NSString+Localization.h"

#import "OBProtein+Mezzanine.h"
#import "OBJSONSerialization.h"

@interface MZCommunicatorRequestor ()

@property (nonatomic, strong) AFURLSessionManager *sessionManager;

@end


@implementation MZCommunicatorRequestor

- (instancetype)initWithCommunicator:(MZCommunicator*)communicator
{
  if (self = [super initWithCommunicator:communicator])
  {
  }
  return self;
}

#pragma mark - Sign In

// Since 3.0 there's no secondary certificate for encrypting username and password (see bug 14123)
- (MZTransaction *)requestClientSignInWithUsername:(NSString*)username password:(NSString*)password
{
  MZCommunicator *communicator = [self valueForKey:@"_communicator"];
  OBProtein *aProtein = [communicator.proteinFactory clientSignInRequest:username password:password];
  NSArray *patValues = [[aProtein.descrips lastObject] arrayRepresentation];
  NSMutableArray *descrips = [[NSMutableArray alloc] initWithArray:aProtein.descrips];
  
  [descrips replaceObjectAtIndex:([descrips count] - 1) withObject:patValues];
  OBProtein *protein = [[OBProtein alloc] initWithDescrips:descrips ingests:aProtein.ingests];
  
  MZURLTransaction *transaction = [MZURLTransaction transactionWithProtein:protein];
  transaction.transactionName = MZTransactionNameClientSignIn;
  
  NSURL *baseUrl = [communicator baseURLForHttpRequests];
  NSURL *url = [NSURL URLWithString:@"/encrypt/login" relativeToURL:baseUrl];
  NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLCacheStorageNotAllowed timeoutInterval:30.0];
  
  NSError *error = nil;
  NSMutableDictionary *jsonDictionary = [[protein dictionaryRepresentation] mutableCopy];
  jsonDictionary[@"pool"] = MEZZ_REQUEST_POOL;
  id jsonObject = [NSJSONSerialization dataWithJSONObject:jsonDictionary options:0 error:&error];
  if (error)
  {
    [transaction failed:error];
    [transaction ended];
    return nil;
  }
  
  [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  request.HTTPMethod = @"POST";
  request.HTTPBody = jsonObject;

  if (!_sessionManager)
    [self configureSessionManager];

  __block NSURLSessionDataTask *task = [_sessionManager dataTaskWithRequest:request
                                                         uploadProgress:nil
                                                       downloadProgress:nil
                                                      completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
                                                        DLog(@"Completed Sign In Request Session Task");

                                                        if (error)
                                                        {
                                                          MZCommunicator *communicator = [self valueForKey:@"_communicator"];
                                                          DLog(@"Sign In Transaction Session Task failed with error %@", error);
                                                          NSString *transactionName = MZTransactionNameClientSignIn;
                                                          NSArray *signInTransactions = [communicator.pendingTransactions filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"transactionName == %@", transactionName]];
                                                          // *Should* only be one but lets loop through anyways
                                                          for (MZURLTransaction *transaction in signInTransactions)
                                                          {
                                                            if (transaction.task == task)
                                                            {
                                                              [transaction failed:error];
                                                              [transaction ended];
                                                              transaction.task = nil;
                                                              [communicator.pendingTransactions removeObject:transaction];
                                                            }
                                                          }
                                                        }
                                                      }];

  transaction.task = task;
  [task resume];

  // Hack to get around the beginTransaction function which sends the protein automatically
  [communicator.pendingTransactions addObject:transaction];
  [transaction begin];
  
  return transaction;
}

#pragma mark - Private

- (void)configureSessionManager
{
  NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
  _sessionManager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
  
  _sessionManager.securityPolicy.allowInvalidCertificates = NO;
  
  // If we don't accept those content types we'll have an error (even if the request it's a success)
  AFHTTPResponseSerializer *responseSerializer = [AFHTTPResponseSerializer serializer];
  [responseSerializer setAcceptableContentTypes:[NSSet setWithObjects:@"text/html", @"text/plain", nil]];
  _sessionManager.responseSerializer = responseSerializer;

  [_sessionManager setSessionDidReceiveAuthenticationChallengeBlock:^NSURLSessionAuthChallengeDisposition(NSURLSession * _Nonnull session, NSURLAuthenticationChallenge * _Nonnull challenge, NSURLCredential *__autoreleasing  _Nullable * _Nullable credential) {
    
    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
    {
      MZSecurityManager *securityManager = [MZSecurityManager new];
      BOOL protected = [securityManager authenticateAgainstProtectionSpace:challenge.protectionSpace];
      
      if (protected)
      {
        *credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
        return NSURLSessionAuthChallengeUseCredential;
      }
      else
      {
        return NSURLSessionAuthChallengeCancelAuthenticationChallenge;
      }
    }
    
    return NSURLSessionAuthChallengePerformDefaultHandling;
  }];
}

@end
