//
//  MZRemoteMezz.m
//  Mezzanine
//
//  Created by Miguel Sánchez Valdés on 10/26/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import "MZRemoteMezz.h"
#import "NSObject+KVCAdditions.h"
#import "MZInfopresenceCall.h"


@implementation MZRemoteMezz

@synthesize collaborationState;
@synthesize online;

-(id) init
{
  if ((self = [super init]))
  {
    online = NO;
    collaborationState = MZRemoteMezzCollaborationStateNotInCollaboration;
  }
  return self;
}

-(NSString*) description
{
  return [NSString stringWithFormat:@"MZRemoteMezz %p: uid=%@, name=%@, location=%@, company=%@, state=%@", self, uid, name, location, company, [self stateString]];
}

// To be used in 2.12 Mezz only
- (NSString *) stateString
{
  if (!online)
  {
    return @"Offline";
  }
  else
  {
    switch (collaborationState) {
      case MZRemoteMezzCollaborationStateNotInCollaboration:
        return @"Online";
        break;
      case MZRemoteMezzCollaborationStateInCollaboration:
        return @"In Collaboration";
        break;
      case MZRemoteMezzCollaborationStateJoining:
        return @"Joining";
        break;
      default:
        return nil;
        break;
    }
  }
}

-(void) updateWithDictionary:(NSDictionary*)dictionary
{
  [super updateWithDictionary:dictionary];
  [self setValueIfNotNullAndChanged:dictionary[@"active"] forKey:@"online"];
}

-(void) updateWithCallResolution:(NSString*)resolution ofType:(MZInfopresenceCallType)type
{
	if ([resolution isEqual:kMZInfopresenceCallResolutionAccepted])
	{
		self.callResolution = MZInfopresenceCallResolutionAccepted;
		
		if (type == MZInfopresenceCallTypeJoinRequest)
			self.collaborationState = MZRemoteMezzCollaborationStateInCollaboration;
	}
	else if ([resolution isEqual:kMZInfopresenceCallResolutionDeclined])
	{
		self.callResolution = MZInfopresenceCallResolutionDeclined;
		self.collaborationState = MZRemoteMezzCollaborationStateNotInCollaboration;
	}
	else if ([resolution isEqual:kMZInfopresenceCallResolutionCanceled])
	{
		self.callResolution = MZInfopresenceCallResolutionCanceled;
		self.collaborationState = MZRemoteMezzCollaborationStateNotInCollaboration;
	}
	else if ([resolution isEqual:kMZInfopresenceCallResolutionCanceled] ||
					 [resolution isEqualToString:@"timeout"])
	{
		self.callResolution = MZInfopresenceCallResolutionTimedOut;
		self.collaborationState = MZRemoteMezzCollaborationStateNotInCollaboration;
	}
}

@end

