//
//  MZCommunicator+Handlers.m
//  MezzKit
//
//  Created by Zai Chang on 6/4/15.
//  Copyright (c) 2015 Oblong Industries. All rights reserved.
//

#import "MZCommunicator+Handlers.h"
#import "MZHandlerFactory.h"


@implementation MZCommunicator (Handlers)

- (void)addMessageHandlers
{
  NSString *responsePoolName = self.poolConnector.poolName;
  
  MZHandlerFactory *handlerFactory = [[MZHandlerFactory alloc] init];
  handlerFactory.communicator = self;
  handlerFactory.systemModel = self.systemModel;
  
  
  // ––– System Protein Handlers –––
  //
  [self addMessageHandler:[handlerFactory createClientEvictHandler] forPool:responsePoolName];
  
  [self addMessageHandler:[handlerFactory createClientLeaveHandler] forPool:responsePoolName];

  [self addMessageHandler:[handlerFactory createHeartbeatHandler] forPool:responsePoolName];
  
  [self addMessageHandler:[handlerFactory createMezzanineAwakeHandler] forPool:responsePoolName];
  
  [self addMessageHandler:[handlerFactory createMezzanineCapabilitiesHandler] forPool:responsePoolName];
  
  
  // ––– Workspace Protein Handlers –––
  //
  [self addMessageHandler:[handlerFactory createWorkspaceStateHandler] forPool:responsePoolName];
  
  [self addMessageHandler:[handlerFactory createWorkspaceReassignHandler] forPool:responsePoolName];
  
  [self addMessageHandler:[handlerFactory createWorkspaceOpenHandler] forPool:responsePoolName];
  
  [self addMessageHandler:[handlerFactory createWorkspaceCloseHandler] forPool:responsePoolName];
  
  [self addMessageHandler:[handlerFactory createWorkspaceCreateHandler] forPool:responsePoolName];
  
  [self addMessageHandler:[handlerFactory createWorkspaceDeleteHandler] forPool:responsePoolName];
  
  [self addMessageHandler:[handlerFactory createWorkspaceModifiedHandler] forPool:responsePoolName];
  
  [self addMessageHandler:[handlerFactory createWorkspaceRenameHandler] forPool:responsePoolName];

  [self addMessageHandler:[handlerFactory createWorkspaceSaveHandler] forPool:responsePoolName];

  [self addMessageHandler:[handlerFactory createWorkspaceListHandler] forPool:responsePoolName];

  [self addMessageHandler:[handlerFactory createClientSignInHandler] forPool:responsePoolName];
  
  [self addMessageHandler:[handlerFactory createClientSignOutHandler] forPool:responsePoolName];
  
  
  // ––– Portfolio Protein Handlers –––
  //
  [self addMessageHandler:[handlerFactory createPortfolioItemInsertHandler] forPool:responsePoolName];
  
  [self addMessageHandler:[handlerFactory createPortfolioItemReorderHandler] forPool:responsePoolName];
  
  [self addMessageHandler:[handlerFactory createPortfolioItemDeleteHandler] forPool:responsePoolName];
  
  [self addMessageHandler:[handlerFactory createPortfolioClearHandler] forPool:responsePoolName];
  
  [self addMessageHandler:[handlerFactory createPortfolioDownloadHandler] forPool:responsePoolName];


  // ––– Presentation Protein Handlers –––
  //
  [self addMessageHandler:[handlerFactory createPresentationDetailHandler] forPool:responsePoolName];

  [self addMessageHandler:[handlerFactory createPresentationScrollHandler] forPool:responsePoolName];
  

  // ––– Passkey Handlers –––
  //
  [self addMessageHandler:[handlerFactory createPasskeyEnabledHandler] forPool:responsePoolName];
  
  [self addMessageHandler:[handlerFactory createPasskeyDisabledHandler] forPool:responsePoolName];
  
  [self addMessageHandler:[handlerFactory createPasskeyDetailsHandler] forPool:responsePoolName];
  
  
  // ––– Windshield Protein Handlers –––
  //
  [self addMessageHandler:[handlerFactory createWindshieldItemCreateHandler] forPool:responsePoolName];
  
  [self addMessageHandler:[handlerFactory createWindshieldItemGrabHandler] forPool:responsePoolName];
  
  [self addMessageHandler:[handlerFactory createWindshieldItemTransformHandler] forPool:responsePoolName];
  
  [self addMessageHandler:[handlerFactory createWindshieldItemDeleteHandler] forPool:responsePoolName];
  
  [self addMessageHandler:[handlerFactory createWindshieldDetailsHandler] forPool:responsePoolName];
  
  [self addMessageHandler:[handlerFactory createWindshieldClearHandler] forPool:responsePoolName];
  
  
  // ––– Infopresence Handlers –––
  //
  [self addMessageHandler:[handlerFactory createInfopresenceSessionOutgoingRequestHandler] forPool:responsePoolName];
  
  [self addMessageHandler:[handlerFactory createInfopresenceSessionOutgoingRequestResolveHandler] forPool:responsePoolName];
  
  [self addMessageHandler:[handlerFactory createInfopresenceSessionIncomingRequestHandler] forPool:responsePoolName];
  
  [self addMessageHandler:[handlerFactory createInfopresenceSessionIncomingRequestResolveHandler] forPool:responsePoolName];
  
  [self addMessageHandler:[handlerFactory createInfopresenceRemoteMezzesStateHandler] forPool:responsePoolName];
  
  [self addMessageHandler:[handlerFactory createInfopresenceSessionDetailHandler] forPool:responsePoolName];
  
  [self addMessageHandler:[handlerFactory createInfopresenceFeldGeometryHandler] forPool:responsePoolName];
  
  [self addMessageHandler:[handlerFactory createProfileDetailsHandler] forPool:responsePoolName];

	
	// ––– Remote Participation Handlers –––
	//
	[self addMessageHandler:[handlerFactory createInfopresenceOutgoingInviteHandler] forPool:responsePoolName];

	[self addMessageHandler:[handlerFactory createInfopresenceOutgoingInviteResolveHandler] forPool:responsePoolName];
	
	[self addMessageHandler:[handlerFactory createInfopresenceIncomingInviteHandler] forPool:responsePoolName];
	
	[self addMessageHandler:[handlerFactory createInfopresenceIncomingInviteResolveHandler] forPool:responsePoolName];

	[self addMessageHandler:[handlerFactory createInfopresenceParticipantJoinHandler] forPool:responsePoolName];
	
	[self addMessageHandler:[handlerFactory createInfopresenceParticipantLeaveHandler] forPool:responsePoolName];

	[self addMessageHandler:[handlerFactory createProfileDetailsHandler] forPool:responsePoolName];


  // ––– Participant List Handlers –––
  //
  [self addMessageHandler:[handlerFactory createParticipantListHandler] forPool:responsePoolName];


  // ––– Live Streams Protein Handlers –––
  //
  [self addMessageHandler:[handlerFactory createLiveStreamsDetailHandler] forPool:responsePoolName];

  [self addMessageHandler:[handlerFactory createLiveStreamAppearHandler] forPool:responsePoolName];

  [self addMessageHandler:[handlerFactory createLiveStreamRefreshHandler] forPool:responsePoolName];

  [self addMessageHandler:[handlerFactory createLiveStreamDisappearHandler] forPool:responsePoolName];

  
  // ––– Grabbed Protein Handlers –––
  //
  [self addMessageHandler:[handlerFactory createSlideGrabbedHandler] forPool:responsePoolName];
  
  [self addMessageHandler:[handlerFactory createShielderGrabHandler] forPool:responsePoolName];
  
  [self addMessageHandler:[handlerFactory createShielderGrabbedHandler] forPool:responsePoolName];

  
  // ––– Refresh –––
  //
  [self addMessageHandler:[handlerFactory createAssetRefreshHandler] forPool:responsePoolName];


  // ––– Upload Handlers –––
  //
  [self addMessageHandler:[handlerFactory createAssetUploadProvisionHandler] forPool:responsePoolName];

  [self addMessageHandler:[handlerFactory createAssetUploadImageReadyHandler] forPool:responsePoolName];

  [self addMessageHandler:[handlerFactory createAssetUploadPDFHandler] forPool:responsePoolName];

  
  // ––– Generic error handler –––
  //
  [self addMessageHandler:[handlerFactory createGeneralErrorHandler] forPool:responsePoolName];

}

@end
