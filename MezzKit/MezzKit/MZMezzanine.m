//
//  MZMezzanine.m
//  Mezzanine
//
//  Created by Miguel Sánchez Valdés on 11/6/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import "MZMezzanine.h"
#import "NSObject+KVCAdditions.h"
#import "MZParticipant.h"

@implementation MZMezzanine

@synthesize uid;
@synthesize name;
@synthesize location;
@synthesize company;
@synthesize version;
@synthesize model;
@synthesize participants;
@synthesize roomType;

-(id) init
{
  if ((self = [super init]))
  {
    model = @"Unknown Model";
    participants = [NSMutableArray new];
    roomType = MZMezzanineRoomTypeUnknown;
  }
  return self;
}

-(NSString*) description
{
  return [NSString stringWithFormat:@"MZMezzanine %p: uid=%@, name=%@, location=%@, company=%@, model=%@, type=%@", self, uid, name, location, company, model, [self roomTypeString]];
}

-(void) updateWithDictionary:(NSDictionary*)dictionary
{
  [self setValueIfNotNullAndChanged:dictionary[@"uid"] forKey:@"uid"];
  [self setValueIfNotNullAndChanged:dictionary[@"name"] forKey:@"name"];
  [self setValueIfNotNullAndChanged:dictionary[@"location"] forKey:@"location"];
  [self setValueIfNotNullAndChanged:dictionary[@"company"] forKey:@"company"];
}

- (void) reset
{
  [self setUid:nil];
  [self setName:nil];
  [self setLocation:nil];
  [self setVersion:nil];
  [self setCompany:nil];
  [self setModel:@"Unknown Model"];
  [[self participants] removeAllObjects];
  [self setRoomType:MZMezzanineRoomTypeUnknown];
}

#pragma mark - Participants

KVO_MUTABLEARRAY_ACCESSOR_IMPLEMENTATION(Participants, participants, participants)

-(NSArray*) participantsWithUid:(NSString*)uid;
{
  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uid == %@", uid];
  return [participants filteredArrayUsingPredicate:predicate];
}

-(NSString*) roomTypeString
{
  NSString *type;
  switch (roomType) {
    case MZMezzanineRoomTypeLocal:
      type = @"Local";
      break;
    case MZMezzanineRoomTypeRemote:
      type = @"Remote";
      break;
    case MZMezzanineRoomTypeCloud:
      type = @"Cloud";
      break;
    case MZMezzanineRoomTypeUnknown:
      type = @"Unknown Type";
      break;
  }
  return type;
}

@end
