//
//  NSMutableArray+Additions.m
//  MezzKit
//
//  Created by miguel on 28/9/15.
//  Copyright (c) 2015 Oblong Industries. All rights reserved.
//

#import "NSMutableArray+Additions.h"

@implementation NSMutableArray (Additions)

- (void) moveObjectAtIndex:(NSUInteger)atIndex toIndex:(NSUInteger)toIndex
{
  if (atIndex == toIndex)
    return;

  if (toIndex >= self.count)
    @throw [NSException exceptionWithName: NSRangeException
                                   reason: [NSString stringWithFormat:@"toIndex %lu beyond bounds [0 .. %lu", (unsigned long)toIndex, (unsigned long)(self.count - 1)]
                                 userInfo: nil];

  if (atIndex >= self.count)
    @throw [NSException exceptionWithName: NSRangeException
                                   reason: [NSString stringWithFormat:@"atIndex %lu beyond bounds [0 .. %lu", (unsigned long)toIndex, (unsigned long)(self.count - 1)]
                                 userInfo: nil];

  if (atIndex < toIndex)
    for (NSInteger i = atIndex; i < toIndex; i++)
      [self exchangeObjectAtIndex:i withObjectAtIndex:i+1];
  else
    for (NSInteger i = atIndex; i > toIndex; i--)
      [self exchangeObjectAtIndex:i withObjectAtIndex:i-1];
}

@end
