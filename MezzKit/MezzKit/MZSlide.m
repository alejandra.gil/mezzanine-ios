//
//  MZSlide.m
//  Mezzanine
//
//  Created by Zai Chang on 2/15/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import "MZSlide.h"


@implementation MZSlide


-(NSString*) description
{
  return [NSString stringWithFormat:@"MZSlide %p: uid=%@, index=%lu", self, uid, (unsigned long)index];
}


#pragma mark - Content


-(NSString*) convertedOriginalURL
{
  if (fullURL)
  {
    if ([fullURL hasSuffix:@"img-slide.png"])
      return [fullURL stringByReplacingOccurrencesOfString:@"img-slide.png" withString:@"img-conf.png"];
    return fullURL;
  }
  return nil;
}


-(void) updateWithDictionary:(NSDictionary*)dictionary
{
  if (!dictionary)
    return;
  
  NSString *aUid = dictionary[@"uid"];
  if (self.uid != aUid && ![self.uid isEqual:aUid])
    self.uid = aUid;
  
  NSString *thumbUrl = dictionary[@"thumb-uri"];
  if (self.thumbURL != thumbUrl && ![self.thumbURL isEqual:thumbUrl])
  {
    self.thumbURL = thumbUrl;
    
    // This is a hack, because native side somehow changed full-uri to point instead at the origin image
    self.fullURL = [[thumbUrl stringByDeletingLastPathComponent] stringByAppendingPathComponent:@"img-slide.png"];
  }
  
  // Instead of using the original image (which full-uri) points to
  // we're using img-slide above because the full image can be as big as 6k
  // which is much too big for iOS to handle well without tiling
  //  NSString *fullUrl = [dictionary objectForKey:@"full-uri"];
  //  if (self.fullURL != fullUrl && ![self.fullURL isEqual:fullUrl])
  //    self.fullURL = fullUrl;
  
  
  BOOL anImageAvailable = [dictionary[@"image-available"] boolValue];
  if (self.imageAvailable != anImageAvailable)
    self.imageAvailable = anImageAvailable;
  
  
  NSString *source = dictionary[@"content-source"];
  if (source && ![self.contentSource isEqual:source])
    self.contentSource = source;
  
  // This is to get around the fact that the new-slide protein puts
  // content source info in asset-uid
  NSString *assetUid = dictionary[@"asset-uid"];
  if (assetUid && ![self.contentSource isEqual:assetUid])
    self.contentSource = assetUid;
  
  // Contains information on video sources
  NSDictionary *someDisplayInfo = dictionary[@"display-info"];
  if (someDisplayInfo)
    self.displayInfo = someDisplayInfo;
}

@end

