//
//  MZSystemModel.h
//  Mezzanine
//
//  Created by Zai Chang on 2/22/11.
//  Copyright 2011 Oblong Industries. All rights reserved.
//

#import "MZWorkspace.h"
#import "MZTransaction.h"
#import "MZRemoteMezz.h"
#import "MZMezzanine.h"
#import "MZWindshieldItem.h"
#import "MZExportInfo.h"
#import "MZUploadLimits.h"
#import "MZSurface.h"
#import "MZInfopresence.h"
#import "MZExport.h"
#import "MZFeatureToggles.h"

typedef NS_ENUM(NSUInteger, MZSystemState)
{
  MZSystemStateNotConnected,
  MZSystemStateWorkspace,
};

typedef NS_ENUM(NSUInteger, MZWandRachetMode)
{
  MZWandRachetModePointing,
  MZWandRachetModeCapture,
  MZWandRachetModePassthrough
};

typedef NS_ENUM(NSUInteger, MZSystemType)
{
  MZSystemTypeMezzanine,
  MZSystemTypeOcelot,
};

@interface MZSystemModel : NSObject

#pragma mark - Properties

/// System Properties
@property (nonatomic, strong) MZMezzanine *myMezzanine;
@property (nonatomic, assign) MZSystemState state;
@property (nonatomic, strong) MZUploadLimits *uploadLimits;
@property (nonatomic, strong) NSMutableArray<MZFeld *> *felds;
@property (nonatomic, strong) NSMutableArray<MZSurface *> *surfaces;

@property (nonatomic, strong, readonly) MZFeld *mainFeld;
@property (nonatomic, strong) MZFeatureToggles *featureToggles;
@property (nonatomic, strong) NSString *apiVersion;
@property (nonatomic, assign) MZSystemType systemType;

/// Secure Session
@property (nonatomic, assign) BOOL passphraseRequested;
@property (nonatomic, strong) NSString* passphrase;

/// Infopresence
@property (nonatomic, strong) NSMutableArray *remoteMezzes;
@property (nonatomic, strong) MZInfopresence *infopresence;

/// Sign In
@property (nonatomic, strong) NSString *currentUsername;
@property (nonatomic, assign) BOOL isCurrentUserSuperuser;
@property (nonatomic, assign, readonly) BOOL isSignedIn;


/// Others
@property (nonatomic, assign) MZWandRachetMode ratchetState;
@property (nonatomic, strong) NSMutableArray *whiteboards;


/// Workspace
@property (nonatomic, strong) NSMutableArray *workspaces;
@property (nonatomic, strong) MZWorkspace *currentWorkspace;


/// Export
@property (nonatomic, strong) MZExport *portfolioExport;


/// Pending States
@property (nonatomic, strong) NSMutableArray *pendingTransactions;
@property (nonatomic, assign) BOOL toBeDisconnected;

// Popup Message
// This is a temporary variable used to display a message on the interface, whilst keeping
// the separation between the communication layer and the view layer
// A more complete solution would involve a list of tasks / messages, but unnecessary for now
@property (nonatomic, strong) NSString *popupMessage;

// Error
@property (nonatomic, copy) void (^showError)(NSString* summary, NSString *description);


#pragma mark - Felds

KVO_MUTABLEARRAY_ACCESSOR_INTERFACE(Felds, felds)

-(NSString*) feldNameAtIndex:(NSInteger)index;
-(MZFeld *) feldWithName:(NSString*)aName;
-(NSDictionary*) feldAtIndex:(NSInteger)index;

-(CGRect) mainFeldspace;
-(CGFloat) feldsZDistance;

-(MZFeld *) centerFeld;
-(NSInteger) indexOfCenterFeld;



#pragma mark - Workspaces

KVO_MUTABLEARRAY_ACCESSOR_INTERFACE(Workspaces, workspaces)

-(MZWorkspace*) workspaceWithUid:(NSString*)uid;
-(NSArray*) workspacesWithOwner:(NSString*)owner;
-(NSArray*) allOwnersOfWorkspaces;
-(void) removeAllWorkspaces;

-(void) openWorkspace:(NSDictionary*)workspaceDict;


// Testing
@property (nonatomic, assign) BOOL testModeEnabled;



#pragma mark - Whiteboards

KVO_MUTABLEARRAY_ACCESSOR_INTERFACE(Whiteboards, whiteboards)

-(NSDictionary*) whiteboardWithUid:(NSString*)uid;


#pragma mark - Surfaces

KVO_MUTABLEARRAY_ACCESSOR_INTERFACE(Surfaces, surfaces)

-(MZSurface*) surfaceWithName:(NSString*)name;
-(MZSurface *) surfaceWithFeld:(MZFeld *)feld;
-(NSArray *) surfacesInPrimaryWindshield;
-(NSArray *) surfacesInExtendedWindshield;
-(NSArray *) surfaceNames;
-(NSArray *) surfaceNamesInPrimaryWindshield;
-(NSArray *) surfaceNamesInExtendedWindshield;

-(NSInteger) sideWallScreenCount;

#pragma mark - Ratcheting

-(NSString*) ratchetStateAsString;


#pragma mark - Transactions

KVO_MUTABLEARRAY_ACCESSOR_INTERFACE(PendingTransactions, pendingTransactions)

-(void) clearPendingTransactions;

#pragma mark - Upload Transactions

-(void) cancelPendingUploadsForTarget:(NSString*)target workspaceUid:(NSString*)workspaceUid; // workspaceUid can be nil in case of general cancel
-(void) reset;


#pragma mark - Remote Mezzanines

KVO_MUTABLEARRAY_ACCESSOR_INTERFACE(RemoteMezzes, remoteMezzes)

-(MZRemoteMezz*) remoteMezzWithUid:(NSString*)uid;
-(NSArray*) remoteMezzesWithCollaborationState:(MZRemoteMezzCollaborationState)collaborationState;
-(void) removeAllRemoteMezzanines;
-(void) updateRemoteMezzanineInfopresenceState; // 3.0 updates collaboration state of all Remote Mezzanines

@end



@interface MZSystemModel (Protein)

-(void) updateWithDictionary:(NSDictionary*)dictionary;
-(void) updateCurrentWorkspaceWithDictionary:(NSDictionary*)workspaceDictionary;
-(void) updateLiveStreamsWithVideoShielders:(NSArray *)items;
@end

