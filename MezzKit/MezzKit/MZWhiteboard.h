//
//  MZWhiteboard.h
//  MezzKit
//
//  Created by Zai Chang on 3/5/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MZWhiteboard : NSObject

@property (nonatomic, copy) NSString *uid;

-(instancetype) initWithUid:(NSString*)aUid;

@end
