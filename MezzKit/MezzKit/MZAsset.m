//
//  MZAsset.m
//  Mezzanine
//
//  Created by Zai Chang on 2/15/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import "MZAsset.h"
#import "NSObject+KVCAdditions.h"


@implementation MZAsset

@synthesize name;
@synthesize imageAvailable;

@synthesize isPendingDeletion;


+ (instancetype)assetWithUid:(NSString*)anUid
{
  MZAsset *asset = [[MZAsset alloc] init];
  asset.uid = anUid;
  return asset;
}


-(id) initWithDictionary:(NSDictionary*)dictionary
{
  self = [super init];
  if (self)
  {
    [self updateWithDictionary:dictionary];
  }
  return self;
}


-(NSString*) description
{
  return [NSString stringWithFormat:@"MZAsset %p: uid=%@, name=%@, url=%@", self, uid, name, thumbURL];
}


-(void) updateWithDictionary:(NSDictionary*)dictionary
{
  [self setValueIfNotNullAndChanged:dictionary[@"uid"] forKey:@"uid"];
  [self setValueIfNotNullAndChanged:dictionary[@"file-name"] forKey:@"name"];
  [self setValueIfNotNullAndChanged:dictionary[@"thumb-uri"] forKey:@"thumbURL"];
  [self setValueIfNotNullAndChanged:dictionary[@"large-uri"] forKey:@"largeURL"];
  // Used in New Asset
  [self setValueIfNotNullAndChanged:dictionary[@"full-image-uri"] forKey:@"fullURL"];
  // Used in mez-state deck-detail
  [self setValueIfNotNullAndChanged:dictionary[@"full-uri"] forKey:@"fullURL"];
  // Maybe API could use same ingest name for same content of ingest -- Native Bug  

  BOOL available = [dictionary[@"image-available"] boolValue];
  if (self.imageAvailable != available)
    self.imageAvailable = available;
}

@end
