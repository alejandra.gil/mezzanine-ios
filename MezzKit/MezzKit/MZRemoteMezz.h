//
//  MZRemoteMezz.h
//  Mezzanine
//
//  Created by Miguel Sánchez Valdés on 10/26/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MZMezzanine.h"


typedef NS_ENUM(NSUInteger, MZRemoteMezzCollaborationState)
{
  MZRemoteMezzCollaborationStateNotInCollaboration,
  MZRemoteMezzCollaborationStateInvited,
  MZRemoteMezzCollaborationStateJoining,
  MZRemoteMezzCollaborationStateInCollaboration
};


typedef NS_ENUM(NSUInteger, MZInfopresenceCallResolution)
{
  MZInfopresenceCallResolutionUnknown=0,
  MZInfopresenceCallResolutionAccepted,
  MZInfopresenceCallResolutionDeclined,
  MZInfopresenceCallResolutionCanceled,
  MZInfopresenceCallResolutionTimedOut,
};


typedef NS_ENUM(NSUInteger, MZInfopresenceCallType)
{
	MZInfopresenceCallTypeInvite = 0,
	MZInfopresenceCallTypeJoinRequest
};


@interface MZRemoteMezz : MZMezzanine
{
  MZRemoteMezzCollaborationState collaborationState;
}

@property (nonatomic, assign) BOOL online;
@property (nonatomic, assign) MZRemoteMezzCollaborationState collaborationState;

// Temporarily putting this here, but should be in separate InfopresenceCall class with has the current mezzBeingJoined property embedded
@property (nonatomic, assign) MZInfopresenceCallResolution callResolution;

-(void) updateWithDictionary:(NSDictionary*)dictionary;

-(void) updateWithCallResolution:(NSString*)resolution ofType:(MZInfopresenceCallType)type;

// To be used in 2.12 Mezz only
- (NSString *) stateString;

@end
