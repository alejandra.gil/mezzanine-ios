//
//  MZPresentation.m
//  MezzKit
//
//  Created by Zai Chang on 1/16/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import "MZPresentation.h"
#import "MZWorkspace.h"


@implementation MZPresentation

@synthesize currentSlideIndex;
@dynamic numberOfSlides;
@synthesize slides;

- (instancetype)init
{
  self = [super init];
  if (self)
  {
    slides = [NSMutableArray array];
  }
  return self;
}



#pragma mark - Slides

KVO_MUTABLEARRAY_ACCESSOR_IMPLEMENTATION(Slides, slides, slides)

-(NSInteger) numberOfSlides
{
  return slides.count;
}

-(void) setCurrentSlideIndex:(NSInteger)newIndex
{
  // Sanitize bad values?
  //currentSlideIndex = MAX(MIN(newIndex, [slides count]-1), 0);
  
  // Or just ignore them?
  if (newIndex < 0 || newIndex >= slides.count)
    return;
  currentSlideIndex = newIndex;
}

-(MZPortfolioItem*) slideWithUid:(NSString*)aUid
{
  for (MZPortfolioItem *slide in slides)
  {
    if ([slide.uid isEqual:aUid])
      return slide;
  }
  return nil;
}

-(NSString*) slideUidAtIndex:(NSInteger)index
{
  if (index >= 0 && index < slides.count)
    return [slides[index] uid];
  return nil;
}


-(void) updateSlideIndexes
{
  NSInteger index = 0;
  for (MZPortfolioItem *slide in slides)
    slide.index = index++;
}


-(NSArray*) slidesWithContentSource:(NSString*)contentSource
{
  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"contentSource == %@", contentSource];
  return [slides filteredArrayUsingPredicate:predicate];
}


-(void) moveSlide:(MZPortfolioItem*)slide toIndex:(NSInteger)index
{
  NSInteger currentIndex = [slides indexOfObject:slide];
  DLog(@"moveSlide: currentIndex:%ld toIndex:%ld", (long)currentIndex, (long)index);
  
  BOOL invalidTargetIndex = (index >= slides.count);
  if (invalidTargetIndex)
    return;
  
  if (currentIndex != NSNotFound && index != currentIndex)
  {
    slide.pendingReorder = YES;
    [self removeObjectFromSlidesAtIndex:currentIndex];
    [self insertObject:slide inSlidesAtIndex:index];
    slide.pendingReorder = NO;
    
    [self updateSlideIndexes];
  }
}


-(void) moveSlideWithUid:(NSString*)aUid toIndex:(NSInteger)index
{
  MZPortfolioItem *slide = [self slideWithUid:aUid];
  if (slide)
    [self moveSlide:slide toIndex:index];
}


-(void) swapSlide:(MZPortfolioItem*)slidea withSlide:(MZPortfolioItem*)slideb
{
  DLog(@"Swapping slides %@ and %@", slidea, slideb);
  
  NSUInteger indexa = [slides indexOfObject:slidea];
  NSUInteger indexb = [slides indexOfObject:slideb];
  
  if (indexa == NSNotFound || indexb == NSNotFound)
    return;
  
  // Naive non-KVO way ...
  //[slides exchangeObjectAtIndex:[slides indexOfObject:slidea] withObjectAtIndex:[slides indexOfObject:slideb]];
  
  // Attempt 1
  //[self removeObjectFromSlidesAtIndex:indexa];
  //[self insertObject:slideb inSlidesAtIndex:indexa];
  //[self removeObjectFromSlidesAtIndex:indexb];
  //[self insertObject:slidea inSlidesAtIndex:indexb];
  
  // Attempt 2 - A KVO way ...
  NSMutableIndexSet *indexSet = [NSMutableIndexSet indexSet];
  NSMutableArray *insertedSlides = [NSMutableArray array];
  // Indexes have to be in order??
  if (indexa < indexb)
  {
    [indexSet addIndex:indexa];
    [insertedSlides addObject:slideb];
    [indexSet addIndex:indexb];
    [insertedSlides addObject:slidea];
  }
  else
  {
    [indexSet addIndex:indexb];
    [insertedSlides addObject:slidea];
    [indexSet addIndex:indexa];
    [insertedSlides addObject:slideb];
  }
  [self removeSlidesAtIndexes:indexSet];
  [self insertSlides:insertedSlides atIndexes:indexSet];
  
  [self updateSlideIndexes];
}


-(void) removeAllSlides
{
  if (slides.count > 0)
  {
    NSIndexSet *indexSet = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, slides.count)];
    [self removeSlidesAtIndexes:indexSet];
  }
  
  [[NSNotificationCenter defaultCenter] postNotificationName:MZWorkspaceSlidesClearedNotification object:self];
}


- (void)updateWithDictionary:(NSDictionary*)dictionary
{
  if (dictionary == nil)
    return;
  
  NSNumber *currentIndex = dictionary[@"current-index"];
  if (currentIndex)
  {
    NSInteger index = [currentIndex integerValue];
    if (index >= 0 && index < self.numberOfSlides && self.currentSlideIndex != index)
      self.currentSlideIndex = index;
  }
  
  
  NSNumber *activeNumber = dictionary[@"active"];
  if (activeNumber && activeNumber.boolValue != self.active)
    self.active = activeNumber.boolValue;
}


- (void)updateWithPortfolioItems:(NSArray*)portfolioItems
{
  NSInteger numberOfSlides = portfolioItems.count;
  NSInteger deltaOfNumberOfSlides = numberOfSlides - self.slides.count;

  // TODO - Fix the non-ideal algorithm below as it doesn't truly handle bulk insert/remove gracefully
  //        and instead replaces content of one MZSlide with another's to achieve model consistency.
  if (deltaOfNumberOfSlides > 0)
  {
    NSMutableArray *newSlides = [NSMutableArray array];
    NSMutableIndexSet *indexSet = [NSMutableIndexSet indexSet];
    for (NSInteger i=0; i<deltaOfNumberOfSlides; i++)
    {
      MZPortfolioItem *slide = [[MZPortfolioItem alloc] init];
      [newSlides addObject:slide];
      [indexSet addIndex:(self.slides.count + i)];
    }
    [self insertSlides:newSlides atIndexes:indexSet];
  }
  else if (deltaOfNumberOfSlides < 0)
  {
    NSMutableIndexSet *indexSet = [NSMutableIndexSet indexSetWithIndexesInRange:NSMakeRange(numberOfSlides, -deltaOfNumberOfSlides)];
    [self removeSlidesAtIndexes:indexSet];
  }


  for (NSInteger i=0; i<numberOfSlides; i++)
  {
    // If I'm just updating the slide based on index,
    // this is susceptible to errors while slides are being reordered
    // We'd need UIDs to resolve this, but for now ...
    MZPortfolioItem *slide = slides[i];
    NSDictionary *slideInfo = portfolioItems[i];
    
    // Or use slide ID???
    /*
     NSString *aUid = [slideInfo objectForKey:@"uid"];
     MZPortfolioItem *slide = [self slideWithUid:aUid];
     if (slide)
     {
     // How to reorder slides correctly???
     }
     */
    
    slide.index = i;
    [slide updateWithDictionary:slideInfo];
  }
}


@end
