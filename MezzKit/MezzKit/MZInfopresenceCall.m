//
//  MZInfopresenceCall.m
//  MezzKit
//
//  Created by Zai Chang on 8/17/15.
//  Copyright (c) 2015 Oblong Industries. All rights reserved.
//

#import "MZInfopresenceCall.h"


NSString* const kMZInfopresenceCallTypeInvite = @"invite";
NSString* const kMZInfopresenceCallTypeJoinRequest = @"join-request";

NSString* const kMZInfopresenceCallResolutionDeclined = @"declined";
NSString* const kMZInfopresenceCallResolutionAccepted = @"accepted";
NSString* const kMZInfopresenceCallResolutionTimeOut = @"timeout";
NSString* const kMZInfopresenceCallResolutionTime_Out = @"time-out";
NSString* const kMZInfopresenceCallResolutionCanceled = @"canceled";


@implementation MZInfopresenceCall

- (instancetype)initWithDictionary:(NSDictionary*)dictionary
{
	self = [super init];
	if (self)
	{
		_uid = dictionary[@"uid"];
		
		NSString *type = dictionary[@"type"];
		if ([type isEqual:kMZInfopresenceCallTypeInvite])
			_type = MZInfopresenceCallTypeInvite;
		else if ([type isEqual:kMZInfopresenceCallTypeJoinRequest])
			_type = MZInfopresenceCallTypeJoinRequest;

		[self updateWithDictionary:dictionary];
	}
	return self;
}

- (void)updateWithDictionary:(NSDictionary*)dictionary
{
	NSString *callResolution = dictionary[@"resolution"];
	if (callResolution.length)
	{
		if ([callResolution isEqual:kMZInfopresenceCallResolutionAccepted])
			self.resolution = MZInfopresenceCallResolutionAccepted;
		else if ([callResolution isEqual:kMZInfopresenceCallResolutionCanceled])
			self.resolution = MZInfopresenceCallResolutionCanceled;
		else if ([callResolution isEqual:kMZInfopresenceCallResolutionDeclined])
			self.resolution = MZInfopresenceCallResolutionDeclined;
		else if ([callResolution isEqual:kMZInfopresenceCallResolutionTimeOut] ||
             [callResolution isEqual:kMZInfopresenceCallResolutionTime_Out])
			self.resolution = MZInfopresenceCallResolutionTimedOut;
	}
	
	NSNumber *sentUtc = dictionary[@"sent-utc"];
	if (sentUtc)
	{
		self.sentDate = [NSDate dateWithTimeIntervalSinceReferenceDate:sentUtc.longValue];
	}
	
	NSNumber *receivedUtc = dictionary[@"received-utc"];
	if (sentUtc)
	{
		self.receivedDate = [NSDate dateWithTimeIntervalSinceReferenceDate:receivedUtc.longValue];
	}
}

- (BOOL)isInvite
{
	return _type == MZInfopresenceCallTypeInvite;
}

- (BOOL)isJoinRequest
{
	return _type == MZInfopresenceCallTypeJoinRequest;
}

- (BOOL)didNotSuceed	// If the call did not go through whether declined, canceled or timedout
{
	return _resolution > MZInfopresenceCallResolutionAccepted;
}

@end
