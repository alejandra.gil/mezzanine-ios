//
//  MZWindshieldItem.h
//  Mezzanine
//
//  Created by Zai Chang on 5/8/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import "MZAsset.h"
#import "MZFeld.h"
#import "MZSurface.h"


@class MZSystemModel;

@interface MZWindshieldItem : MZAsset
{
  NSString *assetUid;
  // Frame of the windshield item, expressed in relative coordinates
  CGRect frame;
  NSInteger ordinal;
  BOOL isBeingGrabbed;
}

@property (nonatomic, strong) NSString *assetUid;
@property (nonatomic, assign) CGRect frame;
@property (nonatomic, assign) NSInteger ordinal;
@property (nonatomic, assign) BOOL isBeingGrabbed;
@property (nonatomic, strong) NSDictionary *displayInfo;

@property (nonatomic, assign, readonly) BOOL isImage;
@property (nonatomic, assign, readonly) BOOL isVideo;

// 2.12 properties
@property (nonatomic, strong) MZFeld *feld;
@property (nonatomic, assign) CGFloat aspectRatio;
@property (nonatomic, strong) NSDictionary *feldRelativeCoordinates;
@property (nonatomic, strong) NSDictionary *feldRelativeSize;
@property (nonatomic, assign) CGFloat scale;

// 2.14 properties
@property (nonatomic, strong) MZSurface *surface;

// Consumes the ingest from a shielder-new protein
-(void) updateWithDictionary:(NSDictionary *)dictionary;

// Consumes the ingest from a shielder-transform protein
-(void) updateWithTransformDictionary:(NSDictionary*)dictionary;

// Somewhat awkward function .. should figure out a way to handle this without more dependency on MZSystemModel
- (void)updateSurfaceAndFeldWithDictionary:(NSDictionary*)dictionary usingSystemModel:(MZSystemModel*)systemModel;

- (NSDictionary*)dictionaryRepresentation;  // N.B. currently only used for tests, not production ready serialization code

@end
