//
//  MZCommunicator.h
//  Mezzanine
//
//  Created by Zai Chang on 10/13/10.
//  Copyright 2010 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MZSystemModel.h"
#import "MZFileUploadInfo.h"
#import "MZTransaction.h"
#import "OBCommunicator.h"
#import "OBMessageHandler.h"

@class MZSessionManager;

#define kMZCommunicatorErrorDomain @"MZCommunicator"
#define kMZCommunicatorErrorCode_ConnectionTimeout -1000
#define kMZCommunicatorErrorCode_LoginTimeout -1001
#define kMZCommunicatorErrorCode_JoinDenied -1002
#define kMZCommunicatorErrorCode_IncompatibleProteinVersion -1003
#define kMZCommunicatorErrorCode_InvalidServerCertificate -1004
#define kMZCommunicatorErrorCode_InvalidRPProtocol -1005


#define ENABLE_HEARTBEAT_TIMEOUT 1

// Join mechanism
#define MZCommunicatorBeganJoinNotification @"MezzanineBeganJoin"
#define MZCommunicatorJoinSuccesNotification @"MezzanineJoinSuccess"
#define MZCommunicatorJoinFailedNotification @"MezzanineJoinFailed"
#define MZCommunicatorIncorrectPassphraseNotification @"MezzanineIncorrectPassphrase"

#define MZCommunicatorBeganRequestingStateNotification @"MezzanineBeganRequestingState"
#define MZCommunicatorReceviedInitialStateNotification @"MezzanineReceivedInitialState"
#define MZCommunicatorReceviedInitialWorkspaceListStateNotification @"MezzanineReceivedInitialWorkspaceListState"
#define MZCommunicatorReceviedInitialInfopresenceStateNotification @"MezzanineReceivedInitialInfopresenceState"

#define MZCommunicatorRequestRemoteParticipationSettingsNotification @"MezzanineRequestRemoteParticipationSettings"

// Regular user-initiated disconnection
#define MZCommunicatorDidDisconnectNotification @"MezzanineDidDisconnect"

// Disconnection Reasons
#define MZCommunicatorDidDisconnectServerRestart @"MZCommunicatorDidDisconnectServerRestart"
#define MZCommunicatorDidDisconnectHeartbeatTimeout @"MZCommunicatorDidDisconnectHeartbeatTimeout"
#define MZCommunicatorDidDisconnectClientEvicted @"MZCommunicatorDidDisconnectClientEvicted"
#define MZCommunicatorDidDisconnectPlasmaSessionEnded @"MZCommunicatorDidDisconnectPlasmaSessionEnded"
#define MZCommunicatorDidDisconnectZiggySessionEnded @"MZCommunicatorDidDisconnectZiggySessionEnded"

// The connection to Mezzanine was severed by acts of God
//#define MZCommunicatorWasDisconnectedNotification @"MezzanineWasDisconnected"
// Use OBCommunicatorWasDisconnectedNotification instead


#define MEZZ_IMAGE_DEPOSIT_POOL @"mz-incoming-assets"
#define MEZZ_REQUEST_POOL @"mz-into-native"
#define MEZZ_RESPONSE_POOL @"mz-from-native"
#define MEZZ_WEBSOCKET @"mz-websocket"


// These constants define the minimum and maximum native Mezzanine
// version that MZCommunicator is able to function properly with
#define MZCOMMUNICATOR_PROTEIN_VERSION_MIN @"3.6"
#define MZCOMMUNICATOR_PROTEIN_VERSION_MAX @"4.0"

@class MZCommunicatorRequestor;
@class MZProteinFactory;
@class MZUploader;
@protocol OBWSSPoolConnectorDelegate;

@interface MZCommunicator : OBCommunicator <OBCommunicatorDelegate, OBWSSPoolConnectorDelegate>
{
  NSString *joinPassphrase; // Storage of a one-time passphrase that will be used the next time a join is requested, use mainly by the URL join scheme
  BOOL awaitingJoinResponse;
  BOOL receivedInitialState;
  BOOL hasJoinedSession;
  
  MZProteinFactory *proteinFactory;
  NSInteger webServicePort;
  
  MZSystemModel *systemModel;
  OBMessageHandler *disableLockedSessionOnJoinHandler;
  BOOL isConnectedToCloudInstance;
}

@property (nonatomic,strong) NSString *joinPassphrase;
@property (nonatomic,assign) BOOL receivedInitialState;
@property (nonatomic,assign) BOOL hasJoinedSession;

@property (nonatomic,copy) OBProteinFilterBlock provenanceFilter;
@property (nonatomic,strong) MZProteinFactory *proteinFactory;
@property (nonatomic,strong) MZCommunicatorRequestor *requestor;
@property (nonatomic,strong) MZUploader *uploader;

@property (readonly,strong) OBPoolConnector *poolConnector;

@property (nonatomic,strong) MZSystemModel *systemModel;
@property (nonatomic,strong) MZSessionManager *sessionManager;

@property (nonatomic,copy) NSString *machineName;

@property (readonly,assign) BOOL isARemoteParticipationConnection;
@property (readonly,strong) NSString *meetingID;
@property (readonly,strong) NSURL *httpImageUploadURL;

// System Use Notification
@property (nonatomic, strong) NSString *systemUseNotificationText;
@property (nonatomic, assign) BOOL systemUseNotification;

// Name of user connecting to server
@property (nonatomic,copy) NSString *displayName;

- (NSString*)inviteURLString;

- (instancetype)initWithServerUrl:(NSURL *)aServerUrl;
- (instancetype)initWithServerUrl:(NSURL *)aServerUrl displayName:(NSString * _Nonnull)displayName;
- (void)startConnection;

- (BOOL)checkConnection;
- (void)requestJoinAfterSystemUseNotification;
- (void)requestJoinWithPassphrase:(NSString *)passphrase;
- (void)requestJoinWithRemoteParticipantName:(NSString *)name passphrase:(NSString *)passphrase;
- (void)cancelPassphraseJoin;
- (void)disconnectWithMessage:(NSString *)message;
- (void)postDidDisconnectNotification:(NSString *)message;

// Version checking
- (BOOL)isProteinVersionAboveMinimum:(NSString*)version;
- (BOOL)isProteinVersionBelowMaximum:(NSString*)version;


// Response proteins handling
- (BOOL)proteinIsPSAOrPM:(OBProtein*)protein;
- (BOOL)proteinIsPSAOrResponseForMe:(OBProtein*)protein;


// Helper method to reduce lines necessary for wrapping a protein in a transaction
- (MZTransaction *)beginTransactionWithName:(NSString*)transactionName proteinBlock:(OBProtein *(^)(void))proteinBlock;


// HTTP Requests
- (NSURL*)baseURLForHttpRequests;
- (NSURL*)urlForResource:(NSString*)resource;
- (NSURL *)RPFileDownload:(NSString*)resource;


// Inform of uploading
- (NSInteger)numberOfImagesBeingUploaded;
- (NSInteger)numberOfFilesBeingUploaded;


// Handlers cancelling
- (void)cancelTransaction:(MZTransaction *)transaction;

// Session
- (void)endSession;

@end
