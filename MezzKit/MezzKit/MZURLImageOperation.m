//
//  MZURLImageOperation.m
//  MezzKit
//
//  Created by Ivan Bella Lopez on 08/06/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

#import "MZURLImageOperation.h"
#import "AFURLSessionManager.h"

@interface MZURLImageOperation ()

@property (nonatomic, strong, readwrite, nullable) NSURLSessionTask *task;

@end


@implementation MZURLImageOperation

- (instancetype)initDownloadOperationWithManager:(AFURLSessionManager *)manager
                                         request:(NSURLRequest *)request
                                        progress:(nullable void (^)(NSProgress *downloadProgress)) downloadProgressBlock
                                     destination:(nullable NSURL * (^)(NSURL *targetPath, NSURLResponse *response))destination
                               completionHandler:(nullable void (^)(MZURLImageOperation *operation, NSURLResponse *response, NSURL * _Nullable filePath, NSError * _Nullable error))completionHandler
{
  if (self = [super init])
  {
    _actionsPerTargets = [@{} mutableCopy];
    
    self.task = [manager downloadTaskWithRequest:request progress:downloadProgressBlock destination:destination completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nullable filePath, NSError * _Nullable error) {
      if (completionHandler) completionHandler(self, response, filePath, error);
      [self completeOperation];
    }];
  }
  
  return self;
}


- (void)main
{
  [self.task resume];
}


- (void)completeOperation
{
  self.task = nil;
  [super completeOperation];
}


- (void)cancel
{
  [self.task cancel];
  [super cancel];
}

@end
