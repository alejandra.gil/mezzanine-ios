//
//  MZTransaction.h
//  Mezzanine
//
//  Created by Zai Chang on 2/8/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import "OBTransaction.h"


// MZTransaction Names
#define MZMezzanineCapabilitiesTransactionName @"mez-caps"
#define MZMezzanineStateTransactionName     @"mez-state"

#define MZRatchetStateTransactionName       @"ratchet"

#define MZImageUploadRequestTransactionName @"upload-images"
#define MZImageUploadTransactionName        @"image-ready"

#define MZLoginTransactionName              @"login"
#define MZLogoutTransactionName             @"logout"

#define MZItemGrabTransactionName           @"item-grab"
#define MZItemRelinquishTransactionName     @"item-relinquish"

#define MZAddWindshieldItemTransactionName       @"shielder-create"
#define MZTransformWindshieldItemTransactionName @"shielder-transform"
#define MZDeleteWindshieldItemTransactionName    @"shielder-delete"
#define MZWindshieldDetailsTransactionName       @"windshield-detail"
#define MZClearWindshieldTransactionName         @"clear-windshield"
#define MZWindshieldArrangeTransactionName       @"windshield-arrange"

#define MZClearAssetsTransactionName             @"clear-paramus"
#define MZClearDeckTransactionName               @"clear-deck"

#define MZProfileDetailsTransactionName  @"profile-detail"
#define MZRemoteMezzanineDetailsTransactionName  @"remote-mezzanines-detail"
#define MZCollaborationDetailsTransactionName  @"collaboration-detail"
#define MZPlaceCallTransactionName          @"place-call"
#define MZPlaceCallTransactionName          @"place-call"
#define MZCancelPlacedCallTransactionName   @"cancel-call"
#define MZAnswerCallTransactionName         @"answer-drop"
#define MZLeaveCollaborationTransactionName @"leave-collaboration"

#define MZPassphraseEnableTransactionName     @"passphrase-enable"
#define MZPassphraseDisableTransactionName    @"passphrase-disable"
#define MZPassphraseDetailsTransactionName    @"passphrase-detail"

#define MZWhiteboardCaptureTransactionName    @"whiteboard-capture"

#define MZDownloadDeckTransactionName       @"download-deck"
#define MZDownloadParamusTransactionName    @"download-paramus"
#define MZPDFReadyTransactionName           @"pdf-ready"
#define MZFilesUploadRequestTransactionName @"files-upload"
#define MZUploadLimitsTransactionName       @"upload-limits"

// Upload Target Types
#define MZUploadTransactionTargetAsset @"asset"
#define MZUploadTransactionTargetSlide @"slide"
#define MZUploadTransactionTargetBoth  @"both"


#define MZTransactionErrorDomain  @"MZTransactionError"


@interface MZTransaction : OBTransaction

@property (strong) MZImage *thumbnail;
@property (strong) NSString *title;
@property (strong) NSString *status;
@property (assign) BOOL isContinuous;

-(instancetype) initWithProtein:(OBProtein*)protein;
+(instancetype) transactionWithProtein:(OBProtein*)protein;

@end



@interface MZURLTransaction : MZTransaction
{
  __weak NSURLSessionTask *task;
}

@property (weak) NSURLSessionTask *task;

@end


// TODO - Replace concrete subclasses and instead use a dictionary to store
// extra information such as uids. The transaction name would then be the
// identifier of transaction type.


@interface MZFileBatchUploadRequestTransaction : MZTransaction

@property (nonatomic, copy) NSString *uid;
@property (nonatomic, copy) NSString *workspaceUid;
@property (nonatomic, strong) NSArray *fileUploadInfos;
@property (nonatomic, assign) NSInteger completedUploads;
@property (nonatomic, assign) NSInteger numberOfUploads;
@property (nonatomic, strong) NSMutableArray *transactionsInProgress;

+(MZFileBatchUploadRequestTransaction *) transactionWithProtein:(OBProtein*)protein;

@property (nonatomic, assign, readonly) BOOL completed;

@end



@interface MZImageUploadRequestTransaction : MZTransaction

@property (copy) NSString *format;
@property (copy) NSString *uid;
@property (assign) NSInteger numberOfImages;

+(MZImageUploadRequestTransaction *) transactionWithProtein:(OBProtein*)protein;

@end


// And Image Upload Request that keeps track of the state
// of all its images uploads (completed, in progress, total)
@interface MZImageBatchUploadRequestTransaction : MZImageUploadRequestTransaction

@property (assign) NSInteger completedImageUploads;
@property (strong) NSMutableArray *transactionsInProgress;
@property (copy) NSString *target;

+(MZImageBatchUploadRequestTransaction *) transactionWithProtein:(OBProtein*)protein;

@end



@interface MZImageUploadTransaction : MZTransaction

@property (copy) NSString *format;
@property (copy) NSString *uid;
@property (copy) NSString *workspaceUid;  // Used in 2.12+
@property (assign) BOOL uploadAsSlide;
@property (assign) BOOL uploadAsAsset;

+(MZImageUploadTransaction *) transactionWithProtein:(OBProtein*)protein;

@end
