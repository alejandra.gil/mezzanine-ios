//
//  NSString+VersionChecking.m
//  MezzKit
//
//  Created by Ivan Bella Lopez on 06/11/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

#import "NSString+VersionChecking.h"

@implementation NSString (VersionChecking)

- (BOOL)isVersionOrGreater:(NSString*)versionString
{
  return [self compare:versionString options:NSNumericSearch] != NSOrderedAscending;
}

@end
