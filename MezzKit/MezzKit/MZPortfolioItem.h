//
//  MZPortfolioItem.h
//  MezzKit
//
//  Created by Miguel Sanchez on 28/05/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import "MZItem.h"


/// Model object for an item within a workspace presentation and porfolio

@interface MZPortfolioItem : MZItem

@end

