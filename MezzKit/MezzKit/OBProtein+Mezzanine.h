//
//  OBProtein+Mezzanine.h
//  Mezzanine
//
//  Created by Zai Chang on 1/25/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import "OBProtein.h"

@interface OBProtein (Mezzanine)

// Extract provenance the protein, assuming that it has a to: or pat: descrip
// which is followed by an array consisting of a [provenance, transactionId] pair
-(NSString*) extractProvenance;
-(BOOL) provenanceIsEqual:(NSString*)aProvenance;

-(NSString*) proteinVersion;

-(NSString*) transactionId;

@end
