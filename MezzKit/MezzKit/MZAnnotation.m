//
//  MZAnnotation.m
//  Mezzanine
//
//  Created by Zai Chang on 10/22/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import "MZAnnotation.h"

@implementation MZAnnotation

-(id) init
{
  self = [super init];
  if (self)
  {
    self.fillColour = [MZColor yellowColor];
    self.strokeColour = [MZColor yellowColor];
    self.strokeWidth = 5.0;
  }
  return self;
}


-(id) copyWithZone:(NSZone *)zone
{
  MZAnnotation *copy = [[[self class] alloc] init];
  copy.fillColour = [self.fillColour copyWithZone:zone];
  copy.strokeColour = [self.strokeColour copyWithZone:zone];
  copy.strokeWidth = self.strokeWidth;
  return copy;
}


@end



@implementation MZFreehandAnnotation

-(id) init
{
  self = [super init];
  if (self)
  {
    self.points = [NSMutableArray array];
  }
  return self;
}


-(id) copyWithZone:(NSZone *)zone
{
  MZFreehandAnnotation *copy = [super copyWithZone:zone];
  copy.points = [self.points copyWithZone:zone];
  return copy;
}

@end



@implementation MZTextAnnotation

@end



@implementation MZArrowAnnotation

@end
