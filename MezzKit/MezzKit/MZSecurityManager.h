//
//  MZSecurityManager.h
//  MezzKit
//
//  Created by miguel on 10/01/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MZSecurityManager : NSObject

// Authentication Processes
- (BOOL)authenticateAgainstProtectionSpace:(NSURLProtectionSpace *)challenge;
- (BOOL)authenticateAgainstServerTrust:(SecTrustRef)serverTrust host:(NSString *)host;
- (NSURLCredential *)credentialsFromAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge;

// Certificate loader
- (CFMutableArrayRef)loadCertificatesFromFilePath:(NSString *)pemPath;

@end
