//
//  MZDownloadManager.m
//  Mezzanine
//
//  Created by miguel on 26/11/13.
//  Copyright (c) 2013 Oblong Industries. All rights reserved.
//

#import "MZDownloadManager.h"
#import <CommonCrypto/CommonDigest.h>
#import "objc/runtime.h"

#import "AFNetworking.h"
#import "MZAssetCache.h"
#import "MZSecurityManager.h"
#import "NSString+Localization.h"

#import <NIInMemoryCache.h>
#import "NIState.h"

#import "MZURLImageOperation.h"
#import "MZDiskImageOperation.h"


@interface MZDownloadManager ()

@property (nonatomic, strong) AFURLSessionManager *sessionManager;
@property (atomic, strong) NSOperationQueue *urlImageQueue;
@property (atomic, strong) NSOperationQueue *diskImageQueue;

@end


@implementation MZDownloadManager

typedef void (^MezzURLImageSuccessBlock)(MZImage *image);
typedef void (^MezzURLImageErrorBlock)(NSError *error);

+ (MZDownloadManager *)sharedLoader
{
  static MZDownloadManager *_sharedLoader = nil;
  if (_sharedLoader == nil)
  {
    _sharedLoader = [MZDownloadManager new];
  }
  return _sharedLoader;
}


- (id)init
{
  self = [super init];
  if (self)
  {
    _urlImageQueue = [NSOperationQueue new];
    _urlImageQueue.name = @"urlImageQueue";
    [_urlImageQueue setMaxConcurrentOperationCount:NSOperationQueueDefaultMaxConcurrentOperationCount];
    
    _diskImageQueue = [NSOperationQueue new];
    _diskImageQueue.name = @"diskImageQueue";
    [_diskImageQueue setMaxConcurrentOperationCount:NSOperationQueueDefaultMaxConcurrentOperationCount];
    
    [self configureSession];
    
  }
  
  return self;
}


#pragma mark - Public

- (void)loadImageWithURL:(NSURL *)url atTarget:(id)target success:(void (^)(MZImage *image))successBlock error:(void (^)(NSError *error))errorBlock
{
  [self loadImageWithURL:url atTarget:target success:successBlock error:errorBlock canceled:nil];
}


- (void)loadImageWithURL:(NSURL *)url atTarget:(id)target success:(void (^)(MZImage *image))successBlock error:(void (^)(NSError *error))errorBlock canceled:(void (^)(void))canceledBlock
{
  [self loadImageWithURL:url atTarget:target success:successBlock error:errorBlock canceled:nil cachingOption:MZDownloadCacheTypeDefault];
}


- (void)loadImageWithURL:(NSURL *)url atTarget:(id)target success:(void (^)(MZImage *image))successBlock error:(void (^)(NSError *error))errorBlock canceled:(void (^)(void))canceledBlock cachingOption:(MZDownloadCacheType)cachingOption
{
  if (_pauseDownloads)
  {
    if (errorBlock)
    {
      NSDictionary *userInfo = @{NSLocalizedFailureReasonErrorKey:MZLocalizedString(@"MZDownloadManager Paused", nil)};
      NSError *error = [[NSError alloc] initWithDomain:@"MZDownloadManager" code:NSURLErrorUnknown userInfo:userInfo];
      errorBlock(error);
    }
    return;
  }
  
  // 1. Safe checks
  if (!url)
  {
    if (errorBlock)
    {
      NSDictionary *userInfo = @{NSLocalizedFailureReasonErrorKey:MZLocalizedString(@"MZDownloadManager Invalid URL", nil)};
      NSError *error = [[NSError alloc] initWithDomain:@"MZDownloadManager" code:NSURLErrorBadURL userInfo:userInfo];
      errorBlock(error);
    }
    return;
  }
  
  if (!target)
  {
    if (errorBlock)
    {
      NSDictionary *userInfo = @{NSLocalizedFailureReasonErrorKey:MZLocalizedString(@"MZDownloadManager Invalid Target", nil)};
      NSError *error = [[NSError alloc] initWithDomain:@"MZDownloadManager" code:NSURLErrorBadURL userInfo:userInfo];
      errorBlock(error);
    }
    return;
  }
  
  // 2. Test purposes
  if ([self testGetImage:url success:successBlock error:errorBlock]) return;

  // 3. MD5 Cache string
  NSString *cacheKey = [self cacheKeyForURL:url];
  
  // 4. Load from memory if available
  MZImage *cachedImage = [self cachedImageForKey:cacheKey];
  if (successBlock && cachedImage)
  {
    successBlock(cachedImage);
    return;
  }
  
  
  // 5. Load from disk if available
  // 5.1. Avoid duplicating operations that reads the same cached image for a particular target
  for (MZDiskImageOperation *op in _diskImageQueue.operations)
  {
    if ([op.target isEqual:target] && [op.cacheKey isEqualToString:cacheKey] && !op.isCancelled)
    {
      if (canceledBlock)
        canceledBlock();
      return;
    }
  }
  
  // 5.2. Actual reading operation
  MZDiskImageOperation *readOperation = [MZDiskImageOperation operationWithTarget:target cacheKey:cacheKey completion:^(MZImage *image) {
    if (image && successBlock)
    {
      successBlock(image);
      
      // We should try to keep images in memory whenever is possible. It's faster
      // We threat disk as a second storage
      [self cacheImageIfNeeded:cachingOption image:image key:cacheKey];
      return;
    }
  }];
  
  
  [_diskImageQueue addOperation:readOperation];
  
  // 6. Download image
  // 6.1. Prepare dictionary of actions for selected target for error and success cases
  NSValue *targetKey = [NSValue valueWithNonretainedObject:target];
  NSDictionary *actions = @{};
  if (successBlock && errorBlock)
    actions = @{@"success" : successBlock, @"error" : errorBlock};
  else if (successBlock)
    actions = @{@"success" : successBlock};
  
  // 6.2. Avoid duplicating operations that downloads same image for a particular target
  // In case it is a duplicate, we assign the operation to the target
  for (MZURLImageOperation *op in _urlImageQueue.operations)
  {
    if ([op.task.originalRequest.URL.absoluteString isEqualToString:url.absoluteString]
        && !op.isCancelled
        && !op.isFinished)
    {
      if (actions)
        (op.actionsPerTargets)[targetKey] = actions;
      return;
    }
  }
  
  // 6.3 Download image at a given URL
  NSURLRequest *request = [NSURLRequest requestWithURL:url];
  __weak typeof(self) __self = self;
  
  MZURLImageOperation *operation = [[MZURLImageOperation alloc]
                                    initDownloadOperationWithManager:_sessionManager
                                    request:request
                                    progress:nil
                                    destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response) {
                                      if (!response) return nil;
                                      
                                      if ([response.URL isFileURL] || ((NSHTTPURLResponse *) response).statusCode == 200)
                                        return [NSURL fileURLWithPath:[[MZAssetCache sharedCache] cachePathForKey:cacheKey]];
                                      
                                      return nil;
                                    }
                                    completionHandler:^(MZURLImageOperation *operation, NSURLResponse * _Nonnull response, NSURL * _Nullable filePath, NSError * _Nullable error) {
                                      
                                      if (operation.isCancelled && canceledBlock)
                                        canceledBlock();
                                      
                                      if (!error && filePath)
                                      {
                                        NSData *imageData = [NSData dataWithContentsOfURL:filePath options:NSDataReadingMappedIfSafe error:&error];
                                        // 6.3.1. Valid image
                                        if ([__self validateImageData:imageData type:response.MIMEType])
                                        {
                                          MZImage *image = [[MZImage alloc] initWithData:imageData];
                                          
                                          // 6.3.1.1. Cache image
                                          [__self cacheImageIfNeeded:cachingOption image:image key:cacheKey];
                                          
                                          // 6.3.1.2. Success
                                          [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                            for (NSDictionary *targetActions in [operation.actionsPerTargets allValues])
                                            {
                                              if (![operation isCancelled])
                                              {
                                                MezzURLImageSuccessBlock successBlock = targetActions[@"success"];
                                                if (successBlock) {
                                                  successBlock(image);
                                                }
                                              }
                                            }
                                          }];
                                        }
                                        // 6.3.2. Invalid image
                                        else
                                        {
                                          // 6.3.2.1 Send error code to retry
                                          [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                            for (NSDictionary *targetActions in [operation.actionsPerTargets allValues])
                                            {
                                              MezzURLImageErrorBlock errorBlock = targetActions[@"error"];
                                              if (errorBlock)
                                              {
                                                NSString *errorString = MZLocalizedString(@"MZDownloadManager Incorrect Download", nil);
                                                NSError *error = [NSError errorWithDomain:kMZDownloadManagerErrorDomain
                                                                                     code:kMZDownloadManagerErrorCode_IncorrectDownload
                                                                                 userInfo:@{NSLocalizedDescriptionKey: errorString}];
                                                errorBlock(error);
                                              }
                                            }
                                          }];
                                        }
                                      }
                                      else
                                      {
                                        // 6.3.3. If something fails execute all error blocks
                                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                          for (NSDictionary *targetActions in [operation.actionsPerTargets allValues])
                                          {
                                            MezzURLImageErrorBlock errorBlock = targetActions[@"error"];
                                            if (errorBlock)
                                              errorBlock(error);
                                          }
                                        }];
                                      }
                                      
                                      [self removeOperationAssociatedWithTarget:target];
                                      operation = nil;
                                    }];
  
  
  // 6.4. Associate operation with target and add it to the queue
  if (actions)
    (operation.actionsPerTargets)[targetKey] = actions;
  
  [self associateTarget:target withOperation:operation];
  [_urlImageQueue addOperation:operation];
  
  // 6.5 Observe when the operation is finished to update network indicator
  [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:nil];
  [self showNetworkActiviyIndicator:YES];
}


- (void)cancelLoadingURLForTarget:(id)target
{
  // 1. Cancel any operation reading images from disk
  NSArray *readOperationsWithSameTarget = [_diskImageQueue.operations filteredArrayUsingPredicate:
                                           [NSPredicate predicateWithFormat:@"target == %@ && isCancelled == NO", target]];
  
  if ([readOperationsWithSameTarget count])
  {
    MZDiskImageOperation *readOp = readOperationsWithSameTarget[0];
    [readOp cancel];
  }
  
  // 2. Cancel URL requests operations and set them as no longer valid
  MZURLImageOperation* operation = [self getOperationAssociatedWithTarget:target];
  if (operation)
  {
    NSValue *targetKey = [NSValue valueWithNonretainedObject:target];
    NSMutableDictionary *actions = (operation.actionsPerTargets)[targetKey];
    if (actions)
    {
      [operation.actionsPerTargets removeObjectForKey:targetKey];
      
      // When there is no more targets interested in the result, cancel the op completely.
      if ([[operation.actionsPerTargets allKeys] count] == 0)
        [operation cancel];
    }
    [self removeOperationAssociatedWithTarget:target];
  }
  
  [self showNetworkActiviyIndicator:NO];
}


- (void)cancelAllOperations
{
  NSArray *readFromDiskOperations = _diskImageQueue.operations;
  [readFromDiskOperations makeObjectsPerformSelector:@selector(cancel)];
  
  NSArray *URLImageOperations = _urlImageQueue.operations;
  [URLImageOperations makeObjectsPerformSelector:@selector(cancel)];
  
  NSOperationQueue *networkOperationQueue = [Nimbus networkOperationQueue];
  [networkOperationQueue cancelAllOperations];
  
  [self showNetworkActiviyIndicator:NO];
}


- (void)unloadImageFromMemoryCacheWithURL:(NSURL *)url
{
  NSString *cacheKey = [self cacheKeyForURL:url];
  [[Nimbus imageMemoryCache] removeObjectWithName:cacheKey];
}


- (void)unloadImageFromDiskCacheWithURL:(NSURL *)url
{
  NSString *cacheKey = [self cacheKeyForURL:url];
  [[MZAssetCache sharedCache] removeKey:cacheKey];
}


#pragma mark - Support Methods

static char fromURLOperationKey;

// Object association accessors between download targets and its download operation
- (void)associateTarget:(id)target withOperation:(MZURLImageOperation *)operation
{
  objc_setAssociatedObject(target, &fromURLOperationKey, operation, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}


- (void)removeOperationAssociatedWithTarget:(id)target
{
  objc_setAssociatedObject(target, &fromURLOperationKey, nil, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}


- (MZURLImageOperation *)getOperationAssociatedWithTarget:(id)target
{
  return objc_getAssociatedObject(target, &fromURLOperationKey);
}

// TODO These methods should be part of NSURL / NSString categories

- (NSString *)cacheKeyForURL:(NSURL *)url
{
  // The cacheKey is just a MD5 enconded value of the URL.
  // The reason of using a value like this is because the in-disk TTURLCache
  // uses this value as the filename and the url absolute string is not a good one.
  return [self md5HexDigest:[url absoluteString]];
}

- (NSString *)md5HexDigest:(NSString *)input
{
  const char* str = [input UTF8String];
  unsigned char result[CC_MD5_DIGEST_LENGTH];
  CC_MD5(str, (uint)strlen(str), result);
  
  NSMutableString *ret = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH*2];
  for(int i = 0; i<CC_MD5_DIGEST_LENGTH; i++) {
    [ret appendFormat:@"%02x",result[i]];
  }
  return ret;
}


- (BOOL)testGetImage:(NSURL *)url success:(void (^)(MZImage *image))successBlock  error:(void (^)(NSError *error))errorBlock
{
  BOOL success = NO;
  // This is to support loading of local assets in offline mode and unit tests.
  // For MezzKit tests we shouldn't enter here since we load a different image.
  if ([url isFileURL] &&
      [[url relativePath] containsString:@"Test-Image-Mezzanine"])
  {
    NSString *filePath = [url relativePath];
    
    MZImage *image;
#if TARGET_OS_IPHONE
    image = [MZImage imageWithContentsOfFile:filePath];
#else
    image = [[MZImage alloc] initWithContentsOfFile:filePath];
#endif
    
    if (image && successBlock)
    {
      successBlock(image);
    }
    else if (errorBlock)
    {
      NSDictionary *userInfo = @{NSLocalizedFailureReasonErrorKey:MZLocalizedString(@"MZDownloadManager Cannot Load Image", nil)};
      NSError *error = [[NSError alloc] initWithDomain:@"MZDownloadManager" code:NSURLErrorBadURL userInfo:userInfo];
      errorBlock(error);
    }
    
    success = YES;
  }
  
  return success;
}


- (void)showNetworkActiviyIndicator:(BOOL)show
{
#if TARGET_OS_IPHONE
  __weak typeof(self) __self = self;

  dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    if (__self.urlImageQueue.operationCount)
      [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    else
      [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
  });

#endif
}


#pragma mark - AFURLSessionManager

- (void)configureSession
{
  NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
  _sessionManager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
  
  _sessionManager.securityPolicy.allowInvalidCertificates = NO;
  
  [_sessionManager setSessionDidReceiveAuthenticationChallengeBlock:^NSURLSessionAuthChallengeDisposition(NSURLSession * _Nonnull session, NSURLAuthenticationChallenge * _Nonnull challenge, NSURLCredential *__autoreleasing  _Nullable * _Nullable credential) {
    
    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
    {
      MZSecurityManager *securityManager = [MZSecurityManager new];
      BOOL protected = [securityManager authenticateAgainstProtectionSpace:challenge.protectionSpace];
      
      if (protected)
      {
        *credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
        return NSURLSessionAuthChallengeUseCredential;
      }
      else
      {
        return NSURLSessionAuthChallengeCancelAuthenticationChallenge;
      }
    }
    
    return NSURLSessionAuthChallengePerformDefaultHandling;
  }];
}


#pragma mark - Caching methods

- (void)cacheImageIfNeeded:(MZDownloadCacheType)cachingOption image:(MZImage *)image key:(NSString *)key
{
  // Cache the image from the response in memory if requested
  if (cachingOption == MZDownloadCacheTypeDefault || cachingOption == MZDownloadCacheTypeOnlyInMemory)
    [[Nimbus imageMemoryCache] storeObject:image withName:key];
  
  // Remove file from in-disk-cache if not requested.
  if (cachingOption == MZDownloadCacheTypeOnlyInMemory || cachingOption == MZDownloadCacheTypeNone)
    [[MZAssetCache sharedCache] removeKey:key];
}


- (MZImage *)cachedImageForKey:(NSString *)key
{
  if (!key)
    return nil;
  
  return [[Nimbus imageMemoryCache] objectWithName:key];
}


- (MZImage *)cachedImageForURL:(NSURL *)url
{
  if (!url)
    return nil;
  
  return [self cachedImageForKey:[self cacheKeyForURL:url]];
}


- (MZImage *)cachedImageInDiskForURL:(NSURL *)url
{
  if (!url)
    return nil;
  
  // Get image from cache and succeed if exists
  NSString *cacheKey = [self cacheKeyForURL:url];
  
  NSString *filepathInCache = [[MZAssetCache sharedCache] cachePathForKey:cacheKey];
  MZImage *cachedImage = [[MZImage alloc] initWithContentsOfFile:filepathInCache];
  return cachedImage;
}


#pragma mark - Image validation

- (BOOL)validateImageData:(NSData *)imageData type:(NSString *)encodingName
{
  if (([encodingName isEqualToString:@"image/png"] && [self dataIsValidPNG:imageData]) ||
      ([encodingName isEqualToString:@"image/jpeg"] && [self dataIsValidJPEG:imageData]))
  {
    return YES;
  }
  
  return NO;
}


// http://stackoverflow.com/questions/3848280/catching-error-corrupt-jpeg-data-premature-end-of-data-segment
- (BOOL)dataIsValidJPEG:(NSData *)data
{
  if (!data || data.length < 2) return NO;
  
  NSInteger totalBytes = data.length;
  const char *bytes = (const char*)[data bytes];
  
  return (bytes[0] == (char)0xff &&
          bytes[1] == (char)0xd8 &&
          bytes[totalBytes-2] == (char)0xff &&
          bytes[totalBytes-1] == (char)0xd9);
}

// http://stackoverflow.com/questions/10552145/detect-if-png-file-is-corrupted-in-objective-c/12726493#12726493
- (BOOL)dataIsValidPNG:(NSData *)data
{
  if (!data || data.length < 12)
  {
    return NO;
  }
  
  NSInteger totalBytes = data.length;
  const char *bytes = (const char *)[data bytes];
  
  return (bytes[0] == (char)0x89 && // PNG
          bytes[1] == (char)0x50 &&
          bytes[2] == (char)0x4e &&
          bytes[3] == (char)0x47 &&
          bytes[4] == (char)0x0d &&
          bytes[5] == (char)0x0a &&
          bytes[6] == (char)0x1a &&
          bytes[7] == (char)0x0a &&
          
          bytes[totalBytes - 12] == (char)0x00 && // IEND
          bytes[totalBytes - 11] == (char)0x00 &&
          bytes[totalBytes - 10] == (char)0x00 &&
          bytes[totalBytes - 9] == (char)0x00 &&
          bytes[totalBytes - 8] == (char)0x49 &&
          bytes[totalBytes - 7] == (char)0x45 &&
          bytes[totalBytes - 6] == (char)0x4e &&
          bytes[totalBytes - 5] == (char)0x44 &&
          bytes[totalBytes - 4] == (char)0xae &&
          bytes[totalBytes - 3] == (char)0x42 &&
          bytes[totalBytes - 2] == (char)0x60 &&
          bytes[totalBytes - 1] == (char)0x82);
}


#pragma mark - Oberservation

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context
{
  if ([keyPath isEqualToString:@"isFinished"])
  {
    [object removeObserver:self forKeyPath:@"isFinished"];
    [self showNetworkActiviyIndicator:NO];
  }
}


@end

