//
//  MZSessionManager.h
//  MezzKit
//
//  Created by Ivan Bella Lopez on 04/02/2019.
//  Copyright © 2019 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, MZSessionType) {
  MZSessionTypeUnknown,
  MZSessionTypePlasmaWebProxy,
  MZSessionTypeZiggy
};

NS_ASSUME_NONNULL_BEGIN

#define MZSessionManagerShowPasskeyViewNotification @"MZSessionManagerShowPasskeyViewNotification"
#define MZSessionManagerHidePasskeyViewNotification @"MZCommunicatorHidePasskeyViewNotification"
#define MZSessionManagerUpdatedPasskeyViewNotification @"MZSessionManagerUpdatedPasskeyViewNotification"

#define MZSessionManagerErrorDomain @"MZSessionManagerErrorDomain"
#define kMZSessionManagerErrorCode_ErrorRetrievingSessionToken -4000
#define kMZSessionManagerErrorCode_WrongPassword -4001
#define kMZSessionManagerErrorCode_SystemStateSerializationError -4002
#define kMZSessionManagerErrorCode_ValidCertificateWithIncorrectHost -4003
#define kMZSessionManagerErrorCode_ErrorCleaningUpSession -4004
#define kMZSessionManagerErrorCode_ErrorOnJoiningHeartbeatRequest -4005
#define kMZSessionManagerErrorCode_ErrorOnAuthorizeRequest -4006


typedef void (^MZSessionManagerHandshakeCompletionBlock) (NSError * _Nullable error, NSString * _Nullable token);
typedef void (^MZSessionManagerCleanupCompletionBlock) (NSError * _Nullable error, NSString * _Nullable token);

@interface MZSessionManager : NSObject <NSURLSessionDelegate>

@property (nonatomic, copy) NSString *provenance;
@property (nonatomic, copy) NSString *displayName;

@property (nonatomic, readonly) MZSessionType sessionType;
@property (nonatomic, readonly) NSDate *sessionStartTime;

- (void)requestStartSessionWithServer:(NSURL *)serverURL completion:(MZSessionManagerHandshakeCompletionBlock)completionBlock;
- (void)requestCleanUpSessionWithCompletion:(MZSessionManagerCleanupCompletionBlock)completionBlock;
- (void)requestEndSession;
- (void)reset;

@end

NS_ASSUME_NONNULL_END
