//
//  MZParticipant.h
//  MezzKit
//
//  Created by miguel on 3/10/18.
//  Copyright © 2018 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MZParticipant : NSObject

@property (nonatomic, strong) NSString *uid;
@property (nonatomic, strong) NSString *displayName;
@property (nonatomic, strong) NSString *type;

-(void) updateWithDictionary:(NSDictionary *)dictionary;

@end

NS_ASSUME_NONNULL_END
