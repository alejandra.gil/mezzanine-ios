//
//  MZUploader.h
//  MezzKit
//
//  Created by miguel on 1/2/19.
//  Copyright © 2019 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MZCommunicator.h"
#import "MZTransaction.h"

///
/// This class manages all related to file upload requests.
///

NS_ASSUME_NONNULL_BEGIN

typedef void (^MZFileBatchUploadRequestContinuationBlock)(MZFileBatchUploadRequestTransaction *, NSArray *, BOOL);
typedef void (^MZFileBatchUploadRequestLimitReachedBlock)(MZFileBatchUploadRequestTransaction *, NSArray *, MZFileBatchUploadRequestContinuationBlock);

@interface MZUploader : NSObject


- (instancetype)initWithCommunicator:(MZCommunicator*)communicator;

- (MZTransaction *)requestFilesUpload:(NSArray *)filesUploadInfos
                         workspaceUid:(NSString *)workspaceUid
                         limitReached:(void (^)(MZFileBatchUploadRequestTransaction *, NSArray *, MZFileBatchUploadRequestContinuationBlock))limitReachedBlock
                singleUploadCompleted:(void (^)(void))singleUploadBlock
                         errorHandler:(void (^)(NSError *))errorBlock;

- (void)beginPDFUploadForFiles:(NSArray *)fileids;

// Manual clean up of failed image uploads in a batch upload request transactions
- (void)removeTransactionForFailedUploadAssetUID:(NSString *)uid;

- (NSInteger)numberOfImagesBeingUploaded;
- (NSInteger)numberOfFilesBeingUploaded;
- (NSInteger)numberOfUploadTransactions;

- (void)cancelAllPendingFileUploads;
- (void)clearPendingUploadTransactionsForAssetUid:(NSString *)assetUid;

@end

NS_ASSUME_NONNULL_END
