//
//  MZAssetCache.m
//  Mezzanine
//
//  Created by Zai Chang on 5/2/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import "MZAssetCache.h"

#define TT_CACHE_EXPIRATION_AGE_NEVER     (1.0 / 0.0)   // inf


@interface TTURLCache (PrivateMethods)

+ (BOOL)createPathIfNecessary:(NSString*)path;
@end

@implementation MZAssetCache

-(id) init
{
  static NSString *cacheName = @"Assets";
  self = [super initWithName:cacheName];
  if (self)
  {
    // For offline viewing of workspaces, one would need to use NSDocumentDirectory instead of NSCachesDirectory
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cachePath = [paths[0] stringByAppendingPathComponent:cacheName];
    self.cachePath = cachePath;
    [TTURLCache createPathIfNecessary:cachePath];
  }
  return self;
}

+(MZAssetCache*) sharedCache
{
	TTURLCache *sharedCache = [TTURLCache sharedCache];
	if ([sharedCache isKindOfClass:[MZAssetCache class]])
		return (MZAssetCache*) sharedCache;
	return nil;
}



# pragma mark - Thumbnail Image

-(NSString *) cacheKeyForThumbnailAssetImage:(MZAsset*)asset
{
  return [asset.uid stringByAppendingString:@".thumbnail"];
}


-(void) storeThumbnailImage:(MZImage*)image forAsset:(MZAsset*)asset
{
  if (asset.uid.length == 0)
    return;
  NSString *cacheKey = [self cacheKeyForThumbnailAssetImage:asset];
	
#if TARGET_OS_IPHONE
  NSData *data = UIImagePNGRepresentation(image);
#else
  NSData *data = [NSBitmapImageRep representationOfImageRepsInArray:[image representations] usingType:NSPNGFileType properties:@{}];
#endif
  [self storeData:data forKey:cacheKey];
}


-(MZImage*) thumbnailImageForAsset:(MZAsset*)asset
{
  if (asset.uid.length == 0)
    return nil;
  NSString *cacheKey = [self cacheKeyForThumbnailAssetImage:asset];
  NSData *data = [self dataForKey:cacheKey expires:TT_CACHE_EXPIRATION_AGE_NEVER timestamp:nil];
  return [[MZImage alloc] initWithData:data];
}


-(void) removeThumbnailImageForAsset:(MZAsset *)asset
{
  [self removeKey:[self cacheKeyForThumbnailAssetImage:asset]];
}


-(NSString *) cacheKeyForThumbnailSlideImage:(MZItem*)slide
{
  if (slide.contentSource.length == 0)
    return nil;

  return [slide.contentSource stringByAppendingString:@".thumbnail"];
}


-(void) storeThumbnailImage:(MZImage*)image forSlide:(MZItem*)slide
{
  if (slide.contentSource.length == 0)
    return;
  NSString *cacheKey = [self cacheKeyForThumbnailSlideImage:slide];
#if TARGET_OS_IPHONE
  NSData *data = UIImagePNGRepresentation(image);
#else
  NSData *data = [NSBitmapImageRep representationOfImageRepsInArray:[image representations] usingType:NSPNGFileType properties:@{}];
#endif
  [self storeData:data forKey:cacheKey];
}


-(MZImage*) thumbnailImageForSlide:(MZItem*)slide
{
  if (slide.contentSource.length == 0)
    return nil;
  NSString *cacheKey = [self cacheKeyForThumbnailSlideImage:slide];
  NSData *data = [self dataForKey:cacheKey expires:TT_CACHE_EXPIRATION_AGE_NEVER timestamp:nil];
  return [[MZImage alloc] initWithData:data];
}


-(void) removeThumbnailImageForSlide:(MZItem *)slide
{
  [self removeKey:[self cacheKeyForThumbnailSlideImage:slide]];
}


#pragma mark - Full Image

-(NSString *) cacheKeyForFullSlideImage:(MZItem*)slide
{
  if (slide.contentSource.length == 0)
    return nil;

  return [slide.contentSource stringByAppendingString:@".full"];
}


-(void) storeFullImage:(MZImage*)image forSlide:(MZItem*)slide
{
  if (slide.contentSource.length == 0)
    return;
  NSString *cacheKey = [self cacheKeyForFullSlideImage:slide];
#if TARGET_OS_IPHONE
  NSData *data = UIImagePNGRepresentation(image);
#else
  NSData *data = [NSBitmapImageRep representationOfImageRepsInArray:[image representations] usingType:NSPNGFileType properties:@{}];
#endif
  [self storeData:data forKey:cacheKey];
}


-(MZImage*) fullImageForSlide:(MZItem*)slide
{
  if (slide.contentSource.length == 0)
    return nil;
  NSString *cacheKey = [self cacheKeyForFullSlideImage:slide];
  NSData *data = [self dataForKey:cacheKey expires:TT_CACHE_EXPIRATION_AGE_NEVER timestamp:nil];
  return [[MZImage alloc] initWithData:data];
}


-(NSURL*) urlForFullSlideImage:(MZItem*)slide
{
  if (slide.contentSource.length == 0)
    return nil;
  NSString *cacheKey = [self cacheKeyForFullSlideImage:slide];
  NSString* filePath = [self cachePathForKey:cacheKey];
  return [NSURL fileURLWithPath:filePath];
}


-(void) removeFullImageForSlide:(MZItem *)slide
{
  [self removeKey:[self cacheKeyForFullSlideImage:slide]];
}


#pragma mark - Original Image

-(NSString *) cacheKeyForOriginalSlideImage:(MZItem*)slide
{
  if (slide.contentSource.length == 0)
    return nil;

  return [slide.contentSource stringByAppendingString:@".original"];
}


-(void) storeOriginalImage:(MZImage*)image forSlide:(MZItem*)slide
{
  if (slide.contentSource.length == 0)
    return;
  NSString *cacheKey = [self cacheKeyForOriginalSlideImage:slide];
#if TARGET_OS_IPHONE
  NSData *data = UIImagePNGRepresentation(image);
#else
  NSData *data = [NSBitmapImageRep representationOfImageRepsInArray:[image representations] usingType:NSPNGFileType properties:@{}];
#endif
  [self storeData:data forKey:cacheKey];
}


-(MZImage*) originalImageForSlide:(MZItem*)slide
{
  if (slide.contentSource.length == 0)
    return nil;
  NSString *cacheKey = [self cacheKeyForOriginalSlideImage:slide];
  NSData *data = [self dataForKey:cacheKey expires:TT_CACHE_EXPIRATION_AGE_NEVER timestamp:nil];
  return [[MZImage alloc] initWithData:data];
}


-(NSURL*) urlForOriginalSlideImage:(MZItem*)slide
{
  if (slide.contentSource.length == 0)
    return nil;
  NSString *cacheKey = [self cacheKeyForOriginalSlideImage:slide];
  NSString* filePath = [self cachePathForKey:cacheKey];
  return [NSURL fileURLWithPath:filePath];
}


-(void) removeOriginalImageForSlide:(MZItem *)slide
{
  [self removeKey:[self cacheKeyForOriginalSlideImage:slide]];
}


#pragma mark - V1 Support

// In Progress
-(BOOL) isV1Asset:(NSString*)filename
{
  return [filename hasPrefix:@"as-0000000"];
}


// In Progress
-(void) cleanupV1Assets
{
  // Since mezzanine v1 asset uids can collide across servers, they need to be
  // cleaned up between connections.
  // Doing this instead of adding server id or creating something unique since
  // 
  NSError *error = nil;
  NSArray *contents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:self.cachePath error:&error];
  if (error)
  {
    NSLog(@"MZAssetCache: Error during cleanupV1AAssets: %@", error);
    return;
  }
  
  for (NSString *filename in contents)
  {
    if ([self isV1Asset:filename])
    {
      NSString *path = [self.cachePath stringByAppendingPathComponent:filename];
      [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
      if (error)
        NSLog(@"Error deleting v1 asset at %@", path);
    } 
  }
}

@end
