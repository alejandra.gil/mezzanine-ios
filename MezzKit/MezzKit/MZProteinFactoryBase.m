//
//  MZProteinFactoryBase.m
//  MezzKit
//
//  Created by Ivan Bella Lopez on 07/04/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

#import "MZProteinFactoryBase.h"

@interface MZProteinFactoryBase()
{
  NSString *proteinVersion;
}

@end


@implementation MZProteinFactoryBase


- (NSArray*)getNextProvenanceAndTransactionId
{
  return @[_provenance, @(++_transactionId)];
}

- (NSArray*)requestDescripsWithCommand:(NSString*)command
{
  return @[@"mezzanine",
           [NSString stringWithFormat:@"prot-spec v%@", proteinVersion],
           @"request",
           command,
           @"from:",
           [self getNextProvenanceAndTransactionId]];
}

@end