//
//  MZExport_Private.h
//  MezzKit
//
//  Created by Ivan Bella Lopez on 13/06/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

#import "MZExport.h"
#import "AFNetworking.h"

@interface MZExport ()

@property (nonatomic, strong) AFURLSessionManager *downloadManager;
@property (nonatomic, strong) NSString *downloadFolder;

@end
