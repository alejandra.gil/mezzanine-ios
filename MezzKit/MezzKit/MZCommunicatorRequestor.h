//
//  MZCommunicatorRequestor.h
//  MezzKit
//
//  Created by Ivan Bella Lopez on 14/04/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

#import "MZProto/MZRequestor.h"

@interface MZCommunicatorRequestor : MZRequestor

/* 
 This class manages more customized requests. For the normal requests, we refer to its superclass
*/

// Sign in process
- (MZTransaction *)requestClientSignInWithUsername:(NSString*)username password:(NSString*)password;


@end
