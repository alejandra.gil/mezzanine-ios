//
//  NSString+Localization.m
//  MezzKit
//
//  Created by Miguel Sánchez Valdés on 17/11/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

#import "NSString+Localization.h"
#import "MezzKit.h"

@implementation NSString (Localization)

NSString *MZLocalizedString(NSString* key, NSString* comment)
{
  static NSBundle* bundle = nil;
  if (!bundle)
  {
    NSString* path = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"MezzKit.bundle"];

    // Tests look for MezzKit bundle at their own bundle location
    if (![[NSFileManager defaultManager] fileExistsAtPath:path])
      path = [[[NSBundle bundleWithIdentifier:@"com.oblong.MezzKitTests"] resourcePath] stringByAppendingPathComponent:@"MezzKit.bundle"];

    bundle = [NSBundle bundleWithPath:path];

    if (!bundle)
    {
      NSBundle *mezzKitFramework = [NSBundle bundleForClass:[MZCommunicator class]];
      path = [mezzKitFramework.resourcePath stringByAppendingPathComponent:@"MezzKit.bundle"];
      bundle = [NSBundle bundleWithPath:path];
    }
  }

  return [bundle localizedStringForKey:key value:key table:nil];
}

@end
