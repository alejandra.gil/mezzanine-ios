//
//  MZInfopresence.m
//  MezzKit
//
//  Created by miguel on 5/6/15.
//  Copyright (c) 2015 Oblong Industries. All rights reserved.
//

#import "MZInfopresence.h"
#import "NSObject+KVCAdditions.h"

NSString* const kMZInfopresenceStatusInactive = @"inactive";
NSString* const kMZInfopresenceStatusPending = @"pending";
NSString* const kMZInfopresenceStatusActive = @"active";
NSString* const kMZInfopresenceStatusInterrupted = @"interrupted";

NSString* const kMZInfopresenceRemoteAccessStatusActive = @"active";
NSString* const kMZInfopresenceRemoteAccessStatusInactive = @"inactive";
NSString* const kMZInfopresenceRemoteAccessStatusStarting = @"starting";


@implementation MZInfopresence

- (instancetype)init
{
  self = [super init];
  if (self)
  {
    _rooms = [NSMutableArray new];
    _remoteParticipants = [NSMutableArray new];
    _incomingCalls = [NSMutableArray new];
		_outgoingCalls = [NSMutableArray new];
    _remoteAccessStatus = MZInfopresenceRemoteAccessStatusInactive;
  }
  return self;
}

- (void)reset
{
  [_rooms removeAllObjects];
  [_remoteParticipants removeAllObjects];
	[_outgoingCalls removeAllObjects];
  [_incomingCalls removeAllObjects];

  _remoteAccessStatus = MZInfopresenceRemoteAccessStatusInactive;
  _status = MZInfopresenceStatusInactive;
  _remoteAccessUrl = nil;

  _videoChatService = nil;
  _pexipNode = nil;
  _pexipConference = nil;
}


#pragma mark - Incoming Calls

KVO_MUTABLEARRAY_ACCESSOR_IMPLEMENTATION(IncomingCalls, incomingCalls, _incomingCalls)

- (MZInfopresenceCall*) incomingCallWithUid:(NSString*)uid
{
	NSPredicate *filter = [NSPredicate predicateWithFormat:@"uid == %@", uid];
	NSArray *results = [_incomingCalls filteredArrayUsingPredicate:filter];
	if ([results count])
		return [results firstObject];
	return nil;
}

- (void)updateIncomingCallsWithDictionary:(NSDictionary*)dictionary
{
	NSArray *incomingCalls = dictionary[@"incoming-calls"];
	NSArray *incomingCallsUids = [incomingCalls valueForKeyPath:@"uid"];
	NSMutableIndexSet *callsToDelete = [NSMutableIndexSet indexSet];
	[_incomingCalls enumerateObjectsUsingBlock:^(MZInfopresenceCall *call, NSUInteger idx, BOOL *stop) {
		if (![incomingCallsUids containsObject:call.uid])
			[callsToDelete addIndex:idx];
	}];
	if (callsToDelete.count > 0)
		[self removeIncomingCallsAtIndexes:callsToDelete];
	
	for (NSDictionary *callDictionary in incomingCalls) {
		MZInfopresenceCall *call = [self incomingCallWithUid:callDictionary[@"uid"]];
		if (call)
		{
			[call updateWithDictionary:callDictionary];
		}
		else
		{
			call = [[MZInfopresenceCall alloc] initWithDictionary:callDictionary];
			[self appendObjectToIncomingCalls:call];
		}
	}
}


#pragma mark - Outgoing Calls

KVO_MUTABLEARRAY_ACCESSOR_IMPLEMENTATION(OutgoingCalls, outgoingCalls, _outgoingCalls)

- (MZInfopresenceCall*) outgoingCallWithUid:(NSString*)uid
{
	NSPredicate *filter = [NSPredicate predicateWithFormat:@"uid == %@", uid];
	NSArray *results = [_outgoingCalls filteredArrayUsingPredicate:filter];
	if ([results count])
		return [results firstObject];
	return nil;
}

- (void)updateOutgoingCallsWithDictionary:(NSDictionary*)dictionary
{
	NSArray *outgoingCalls = dictionary[@"outgoing-calls"];
	NSArray *outgoingCallsUids = [outgoingCalls valueForKeyPath:@"uid"];
	NSMutableIndexSet *callsToDelete = [NSMutableIndexSet indexSet];
	[_outgoingCalls enumerateObjectsUsingBlock:^(MZInfopresenceCall *call, NSUInteger idx, BOOL *stop) {
		if (![outgoingCallsUids containsObject:call.uid])
			[callsToDelete addIndex:idx];
	}];
	if (callsToDelete.count > 0)
		[self removeOutgoingCallsAtIndexes:callsToDelete];
	
	for (NSDictionary *callDictionary in outgoingCalls) {
		MZInfopresenceCall *call = [self outgoingCallWithUid:callDictionary[@"uid"]];
		if (call)
		{
			[call updateWithDictionary:callDictionary];
		}
		else
		{
			call = [[MZInfopresenceCall alloc] initWithDictionary:callDictionary];
			[self appendObjectToOutgoingCalls:call];
		}
	}
}

- (NSArray *)pendingInvites
{
  NSPredicate *filter = [NSPredicate predicateWithFormat:@"type == 0"]; // 0 == kMZInfopresenceCallTypeInvite
  NSArray *results = [_outgoingCalls filteredArrayUsingPredicate:filter];
  if ([results count])
    return results;
  return nil;
}

- (NSInteger)pendingInvitesCount
{
  NSArray *pendingInvites = [self pendingInvites];
  return pendingInvites ? pendingInvites.count : 0;
}

#pragma mark - Rooms

KVO_MUTABLEARRAY_ACCESSOR_IMPLEMENTATION(Rooms, rooms, _rooms)


#pragma mark - RemoteParticipants

KVO_MUTABLEARRAY_ACCESSOR_IMPLEMENTATION(RemoteParticipants, remoteParticipants, _remoteParticipants)

- (MZRemoteParticipant *)remoteParticipantWithUid:(NSString *)uid
{
  NSPredicate *filter = [NSPredicate predicateWithFormat:@"self.uid == %@", uid];
  NSArray *participants = [_remoteParticipants filteredArrayUsingPredicate:filter];
  if ([participants count])
    return [participants firstObject];

return nil;
}

- (MZMezzanine *)mezzanineRoomWithUid:(NSString *)uid
{
  NSPredicate *filter = [NSPredicate predicateWithFormat:@"self.uid == %@", uid];
  NSArray *participants = [_rooms filteredArrayUsingPredicate:filter];
  if ([participants count])
    return [participants firstObject];

  return nil;
}

#pragma mark - VideoChat

- (void)updateWithVideoChatDictionary:(NSDictionary *)videoChatDictionary
{
  if (videoChatDictionary)
  {
    [self setValueIfNotNullAndChanged:videoChatDictionary[@"service"] forKey:@"videoChatService"];
    NSDictionary *pexipDetail = [videoChatDictionary objectForKey:@"pexip"];
    if (pexipDetail)
    {
      [self setValueIfNotNullAndChanged:pexipDetail[@"node"] forKey:@"pexipNode"];
      [self setValueIfNotNullAndChanged:pexipDetail[@"conference"] forKey:@"pexipConference"];
    }
  }
  else
  {
    _videoChatService = nil;
    _pexipNode = nil;
    _pexipConference = nil;
  }
}

@end
