//
//  MZAsset.h
//  Mezzanine
//
//  Created by Zai Chang on 2/15/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//
#import "MZItem.h"


/// Model object for an asset.
///
@interface MZAsset : MZItem
{
  NSString *name;

  // Volatile states
  BOOL isPendingDeletion;
}
@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign) BOOL imageAvailable;

@property (nonatomic, assign) BOOL isPendingDeletion;

+ (instancetype)assetWithUid:(NSString*)anUid;

-(instancetype) initWithDictionary:(NSDictionary*)dictionary;
-(void) updateWithDictionary:(NSDictionary*)dictionary;

@end

