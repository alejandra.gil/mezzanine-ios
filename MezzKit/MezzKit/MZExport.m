//
//  MZExport.m
//  Mezzanine
//
//  Created by miguel on 22/07/13.
//  Copyright (c) 2013 Oblong Industries. All rights reserved.
//

#import "MZExport.h"
#import "MZExport_Private.h"
#import "MZSecurityManager.h"
#import "MZExportInfo.h"

#define kExportInfoContext @"kExportInfoContext"


@implementation MZExport

- (instancetype)init
{
  self = [super init];
  if (self)
  {
    self.info = [[MZExportInfo alloc] init];
    self.info.source = MZExportSourceTypePortfolio;
    self.downloadFolder = [NSTemporaryDirectory() stringByAppendingPathComponent:@"Portfolio"];
    [self configureSessionManager];
  }
  
  return self;
}


- (void)dealloc
{
  self.info = nil;
}


- (void)setInfo:(MZExportInfo *)info
{
  if (_info != info)
  {
    if (_info)
    {
      [_info removeObserver:self forKeyPath:@"url"];
    }
    
    _info = info;
    
    if (_info)
    {
      [_info addObserver:self forKeyPath:@"url" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:kExportInfoContext];
    }
  }
}


- (void) setDownloadFolder:(NSString *)folder
{
  _downloadFolder = folder;
  
  NSFileManager *fileManager = [NSFileManager defaultManager];
  if (![fileManager fileExistsAtPath:_downloadFolder])
  {
    NSError *error = nil;
    [fileManager createDirectoryAtPath:_downloadFolder withIntermediateDirectories:YES attributes:nil error:&error];
  }
}


#pragma mark - Export Info observation

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
  if (context == kExportInfoContext)
  {
    if ([keyPath isEqualToString:@"url"])
      [self downloadPDF];
  }
  else
  {
    [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
  }
}


#pragma mark - States

- (void)reset
{
  [self cancelExport];
  [_info reset];
}


- (BOOL)isExporting
{
  return ((_info.state == MZExportStateRequested) || (_info.state == MZExportStateDownloading));
}

- (void)eraseDownloadFolder
{
  NSFileManager *fileManager = [NSFileManager defaultManager];
  NSError *error = nil;
  [fileManager removeItemAtPath:_downloadFolder error:&error];
}


#pragma mark - Private

- (void)configureSessionManager
{
  NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
  _downloadManager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
  
  _downloadManager.securityPolicy.allowInvalidCertificates = NO;
  
  [_downloadManager setSessionDidReceiveAuthenticationChallengeBlock:^NSURLSessionAuthChallengeDisposition(NSURLSession * _Nonnull session, NSURLAuthenticationChallenge * _Nonnull challenge, NSURLCredential *__autoreleasing  _Nullable * _Nullable credential) {
    
    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
    {
      MZSecurityManager *securityManager = [MZSecurityManager new];
      BOOL protected = [securityManager authenticateAgainstProtectionSpace:challenge.protectionSpace];
      
      if (protected)
      {
        *credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
        return NSURLSessionAuthChallengeUseCredential;
      }
      else
      {
        return NSURLSessionAuthChallengeCancelAuthenticationChallenge;
      }
    }
    
    return NSURLSessionAuthChallengePerformDefaultHandling;
  }];
  
  __weak MZExportInfo *__info = _info;

  [_downloadManager setDownloadTaskDidWriteDataBlock:^(NSURLSession * _Nonnull session, NSURLSessionDownloadTask * _Nonnull downloadTask, int64_t bytesWritten, int64_t totalBytesWritten, int64_t totalBytesExpectedToWrite) {
    __info.progress = (float)totalBytesWritten / (float)totalBytesExpectedToWrite;
    __info.filesizeInBytes = (NSUInteger) totalBytesExpectedToWrite;
  }];
}


- (void)downloadPDF
{
  __weak MZExportInfo *__info = _info;
  
  NSURLRequest *request = [NSURLRequest requestWithURL:_info.url];
  NSURLSessionDownloadTask *downloadTask = [_downloadManager downloadTaskWithRequest:request
                                                                    progress:nil
                                                                 destination:^ NSURL *(NSURL *targetPath, NSURLResponse *response) {
                                                                   NSString *path= [_downloadFolder stringByAppendingPathComponent:[response suggestedFilename]];
                                                                   __info.filepath = path;
                                                                   return [NSURL fileURLWithPath:path];
                                                                 } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
                                                                   if (!error)
                                                                   {
                                                                     __info.progress = 1.0;
                                                                     __info.state = MZExportStateReady;
                                                                   }
                                                                   else
                                                                   {
                                                                     __info.errorMessage = [error localizedDescription];
                                                                     __info.state = MZExportStateFailed;
                                                                   }
                                                                 }];
  
  _info.state = MZExportStateDownloading;
  
  [downloadTask resume];
}

- (void)cancelExport
{
  if ([[_downloadManager downloadTasks] count] > 0)
    [_downloadManager.operationQueue cancelAllOperations];
}

@end
