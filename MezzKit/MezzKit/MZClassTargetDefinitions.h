//
//  MZClassTargetDefinitions.h
//  MezzKit
//
//  Created by miguel on 25/01/13.
//  Copyright (c) 2013 Oblong Industries. All rights reserved.
//

#ifndef MezzKit_MZClassTargetDefinitions_h
#define MezzKit_MZClassTargetDefinitions_h

#if TARGET_OS_IPHONE

#import <UIKit/UIKit.h>

typedef UIColor MZColor;
typedef UIImage MZImage;

#else

#import <AppKit/AppKit.h>

typedef NSColor MZColor;
typedef NSImage MZImage;

#endif

#endif
