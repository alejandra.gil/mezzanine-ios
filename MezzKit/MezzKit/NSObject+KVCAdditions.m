//
//  NSObject+KVCAdditions.m
//  Mezzanine
//
//  Created by Zai Chang on 2/17/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import "NSObject+KVCAdditions.h"


@implementation NSObject (KVCAdditions)

-(void) setValueIfChanged:(id)value forKey:(NSString *)key
{
  id existingValue = [self valueForKey:key];
  if (![value isEqual:existingValue])
    [self setValue:value forKey:key];
}

-(void) setValueIfNotNullAndChanged:(id)value forKey:(NSString *)key
{
  if (value == nil || value == [NSNull null])
    return;
  [self setValueIfChanged:value forKey:key];
}

@end
