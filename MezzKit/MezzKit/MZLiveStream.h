//
//  MZLiveStream.h
//  Mezzanine
//
//  Created by Miguel Sánchez Valdés on 12/4/12.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MZItem.h"


@interface MZLiveStream : MZItem

@property (nonatomic, assign) BOOL active;

@property (nonatomic, strong) MZImage *thumbnail;
@property (nonatomic, readonly) BOOL local;
@property (nonatomic, strong) NSString* displayName;
@property (nonatomic, strong) NSString* remoteMezzName;


-(void) updateWithDictionary:(NSDictionary *)dictionary;

@end
