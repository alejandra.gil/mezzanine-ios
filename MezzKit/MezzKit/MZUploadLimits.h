//
//  MZUploadLimits.h
//  MezzKit
//
//  Created by Zai Chang on 3/5/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MZUploadLimits : NSObject

@property (nonatomic, retain) NSNumber *maxImageWidth;
@property (nonatomic, retain) NSNumber *maxImageHeight;
@property (nonatomic, retain) NSNumber *maxImageSizeInMB;
@property (nonatomic, retain) NSNumber *maxImageSizeInMP;
@property (nonatomic, retain) NSNumber *maxPDFSizeInMB;
@property (nonatomic, retain) NSNumber *maxGlobalSizeInMB;

-(void) updateWithDictionary:(NSDictionary*)dictionary;

@end
