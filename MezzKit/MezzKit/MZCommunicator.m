//
//  MZCommunicator.m
//  Mezzanine
//
//  Created by Zai Chang on 10/13/10.
//  Copyright 2010 Oblong Industries. All rights reserved.
//

#import "MZCommunicator.h"
#import "MZCommunicator+Constants.h"

#import "OBProtein+Mezzanine.h"
#import "OBWSSPoolConnector.h"

#import "MZSecurityManager.h"
#import "NSString+VersionChecking.h"
#import "NSString+Localization.h"
#import "NSURL+Additions.h"

#import "MZProtoFactory.h"
#import "MZCommunicatorRequestor.h"
#import "MZUploader.h"
#import "MZCommunicator+Handlers.h"
#import "MZHandlerFactory.h"

#import "NetworkHelpers.h"
#import "MZSessionManager.h"

#if TARGET_OS_IPHONE
#import <UIKit/UIKit.h>
#else
#import <SystemConfiguration/SystemConfiguration.h>
#endif

#define POSE_AS_BROWSER 0

//TODO: Move this
#define kWEBSOCKETURL   @"/plasma-web-proxy/sockjs/websocket"
#define kSESSIONTOKENWEBSOCKETURL   @"/session/v1/client-ws"

// 20 seconds should be plenty for mezzanine to respond to the join protein after a secure joining process
#define MezzanineJoinRequestTimeout 20.0

@interface MZCommunicator (Private)

-(void) joinSuccessful;
-(void) joinFailed:(NSError*)error;

-(void) setupJoinResponseHandler;
-(void) setupMessageHandlers;
-(void) setupDisableLockedSessionOnJoinHandler;

@end


@implementation MZCommunicator

@synthesize joinPassphrase;
@synthesize receivedInitialState;
@synthesize hasJoinedSession;

@synthesize proteinFactory;

@synthesize systemModel;
@synthesize sessionManager;


-(OBPoolConnector*) primaryPool
{
  return self.poolConnector;
}

- (instancetype)initWithServerUrl:(NSURL *)aServerUrl
{
  return [self initWithServerUrl:aServerUrl displayName:@"iPhone User"];
}

- (instancetype)initWithServerUrl:(NSURL *)aServerUrl displayName:(NSString * _Nonnull)displayName
{
  if ((self = [super init]))
  {
    self.delegate = self;

    self.displayName = displayName;
    
    self.provenance = [MZCommunicator getProvenanceIngest];
    self.uploader = [[MZUploader alloc] initWithCommunicator:self];
    self.sessionManager = [MZSessionManager new];
    self.sessionManager.provenance = self.provenance;
    self.sessionManager.displayName = self.displayName;

#if TARGET_OS_IPHONE
    
    UIDevice *device = [UIDevice currentDevice];
    deviceName = device.name;
    model = device.model;
    systemName = device.systemName;
    systemVersion = device.systemVersion;
    
#else
    
    CFStringRef aName = SCDynamicStoreCopyComputerName(NULL,NULL);
    deviceName = [NSString stringWithString:(__bridge NSString *)aName];
    CFRelease(aName);
    model = @"Mac";
    systemName = [[NSProcessInfo processInfo] operatingSystemName];
    systemVersion = [[NSProcessInfo processInfo] operatingSystemVersionString];
    
#endif
    
    self.serverUrl = aServerUrl;

    if ([aServerUrl isARemoteParticipationURL])
      [self setMeetingIdWithURL:aServerUrl];
    
    // Extract the passkey if the URL contains one
    if (self.serverUrl.fragment.length > 0)
    {
      joinPassphrase = [aServerUrl.fragment copy];
      self.serverUrl = [self.serverUrl URLByDeletingFragment];
    }
  }
  
  return self;
}

-(void) dealloc
{
  DLog(@"%@ dealloc", [self class]);

  [self clearPoolConfigs];

  self.delegate = nil;

  [self.sessionManager reset];
  self.sessionManager = nil;
  self.proteinFactory = nil;
}


#pragma mark - Connection Cycle

-(void) startConnection
{
  self.isConnecting = YES;
  __weak typeof(self) weakSelf = self;
  [self.sessionManager requestStartSessionWithServer:self.serverUrl
                                          completion:^(NSError *error, NSString *token) {
                                            typeof(self) strongSelf = weakSelf;
                                            if (error && error.code == kMZSessionManagerErrorCode_ValidCertificateWithIncorrectHost)
                                            {
                                              [strongSelf retrySSLHandshakeWithIP];
                                            }
                                            else
                                            {
                                              [strongSelf checkSessionStartResult:error token:token];
                                            }
                                          }];
}

// ATS has become more restrictive after iOS 11 (still ocurring in iOS 12)
// Our servers certficates chain are validated without problems but fail
// to pass the final challenge if user try to connect using the hostname or
// FQDN. It works fine using the IP.
// TODO: this is a temporary hack until we figure out what changes are required
// either in this client, the server or the certificate itself
- (void)retrySSLHandshakeWithIP
{
  NSString *ip = [NetworkHelpers lookupHostIPAddressForURL:self.serverUrl];
  self.serverUrl = ip ? [NSURL httpsSchemeWithHostname:ip] : self.serverUrl;

  __weak typeof(self) weakSelf = self;
  [self.sessionManager requestStartSessionWithServer:self.serverUrl
                                          completion:^(NSError *error, NSString *token) {
                                            typeof(self) strongSelf = weakSelf;
                                            [strongSelf checkSessionStartResult:error token:token];
                                          }];
}

- (void)checkSessionStartResult:(NSError *)error token:(NSString *)token
{
  if (error)
  {
    if ((error.code == NSURLErrorCancelled) || [self isAnSSLError:error])
    {
      // The server is a secure one but its certificate is invalid for some reason.
      // Switching to a non-secure one is not possible, so join must fail.
      NSString *errorString = MZLocalizedString(@"MZCommunicator Error Invalid Server Certificate", nil);
      NSError *error = [NSError errorWithDomain:kMZCommunicatorErrorDomain
                                           code:kMZCommunicatorErrorCode_InvalidServerCertificate
                                       userInfo:@{NSLocalizedDescriptionKey: errorString}];
      [self joinFailed:error];
    }
    // Unauthorized from a session token authorization response (wrong passkey)
    else if (error.code == kMZSessionManagerErrorCode_WrongPassword)
    {
      [[NSNotificationCenter defaultCenter] postNotificationName:MZCommunicatorIncorrectPassphraseNotification object:self userInfo:nil];
    }
    else
    {
      [self joinFailed:error];
    }
  }
  else if (token)
  {
    [self continueInitializingCommunicator:token];
  }
  else
  {
    [self continueInitializingCommunicator:nil];
  }
}


-(BOOL) isAnSSLError:(NSError *)error
{
  if (error.code == NSURLErrorSecureConnectionFailed ||
      error.code == NSURLErrorServerCertificateHasBadDate ||
      error.code == NSURLErrorServerCertificateUntrusted ||
      error.code == NSURLErrorServerCertificateHasUnknownRoot ||
      error.code == NSURLErrorServerCertificateNotYetValid ||
      error.code == NSURLErrorClientCertificateRejected ||
      error.code == NSURLErrorClientCertificateRequired ||
      error.code == NSURLErrorCannotLoadFromNetwork)
    return YES;
  else
    return NO;
}


#pragma mark - Communicator initializer

- (void)connect
{
  if ([OBPoolConnector hasInternetConnection])
    [super connect];
  else
    [self disconnect];
}

-(void)continueInitializingCommunicator:(NSString *)token
{
  self.heartbeatInterval = 5.0; // 5 seconds per heartbeat
  self.heartbeatTimeout = 15.0; // Wait at most 15 seconds for a heartbeat from native
  
  __weak MZCommunicator *__self = self;
  self.connectionTimedOut = ^{
    NSString *errorString = MZLocalizedString(@"MZCommunicator Error Connection Timeout", nil);
    NSError *error = [NSError errorWithDomain:kMZCommunicatorErrorDomain
                                         code:kMZCommunicatorErrorCode_ConnectionTimeout
                                     userInfo:@{NSLocalizedDescriptionKey: errorString}];
    [__self joinFailed:error];
  };

  [self createWSSPoolConfiguration:token];
  [self connect];
}


- (void)createWSSPoolConfiguration:(NSString *)token
{
  if (!poolConfigs)
    poolConfigs = [@[] mutableCopy];

  self.serverUrl = [self.serverUrl URLByReplacingSchemeWithWSS];
  self.communicatorProtocol = OBCommunicatorProtocolWSS;

  OBPoolConnectorServerTrustSecurityPolicy *commSecPolicy = nil;
  if (token) {
    commSecPolicy = [OBPoolConnectorServerTrustSessionTokenSecurityPolicy poolSecurityPolicyWithServerTrustEvaluator:^BOOL(SecTrustRef serverTrust, NSString * domain) {
      MZSecurityManager *securityManager = [MZSecurityManager new];
      return [securityManager authenticateAgainstServerTrust:serverTrust host:domain];
    } sessionToken:token];
  } else {
    commSecPolicy = [OBPoolConnectorServerTrustSecurityPolicy poolSecurityPolicyWithServerTrustEvaluator:^BOOL(SecTrustRef serverTrust, NSString *domain) {
      MZSecurityManager *securityManager = [MZSecurityManager new];
      return [securityManager authenticateAgainstServerTrust:serverTrust host:domain];
    }];
  }

  __weak MZCommunicator *__self = self;
  id websocketConnector = [OBCommunicatorPoolConfig poolConfigWithPoolName:MEZZ_WEBSOCKET
                                                           additionalPools:@[MEZZ_RESPONSE_POOL]
                                                                      mode:OBPoolConnectorModeReadOnly
                                                               requirement:OBCommunicatorConnectorRequired
                                                            securityPolicy:commSecPolicy
                                                                  endPoint:token ? kSESSIONTOKENWEBSOCKETURL : kWEBSOCKETURL
                                                              wasConnected:^{
                                                                [__self checkWSSConnectionComplete];
                                                              }
                                                           failedToConnect:^(NSError *error) {
                                                              [__self joinWSSFailed:error];
                                                           }];
  
  [poolConfigs addObject:websocketConnector];
}


- (void)checkConnectionComplete
{
  [self setupJoinResponseHandler];
  [self requestJoinWithPassphrase:self.joinPassphrase];
  self.joinPassphrase = nil; // Clear the passphrase
}


- (void)checkWSSConnectionComplete
{
  OBWSSPoolConnector *wssPoolConnector = (OBWSSPoolConnector *)self.poolConnector;
  wssPoolConnector.delegate = self;
}


- (void)joinWSSFailed:(NSError *)error
{
  awaitingJoinResponse = NO;
  
  // Clean up the previous pool configuration
  [poolConfigs removeAllObjects];
  [self removeAllMessageHandlers];
  
  // End listening to connection timeouts
  if (connectionTimeoutTimer)
  {
    [connectionTimeoutTimer invalidate];
    connectionTimeoutTimer = nil;
  }

  self.isConnecting = NO;
  self.isConnected = NO;

  [self stopHeartbeat];
  [self stopHeartbeatTimeoutTimer];

  NSError *wssConnectionFailedError;
  // TMP until we have a better way to distinguish RP attempts
  // Error Domain=SRWebSocketErrorDomain Code=2132 "received bad response code from server 404" == invalid RP session id
  if ([self.serverUrl isARemoteParticipationURL] && error.code == 2132)
  {
    wssConnectionFailedError = [NSError errorWithDomain:error.domain
                                         code:error.code
                                     userInfo:@{@"NSLocalizedDescription" : NSLocalizedString(@"Invalid RP Conference ID", @"")}];
  }
  else
  {
    NSString *errorString = MZLocalizedString(@"MZCommunicator Error Mezzanine Not Responding", nil);
    wssConnectionFailedError = [NSError errorWithDomain:kMZCommunicatorErrorDomain
                                         code:kMZCommunicatorErrorCode_LoginTimeout
                                     userInfo:@{NSLocalizedDescriptionKey: errorString}];
  }
  [self joinFailed:wssConnectionFailedError];
}


#pragma mark - End Session

- (void)endSession
{
  __weak typeof(self) weakSelf = self;
  switch (self.sessionManager.sessionType)
  {
    case MZSessionTypeZiggy:
      // If request happens less than 5 minutes after client connects, simply clean up the session
      // and keep the client connected. All other clients will be disconnected.
      // Otherwise disconnect and reconnect and should show the passkey view again.
      if ([self clientCanRejoin])
      {
        [self.sessionManager requestCleanUpSessionWithCompletion:^(NSError * _Nullable error, NSString * _Nullable token) {
          if (error || !token)
            [weakSelf.sessionManager requestEndSession];
          else
            [weakSelf rejoinZiggySession:token];
        }];
      }
      else
      {
        [self.sessionManager requestEndSession];
      }
      break;
    case MZSessionTypePlasmaWebProxy:
      // In older systems ending a session for everyone means cleaning up the workspace
      // besides terminating any m2m or mezz-in session, which always cleans up the workspace.
      if (systemModel.infopresence.status == MZInfopresenceStatusInactive && systemModel.currentWorkspace.hasContent)
        [self.requestor requestWorkspaceDiscardRequest:systemModel.currentWorkspace.uid];
      else
        [self.requestor requestMeetingEndRequest:@""];
      break;
    case MZSessionTypeUnknown:
      DLog(@"Ending an unknown type session");
    default:
      break;
  }
}

-(BOOL)clientCanRejoin
{
  // A client that tries to rejoin can only do it it hasn't been connected for more than 5 minutes
  NSTimeInterval timeSinceSessionStarted = [[NSDate date] timeIntervalSinceDate:[self.sessionManager sessionStartTime]];
  BOOL isSessionLessThan5Minutes = timeSinceSessionStarted < 5*60;
  return isSessionLessThan5Minutes;
}

-(void)rejoinZiggySession:(NSString *)token
{
  DLog(@"Recreate websocket pool connection using a new Ziggy session");

  if (!self.hasJoinedSession)
    return;

  self.hasJoinedSession = NO;

  // Clean up the previous pool configuration, protein handlers and timers
  OBWSSPoolConnector *wssPC = (OBWSSPoolConnector *) self.poolConnector;
  wssPC.delegate = nil;
  [self disconnectAllPools];
  [self clearPoolConfigs];
  [self removeAllMessageHandlers];
  [self stopHeartbeat];
  [self stopHeartbeatTimeoutTimer];

  // Start the reconnection again using the new valid token.
  [self continueInitializingCommunicator:token];
}


#pragma mark - Protein Specs

-(BOOL) isProteinVersionAboveMinimum:(NSString*)version
{
  BOOL versionAboveMinimum = [version compare:MZCOMMUNICATOR_PROTEIN_VERSION_MIN options:NSNumericSearch] != NSOrderedAscending;
  return versionAboveMinimum;
}

-(BOOL) isProteinVersionBelowMaximum:(NSString*)version
{
  BOOL versionBelowMaximum = [version compare:MZCOMMUNICATOR_PROTEIN_VERSION_MAX options:NSNumericSearch] != NSOrderedDescending;
  return versionBelowMaximum;
}



#pragma mark - Provenance Handling

+(NSString*) deviceUUID
{
  static NSString *_deviceUUID = nil;
  if (_deviceUUID == nil)
  {
    CFUUIDRef theUUID = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
    CFRelease(theUUID);
    _deviceUUID = (__bridge_transfer NSString *)string;
  }
  return _deviceUUID;
}


+(NSString*) getProvenanceIngest
{
  NSString *model;
  NSString *deviceIdentifier;
  
#if TARGET_OS_IPHONE
  
  model = [[UIDevice currentDevice] model];
  deviceIdentifier = [self deviceUUID];
  
#else
  
  // Native side is still not accepting Mac as a valid provenance so we fake it
  model = @"iPhone";//@"Mac";
  deviceIdentifier = @"ada25992437c03daa77c3eb6d36a7c79c9e15ba7";//@"UNIQUE-MAC-TEST-TO-BE-CHANGED";
  
#endif
  
  model = [model componentsSeparatedByString:@" "][0];
  return [NSString stringWithFormat:@"%@-%@", model, deviceIdentifier];
}


- (void)setProvenance:(NSString *)aProvenance
{
  super.provenance = aProvenance;
  [self createProvenanceFilter];
}


-(void) createProvenanceFilter
{
  __weak MZCommunicator *__self = self;
  OBProteinFilterBlock filter = ^(OBProtein *protein)
  {
    NSString *aProvenace = [protein extractProvenance]; // Find provenance string
    BOOL provenanceMatch = [aProvenace isEqual:__self.provenance];
    return provenanceMatch;
  };
  self.provenanceFilter = filter;
}


#pragma mark - Response Handling

-(BOOL) proteinIsPSAOrPM:(OBProtein*)protein
{
  return ([protein containsDescrip:@"psa"] || [protein containsDescrip:@"pm"]);
}


-(BOOL) proteinIsPSAOrResponseForMe:(OBProtein*)protein
{
  return [protein containsDescrip:@"psa"] || ([protein containsDescrip:@"response"] && self.provenanceFilter(protein));
}



#pragma mark - Various Pools

-(OBPoolConnector*) poolConnector { return [self connectorForPool:MEZZ_WEBSOCKET]; }

-(BOOL) checkConnection
{
  return [self.poolConnector checkConnection];
}


- (void)checkMezzMetadataAndContinueConnection
{
  __weak typeof(self) __self = self;
  OBMessageHandler *mezzMetadataHandler = [OBMessageHandler messageHandler];
  mezzMetadataHandler.descrips = @[MZTransactionNameMezzMetadata];
  mezzMetadataHandler.filter = self.provenanceFilter;
  mezzMetadataHandler.handler = ^(OBProtein *p) {
    NSDictionary* featureTogglesDict = [p ingestForKey:@"feature-toggles"];
    __self.systemModel.featureToggles.remoteAccessEnabled = [[featureTogglesDict objectForKey:@"remote-access"] boolValue];
    __self.systemModel.featureToggles.infopresenceInvitesEnabled = [[featureTogglesDict objectForKey:@"m2m-invites"] boolValue];
    __self.systemModel.featureToggles.disableDownloadsEnabled = [[featureTogglesDict objectForKey:@"disable-downloads"] boolValue];
    __self.systemModel.featureToggles.largeImagesEnabled = [[featureTogglesDict objectForKey:@"large-images"] boolValue];
    __self.systemModel.featureToggles.immenseImagesEnabled = [[featureTogglesDict objectForKey:@"immense-images"] boolValue];
    __self.systemModel.featureToggles.accessWarning = [[featureTogglesDict objectForKey:@"access-warning"] boolValue];
    __self.systemModel.featureToggles.roomKey = [[featureTogglesDict objectForKey:@"room-key"] boolValue];
    __self.systemModel.featureToggles.participantRoster = [[featureTogglesDict objectForKey:@"participant-roster"] boolValue];

    __self.systemModel.apiVersion = [p ingestForKey:@"api-version"];
    // TODO: FIX ME with incoming MZProto generated code ([NSNull null])
    __self.systemUseNotificationText = [p ingestForKey:@"system-use-notification-text"] == [NSNull null] ? nil : [p ingestForKey:@"system-use-notification-text"];
    __self.systemUseNotification = [[p ingestForKey:@"system-use-notification"] boolValue];
    self->isConnectedToCloudInstance = [[p ingestForKey:@"cloud-instance"] boolValue];
    __self.systemModel.myMezzanine.model = [p ingestForKey:@"model"] == [NSNull null] ? nil : [p ingestForKey:@"model"];

    __self.systemModel.systemType = [[p ingestForKey:@"ocelot-system"] boolValue] ? MZSystemTypeOcelot : MZSystemTypeMezzanine;

    if (__self.isARemoteParticipationConnection)
    {
      [__self postNotification:MZCommunicatorRequestRemoteParticipationSettingsNotification];
    }
    else
      [__self checkConnectionComplete];
  };
  
  [self addMessageHandler:mezzMetadataHandler forPool:self.poolConnector.poolName];

  proteinFactory = [[MZProteinFactory alloc] init];
#if POSE_AS_BROWSER
  proteinFactory.provenance = [NSString stringWithFormat:@"browser-%@", [[self class] deviceUUID]];
#else
  proteinFactory.provenance = self.provenance;
#endif

  MZTransaction *transaction = [self beginTransactionWithName:@"mezz-metadata" proteinBlock:^{
    return [__self.proteinFactory mezzMetadataRequest];
  }];

  transaction.failedBlock = ^(NSError *error){
    // This is a Mezzanine not using Mezzanine Metadata yet
    [__self checkConnectionComplete];
  };
}

-(BOOL)isARemoteParticipationConnection {
  return isConnectedToCloudInstance;
}

-(void) setupJoinResponseHandler
{
  __weak typeof(self) weakSelf = self;

  // A version-independent join handler that will then feed the handlers of the appropriate version
  // of Mezzanine
  OBMessageHandler *joinResponseHandler = [OBMessageHandler messageHandler];
  joinResponseHandler.descrips = @[MZTransactionNameClientJoin];
  joinResponseHandler.filter = self.provenanceFilter;
  joinResponseHandler.handler = ^(OBProtein *p) {
    if (!self.hasJoinedSession)
    {
      // In case apiVersion is nil (below 3.6) we use mezzanine-version value
      weakSelf.systemModel.apiVersion = !weakSelf.systemModel.apiVersion ? [p.ingests valueForKey:@"mezzanine-version"] : weakSelf.systemModel.apiVersion;
      
      BOOL versionAboveMinimum = [self isProteinVersionAboveMinimum:self.systemModel.apiVersion];
      BOOL versionBelowMaximum = [self isProteinVersionBelowMaximum:self.systemModel.apiVersion];
      if (self.systemModel.apiVersion && versionAboveMinimum && versionBelowMaximum)
      {
        MZHandlerFactory *handlerFactory = [[MZHandlerFactory alloc] init];
        handlerFactory.communicator = weakSelf;
        handlerFactory.systemModel = weakSelf.systemModel;
        OBMessageHandler *joinHandler = [handlerFactory createClientJoinHandler];

        if (joinHandler)
          joinHandler.handler(p);
      }
      else
      {
        NSString *title = @"";
        NSString *errorString = MZLocalizedString(@"MZCommunicator Error Mezzanine Undetermined Version", nil);
        if (!versionAboveMinimum)
        {
          // Native system is too old for this app to connect to
          title = MZLocalizedString(@"MZCommunicator Error Mezzanine Incompatible Version Title", nil);
          errorString = MZLocalizedString(@"MZCommunicator Error Mezzanine Below Supported Version", nil);
        }
        else if (!versionBelowMaximum)
        {
          // Native system is too recent and has breaking changes for this app to connect to
          title = MZLocalizedString(@"MZCommunicator Error Mezzanine Incompatible Version Title", nil);
          errorString = MZLocalizedString(@"MZCommunicator Error Mezzanine Above Supported Version", nil);
        }
        NSError *error = [NSError errorWithDomain:kMZCommunicatorErrorDomain 
                                             code:kMZCommunicatorErrorCode_IncompatibleProteinVersion 
                                         userInfo:@{@"title": title,
                                                   NSLocalizedDescriptionKey: errorString}];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
          [weakSelf joinFailed:error];
        }];
      }
    }
    
    // Cancel join request timeout
    [NSObject cancelPreviousPerformRequestsWithTarget:weakSelf selector:@selector(checkOnJoinResponse) object:nil];
  };
  [self addMessageHandler:joinResponseHandler forPool:self.poolConnector.poolName];
}

- (void)requestJoinAfterSystemUseNotification
{
  if ([self isARemoteParticipationConnection])
  {
    [self postNotification:MZCommunicatorRequestRemoteParticipationSettingsNotification];
  }
  else
  {
    [self setupJoinResponseHandler];
    [self requestJoinWithPassphrase:nil];
  }
}


// A synchronous login method designed to utilize OBConnectionViewController's
-(void) requestJoinWithPassphrase:(NSString *)passphrase
{
  DLog(@"MZCommunicator attempt join with passphrase");
  
  awaitingJoinResponse = YES;
  
  OBPoolConnector *connector = self.poolConnector;
  
  // This should be the case when a client requests another join after being locked out by the passphrase (see bug 11387)
  if (passphrase)
    [connector sendProtein:[proteinFactory clientJoinRequestWithNameAndPasskey:self.displayName passkey:passphrase]];
  else
    [connector sendProtein:[proteinFactory clientJoinRequestWithName:self.displayName]];
  
  
  // Add timeout
  [self performSelector:@selector(checkOnJoinResponse) withObject:nil afterDelay:MezzanineJoinRequestTimeout];
  
  [self postNotification:MZCommunicatorBeganJoinNotification];
}


- (void)requestJoinWithRemoteParticipantName:(NSString *)name passphrase:(NSString *)passphrase
{
  DLog(@"MZCommunicator attempt join with RP name");

  [self setupJoinResponseHandler];


  awaitingJoinResponse = YES;

  OBPoolConnector *connector = self.poolConnector;

  if (passphrase)
    [connector sendProtein:[proteinFactory clientJoinRequestWithNameAndPasskey:name passkey:passphrase]];
  else
    [connector sendProtein:[proteinFactory clientJoinRequestWithName:name]];

  // Add timeout
  [self performSelector:@selector(checkOnJoinResponse) withObject:nil afterDelay:MezzanineJoinRequestTimeout];

  [self postNotification:MZCommunicatorBeganJoinNotification];

  self.joinPassphrase = nil; // Clear the passphrase
}

-(void) cancelPassphraseJoin
{
  // This call comes from the user cancelling the passkey view modal view
  // There's no need to show any alert.

  DLog(@"MZCommunicator cancel join with passphrase");
  [self joinFailed:nil];
}


-(void) checkOnJoinResponse
{
  DLog(@"MZCommunicator checkOnJoinResponse");
    
  if (awaitingJoinResponse)
  {
    NSString *errorString = MZLocalizedString(@"MZCommunicator Error Mezzanine Not Responding", nil);
    NSError *error = [NSError errorWithDomain:kMZCommunicatorErrorDomain 
                                         code:kMZCommunicatorErrorCode_LoginTimeout 
                                     userInfo:@{NSLocalizedDescriptionKey: errorString}];
    [self joinFailed:error];
  }
}


-(void) joinFailed:(NSError*)error
{
  DLog(@"MZCommunicator joinFailed %@", error);
  
  awaitingJoinResponse = NO;
  
  [self removeAllMessageHandlers];

  [self.sessionManager reset];
  
  // N.B. In this case we call the OBCommunicator's disconnect method because if we haven't joined
  // we're not 'connected' from the Mezzanine's perspective
  [super disconnect];

  NSDictionary *userInfo = error ? @{@"error": error} : nil;
  [[NSNotificationCenter defaultCenter] postNotificationName:MZCommunicatorJoinFailedNotification object:self userInfo:userInfo];
}

-(void) joinSuccessful
{
  DLog(@"MZCommunicator joinSuccessful");
  if (!self.isConnected)
    self.isConnected = YES;

  awaitingJoinResponse = NO;
  self.hasJoinedSession = YES;
  
  _requestor = [[MZCommunicatorRequestor alloc] initWithCommunicator:self];
  [self postNotification:MZCommunicatorJoinSuccesNotification];
  
  // First set up message handlers
  [self setupMessageHandlers];

  // Dismiss passphrase screen if its currently shown
  if (systemModel.passphraseRequested)
  {
    systemModel.passphraseRequested = NO;
  }
  [self.requestor requestPasskeyDetailRequest];
  
  [self postNotification:MZCommunicatorBeganRequestingStateNotification];
  
  // Note, need to request profile details before windshield items,
  // because checking of local mezzanineUid is needed
  [self.requestor requestRemoteMezzaninesDetailRequest];
  [self.requestor requestProfileDetailRequest];
  [self.requestor requestInfopresenceDetailRequest];
  [self.requestor requestParticipantListRequest];
  
  [self startHeartbeat];
#if ENABLE_HEARTBEAT_TIMEOUT
  [self scheduleOrResetHeartbeatTimeout];
#endif
  
  // Associate the model's pending transaction list with this communicator's
  systemModel.pendingTransactions = self.pendingTransactions;
}

// Regular disconnect call, inherited from OBCommunicator
-(void) disconnect
{
  [self disconnectWithMessage:nil];
}

// Provides extra information of the disconnection
-(void) disconnectWithMessage:(NSString *)message
{
  [self.requestor requestClientLeaveRequest];
  [self.uploader cancelAllPendingFileUploads];

  [self removeAllMessageHandlers];

  self.receivedInitialState = NO;
  self.hasJoinedSession = NO;

  [super disconnect];

  [self postDidDisconnectNotification:message];
}

-(void) postDidDisconnectNotification:(NSString *)message
{
  NSDictionary *userInfo = message ? @{@"message" : message} : nil;
  NSNotification *notification = [[NSNotification alloc] initWithName:MZCommunicatorDidDisconnectNotification object:self userInfo:userInfo];
  [[NSNotificationCenter defaultCenter] postNotification:notification ];
}


-(NSString*) inviteURLString
{
  if (systemModel.passphrase)
    return [NSString stringWithFormat:@"mezzanine://%@/#%@", self.serverUrl.host, systemModel.passphrase];
  else
    return [NSString stringWithFormat:@"mezzanine://%@", self.serverUrl.host];
}


#pragma mark - Message Handlers

-(void) setupMessageHandlers
{
  DLog(@"MZCommunicator setupMessageHandlers");
  
  if (disableLockedSessionOnJoinHandler)
  {
    [self removeMessageHandler:disableLockedSessionOnJoinHandler];
    disableLockedSessionOnJoinHandler = nil;
  }
  
  [self addMessageHandlers];
}

-(void) setupDisableLockedSessionOnJoinHandler
{
  NSString *responsePoolName = self.poolConnector.poolName;
  __weak MZCommunicator *__self = self;
  __weak MZSystemModel *__model = systemModel;
  
  disableLockedSessionOnJoinHandler = [OBMessageHandler messageHandler];
  disableLockedSessionOnJoinHandler.descrips = @[@"psa",
                                                MZTransactionNamePasskeyDisable];
  disableLockedSessionOnJoinHandler.handler = ^(OBProtein *p)
  {
    [__model setPassphraseRequested:NO];
    [__self requestJoinWithPassphrase:nil];
  };
  
  [self addMessageHandler:disableLockedSessionOnJoinHandler forPool:responsePoolName];
}

-(void) heartbeatTimeoutReached
{
  DLog(@"MZCommunicator heartbeatTimeoutReached");
  
  //if ([self isConnected])
  {
    // Post this notification first because after [self disconnect]
    // observers may have been removed
    [self postNotification:OBCommunicatorWasDisconnectedNotification];
    
    [self disconnectWithMessage:MZCommunicatorDidDisconnectHeartbeatTimeout];
  }
}


#pragma mark - Transactions

-(NSString*) transactionIdFromProtein:(OBProtein*)protein
{
  return [protein transactionId];
}

-(OBProteinFilterBlock) createProteinFilterForTransactionId:(NSInteger)transactionId
{
  // Making a copy so that the block doesn't retain self
  NSString *prov = [self.provenance copy];
  NSString *transactionIdString = [NSString stringWithFormat:@"%ld", (long)transactionId];
  
  OBProteinFilterBlock filter = ^(OBProtein *protein)
  {
    // Find transaction id
    NSInteger index = [protein.descrips indexOfObject:@"to:"];
    if ([protein.descrips count] > index+1)
    {
      NSArray *to = (protein.descrips)[index+1];
      if ([to isKindOfClass:[NSArray class]] && [to count] > 1)
      {
        // Match both provenance and transaction id
        NSString *toProvenance = to[0];
        NSString *tid = [to[0] stringValue];
        if ([toProvenance isEqual:prov] && [tid isEqual:transactionIdString])
          return YES;
      }
    }
    return NO;
  };
  return [filter copy];
}


#pragma mark - HTTP Requests

-(NSURL*) baseURLForHttpRequests
{
  return [self.serverUrl URLByReplacingSchemeWithHTTPS];
}


#pragma mark - Pool Events

-(BOOL) checkResponseProvenance:(OBProtein*)p
{
  return [p provenanceIsEqual:self.provenance];
}



#pragma mark - Heartbeat Protein

-(OBProtein*) heartbeatProtein
{
  BOOL joined = self.isConnected && !systemModel.passphraseRequested;
  if (!proteinFactory.provenance)
    proteinFactory.provenance = self.provenance;
  
  return [proteinFactory heartbeatRequest:joined];
}


#pragma mark - Transaction Management

-(MZTransaction *) beginTransactionWithName:(NSString*)transactionName proteinBlock:(OBProtein *(^)(void))proteinBlock
{
  DAssert((proteinBlock != NULL), @"Protein block cannot be null");
  OBProtein *protein = proteinBlock();
  MZTransaction *transaction = [MZTransaction transactionWithProtein:protein];
  transaction.transactionName = transactionName;
  [self beginTransaction:transaction];
  return transaction;
}


-(void) cancelTransaction:(MZTransaction *)transaction
{
  OBPoolConnector *connector = [self connectorForPool:transaction.requestPoolName];
  if (connector)
    [connector removeProteinFromQueue:transaction.requestProtein];
  
  // TODO - Add a separate canceled call to OBTransaction in case caller needs to distinguish
  // between failed and canceled
  [transaction ended];
  
  NSInteger index = [systemModel.pendingTransactions indexOfObject:transaction];
  [systemModel removeObjectFromPendingTransactionsAtIndex:index];
}


#pragma mark - Image Resources

-(NSURL *)urlForResource:(NSString *)resource
{
  if (!resource) {
    return nil;
  }
  if ([resource hasPrefix:@"http://"])
    return [NSURL URLWithString:resource];

  if ([resource hasPrefix:@"https://"])
    return [NSURL URLWithString:resource];

  if ([resource hasPrefix:@"file://"])
    return [NSURL URLWithString:resource];

  NSURL *baseUrl = [self baseURLForHttpRequests];

  NSString *finalString = @"";
  if ([[resource substringToIndex:1] isEqualToString:@"/"])
  {
    finalString = [NSString stringWithFormat:@"%@%@", baseUrl.absoluteString, resource];
  }
  else
  {
    finalString = [NSString stringWithFormat:@"%@/%@", baseUrl.absoluteString, resource];
  }

  finalString = [finalString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];

  return [NSURL URLWithString:finalString];
}


#pragma mark - PDF download

- (NSURL *)RPFileDownload:(NSString*)resource;
{
  NSURL *baseUrl = [self baseURLForHttpRequests];
  return [baseUrl URLByAppendingPathComponent:[NSString stringWithFormat:@"mezzanine-download/download/%@", resource]];
}

#pragma mark - Uploading process

- (NSInteger)numberOfImagesBeingUploaded
{
  return [self.uploader numberOfImagesBeingUploaded];
}


- (NSInteger)numberOfFilesBeingUploaded
{
  return [self.uploader numberOfFilesBeingUploaded];
}


- (NSURL *)httpImageUploadURL
{
  return [[self baseURLForHttpRequests] URLByAppendingPathComponent:@"mezzanine-upload/assets"];
}


#pragma mark - Remote Participation

- (void)setMeetingIdWithURL:(NSURL *)url
{
  NSArray *pahtComponents = [url pathComponents];
  if ([pahtComponents containsObject:@"m"])
  {
    NSInteger ix = [pahtComponents indexOfObject:@"m"];
    _meetingID = pahtComponents[ix+1];
  }
  else if ([pahtComponents containsObject:@"a"])
  {
    NSInteger ix = [pahtComponents indexOfObject:@"a"];
    _meetingID = pahtComponents[ix+1];
  }
}


#pragma mark - OBWSSPoolConnectorDelegate

- (void)wssPoolConnectorDidClose:(OBWSSPoolConnector *)connector code:(OBWSSPoolConnectorConnectionEnd)code
{
  if (self.isConnected && code == OBWSSPoolConnectorConnectionEndClosedRemotely)
  {
    switch (self.sessionManager.sessionType) {
      case MZSessionTypeZiggy:
        // TODO - Besides disconnecting we need to request showing Ziggy passkey again.
        if (!self.hasJoinedSession)
        {
          [self disconnectWithMessage:MZCommunicatorDidDisconnectZiggySessionEnded];
        }
        else
        {
          DLog(@"A webSocket closed on a ziggy connected system that is rejoining.");
        }
        break;
      case MZSessionTypePlasmaWebProxy:
        // Bug 16928. When RP is turned off while we're still connected
        [self disconnectWithMessage:MZCommunicatorDidDisconnectPlasmaSessionEnded];
        break;
      case MZSessionTypeUnknown:
        DLog(@"A webSocket pool was closed but it didn't belong to an active session.");
      default:
        break;
    }
  }
  else if (self.hasJoinedSession)
  {
    DLog(@"A webSocket pool was closed while rejoining.");
  }
  else
  {
    DLog(@"A webSocket pool was closed but it didn't belong to an active session.");
  }
}

- (void)wssPoolConnectorFailure:(OBWSSPoolConnector *)connector error:(NSError *)error
{
  if (self.isConnected)
    [self disconnect];
  else
    [self joinWSSFailed:error];
}


- (void)wssPoolConnectorSuccess:(OBWSSPoolConnector *)connector
{
  if (self.isConnected)
    [self checkMezzMetadataAndContinueConnection];
  else
    // In case pools are not removed
    [super disconnect];
}

- (void)wssPoolConnectorSessionTokenSuccess:(OBWSSPoolConnector *)connector sessionDetail:(OBProtein *)sessionDetail
{
  [[NSNotificationCenter defaultCenter] postNotificationName:MZSessionManagerHidePasskeyViewNotification object:self userInfo:nil];

  if ([sessionDetail ingestForKey:@"session"]) {
    self.joinPassphrase = [sessionDetail ingestForKey:@"session"][@"passkey"];
  }

  [self checkMezzMetadataAndContinueConnection];
}

@end
