//
//  MZAssetCache.h
//  Mezzanine
//
//  Created by Zai Chang on 5/2/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import "MZAsset.h"
#import "MZItem.h"
#import <TTURLCache.h>


@interface MZAssetCache : TTURLCache

+(MZAssetCache*) sharedCache;

-(void) storeThumbnailImage:(MZImage*)image forAsset:(MZAsset*)asset;
-(MZImage*) thumbnailImageForAsset:(MZAsset*)asset;
-(void) removeThumbnailImageForAsset:(MZAsset *)asset;


-(void) storeThumbnailImage:(MZImage*)image forSlide:(MZItem*)slide;
-(MZImage*) thumbnailImageForSlide:(MZItem*)slide;
-(void) removeThumbnailImageForSlide:(MZItem *)slide;


-(void) storeFullImage:(MZImage*)image forSlide:(MZItem*)slide;
-(MZImage*) fullImageForSlide:(MZItem*)slide;
-(NSURL*) urlForFullSlideImage:(MZItem*)slide;
-(void) removeFullImageForSlide:(MZItem *)slide;


-(void) storeOriginalImage:(MZImage*)image forSlide:(MZItem*)slide;
-(MZImage*) originalImageForSlide:(MZItem*)slide;
-(NSURL*) urlForOriginalSlideImage:(MZItem*)slide;
-(void) removeOriginalImageForSlide:(MZItem *)slide;


-(void) cleanupV1Assets;

@end
