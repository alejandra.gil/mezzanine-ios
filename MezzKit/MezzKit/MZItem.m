//
//  MZItem.m
//  MezzKit
//
//  Created by miguel on 29/05/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import "MZItem.h"

@implementation MZItem

@synthesize index;
@synthesize uid;
@synthesize thumbURL;
@synthesize largeURL;
@synthesize fullURL;
@dynamic convertedOriginalURL;
@synthesize contentSource;
@synthesize imageAvailable;
@synthesize viddleName;
@synthesize image;
@synthesize displayInfo;

@synthesize annotations;

@synthesize pendingReorder;


-(id) init
{
  self = [super init];
  if (self)
  {
    annotations = [[NSMutableArray alloc] init];

    self.undoManager = [[NSUndoManager alloc] init];
  }
  return self;
}


-(NSString*) description
{
  return [NSString stringWithFormat:@"MZItem %p: uid=%@, index=%lu", self, uid, (unsigned long)index];
}


#pragma mark - Annotations

KVO_MUTABLEARRAY_ACCESSOR_IMPLEMENTATION(Annotations, annotations, annotations)

-(void) addAnnotation:(MZAnnotation*)annotation
{
  [self.undoManager registerUndoWithTarget:self selector:@selector(removeAnnotation:) object:annotation];
  [self insertObject:annotation inAnnotationsAtIndex:annotations.count];
}


-(void) addAnnotationsArray:(NSArray *)objects
{
  [self.undoManager registerUndoWithTarget:self selector:@selector(removeAnnotationsArray:) object:objects];
  NSIndexSet *indexSet = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(annotations.count, objects.count)];
  [self insertAnnotations:objects atIndexes:indexSet];
}


-(void) removeAnnotation:(MZAnnotation*)annotation
{
  NSInteger i = [annotations indexOfObject:annotation];
  if (i == NSNotFound)
    return;

  [self.undoManager registerUndoWithTarget:self selector:@selector(addAnnotation:) object:annotation];
  [self removeObjectFromAnnotationsAtIndex:i];
}


-(void) removeAnnotationsArray:(NSArray *)objects
{
  NSArray *copy = [annotations copy];
  [self.undoManager registerUndoWithTarget:self selector:@selector(addAnnotationsArray:) object:copy];
  NSMutableIndexSet *indexSet = [NSMutableIndexSet indexSet];
  for (MZAnnotation *annotation in objects)
  {
    NSInteger i = [annotations indexOfObject:annotation];
    if (i != NSNotFound)
      [indexSet addIndex:i];
  }
  [self removeAnnotationsAtIndexes:indexSet];
}


-(void) clearAnnotations
{
  if (annotations.count == 0)
    return;

  /* Old implementation
   NSArray *copy = [annotations copy];
   [self.undoManager registerUndoWithTarget:self selector:@selector(addAnnotations:) object:copy];
   NSIndexSet *indexSet = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, annotations.count)];
   [self removeAnnotationsAtIndexes:indexSet];
   */
  [self removeAnnotationsArray:annotations];
}



#pragma mark - Content

-(void) setContentSource:(NSString *)source
{
  if (![source isEqual:contentSource])
  {
    contentSource = [source copy];

    if ([contentSource hasPrefix:@"vi-"])
    {
      // This is a video slide
      NSString *theViddleName = [contentSource copy];

      // TODO remove viddleName and only use contentSource
      self.viddleName = theViddleName;
      _isVideo = YES;
    }
    else
    {
      self.viddleName = nil;
      _isVideo = NO;
    }
  }
}


#pragma mark - Serialization

-(void) updateWithDictionary:(NSDictionary*)dictionary
{
  // Currently handled by subclasses, but could consider pulling common code here
}

@end
