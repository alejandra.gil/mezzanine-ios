//
//  MZVect.h
//  MezzKit
//
//  Created by Miguel Sanchez Valdes on 26/03/2019.
//  Copyright © 2019 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MZVect : NSObject
{
  NSArray *_components;
}

- (instancetype)initWithX:(NSNumber * _Nonnull)x y:(NSNumber * _Nonnull)y;
- (instancetype)initWithX:(NSNumber * _Nonnull)x y:(NSNumber * _Nonnull)y z:(NSNumber * _Nonnull)z;

+ (instancetype)vectWithX:(NSNumber * _Nonnull)x y:(NSNumber * _Nonnull)y;
+ (instancetype)vectWithX:(NSNumber * _Nonnull)x y:(NSNumber * _Nonnull)y z:(NSNumber * _Nonnull)z;

+ (instancetype)integerVectWithX:(NSInteger)x y:(NSInteger)y;
+ (instancetype)integerVectWithX:(NSInteger)x y:(NSInteger)y z:(NSInteger)z;

+ (instancetype)doubleVectWithX:(double)x y:(double)y;
+ (instancetype)doubleVectWithX:(double)x y:(double)y z:(double)z;

- (NSString *)description;
- (MZVect *)cross:(MZVect *)v;
- (NSInteger)integerVectDot:(MZVect *)v;
- (double)doubleVectDot:(MZVect *)v;
- (BOOL)equalToVect:(MZVect *)v;

- (BOOL)isIntegerVect;
- (BOOL)isDoubleVect;

@property (nonatomic, strong) NSNumber * _Nonnull x;
@property (nonatomic, strong) NSNumber * _Nonnull y;
@property (nonatomic, strong) NSNumber *z;

@end

NS_ASSUME_NONNULL_END
