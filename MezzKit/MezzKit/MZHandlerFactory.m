//
//  MZHandlerFactory.m
//  MezzKit
//
//  Created by Zai Chang on 6/4/15.
//  Copyright (c) 2015 Oblong Industries. All rights reserved.
//

#import "MZHandlerFactory.h"
#import "MZCommunicator+Constants.h"
#import "OBProtein+Mezzanine.h"
#import "MZInfopresenceCall.h"
#import "NSString+Localization.h"
#import "MZCommunicatorRequestor.h"
#import "MZUploader.h"
#import "MZSessionManager.h"

@interface MZCommunicator (Private)

-(void) joinSuccessful;
-(void) joinFailed:(NSError*)error;
-(void) setupDisableLockedSessionOnJoinHandler;

@end


@implementation MZHandlerFactory


- (MZWorkspace*)workspaceFromWorkspaceContext:(OBProtein*)protein
{
  NSString *workspaceUid = [protein ingestForKey:@"workspace-uid"];
  if ([workspaceUid isEqual:systemModel.currentWorkspace.uid])
    return systemModel.currentWorkspace;

  MZWorkspace *workspaceFromModel = [systemModel workspaceWithUid:workspaceUid];
  if (workspaceFromModel == nil)
    DLog(@"Workspace with uid %@ not available in model. Protein: %@", workspaceUid, protein);
  return workspaceFromModel;
}


#pragma mark - Client Proteins

-(OBMessageHandler*) createClientJoinHandler
{
  OBMessageHandler *handler = [OBMessageHandler messageHandler];
  handler.descrips = @[MZTransactionNameClientJoin];
  handler.filter = communicator.provenanceFilter;
  handler.handler = ^(OBProtein *p)
  {
    // Cancel join request timeout
    [NSObject cancelPreviousPerformRequestsWithTarget:communicator selector:@selector(checkOnJoinResponse) object:nil];

    if (communicator.hasJoinedSession)
      return;

    // Sets Mezzanine version into the model in order to have an explicit reference accessible from everywhere
    systemModel.myMezzanine.version = (p.ingests)[@"mezzanine-version"];
    
    // In case apiVersion is nil (below 3.6) we use mezzanine-version value
    systemModel.apiVersion = !systemModel.apiVersion ? systemModel.myMezzanine.version : systemModel.apiVersion;
    
    NSNumber *success = (p.ingests)[@"permission"];
    if ([success boolValue])
    {
      [communicator joinSuccessful];
      
      [systemModel updateWithDictionary:p.ingests];
      
      systemModel.state = MZSystemStateWorkspace;
      
      [communicator.requestor requestWorkspaceListRequest];
      MZTransaction *infopresenceTransaction = [communicator.requestor requestInfopresenceDetailRequest];

      // HACK: On the workspace list case the success block is called before its handler processes the response protein.
      // Because protein responses are sent in the order, by waiting to the response of the next protein (infopresense-detail)
      // we can be sure that the former (workspace-list) is already processed.
      infopresenceTransaction.successBlock = ^{
        [communicator postNotification:MZCommunicatorReceviedInitialWorkspaceListStateNotification];
        [communicator postNotification:MZCommunicatorReceviedInitialInfopresenceStateNotification];
      };

      if (!communicator.receivedInitialState)
      {
        [communicator postNotification:MZCommunicatorReceviedInitialStateNotification];
        communicator.receivedInitialState = YES;
      }
    }
    else
    {
      NSString *reason = (p.ingests)[@"reason"];
      if ([reason isEqualToString:@"passkey-required"])
      {
        systemModel.passphraseRequested = YES;
        [communicator setupDisableLockedSessionOnJoinHandler];
        
        // Begin sending heartbeat during passkey screen
        [communicator startHeartbeat];
      }
      else if ([reason isEqualToString:@"incorrect-passkey"])
      {
        if (!systemModel.passphraseRequested) // This can happen if passphrase is entered via URL
          systemModel.passphraseRequested = YES;
        
        NSString *errorString = MZLocalizedString(@"MZCommunicator Error Mezzanine Denied Join", nil);
        NSError *error = [NSError errorWithDomain:kMZCommunicatorErrorDomain
                                             code:kMZCommunicatorErrorCode_JoinDenied
                                         userInfo:@{NSLocalizedDescriptionKey: errorString}];
        NSDictionary *userInfo = @{@"error": error};
        
        [[NSNotificationCenter defaultCenter] postNotificationName:MZCommunicatorIncorrectPassphraseNotification object:communicator userInfo:userInfo];
      }
      else
      {
        NSString *errorString = MZLocalizedString(@"MZCommunicator Error Mezzanine Denied Join", nil);
        NSError *error = [NSError errorWithDomain:kMZCommunicatorErrorDomain
                                             code:kMZCommunicatorErrorCode_JoinDenied
                                         userInfo:@{NSLocalizedDescriptionKey: errorString}];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
          [communicator joinFailed:error];
        }];
      }
    }
  };
  
  return handler;
}

- (OBMessageHandler*)createClientLeaveHandler
{
  OBMessageHandler *handler = [OBMessageHandler messageHandler];
  handler.descrips = @[MZTransactionNameClientLeave];
  handler.filter = communicator.provenanceFilter;
  handler.handler = ^(OBProtein *p)
  {
    // It's a fire and forget request
  };
  return handler;
}


- (OBMessageHandler*)createClientEvictHandler
{
  OBMessageHandler *handler = [OBMessageHandler messageHandler];
  handler.descrips = @[MZTransactionNameClientEvict];
  handler.filter = communicator.provenanceFilter;
  handler.handler = ^(OBProtein *p)
  {
    [communicator disconnectWithMessage:MZCommunicatorDidDisconnectClientEvicted];
  };
  return handler;
}



#pragma mark - Mezzanine State Proteins

-(OBMessageHandler*) createHeartbeatHandler
{
  OBMessageHandler *heartbeatHandler = [OBMessageHandler messageHandler];
  heartbeatHandler.descrips = @[@"heartbeat",
                                @"native-mezz"];
  heartbeatHandler.handler = ^(OBProtein *p)
  {
#if ENABLE_HEARTBEAT_TIMEOUT
    
    // Its not enough to just assume we're still connected
    // In the case of an iOS device dropping wifi connection
    // to save battery (locked screen, etc), the native side
    // will deem the client as disconnected and won't respond
    // to communications. See bug 2599
    BOOL heartbeatIsCurrent = [communicator checkHeartbeatStatus];
    if (heartbeatIsCurrent)
      [communicator scheduleOrResetHeartbeatTimeout];
    else
      [communicator disconnectWithMessage:MZCommunicatorDidDisconnectHeartbeatTimeout];
#endif
  };
  return heartbeatHandler;
}


-(OBMessageHandler*) createMezzanineAwakeHandler
{
  OBMessageHandler *nihongoHandler = [OBMessageHandler messageHandler];
  nihongoHandler.descrips = @[@"psa",
                              @"ohayo-gozaimasu"];
  nihongoHandler.handler = ^(OBProtein *p)
  {
    // Probably means Mezzanine was just restarted. Disconnect
    if (communicator.isConnected)
    {
      // -[MZCommunicator disconnect] has the effect of removing all message handlers
      // but since that function is being called inside an OBMessageHandler
      // which is being enumerated in -[OBCommunicator handleMessage:fromPool:]
      // This will cause a crash since the collection is changed while being enumerated
      [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [communicator disconnectWithMessage:MZCommunicatorDidDisconnectServerRestart];
      }];
    }
  };
  return nihongoHandler;
}



#pragma mark - Mezzanine Capabilities

-(OBMessageHandler*) createMezzanineCapabilitiesHandler
{
  OBMessageHandler *handler = [OBMessageHandler messageHandler];
  handler.descrips = @[@"mez-caps"];
  handler.handler = ^(OBProtein *p)
  {
    [systemModel updateWithDictionary:p.ingests];
  };
  return handler;
}


#pragma mark - Workspace Handlers

-(OBMessageHandler*) createWorkspaceStateHandler
{
  OBMessageHandler *handler = [OBMessageHandler messageHandler];
  handler.descrips = @[@"psa",
                       MZTransactionNameWorkspaceSwitch];
  handler.handler = ^(OBProtein *p)
  {
    [communicator.uploader cancelAllPendingFileUploads];  // This may be moved to inside the previousWorkspaceDictionary block such that it isn't called twice (once when opening == YES, and once when opening == NO)

    NSDictionary *previousWorkspaceDictionary = (p.ingests)[@"previous-workspace"];
    if (previousWorkspaceDictionary &&
        systemModel.currentWorkspace &&
        previousWorkspaceDictionary[@"uid"] != [NSNull null] &&
        [previousWorkspaceDictionary[@"uid"] isEqualToString:systemModel.currentWorkspace.uid])
    {
      // Update flags on the current workspace before it is replaced by the new one
      [systemModel.currentWorkspace updateWithDictionary:previousWorkspaceDictionary];
    }

    NSDictionary *workspaceDictionary = (p.ingests)[@"current-workspace"];
    if (workspaceDictionary)
    {
      [systemModel updateCurrentWorkspaceWithDictionary:workspaceDictionary];
      
      BOOL currentWorkspaceExistsInWorkspaceList = [systemModel.workspaces containsObject:systemModel.currentWorkspace];
      if (systemModel.currentWorkspace.isSaved && !currentWorkspaceExistsInWorkspaceList)
        [systemModel insertObject:systemModel.currentWorkspace inWorkspacesAtIndex:systemModel.countOfWorkspaces];
    }
  };
  return handler;
}


-(OBMessageHandler*) createWorkspaceOpenHandler
{
  OBMessageHandler *handler = [OBMessageHandler messageHandler];
  handler.descrips = @[@"psa",
                       MZTransactionNameWorkspaceOpen];
  handler.handler = ^(OBProtein *p)
  {
    NSDictionary *workspaceDict = (p.ingests)[@"workspace"];
    [systemModel openWorkspace:workspaceDict];
  };
  return handler;
}


-(OBMessageHandler*) createWorkspaceCloseHandler
{
  OBMessageHandler *handler = [OBMessageHandler messageHandler];
  handler.descrips = @[@"psa",
                       MZTransactionNameWorkspaceClose];
  handler.handler = ^(OBProtein *p)
  {
    [communicator.uploader cancelAllPendingFileUploads];
    [systemModel updateWithDictionary:p.ingests];
    //systemModel.currentWorkspace = nil;
  };
  return handler;
}


-(OBMessageHandler*) createWorkspaceDiscardHandler
{
  OBMessageHandler *handler = [OBMessageHandler messageHandler];
  handler.descrips = @[@"psa",
                       MZTransactionNameWorkspaceDiscard];
  handler.handler = ^(OBProtein *p)
  {
    [communicator.uploader cancelAllPendingFileUploads];
    [systemModel updateWithDictionary:p.ingests];
    //systemModel.currentWorkspace = nil;
  };
  return handler;
}


-(OBMessageHandler*) createWorkspaceCreateHandler
{
  OBMessageHandler *handler = [OBMessageHandler messageHandler];
  handler.descrips = @[MZTransactionNameWorkspaceCreate];
  handler.filter = ^(OBProtein *p)
  {
    // Check for psa descrip here because this could either be a psa or pm
    return [communicator proteinIsPSAOrPM:p];
  };
  handler.handler = ^(OBProtein *p)
  {
    NSString *workspaceUid = (p.ingests)[@"uid"];
    if ([systemModel.currentWorkspace.uid isEqual:workspaceUid])
    {
      // This happens when another client or native saves an unsaved workspace
      // The only response is workspace-create, in which case we must update the name and other properties
      [systemModel.currentWorkspace updateWithDictionary:p.ingests];
      [systemModel insertObject:systemModel.currentWorkspace inWorkspacesAtIndex:systemModel.countOfWorkspaces];
    }
    else
    {
      MZWorkspace *newWorkspace = [[MZWorkspace alloc] init];
      newWorkspace.uid = workspaceUid;
      [newWorkspace updateWithDictionary:p.ingests];
      [systemModel insertObject:newWorkspace inWorkspacesAtIndex:systemModel.countOfWorkspaces];
    }
  };
  return handler;
}


-(OBMessageHandler*) createWorkspaceDeleteHandler
{
  OBMessageHandler *handler = [OBMessageHandler messageHandler];
  handler.descrips = @[MZTransactionNameWorkspaceDelete];
  handler.filter = ^(OBProtein *p)
  {
    // Check for psa descrip here because this could either be a psa or pm
    return [communicator proteinIsPSAOrPM:p];
  };
  handler.handler = ^(OBProtein *p)
  {
    NSString *uid = [p ingestForKey:@"uid"];
    if (uid && systemModel)
    {
      NSUInteger index = [systemModel.workspaces indexOfObject:[systemModel workspaceWithUid:uid]];
      if (index != NSNotFound)
        [systemModel removeObjectFromWorkspacesAtIndex:index];
    }
  };
  return handler;
}


-(OBMessageHandler*) createWorkspaceModifiedHandler
{
  OBMessageHandler *handler = [OBMessageHandler messageHandler];
  handler.descrips = @[MZTransactionNameWorkspaceModified];
  handler.handler = ^(OBProtein *p)
  {
    MZWorkspace *workspace = [systemModel workspaceWithUid:p.ingests[@"uid"]];
    if (workspace)
    {
      [workspace updateWithDictionary:p.ingests];
    }
  };
  return handler;
}


-(OBMessageHandler*) createWorkspaceRenameHandler
{
  OBMessageHandler *handler = [OBMessageHandler messageHandler];
  handler.descrips = @[MZTransactionNameWorkspaceRename];
  handler.filter = ^(OBProtein *p)
  {
    // Check for psa descrip here because this could either be a psa or pm
    return [communicator proteinIsPSAOrPM:p];
  };
  handler.handler = ^(OBProtein *p)
  {
    if (!systemModel)
      return;
    
    NSString *uid = [p ingestForKey:@"uid"];
    if (uid)
    {
      MZWorkspace *workspace = [systemModel workspaceWithUid:uid];
      if (workspace)
      {
        // This is a measure to prevent owner information being wiped out because
        // the rename-workspace protein doesn't include the owner field
        NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:p.ingests];
        [dict setValue:workspace.owner forKey:@"owners"];
        [workspace updateWithDictionary:dict];
      }
    }
  };
  return handler;
}


-(OBMessageHandler*) createWorkspaceReassignHandler
{
  OBMessageHandler *handler = [OBMessageHandler messageHandler];
  handler.descrips = @[@"psa",
                       MZTransactionNameWorkspaceReassign];
  handler.handler = ^(OBProtein *p)
  {
    NSString *oldUid = [p ingestForKey:@"old-uid"];
    NSString *newUid = [p ingestForKey:@"new-uid"];
    MZWorkspace *workspace = [systemModel workspaceWithUid:oldUid];
    if (workspace)
    {
      workspace.uid = newUid;

      // This is a workaround for a check [MZWorkspace updateWithDictionary:] which expects the key @"uid" instead of @"new-uid"
      NSMutableDictionary *workspaceDict = [p.ingests mutableCopy];
      workspaceDict[@"uid"] = newUid;

      [workspace updateWithDictionary:workspaceDict];
    }
  };
  return handler;
  
}


-(OBMessageHandler*) createWorkspaceSaveHandler
{
  OBMessageHandler *handler = [OBMessageHandler messageHandler];
  handler.descrips = @[@"psa",
                       MZTransactionNameWorkspaceSave];
  handler.handler = ^(OBProtein *p)
  {
    MZWorkspace *workspace = [systemModel workspaceWithUid:p.ingests[@"uid"]];
    if (workspace)
    {
      [workspace updateWithDictionary:p.ingests];
    }
  };
  return handler;
}


-(OBMessageHandler*) createWorkspaceListHandler
{
  OBMessageHandler *handler = [OBMessageHandler messageHandler];
  handler.descrips = @[MZTransactionNameWorkspaceList];
  handler.filter = communicator.provenanceFilter;
  handler.handler = ^(OBProtein *p)
  {
    [systemModel updateWithDictionary:p.ingests];
  };
  return handler;
}


#pragma mark - Portfolio Handlers

-(OBMessageHandler*) createPortfolioItemInsertHandler
{
  OBMessageHandler *handler = [OBMessageHandler messageHandler];
  handler.descrips = @[@"psa",
                       MZTransactionNamePortfolioItemInsert];
  handler.handler = ^(OBProtein *p)
  {
    // Check if the new slide belongs has an asset uid that corresponds to an
    // existing transaction and clear it.
    // This is done here because there is no transaction id specced out in
    // the image-ready proteins, and thus the regular transaction clearing
    // mechanism won't work
    NSString *assetUid = [p ingestForKey:@"content-source"];
    [communicator.uploader clearPendingUploadTransactionsForAssetUid:assetUid];
    
    
    if (systemModel.currentWorkspace)
    {
      NSNumber *indexNumber = [p ingestForKey:@"index"];
      if (!indexNumber)
        return;
      
      MZWorkspace *workspace = systemModel.currentWorkspace;

      [systemModel.currentWorkspace updateLiveStreamsWithDictionary:p.ingests andRemoteMezzes:systemModel.remoteMezzes];

      MZPortfolioItem *newSlide = [[MZPortfolioItem alloc] init];
      [newSlide updateWithDictionary:p.ingests];
      
      NSInteger index = [indexNumber integerValue];
      
      DLog(@"Received new slide %@ at index %ld", newSlide, (long)index);
      
      if (index >= 0 && index <= workspace.presentation.numberOfSlides)
      {
        [workspace.presentation insertObject:newSlide inSlidesAtIndex:index];
        [workspace.presentation updateSlideIndexes];
      }
      else
        // This should never be the case as the native side's state proteins
        // should properly reflect the correct workspace state, but this arises
        // in the case of someone clearing all slides while this device is
        // mid-upload
        NSLog(@"Mezzanine: Cannot insert slide at index %ld", (long)index);
    }
  };
  return handler;
}


-(OBMessageHandler*) createPortfolioItemReorderHandler
{
  OBMessageHandler *portfolioReorderHandler = [OBMessageHandler messageHandler];
  portfolioReorderHandler.descrips = @[@"psa",
                                   MZTransactionNamePortfolioItemReorder];
  portfolioReorderHandler.handler = ^(OBProtein *p)
  {
    if (!systemModel.currentWorkspace)
      return;
    
    id uid = [p ingestForKey:@"uid"];
    NSNumber *toIndexNumber = [p ingestForKey:@"index"];
    if (!uid || !toIndexNumber)
      return;
    
    if ([uid isKindOfClass:[NSNumber class]])
      uid = [uid stringValue];
    
    NSInteger toIndex = [toIndexNumber integerValue];
    MZPresentation *presentation = systemModel.currentWorkspace.presentation;
    [presentation moveSlideWithUid:uid toIndex:toIndex];
  };
  return portfolioReorderHandler;
}


-(OBMessageHandler*) createPortfolioItemDeleteHandler
{
  OBMessageHandler *handler = [OBMessageHandler messageHandler];
  handler.descrips = @[@"psa",
                       MZTransactionNamePortfolioItemDelete];
  handler.handler = ^(OBProtein *p)
  {
    if (systemModel.currentWorkspace)
    {
      NSString *uid = [p ingestForKey:@"uid"];
      if (!uid)
        return;
      
      MZPresentation *presentation = systemModel.currentWorkspace.presentation;
      MZPortfolioItem *slide = [presentation slideWithUid:uid];
      if (slide)
      {
        NSInteger index = [presentation.slides indexOfObject:slide];
        
        DLog(@"Deleting slide %@ at index %ld", slide, (long)index);
        
        [presentation removeObjectFromSlidesAtIndex:index];
        [presentation updateSlideIndexes];

        [systemModel.currentWorkspace removeRemoteLiveStreamsWithContentSource:slide.contentSource];
      }
    }
  };
  return handler;
}


-(OBMessageHandler*) createPortfolioClearHandler
{
  OBMessageHandler *handler = [OBMessageHandler messageHandler];
  handler.descrips = @[@"psa", MZTransactionNamePortfolioClear];
  handler.handler = ^(OBProtein *p)
  {
    MZWorkspace *workspace = [self workspaceFromWorkspaceContext:p];
    if (!workspace)
      return;
    
    [workspace.presentation removeAllSlides];

    [communicator.uploader cancelAllPendingFileUploads];

    [workspace removeUnusedRemoteLiveStreams];
  };
  return handler;
}


-(OBMessageHandler*) createPortfolioDownloadHandler
{
  OBMessageHandler *handler = [OBMessageHandler messageHandler];
  handler.descrips = @[MZTransactionNamePortfolioDownload];
  handler.filter = communicator.provenanceFilter;
  handler.handler = ^(OBProtein *p)
  {
    if ([p containsDescrip:@"error"])
    {
      NSString *description = (p.ingests)[@"description"];
      NSString *summary = (p.ingests)[@"summary"];
      systemModel.portfolioExport.info.errorMessage = description;
      systemModel.portfolioExport.info.errorTitle = summary;
      systemModel.portfolioExport.info.state = MZExportStateFailed;
      return;
    }

    // We only accept downloads that we have just requested
    if (systemModel.portfolioExport.info.state != MZExportStateRequested)
      return;

    systemModel.portfolioExport.info.workspaceName = systemModel.currentWorkspace.name;

    NSString *url = (p.ingests)[@"download-url"];
    if ([communicator isARemoteParticipationConnection]) {
      systemModel.portfolioExport.info.url = [communicator RPFileDownload:url];
    } else {
      NSURL *urlForResource = [communicator urlForResource:url];
      systemModel.portfolioExport.info.url = urlForResource;
    }
  };
  
  return handler;
}



#pragma mark - Presentation Handlers

- (OBMessageHandler*) createPresentationDetailHandler
{
  OBMessageHandler *handler = [OBMessageHandler messageHandler];
  handler.descrips = @[MZTransactionNamePresentationDetail, @"psa"];
  handler.handler = ^(OBProtein *p)
  {
//    MZWorkspace *workspace = [self workspaceFromWorkspaceContext:p];
    MZWorkspace *workspace = systemModel.currentWorkspace;
    if (workspace)
    {
      [workspace.presentation updateWithDictionary:p.ingests];
    }
  };
  return handler;
}


- (OBMessageHandler*) createPresentationScrollHandler
{
  OBMessageHandler *handler = [OBMessageHandler messageHandler];
  handler.descrips = @[MZTransactionNamePresentationScroll, @"psa"];
  handler.handler = ^(OBProtein *p)
  {
//    MZWorkspace *workspace = [self workspaceFromWorkspaceContext:p];
    MZWorkspace *workspace = systemModel.currentWorkspace;
    if (workspace)
    {
      [workspace.presentation updateWithDictionary:p.ingests];
    }
  };
  return handler;
}



#pragma mark - Passkey Handlers

-(OBMessageHandler*) createPasskeyEnabledHandler
{
  OBMessageHandler *handler = [OBMessageHandler messageHandler];
  handler.descrips = @[MZTransactionNamePasskeyEnable, @"psa"];
  handler.handler = ^(OBProtein *p)
  {
    if (!systemModel.passphraseRequested)
    {
      NSString *exempted = [p ingestForKey:@"exempt"];
      if (exempted)
      { // v 2.4
        NSString *provenance = communicator.provenance;
        if (![provenance isEqualToString:exempted])
        {
          communicator.hasJoinedSession = NO;
          systemModel.passphraseRequested = YES;
        }
        else
        {
          [communicator.requestor requestPasskeyDetailRequest];
        }
      }
      else
      { // v 2.0
        communicator.hasJoinedSession = NO;
        systemModel.passphraseRequested = YES;
      }
    }
  };
  return handler;
}


-(OBMessageHandler*) createPasskeyDisabledHandler
{
  OBMessageHandler *handler = [OBMessageHandler messageHandler];
  handler.descrips = @[@"psa",
                       MZTransactionNamePasskeyDisable];
  handler.handler = ^(OBProtein *p)
  {
    systemModel.passphraseRequested = NO;
    systemModel.passphrase = nil;
  };
  return handler;
}


-(OBMessageHandler*) createPasskeyDetailsHandler
{
  OBMessageHandler *handler = [OBMessageHandler messageHandler];
  handler.descrips = @[MZTransactionNamePasskeyDetails];
  handler.handler = ^(OBProtein *p)
  {
    NSString *pass = (p.ingests)[@"passkey"];
    // TODO: FIX ME with incoming MZProto generated code ([NSNull null])
    if ([pass isEqual: [NSNull null]] || !pass || [pass isEqualToString:@""])
      systemModel.passphrase = nil;
    else
      systemModel.passphrase = pass;
  };
  return handler;
}



#pragma mark - Login / Logout

-(OBMessageHandler*) createClientSignInHandler
{
  OBMessageHandler *handler = [OBMessageHandler messageHandler];
  handler.descrips = @[MZTransactionNameClientSignIn];
  handler.filter = communicator.provenanceFilter;
  handler.handler = ^(OBProtein *p)
  {
    if ([p containsDescrip:@"error"])
      return;
    
    [communicator.requestor requestWorkspaceListRequest];
    
    systemModel.isCurrentUserSuperuser = [[p ingestForKey:@"superuser"] boolValue];
    systemModel.currentUsername = [p ingestForKey:@"username"];
    
  };
  return handler;
}


-(OBMessageHandler*) createClientSignOutHandler
{
  OBMessageHandler *handler = [OBMessageHandler messageHandler];
  handler.descrips = @[MZTransactionNameClientSignOut];
  handler.filter = communicator.provenanceFilter;
  handler.handler = ^(OBProtein *p)
  {
    if (systemModel.toBeDisconnected)
    {
      systemModel.toBeDisconnected = NO;
      [communicator disconnect];
    }
    else
    {
      systemModel.isCurrentUserSuperuser = NO;
      systemModel.currentUsername = nil;
    }
  };
  return handler;
}


#pragma mark - Windshield

-(OBMessageHandler*) createWindshieldItemGrabHandler
{
  OBMessageHandler *handler = [OBMessageHandler messageHandler];
  handler.descrips = @[@"psa",
                       MZTransactionNameWindshieldItemGrab];
  handler.handler = ^(OBProtein *p)
  {
    if (!systemModel.currentWorkspace)
      return;
    
    NSString *uid = [p ingestForKey:@"uid"];
    if (!uid)
      return;
    
    MZWindshield *windshield = systemModel.currentWorkspace.windshield;
    MZWindshieldItem *item = [windshield itemWithUid:uid];
    if (item)
    {
#if __DEBUG__
      NSInteger index = [windshield.items indexOfObject:item];
      DLog(@"Grabbing item %@ at index %d", item, index);
#endif
      item.isBeingGrabbed = YES;
    }
  };
  return handler;
}


-(OBMessageHandler*) createWindshieldItemCreateHandler
{
  OBMessageHandler *handler = [OBMessageHandler messageHandler];
  handler.descrips = @[MZTransactionNameWindshieldItemCreate];
  handler.filter = ^(OBProtein *p)
  {
    return [communicator proteinIsPSAOrResponseForMe:p];
  };
  handler.handler = ^(OBProtein *p)
  {
    if ([p containsDescrip:@"error"])
    {
      NSString *description = [p ingestForKey:@"description"];
      systemModel.popupMessage = description;
      return;
    }
    
    MZWindshield *windshield = systemModel.currentWorkspace.windshield;
    if (!windshield)
      return;

    [systemModel.currentWorkspace updateLiveStreamsWithDictionary:p.ingests andRemoteMezzes:systemModel.remoteMezzes];

    MZWindshieldItem *newItem = [[MZWindshieldItem alloc] init];
    NSString *surfaceName = p.ingests[@"surface-name"] ? p.ingests[@"surface-name"] : @"main";
    newItem.surface = [systemModel surfaceWithName:surfaceName];
    newItem.feld = [newItem.surface feldWithName:p.ingests[@"feld-relative-coords"][@"feld-id"]];
    [newItem updateWithDictionary:p.ingests];
    
    NSInteger index = [windshield countOfItems];
    DLog(@"Received new item %@ at index %ld", newItem, (long)index);
    
    if (index >= 0)
      [windshield insertObject:newItem inItemsAtIndex:index];
    else
      // This should never be the case as the native side's state proteins
      // should properly reflect the correct workspace state, but this arises
      // in the case of someone clearing all slides while this device is
      // mid-upload
      NSLog(@"Mezzanine: Cannot insert item at index %ld", (long)index);
  };
  return handler;
}


-(OBMessageHandler*) createWindshieldItemTransformHandler
{
  OBMessageHandler *handler = [OBMessageHandler messageHandler];
  handler.descrips = @[MZTransactionNameWindshieldItemTransform];
  handler.filter = ^(OBProtein *p)
  {
    return [communicator proteinIsPSAOrResponseForMe:p];
  };
  handler.handler = ^(OBProtein *p)
  {
    if ([p containsDescrip:@"error"])
    {
      // When a transformation can't be completed because a remote mezz
      // is holding the lock there are two errors been sent with the same
      // message. This shouldn't happen if native confirmed a shielder-grab req.
      // Until then, this hack is needed to ignore the duplicity.
      // Check Bug 6994 to check when Native implements the solution.
      // and Bug 6967 to know the origin of this bug.
      
      // NSString *error = [p ingestForKey:@"error-code"];
      // if ([error isEqualToString:@"lock-req-denied"])
      //  return;
      
      // NSString *description = [p ingestForKey:@"description"];
      // systemModel.popupMessage = description;
      
      // After bug 6965 no error should be shown no matter the reason when doing a transformation.
      return;
    }
    
    if (systemModel.currentWorkspace)
    {
      NSString *uid = p.ingests[@"uid"];
      MZWindshield *windshield = systemModel.currentWorkspace.windshield;
      MZWindshieldItem *item = [windshield itemWithUid:uid];
      
      if (item)
      {
        DLog(@"Updating item %@", item);
        NSString *surfaceName = p.ingests[@"surface-name"] ? p.ingests[@"surface-name"] : @"main";
        item.surface = [systemModel surfaceWithName:surfaceName];
        item.feld = [item.surface feldWithName:p.ingests[@"feld-relative-coords"][@"feld-id"]];
        [item updateWithTransformDictionary:p.ingests];
      }
    }
  };
  return handler;
}

-(OBMessageHandler*) createWindshieldItemDeleteHandler
{
  OBMessageHandler *handler = [OBMessageHandler messageHandler];
  handler.descrips = @[MZTransactionNameWindshieldItemDelete];
  handler.handler = ^(OBProtein *p)
  {
    if (!systemModel.currentWorkspace)
      return;
    
    if ([p containsDescrip:@"error"])
    {
      NSString *description = [p ingestForKey:@"description"];
      systemModel.popupMessage = description;
      return;
    }
    
    NSString *uid = [p ingestForKey:@"uid"];
    if (!uid)
      return;
    
    MZWindshield *windshield = systemModel.currentWorkspace.windshield;
    MZWindshieldItem *item = [windshield itemWithUid:uid];
    if (item)
    {
      NSInteger index = [windshield.items indexOfObject:item];
      DLog(@"Deleting item %@ at index %ld", item, (long)index);
      [windshield removeObjectFromItemsAtIndex:index];

      [systemModel.currentWorkspace removeRemoteLiveStreamsWithContentSource:item.assetUid];
    }
  };
  
  return handler;
}

-(OBMessageHandler*) createWindshieldDetailsHandler
{
  OBMessageHandler *handler = [OBMessageHandler messageHandler];
  handler.descrips = @[MZTransactionNameWindshieldDetail];
  handler.handler = ^(OBProtein *p)
  {
    NSArray *items = (p.ingests)[@"windshield-items"];
    MZWindshield *windshield = systemModel.currentWorkspace.windshield;

    [systemModel updateLiveStreamsWithVideoShielders:items];

    for (NSDictionary *shielderDict in items)
    {
      NSString *uid = shielderDict[@"uid"];
      MZWindshieldItem *item = [windshield itemWithUid:uid];
      if (!item)
      {
        item = [[MZWindshieldItem alloc] init];
        [item updateSurfaceAndFeldWithDictionary:shielderDict usingSystemModel:systemModel];
        [item updateWithDictionary:shielderDict];
        
        NSInteger index = [windshield countOfItems];
        DLog(@"Received new item %@ at index %ld", item, (long)index);
        
        if (index >= 0)
          [windshield insertObject:item inItemsAtIndex:index];
        else
          // This should never be the case as the native side's state proteins
          // should properly reflect the correct workspace state, but this arises
          // in the case of someone clearing all slides while this device is
          // mid-upload
          NSLog(@"Mezzanine: Cannot insert item at index %ld", (long)index);
      }
      else
      {
        NSString *surfaceName = shielderDict[@"surface-name"] ? shielderDict[@"surface-name"] : @"main";
        item.surface = [systemModel surfaceWithName:surfaceName];
        item.feld = [item.surface feldWithName:shielderDict[@"feld-relative-coords"][@"feld-id"]];
        [item updateWithDictionary:shielderDict];
      }
    }
    
    // In the case of a windshield item being deleted, check that all current items still exists in the windshield
    NSMutableIndexSet *itemsToDelete = [NSMutableIndexSet indexSet];
    NSArray *uidsFromProtein = [items valueForKey:@"uid"];
    for (NSInteger i=0; i<windshield.items.count; i++)
    {
      MZWindshieldItem *item = (MZWindshieldItem *)windshield.items[i];
      if (![uidsFromProtein containsObject:item.uid])
        [itemsToDelete addIndex:i];
    }
    [windshield removeItemsAtIndexes:itemsToDelete];

  };
  return handler;
}

-(OBMessageHandler*) createWindshieldClearHandler
{
  OBMessageHandler *handler = [OBMessageHandler messageHandler];
  handler.descrips = @[@"psa",
                       MZTransactionNameWindshieldClear];
  handler.handler = ^(OBProtein *p)
  {
    MZWindshield *windshield = systemModel.currentWorkspace.windshield;
    if (!windshield)
      return;

    NSArray *surfacesToDelete = (p.ingests)[@"surfaces"];

    NSMutableIndexSet *indexesToRemove = [NSMutableIndexSet indexSet];
    for (NSInteger i=0; i<windshield.items.count; i++)
    {
      MZWindshieldItem *item = (MZWindshieldItem *)windshield.items[i];
      if ([surfacesToDelete containsObject:item.surface.name])
        [indexesToRemove addIndex:i];
    }

    [windshield removeItemsAtIndexes:indexesToRemove];
    [systemModel.currentWorkspace removeUnusedRemoteLiveStreams];
  };
  return handler;
}



#pragma mark - Live Streams

-(OBMessageHandler*) createLiveStreamsDetailHandler
{
  OBMessageHandler *handler = [OBMessageHandler messageHandler];
  handler.descrips = @[MZTransactionNameLiveStreamsDetail];
  handler.handler = ^(OBProtein *p)
  {
    [systemModel.currentWorkspace updateLiveStreamsWithDictionary:p.ingests];
  };
  return handler;
}

-(OBMessageHandler*) createLiveStreamAppearHandler
{
  OBMessageHandler *handler = [OBMessageHandler messageHandler];
  handler.descrips = @[@"live-stream-appear"];
  handler.handler = ^(OBProtein *p)
  {
    // N.B. For live streams their content-source is their uid
    NSString *contentSourceButReallyItsUid = p.ingests[@"content-source"];
    if (!systemModel.currentWorkspace)
      return;

    MZLiveStream *liveStream = [systemModel.currentWorkspace liveStreamWithUid:contentSourceButReallyItsUid];
    if (liveStream)
    {
      liveStream.active = YES;
    }
  };
  return handler;
}

-(OBMessageHandler*) createLiveStreamRefreshHandler
{
  OBMessageHandler *handler = [OBMessageHandler messageHandler];
  handler.descrips = @[@"live-stream-refresh"];
  handler.handler = ^(OBProtein *p)
  {
    NSString *source = (p.ingests)[@"content-source"];
    for (MZLiveStream *liveStream in systemModel.currentWorkspace.liveStreams)
    {
      if ([liveStream.uid isEqualToString:source])
      {
        NSString *absoluteTime = [NSString stringWithFormat:@"%f", CFAbsoluteTimeGetCurrent()];
        liveStream.thumbURL = [NSString stringWithFormat:@"%@?%@", (p.ingests)[@"thumbnail-image"][@"uri"], absoluteTime];
        liveStream.fullURL = [NSString stringWithFormat:@"%@?%@", (p.ingests)[@"image"][@"uri"], absoluteTime];
        break;
      }
    }
  };
  return handler;
}

-(OBMessageHandler*) createLiveStreamDisappearHandler
{
  OBMessageHandler *handler = [OBMessageHandler messageHandler];
  handler.descrips = @[@"live-stream-disappear"];
  handler.handler = ^(OBProtein *p)
  {
    NSString *source = (p.ingests)[@"content-source"];
    for (MZLiveStream *liveStream in systemModel.currentWorkspace.liveStreams)
    {
      if ([liveStream.uid isEqualToString:source])
      {
        liveStream.thumbURL = nil;
        liveStream.fullURL = nil;
        liveStream.thumbnail = nil;
        break;
      }
    }
  };
  return handler;
}


#pragma mark - Infopresence

-(OBMessageHandler*) createInfopresenceSessionOutgoingRequestHandler
{
  OBMessageHandler *handler = [OBMessageHandler messageHandler];
  handler.descrips = @[MZTransactionNameInfopresenceSessionOutgoingRequest, @"psa"];
  handler.handler = ^(OBProtein *p)
  {
    NSString *mezzUid = (p.ingests)[@"uid"];
    if (!mezzUid)
      return;
    
    MZRemoteMezz *mezz = [systemModel remoteMezzWithUid:mezzUid];
    mezz.callResolution = MZInfopresenceCallResolutionUnknown;

		MZInfopresenceCall *call = [[MZInfopresenceCall alloc] initWithDictionary:@{ @"uid": mezzUid,
																																								 @"type": kMZInfopresenceCallTypeJoinRequest }];
		[systemModel.infopresence appendObjectToOutgoingCalls:call];

		[systemModel updateRemoteMezzanineInfopresenceState];
  };
  return handler;
}

-(OBMessageHandler*) createInfopresenceSessionOutgoingRequestResolveHandler
{
  OBMessageHandler *handler = [OBMessageHandler messageHandler];
  handler.descrips = @[MZTransactionNameInfopresenceSessionOutgoingRequestResolve];
  handler.handler = ^(OBProtein *p)
  {
    MZInfopresence *infopresence = systemModel.infopresence;
    NSString *outgoingRequestToMezzUid = [p ingestForKey:@"uid"];
		
		MZInfopresenceCall *call = [infopresence outgoingCallWithUid:outgoingRequestToMezzUid];
		[call updateWithDictionary:p.ingests];

		MZRemoteMezz *mezz = [systemModel remoteMezzWithUid:outgoingRequestToMezzUid];
		NSString *resolution = [p ingestForKey:@"resolution"];
		[mezz updateWithCallResolution:resolution ofType:MZInfopresenceCallTypeJoinRequest];
		
		[systemModel updateRemoteMezzanineInfopresenceState];

		// Remove the call object after updating remote mezz states
		[infopresence removeObjectFromOutgoingCalls:call];
  };
  return handler;
}

-(OBMessageHandler*) createInfopresenceSessionIncomingRequestHandler
{
  OBMessageHandler *handler = [OBMessageHandler messageHandler];
  handler.descrips = @[MZTransactionNameInfopresenceSessionIncomingRequest];
  handler.handler = ^(OBProtein *p)
  {
    NSString *callerMezzUID = [p ingestForKey:@"uid"];
    if (!callerMezzUID)
      return;

    MZRemoteMezz *mezz = [systemModel remoteMezzWithUid:callerMezzUID];
    mezz.callResolution = MZInfopresenceCallResolutionUnknown;

    MZInfopresence *infopresence = systemModel.infopresence;

    // Check if this call is already at the incoming calls array and add it if not.
		if ([infopresence incomingCallWithUid:callerMezzUID] == nil)
			[infopresence appendObjectToIncomingCalls:[[MZInfopresenceCall alloc] initWithDictionary:@{
																																																 @"uid": callerMezzUID,
																																																 @"type": kMZInfopresenceCallTypeJoinRequest
																																																 }]];

    [systemModel updateRemoteMezzanineInfopresenceState];
  };
  return handler;
}

-(OBMessageHandler*) createInfopresenceSessionIncomingRequestResolveHandler
{
  OBMessageHandler *handler = [OBMessageHandler messageHandler];
  handler.descrips = @[MZTransactionNameInfopresenceSessionIncomingRequestResolve];
  handler.handler = ^(OBProtein *p)
  {
    MZInfopresence *infopresence = systemModel.infopresence;
    NSString *incomingRequestFromMezzUid = [p ingestForKey:@"uid"];
    NSString *resolution = [p ingestForKey:@"resolution"];

		MZInfopresenceCall *call = [infopresence incomingCallWithUid:incomingRequestFromMezzUid];
		[call updateWithDictionary:p.ingests];

		MZRemoteMezz *mezz = [systemModel remoteMezzWithUid:incomingRequestFromMezzUid];
		[mezz updateWithCallResolution:resolution ofType:MZInfopresenceCallTypeJoinRequest];
		
		[systemModel updateRemoteMezzanineInfopresenceState];

		// Remove the call object after updating remote mezz states
		[infopresence removeObjectFromIncomingCalls:call];
  };
  return handler;
}


-(OBMessageHandler*) createInfopresenceRemoteMezzesStateHandler
{
  OBMessageHandler *handler = [OBMessageHandler messageHandler];
  handler.descrips = @[MZRemoteMezzanineDetailsTransactionName];
  handler.handler = ^(OBProtein *p)
  {
    NSMutableArray *remoteMezzesDicts = (p.ingests)[@"remote-mezzanines"];
    
    // First remove all remote mezzes not in the list anymore.
    NSMutableArray *deletedMezzanines = [[NSMutableArray alloc] init];
    for (MZRemoteMezz *mezzModel in systemModel.remoteMezzes)
    {
      NSString *uidModel = mezzModel.uid;
      BOOL found = NO;
      for (NSDictionary *mezzDictDetail in remoteMezzesDicts)
      {
        NSString *uidDetail = mezzDictDetail[@"uid"];
        if ([uidModel isEqualToString:uidDetail])
        {
          found = YES;
          break;
        }
      }
      if (!found)
      {
        [deletedMezzanines addObject:mezzModel];
      }
    }
    for (MZRemoteMezz *mezz in deletedMezzanines)
    {
      NSInteger index = [systemModel.remoteMezzes indexOfObject:mezz];
      DLog(@"Deleting Mezz %@ at index %ld", mezz, (long)index);
      [systemModel removeObjectFromRemoteMezzesAtIndex:index];
    }
    
    // Then add the new ones or update the existent ones.
    for (NSDictionary *mezzDict in remoteMezzesDicts)
    {
      NSString *uid = mezzDict[@"uid"];
      MZRemoteMezz *mezz = [systemModel remoteMezzWithUid:uid];
      if (!mezz)
      {
        mezz = [[MZRemoteMezz alloc] init];
        mezz.uid = uid;
        [mezz updateWithDictionary:mezzDict];
        mezz.roomType = [mezzDict[@"cloud-instance"] boolValue] ? MZMezzanineRoomTypeCloud : MZMezzanineRoomTypeRemote;
        [systemModel insertObject:mezz inRemoteMezzesAtIndex:systemModel.remoteMezzes.count];
      }
      else
      {
        [mezz updateWithDictionary:mezzDict];
      }
    }

    if (systemModel.currentWorkspace)
    {
      for (MZLiveStream *liveStream in systemModel.currentWorkspace.liveStreams)
        [systemModel.currentWorkspace updateLiveStreamRemoteMezzName:liveStream usingRemoteMezzes:systemModel.remoteMezzes];
    }
  };
  return handler;
}


-(OBMessageHandler*) createInfopresenceSessionDetailHandler
{
  OBMessageHandler *handler = [OBMessageHandler messageHandler];
  handler.descrips = @[MZTransactionNameInfopresenceSessionDetail];
  handler.handler = ^(OBProtein *p)
  {

    // HACK - WORKAROUND to avoid the initial infopresence-detail#PSA received right after client-join
    if (!systemModel.myMezzanine.uid)
      return;

    MZInfopresence *infopresence = systemModel.infopresence;

    // Update remote access values
    // TODO: FIX ME with incoming MZProto generated code ([NSNull null])
    NSString *remoteAccessURL = [p ingestForKey:@"remote-access-url"] == [NSNull null] ? nil : [p ingestForKey:@"remote-access-url"];
    remoteAccessURL = [remoteAccessURL isEqualToString:@""] ? nil : remoteAccessURL;
    infopresence.remoteAccessUrl = remoteAccessURL;

    // TODO: FIX ME with incoming MZProto generated code ([NSNull null])
    NSString *remoteAccessStatusString = [p ingestForKey:@"remote-access-status"] == [NSNull null] ? nil : [p ingestForKey:@"remote-access-status"];
    if ([remoteAccessStatusString isEqualToString:kMZInfopresenceRemoteAccessStatusActive])
      infopresence.remoteAccessStatus = MZInfopresenceRemoteAccessStatusActive;
    else if ([remoteAccessStatusString isEqualToString:kMZInfopresenceRemoteAccessStatusInactive])
      infopresence.remoteAccessStatus = MZInfopresenceRemoteAccessStatusInactive;
    else if ([remoteAccessStatusString isEqualToString:kMZInfopresenceRemoteAccessStatusStarting])
      infopresence.remoteAccessStatus = MZInfopresenceRemoteAccessStatusStarting;

    // Update infopresence state
    NSString *status = p.ingests[@"status"];
    if ([status isEqual:kMZInfopresenceStatusActive])
    {
      infopresence.status = MZInfopresenceStatusActive;
    }
    else if ([status isEqual:kMZInfopresenceStatusInterrupted])
    {
      infopresence.status = MZInfopresenceStatusInterrupted;
    }
    else if ([status isEqual:kMZInfopresenceStatusInactive])
    {
      infopresence.status = MZInfopresenceStatusInactive;
    }
    else if ([status isEqual:kMZInfopresenceStatusPending])
    {
      infopresence.status = MZInfopresenceStatusPending;
    }

    // PEXIP - Video chat
    [infopresence updateWithVideoChatDictionary:[p ingestForKey:@"video-chat"]];


    if (!self.systemModel.featureToggles.participantRoster)
    {
      // First remove some remote participants that are not in the infopresence session anymore.
      NSMutableArray *remoteParticipantsToRemove = [NSMutableArray new];
      NSArray *remoteParticipants = [p ingestForKey:@"remote-participants"];
      for (MZRemoteParticipant *remoteParticipant in systemModel.infopresence.remoteParticipants)
      {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", @"uid", remoteParticipant.uid];
        if ([[remoteParticipants filteredArrayUsingPredicate:predicate] count] == 0)
          [remoteParticipantsToRemove addObject:remoteParticipant];
      }

      for (MZRemoteParticipant *remoteParticipant in remoteParticipantsToRemove)
      {
        NSInteger index = [infopresence.remoteParticipants indexOfObject:remoteParticipant];
        [infopresence removeObjectFromRemoteParticipantsAtIndex:index];
      }

      // Then add new remote participants or update the existing ones.
      for (NSDictionary *participantDict in remoteParticipants)
      {
        MZRemoteParticipant *remoteParticipant = [infopresence remoteParticipantWithUid:participantDict[@"uid"]];
        if (!remoteParticipant)
        {
          remoteParticipant = [MZRemoteParticipant new];
          [remoteParticipant updateWithDictionary:participantDict];
          [infopresence insertObject:remoteParticipant inRemoteParticipantsAtIndex:[infopresence.remoteParticipants count]];
        }
        else
        {
          [remoteParticipant updateWithDictionary:participantDict];
        }
      }

      // Update Rooms and Join states
      NSArray *newRoomUids = [p ingestForKey:@"rooms"];
      NSMutableArray *roomsInInfopresence = [NSMutableArray new];
      for (NSString *roomUid in newRoomUids)
      {
        MZMezzanine *remoteMezz = [systemModel remoteMezzWithUid:roomUid];
        if (!remoteMezz && [systemModel.myMezzanine.uid isEqualToString:roomUid])
          remoteMezz = systemModel.myMezzanine;

        if (remoteMezz)
          [roomsInInfopresence addObject:remoteMezz];
      }

      NSArray *roomsToRemove = [systemModel.infopresence.rooms filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"NOT (self IN %@)", roomsInInfopresence]];
      NSIndexSet *indexSet = [systemModel.infopresence.rooms indexesOfObjectsPassingTest:^(id obj, NSUInteger idx, BOOL *stop){
        return [roomsToRemove containsObject:obj];
      }];
      [systemModel.infopresence removeRoomsAtIndexes:indexSet];
      NSArray *roomsToAdd = [roomsInInfopresence filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"NOT (self IN %@)", systemModel.infopresence.rooms]];
      indexSet = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(systemModel.infopresence.rooms.count, roomsToAdd.count)];
      [systemModel.infopresence insertRooms:roomsToAdd atIndexes:indexSet];

    }

    // Incoming / Outgoing calls can either be outgoing join requests or invites
    [infopresence updateOutgoingCallsWithDictionary:p.ingests];
    [infopresence updateIncomingCallsWithDictionary:p.ingests];

    // Finally, update the collaboration state of all remote mezzanines
    [systemModel updateRemoteMezzanineInfopresenceState];
  };
  return handler;
}


-(OBMessageHandler*) createInfopresenceFeldGeometryHandler
{
  OBMessageHandler *handler = [OBMessageHandler messageHandler];
  handler.descrips = @[@"workspace-geom-changed"];
  handler.handler = ^(OBProtein *p)
  {
    NSDictionary *feldsDictionary = [p ingestForKey:@"felds"];
    if (!feldsDictionary)
      return;

    // Work with felds in systemModel
    [self updateFeldsArrayInContainer:systemModel withFeldDictionary:feldsDictionary];

    // Work with felds in systemModel and add them to the main surface felds
    MZSurface *mainSurface = [systemModel surfacesInPrimaryWindshield].firstObject;
    NSMutableArray *feldsToAdd = @[].mutableCopy;
    NSMutableArray *feldsToRemove = @[].mutableCopy;
    if (systemModel.felds.count != mainSurface.felds.count)
    {
      for (MZFeld *feld in systemModel.felds)
      {
        if ([mainSurface.felds containsObject:feld])
          continue;
        else
          [feldsToAdd addObject:feld];
      }
      for (MZFeld *feld in mainSurface.felds)
      {
        if ([systemModel.felds containsObject:feld])
          continue;
        else
          [feldsToRemove addObject:feld];
      }
    }

    [mainSurface.felds addObjectsFromArray:feldsToAdd];
    [mainSurface.felds removeObjectsInArray:feldsToRemove];

    // Trigger KVO updates
    [systemModel willChangeValueForKey:@"felds"];
    [systemModel didChangeValueForKey:@"felds"];
    [mainSurface willChangeValueForKey:@"felds"];
    [mainSurface didChangeValueForKey:@"felds"];
  };
  return handler;
}

-(void) updateFeldsArrayInContainer:(id)container withFeldDictionary:(NSDictionary *)feldsDictionary
{
  // Windshield items keep references to the feld they belong, if in a mixed geometry update the felds
  // array gets simply swapped those references are lost. So it's better to update the information
  // within the current feld object.

  NSMutableArray *feldsIncludingInvisibles = [MZFeld orderedFeldsArrayFromDictionary:feldsDictionary includeInvisible:YES];
  NSMutableArray *felds = [container felds];

  // Remove felds no longer needed
  NSArray *feldNamesArray = [feldsIncludingInvisibles valueForKey:@"name"];
  NSArray *feldsToRemove = [felds filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"NOT (self.name IN %@)", feldNamesArray]];
  NSIndexSet *indexSet = [felds indexesOfObjectsPassingTest:^(id obj, NSUInteger idx, BOOL *stop){
    return [feldsToRemove containsObject:obj];
  }];
  [felds removeObjectsAtIndexes:indexSet];

  for (MZFeld *aFeld in feldsIncludingInvisibles)
  {
    NSArray *filteredArray = [felds filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"name == %@", aFeld.name]];
    if (filteredArray.count > 0)
    {
      [filteredArray.firstObject updateWithFeld:aFeld];
    }
    else
    {
      [felds insertObject:aFeld atIndex:felds.count];
    }
  }

  // Sort felds using x coordinates to ensure left-main-right order
  [felds sortUsingComparator:^(MZFeld *feld1, MZFeld *feld2) {
    if (feld1.center.x.doubleValue < feld2.center.x.doubleValue)
      return NSOrderedAscending;
    else if (feld1.center.x.doubleValue > feld2.center.x.doubleValue)
      return NSOrderedDescending;
    return NSOrderedSame;
  }];
}


-(OBMessageHandler*) createProfileDetailsHandler
{
  OBMessageHandler *handler = [OBMessageHandler messageHandler];
  handler.descrips = @[@"profile-detail"];
  handler.handler = ^(OBProtein *p)
  {
    NSDictionary *profileDict = (p.ingests)[@"profile"];
    [systemModel.myMezzanine updateWithDictionary:profileDict];
    systemModel.myMezzanine.roomType = MZMezzanineRoomTypeLocal;
  };
  return handler;
}


-(OBMessageHandler*) createInfopresenceOutgoingInviteHandler
{
	OBMessageHandler *handler = [OBMessageHandler messageHandler];
	handler.descrips = @[MZTransactionNameInfopresenceOutgoingInvite, @"psa"];
	handler.handler = ^(OBProtein *p) {
		MZInfopresenceCall *call = [[MZInfopresenceCall alloc] initWithDictionary:p.ingests];
		[systemModel.infopresence appendObjectToOutgoingCalls:call];
		
		NSString *uid = p.ingests[@"uid"];
		MZRemoteMezz *mezz = [systemModel remoteMezzWithUid:uid];
    mezz.callResolution = MZInfopresenceCallResolutionUnknown;
		mezz.collaborationState = MZRemoteMezzCollaborationStateInvited;
	};
	return handler;
}


-(OBMessageHandler*) createInfopresenceOutgoingInviteResolveHandler
{
	OBMessageHandler *handler = [OBMessageHandler messageHandler];
	handler.descrips = @[MZTransactionNameInfopresenceOutgoingInviteResolve, @"psa"];
	handler.handler = ^(OBProtein *p) {
		NSString *uid = p.ingests[@"uid"];
		
		MZInfopresenceCall *call = [systemModel.infopresence outgoingCallWithUid:uid];
		[call updateWithDictionary:p.ingests];
		[systemModel.infopresence removeObjectFromOutgoingCalls:call];

		MZRemoteMezz *mezz = [systemModel remoteMezzWithUid:uid];
		NSString *resolution = [p ingestForKey:@"resolution"];	// 'declined', 'accepted', 'time-out', 'canceled'
		[mezz updateWithCallResolution:resolution ofType:MZInfopresenceCallTypeInvite];
	};
	return handler;
}


-(OBMessageHandler*) createInfopresenceIncomingInviteHandler
{
	OBMessageHandler *handler = [OBMessageHandler messageHandler];
	handler.descrips = @[MZTransactionNameInfopresenceIncomingInvite, @"psa"];
	handler.handler = ^(OBProtein *p) {
		MZInfopresenceCall *call = [[MZInfopresenceCall alloc] initWithDictionary:p.ingests];

    MZRemoteMezz *mezz = [systemModel remoteMezzWithUid:call.uid];
    mezz.callResolution = MZInfopresenceCallResolutionUnknown;

		[systemModel.infopresence appendObjectToIncomingCalls:call];
	};
	return handler;
}


-(OBMessageHandler*) createInfopresenceIncomingInviteResolveHandler
{
	OBMessageHandler *handler = [OBMessageHandler messageHandler];
	handler.descrips = @[MZTransactionNameInfopresenceIncomingInviteResolve, @"psa"];
	handler.handler = ^(OBProtein *p) {
		NSString *uid = p.ingests[@"uid"];
		
		MZInfopresenceCall *call = [systemModel.infopresence incomingCallWithUid:uid];
		[call updateWithDictionary:p.ingests];
		[systemModel.infopresence removeObjectFromIncomingCalls:call];
		
		MZRemoteMezz *mezz = [systemModel remoteMezzWithUid:uid];
		NSString *resolution = [p ingestForKey:@"resolution"];	// 'declined', 'accepted', 'time-out', 'canceled'
		[mezz updateWithCallResolution:resolution ofType:MZInfopresenceCallTypeInvite];
	};
	return handler;
}


-(OBMessageHandler*) createInfopresenceParticipantJoinHandler
{
	OBMessageHandler *handler = [OBMessageHandler messageHandler];
	handler.descrips = @[MZTransactionNameInfopresenceParticipantJoin];
	handler.handler = ^(OBProtein *p) {

    if (self.systemModel.featureToggles.participantRoster)
      return;

    NSString *uid = p.ingests[@"uid"];
    NSString *type = p.ingests[@"type"];	// 'room' or 'remote-participant'

    MZInfopresence *infopresence = systemModel.infopresence;
    if ([type isEqual:@"room"])
    {
      if ([infopresence mezzanineRoomWithUid:uid])
        return;

      MZRemoteMezz *remoteMezz = [systemModel remoteMezzWithUid:uid];
      if (!remoteMezz)
      {
        NSLog(@"Error: This remote mezz should exist.");
        return;
      }

      [infopresence insertObject:remoteMezz inRoomsAtIndex:systemModel.infopresence.rooms.count];
    }
    else if ([type isEqual:@"remote-participant"])
    {
      if ([infopresence remoteParticipantWithUid:uid])
        return;

      MZRemoteParticipant *newParticipant = [[MZRemoteParticipant alloc] init];
      [newParticipant updateWithDictionary:p.ingests];
      [infopresence insertObject:newParticipant inRemoteParticipantsAtIndex:infopresence.remoteParticipants.count];
    }
	};
	return handler;
}


-(OBMessageHandler*) createInfopresenceParticipantLeaveHandler
{
	OBMessageHandler *handler = [OBMessageHandler messageHandler];
	handler.descrips = @[MZTransactionNameInfopresenceParticipantLeave];
	handler.handler = ^(OBProtein *p) {

    if (self.systemModel.featureToggles.participantRoster)
      return;

    NSString *uid = p.ingests[@"uid"];
    NSString *type = p.ingests[@"type"];	// 'room' or 'remote-participant'
    MZInfopresence *infopresence = systemModel.infopresence;

    if ([type isEqual:@"remote-participant"])
    {
      MZRemoteParticipant *participant = [infopresence remoteParticipantWithUid:uid];
      if (participant)
      {
        NSInteger index = [infopresence.remoteParticipants indexOfObject:participant];
        [infopresence removeRemoteParticipantsAtIndexes:[NSIndexSet indexSetWithIndex:index]];
      }
    }
    else if ([type isEqual:@"room"])
    {
      MZMezzanine *mezzanine = [infopresence mezzanineRoomWithUid:uid];
      if (mezzanine)
      {
        NSInteger index = [infopresence.rooms indexOfObject:mezzanine];
        [infopresence removeRoomsAtIndexes:[NSIndexSet indexSetWithIndex:index]];
      }
    }
	};
	return handler;
}


#pragma mark - Participant List

-(void) updateParticipants:(NSArray *)participants withMezzanine:(MZMezzanine *)mezzanine
{
  NSMutableArray *toRemove = [NSMutableArray new];
  NSMutableArray *toInsert = [NSMutableArray new];
  for (NSDictionary *participant in participants)
  {
    NSArray* alreadyPresent = [mezzanine participantsWithUid:participant[@"provenance"]];
    if (alreadyPresent.count == 0)
      [toInsert addObject:participant];
  }
  for (MZRemoteParticipant *participant in mezzanine.participants)
  {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.provenance == %@", participant.uid];
    NSArray* stillPresent = [participants filteredArrayUsingPredicate:predicate];

    if (stillPresent.count == 0)
      [toRemove addObject:participant];
  }

  for (MZParticipant *participant in toRemove)
  {
    [mezzanine removeObjectFromParticipants:participant];
  }

  for (NSDictionary *participantDict in toInsert)
  {
    MZParticipant *participant = [MZParticipant new];
    [participant updateWithDictionary:participantDict];
    [mezzanine insertObject:participant inParticipantsAtIndex:mezzanine.participants.count];
  }
}

-(OBMessageHandler *) createParticipantListHandler
{
  OBMessageHandler *participantListHandler = [OBMessageHandler messageHandler];
  participantListHandler.descrips = @[MZTransactionNameParticipantList];
  participantListHandler.handler = ^(OBProtein *p)
  {
    if (!self.systemModel.featureToggles.participantRoster)
      return;

    MZInfopresence *infopresence = self.systemModel.infopresence;

    // 1. Update local participants
    NSArray *localParticipants = p.ingests[@"local-room"][@"participants"];
    [self updateParticipants:localParticipants withMezzanine:self.systemModel.myMezzanine];

    // 2. Update remote rooms and their participants
    NSArray *remoteRooms = p.ingests[@"remote-rooms"];
    NSMutableArray *toRemove = [NSMutableArray new];
    NSMutableArray *toInsert = [NSMutableArray new];
    for (NSDictionary *remoteRoomDict in remoteRooms)
    {
      NSString *roomUID = remoteRoomDict[@"uid"];
      NSArray *roomParticipants = remoteRoomDict[@"participants"];

      MZMezzanine *mezz = [self.systemModel remoteMezzWithUid:roomUID];
      MZMezzanine *mezzInInfopresence = [infopresence mezzanineRoomWithUid:roomUID];

      if (mezz == mezzInInfopresence)
      {
        [self updateParticipants:roomParticipants withMezzanine:mezz];
      }
      else
      {
        [toInsert addObject:remoteRoomDict];
      }
    }

    for (MZMezzanine *remoteRoom in infopresence.rooms)
    {
      if (remoteRoom.roomType == MZMezzanineRoomTypeCloud)
        continue;

      NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.uid == %@", remoteRoom.uid];
      NSArray* stillPresent = [remoteRooms filteredArrayUsingPredicate:predicate];

      if (stillPresent.count == 0)
        [toRemove addObject:remoteRoom];
    }

    for (MZMezzanine *mezzanineToRemove in toRemove)
    {
      [mezzanineToRemove.participants removeAllObjects];
      [infopresence removeObjectFromRooms:mezzanineToRemove];
    }

    for (NSDictionary *remoteRoomDict in toInsert)
    {
      NSString *roomUID = remoteRoomDict[@"uid"];
      NSArray *roomParticipants = remoteRoomDict[@"participants"];
      MZMezzanine *mezz = [self.systemModel remoteMezzWithUid:roomUID];
      [self updateParticipants:roomParticipants withMezzanine:mezz];
      [infopresence insertObject:mezz inRoomsAtIndex:infopresence.countOfRooms];
    }

    // 3. Update remote participants from Mezz-In (a remote room with a special treatment)
    NSArray *mezzInParticipants = p.ingests[@"cloud-participants"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.roomType == %d", MZMezzanineRoomTypeCloud];
    NSArray* cloudInstances = [self.systemModel.remoteMezzes filteredArrayUsingPredicate:predicate];

    // For now there's just one cloud instance so we are safe using the first one
    if (cloudInstances.count)
    {
      MZMezzanine *mezzCloud = cloudInstances.firstObject;
      [self updateParticipants:mezzInParticipants withMezzanine:mezzCloud];
      if (![infopresence.rooms containsObject:mezzCloud])
        [infopresence insertObject:mezzCloud inRoomsAtIndex:infopresence.countOfRooms];
    }
    else
    {
      NSArray* oldcloudInstances = [infopresence.rooms filteredArrayUsingPredicate:predicate];
      MZMezzanine *mezzCloud = oldcloudInstances.firstObject;
      if ([infopresence.rooms containsObject:mezzCloud])
        [infopresence removeObjectFromRooms:mezzCloud];
      [mezzCloud.participants removeAllObjects];
    }

    // Finally, update the collaboration state of all remote mezzanines
    [self.systemModel updateRemoteMezzanineInfopresenceState];
  };

  return participantListHandler;
}


#pragma mark - Grabbed Handlers

-(OBMessageHandler*) createSlideGrabbedHandler
{
  OBMessageHandler *slideGrabbedHandler = [OBMessageHandler messageHandler];
  slideGrabbedHandler.descrips = @[@"slide-grabbed"];
  slideGrabbedHandler.filter = communicator.provenanceFilter;
  slideGrabbedHandler.handler = ^(OBProtein *p)
  {
    if ([p containsDescrip:@"error"])
    {
      // Lock grab failure should not be displayed to user as the resultant action failure
      // will inform the user with something more useful.
      //systemModel.popupMessage = lockGrabbingFailedMessage;
    }
  };
  return slideGrabbedHandler;
}


-(OBMessageHandler*) createShielderGrabHandler
{
  OBMessageHandler *shielderGrabHandler = [OBMessageHandler messageHandler];
  shielderGrabHandler.descrips = @[MZTransactionNameWindshieldItemGrab];
  shielderGrabHandler.filter = communicator.provenanceFilter;
  shielderGrabHandler.handler = ^(OBProtein *p)
  {
    if ([p containsDescrip:@"error"])
    {
      // Lock grab failure should not be displayed to user as the resultant action failure
      // will inform the user with something more useful.
      // In this case the shielder is snapped back to the original position.
    }
  };
  return shielderGrabHandler;
}


-(OBMessageHandler*) createShielderGrabbedHandler
{
  OBMessageHandler *shielderGrabbedHandler = [OBMessageHandler messageHandler];
  shielderGrabbedHandler.descrips = @[@"shielder-grabbed"];
  shielderGrabbedHandler.filter = communicator.provenanceFilter;
  shielderGrabbedHandler.handler = ^(OBProtein *p)
  {
    if ([p containsDescrip:@"error"])
    {
      // Lock grab failure should not be displayed to user as the resultant action failure
      // will inform the user with something more useful.
      //systemModel.popupMessage = lockGrabbingFailedMessage;
    }
  };
  return shielderGrabbedHandler;
}


#pragma mark - Refresh

-(OBMessageHandler*) createAssetRefreshHandler
{
  // As of 2.4, uploads cause the new-slide or new-asset to be sent out immediately instead of when
  // image is transferred, as such
  OBMessageHandler *refreshAssetImageHandler = [OBMessageHandler messageHandler];
  refreshAssetImageHandler.descrips = @[MZTransactionNameAssetRefresh];
  refreshAssetImageHandler.handler = ^(OBProtein *p)
  {
    NSString *assetUid = [p ingestForKey:@"content-source"];
    [communicator.uploader clearPendingUploadTransactionsForAssetUid:assetUid];
    
    // Find all slides that needs image refreshing
    NSArray *affectedSlides = [systemModel.currentWorkspace.presentation slidesWithContentSource:assetUid];
    for (MZPortfolioItem *slide in affectedSlides)
    {
      if (!slide.imageAvailable)
        slide.imageAvailable = YES;
    }
    
    NSArray *affectedShielders = [systemModel.currentWorkspace.windshield itemsWithContentSource:assetUid];
    for (MZWindshieldItem *item in affectedShielders)
    {
      if (!item.imageAvailable)
        item.imageAvailable = YES;
    }
  };
  return refreshAssetImageHandler;
}


#pragma mark - Upload

-(OBMessageHandler*) createAssetUploadProvisionHandler
{
  // Sometimes mezzanine would respond with an upload-images response (which clears the transaction)
  // but then follow it up with a timeout protein, which triggers the generic error handler below.
  // See bug 6283
  OBMessageHandler *uploadImagesHandler = [OBMessageHandler messageHandler];
  uploadImagesHandler.descrips = @[MZTransactionNameAssetUploadProvision, @"error"];
  uploadImagesHandler.filter = communicator.provenanceFilter;
  uploadImagesHandler.handler = ^(OBProtein *p)
  {
    if ([p containsDescrip:@"error"])
    {
      NSString *description = [p ingestForKey:@"description"];
      systemModel.popupMessage = description;
    }
  };
  return uploadImagesHandler;
}


-(OBMessageHandler*) createAssetUploadImageReadyHandler
{
  // Catch messages that occur during pygiandro's (native mezzanine's asset system)
  // consumption of an uploaded image
  OBMessageHandler *imageReadyHandler = [OBMessageHandler messageHandler];
  imageReadyHandler.descrips = @[MZTransactionNameAssetUploadImageReady, @"error"];
  imageReadyHandler.filter = communicator.provenanceFilter;
  imageReadyHandler.handler = ^(OBProtein *p)
  {
    systemModel.popupMessage = MZLocalizedString(@"MZHandlerFactory Error Image Upload", nil);
    NSArray *uids = [p ingestForKey:@"uids"];
    for (NSString *uid in uids)
    {
      NSLog(@"Upload of asset %@ resulted in an error", uid);
      [communicator.uploader removeTransactionForFailedUploadAssetUID:uid];
    }
  };
  return imageReadyHandler;
}



#pragma mark - Download and PDF

-(OBMessageHandler*) createAssetUploadPDFHandler
{
  OBMessageHandler *handler = [OBMessageHandler messageHandler];
  handler.descrips = @[@"pm",
                       @"asset-upload-pdf"];
  handler.handler = ^(OBProtein *p)
  {
    NSArray *fileids = [p ingestForKey:@"file-ids"];
    [communicator.uploader beginPDFUploadForFiles:fileids];
  };
  return handler;
}


#pragma mark - Error

-(OBMessageHandler*) createGeneralErrorHandler
{
  OBMessageHandler *errorHandler = [OBMessageHandler messageHandler];
  errorHandler.descrips = @[@"error"];
  errorHandler.filter = communicator.provenanceFilter;
  errorHandler.handler = ^(OBProtein *p)
  {
    // TODO - encapsulate errors into MZError class
    NSString *errorCode = [p ingestForKey:@"error-code"];
    if ([errorCode isEqual:@"system"] || [errorCode isEqual:@"workspace-uid-mismatch"])
      return; // Ignore 'system' errors, whatever they may be
    
    NSString *summary = [p ingestForKey:@"summary"];
    NSString *description = [p ingestForKey:@"description"];
    
    // Only show an alert view if there's some text to display
    if (summary || description)
    {
      if (systemModel.showError)
        systemModel.showError (summary, description);
    }
  };
  return errorHandler;
}

@end
