//
//  MZFeld.m
//  Mezzanine
//
//  Created by Zai Chang on 5/18/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import "MZFeld.h"
#import "NSObject+KVCAdditions.h"


@implementation MZFeld

@synthesize name;
@synthesize type;
@synthesize center;
@synthesize norm;
@synthesize over;
@synthesize up;
@synthesize physicalSize;
@synthesize pixelSize;
@synthesize viewDistance;
@synthesize visibleToAll;
@synthesize visibility;
@dynamic aspectRatio;


+(MZFeld *) feld
{
  return [[MZFeld alloc] init];
}


+(MZFeld *) feldWithName:(NSString*)aName
{
  MZFeld *feld = [[MZFeld alloc] init];
  feld.name = aName;
  return feld;
}


+(NSMutableArray*) orderedFeldsArrayFromDictionary:(NSDictionary *)dictionary
{
  return [self orderedFeldsArrayFromDictionary:dictionary includeInvisible:NO];
}


+(NSMutableArray*) orderedFeldsArrayFromDictionary:(NSDictionary *)dictionary includeInvisible:(BOOL)includeInvisible
{
  NSMutableArray *felds = [NSMutableArray arrayWithCapacity:dictionary.count];
  NSArray *feldNames = dictionary.allKeys;
  
  for (NSString *feldName in feldNames)
  {
    NSDictionary *feldInfo = dictionary[feldName];
    if (feldInfo)
    {
      MZFeld *feld = [MZFeld feldWithName:feldName];
      [feld updateWithDictionary:feldInfo];
      
      // Only insert feld if its an actual feld
      // Before mezz 3.20 felds type define its visibility with 'visible' (as opposed to 'mullion') or "invisible" on mixed-geom during m2m
      // 3.20 "visiblity" introduces [all, some, none] besides its type information

      BOOL shouldAddPrev320 = !feld.visibility && ([feld.type isEqualToString:@"visible"] || (includeInvisible && [feld.type isEqualToString:@"invisible"]));
      BOOL shouldAddAfter320 = [feld.visibility isEqualToString:@"all"] || [feld.visibility isEqualToString:@"some"];

      if (shouldAddPrev320 || shouldAddAfter320)
      {
        [felds addObject:feld];
      }
    }
  }
  
  // Sort felds using x coordinates to ensure left-main-right order
  [felds sortUsingComparator:^(MZFeld *feld1, MZFeld *feld2) {
    if (feld1.center.x.doubleValue < feld2.center.x.doubleValue)
      return NSOrderedAscending;
    else if (feld1.center.x.doubleValue > feld2.center.x.doubleValue)
      return NSOrderedDescending;
    return NSOrderedSame;
  }];
  
  return felds;
}


-(void) updateWithDictionary:(NSDictionary *)dictionary
{
  [self setValueIfNotNullAndChanged:dictionary[@"type"] forKey:@"type"];
  [self setValueIfNotNullAndChanged:dictionary[@"cent"] forKey:@"center"];
  [self setValueIfNotNullAndChanged:dictionary[@"norm"] forKey:@"norm"];
  [self setValueIfNotNullAndChanged:dictionary[@"over"] forKey:@"over"];
  [self setValueIfNotNullAndChanged:dictionary[@"up"] forKey:@"up"];
  [self setValueIfNotNullAndChanged:dictionary[@"phys-size"] forKey:@"physicalSize"];
  [self setValueIfNotNullAndChanged:dictionary[@"px-size"] forKey:@"pixelSize"];
  [self setValueIfNotNullAndChanged:dictionary[@"view-dist"] forKey:@"viewDistance"];

  self.visibleToAll = [dictionary[@"visible-to-all"] boolValue];
  [self setValueIfNotNullAndChanged:dictionary[@"visibility"] forKey:@"visibility"];

  // TODO: FIX ME with incoming MZProto generated code
  if ([dictionary[@"cent"] isKindOfClass:[MZVect class]] ||
      [dictionary[@"norm"] isKindOfClass:[MZVect class]] ||
      [dictionary[@"over"] isKindOfClass:[MZVect class]] ||
      [dictionary[@"up"] isKindOfClass:[MZVect class]] ||
      [dictionary[@"phys-size"] isKindOfClass:[MZVect class]] ||
      [dictionary[@"px-size"] isKindOfClass:[MZVect class]])
    return;
  
  // Center
  NSArray *centArray = dictionary[@"cent"];
  if (centArray && [centArray count] > 0) {
    self.center = [[MZVect alloc] initWithX:centArray[0] y:centArray[1] z:centArray[2]];
  }
  
  // Norm
  NSArray *normArray = dictionary[@"norm"];
  if (normArray && [normArray count] > 0) {
    self.norm = [[MZVect alloc] initWithX:normArray[0] y:normArray[1] z:normArray[2]];
  }
  
  // Over
  NSArray *overArray = dictionary[@"over"];
  if (overArray && [overArray count] > 0) {
    self.over = [[MZVect alloc] initWithX:overArray[0] y:overArray[1] z:overArray[2]];
  }
  
  // Up
  NSArray *upArray = dictionary[@"up"];
  if (upArray && [upArray count] > 0) {
    self.up = [[MZVect alloc] initWithX:upArray[0] y:upArray[1] z:upArray[2]];
  }
  
  // PhysicalSize
  NSArray *physicalSizeArray = dictionary[@"phys-size"];
  if (physicalSizeArray && [physicalSizeArray count] > 0) {
    self.physicalSize = [[MZVect alloc] initWithX:physicalSizeArray[0] y:physicalSizeArray[1]];
  }
  
  // PixelSize
  NSArray *pixelSizeArray = dictionary[@"px-size"];
  if (pixelSizeArray && [pixelSizeArray count] > 0) {
    self.pixelSize = [[MZVect alloc] initWithX:pixelSizeArray[0] y:pixelSizeArray[1]];
  }
}

-(void) updateWithFeld:(MZFeld *)feld
{
  [self setValueIfNotNullAndChanged:feld.type forKey:@"type"];
  [self setValueIfNotNullAndChanged:feld.center forKey:@"center"];
  [self setValueIfNotNullAndChanged:feld.over forKey:@"over"];
  [self setValueIfNotNullAndChanged:feld.up forKey:@"up"];
  [self setValueIfNotNullAndChanged:feld.physicalSize forKey:@"physicalSize"];
  [self setValueIfNotNullAndChanged:feld.pixelSize forKey:@"pixelSize"];
  self.viewDistance = feld.viewDistance;
  self.visibleToAll = feld.visibleToAll;
  [self setValueIfNotNullAndChanged:feld.visibility forKey:@"visibility"];
}


-(double) aspectRatio
{
  return pixelSize.x.doubleValue / pixelSize.y.doubleValue;
}


-(CGRect) feldRect
{
  CGRect feldRect = CGRectMake([center doubleVectDot:over] - physicalSize.x.doubleValue / 2,
                               [center doubleVectDot:up] - physicalSize.y.doubleValue/2,
                               physicalSize.x.doubleValue,
                               physicalSize.y.doubleValue);
  return feldRect;
}

@end
