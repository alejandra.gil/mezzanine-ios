//
//  MZTransaction.m
//  Mezzanine
//
//  Created by Zai Chang on 2/8/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import "MZTransaction.h"
#import "MZCommunicator.h"
#import "OBProtein+Mezzanine.h"


@implementation MZTransaction

@synthesize thumbnail;
@synthesize title;
@synthesize status;
@synthesize isContinuous;

-(id) initWithProtein:(OBProtein*)protein
{
  self = [super init];
  if (self)
  {
    self.requestProtein = protein;
    self.transactionId = protein.transactionId;
    self.isContinuous = NO;
  }
  return self;
}

+(id) transactionWithProtein:(OBProtein*)protein
{
  MZTransaction *transaction = [(MZTransaction*)[[self class] alloc] initWithProtein:protein];
  transaction.requestPoolName = MEZZ_REQUEST_POOL;
  return transaction;
}


-(NSString*) description
{
  return [NSString stringWithFormat:@"MZTransaction id=%@ name=%@", self.transactionId, self.transactionName];
}


// Overriding OBTransacton's -handleProtein method to add in mezz specific
// success / error
-(BOOL) handleProtein:(OBProtein *)protein
{
  BOOL handled = [super handleProtein:protein];
  
  // Checking if this is an error response
  // This is done in MZTransaction level instead of OBTransaction because
  // other apps may not use the same descrip for an error protein
  if (handled)
  {
    if (([protein containsDescrip:@"error"] || [protein containsDescrip:@"unknown-request"]) && self.failedBlock)
    {
      NSDictionary *userInfo = protein.ingests;
      NSError *error = [NSError errorWithDomain:MZTransactionErrorDomain code:-1 userInfo:userInfo];
      self.failedBlock(error);
    }
    else if (self.successBlock)
      self.successBlock();
  }
  
  return handled;
}

@end



@implementation MZURLTransaction

@synthesize task;

@end



@implementation MZFileBatchUploadRequestTransaction

+(MZFileBatchUploadRequestTransaction *) transactionWithProtein:(OBProtein*)protein
{
  MZFileBatchUploadRequestTransaction *transaction = [[MZFileBatchUploadRequestTransaction alloc] initWithProtein:protein];
  transaction.transactionName = MZFilesUploadRequestTransactionName;
  transaction.requestPoolName = MEZZ_REQUEST_POOL;
  transaction.descrips = @[MZFilesUploadRequestTransactionName];
  transaction.consumesProtein = YES;
  transaction.transactionsInProgress = [[NSMutableArray alloc] init];
  return transaction;
}


-(BOOL) completed
{
  for (MZFileUploadInfo *uploadInfo in self.fileUploadInfos)
  {
    if (!uploadInfo.completed)
    {
      // If any one upload is not completed, the overall transaction is not completed
      return NO;
    }
  }
  
  return YES;
}

@end



@implementation MZImageUploadRequestTransaction

@synthesize format;
@synthesize uid;
@synthesize numberOfImages;

+(MZImageUploadRequestTransaction *) transactionWithProtein:(OBProtein*)protein
{
  MZImageUploadRequestTransaction *transaction = [[MZImageUploadRequestTransaction alloc] initWithProtein:protein];
  transaction.transactionName = MZImageUploadRequestTransactionName;
  transaction.requestPoolName = MEZZ_REQUEST_POOL;
  transaction.descrips = @[@"upload-images"];
  transaction.consumesProtein = YES;
  return transaction;
}

@end



@implementation MZImageBatchUploadRequestTransaction

@synthesize completedImageUploads;
@synthesize transactionsInProgress;
@synthesize target;

+(MZImageBatchUploadRequestTransaction *) transactionWithProtein:(OBProtein*)protein
{
  MZImageBatchUploadRequestTransaction *transaction = [[MZImageBatchUploadRequestTransaction alloc] initWithProtein:protein];
  transaction.transactionName = MZImageUploadRequestTransactionName;
  transaction.requestPoolName = MEZZ_REQUEST_POOL;
  transaction.descrips = @[@"upload-images"];
  transaction.consumesProtein = YES;
  transaction.transactionsInProgress = [[NSMutableArray alloc] init];
  return transaction;
}

@end



@implementation MZImageUploadTransaction

@synthesize format;
@synthesize uid;
@synthesize uploadAsSlide;
@synthesize uploadAsAsset;

+(MZImageUploadTransaction *) transactionWithProtein:(OBProtein*)protein
{
  MZImageUploadTransaction *transaction = [[MZImageUploadTransaction alloc] initWithProtein:protein];
  transaction.transactionName = MZImageUploadTransactionName;
  transaction.requestPoolName = MEZZ_IMAGE_DEPOSIT_POOL;
  
  return transaction;
}

@end
