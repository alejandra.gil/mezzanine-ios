//
//  MZDiskImageOperation.m
//  MezzKit
//
//  Created by Ivan Bella Lopez on 09/06/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

#import "MZDiskImageOperation.h"
#import "MZAssetCache.h"

@interface MZDiskImageOperation ()

typedef void (^URLImageFromDiskDoneBlock)(MZImage *image);

@property (nonatomic, strong, readwrite, nonnull) id target;
@property (nonatomic, strong, readwrite, nullable) NSString *cacheKey;
@property (nonatomic, copy, nullable) URLImageFromDiskDoneBlock doneBlock;

@end


@implementation MZDiskImageOperation

+ (MZDiskImageOperation*)operationWithTarget:(id)aTarget cacheKey:(NSString *)key completion:(URLImageFromDiskDoneBlock)completionBlock;
{
  return [[MZDiskImageOperation alloc] initWithTarget:aTarget cacheKey:key completion:completionBlock];
}


- (id)initWithTarget:(id)aTarget cacheKey:(NSString *)key completion:(URLImageFromDiskDoneBlock)completionBlock
{
  self = [super init];
  if (self)
  {
    _target = aTarget;
    _cacheKey = key;
    _doneBlock = completionBlock;
  }
  
  return self;
}


- (NSString *)description
{
  return [NSString stringWithFormat:@"<%@: %p, cancelled: %@ cacheKey: %@, target: %@>", NSStringFromClass([self class]), self, ([self isCancelled] ? @"YES" : @"NO"), self.cacheKey, [NSValue valueWithNonretainedObject:self.target]];
}


- (void)main
{
  @autoreleasepool
  {
    if ([self isCancelled])
      return;
    
    MZAssetCache *diskCache = [MZAssetCache sharedCache];
    NSString *filepathInCache = [diskCache cachePathForKey:_cacheKey];
    MZImage *cachedImage = [[MZImage alloc] initWithContentsOfFile:filepathInCache];
    
    if (cachedImage)
    {
      if ([self isCancelled])
        return;
      
      [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        _doneBlock(cachedImage);
      }];
    }
    else
    {
      [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        _doneBlock(nil);
      }];
    }
  }
}

@end