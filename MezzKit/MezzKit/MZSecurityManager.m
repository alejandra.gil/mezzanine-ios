//
//  MZSecurityManager.m
//  MezzKit
//
//  Created by miguel on 10/01/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import "MZSecurityManager.h"

@implementation MZSecurityManager

#pragma mark - Authentication Processes

- (BOOL)authenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace
{
  return [self authenticateAgainstServerTrust:protectionSpace.serverTrust host:protectionSpace.host];
}

- (BOOL)authenticateAgainstServerTrust:(SecTrustRef)serverTrust host:(NSString *)host
{
  CFMutableArrayRef newAnchorArray = [self loadRootCAs];

  NSMutableArray *policies = [NSMutableArray array];
  [policies addObject:(__bridge_transfer id)SecPolicyCreateSSL(true, (__bridge CFStringRef)host)];
  SecTrustSetPolicies(serverTrust, (__bridge CFArrayRef)policies);

  SecTrustSetAnchorCertificates(serverTrust, newAnchorArray);
  SecTrustSetAnchorCertificatesOnly(serverTrust, false);

  return [self trustIsValid:serverTrust host:host];;
}


- (NSURLCredential *)credentialsFromAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
  NSURLCredential *credential = nil;
  if ([self authenticateAgainstProtectionSpace:challenge.protectionSpace])
  {
    credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
  }

  return credential;
}

- (CFMutableArrayRef)loadRootCAs
{
  // Load Oblong certificate authorities PEM file and convert it to data
  NSBundle *bundle = [NSBundle bundleForClass:[self class]];
  NSString *pemPath = [bundle pathForResource:@"ob_root_ca" ofType:@"pem"];

  return [self loadCertificatesFromFilePath:pemPath];
}

- (CFMutableArrayRef)loadCertificatesFromFilePath:(NSString *)pemPath
{
  // PEM files can contain multiple certificates so we need parse the file
  // and analize each of them to add them individually to the trust store that the OS uses.
  NSString* fileContents = [NSString stringWithContentsOfFile:pemPath encoding:NSUTF8StringEncoding error:nil];

  // first, clean up contents before the beginning of the certificate and get the number of certificates
  NSArray* allLinedStrings = [fileContents componentsSeparatedByString:@"-----BEGIN CERTIFICATE-----\n"];
  NSString *endCertificateString = @"-----END CERTIFICATE-----\n";
  NSArray* certificates = [allLinedStrings filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self CONTAINS[cd] %@", endCertificateString]];

  CFMutableArrayRef certsArray = CFArrayCreateMutable (kCFAllocatorDefault, certificates.count, &kCFTypeArrayCallBacks);
  for (NSString *certString in certificates)
  {
    // Strip out the header "BEGIN" and the tail "END" of the base64 enconded certificate
    NSString *cleanPEMcertificateString = @"";
    cleanPEMcertificateString = [cleanPEMcertificateString stringByAppendingFormat:@"%@",
                                 [certString componentsSeparatedByString:@"-----END CERTIFICATE-----"][0]];

    // Use the data representation of the string removing characters (/n) to decode the string to NSData
    NSData *pemCertStringData = [cleanPEMcertificateString dataUsingEncoding:NSUTF8StringEncoding];
    NSData *dataCert = [[NSData alloc] initWithBase64EncodedData:pemCertStringData options:NSDataBase64DecodingIgnoreUnknownCharacters];

    // Convert the data to a certificate reference
    SecCertificateRef newAnchorRef = SecCertificateCreateWithData(kCFAllocatorDefault, (__bridge CFDataRef)dataCert);

    if (newAnchorRef != nil)
      CFArrayAppendValue(certsArray, newAnchorRef);
    else
      NSLog(@"Error: Could not generate certificate from:\n%@", cleanPEMcertificateString);
  }

  return certsArray;
}

- (BOOL)trustIsValid:(SecTrustRef)serverTrust host:(NSString *)host
{
  BOOL isValid = NO;

  SecTrustResultType res = kSecTrustResultInvalid;
  OSStatus status = SecTrustEvaluate(serverTrust, &res);

  if (status == errSecSuccess)
  {
    // Condition 1 added to bypass 825 day validity error in iOS 13, info below
    // https://mezzanine.tpondemand.com/entity/24047-ios-13-server-certificate-issue
    // ValidityPeriodMaximum error falls under broad category of RecoverableTrustFailures
    if (res == kSecTrustResultRecoverableTrustFailure)
    {
      DLog(@"iOS13 certificate validity bypass initiated");
      // Copy the result as a dictionary
      CFDictionaryRef cfTrustResults = SecTrustCopyResult(serverTrust);
      // Cast to a NS dictionary to access nested key:value as regular dictionary
      NSDictionary *nsTrustResults = (__bridge NSDictionary*)cfTrustResults;
      // If it is recoverable error and if the error is validityPeriodMaximum (>825 days certificate)
      if (([nsTrustResults[@"TrustResultDetails"][0] count] == 1) && (nsTrustResults[@"TrustResultDetails"][0][@"ValidityPeriodMaximums"]))
      {
        isValid = YES;
      }
      else {
        DLog(@"iOS certificate chain validation for host %@ failed", host);
      }
    }
    else if ((res == kSecTrustResultProceed) || (res == kSecTrustResultUnspecified))
    {
      DLog(@"iOS certificate chain validation for host %@ passed", host);
      isValid = YES;
    }
    else
    {
      DLog(@"iOS certificate chain validation for host %@ failed", host);
    }
  }
  

  return isValid;
}

@end
