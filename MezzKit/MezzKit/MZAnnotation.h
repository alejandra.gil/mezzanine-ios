//
//  MZAnnotation.h
//  Mezzanine
//
//  Created by Zai Chang on 10/22/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//


#import "MZClassTargetDefinitions.h"


/// Model object for an annotation.
///
@interface MZAnnotation : NSObject <NSCopying, CALayerDelegate>

@property (nonatomic, strong) MZColor *fillColour;
@property (nonatomic, strong) MZColor *strokeColour;
@property (nonatomic, assign) CGFloat strokeWidth;

// Volatile properties, should not be serialized
@property (nonatomic, assign) BOOL needsRedraw;

@end



@interface MZFreehandAnnotation : MZAnnotation

@property (nonatomic, strong) NSMutableArray *points;

@end



@interface MZTextAnnotation : MZAnnotation

@property (nonatomic, assign) CGPoint position;

@end



@interface MZArrowAnnotation : MZAnnotation

@property (nonatomic, assign) CGPoint startPoint;
@property (nonatomic, assign) CGPoint endPoint;

@end
