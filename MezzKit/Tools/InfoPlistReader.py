#!/usr/bin/python


import sys, plistlib

class InfoPlistReader:
	
	bundle_identifier_key = "CFBundleIdentifier"
	short_version_key = "CFBundleShortVersionString"
	version_key = "CFBundleVersion"
	
	def __init__(self, filename):	
		self.dict = plistlib.readPlist(filename)
	
	def valueForKey(self, key):
		return self.dict[key]
	
	
	

if __name__ == "__main__":
		
	from optparse import OptionParser
	
	usage = "usage: %prog [options] plistfile"
	parser = OptionParser(usage=usage)
	parser.add_option("-k", "--key",
					  action="store", type="string",
					  help="key to extract from info.plist")
	parser.add_option("-i", "--bundle-identifier",
					  action="store_true",
					  help="display bundle identifier from info.plist")
	parser.add_option("-v", "--version",
					  action="store_true",
					  help="display version from info.plist")
	parser.add_option("-s", "--short-version",
					  action="store_true",
					  help="display short version from info.plist")
	
	(options, args) = parser.parse_args()
	
	if len(args) == 0:
		print "No input filename"
		sys.exit(0)
	
	reader = InfoPlistReader(args[0])
	
	if options.key:
		if reader.dict.has_key(options.key):
			print reader.valueForKey(options.key)
		#else:
		#	print "Info plist does not contain key %s" % (options.key)
	elif options.bundle_identifier:
		print reader.valueForKey(reader.bundle_identifier_key)
		
	elif options.version:
		print reader.valueForKey(reader.version_key)
		
	elif options.short_version:
		print reader.valueForKey(reader.short_version_key)
	