//
//  MZWorkspace_Tests.m
//  MezzKit
//
//  Created by Zai Chang on 11/25/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MezzKit.h"

@interface MZWorkspace_Tests : XCTestCase
{
  MZWorkspace *_workspace;
}
@end

@implementation MZWorkspace_Tests


- (NSString*)liveStreamUidForIndex:(NSInteger)index
{
  return [NSString stringWithFormat:@"vi-%ld/_local_", (long)index];
}

- (void)setUp {
    [super setUp];

  _workspace = [MZWorkspace new];
  _workspace.thumbURL = @"http://localhost/someimage.gif";

  for (NSInteger i=0; i<5; i++)
  {
    MZLiveStream *liveStream = [[MZLiveStream alloc] init];
    liveStream.uid = [self liveStreamUidForIndex:i];
    [_workspace.liveStreams addObject:liveStream];
  }
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}


- (void)testSetPresentation {
  MZPresentation *presentation = [MZPresentation new];
  _workspace.presentation = presentation;
  XCTAssertEqual(presentation.workspace, _workspace);
}


- (void)testModifiedDate {
  _workspace.modifiedDateString = nil;

  _workspace.modifiedDate = [NSDate date];
  
  XCTAssertNotNil(_workspace.modifiedDateString);
}


- (void)testDynamicThumbURL {
  _workspace.modifiedDate = [NSDate dateWithTimeIntervalSinceNow:-10];
  NSString *oldThumbURL = _workspace.dynamicThumbURL;

  _workspace.modifiedDate = [NSDate date];
  NSString *newThumbURL = _workspace.dynamicThumbURL;
  
  XCTAssertNotEqualObjects(newThumbURL, oldThumbURL);
}

- (void)testDynamicThumbURLWithModifiedDateString {
  _workspace.modifiedDate = [NSDate dateWithTimeIntervalSince1970:1318834800];
  _workspace.modifiedDate = nil; // To force using modifiedDateString
  NSString *dateWithModifiedDateStringURL = _workspace.dynamicThumbURL;
  NSString *stringForDate171020119AM = @"http://localhost/someimage.gif?q=17/10/11%2009:00";
  XCTAssertTrue([stringForDate171020119AM isEqualToString:dateWithModifiedDateStringURL]);
}


- (void)testLiveStreamWithUid {
  XCTAssertNotNil([_workspace liveStreamWithUid:[self liveStreamUidForIndex:0]]);
  XCTAssertNotNil([_workspace liveStreamWithUid:[self liveStreamUidForIndex:1]]);
  XCTAssertNotNil([_workspace liveStreamWithUid:[self liveStreamUidForIndex:2]]);
  XCTAssertNotNil([_workspace liveStreamWithUid:[self liveStreamUidForIndex:3]]);
  XCTAssertNotNil([_workspace liveStreamWithUid:[self liveStreamUidForIndex:4]]);
  XCTAssertNil([_workspace liveStreamWithUid:[self liveStreamUidForIndex:5]]);
}


- (void)testRemoveLiveStreams {
  [_workspace removeAllLiveStreams];
  XCTAssertEqual(_workspace.liveStreams.count, 0);
  XCTAssertNil([_workspace liveStreamWithUid:[self liveStreamUidForIndex:1]]);
}


- (void)testUpdateWithDictionary {
  // TODO
}


- (void)testUpdateLiveStreamsWithDictionaryAndOneLiveStreamLess {
  // TODO

  NSMutableArray *liveStreams = @[].mutableCopy;
  for (NSInteger i=0; i<4; i++)
  {
    NSDictionary *liveStreamDict = @{@"source-id": [self liveStreamUidForIndex:i]};
    [liveStreams addObject:liveStreamDict];
  }

  NSDictionary *workspaceUpdateDictionary = @{@"live-streams": liveStreams};
  [_workspace updateLiveStreamsWithDictionary:workspaceUpdateDictionary];

  MZLiveStream *lastLiveStream = [_workspace liveStreamWithUid:[self liveStreamUidForIndex:4]];
  XCTAssertNotNil(lastLiveStream, @"The missing live stream should exist and be deactivated");
  XCTAssertFalse(lastLiveStream.active, @"The missing live stream should exist and be deactivated");
}


- (void)testUpdateLiveStreamsWithDictionaryWithANewLiveStream
{
  NSMutableArray *liveStreams = @[].mutableCopy;
  for (NSInteger i=0; i<6; i++)
  {
    NSDictionary *liveStreamDict = @{@"source-id": [self liveStreamUidForIndex:i],
                                     @"active": @"false"};
    [liveStreams addObject:liveStreamDict];
  }

  NSDictionary *workspaceUpdateDictionary = @{@"live-streams": liveStreams};

  XCTAssertEqual(_workspace.liveStreams.count, 5, @"There should be 5 live streams but there are %ld", (long)_workspace.liveStreams.count);
  [_workspace updateLiveStreamsWithDictionary:workspaceUpdateDictionary];
  XCTAssertEqual(_workspace.liveStreams.count, 6, @"There should be 6 live streams but there are %ld", (long)_workspace.liveStreams.count);
}


- (void)testUpdateLiveStreamsWithDictionaryInADifferentOrder
{
  NSArray *newPositions = @[@2, @0, @4, @3, @1];
  NSMutableArray *liveStreams = @[].mutableCopy;

  for (NSNumber *ixNumber in newPositions)
  {
    NSDictionary *liveStreamDict = @{@"source-id": [self liveStreamUidForIndex:ixNumber.integerValue],
                                     @"active": @"true"};
    [liveStreams addObject:liveStreamDict];
  }

  NSDictionary *workspaceUpdateDictionary = @{@"live-streams": liveStreams};

  // Check all live streams are in the original order
  XCTAssertEqual([_workspace liveStreamWithUid:[self liveStreamUidForIndex:0]], _workspace.liveStreams[0]);
  XCTAssertEqual([_workspace liveStreamWithUid:[self liveStreamUidForIndex:1]], _workspace.liveStreams[1]);
  XCTAssertEqual([_workspace liveStreamWithUid:[self liveStreamUidForIndex:2]], _workspace.liveStreams[2]);
  XCTAssertEqual([_workspace liveStreamWithUid:[self liveStreamUidForIndex:3]], _workspace.liveStreams[3]);
  XCTAssertEqual([_workspace liveStreamWithUid:[self liveStreamUidForIndex:4]], _workspace.liveStreams[4]);

  [_workspace updateLiveStreamsWithDictionary:workspaceUpdateDictionary];

  // Check all live streams uids are the same than the one generated with the index values of the new order
  XCTAssertEqualObjects([self liveStreamUidForIndex:[newPositions[0] integerValue]], [_workspace.liveStreams[0] uid]);
  XCTAssertEqualObjects([self liveStreamUidForIndex:[newPositions[1] integerValue]], [_workspace.liveStreams[1] uid]);
  XCTAssertEqualObjects([self liveStreamUidForIndex:[newPositions[2] integerValue]], [_workspace.liveStreams[2] uid]);
  XCTAssertEqualObjects([self liveStreamUidForIndex:[newPositions[3] integerValue]], [_workspace.liveStreams[3] uid]);
  XCTAssertEqualObjects([self liveStreamUidForIndex:[newPositions[4] integerValue]], [_workspace.liveStreams[4] uid]);

  // Check all live streams have been reorder to the new position determined by the order in the array of live-stream dictionaries
  XCTAssertEqual([_workspace liveStreamWithUid:[self liveStreamUidForIndex:[newPositions[0] integerValue]]], _workspace.liveStreams[0]);
  XCTAssertEqual([_workspace liveStreamWithUid:[self liveStreamUidForIndex:[newPositions[1] integerValue]]], _workspace.liveStreams[1]);
  XCTAssertEqual([_workspace liveStreamWithUid:[self liveStreamUidForIndex:[newPositions[2] integerValue]]], _workspace.liveStreams[2]);
  XCTAssertEqual([_workspace liveStreamWithUid:[self liveStreamUidForIndex:[newPositions[3] integerValue]]], _workspace.liveStreams[3]);
  XCTAssertEqual([_workspace liveStreamWithUid:[self liveStreamUidForIndex:[newPositions[4] integerValue]]], _workspace.liveStreams[4]);
}


- (void)testUpdateLiveStreamsWithDictionaryAndRemoteMezzes
{
  NSDictionary *liveStreamDict = @{@"content-source": @"vi-0/_local_",
                                   @"display-name": @"A Test Name"};

  XCTAssertEqual(_workspace.liveStreams.count, 5, @"There should be 5 live streams");

  [_workspace updateLiveStreamsWithDictionary:liveStreamDict andRemoteMezzes:nil];

  XCTAssertEqual(_workspace.liveStreams.count, 5, @"There should still be 5 live streams");

  liveStreamDict = @{@"content-source": @"vi-6/_local_",
                     @"display-name": @"A Test Name"};

  [_workspace updateLiveStreamsWithDictionary:liveStreamDict andRemoteMezzes:nil];

  XCTAssertEqual(_workspace.liveStreams.count, 6, @"There should be 6 live streams");
}


- (void)testUpdateLocalLiveStreamNameWithRemoteMezzName
{
  MZLiveStream *liveStream = [[MZLiveStream alloc] init];
  liveStream.uid = [self liveStreamUidForIndex:50];
  liveStream.remoteMezzName = nil;

  MZRemoteMezz *remoteMezz = [[MZRemoteMezz alloc] init];
  remoteMezz.uid = @"remote-mezz-1234";
  remoteMezz.name = @"The Remote Mezz";

  [_workspace updateLiveStreamRemoteMezzName:liveStream usingRemoteMezzes:@[remoteMezz]];

  XCTAssertNil(liveStream.remoteMezzName, @"Local live streams should not have a remote mezz name");
}


- (void)testUpdateRemoteLiveStreamNameWithRemoteMezzName
{
  MZLiveStream *liveStream = [[MZLiveStream alloc] init];
  liveStream.uid = @"vi-1/remote-mezz-1234";
  liveStream.remoteMezzName = nil;

  MZRemoteMezz *remoteMezz = [[MZRemoteMezz alloc] init];
  remoteMezz.uid = @"remote-mezz-1234";
  remoteMezz.name = @"The Remote Mezz";

  [_workspace updateLiveStreamRemoteMezzName:liveStream usingRemoteMezzes:@[remoteMezz]];

  XCTAssertEqual(liveStream.remoteMezzName, remoteMezz.name, @"The remote live stream should be named as its remote Mezz %@", remoteMezz.name);
}


- (void)testRemoveRemoteLiveStreamWithContentSource
{
  XCTAssertEqual(_workspace.liveStreams.count, 5, @"The test should start with 5 live streams");

  NSString *remoteLiveStreamUID = @"vi-1/remote-mezz-1234";
  MZLiveStream *liveStream = [[MZLiveStream alloc] init];
  liveStream.uid = remoteLiveStreamUID;
  liveStream.remoteMezzName = nil;
  [_workspace.liveStreams addObject:liveStream];

  MZWindshieldItem *windshieldItem = [MZWindshieldItem new];
  windshieldItem.uid = @"el-1";
  windshieldItem.contentSource = remoteLiveStreamUID;
  [_workspace.windshield.items addObject:windshieldItem];

  MZPortfolioItem *portfolioItem = [MZPortfolioItem new];
  portfolioItem.uid = @"sl-1";
  portfolioItem.contentSource = remoteLiveStreamUID;
  [_workspace.presentation.slides addObject:portfolioItem];

  XCTAssertEqual(_workspace.liveStreams.count, 6, @"There should be 6 live streams now.");

  [_workspace removeRemoteLiveStreamsWithContentSource:@"livestream-not-valid-content-source"];
  XCTAssertEqual(_workspace.liveStreams.count, 6, @"There should still be 6 live streams, content source not named as livestreams should not affect");

  [_workspace removeRemoteLiveStreamsWithContentSource:@"vi-0/_local_"];
  XCTAssertEqual(_workspace.liveStreams.count, 6, @"There should still be 6 live streams, local streams are not removed");

  [_workspace removeRemoteLiveStreamsWithContentSource:@"vi-vtc/_vtc_"];
  XCTAssertEqual(_workspace.liveStreams.count, 6, @"There should still be 6 live streams, VTC stream is not removed");

  [_workspace removeRemoteLiveStreamsWithContentSource:remoteLiveStreamUID];
  XCTAssertEqual(_workspace.liveStreams.count, 6, @"There should still be 6 live streams, remote stream should not be removed because there are items using it");

  [_workspace.windshield.items removeObject:windshieldItem];
  [_workspace removeRemoteLiveStreamsWithContentSource:remoteLiveStreamUID];
  XCTAssertEqual(_workspace.liveStreams.count, 6, @"There should still be 6 live streams, remote stream should not be removed because there are items using it");

  [_workspace.presentation.slides removeObject:portfolioItem];
  [_workspace removeRemoteLiveStreamsWithContentSource:remoteLiveStreamUID];
  XCTAssertEqual(_workspace.liveStreams.count, 5, @"There should be 5 live streams, remote stream should be removed no item is using it.");
}


- (void)testHasContent
{
  XCTAssertFalse([_workspace hasContent], @"Workspace should have no content yet");
  [_workspace.windshield.items addObject:[MZWindshieldItem new]];
  XCTAssertTrue([_workspace hasContent], @"Workspace should have some content (windshield)");
  [_workspace.presentation.slides addObject:[MZPortfolioItem new]];
  XCTAssertTrue([_workspace hasContent], @"Workspace should still have some content (windshield+portfolio)");
  [_workspace.windshield.items removeAllObjects];
  XCTAssertTrue([_workspace hasContent], @"Workspace should still have some content (portfolio)");
  [_workspace.presentation.slides removeAllObjects];
  XCTAssertFalse([_workspace hasContent], @"Workspace should have no content anymore");
}


@end
