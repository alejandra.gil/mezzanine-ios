//
//  MZSurface_Tests.m
//  MezzKit
//
//  Created by Miguel Sánchez Valdés on 05/10/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MezzKit.h"
#import "MockProteinSource.h"

@interface MZSurface_Tests : XCTestCase
{
  MZSurface *_surface;
  id<MockProteinSourceBaseProtocol> _proteinSource;

}

@end

@implementation MZSurface_Tests

- (void)setUp
{
  [super setUp];
  
  _proteinSource = [[MockProteinSource alloc] init];
}

- (void)tearDown
{
  [super tearDown];
}

- (NSDictionary *)setupSurfaceInfo
{
  NSDictionary *leftFeldDict = [_proteinSource parameterForKey:kCanonicalParameterFeldLeftKey];
  NSDictionary *mainFeldDict = [_proteinSource parameterForKey:kCanonicalParameterFeldMainKey];
  NSDictionary *rightFeldDict = [_proteinSource parameterForKey:kCanonicalParameterFeldRightKey];
  NSDictionary *boundingFeldDict = [_proteinSource parameterForKey:kCanonicalParameterBoundingFeldKey];

  NSDictionary *surfaceDict = @{@"name": @"surface",
                                @"bounding-feld": boundingFeldDict,
                                @"felds": @[leftFeldDict, mainFeldDict, rightFeldDict]};

  _surface = [[MZSurface alloc] initWithName:surfaceDict[@"surface"]];

  return surfaceDict;
}

- (void)testInitSurface
{
  NSDictionary *surfaceDict = [self setupSurfaceInfo];
  XCTAssertTrue(0 == _surface.felds.count, @"Surface should have no feld but has %lu", (unsigned long)_surface.felds.count);

  [_surface updateWithDictionary:surfaceDict];

  XCTAssertTrue(3 == _surface.felds.count, @"Surface should have 3 felds but has %lu", (unsigned long)_surface.felds.count);
}

- (void)testFeldWithNameExists
{
  NSDictionary *surfaceDict = [self setupSurfaceInfo];
  [_surface updateWithDictionary:surfaceDict];
  XCTAssertNotNil([_surface feldWithName:@"main"], @"Surface should contain a feld named 'main'");
}

- (void)testFeldWithNameDoesNotExist
{
  NSDictionary *surfaceDict = [self setupSurfaceInfo];
  [_surface updateWithDictionary:surfaceDict];
  XCTAssertNil([_surface feldWithName:@"non-existent-feld"], @"Surface should not contain a feld named 'non-existent-feld'");
}



@end
