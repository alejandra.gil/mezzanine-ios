//
//  MZSystemModel_Tests.m
//  MezzKit
//
//  Created by Zai Chang on 11/27/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MezzKit.h"

@interface MZSystemModel_Tests : XCTestCase
{
  MZSystemModel *_systemModel;
}

@end

@implementation MZSystemModel_Tests

- (void)setUp {
    [super setUp];

  _systemModel = [MZSystemModel new];
}

static NSString *feldName1 = @"Feld 1";
static NSString *feldName2 = @"Feld 2";
static NSString *feldName3 = @"Feld 3";

- (void)initOneFeld {
  MZFeld *feld = [MZFeld new];
  feld.name = feldName1;
  [_systemModel insertObject:feld inFeldsAtIndex:_systemModel.felds.count];
}

- (void)initThreeFelds {
  MZFeld *feld = [MZFeld new];
  feld.name = feldName1;
  [_systemModel insertObject:feld inFeldsAtIndex:_systemModel.felds.count];
  
  feld = [MZFeld new];
  feld.name = feldName2;
  [_systemModel insertObject:feld inFeldsAtIndex:_systemModel.felds.count];
  
  feld = [MZFeld new];
  feld.name = feldName3;
  [_systemModel insertObject:feld inFeldsAtIndex:_systemModel.felds.count];
}

- (void)tearDown {

  [_systemModel reset];
  [super tearDown];
}

- (void)testOneFeld {
  [self initOneFeld];

  XCTAssertNotNil([_systemModel feldWithName:feldName1]);
  XCTAssertNil([_systemModel feldWithName:feldName2]);
  XCTAssertNil([_systemModel feldWithName:feldName3]);
  
  XCTAssertEqualObjects(_systemModel.mainFeld.name, [_systemModel feldNameAtIndex:0]);
  XCTAssertEqualObjects(_systemModel.centerFeld.name, [_systemModel feldNameAtIndex:0]);
  
  // Out of bounds tests
  XCTAssertNil([_systemModel feldNameAtIndex:-1]);
  XCTAssertNil([_systemModel feldNameAtIndex:1]);
}

- (void)testThreeFelds {
  [self initThreeFelds];
  
  XCTAssertNotNil([_systemModel feldWithName:feldName1]);
  XCTAssertNotNil([_systemModel feldWithName:feldName2]);
  XCTAssertNotNil([_systemModel feldWithName:feldName3]);
  XCTAssertNil([_systemModel feldWithName:@"Feld 4"]);

  XCTAssertEqualObjects(feldName2, [_systemModel feldNameAtIndex:1]);
  
  XCTAssertEqualObjects(_systemModel.mainFeld.name, [_systemModel feldNameAtIndex:1]);
  XCTAssertEqualObjects(_systemModel.centerFeld.name, [_systemModel feldNameAtIndex:1]);
}

- (void)testWorkspaceOperations {
  NSString *anOwner = @"An owner";
  for (NSInteger i=0; i<3; i++) {
    MZWorkspace *workspace = [MZWorkspace new];
    workspace.uid = [NSString stringWithFormat:@"ws-%ld", (long)i];
    workspace.name = [NSString stringWithFormat:@"Workspace %ld", (long)i];
    if (i == 2)
      workspace.owner = anOwner;
    [_systemModel.workspaces addObject:workspace];
  }
  
  XCTAssertNotNil([_systemModel workspaceWithUid:@"ws-1"]);
  XCTAssertNil([_systemModel workspaceWithUid:@"ws-5"]);
  
  XCTAssertEqual([_systemModel workspacesWithOwner:nil].count, 2);
  XCTAssertEqual([_systemModel workspacesWithOwner:anOwner].count, 1);
  
  XCTAssertEqualObjects([_systemModel allOwnersOfWorkspaces], @[anOwner]);
  
  [_systemModel removeAllWorkspaces];
  
  XCTAssertEqual(_systemModel.countOfWorkspaces, 0);
}


#pragma mark - Pending Transactions Tests

- (void)setupTransactions {
  MZImageUploadTransaction *imageUploadTransaction = [MZImageUploadTransaction new];
  imageUploadTransaction.transactionName = MZImageUploadTransactionName;
  imageUploadTransaction.workspaceUid = @"ws-0001";
  [_systemModel insertObject:imageUploadTransaction inPendingTransactionsAtIndex:_systemModel.countOfPendingTransactions];
  
  imageUploadTransaction = [MZImageUploadTransaction new];
  imageUploadTransaction.transactionName = MZImageUploadTransactionName;
  imageUploadTransaction.workspaceUid = @"ws-0002";
  [_systemModel insertObject:imageUploadTransaction inPendingTransactionsAtIndex:_systemModel.countOfPendingTransactions];
  
  imageUploadTransaction = [MZImageUploadTransaction new];
  imageUploadTransaction.transactionName = MZImageUploadTransactionName;
  imageUploadTransaction.workspaceUid = @"ws-0001";
  [_systemModel insertObject:imageUploadTransaction inPendingTransactionsAtIndex:_systemModel.countOfPendingTransactions];
}


- (void)testClearPendingTransactions {
  [self setupTransactions];
  
  XCTAssertEqual(_systemModel.countOfPendingTransactions, 3);

  [_systemModel clearPendingTransactions];
  
  XCTAssertEqual(_systemModel.countOfPendingTransactions, 0);
}


- (void)testCancelPendingUploadTransactions {
  [self setupTransactions];
  
  XCTAssertEqual(_systemModel.countOfPendingTransactions, 3);

  NSString *target = nil;  // target is only used for 2.10 or before
  [_systemModel cancelPendingUploadsForTarget:target workspaceUid:@"ws-0001"];
  
  XCTAssertEqual(_systemModel.countOfPendingTransactions, 1);
}

#pragma mark - Pending Transactions Tests

- (void)setupSystemModelWithSurfaces
{
  MZSurface *surfaceMain = [[MZSurface alloc] initWithName:@"main"];
  MZSurface *surfaceLeftWall = [[MZSurface alloc] initWithName:@"left-wall"];
  MZSurface *surfaceRightWall = [[MZSurface alloc] initWithName:@"right-wall"];

  [_systemModel.surfaces addObjectsFromArray:@[surfaceMain, surfaceLeftWall, surfaceRightWall]];
}

- (void)testSurfacesInPrimaryWindshield
{
  [self setupSystemModelWithSurfaces];
  NSArray *surfaces = [_systemModel surfacesInPrimaryWindshield];
  XCTAssertEqual(surfaces.count, 1, @"There should be just one main surface");
}

- (void)testSurfacesInExtendedWindshield
{
  [self setupSystemModelWithSurfaces];
  NSArray *surfaces = [_systemModel surfacesInExtendedWindshield];
  XCTAssertEqual(surfaces.count, 2, @"There should be two surfaces forming the extended windshield");
}

- (void)testSurfaceNamesInPrimaryWindshield
{
  [self setupSystemModelWithSurfaces];
  NSArray *surfaceNames = [_systemModel surfaceNamesInPrimaryWindshield];
  XCTAssertEqual(surfaceNames.count, 1, @"There should be just one main surface");
  XCTAssertTrue([@"main" isEqualToString:[surfaceNames firstObject]], @"The only surface should be called main");
}

- (void)testSurfaceNamesInExtendedWindshield
{
  [self setupSystemModelWithSurfaces];
  NSArray *surfaceNames = [_systemModel surfaceNamesInExtendedWindshield];
  XCTAssertEqual(surfaceNames.count, 2, @"There should be two surfaces forming the extended windshield");
  XCTAssertTrue([surfaceNames containsObject:@"left-wall"], @"There should be a surface called left-wall");
  XCTAssertTrue([surfaceNames containsObject:@"right-wall"], @"There should be a surface called right-wall");
}




@end
