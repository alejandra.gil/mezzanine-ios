//
//  ParticipantsRoster_Tests.m
//  MezzKit
//
//  Created by miguel on 07/03/2019.
//  Copyright © 2019 Oblong Industries. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MockPoolConnector.h"
#import "MockProteinSource.h"

#import "MZCommunicator.h"
#import "MZCommunicator+Constants.h"
#import "MZCommunicatorRequestor.h"

@interface MZCommunicator (Internal)

-(void) setupJoinResponseHandler;
-(void) joinSuccessful;

-(void) beginListeningPoolConnectorNotifications;
-(void) endListeningPoolConnectorNotifications;

@end

@interface ParticipantsRoster_Tests : XCTestCase
{
  MZCommunicator *communicator;
  MZSystemModel *systemModel;
  MockPoolConnector *websocketPoolConnector;

  id<MockProteinSourceBaseProtocol> proteinSource;
}

@end

@implementation ParticipantsRoster_Tests

- (void)setUp
{
  communicator = [[MZCommunicator alloc] initWithServerUrl:[NSURL URLWithString:@"https://test"]];
  systemModel = [[MZSystemModel alloc] init];
  communicator.systemModel = systemModel;

  communicator.communicatorProtocol = OBCommunicatorProtocolWSS;
  websocketPoolConnector = [[MockPoolConnector alloc] initWithPool:MEZZ_WEBSOCKET];
  [communicator addPoolConnector:websocketPoolConnector forPoolName:MEZZ_WEBSOCKET];
}

- (void)tearDown
{
  [communicator endListeningPoolConnectorNotifications];
  communicator = nil;
  websocketPoolConnector = nil;

  XCTAssertTrue([communicator pendingTransactions].count == 0, @"Pending transactions should be %d, instead got %ld", 0, (long)[[communicator pendingTransactions] count]);
  [super tearDown];
}


- (void)setupCommunicatorCore
{
  systemModel.myMezzanine.version = @"3.13";
  systemModel.apiVersion = @"3.6";

  proteinSource = [[MockProteinSource alloc] init];

  communicator.provenance = [proteinSource parameterForKey:kCanonicalParameterProvenanceKey];
  communicator.proteinFactory = proteinSource;
  communicator.proteinFactory.provenance = communicator.provenance;

  // Begin listening to pool events, normally called in [OBCommunicator connect]
  [communicator beginListeningPoolConnectorNotifications];

  systemModel.state = MZSystemStateWorkspace;
}


-(void) setUpUnconnectedCommunicator
{
  [self setupCommunicatorCore];
  communicator.hasJoinedSession = NO;
  communicator.isConnected = NO;
  communicator.isConnecting = NO;
  [communicator setupJoinResponseHandler];
}


-(void) setUpConnectedCommunicator
{
  [self setupCommunicatorCore];
  [communicator joinSuccessful];
}

- (void)prepareSystemModelForMeetingTests
{
  systemModel.myMezzanine.uid = kCanonicalParameterMeetingRoomParticipantLocalKey;
  systemModel.myMezzanine.roomType = MZMezzanineRoomTypeLocal;

  void (^addRemoteMezzWithParameterKey)(NSString*) = ^(NSString *key)
  {
    NSDictionary *collaborator = [self->proteinSource parameterForKey:key];
    MZRemoteMezz *remoteMezz = [[MZRemoteMezz alloc] init];
    remoteMezz.uid = collaborator[@"uid"];
    remoteMezz.name = collaborator[@"room-name"];
    remoteMezz.roomType = MZMezzanineRoomTypeRemote;
    [self->systemModel.remoteMezzes addObject:remoteMezz];
  };

  void (^addCloudMezzWithParameterKey)(NSString*) = ^(NSString *key)
  {
    NSDictionary *collaborator = [self->proteinSource parameterForKey:key];
    MZRemoteMezz *remoteMezz = [[MZRemoteMezz alloc] init];
    remoteMezz.uid = collaborator[@"uid"];
    remoteMezz.name = collaborator[@"room-name"];
    remoteMezz.roomType = MZMezzanineRoomTypeCloud;
    [self->systemModel.remoteMezzes addObject:remoteMezz];
  };

  addRemoteMezzWithParameterKey(kCanonicalParameterMeetingRoomParticipant1Key);
  addRemoteMezzWithParameterKey(kCanonicalParameterMeetingRoomParticipant2Key);
  addCloudMezzWithParameterKey(kCanonicalParameterMeetingRoomParticipantCloudKey);
}


#pragma mark - Participants List Tests

- (void)testParticipantsListInLocalRemoteRoomsAndCloud
{
  [self setUpConnectedCommunicator];
  [self prepareSystemModelForMeetingTests];
  systemModel.featureToggles.participantRoster = true;

  XCTAssertTrue(systemModel.infopresence.rooms.count == 0, @"There should have been 0 rooms but instead there are %lu", (unsigned long)systemModel.infopresence.rooms.count);
  XCTAssertTrue(systemModel.myMezzanine.participants.count == 0, @"There should have been 0 participants but instead there are %lu", (unsigned long)systemModel.myMezzanine.participants.count);


  // Test case with 1 participant in each room: Local, 2 remote rooms and 1 cloud room
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameParticipantList responseType:@"psa"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];

  XCTAssertTrue(systemModel.infopresence.rooms.count == 3, @"There should have been 3 rooms but instead there are %lu", (unsigned long)systemModel.infopresence.rooms.count);
  for (MZMezzanine *mz in systemModel.infopresence.rooms) {
    XCTAssertTrue(mz.participants.count == 1, @"There should have been 1 participant but instead there are %lu", (unsigned long)mz.participants.count);
  }

  NSPredicate *remoteMezzaninePredicate = [NSPredicate predicateWithFormat:@"self.roomType == %d", MZMezzanineRoomTypeRemote];
  NSArray *remoteMezzes = [systemModel.infopresence.rooms filteredArrayUsingPredicate:remoteMezzaninePredicate];
  XCTAssertTrue(remoteMezzes.count == 2, @"There should have been 2 rooms but instead there are %lu", (unsigned long)remoteMezzes.count);

  NSPredicate *cloudMezzaninePredicate = [NSPredicate predicateWithFormat:@"self.roomType == %d", MZMezzanineRoomTypeCloud];
  NSArray *cloudMezzes = [systemModel.infopresence.rooms filteredArrayUsingPredicate:cloudMezzaninePredicate];
  XCTAssertTrue(cloudMezzes.count == 1, @"There should have been 1 room but instead there are %lu", (unsigned long)cloudMezzes.count);

  XCTAssertTrue(systemModel.myMezzanine.participants.count == 1, @"There should have been 1 participant but instead there are %lu", (unsigned long)systemModel.myMezzanine.participants.count);
  XCTAssertTrue(systemModel.myMezzanine.roomType == MZMezzanineRoomTypeLocal);
}

- (void)testParticipantsListInLocalRemoteRoomsAndEmtpyCloud
{
  [self setUpConnectedCommunicator];
  [self prepareSystemModelForMeetingTests];
  systemModel.featureToggles.participantRoster = true;
  NSPredicate *remoteMezzaninePredicate = [NSPredicate predicateWithFormat:@"self.roomType == %d", MZMezzanineRoomTypeRemote];
  NSPredicate *cloudMezzaninePredicate = [NSPredicate predicateWithFormat:@"self.roomType == %d", MZMezzanineRoomTypeCloud];

  XCTAssertTrue(systemModel.infopresence.rooms.count == 0, @"There should have been 0 rooms but instead there are %lu", (unsigned long)systemModel.infopresence.rooms.count);
  XCTAssertTrue(systemModel.myMezzanine.participants.count == 0, @"There should have been 0 participants but instead there are %lu", (unsigned long)systemModel.myMezzanine.participants.count);


  // Test case with 1 participant in each room: Local, 2 remote rooms and 1 emtpy cloud room
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameParticipantList responseType:@"psa?local-and-remote-participants"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];

  XCTAssertTrue(systemModel.infopresence.rooms.count == 3, @"There should have been 3 rooms but instead there are %lu", (unsigned long)systemModel.infopresence.rooms.count);

  NSArray *remoteMezzes = [systemModel.infopresence.rooms filteredArrayUsingPredicate:remoteMezzaninePredicate];
  XCTAssertTrue(remoteMezzes.count == 2, @"There should have been 2 rooms but instead there are %lu", (unsigned long)remoteMezzes.count);
  for (MZMezzanine *mz in remoteMezzes) {
    XCTAssertTrue(mz.participants.count == 1, @"There should have been 1 participant but instead there are %lu", (unsigned long)mz.participants.count);
  }

  NSArray *cloudMezzes = [systemModel.infopresence.rooms filteredArrayUsingPredicate:cloudMezzaninePredicate];
  XCTAssertTrue(cloudMezzes.count == 1, @"There should have been 1 room but instead there are %lu", (unsigned long)cloudMezzes.count);
  for (MZMezzanine *mz in cloudMezzes) {
    XCTAssertTrue(mz.participants.count == 0, @"There should have been 0 participant but instead there are %lu", (unsigned long)mz.participants.count);
  }

  XCTAssertTrue(systemModel.myMezzanine.participants.count == 1, @"There should have been 1 participant but instead there are %lu", (unsigned long)systemModel.myMezzanine.participants.count);
  XCTAssertTrue(systemModel.myMezzanine.roomType == MZMezzanineRoomTypeLocal);
}

- (void)testParticipantsListInLocalRemoteRoomsAndNoCloud
{
  [self setUpConnectedCommunicator];
  [self prepareSystemModelForMeetingTests];
  systemModel.featureToggles.participantRoster = true;
  NSPredicate *remoteMezzaninePredicate = [NSPredicate predicateWithFormat:@"self.roomType == %d", MZMezzanineRoomTypeRemote];
  NSPredicate *cloudMezzaninePredicate = [NSPredicate predicateWithFormat:@"self.roomType == %d", MZMezzanineRoomTypeCloud];

  XCTAssertTrue(systemModel.infopresence.rooms.count == 0, @"There should have been 0 rooms but instead there are %lu", (unsigned long)systemModel.infopresence.rooms.count);
  XCTAssertTrue(systemModel.myMezzanine.participants.count == 0, @"There should have been 0 participants but instead there are %lu", (unsigned long)systemModel.myMezzanine.participants.count);

  // Test case with 1 participant in each room: Local, 2 remote rooms and 0 cloud room
  NSArray *cloudMezzes = [systemModel.remoteMezzes filteredArrayUsingPredicate:cloudMezzaninePredicate];
  [systemModel.remoteMezzes removeObject:cloudMezzes.firstObject];

  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameParticipantList responseType:@"psa?local-and-remote-participants"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];

  XCTAssertTrue(systemModel.infopresence.rooms.count == 2, @"There should have been 2 rooms but instead there are %lu", (unsigned long)systemModel.infopresence.rooms.count);

  NSArray *remoteMezzes = [systemModel.infopresence.rooms filteredArrayUsingPredicate:remoteMezzaninePredicate];
  XCTAssertTrue(remoteMezzes.count == 2, @"There should have been 2 rooms but instead there are %lu", (unsigned long)remoteMezzes.count);
  for (MZMezzanine *mz in remoteMezzes) {
    XCTAssertTrue(mz.participants.count == 1, @"There should have been 1 participant but instead there are %lu", (unsigned long)mz.participants.count);
  }

  cloudMezzes = [systemModel.infopresence.rooms filteredArrayUsingPredicate:cloudMezzaninePredicate];
  XCTAssertTrue(cloudMezzes.count == 0, @"There should have been 0 rooms but instead there are %lu", (unsigned long)cloudMezzes.count);

  XCTAssertTrue(systemModel.myMezzanine.participants.count == 1, @"There should have been 1 participant but instead there are %lu", (unsigned long)systemModel.myMezzanine.participants.count);
  XCTAssertTrue(systemModel.myMezzanine.roomType == MZMezzanineRoomTypeLocal);
}


- (void)testParticipantsListInLocalNoRemoteRoomsAndNoCloud
{
  [self setUpConnectedCommunicator];
  [self prepareSystemModelForMeetingTests];
  systemModel.featureToggles.participantRoster = true;
  NSPredicate *remoteMezzaninePredicate = [NSPredicate predicateWithFormat:@"self.roomType == %d", MZMezzanineRoomTypeRemote];
  NSPredicate *cloudMezzaninePredicate = [NSPredicate predicateWithFormat:@"self.roomType == %d", MZMezzanineRoomTypeCloud];

  XCTAssertTrue(systemModel.infopresence.rooms.count == 0, @"There should have been 0 rooms but instead there are %lu", (unsigned long)systemModel.infopresence.rooms.count);
  XCTAssertTrue(systemModel.myMezzanine.participants.count == 0, @"There should have been 0 participants but instead there are %lu", (unsigned long)systemModel.myMezzanine.participants.count);

  // Test case with 1 participant in Local Mezz and no participants in the 2 remote rooms. No cloud room
  NSArray *cloudMezzes = [systemModel.remoteMezzes filteredArrayUsingPredicate:cloudMezzaninePredicate];
  [systemModel.remoteMezzes removeObject:cloudMezzes.firstObject];

  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameParticipantList responseType:@"psa?only-local-participants"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];

  XCTAssertTrue(systemModel.infopresence.rooms.count == 0, @"There should have been 0 rooms but instead there are %lu", (unsigned long)systemModel.infopresence.rooms.count);
  for (MZMezzanine *mz in systemModel.infopresence.rooms) {
    XCTAssertTrue(mz.participants.count == 1);
  }

  NSArray *remoteMezzes = [systemModel.infopresence.rooms filteredArrayUsingPredicate:remoteMezzaninePredicate];
  XCTAssertTrue(remoteMezzes.count == 0, @"There should have been 0 rooms but instead there are %lu", (unsigned long)remoteMezzes.count);

  cloudMezzes = [systemModel.infopresence.rooms filteredArrayUsingPredicate:cloudMezzaninePredicate];
  XCTAssertTrue(cloudMezzes.count == 0, @"There should have been 0 rooms but instead there are %lu", (unsigned long)cloudMezzes.count);

  XCTAssertTrue(systemModel.myMezzanine.participants.count == 1);
  XCTAssertTrue(systemModel.myMezzanine.roomType == MZMezzanineRoomTypeLocal);
}


- (void)testParticipantsListInLocalAndCloudNoRemoteRooms
{
  [self setUpConnectedCommunicator];
  [self prepareSystemModelForMeetingTests];
  systemModel.featureToggles.participantRoster = true;
  NSPredicate *remoteMezzaninePredicate = [NSPredicate predicateWithFormat:@"self.roomType == %d", MZMezzanineRoomTypeRemote];
  NSPredicate *cloudMezzaninePredicate = [NSPredicate predicateWithFormat:@"self.roomType == %d", MZMezzanineRoomTypeCloud];

  XCTAssertTrue(systemModel.infopresence.rooms.count == 0, @"There should have been 0 rooms but instead there are %lu", (unsigned long)systemModel.infopresence.rooms.count);
  XCTAssertTrue(systemModel.myMezzanine.participants.count == 0, @"There should have been 0 participants but instead there are %lu", (unsigned long)systemModel.myMezzanine.participants.count);

  // Test case with 1 participant in Local Mezz and 1 participant in cloud Mezz. No participants in the 2 remote rooms
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameParticipantList responseType:@"psa?local-and-cloud-participants"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];

  XCTAssertTrue(systemModel.infopresence.rooms.count == 1, @"There should have been 1 room1 but instead there are %lu", (unsigned long)systemModel.infopresence.rooms.count);
  for (MZMezzanine *mz in systemModel.infopresence.rooms) {
    XCTAssertTrue(mz.participants.count == 1, @"There should have been 1 participant but instead there are %lu", (unsigned long)mz.participants.count);
  }

  NSArray *remoteMezzes = [systemModel.infopresence.rooms filteredArrayUsingPredicate:remoteMezzaninePredicate];
  XCTAssertTrue(remoteMezzes.count == 0, @"There should have been 0 rooms but instead there are %lu", (unsigned long)remoteMezzes.count);

  NSArray *cloudMezzes = [systemModel.infopresence.rooms filteredArrayUsingPredicate:cloudMezzaninePredicate];
  XCTAssertTrue(cloudMezzes.count == 1, @"There should have been 1 room but instead there are %lu", (unsigned long)cloudMezzes.count);

  XCTAssertTrue(systemModel.myMezzanine.participants.count == 1, @"There should have been 1 participant but instead there are %lu", (unsigned long)systemModel.myMezzanine.participants.count);
  XCTAssertTrue(systemModel.myMezzanine.roomType == MZMezzanineRoomTypeLocal);
}


- (void)testParticipantsListChainingAllCases
{
  [self setUpConnectedCommunicator];
  [self prepareSystemModelForMeetingTests];
  systemModel.featureToggles.participantRoster = true;

  XCTAssertTrue(systemModel.infopresence.rooms.count == 0, @"There should have been 0 rooms but instead there are %lu", (unsigned long)systemModel.infopresence.rooms.count);
  XCTAssertTrue(systemModel.myMezzanine.participants.count == 0, @"There should have been 0 participants but instead there are %lu", (unsigned long)systemModel.myMezzanine.participants.count);


  // Test case with 1 participant in each room: Local, 2 remote rooms and 1 cloud room
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameParticipantList responseType:@"psa"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];

  XCTAssertTrue(systemModel.infopresence.rooms.count == 3, @"There should have been 3 rooms but instead there are %lu", (unsigned long)systemModel.infopresence.rooms.count);
  for (MZMezzanine *mz in systemModel.infopresence.rooms) {
    XCTAssertTrue(mz.participants.count == 1, @"There should have been 1 participant but instead there are %lu", (unsigned long)mz.participants.count);
  }

  NSPredicate *remoteMezzaninePredicate = [NSPredicate predicateWithFormat:@"self.roomType == %d", MZMezzanineRoomTypeRemote];
  NSArray *remoteMezzes = [systemModel.infopresence.rooms filteredArrayUsingPredicate:remoteMezzaninePredicate];
  XCTAssertTrue(remoteMezzes.count == 2, @"There should have been 2 rooms but instead there are %lu", (unsigned long)remoteMezzes.count);

  NSPredicate *cloudMezzaninePredicate = [NSPredicate predicateWithFormat:@"self.roomType == %d", MZMezzanineRoomTypeCloud];
  NSArray *cloudMezzes = [systemModel.infopresence.rooms filteredArrayUsingPredicate:cloudMezzaninePredicate];
  XCTAssertTrue(cloudMezzes.count == 1, @"There should have been 1 room but instead there are %lu", (unsigned long)cloudMezzes.count);

  XCTAssertTrue(systemModel.myMezzanine.participants.count == 1, @"There should have been 1 participant but instead there are %lu", (unsigned long)systemModel.myMezzanine.participants.count);
  XCTAssertTrue(systemModel.myMezzanine.roomType == MZMezzanineRoomTypeLocal);


  // Test case with 1 participant in each room: Local and 2 remote rooms AND no participant in cloud mezz
  key = [proteinSource proteinKeyForTransactionName:MZTransactionNameParticipantList responseType:@"psa?local-and-remote-participants"];
  p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];

  XCTAssertTrue(systemModel.infopresence.rooms.count == 3, @"There should have been 3 rooms but instead there are %lu", (unsigned long)systemModel.infopresence.rooms.count);

  remoteMezzes = [systemModel.infopresence.rooms filteredArrayUsingPredicate:remoteMezzaninePredicate];
  XCTAssertTrue(remoteMezzes.count == 2, @"There should have been 2 rooms but instead there are %lu", (unsigned long)remoteMezzes.count);
  for (MZMezzanine *mz in remoteMezzes) {
    XCTAssertTrue(mz.participants.count == 1, @"There should have been 1 participant but instead there are %lu", (unsigned long)mz.participants.count);
  }

  cloudMezzes = [systemModel.infopresence.rooms filteredArrayUsingPredicate:cloudMezzaninePredicate];
  XCTAssertTrue(cloudMezzes.count == 1, @"There should have been 1 room but instead there are %lu", (unsigned long)cloudMezzes.count);
  for (MZMezzanine *mz in cloudMezzes) {
    XCTAssertTrue(mz.participants.count == 0, @"There should have been 0 participant but instead there are %lu", (unsigned long)mz.participants.count);
  }

  XCTAssertTrue(systemModel.myMezzanine.participants.count == 1, @"There should have been 1 participant but instead there are %lu", (unsigned long)systemModel.myMezzanine.participants.count);
  XCTAssertTrue(systemModel.myMezzanine.roomType == MZMezzanineRoomTypeLocal);


  // Test case  with 1 participant in each room: Local and 2 remote rooms AND NO cloud mezz
  [systemModel.remoteMezzes removeObject:cloudMezzes.firstObject];

  key = [proteinSource proteinKeyForTransactionName:MZTransactionNameParticipantList responseType:@"psa?local-and-remote-participants"];
  p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];

  XCTAssertTrue(systemModel.infopresence.rooms.count == 2, @"There should have been 2 rooms but instead there are %lu", (unsigned long)systemModel.infopresence.rooms.count);

  remoteMezzes = [systemModel.infopresence.rooms filteredArrayUsingPredicate:remoteMezzaninePredicate];
  XCTAssertTrue(remoteMezzes.count == 2, @"There should have been 2 rooms but instead there are %lu", (unsigned long)remoteMezzes.count);
  for (MZMezzanine *mz in remoteMezzes) {
    XCTAssertTrue(mz.participants.count == 1, @"There should have been 1 participant but instead there are %lu", (unsigned long)mz.participants.count);
  }

  cloudMezzes = [systemModel.infopresence.rooms filteredArrayUsingPredicate:cloudMezzaninePredicate];
  XCTAssertTrue(cloudMezzes.count == 0, @"There should have been 0 rooms but instead there are %lu", (unsigned long)cloudMezzes.count);

  XCTAssertTrue(systemModel.myMezzanine.participants.count == 1, @"There should have been 1 participant but instead there are %lu", (unsigned long)systemModel.myMezzanine.participants.count);
  XCTAssertTrue(systemModel.myMezzanine.roomType == MZMezzanineRoomTypeLocal);


  // Test case with 1 participant in each room: Local and 1 cloud room
  void (^addCloudMezzWithParameterKey)(NSString*) = ^(NSString *key)
  {
    NSDictionary *collaborator = [self->proteinSource parameterForKey:key];
    MZRemoteMezz *remoteMezz = [[MZRemoteMezz alloc] init];
    remoteMezz.uid = collaborator[@"uid"];
    remoteMezz.name = collaborator[@"room-name"];
    remoteMezz.roomType = MZMezzanineRoomTypeCloud;
    [self->systemModel.remoteMezzes addObject:remoteMezz];
  };

  addCloudMezzWithParameterKey(kCanonicalParameterMeetingRoomParticipantCloudKey);

  key = [proteinSource proteinKeyForTransactionName:MZTransactionNameParticipantList responseType:@"psa?local-and-cloud-participants"];
  p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];

  XCTAssertTrue(systemModel.infopresence.rooms.count == 1, @"There should have been 1 room1 but instead there are %lu", (unsigned long)systemModel.infopresence.rooms.count);
  for (MZMezzanine *mz in systemModel.infopresence.rooms) {
    XCTAssertTrue(mz.participants.count == 1, @"There should have been 1 participant but instead there are %lu", (unsigned long)mz.participants.count);
  }

  remoteMezzes = [systemModel.infopresence.rooms filteredArrayUsingPredicate:remoteMezzaninePredicate];
  XCTAssertTrue(remoteMezzes.count == 0, @"There should have been 0 rooms but instead there are %lu", (unsigned long)remoteMezzes.count);

  cloudMezzes = [systemModel.infopresence.rooms filteredArrayUsingPredicate:cloudMezzaninePredicate];
  XCTAssertTrue(cloudMezzes.count == 1, @"There should have been 1 room but instead there are %lu", (unsigned long)cloudMezzes.count);

  XCTAssertTrue(systemModel.myMezzanine.participants.count == 1, @"There should have been 1 participant but instead there are %lu", (unsigned long)systemModel.myMezzanine.participants.count);
  XCTAssertTrue(systemModel.myMezzanine.roomType == MZMezzanineRoomTypeLocal);


  // Test case with 1 participant in each room: Local
  [systemModel.remoteMezzes removeObject:cloudMezzes.firstObject];

  key = [proteinSource proteinKeyForTransactionName:MZTransactionNameParticipantList responseType:@"psa?only-local-participants"];
  p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];

  XCTAssertTrue(systemModel.infopresence.rooms.count == 0, @"There should have been 0 rooms but instead there are %lu", (unsigned long)systemModel.infopresence.rooms.count);
  for (MZMezzanine *mz in systemModel.infopresence.rooms) {
    XCTAssertTrue(mz.participants.count == 1);
  }

  remoteMezzes = [systemModel.infopresence.rooms filteredArrayUsingPredicate:remoteMezzaninePredicate];
  XCTAssertTrue(remoteMezzes.count == 0, @"There should have been 0 rooms but instead there are %lu", (unsigned long)remoteMezzes.count);

  cloudMezzes = [systemModel.infopresence.rooms filteredArrayUsingPredicate:cloudMezzaninePredicate];
  XCTAssertTrue(cloudMezzes.count == 0, @"There should have been 0 rooms but instead there are %lu", (unsigned long)cloudMezzes.count);

  XCTAssertTrue(systemModel.myMezzanine.participants.count == 1);
  XCTAssertTrue(systemModel.myMezzanine.roomType == MZMezzanineRoomTypeLocal);
}


@end
