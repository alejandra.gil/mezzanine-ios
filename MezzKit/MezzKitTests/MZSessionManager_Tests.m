//
//  MZSessionManager_Tests.m
//  MezzKit
//
//  Created by miguel on 04/03/2019.
//  Copyright © 2019 Oblong Industries. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MezzKit.h"

#define MZSessionManagerTestsErrorDomain @"MZSessionManagerTestsErrorDomain"
#define kMZSessionManagerErrorCode_TEST -104000
#define TestToken @"test-test-token-has-12345-digits-and-more"


typedef void (^MZSessionManagerAuthorizeCompletionBlock) (NSError * _Nullable error, NSString * _Nullable token);
typedef void (^MZSessionManagerJoiningHeartbeatCompletionBlock) (NSError *error);

@interface MZSessionManager ()

- (NSURL *)urlWithEndPoint:(NSString *)endPoint;
- (void)handleJoiningHeartbeatRequestResponse:(NSURLResponse * _Nullable)response data:(NSData * _Nullable)data error:(NSError * _Nullable)error completion:(MZSessionManagerJoiningHeartbeatCompletionBlock)completionBlock;
- (void)handleSystemStateRequestResponse:(NSURLResponse * _Nullable)response data:(NSData * _Nullable)data error:(NSError * _Nullable)error completion:(MZSessionManagerHandshakeCompletionBlock)completionBlock;
- (void)handleAuthorizeRequestResponse:(NSURLResponse * _Nullable)response data:(NSData * _Nullable)data error:(NSError * _Nullable)error completion:(MZSessionManagerAuthorizeCompletionBlock)completionBlock;
- (void)handleCleanUpRequestResponse:(NSURLResponse * _Nullable)response data:(NSData * _Nullable)data error:(NSError * _Nullable)error completion:(MZSessionManagerCleanupCompletionBlock)completionBlock;
- (void)handleEndSessionRequestResponse:(NSURLResponse * _Nullable)response data:(NSData * _Nullable)data error:(NSError * _Nullable)error;
- (NSString *)extractSessionTokenFromResponseHeaders:(NSURLResponse *)response;

- (void)setCurrentSessionToken:(NSString *)sessionToken;
- (NSString *)getCurrentSessionToken;

@end

@interface MZSessionManager_Tests : XCTestCase
{
  MZSessionManager *sessionManager;
}


@end

@implementation MZSessionManager_Tests

- (void)setUp
{
  sessionManager = [[MZSessionManager alloc] init];
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
}

#pragma mark - System State responses Non-Ziggy Systems

- (void)testSystemStateMissingOnPlasmaConnection
{
  XCTestExpectation *expectation = [self expectationWithDescription:@"Waiting for System State error response"];

  MZSessionManagerHandshakeCompletionBlock block = ^(NSError *error, NSString *token) {
    [expectation fulfill];
    XCTAssertNil(error, @"There should be no error");
    XCTAssertNil(token, @"There should be no token");
  };

  NSHTTPURLResponse *response = [[NSHTTPURLResponse alloc] initWithURL:[self systemStateGETRequestURL] statusCode:404 HTTPVersion:nil headerFields:@{}];

  [sessionManager handleSystemStateRequestResponse:response data:nil error:nil completion:block];

  [self waitForExpectationsWithTimeout:3.0 handler:nil];
}

- (void)testPlasmaConnectionWithAnCustomError
{
  XCTestExpectation *expectation = [self expectationWithDescription:@"Waiting for System State error response"];

  MZSessionManagerHandshakeCompletionBlock block = ^(NSError *error, NSString *token) {
    [expectation fulfill];
    XCTAssertNotNil(error, @"There should be an error");
    XCTAssertTrue(error.code == kMZSessionManagerErrorCode_TEST);
    XCTAssertNil(token, @"There should be no token");
  };

  NSHTTPURLResponse *response = [[NSHTTPURLResponse alloc] initWithURL:[self systemStateGETRequestURL] statusCode:404 HTTPVersion:nil headerFields:@{}];

  NSError *error = [NSError errorWithDomain:MZSessionManagerTestsErrorDomain code:kMZSessionManagerErrorCode_TEST userInfo:nil];

  [sessionManager handleSystemStateRequestResponse:response data:nil error:error completion:block];

  [self waitForExpectationsWithTimeout:3.0 handler:nil];
}

#pragma mark - System State responses Ziggy Systems

- (void)testSystemStateMissingOnZiggyConnection
{
  XCTestExpectation *expectation = [self expectationWithDescription:@"Waiting for System State response"];

  MZSessionManagerHandshakeCompletionBlock block = ^(NSError *error, NSString *token) {
    [expectation fulfill];
    XCTAssertNotNil(error, @"There should be an error");
    XCTAssertTrue(error.code == kMZSessionManagerErrorCode_SystemStateSerializationError);
    XCTAssertNil(token, @"There should be no token");
  };

  NSHTTPURLResponse *response = [[NSHTTPURLResponse alloc] initWithURL:[self systemStateGETRequestURL] statusCode:200 HTTPVersion:nil headerFields:@{}];

  [sessionManager handleSystemStateRequestResponse:response data:nil error:nil completion:block];

  [self waitForExpectationsWithTimeout:3.0 handler:nil];
}


// In this case the request will chain with the joining heartbeat request that
// handles the visibility of the passkey view. Test that it can get there
- (void)testSystemStateResponseOnPasskeyProtectedZiggyConnection
{
  XCTestExpectation *expectation = [self expectationWithDescription:@"Waiting for Joining Heartbeat request"];

  MZSessionManagerHandshakeCompletionBlock block = ^(NSError *error, NSString *token) {
    XCTAssertNotNil(error, @"There should be an error");
    XCTAssertTrue(error.code == kMZSessionManagerErrorCode_ErrorOnJoiningHeartbeatRequest);
    [expectation fulfill];
    XCTAssertNil(token, @"There should be no token");
  };

  NSHTTPURLResponse *response = [[NSHTTPURLResponse alloc] initWithURL:[self systemStateGETRequestURL] statusCode:200 HTTPVersion:nil headerFields:@{}];

  NSString *systemStateJSONWithoutMetaDataString = @"{"
  "\"response\": {"
                  "\"mezzanine-version\": \"3.30\","
                  "\"passkey-required\": true,"
                  "\"passkey-length\": 6,"
                "}"
  "}";

  NSData *data = [systemStateJSONWithoutMetaDataString dataUsingEncoding:NSUTF8StringEncoding];
  [sessionManager handleSystemStateRequestResponse:response data:data error:nil completion:block];

  [self waitForExpectationsWithTimeout:3.0 handler:nil];
}

// In this case the request will chain with the authentication request.
// Test that it can get there
- (void)testSystemStateResponseOnPasskeyLessZiggyConnection
{
  XCTestExpectation *expectation = [self expectationWithDescription:@"Waiting for Joining Heartbeat request"];

  MZSessionManagerHandshakeCompletionBlock block = ^(NSError *error, NSString *token) {
    XCTAssertNotNil(error, @"There should be no error");
    XCTAssertTrue(error.code == kMZSessionManagerErrorCode_ErrorOnAuthorizeRequest);
    [expectation fulfill];
    XCTAssertNil(token, @"There should be no token");
  };

  NSHTTPURLResponse *response = [[NSHTTPURLResponse alloc] initWithURL:[self systemStateGETRequestURL] statusCode:200 HTTPVersion:nil headerFields:@{}];

  NSString *systemStateJSONWithoutMetaDataString = @"{"
  "\"response\": {"
                  "\"mezzanine-version\": \"3.30\","
                  "\"passkey-required\": false,"
                  "\"passkey-length\": 0,"
                "}"
  "}";

  NSData *data = [systemStateJSONWithoutMetaDataString dataUsingEncoding:NSUTF8StringEncoding];
  [sessionManager handleSystemStateRequestResponse:response data:data error:nil completion:block];

  [self waitForExpectationsWithTimeout:3.0 handler:nil];
}

#pragma mark - Authorize response tests

// In this case the request will chain with the authentication request.
// Test that it can get there
- (void)testAuthorize200Response
{
  XCTestExpectation *expectation = [self expectationWithDescription:@"Waiting for Authorize response"];
  NSString *tokenInCookie = [NSString stringWithFormat:@"Session-Token=%@; HttpOnly; Secure", TestToken];
  NSDictionary *headerDictionary = @{@"Set-Cookie": tokenInCookie};

  MZSessionManagerAuthorizeCompletionBlock block = ^(NSError *error, NSString *token) {
    XCTAssertNil(error, @"There should be no error");
    [expectation fulfill];
    XCTAssertTrue([token isEqualToString:TestToken], @"There should be a token");
  };

  NSHTTPURLResponse *response = [[NSHTTPURLResponse alloc] initWithURL:[self authorizeGETRequestURL]
                                                            statusCode:200
                                                           HTTPVersion:@"HTTP/1.1"
                                                          headerFields:headerDictionary];

  [sessionManager handleAuthorizeRequestResponse:response data:nil error:nil completion:block];

  [self waitForExpectationsWithTimeout:3.0 handler:nil];
}

// Authorize on reconnection won't return a token so if there's already one don't delete it.
- (void)testAuthorize200WithoutTokenResponse
{
  XCTestExpectation *expectation = [self expectationWithDescription:@"Waiting for Authorize response"];

  NSString *initialToken = @"initial-test-token";
  [sessionManager setCurrentSessionToken:initialToken];

  MZSessionManagerAuthorizeCompletionBlock block = ^(NSError *error, NSString *token) {
    XCTAssertNil(error, @"There should be no error");
    [expectation fulfill];
    NSString *finalToken = [self->sessionManager getCurrentSessionToken];
    XCTAssertNotNil(finalToken, @"A Token should have been stored");
    XCTAssertTrue([finalToken isEqualToString:initialToken], @"Stored token should have been updated");
  };

  NSHTTPURLResponse *response = [[NSHTTPURLResponse alloc] initWithURL:[self authorizeGETRequestURL]
                                                            statusCode:200
                                                           HTTPVersion:@"HTTP/1.1"
                                                          headerFields:nil];

  [sessionManager handleAuthorizeRequestResponse:response data:nil error:nil completion:block];

  [self waitForExpectationsWithTimeout:3.0 handler:nil];
}

- (void)testAuthorize404Response
{
  XCTestExpectation *expectation = [self expectationWithDescription:@"Waiting for Authorize response"];

  MZSessionManagerAuthorizeCompletionBlock block = ^(NSError *error, NSString *token) {
    XCTAssertNotNil(error, @"There should be an error");
    XCTAssertTrue(error.code == kMZSessionManagerErrorCode_ErrorOnAuthorizeRequest);
    [expectation fulfill];
    XCTAssertNil(token, @"There should be no token");
  };

  NSHTTPURLResponse *response = [[NSHTTPURLResponse alloc] initWithURL:[self authorizeGETRequestURL]
                                                            statusCode:404
                                                           HTTPVersion:@"HTTP/1.1"
                                                          headerFields:nil];

  [sessionManager handleAuthorizeRequestResponse:response data:nil error:nil completion:block];

  [self waitForExpectationsWithTimeout:3.0 handler:nil];
}

#pragma mark - Joining Heartbeat response tests

// In this case the request will chain with the authentication request.
// Test that it can get there
- (void)testJoiningHeartbeat200Response
{
  XCTestExpectation *expectation = [self expectationWithDescription:@"Waiting for Joining Heartbeat response"];

  MZSessionManagerJoiningHeartbeatCompletionBlock block = ^(NSError *error) {
    XCTAssertNil(error, @"There should be no error");
    [expectation fulfill];
  };

  NSHTTPURLResponse *response = [[NSHTTPURLResponse alloc] initWithURL:[self joiningHeartbeatGETRequestURL]
                                                            statusCode:200
                                                           HTTPVersion:@"HTTP/1.1"
                                                          headerFields:nil];

  [sessionManager handleJoiningHeartbeatRequestResponse:response data:nil error:nil completion:block];

  [self waitForExpectationsWithTimeout:3.0 handler:nil];
}

- (void)testJoiningHeartbeat404Response
{
  XCTestExpectation *expectation = [self expectationWithDescription:@"Waiting for Joining Heartbeat response"];

  MZSessionManagerJoiningHeartbeatCompletionBlock block = ^(NSError *error) {
    XCTAssertNotNil(error, @"There should be an error");
    XCTAssertTrue(error.code == kMZSessionManagerErrorCode_ErrorOnJoiningHeartbeatRequest);
    [expectation fulfill];
  };

  NSHTTPURLResponse *response = [[NSHTTPURLResponse alloc] initWithURL:[self joiningHeartbeatGETRequestURL]
                                                            statusCode:404
                                                           HTTPVersion:@"HTTP/1.1"
                                                          headerFields:nil];

  [sessionManager handleJoiningHeartbeatRequestResponse:response data:nil error:nil completion:block];

  [self waitForExpectationsWithTimeout:3.0 handler:nil];
}


#pragma mark - Clean up response tests

// In this case the request will chain with the authentication request.
// Test that it can get there
- (void)testCleanUp200Response
{
  XCTestExpectation *expectation = [self expectationWithDescription:@"Waiting for clean up response"];
  NSString *tokenInCookie = [NSString stringWithFormat:@"Session-Token=%@; HttpOnly; Secure", TestToken];
  NSDictionary *headerDictionary = @{@"Set-Cookie": tokenInCookie};

  NSString *initialToken = @"initial-test-token";
  [sessionManager setCurrentSessionToken:initialToken];

  MZSessionManagerCleanupCompletionBlock block = ^(NSError *error, NSString *token) {
    XCTAssertNil(error, @"There should be no error");
    [expectation fulfill];
    XCTAssertTrue([token isEqualToString:TestToken], @"There should be a token");
    NSString *finalToken = [self->sessionManager getCurrentSessionToken];
    XCTAssertNotNil(finalToken, @"A Token should have been stored");
    XCTAssertTrue([finalToken isEqualToString:TestToken], @"There should be a token");
    XCTAssertTrue(![finalToken isEqualToString:initialToken], @"Stored token should have been updated");
  };

  NSHTTPURLResponse *response = [[NSHTTPURLResponse alloc] initWithURL:[self cleanupGETRequestURL]
                                                            statusCode:200
                                                           HTTPVersion:@"HTTP/1.1"
                                                          headerFields:headerDictionary];
  [sessionManager handleCleanUpRequestResponse:response data:nil error:nil completion:block];

  [self waitForExpectationsWithTimeout:3.0 handler:nil];
}

- (void)testCleanUp404Response
{
  XCTestExpectation *expectation = [self expectationWithDescription:@"Waiting for clean up response"];

  MZSessionManagerCleanupCompletionBlock block = ^(NSError *error, NSString *token) {
    XCTAssertNotNil(error, @"There should be an error");
    XCTAssertTrue(error.code == kMZSessionManagerErrorCode_ErrorCleaningUpSession);
    [expectation fulfill];
    XCTAssertNil(token, @"There should be no token");
  };

  NSHTTPURLResponse *response = [[NSHTTPURLResponse alloc] initWithURL:[self cleanupGETRequestURL]
                                                            statusCode:404
                                                           HTTPVersion:@"HTTP/1.1"
                                                          headerFields:nil];

  [sessionManager handleCleanUpRequestResponse:response data:nil error:nil completion:block];

  [self waitForExpectationsWithTimeout:3.0 handler:nil];
}


#pragma mark - End Session response tests

// In this case the request will chain with the authentication request.
// Test that it can get there
- (void)testEndSession200Response
{
  NSString *initialToken = @"initial-test-token";
  [sessionManager setCurrentSessionToken:initialToken];

  NSHTTPURLResponse *response = [[NSHTTPURLResponse alloc] initWithURL:[self endSessionGETRequestURL]
                                                            statusCode:200
                                                           HTTPVersion:@"HTTP/1.1"
                                                          headerFields:nil];
  [sessionManager handleEndSessionRequestResponse:response data:nil error:nil];

  NSString *finalToken = [sessionManager getCurrentSessionToken];
  XCTAssertNil(finalToken, @"A Token should have been deleted");
}

- (void)testEndSession404Response
{
  NSString *initialToken = @"initial-test-token";
  [sessionManager setCurrentSessionToken:initialToken];

  NSHTTPURLResponse *response = [[NSHTTPURLResponse alloc] initWithURL:[self endSessionGETRequestURL]
                                                            statusCode:404
                                                           HTTPVersion:@"HTTP/1.1"
                                                          headerFields:nil];

  [sessionManager handleEndSessionRequestResponse:response data:nil error:nil];

  NSString *finalToken = [sessionManager getCurrentSessionToken];
  XCTAssertNotNil(finalToken, @"There should be a token because it wasn't deleted");
}


#pragma mark - Test helpers methods

-(NSURL *)systemStateGETRequestURL
{
  return [NSURL URLWithString:@"https://test.server.com/session/v1/system-state"];
}

-(NSURL *)authorizeGETRequestURL
{
  return [NSURL URLWithString:@"https://test.server.com/session/v1/authorize"];
}

-(NSURL *)joiningHeartbeatGETRequestURL
{
  return [NSURL URLWithString:@"https://test.server.com/session/v1/joining-heartbeat"];
}

-(NSURL *)cleanupGETRequestURL
{
  return [NSURL URLWithString:@"https://test.server.com/session/v1/cleanup-session"];
}

-(NSURL *)endSessionGETRequestURL
{
  return [NSURL URLWithString:@"https://test.server.com/session/v1/end-session"];
}


#pragma mark - Session Token & Cookies tests

- (void)testSessionTokenExtraction
{
  NSString *tokenInCookie = [NSString stringWithFormat:@"Session-Token=%@; HttpOnly; Secure", TestToken];
  NSDictionary *headerDictionary = @{@"Set-Cookie": tokenInCookie};
  NSHTTPURLResponse *response = [[NSHTTPURLResponse alloc] initWithURL:[self systemStateGETRequestURL]
                                                            statusCode:200
                                                           HTTPVersion:@"HTTP/1.1"
                                                          headerFields:headerDictionary];

  NSString *receivedTestToken = [sessionManager extractSessionTokenFromResponseHeaders:response];
  XCTAssertTrue([receivedTestToken isEqualToString:TestToken]);
}

- (void)testSessionTokenFailedExtraction
{
  NSHTTPURLResponse *response = [[NSHTTPURLResponse alloc] initWithURL:[self systemStateGETRequestURL]
                                                            statusCode:200
                                                           HTTPVersion:@"HTTP/1.1"
                                                          headerFields:nil];

  NSString *testToken = [sessionManager extractSessionTokenFromResponseHeaders:response];
  XCTAssertNil(testToken);
}


@end
