//
//  MZMezzanine_Tests.m
//  MezzKit
//
//  Created by Zai Chang on 11/25/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MezzKit.h"
#import "NSString+VersionChecking.h"

@interface MZMezzanine_Tests : XCTestCase
{
  MZMezzanine *_mezzanine;
}
@end

@implementation MZMezzanine_Tests

- (void)setUp {
    [super setUp];
  
  _mezzanine = [[MZMezzanine alloc] init];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testUpdateWithDictionary {
  NSString *uid = @"mezz-uid-something";
  NSString *name = @"Mezzanine Name";
  NSString *location = @"Some Location";
  NSString *company = @"Company Name";
  NSDictionary *dictionary = @{ @"uid" : uid,
                                @"name" : name,
                                @"location" : location,
                                @"company" : company,
                                };
  
  [_mezzanine updateWithDictionary:dictionary];
  
  XCTAssertEqualObjects(_mezzanine.uid, uid);
  XCTAssertEqualObjects(_mezzanine.name, name);
  XCTAssertEqualObjects(_mezzanine.location, location);
  XCTAssertEqualObjects(_mezzanine.company, company);
}

- (void)testVersionIsGreater {
  
  MZMezzanine *oldMezzanine = [[MZMezzanine alloc] init];
  oldMezzanine.version = @"2.12";
  
  MZMezzanine *newMezzanine = [[MZMezzanine alloc] init];
  newMezzanine.version = @"3.0";
  
  XCTAssertTrue([newMezzanine.version isVersionOrGreater:oldMezzanine.version]);
  XCTAssertFalse([oldMezzanine.version isVersionOrGreater:newMezzanine.version]);
}

- (void)testVersionIsTheSame {
  
  MZMezzanine *mezzanineA = [[MZMezzanine alloc] init];
  mezzanineA.version = @"3.0";
  
  MZMezzanine *mezzanineB = [[MZMezzanine alloc] init];
  mezzanineB.version = @"3.0";
  
  XCTAssertTrue([mezzanineA.version isVersionOrGreater:mezzanineB.version]);
  XCTAssertTrue([mezzanineB.version isVersionOrGreater:mezzanineA.version]);
}

@end
