//
//  MZPresentation_Tests.m
//  MezzKit
//
//  Created by Zai Chang on 11/25/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MezzKit.h"


@interface MZPresentation_Tests : XCTestCase
{
  MZPresentation *_presentation;
}
@end


@implementation MZPresentation_Tests

- (NSString*)itemUidForIndex:(NSInteger)i
{
  return [NSString stringWithFormat:@"sl-%ld", (long)i];
}

- (NSString*)itemContentSourceForIndex:(NSInteger)i
{
  return [NSString stringWithFormat:@"as-%ld", (long)i];
}


- (void)setUp {
    [super setUp];
  
  _presentation = [[MZPresentation alloc] init];
  
  for (NSInteger i=0; i<10; i++)
  {
    MZPortfolioItem *item = [[MZPortfolioItem alloc] init];
    item.uid = [self itemUidForIndex:i];
    item.contentSource = [self itemContentSourceForIndex:i];
    [_presentation.slides addObject:item];
  }
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testCurrentSlideIndex {
  NSInteger newCurrentSlideIndex = 8;
  _presentation.currentSlideIndex = newCurrentSlideIndex;
  XCTAssertEqual(_presentation.currentSlideIndex, newCurrentSlideIndex);
}

- (void)testCurrentSlideIndexOutOfBounds {
  NSInteger newCurrentSlideIndex = -1;
  _presentation.currentSlideIndex = newCurrentSlideIndex;
  XCTAssertEqual(_presentation.currentSlideIndex, 0);

  newCurrentSlideIndex = 11;
  _presentation.currentSlideIndex = newCurrentSlideIndex;
  XCTAssertEqual(_presentation.currentSlideIndex, 0);
}

- (void)testSlideWithUid {
  MZPortfolioItem *slideShouldExist = [_presentation slideWithUid:[self itemUidForIndex:5]];
  XCTAssertNotNil(slideShouldExist);
  MZPortfolioItem *slideShouldntExist = [_presentation slideWithUid:[self itemUidForIndex:10]];
  XCTAssertNil(slideShouldntExist);
}

- (void)testSlideUidAtIndex {
  for (NSInteger i=0; i<_presentation.numberOfSlides; i++)
  {
    XCTAssertEqualObjects([_presentation slideUidAtIndex:i], [self itemUidForIndex:i]);
  }
  NSString *slideShouldntExist = [_presentation slideUidAtIndex:10];
  XCTAssertNil(slideShouldntExist);
}

- (void)checkSlideIndexes {
  for (NSInteger i=0; i<_presentation.numberOfSlides; i++)
  {
    MZPortfolioItem *slide = _presentation.slides[i];
    XCTAssertEqual(slide.index, i);
  }
}

- (void)testUpdateSlideIndexes {
  [_presentation updateSlideIndexes];
  [self checkSlideIndexes];
}

- (void)testSlidesWithContentSource {
  [_presentation updateSlideIndexes];

  NSInteger indexOfSlideUnderTest = 3;
  NSString *contentSource = [self itemContentSourceForIndex:indexOfSlideUnderTest];
  NSArray *items = [_presentation slidesWithContentSource:contentSource];
  XCTAssertTrue(items.count == 1);
  if (items.count == 1)
  {
    XCTAssertEqual([items[0] index], indexOfSlideUnderTest);
    XCTAssertEqualObjects([items[0] contentSource], contentSource);
  }
}

- (void)testMoveSlideToIndex_Forward {
  NSInteger originalIndex = 3;
  NSInteger targetIndex = 6;
  [_presentation moveSlide:_presentation.slides[originalIndex] toIndex:targetIndex];

  MZPortfolioItem *item = _presentation.slides[targetIndex];
  XCTAssertEqualObjects(item.contentSource, [self itemContentSourceForIndex:originalIndex]);

  MZPortfolioItem *itemOriginallyToTheRight = _presentation.slides[originalIndex];
  XCTAssertEqualObjects(itemOriginallyToTheRight.contentSource, [self itemContentSourceForIndex:originalIndex + 1]);

  [self checkSlideIndexes];
}

- (void)testMoveSlideToIndex_Backwards {
  NSInteger originalIndex = 6;
  NSInteger targetIndex = 3;
  [_presentation moveSlide:_presentation.slides[originalIndex] toIndex:targetIndex];
  
  MZPortfolioItem *item = _presentation.slides[targetIndex];
  XCTAssertEqualObjects(item.contentSource, [self itemContentSourceForIndex:originalIndex]);
  
  MZPortfolioItem *itemOriginallyToTheLeft = _presentation.slides[originalIndex];
  XCTAssertEqualObjects(itemOriginallyToTheLeft.contentSource, [self itemContentSourceForIndex:originalIndex - 1]);
  
  [self checkSlideIndexes];
}

- (void)testMoveSlideWithUidToIndex {
  NSInteger originalIndex = 3;
  NSInteger targetIndex = 6;
  NSString *uid = [_presentation.slides[originalIndex] uid];
  [_presentation moveSlideWithUid:uid toIndex:targetIndex];
  
  MZPortfolioItem *item = _presentation.slides[targetIndex];
  XCTAssertEqualObjects(item.contentSource, [self itemContentSourceForIndex:originalIndex]);
  
  MZPortfolioItem *itemToTheRight = _presentation.slides[originalIndex];
  XCTAssertEqualObjects(itemToTheRight.contentSource, [self itemContentSourceForIndex:originalIndex + 1]);
  
  [self checkSlideIndexes];
}

- (void)testSwapSlides {
  NSInteger indexA = 3;
  NSInteger indexB = 6;
  MZPortfolioItem *itemA = _presentation.slides[indexA];
  MZPortfolioItem *itemB = _presentation.slides[indexB];

  [_presentation swapSlide:itemA withSlide:itemB];
  
  XCTAssertEqual(itemA.index, indexB);
  XCTAssertEqualObjects([_presentation.slides[indexA] contentSource], [self itemContentSourceForIndex:indexB]);
  XCTAssertEqual(itemB.index, indexA);
  XCTAssertEqualObjects([_presentation.slides[indexB] contentSource], [self itemContentSourceForIndex:indexA]);
  
  for (NSInteger i=0; i<_presentation.numberOfSlides; i++)
  {
    if (i != indexA && i != indexB)
    {
      MZPortfolioItem *item = _presentation.slides[i];
      XCTAssertEqual(item.index, i);
      XCTAssertEqualObjects(item.contentSource, [self itemContentSourceForIndex:i]);
    }
  }
}

- (void)testRemoveAllSlides {
  [_presentation removeAllSlides];
  XCTAssertEqual(_presentation.slides.count, 0);
}

- (void)testUpdatePresentationWithPortfolioItemsAddingOne
{
  NSMutableArray *portfolioItemsDictionaries = @[].mutableCopy;

  XCTAssertEqual(_presentation.slides.count, 10, @"There should be 10 slides to begin the test");
  for (NSInteger i=0; i<11; i++)
  {
    [portfolioItemsDictionaries addObject:@{@"uid": [self itemUidForIndex:i],
                                            @"content-source": [self itemContentSourceForIndex:i]}];
  }

  [_presentation updateWithPortfolioItems:portfolioItemsDictionaries];

  XCTAssertEqual(_presentation.slides.count, 11, @"There should be 11 after updating the list of items");
}

- (void)testUpdatePresentationWithPortfolioItemsRemovingOne
{
  NSMutableArray *portfolioItemsDictionaries = @[].mutableCopy;

  XCTAssertEqual(_presentation.slides.count, 10, @"There should be 10 slides to begin the test");
  for (NSInteger i=0; i<9; i++)
  {
    [portfolioItemsDictionaries addObject:@{@"uid": [self itemUidForIndex:i],
                                            @"content-source": [self itemContentSourceForIndex:i]}];
  }

  [_presentation updateWithPortfolioItems:portfolioItemsDictionaries];

  XCTAssertEqual(_presentation.slides.count, 9, @"There should be 9 after updating the list of items");
}


@end
