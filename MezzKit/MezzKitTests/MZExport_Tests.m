//
//  MZExport_Tests.m
//  MezzKit
//
//  Created by Miguel Sánchez Valdés on 22/10/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MZExport.h"
#import "MZExport_Private.h"
#import "MZExportInfo.h"

@interface MZExport_Tests : XCTestCase
{
  MZExport *_export;
  MZExportInfo *_exportInfo;
}
@end

@implementation MZExport_Tests

- (void)setUp
{
  [super setUp];
  _export = [MZExport new];
  _exportInfo = _export.info;
  _export.downloadFolder = @"/tmp/tests";
}

- (void)tearDown
{
  _export = nil;
  [super tearDown];
}

#pragma mark - tests

- (void)testObservingExportInfoURL
{
  _export.info = _exportInfo;
  id observation = [_exportInfo observationInfo];
  id observer = [[[observation valueForKey:@"_observances"] firstObject] valueForKey:@"_observer"] ;
  XCTAssertEqualObjects(observer, _export);

  _export.info = nil;
  observation = [_exportInfo observationInfo];
  observer = [[[observation valueForKey:@"_observances"] firstObject] valueForKey:@"_observer"] ;
  XCTAssertNil(observer);
}

- (void)testFileExists
{
  NSBundle *bundle = [NSBundle bundleForClass:[self class]];
  NSString *path = [bundle pathForResource:@"Test-PDF" ofType:@"pdf"];
  _exportInfo.url = [NSURL fileURLWithPath:path];

  NSPredicate *exportProgress = [NSPredicate predicateWithFormat:@"progress == 1.0"];
  [self expectationForPredicate:exportProgress evaluatedWithObject:_exportInfo handler:nil];
  [self waitForExpectationsWithTimeout:2.0 handler:nil];

  XCTAssertEqual(_exportInfo.state, MZExportStateReady, @"Final State should be 'Ready'");
  XCTAssertTrue(_exportInfo.progress == 1.0, @"Final be with progress = 1.0");
}

- (void)testFileDoesNotExist
{
  NSBundle *bundle = [NSBundle bundleForClass:[self class]];
  NSString *path = [[bundle pathForResource:@"Test-PDF" ofType:@"pdf"] stringByAppendingString:@"not-valid"];
  _exportInfo.url = [NSURL fileURLWithPath:path];

  NSPredicate *exportState = [NSPredicate predicateWithFormat:@"state == 4"]; // 4 -> MZExportStateFailed
  [self expectationForPredicate:exportState evaluatedWithObject:_exportInfo handler:nil];
  [self waitForExpectationsWithTimeout:2.0 handler:nil];

  XCTAssertEqual(_exportInfo.state, MZExportStateFailed, @"Final State should be 'Failed");
}

- (void)testIsExporting
{
  _exportInfo.state = MZExportStateNone;
  XCTAssertFalse([_export isExporting], @"Export State None is not exporting");

  _exportInfo.state = MZExportStateRequested;
  XCTAssertTrue([_export isExporting], @"Export State Requested is exporting");

  _exportInfo.state = MZExportStateDownloading;
  XCTAssertTrue([_export isExporting], @"Export State Downloading is not exporting");

  _exportInfo.state = MZExportStateReady;
  XCTAssertFalse([_export isExporting], @"Export State Ready is not exporting");

  _exportInfo.state = MZExportStateFailed;
  XCTAssertFalse([_export isExporting], @"Export State Failed is not exporting");
}

- (void)testEraseDownloadFolder
{
  NSFileManager *fileManager = [NSFileManager defaultManager];
  XCTAssertTrue([fileManager fileExistsAtPath:_export.downloadFolder], @"Download folder should exist before being deleted.");

  [_export eraseDownloadFolder];

  XCTAssertFalse([fileManager fileExistsAtPath:_export.downloadFolder], @"Download folder should have been deleted.");
}


#pragma mark - MZExportInfo tests

- (void)testSourceString
{
  _exportInfo.source = MZExportSourceTypePortfolio;
  XCTAssertTrue([_exportInfo.sourceString isEqualToString:@"Portfolio"], @"Source string should be Portfolio but it is %@", _exportInfo.sourceString);
}


@end
