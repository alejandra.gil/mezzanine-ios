//
//  MockPoolConnector.m
//  OBCore
//
//  Created by Zai Chang on 2/4/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#if TEST_IOS_FRAMEWORK
@import OBCore;
#elif TARGET_OS_MAC
// @import OBCore_macOS_Framework;
#endif

#import "MockPoolConnector.h"
#import "MZSlaws.h"

@implementation MockPoolConnector

#pragma mark - Override Constructors

// All other designed constructors eventually calls this
-(id) initWithPool:(NSString*)aPoolName mode:(OBPoolConnectorMode)mode error:(NSError**)error
{
  self = [super init];
  if (self)
  {
    poolName = [aPoolName copy];
    isConnected = YES;
    self.sentProteins = [NSMutableArray array];
  }
  return self;
}


-(void) receivedProteinYAML:(NSString*)yaml
{
  [NSException raise:@"NotImplementedException" format:@"MockPoolConnector simulateReceivedProteinYAML"];
}


-(void) sendProtein:(OBProtein*)protein
{
  if ([self.testDelegate respondsToSelector:@selector(mockPoolConnector:willSendProtein:)])
    [self.testDelegate mockPoolConnector:self willSendProtein:protein];
  
  if (protein)
    [_sentProteins addObject:protein];
}


-(void) sendProteinImmediately:(OBProtein*)protein
{
  // For test cases there isn't any difference between sendProtein and sendProteinImmediately
  [self sendProtein:protein];
}


@end
