//
//  MockPoolConnector.h
//  OBCore
//
//  Created by Zai Chang on 2/4/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import "MezzKit.h"
#import "OBPoolConnector.h"


@protocol MockPoolConnectorDelegate;


/// The purpose of this MockPoolConnector class is to allow a pool connector
/// to be initialized for a non-existance pool, for the purpose of unit tests

@interface MockPoolConnector : OBPoolConnector
{
}

@property (nonatomic, weak) id<MockPoolConnectorDelegate> testDelegate;
@property (nonatomic, strong) NSMutableArray *sentProteins;

-(void) receivedProteinYAML:(NSString*)yaml;

@end



@protocol MockPoolConnectorDelegate <NSObject>

// Delegate method for test cases to check that there is something being sent
-(void) mockPoolConnector:(MockPoolConnector*)mockPoolConnector willSendProtein:(OBProtein*)protein;

@end
