//
//  MockProteinSource.m
//  MezzKit
//
//  Created by Zai Chang on 6/5/15.
//  Copyright (c) 2015 Oblong Industries. All rights reserved.
//

#import "MockProteinSource.h"
#import "MZCommunicator+Constants.h"
#import "MZProtoFactory.h"
#import "MZInfopresenceCall.h"


@implementation MockProteinSource

- (NSString *)proteinKeyForTransactionName:(NSString *)transactionName responseType:(NSString *)responseType
{
  return [super proteinKeyForTransactionName:transactionName responseType:responseType];
}


- (id)parameterForKey:(NSString *)key
{
  return [super parameterForKey:key];
}



- (OBProtein *)proteinForKey:(NSString *)key
{
  return  [super proteinForKey:key];
}

@end

