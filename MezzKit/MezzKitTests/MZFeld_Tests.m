//
//  MZFeld_Tests.m
//  MezzKit
//
//  Created by miguel on 28/2/17.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MZFeld.h"
#import "MockProteinSource.h"

@interface MZFeld_Tests : XCTestCase
{
  MZFeld *_feld;
}

@end

@implementation MZFeld_Tests

- (void)setUp
{
    [super setUp];
  // Put setup code here.
}

- (void)tearDown
{
    // Put teardown code here.
    [super tearDown];
}

// Mark - Feld Rect Vect calculations Tests

// Consider a room with V(0,0,0) as its center point and four felds in each wall

- (void)testFeldRectInFrontWall
{
  MZFeld *feld = [MZFeld feldWithName:@"front"];
  feld.over = [MZVect doubleVectWithX:1 y:0 z:0];
  feld.up = [MZVect doubleVectWithX:0 y:1 z:0];
  feld.physicalSize = [MZVect doubleVectWithX:160 y:90];
  feld.center = [MZVect doubleVectWithX:200 y:300 z:70];

  XCTAssertTrue(CGRectEqualToRect(feld.feldRect, CGRectMake(120, 255, 160, 90)));
}

- (void)testFeldRectInRightWall
{
  MZFeld *feld = [MZFeld feldWithName:@"right"];
  feld.over = [MZVect doubleVectWithX:0 y:0 z:-1];
  feld.up = [MZVect doubleVectWithX:0 y:1 z:0];
  feld.physicalSize = [MZVect doubleVectWithX:160 y:90];
  feld.center = [MZVect doubleVectWithX:200 y:300 z:-150];

  XCTAssertTrue(CGRectEqualToRect(feld.feldRect, CGRectMake(70, 255, 160, 90)));
}


- (void)testFeldRectInBehindWall
{
  MZFeld *feld = [MZFeld feldWithName:@"behind"];
  feld.over = [MZVect doubleVectWithX:-1 y:0 z:0];
  feld.up = [MZVect doubleVectWithX:0 y:1 z:0];
  feld.physicalSize = [MZVect doubleVectWithX:160 y:90];
  feld.center = [MZVect doubleVectWithX:200 y:300 z:-320];

  XCTAssertTrue(CGRectEqualToRect(feld.feldRect, CGRectMake(-280, 255, 160, 90)));
}

- (void)testFeldRectInLeftWall
{
  MZFeld *feld = [MZFeld feldWithName:@"left"];
  feld.over = [MZVect doubleVectWithX:0 y:0 z:1];
  feld.up = [MZVect doubleVectWithX:0 y:1 z:0];
  feld.physicalSize = [MZVect doubleVectWithX:160 y:90];
  feld.center = [MZVect doubleVectWithX:400 y:300 z:-150];

  XCTAssertTrue(CGRectEqualToRect(feld.feldRect, CGRectMake(-230, 255, 160, 90)));
}


// Mark - Feld Visibility

- (void)testOrderedFeldsArrayFromDictionaryUntilMezzV320AllVisible
{
  MockProteinSource *proteinSource = [[MockProteinSource alloc] init];
  NSDictionary *leftFeldDict = [proteinSource parameterForKey:kCanonicalParameterFeldLeftKey];
  NSDictionary *mainFeldDict = [proteinSource parameterForKey:kCanonicalParameterFeldMainKey];
  NSDictionary *rightFeldDict = [proteinSource parameterForKey:kCanonicalParameterFeldRightKey];

  NSDictionary *feldsDictionary = @{leftFeldDict[@"id"]:leftFeldDict,
                                    mainFeldDict[@"id"]:mainFeldDict,
                                    rightFeldDict[@"id"]:rightFeldDict};

  NSArray *felds =[MZFeld orderedFeldsArrayFromDictionary:feldsDictionary];

  XCTAssertEqual(felds.count, 3);
  XCTAssertTrue([[felds[0] name] isEqualToString:leftFeldDict[@"id"]]);
  XCTAssertTrue([[felds[1] name] isEqualToString:mainFeldDict[@"id"]]);
  XCTAssertTrue([[felds[2] name] isEqualToString:rightFeldDict[@"id"]]);
}


- (void)testOrderedFeldsArrayFromDictionaryUntilMezzV320AllVisibleSpatiallyUnordered
{
  MockProteinSource *proteinSource = [[MockProteinSource alloc] init];
  NSDictionary *leftFeldDict = [proteinSource parameterForKey:kCanonicalParameterFeldLeftKey];
  NSDictionary *mainFeldDict = [proteinSource parameterForKey:kCanonicalParameterFeldMainKey];
  NSDictionary *rightFeldDict = [proteinSource parameterForKey:kCanonicalParameterFeldRightKey];

  NSDictionary *feldsDictionary = @{rightFeldDict[@"id"]:rightFeldDict,
                                    mainFeldDict[@"id"]:mainFeldDict,
                                    leftFeldDict[@"id"]:leftFeldDict};

  NSArray *felds =[MZFeld orderedFeldsArrayFromDictionary:feldsDictionary];

  XCTAssertEqual(felds.count, 3);
  XCTAssertTrue([[felds[0] name] isEqualToString:leftFeldDict[@"id"]]);
  XCTAssertTrue([[felds[1] name] isEqualToString:mainFeldDict[@"id"]]);
  XCTAssertTrue([[felds[2] name] isEqualToString:rightFeldDict[@"id"]]);
}


- (void)testOrderedFeldsArrayFromDictionaryWithVisibilityUntilMezzV320SomeInvisible
{
  MockProteinSource *proteinSource = [[MockProteinSource alloc] init];
  NSMutableDictionary *leftFeldDict = [[NSMutableDictionary alloc] initWithDictionary:[proteinSource parameterForKey:kCanonicalParameterFeldLeftKey]];
  NSMutableDictionary *mainFeldDict = [[NSMutableDictionary alloc] initWithDictionary:[proteinSource parameterForKey:kCanonicalParameterFeldMainKey]];
  NSMutableDictionary *rightFeldDict = [[NSMutableDictionary alloc] initWithDictionary:[proteinSource parameterForKey:kCanonicalParameterFeldRightKey]];

  leftFeldDict[@"type"] = @"invisible";
  rightFeldDict[@"type"] = @"invisible";

  NSDictionary *feldsDictionary = @{leftFeldDict[@"id"]:leftFeldDict,
                                    mainFeldDict[@"id"]:mainFeldDict,
                                    rightFeldDict[@"id"]:rightFeldDict};

  NSArray *feldsWithInvisible =[MZFeld orderedFeldsArrayFromDictionary:feldsDictionary includeInvisible:YES];

  XCTAssertEqual(feldsWithInvisible.count, 3);
  XCTAssertTrue([[feldsWithInvisible[0] name] isEqualToString:leftFeldDict[@"id"]]);
  XCTAssertTrue([[feldsWithInvisible[1] name] isEqualToString:mainFeldDict[@"id"]]);
  XCTAssertTrue([[feldsWithInvisible[2] name] isEqualToString:rightFeldDict[@"id"]]);

  NSArray *feldsWithoutInvisible =[MZFeld orderedFeldsArrayFromDictionary:feldsDictionary includeInvisible:NO];

  XCTAssertEqual(feldsWithoutInvisible.count, 1);
  XCTAssertTrue([[feldsWithoutInvisible[0] name] isEqualToString:mainFeldDict[@"id"]]);
}


- (void)testOrderedFeldsArrayFromDictionaryWithVisibilityUntilMezzV320SomeInvisibleSpatiallyUnordered
{
  MockProteinSource *proteinSource = [[MockProteinSource alloc] init];
  NSMutableDictionary *leftFeldDict = [[NSMutableDictionary alloc] initWithDictionary:[proteinSource parameterForKey:kCanonicalParameterFeldLeftKey]];
  NSMutableDictionary *mainFeldDict = [[NSMutableDictionary alloc] initWithDictionary:[proteinSource parameterForKey:kCanonicalParameterFeldMainKey]];
  NSMutableDictionary *rightFeldDict = [[NSMutableDictionary alloc] initWithDictionary:[proteinSource parameterForKey:kCanonicalParameterFeldRightKey]];

  leftFeldDict[@"type"] = @"invisible";
  rightFeldDict[@"type"] = @"invisible";

  NSDictionary *feldsDictionary = @{rightFeldDict[@"id"]:rightFeldDict,
                                    mainFeldDict[@"id"]:mainFeldDict,
                                    leftFeldDict[@"id"]:leftFeldDict};

  NSArray *feldsWithInvisible =[MZFeld orderedFeldsArrayFromDictionary:feldsDictionary includeInvisible:YES];

  XCTAssertEqual(feldsWithInvisible.count, 3);
  XCTAssertTrue([[feldsWithInvisible[0] name] isEqualToString:leftFeldDict[@"id"]]);
  XCTAssertTrue([[feldsWithInvisible[1] name] isEqualToString:mainFeldDict[@"id"]]);
  XCTAssertTrue([[feldsWithInvisible[2] name] isEqualToString:rightFeldDict[@"id"]]);

  NSArray *feldsWithoutInvisible =[MZFeld orderedFeldsArrayFromDictionary:feldsDictionary includeInvisible:NO];

  XCTAssertEqual(feldsWithoutInvisible.count, 1);
  XCTAssertTrue([[feldsWithoutInvisible[0] name] isEqualToString:mainFeldDict[@"id"]]);
}


- (void)testOrderedFeldsArrayFromDictionaryWithVisibilityAfterMezzV320AllVisible
{
  MockProteinSource *proteinSource = [[MockProteinSource alloc] init];
  NSMutableDictionary *leftFeldDict = [[NSMutableDictionary alloc] initWithDictionary:[proteinSource parameterForKey:kCanonicalParameterFeldLeftKey]];
  NSMutableDictionary *mainFeldDict = [[NSMutableDictionary alloc] initWithDictionary:[proteinSource parameterForKey:kCanonicalParameterFeldMainKey]];
  NSMutableDictionary *rightFeldDict = [[NSMutableDictionary alloc] initWithDictionary:[proteinSource parameterForKey:kCanonicalParameterFeldRightKey]];

  leftFeldDict[@"type"] = @"visible";
  mainFeldDict[@"type"] = @"visible";
  rightFeldDict[@"type"] = @"visible";

  leftFeldDict[@"visibility"] = @"all";
  mainFeldDict[@"visibility"] = @"all";
  rightFeldDict[@"visibility"] = @"all";

  NSDictionary *feldsDictionary = @{leftFeldDict[@"id"]:leftFeldDict,
                                    mainFeldDict[@"id"]:mainFeldDict,
                                    rightFeldDict[@"id"]:rightFeldDict};

  NSArray *feldsWithInvisible =[MZFeld orderedFeldsArrayFromDictionary:feldsDictionary includeInvisible:YES];

  XCTAssertEqual(feldsWithInvisible.count, 3);
  XCTAssertTrue([[feldsWithInvisible[0] name] isEqualToString:leftFeldDict[@"id"]]);
  XCTAssertTrue([[feldsWithInvisible[1] name] isEqualToString:mainFeldDict[@"id"]]);
  XCTAssertTrue([[feldsWithInvisible[2] name] isEqualToString:rightFeldDict[@"id"]]);

  NSArray *feldsWithoutInvisible =[MZFeld orderedFeldsArrayFromDictionary:feldsDictionary includeInvisible:NO];

  XCTAssertEqual(feldsWithoutInvisible.count, 3);
  XCTAssertTrue([[feldsWithoutInvisible[0] name] isEqualToString:leftFeldDict[@"id"]]);
  XCTAssertTrue([[feldsWithoutInvisible[1] name] isEqualToString:mainFeldDict[@"id"]]);
  XCTAssertTrue([[feldsWithoutInvisible[2] name] isEqualToString:rightFeldDict[@"id"]]);
}


- (void)testOrderedFeldsArrayFromDictionaryWithVisibilityAfterMezzV320SomeVisible
{
  MockProteinSource *proteinSource = [[MockProteinSource alloc] init];
  NSMutableDictionary *leftFeldDict = [[NSMutableDictionary alloc] initWithDictionary:[proteinSource parameterForKey:kCanonicalParameterFeldLeftKey]];
  NSMutableDictionary *mainFeldDict = [[NSMutableDictionary alloc] initWithDictionary:[proteinSource parameterForKey:kCanonicalParameterFeldMainKey]];
  NSMutableDictionary *rightFeldDict = [[NSMutableDictionary alloc] initWithDictionary:[proteinSource parameterForKey:kCanonicalParameterFeldRightKey]];

  leftFeldDict[@"type"] = @"visible";
  mainFeldDict[@"type"] = @"visible";
  rightFeldDict[@"type"] = @"visible";

  leftFeldDict[@"visibility"] = @"some";
  mainFeldDict[@"visibility"] = @"all";
  rightFeldDict[@"visibility"] = @"some";

  NSDictionary *feldsDictionary = @{leftFeldDict[@"id"]:leftFeldDict,
                                    mainFeldDict[@"id"]:mainFeldDict,
                                    rightFeldDict[@"id"]:rightFeldDict};

  NSArray *feldsWithInvisible =[MZFeld orderedFeldsArrayFromDictionary:feldsDictionary includeInvisible:YES];

  XCTAssertEqual(feldsWithInvisible.count, 3);
  XCTAssertTrue([[feldsWithInvisible[0] name] isEqualToString:leftFeldDict[@"id"]]);
  XCTAssertTrue([[feldsWithInvisible[1] name] isEqualToString:mainFeldDict[@"id"]]);
  XCTAssertTrue([[feldsWithInvisible[2] name] isEqualToString:rightFeldDict[@"id"]]);

  NSArray *feldsWithoutInvisible =[MZFeld orderedFeldsArrayFromDictionary:feldsDictionary includeInvisible:NO];

  XCTAssertEqual(feldsWithoutInvisible.count, 3);
  XCTAssertTrue([[feldsWithoutInvisible[0] name] isEqualToString:leftFeldDict[@"id"]]);
  XCTAssertTrue([[feldsWithoutInvisible[1] name] isEqualToString:mainFeldDict[@"id"]]);
  XCTAssertTrue([[feldsWithoutInvisible[2] name] isEqualToString:rightFeldDict[@"id"]]);
}

- (void)testOrderedFeldsArrayFromDictionaryWithVisibilityAfterMezzV320InvisibleForAllOrForSome
{
  MockProteinSource *proteinSource = [[MockProteinSource alloc] init];
  NSMutableDictionary *leftFeldDict = [[NSMutableDictionary alloc] initWithDictionary:[proteinSource parameterForKey:kCanonicalParameterFeldLeftKey]];
  NSMutableDictionary *mainFeldDict = [[NSMutableDictionary alloc] initWithDictionary:[proteinSource parameterForKey:kCanonicalParameterFeldMainKey]];
  NSMutableDictionary *rightFeldDict = [[NSMutableDictionary alloc] initWithDictionary:[proteinSource parameterForKey:kCanonicalParameterFeldRightKey]];

  leftFeldDict[@"type"] = @"invisible";
  mainFeldDict[@"type"] = @"visible";
  rightFeldDict[@"type"] = @"invisible";

  leftFeldDict[@"visibility"] = @"none";
  mainFeldDict[@"visibility"] = @"all";
  rightFeldDict[@"visibility"] = @"some";

  NSDictionary *feldsDictionary = @{leftFeldDict[@"id"]:leftFeldDict,
                                    mainFeldDict[@"id"]:mainFeldDict,
                                    rightFeldDict[@"id"]:rightFeldDict};

  NSArray *feldsWithInvisible =[MZFeld orderedFeldsArrayFromDictionary:feldsDictionary includeInvisible:YES];

  XCTAssertEqual(feldsWithInvisible.count, 2);
  XCTAssertTrue([[feldsWithInvisible[0] name] isEqualToString:mainFeldDict[@"id"]]);
  XCTAssertTrue([[feldsWithInvisible[1] name] isEqualToString:rightFeldDict[@"id"]]);

  NSArray *feldsWithoutInvisible =[MZFeld orderedFeldsArrayFromDictionary:feldsDictionary includeInvisible:NO];

  XCTAssertEqual(feldsWithoutInvisible.count, 2);
  XCTAssertTrue([[feldsWithoutInvisible[0] name] isEqualToString:mainFeldDict[@"id"]]);
  XCTAssertTrue([[feldsWithoutInvisible[1] name] isEqualToString:rightFeldDict[@"id"]]);
}


@end
