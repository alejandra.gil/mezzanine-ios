//
//  MZWindshield_Tests.m
//  MezzKit
//
//  Created by Zai Chang on 11/25/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MezzKit.h"

@interface MZWindshield_Tests : XCTestCase
{
  MZWindshield *_windshield;
  CGFloat itemsInPrimaryWindshield;
}
@end

@implementation MZWindshield_Tests

- (NSString*)itemUidForIndex:(NSInteger)i
{
  return [NSString stringWithFormat:@"ws-%ld", (long)i];
}

- (NSString*)itemAssetUidForIndex:(NSInteger)i
{
  return [NSString stringWithFormat:@"as-%ld", (long)i];
}


- (void)setUp {
  [super setUp];
  
  _windshield = [[MZWindshield alloc] init];
  MZSurface *mainSurface = [[MZSurface alloc] initWithName:@"main"];  // 6 items
  MZSurface *extendedSurface = [[MZSurface alloc] initWithName:@"extended"]; // 4 items

  itemsInPrimaryWindshield = 4;

  for (NSInteger i=0; i<10; i++)
  {
    MZWindshieldItem *item = [[MZWindshieldItem alloc] init];
    item.uid = [self itemUidForIndex:i];
    item.assetUid = [self itemAssetUidForIndex:i];
    item.surface = (i < itemsInPrimaryWindshield) ? mainSurface : extendedSurface;
    [_windshield.items addObject:item];
  }
}


- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}


- (void)testItemWithUid {
  MZWindshieldItem *itemShouldExist = [_windshield itemWithUid:[self itemUidForIndex:5]];
  XCTAssertNotNil(itemShouldExist);
  MZWindshieldItem *itemShouldntExist = [_windshield itemWithUid:[self itemUidForIndex:10]];
  XCTAssertNil(itemShouldntExist);
}


- (void)testIndexOfItemWithUid {
  NSInteger index = 5;
  XCTAssertEqual([_windshield indexOfItemWithUid:[self itemUidForIndex:index]], index);

  XCTAssertEqual([_windshield indexOfItemWithUid:@"no such uid"], NSNotFound);
}


- (void)testItemsWithContentSource {
  NSInteger indexOfItemUnderTest = 3;
  NSString *assetUid = [self itemAssetUidForIndex:indexOfItemUnderTest];
  NSArray *items = [_windshield itemsWithContentSource:assetUid];
  XCTAssertTrue(items.count == 1);
  if (items.count == 1)
  {
    XCTAssertEqualObjects([items[0] assetUid], assetUid);
  }
}


- (void)testClear {
  [_windshield clear];
  
  XCTAssertEqual(_windshield.items.count, 0);
}


- (NSArray*)windshieldItemsDictionaryArray:(NSArray*)arrayOfWindshieldItems
{
  NSMutableArray *arrayOfDictionaries = [NSMutableArray array];
  for (MZWindshieldItem *item in arrayOfWindshieldItems)
  {
    [arrayOfDictionaries addObject:[item dictionaryRepresentation]];
  }
  return arrayOfDictionaries;
}


- (void)testUpdateWithWindshieldItems {
  NSArray *originalItems = [_windshield.items copy];

  // Taking items from index 3 to index 7
  NSRange range = NSMakeRange(3, 5);
  NSArray *reducedArray = [originalItems subarrayWithRange:range];

  [_windshield updateWithWindshieldItems:[self windshieldItemsDictionaryArray:reducedArray]];
  
  XCTAssertEqual(_windshield.items.count, reducedArray.count);
  XCTAssertNil([_windshield itemWithUid:[self itemUidForIndex:2]]);
  XCTAssertNotNil([_windshield itemWithUid:[self itemUidForIndex:3]]);
  XCTAssertNotNil([_windshield itemWithUid:[self itemUidForIndex:7]]);
  XCTAssertNil([_windshield itemWithUid:[self itemUidForIndex:8]]);
  
  [_windshield updateWithWindshieldItems:[self windshieldItemsDictionaryArray:originalItems]];

  XCTAssertEqual(_windshield.items.count, originalItems.count);
  for (NSInteger i=0; i<originalItems.count; i++)
  {
    XCTAssertNotNil([_windshield itemWithUid:[self itemUidForIndex:i]]);
  }
}


- (void)testItemsInMainWindshield
{
  NSArray *inPrimaryWindshield =  [_windshield itemsInPrimaryWindshield];
  XCTAssertEqual(itemsInPrimaryWindshield, inPrimaryWindshield.count, @"There should be %f items in the primary windshield but there are %lu", itemsInPrimaryWindshield, (unsigned long)inPrimaryWindshield.count);
}


- (void)testItemsInExtendedWindshield
{
  NSArray *inExtendedWindshield =  [_windshield itemsInExtendedWindshield];
  CGFloat itemsInExtended = _windshield.items.count - itemsInPrimaryWindshield;
  XCTAssertEqual(itemsInExtended, inExtendedWindshield.count, @"There should be %f items in the extended windshield but there are %lu", itemsInExtended, (unsigned long)inExtendedWindshield.count);
}

@end
