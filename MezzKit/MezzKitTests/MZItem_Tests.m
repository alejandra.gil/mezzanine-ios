//
//  MZItem_Tests.m
//  MezzKit
//
//  Created by Zai Chang on 11/25/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MezzKit.h"

@interface MZItem_Tests : XCTestCase
{
  MZItem *_item;
}
@end

@implementation MZItem_Tests

- (void)setUp {
  [super setUp];

  _item = [[MZItem alloc] init];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}


- (void)testSetContentSourceAsset
{
  _item.contentSource = @"as-someassetuuid";
  
  XCTAssertFalse(_item.isVideo);
  XCTAssertNil(_item.viddleName);
}


- (void)testSetContentSourceVideo
{
  _item.contentSource = @"vi-somevideouuid";
  
  XCTAssertTrue(_item.isVideo);
  XCTAssertNotNil(_item.viddleName);
}


- (void)testAddAnnotation
{
  MZAnnotation *annotation = [[MZAnnotation alloc] init];
  [_item addAnnotation:annotation];
  XCTAssertTrue(_item.annotations.count == 1);
}


- (NSMutableArray*)createAnnotationArray:(NSInteger)count
{
  NSMutableArray *newAnnotations = [NSMutableArray array];
  for (NSInteger i=0; i<count; i++)
  {
    MZAnnotation *annotation = [[MZAnnotation alloc] init];
    [newAnnotations addObject:annotation];
  }
  return newAnnotations;
}


- (void)testAddAnnotationsArray
{
  NSInteger annotationCount = 12;
  NSArray *newAnnotations = [self createAnnotationArray:annotationCount];
  [_item addAnnotationsArray:newAnnotations];

  XCTAssertTrue(_item.annotations.count == annotationCount);
}


- (void)testRemoveAnnotationThatExists
{
  NSInteger annotationCount = 12;
  NSMutableArray *newAnnotations = [self createAnnotationArray:annotationCount];
  _item.annotations = newAnnotations;
  
  MZAnnotation *annotationToRemove = _item.annotations[_item.annotations.count * rand() / NSIntegerMax];
  [_item removeAnnotation:annotationToRemove];
  
  XCTAssertTrue(_item.annotations.count == annotationCount - 1);
  XCTAssertFalse([_item.annotations containsObject:annotationToRemove]);
}


- (void)testRemoveAnnotationsArray
{
  NSInteger annotationCount = 12;
  NSMutableArray *newAnnotations = [self createAnnotationArray:annotationCount];
  _item.annotations = newAnnotations;

  NSArray *annotationsToRemove = [newAnnotations subarrayWithRange:NSMakeRange(3, 6)];
  [_item removeAnnotationsArray:annotationsToRemove];

  XCTAssertTrue(_item.annotations.count == annotationCount - annotationsToRemove.count);
  
  for (MZAnnotation *removedAnnotation in annotationsToRemove) {
    XCTAssertFalse([_item.annotations containsObject:removedAnnotation]);
  }
}


- (void)testClearAnnotations
{
  NSInteger annotationCount = 12;
  NSMutableArray *newAnnotations = [self createAnnotationArray:annotationCount];
  _item.annotations = newAnnotations;
  
  [_item clearAnnotations];
  
  XCTAssertTrue(_item.annotations.count == 0);
}

@end
