//
//  MZVect_Tests.m
//  MezzKit
//
//  Created by Miguel Sanchez Valdes on 30/03/2019.
//  Copyright © 2019 Oblong Industries. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MZVect.h"

@interface MZVect_Tests : XCTestCase

@end

@implementation MZVect_Tests

- (void)testVectTypeCheckers
{
  MZVect *integerVect = [MZVect integerVectWithX:1 y:1];
  XCTAssertTrue([integerVect isIntegerVect]);
  XCTAssertFalse([integerVect isDoubleVect]);

  MZVect *doubleVect = [MZVect doubleVectWithX:1.0 y:1.0];
  XCTAssertFalse([doubleVect isIntegerVect]);
  XCTAssertTrue([doubleVect isDoubleVect]);
}

- (void)testValues
{
  MZVect *integerVect = [MZVect integerVectWithX:1 y:9];
  XCTAssertTrue(integerVect.x.integerValue == 1);
  XCTAssertTrue(integerVect.y.integerValue == 9);
  XCTAssertTrue(integerVect.x.doubleValue == 1.0);
  XCTAssertTrue(integerVect.y.doubleValue == 9.0);
  XCTAssertFalse(integerVect.z.integerValue);
  XCTAssertTrue(integerVect.z == nil);

  MZVect *doubleVect = [MZVect doubleVectWithX:1.3 y:23.89];
  XCTAssertTrue(doubleVect.x.integerValue == 1);
  XCTAssertTrue(doubleVect.y.integerValue == 23);
  XCTAssertTrue(doubleVect.x.doubleValue == 1.3);
  XCTAssertTrue(doubleVect.y.doubleValue == 23.89);
  XCTAssertFalse(doubleVect.z.doubleValue);
  XCTAssertTrue(doubleVect.z == nil);
}

- (void)testCrossVect
{
  MZVect *vect1 = [MZVect integerVectWithX:1 y:9];
  MZVect *vect2 = [MZVect integerVectWithX:12 y:12];
  XCTAssertNil([vect1 cross:vect2]);

  MZVect *vect3 = [MZVect integerVectWithX:1 y:9 z:3];
  MZVect *vect4 = [MZVect integerVectWithX:15 y:22 z:10];
  MZVect *cross34 = [vect3 cross:vect4];
  XCTAssertNotNil(cross34);
  XCTAssertTrue(cross34.x.integerValue == 24);
  XCTAssertTrue(cross34.y.integerValue == 35);
  XCTAssertTrue(cross34.z.integerValue == -113);


  MZVect *vect5 = [MZVect doubleVectWithX:12.56 y:87.65 z:-12.12];
  MZVect *vect6 = [MZVect doubleVectWithX:3.56 y:-2.34 z:9.98];
  MZVect *cross56 = [vect5 cross:vect6];
  XCTAssertNotNil(cross56);
  XCTAssertEqualWithAccuracy(cross56.x.doubleValue, 846.3862, 0.000000001);
  XCTAssertEqualWithAccuracy(cross56.y.doubleValue, -168.496, 0.000000001);
  XCTAssertEqualWithAccuracy(cross56.z.doubleValue, -341.4244, 0.000000001);

  MZVect *cross36 = [vect3 cross:vect6];
  XCTAssertNotNil(cross36);
  XCTAssertEqualWithAccuracy(cross36.x.doubleValue, 96.84, 0.000000001);
  XCTAssertEqualWithAccuracy(cross36.y.doubleValue, 0.7, 0.000000001);
  XCTAssertEqualWithAccuracy(cross36.z.doubleValue, -34.38, 0.000000001);
}

- (void)testDotVect
{
  MZVect *vect1 = [MZVect integerVectWithX:3 y:12];
  MZVect *vect2 = [MZVect integerVectWithX:5 y:7];
  MZVect *vect3 = [MZVect doubleVectWithX:12.56 y:87.65 z:-12.12];
  MZVect *vect4 = [MZVect doubleVectWithX:3.56 y:-2.34 z:9.98];

  XCTAssertEqual([vect1 integerVectDot:vect2], 99);
  XCTAssertEqualWithAccuracy([vect3 doubleVectDot:vect4], -281.345, 0.000000001);
}

- (void)testEquality
{
  MZVect *vect1 = [MZVect integerVectWithX:3 y:12];
  MZVect *vect2 = [MZVect integerVectWithX:5 y:7];
  MZVect *vect3 = [MZVect doubleVectWithX:12.56 y:87.65 z:-12.12];
  MZVect *vect4 = [MZVect doubleVectWithX:3.56 y:-2.34 z:9.98];

  XCTAssertFalse([vect1 equalToVect:vect2]);
  XCTAssertFalse([vect1 equalToVect:vect3]);
  XCTAssertFalse([vect1 equalToVect:vect4]);

  XCTAssertFalse([vect2 equalToVect:vect1]);
  XCTAssertFalse([vect2 equalToVect:vect3]);
  XCTAssertFalse([vect2 equalToVect:vect4]);

  XCTAssertFalse([vect3 equalToVect:vect1]);
  XCTAssertFalse([vect3 equalToVect:vect2]);
  XCTAssertFalse([vect3 equalToVect:vect4]);

  XCTAssertFalse([vect4 equalToVect:vect1]);
  XCTAssertFalse([vect4 equalToVect:vect2]);
  XCTAssertFalse([vect4 equalToVect:vect3]);

  MZVect *vect1bis = [MZVect integerVectWithX:3 y:12];
  MZVect *vect1bisDouble = [MZVect doubleVectWithX:3 y:12];
  MZVect *vect3bis = [MZVect doubleVectWithX:12.56 y:87.65 z:-12.12];
  MZVect *vect3bisInteger = [MZVect integerVectWithX:12.56 y:87.65 z:-12.12];

  XCTAssertTrue([vect1 equalToVect:vect1bis]);
  XCTAssertFalse([vect1 equalToVect:vect1bisDouble]);

  XCTAssertTrue([vect3 equalToVect:vect3bis]);
  XCTAssertFalse([vect3 equalToVect:vect3bisInteger]);
}

@end
