//
//  NSMutableArray+Additions_Tests.m
//  MezzKit
//
//  Created by miguel on 28/9/15.
//  Copyright (c) 2015 Oblong Industries. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MezzKit.h"

@interface NSMutableArray_Additions_Tests : XCTestCase
{
  NSMutableArray *mutableArray;
}

@end

@implementation NSMutableArray_Additions_Tests

- (void)setUp
{
  [super setUp];

  mutableArray = @[].mutableCopy;
  for (NSInteger i = 0 ; i < 5 ; i++)
    [mutableArray addObject:[NSNumber numberWithInteger:i]];
}

- (void)tearDown
{
  [super tearDown];
}

- (void)testMoveOneUpItem
{
  [mutableArray moveObjectAtIndex:0 toIndex:1];

  XCTAssertEqual(1, [mutableArray[0] integerValue]);
  XCTAssertEqual(0, [mutableArray[1] integerValue]);
  XCTAssertEqual(2, [mutableArray[2] integerValue]);
  XCTAssertEqual(3, [mutableArray[3] integerValue]);
  XCTAssertEqual(4, [mutableArray[4] integerValue]);
}

- (void)testMoveOneDownItem
{
  [mutableArray moveObjectAtIndex:1 toIndex:0];

  XCTAssertEqual(1, [mutableArray[0] integerValue]);
  XCTAssertEqual(0, [mutableArray[1] integerValue]);
  XCTAssertEqual(2, [mutableArray[2] integerValue]);
  XCTAssertEqual(3, [mutableArray[3] integerValue]);
  XCTAssertEqual(4, [mutableArray[4] integerValue]);

}

- (void)testMoveHeadToTail
{
  [mutableArray moveObjectAtIndex:0 toIndex:4];

  XCTAssertEqual(1, [mutableArray[0] integerValue]);
  XCTAssertEqual(2, [mutableArray[1] integerValue]);
  XCTAssertEqual(3, [mutableArray[2] integerValue]);
  XCTAssertEqual(4, [mutableArray[3] integerValue]);
  XCTAssertEqual(0, [mutableArray[4] integerValue]);
}

- (void)testMoveTailToHead
{
  [mutableArray moveObjectAtIndex:2 toIndex:0];

  XCTAssertEqual(2, [mutableArray[0] integerValue]);
  XCTAssertEqual(0, [mutableArray[1] integerValue]);
  XCTAssertEqual(1, [mutableArray[2] integerValue]);
  XCTAssertEqual(3, [mutableArray[3] integerValue]);
  XCTAssertEqual(4, [mutableArray[4] integerValue]);
}

- (void)testMoveMidItemToTail
{
  [mutableArray moveObjectAtIndex:2 toIndex:4];

  XCTAssertEqual(0, [mutableArray[0] integerValue]);
  XCTAssertEqual(1, [mutableArray[1] integerValue]);
  XCTAssertEqual(3, [mutableArray[2] integerValue]);
  XCTAssertEqual(4, [mutableArray[3] integerValue]);
  XCTAssertEqual(2, [mutableArray[4] integerValue]);
}

- (void)testMoveItemAtNonValidIndexToValidIndex
{
  XCTAssertThrows([mutableArray moveObjectAtIndex:10 toIndex:4]);

  XCTAssertEqual(0, [mutableArray[0] integerValue]);
  XCTAssertEqual(1, [mutableArray[1] integerValue]);
  XCTAssertEqual(2, [mutableArray[2] integerValue]);
  XCTAssertEqual(3, [mutableArray[3] integerValue]);
  XCTAssertEqual(4, [mutableArray[4] integerValue]);
}

- (void)testMoveItemAtValidIndexToNonValidIndex
{
  XCTAssertThrows([mutableArray moveObjectAtIndex:4 toIndex:10]);

  XCTAssertEqual(0, [mutableArray[0] integerValue]);
  XCTAssertEqual(1, [mutableArray[1] integerValue]);
  XCTAssertEqual(2, [mutableArray[2] integerValue]);
  XCTAssertEqual(3, [mutableArray[3] integerValue]);
  XCTAssertEqual(4, [mutableArray[4] integerValue]);
}

- (void)testMoveItemAtNonValidIndexToNonValidIndex
{
  XCTAssertThrows([mutableArray moveObjectAtIndex:14 toIndex:10]);

  XCTAssertEqual(0, [mutableArray[0] integerValue]);
  XCTAssertEqual(1, [mutableArray[1] integerValue]);
  XCTAssertEqual(2, [mutableArray[2] integerValue]);
  XCTAssertEqual(3, [mutableArray[3] integerValue]);
  XCTAssertEqual(4, [mutableArray[4] integerValue]);
}


@end
