//
//  MZCanonicalProteinConstructor.h
//  MezzKit
//
//  Created by Zai Chang on 5/10/13.
//  Copyright (c) 2013 Oblong Industries. All rights reserved.
//

#import "MZProtoFactory.h"
@class MZProteinFactory;

extern NSString* const kCanonicalParameterProvenanceKey;

extern NSString* const kCanonicalParameterCorrectPassphraseKey;
extern NSString* const kCanonicalParameterIncorrectPassphraseKey;

extern NSString* const kCanonicalParameterLoginUsernameKey;
extern NSString* const kCanonicalParameterLoginCorrectPasswordKey;
extern NSString* const kCanonicalParameterLoginIncorrectPasswordKey;

extern NSString* const kCanonicalParameterPasskeyKey;

extern NSString* const kCanonicalParameterUploadMaxWidth;
extern NSString* const kCanonicalParameterUploadMaxHeight;
extern NSString* const kCanonicalParameterUploadImageSizeMB;
extern NSString* const kCanonicalParameterUploadPDFSizeMB;
extern NSString* const kCanonicalParameterUploadSizeMB;

extern NSString* const kCanonicalParameterInfopresenceParticipant1Key;
extern NSString* const kCanonicalParameterInfopresenceParticipant2Key;
extern NSString* const kCanonicalParameterInfopresenceParticipant3Key;
extern NSString* const kCanonicalParameterInfopresenceParticipant4Key;
extern NSString* const kCanonicalParameterInfopresenceIncomingJoinRequestUidKey;
extern NSString* const kCanonicalParameterInfopresenceOutgoingJoinRequestUidKey;
extern NSString* const kCanonicalParameterInfopresenceIncomingInviteUidKey;
extern NSString* const kCanonicalParameterInfopresenceOutgoingInviteUidKey;
extern NSString* const kCanonicalParameterInfopresenceRemoteParticipant1Key;
extern NSString* const kCanonicalParameterInfopresenceRemoteParticipant2Key;
extern NSString* const kCanonicalParameterInfopresenceVideoChatPexipService;
extern NSString* const kCanonicalParameterInfopresenceVideoChatPexipConferenceUid;
extern NSString* const kCanonicalParameterInfopresenceVideoChatPexipNodeUid;

extern NSString* const kCanonicalParameterMeetingRoomParticipant1Key;
extern NSString* const kCanonicalParameterMeetingRoomParticipant2Key;
extern NSString* const kCanonicalParameterMeetingRoomParticipantLocalKey;
extern NSString* const kCanonicalParameterMeetingRoomParticipantCloudKey;
extern NSString* const kCanonicalParameterMeetingParticipantLocalKey;
extern NSString* const kCanonicalParameterMeetingParticipantRoom1Key;
extern NSString* const kCanonicalParameterMeetingParticipantRoom2Key;
extern NSString* const kCanonicalParameterMeetingParticipantRemoterKey;

extern NSString* const kCanonicalParameterWorkspaceNameKey;
extern NSString* const kCanonicalParameterWorkspaceUidKey;
extern NSString* const kCanonicalParameterWorkspaceOwnerKey;
extern NSString* const kCanonicalParameterWorkspaceOldUidKey;
extern NSString* const kCanonicalParameterPublicWorkspaceUidKey;
extern NSString* const kCanonicalParameterPrivateWorkspaceUidKey;
extern NSString* const kCanonicalParameterWorkspaceModifiedKey;

extern NSString* const kCanonicalParameterDeckScrollIndexKey;

extern NSString* const kCanonicalParameterPresentationNumberOfSlidesKey;
extern NSString* const kCanonicalParameterPresentationScrollIndexKey;

extern NSString* const kCanonicalParameterSlideUidKey;
extern NSString* const kCanonicalParameterSlideInsertionIndexKey;
extern NSString* const kCanonicalParameterSlideInsertionRightOfKey;
extern NSString* const kCanonicalParameterSlideInsertionLeftOfKey;

extern NSString* const kCanonicalParameterSlideInsertionUidKey;
extern NSString* const kCanonicalParameterSlideDeletionUidKey;
extern NSString* const kCanonicalParameterSlideReorderUidKey;

extern NSString* const kCanonicalParameterAssetUidKey;
extern NSString* const kCanonicalParameterAssetThumbnailURIKey;
extern NSString* const kCanonicalParameterAssetFullURIKey;
extern NSString* const kCanonicalParameterAssetUploadFilenameKey;
extern NSString* const kCanonicalParameterAssetUploadImageDataKey;

extern NSString* const kCanonicalParameterLiveStreamUid1Key;
extern NSString* const kCanonicalParameterLiveStreamUid2Key;
extern NSString* const kCanonicalParameterLiveStreamUid3Key;

extern NSString* const kCanonicalParameterWindshieldItemUidKey;
extern NSString* const kCanonicalParameterWindshieldItemAspectRatioKey;
extern NSString* const kCanonicalParameterWindshieldItemContentSourceKey;
extern NSString* const kCanonicalParameterWindshieldItemFeldIdKey;
extern NSString* const kCanonicalParameterWindshieldItemFeldRelativeCoordsKey;
extern NSString* const kCanonicalParameterWindshieldItemFeldRelativeSizeKey;
extern NSString* const kCanonicalParameterWindshieldItemFrameKey; // Needed still?

extern NSString* const kCanonicalParameterWhiteboardUidKey;
extern NSString* const kCanonicalParameterCollaborationUidKey;

extern NSString* const kCanonicalParameterAssetUid1Key;
extern NSString* const kCanonicalParameterAssetUid2Key;
extern NSString* const kCanonicalParameterAssetUid3Key;
extern NSString* const kCanonicalParameterAssetUid4Key;

extern NSString* const kCanonicalParameterFileId1Key;
extern NSString* const kCanonicalParameterFileId2Key;
extern NSString* const kCanonicalParameterFileId3Key;

extern NSString* const kCanonicalParameterTransactionIdKey;

extern NSString* const kCanonicalParameterItemUidKey;

extern NSString* const kCanonicalParameterFeldLeftKey;
extern NSString* const kCanonicalParameterFeldMainKey;
extern NSString* const kCanonicalParameterFeldRightKey;
extern NSString* const kCanonicalParameterBoundingFeldKey;

typedef OBProtein *(^OBProteinConstructorBlock)();


@interface MZCanonicalProteinConstructor : NSObject
{
  MZProteinFactory *proteinFactory;
  
  // Recipes map a protein ID to a function call in the factory
  NSMutableDictionary *proteinRecipes;
  
  // Parametrs that are inserted into proteins
  NSMutableDictionary *canonicalParameters;
}
@property (nonatomic, strong) NSMutableDictionary *canonicalParameters;

-(instancetype) initWithProteinFactory:(MZProteinFactory *)proteinFactory;

@end

