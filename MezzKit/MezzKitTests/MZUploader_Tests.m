//
//  MZUploader_Tests.m
//  MezzKit
//
//  Created by miguel on 1/2/19.
//  Copyright © 2019 Oblong Industries. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MockPoolConnector.h"
#import "MockProteinSource.h"

#import "MZCommunicator.h"
#import "MZCommunicator+Constants.h"
#import "MZCommunicatorRequestor.h"
#import "MZUploader.h"

@interface MZCommunicator (Internal)

-(void) setupJoinResponseHandler;
-(void) joinSuccessful;

-(void) beginListeningPoolConnectorNotifications;
-(void) endListeningPoolConnectorNotifications;

@end


@interface MZDummyUploader : MZUploader

- (void)httpsUploadFiledata:(NSData *)imageData protein:(OBProtein *)protein mimeType:(NSString *)format filename:(NSString *)filename;

@end

@implementation MZDummyUploader

// We don't need to do the actual upload in the tests so let's do nothig
- (void)httpsUploadFiledata:(NSData *)imageData protein:(OBProtein *)protein mimeType:(NSString *)format filename:(NSString *)filename
{

}

@end

@interface MZUploader_Tests : XCTestCase
{
  MZCommunicator *communicator;
  MZSystemModel *systemModel;
  MockPoolConnector *websocketPoolConnector;

  id<MockProteinSourceBaseProtocol> proteinSource;
}

@end

@implementation MZUploader_Tests

- (void)setUp
{
  [super setUp];

  communicator = [[MZCommunicator alloc] initWithServerUrl:[NSURL URLWithString:@"https://test"]];
  systemModel = [[MZSystemModel alloc] init];
  communicator.systemModel = systemModel;

  communicator.communicatorProtocol = OBCommunicatorProtocolWSS;
  websocketPoolConnector = [[MockPoolConnector alloc] initWithPool:MEZZ_WEBSOCKET];
  [communicator addPoolConnector:websocketPoolConnector forPoolName:MEZZ_WEBSOCKET];
}


- (void)tearDown
{
  [communicator endListeningPoolConnectorNotifications];
  communicator = nil;
  websocketPoolConnector = nil;

  XCTAssertTrue([communicator pendingTransactions].count == 0, @"Pending transactions should be %d, instead got %ld", 0, (long)[[communicator pendingTransactions] count]);
  [super tearDown];
}


- (void)setupCommunicatorCore
{
  systemModel.myMezzanine.version = @"3.13";
  systemModel.apiVersion = @"3.6";

  proteinSource = [[MockProteinSource alloc] init];

  communicator.provenance = [proteinSource parameterForKey:kCanonicalParameterProvenanceKey];
  communicator.proteinFactory = proteinSource;
  communicator.proteinFactory.provenance = communicator.provenance;

  // Begin listening to pool events, normally called in [OBCommunicator connect]
  [communicator beginListeningPoolConnectorNotifications];

  systemModel.state = MZSystemStateWorkspace;
}


-(void) setUpUnconnectedCommunicator
{
  [self setupCommunicatorCore];
  communicator.hasJoinedSession = NO;
  communicator.isConnected = NO;
  communicator.isConnecting = NO;
  [communicator setupJoinResponseHandler];
}


-(void) setUpConnectedCommunicator
{
  [self setupCommunicatorCore];
  [communicator joinSuccessful];

  // Force to have a dummy requestor for http upload
  MZDummyUploader *dummyUploader = [[MZDummyUploader alloc] initWithCommunicator:communicator];
  communicator.uploader = dummyUploader;
}

#pragma mark - Assets Tests

-(void) testRequestFileUpload
{
  [self setUpConnectedCommunicator];

  NSString *workspaceUid = [proteinSource parameterForKey:kCanonicalParameterWorkspaceUidKey];

  NSURL *fileURL = [NSURL fileURLWithPath:@"/temp/someimage.jpg"];
  NSString *fileType = @"image/jpeg";
  MZFileUploadInfo *uploadInfo = [[MZFileUploadInfo alloc] init];
  uploadInfo.fileURL = fileURL;
  uploadInfo.fileName = [fileURL lastPathComponent];
  uploadInfo.format = fileType;

  NSInteger sentProteinCountBeforeRequest = websocketPoolConnector.sentProteins.count;

  NSArray *pendingFileUploads = [communicator.uploader valueForKey:@"pendingFileUploads"];
  XCTAssertTrue([pendingFileUploads count] == 0, @"No pending file uploads should be present before we start");

  [communicator.uploader requestFilesUpload:@[uploadInfo]
                               workspaceUid:workspaceUid
                               limitReached:nil
                      singleUploadCompleted:nil
                               errorHandler:nil];

  XCTAssertTrue([pendingFileUploads count] == 1, @"One pending file upload should be present.");
  id fileInfo = pendingFileUploads[0];

  XCTAssertTrue([fileInfo isKindOfClass:[MZFileUploadInfo class]], @"As of 2.12 the pendingFilesUploads array should contain instances of MZFileUploadInfo.");

  MZFileUploadInfo *fileInfoChecked = (MZFileUploadInfo *)fileInfo;

  XCTAssertTrue([fileInfoChecked fileUid] > 0, @"File id should be set");
  XCTAssertEqualObjects([fileInfoChecked fileURL], fileURL, @"File url should equal.");
  XCTAssertEqualObjects([fileInfoChecked format], fileType, @"File types should equal.");

  XCTAssertTrue(websocketPoolConnector.sentProteins.count == sentProteinCountBeforeRequest+1, @"One protein should've been sent.");
  if (websocketPoolConnector.sentProteins.count == 1)
  {
    OBProtein *sentProtein = websocketPoolConnector.sentProteins[0];
    XCTAssertTrue([sentProtein containsDescrip:MZTransactionNameAssetUploadProvision], @"Send protein should be of transaction type '%@'", MZTransactionNameAssetUploadProvision);
  }
}


-(void) testRequestFileUploadBatch
{
  [self setUpConnectedCommunicator];

  NSString *workspaceUid = [proteinSource parameterForKey:kCanonicalParameterWorkspaceUidKey];

  NSMutableArray *fileInfos = [[NSMutableArray alloc] init];
  for (NSInteger i = 1 ; i <= 5 ; i++)
  {
    MZFileUploadInfo *uploadInfo = [[MZFileUploadInfo alloc] init];
    uploadInfo.fileName = [NSString stringWithFormat:@"image%ld.jpg",(long)i];
    uploadInfo.format = @"png";
    uploadInfo.imageBlock = ^{
      NSBundle *bundle = [NSBundle bundleForClass:[self class]];
      NSString *path = [bundle pathForResource:@"Test-Image" ofType:@"png"];
      MZImage *image = [[MZImage alloc] initWithContentsOfFile:path];
      return image;
    };
    [fileInfos addObject:uploadInfo];
  }

  NSInteger numberOfFiles = fileInfos.count;


  NSArray *pendingFileUploads = [communicator.uploader valueForKey:@"pendingFileUploads"];
  XCTAssertTrue([pendingFileUploads count] == 0, @"No pending file uploads should be present before we start");

  [communicator.uploader requestFilesUpload:fileInfos
                               workspaceUid:workspaceUid
                               limitReached:nil
                      singleUploadCompleted:nil
                               errorHandler:nil];

  XCTAssertTrue([pendingFileUploads count] == numberOfFiles, @"Pending file upload should be %ld, instead got %ld", (long)numberOfFiles, (long)[pendingFileUploads count]);
  if (websocketPoolConnector.sentProteins.count > 0)
  {
    OBProtein *lastProtein = websocketPoolConnector.sentProteins[websocketPoolConnector.sentProteins.count-1];

    NSArray *files = lastProtein.ingests[@"files"];
    XCTAssertTrue(files.count == fileInfos.count, @"Number of files in protein should be %d, instead got %ld", 5, (long)numberOfFiles);
  }
}

-(void) prepareFileBatchUploadTransactionForAssetUploadTests:(NSInteger)numberOfAssets limitReach:(void (^)(MZFileBatchUploadRequestTransaction *, NSArray *, MZFileBatchUploadRequestContinuationBlock))limitReachedBlock
{
  NSString *workspaceUid = [proteinSource parameterForKey:kCanonicalParameterWorkspaceUidKey];

  NSMutableArray *fileUploadInfos = [[NSMutableArray alloc] init];
  for (NSInteger i = 0 ; i < numberOfAssets ; i++)
  {
    MZFileUploadInfo *uploadInfo = [[MZFileUploadInfo alloc] init];
    uploadInfo.fileName = [NSString stringWithFormat:@"%ld.png",(long)i];
    uploadInfo.format = @"png";
    uploadInfo.imageBlock = ^{
      NSBundle *bundle = [NSBundle bundleForClass:[self class]];
      NSString *path = [bundle pathForResource:@"Test-Image" ofType:@"png"];
      MZImage *image = [[MZImage alloc] initWithContentsOfFile:path];
      return image;
    };
    uploadInfo.workspaceUid = workspaceUid;
    [fileUploadInfos addObject:uploadInfo];
  }

  [communicator.uploader requestFilesUpload:fileUploadInfos
                               workspaceUid:workspaceUid
                               limitReached:limitReachedBlock
                      singleUploadCompleted:nil
                               errorHandler:nil];
}


-(void) testRequestImageFileBatchUploadAllAccepted
{
  [self setUpConnectedCommunicator];

  NSInteger numberOfFiles = 3;
  [self prepareFileBatchUploadTransactionForAssetUploadTests:numberOfFiles limitReach:nil];

  NSArray *pendingFileUploads = [communicator.uploader valueForKey:@"pendingFileUploads"];
  NSArray *uploadTransactions = [communicator.uploader valueForKey:@"uploadTransactions"];

  XCTAssertTrue([pendingFileUploads count] == numberOfFiles, @"Pending file upload should be %ld, instead got %ld", (long)numberOfFiles, (long)[pendingFileUploads count]);

  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameAssetUploadProvision responseType:@"response?all-images-fit"];
  [websocketPoolConnector receivedProtein:[proteinSource proteinForKey:key]];

  XCTAssertTrue([pendingFileUploads count] == 0, @"Pending file uploads should be present before we start");

  NSLog(@"Number of uploads in progress %lu", (unsigned long) uploadTransactions.count);

  XCTAssertTrue(uploadTransactions.count == 1, @"Progressive upload transactions should be %d, instead got %ld", 1, uploadTransactions.count);

  [NSThread sleepForTimeInterval:3.0];

  MZFileBatchUploadRequestTransaction *progressiveTransaction = uploadTransactions[0];

  XCTAssertTrue(progressiveTransaction.transactionsInProgress.count == numberOfFiles, @"Transactions in progress should be %ld, instead got %ld", (long)numberOfFiles, (long)progressiveTransaction.transactionsInProgress.count);

  for (NSInteger i = 0 ; i < numberOfFiles; i++)
  {
    NSString *responseType = [NSString stringWithFormat:@"psa?image-asset-%ld", (long)i+1];
    NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameAssetRefresh responseType:responseType];
    OBProtein *p = [proteinSource proteinForKey:key];
    [websocketPoolConnector receivedProtein:p];

    XCTAssertTrue(progressiveTransaction.transactionsInProgress.count == numberOfFiles-1-i, @"Transactions in progress should be %ld, instead got %ld", (long)numberOfFiles-1-i, (long)progressiveTransaction.transactionsInProgress.count);
  }

  XCTAssertTrue(uploadTransactions.count == 0, @"Progressive upload transactions should be %d, instead got %ld", 0, (long)[uploadTransactions count]);

}

-(void) testRequestImageFileBatchUploadSomeAccepted
{
  [self setUpConnectedCommunicator];

  MZFileBatchUploadRequestLimitReachedBlock limitReached = ^(MZFileBatchUploadRequestTransaction *transaction, NSArray *assetUids, MZFileBatchUploadRequestContinuationBlock continuationBlock) {
    continuationBlock ((MZFileBatchUploadRequestTransaction *)transaction, assetUids, @YES);
  };

  NSInteger numberOfFiles = 3;
  NSInteger numberOfFilesThatCanBeUploaded = 2;

  [self prepareFileBatchUploadTransactionForAssetUploadTests:numberOfFiles limitReach:limitReached];

  NSArray *pendingFileUploads = [communicator.uploader valueForKey:@"pendingFileUploads"];
  NSArray *uploadTransactions = [communicator.uploader valueForKey:@"uploadTransactions"];
  XCTAssertTrue([pendingFileUploads count] == numberOfFiles, @"Pending file upload should be %ld, instead got %ld", (long)numberOfFiles, (long)[pendingFileUploads count]);

  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameAssetUploadProvision responseType:@"response?some-images-fit"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];

  XCTAssertTrue([pendingFileUploads count] == 0, @"No pending file uploads should be present before we start");

  [NSThread sleepForTimeInterval:3.0];

  MZFileBatchUploadRequestTransaction *progressiveTransaction = uploadTransactions[0];

  XCTAssertTrue(progressiveTransaction.transactionsInProgress.count == numberOfFilesThatCanBeUploaded, @"Transactions in progress should be %ld, instead got %ld", (long)numberOfFilesThatCanBeUploaded, (long)progressiveTransaction.transactionsInProgress.count);

  for (NSInteger i = 0 ; i < numberOfFilesThatCanBeUploaded ; i++)
  {
    NSString *responseType = [NSString stringWithFormat:@"psa?image-asset-%ld", (long)i+1];
    NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameAssetRefresh responseType:responseType];
    [websocketPoolConnector receivedProtein:[proteinSource proteinForKey:key]];

    XCTAssertTrue(progressiveTransaction.transactionsInProgress.count == numberOfFilesThatCanBeUploaded-1-i, @"Transactions in progress should be %ld, instead got %ld", (long)numberOfFilesThatCanBeUploaded-1-i, (long)progressiveTransaction.transactionsInProgress.count);
  }

  XCTAssertTrue(uploadTransactions.count == 0, @"Progressive upload transactions should be %d, instead got %ld", 0, (long)[uploadTransactions count]);
}


-(void) testRequestImageFileBatchUploadNoneAccepted
{
  [self setUpConnectedCommunicator];
  NSInteger numberOfFiles = 3;
  [self prepareFileBatchUploadTransactionForAssetUploadTests:numberOfFiles limitReach:nil];

  NSArray *pendingFileUploads = [communicator.uploader valueForKey:@"pendingFileUploads"];
  NSArray *uploadTransactions = [communicator.uploader valueForKey:@"uploadTransactions"];
  XCTAssertTrue([pendingFileUploads count] == numberOfFiles, @"Pending file upload should be %ld, instead got %ld", (long)numberOfFiles, (long)[pendingFileUploads count]);

  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameAssetUploadProvision responseType:@"response?no-images-fit"];
  [websocketPoolConnector receivedProtein:[proteinSource proteinForKey:key]];

  XCTAssertTrue([pendingFileUploads count] == 0, @"No pending file uploads should be present before we start");

  XCTAssertTrue(uploadTransactions.count == 0, @"Progressive upload transactions should be %d, instead got %ld", 0, (long)[uploadTransactions count]);
}

-(void) preparePDFFileBatchUploadTransaction
{
  [self setUpConnectedCommunicator];

  NSString *workspaceUid = [proteinSource parameterForKey:kCanonicalParameterWorkspaceUidKey];

  NSBundle *bundle = [NSBundle bundleForClass:[self class]];
  NSString *path = [bundle pathForResource:@"Test-PDF" ofType:@"pdf"];

  NSMutableArray *fileUploadInfos = [[NSMutableArray alloc] init];
  MZFileUploadInfo *uploadInfo = [[MZFileUploadInfo alloc] init];
  uploadInfo.fileName = @"Test-PDF.pdf";
  uploadInfo.format = @"pdf";
  uploadInfo.fileURL = [NSURL fileURLWithPath:path];
  uploadInfo.workspaceUid = workspaceUid;
  [fileUploadInfos addObject:uploadInfo];

  [communicator.uploader requestFilesUpload:fileUploadInfos
                               workspaceUid:workspaceUid
                               limitReached:nil
                      singleUploadCompleted:nil
                               errorHandler:nil];
}

-(void) testRequestPDFFileUploadAccepted
{
  [self preparePDFFileBatchUploadTransaction];
  NSInteger numberOfFiles = 1;

  NSArray *pendingFileUploads = [communicator.uploader valueForKey:@"pendingFileUploads"];
  NSArray *uploadTransactions = [communicator.uploader valueForKey:@"uploadTransactions"];
  XCTAssertTrue([pendingFileUploads count] == numberOfFiles, @"Pending file upload should be %ld, instead got %lu", (long)numberOfFiles, (unsigned long)[pendingFileUploads count]);

  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameAssetUploadProvision responseType:@"pm?asset-upload-pdf"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];

  XCTAssertTrue([pendingFileUploads count] == 0, @"No pending file uploads should be present before we start");

  key = [proteinSource proteinKeyForTransactionName:MZTransactionNameAssetUploadProvision responseType:@"response?pdf-all-pages-fit"];
  p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];

  XCTAssertTrue(uploadTransactions.count == 0, @"Progressive upload transactions should be %d, instead got %ld", 0, (long)[uploadTransactions count]);
}

-(void) testRequestPDFFileUploadNotAccepted
{
  [self preparePDFFileBatchUploadTransaction];
  NSInteger numberOfFiles = 1;

  NSArray *pendingFileUploads = [communicator.uploader valueForKey:@"pendingFileUploads"];
  NSArray *uploadTransactions = [communicator.uploader valueForKey:@"uploadTransactions"];
  XCTAssertTrue([pendingFileUploads count] == numberOfFiles, @"Pending file upload should be %ld, instead got %ld", (long)numberOfFiles, (long)[pendingFileUploads count]);

  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameAssetUploadProvision responseType:@"response?pdf-does-not-fit"];
  [websocketPoolConnector receivedProtein:[proteinSource proteinForKey:key]];

  XCTAssertTrue([pendingFileUploads count] == 0, @"No pending file uploads should be present before we start");

  XCTAssertTrue(uploadTransactions.count == 0, @"Progressive upload transactions should be %d, instead got %ld", 0, (long)[uploadTransactions count]);
}


@end
