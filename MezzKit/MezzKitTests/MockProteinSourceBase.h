//
//  MockProteinSource.h
//  MezzKit
//
//  Created by Zai Chang on 4/25/13.
//  Copyright (c) 2013 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MZCanonicalProteinConstructor.h"
#import "MZProteinFactoryBase.h"

@protocol MockProteinSourceBaseProtocol <NSObject>

// Helper method for generating the protein key from transaction name.
// responseType can be nil. If nil, it is assumed to be a psa
- (NSString *)proteinKeyForTransactionName:(NSString *)transactionName responseType:(NSString *)responseType;

// key is something like ob.mezz.client.asset-upload?request-single-asset.protein
- (OBProtein *)proteinForKey:(NSString *)key;

// parameters used for unit test assersions
- (id)parameterForKey:(NSString *)key;

@end

@interface MockProteinSourceBase : MZProteinFactory <MockProteinSourceBaseProtocol>
{
  MZCanonicalProteinConstructor *constructor;
}

@end


// Reads in generated protein files
@interface FileSystemProteinSource : NSObject <MockProteinSourceBaseProtocol>
{
  NSDictionary *parameters;
}

@property (nonatomic, copy) NSString *path;

- (id)initWithPath:(NSString *)path;

@end

