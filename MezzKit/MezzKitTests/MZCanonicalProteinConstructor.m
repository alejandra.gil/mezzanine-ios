//
//  MZCanonicalProteinConstructor.m
//  MezzKit
//
//  Created by Zai Chang on 5/10/13.
//  Copyright (c) 2013 Oblong Industries. All rights reserved.
//

#import "MZCanonicalProteinConstructor.h"
#import "MZProtoFactory.h"

NSString* const kCanonicalParameterProvenanceKey = @"provenance";

NSString* const kCanonicalParameterCorrectPassphraseKey = @"passphrase-correct";
NSString* const kCanonicalParameterIncorrectPassphraseKey = @"passphrase-incorrect";

NSString* const kCanonicalParameterLoginUsernameKey = @"username";
NSString* const kCanonicalParameterLoginCorrectPasswordKey = @"password-correct";
NSString* const kCanonicalParameterLoginIncorrectPasswordKey = @"password-incorrect";

NSString* const kCanonicalParameterPasskeyKey = @"passkey";

NSString* const kCanonicalParameterUploadMaxWidth = @"upload-max-width";
NSString* const kCanonicalParameterUploadMaxHeight = @"upload-max-height";
NSString* const kCanonicalParameterUploadImageSizeMB = @"upload-max-image-size-mb";
NSString* const kCanonicalParameterUploadPDFSizeMB = @"upload-max-pdf-size-mb";
NSString* const kCanonicalParameterUploadSizeMB = @"upload-max-upload-size-mb";

NSString* const kCanonicalParameterInfopresenceParticipant1Key = @"infopresence-participant-1";
NSString* const kCanonicalParameterInfopresenceParticipant2Key = @"infopresence-participant-2";
NSString* const kCanonicalParameterInfopresenceParticipant3Key = @"infopresence-participant-3";
NSString* const kCanonicalParameterInfopresenceParticipant4Key = @"infopresence-participant-4";
NSString* const kCanonicalParameterInfopresenceIncomingJoinRequestUidKey = @"infopresence-participant-2";
NSString* const kCanonicalParameterInfopresenceOutgoingJoinRequestUidKey = @"infopresence-participant-3";
NSString* const kCanonicalParameterInfopresenceIncomingInviteUidKey = @"infopresence-participant-4";
NSString* const kCanonicalParameterInfopresenceOutgoingInviteUidKey = @"infopresence-participant-1";
NSString* const kCanonicalParameterInfopresenceRemoteParticipant1Key = @"infopresence-remote-participant-1";
NSString* const kCanonicalParameterInfopresenceRemoteParticipant2Key = @"infopresence-remote-participant-2";
NSString* const kCanonicalParameterInfopresenceVideoChatPexipService = @"infopresence-video-chat-pexip-service";
NSString* const kCanonicalParameterInfopresenceVideoChatPexipConferenceUid = @"infopresence-video-chat-pexip-conference";
NSString* const kCanonicalParameterInfopresenceVideoChatPexipNodeUid = @"infopresence-video-chat-pexip-node";

NSString* const kCanonicalParameterMeetingRoomParticipant1Key = @"meeting-room-participant-1";
NSString* const kCanonicalParameterMeetingRoomParticipant2Key = @"meeting-room-participant-2";
NSString* const kCanonicalParameterMeetingRoomParticipantLocalKey = @"meeting-room-participant-local";
NSString* const kCanonicalParameterMeetingRoomParticipantCloudKey = @"meeting-room-participant-cloud";
NSString* const kCanonicalParameterMeetingParticipantLocalKey = @"meeting-participant-local";
NSString* const kCanonicalParameterMeetingParticipantRoom1Key = @"meeting-participant-room-1";
NSString* const kCanonicalParameterMeetingParticipantRoom2Key = @"meeting-participant-room-2";
NSString* const kCanonicalParameterMeetingParticipantRemoterKey = @"meeting-participant-remoter";

NSString* const kCanonicalParameterWorkspaceNameKey = @"workspace-name";
NSString* const kCanonicalParameterWorkspaceUidKey = @"workspace-uid";
NSString* const kCanonicalParameterWorkspaceOwnerKey = @"workspace-owner";
NSString* const kCanonicalParameterWorkspaceOldUidKey = @"workspace-old-uid";
NSString* const kCanonicalParameterPublicWorkspaceUidKey = @"workspace-public-uid";
NSString* const kCanonicalParameterPrivateWorkspaceUidKey = @"workspace-private-uid";
NSString* const kCanonicalParameterWorkspaceModifiedKey = @"workspace-modified-date";


NSString* const kCanonicalParameterDeckScrollIndexKey = @"deck-scroll-index";

NSString* const kCanonicalParameterPresentationNumberOfSlidesKey = @"presentation-number-of-slides";
NSString* const kCanonicalParameterPresentationScrollIndexKey = @"presentation-scroll-index";

NSString* const kCanonicalParameterSlideUidKey = @"slide-uid";
NSString* const kCanonicalParameterSlideInsertionIndexKey = @"slide-insertion-index";
NSString* const kCanonicalParameterSlideInsertionRightOfKey = @"slide-insertion-rightof";
NSString* const kCanonicalParameterSlideInsertionLeftOfKey = @"slide-insertion-leftof";

NSString* const kCanonicalParameterSlideInsertionUidKey = @"slide-insertion-uid";
NSString* const kCanonicalParameterSlideDeletionUidKey = @"slide-deletion-uid";
NSString* const kCanonicalParameterSlideReorderUidKey = @"slide-reorder-uid";

NSString* const kCanonicalParameterAssetUidKey = @"asset-uid";
NSString* const kCanonicalParameterAssetThumbnailURIKey = @"asset-thumb-uri";
NSString* const kCanonicalParameterAssetFullURIKey = @"asset-full-uri";
NSString* const kCanonicalParameterAssetUploadFilenameKey = @"asset-upload-filename";
NSString* const kCanonicalParameterAssetUploadImageDataKey = @"asset-upload-imagedata";

NSString* const kCanonicalParameterLiveStreamUid1Key = @"live-stream-uid-1";
NSString* const kCanonicalParameterLiveStreamUid2Key = @"live-stream-uid-2";
NSString* const kCanonicalParameterLiveStreamUid3Key = @"live-stream-uid-3";

NSString* const kCanonicalParameterWindshieldItemUidKey = @"windshield-item-uid";
NSString* const kCanonicalParameterWindshieldItemAspectRatioKey = @"windshield-item-aspect-ratio";
NSString* const kCanonicalParameterWindshieldItemContentSourceKey = @"windshield-item-content-source";
NSString* const kCanonicalParameterWindshieldItemFeldIdKey = @"windshield-item-feld-id";
NSString* const kCanonicalParameterWindshieldItemFeldRelativeCoordsKey = @"windshield-item-feld-relative-coords";
NSString* const kCanonicalParameterWindshieldItemFeldRelativeSizeKey = @"windshield-item-feld-relative-size";
NSString* const kCanonicalParameterWindshieldItemFrameKey = @"windshield-item-frame";

NSString* const kCanonicalParameterWhiteboardUidKey = @"whiteboard-uid";

NSString* const kCanonicalParameterCollaborationUidKey = @"collaboration-uid";

NSString* const kCanonicalParameterFilesArrayKey = @"files-array";
NSString* const kCanonicalParameterUploadTypeKey = @"upload-type";
NSString* const kCanonicalParameterAssetUploadPDFDataKey = @"asset-upload-pdfdata";
NSString* const kCanonicalParameterAssetUploadFileIDKey = @"asset-upload-file-id";

NSString* const kCanonicalParameterAssetUid1Key = @"asset-uid-1";
NSString* const kCanonicalParameterAssetUid2Key = @"asset-uid-2";
NSString* const kCanonicalParameterAssetUid3Key = @"asset-uid-3";
NSString* const kCanonicalParameterAssetUid4Key = @"asset-uid-4";

NSString* const kCanonicalParameterFileId1Key = @"1";
NSString* const kCanonicalParameterFileId2Key = @"2";
NSString* const kCanonicalParameterFileId3Key = @"3";

NSString* const kCanonicalParameterTransactionIdKey = @"transaction-id";

NSString* const kCanonicalParameterItemUidKey = @"item-uid";

NSString* const kCanonicalParameterFeldLeftKey = @"feld-left";
NSString* const kCanonicalParameterFeldMainKey = @"feld-main";
NSString* const kCanonicalParameterFeldRightKey = @"feld-right";
NSString* const kCanonicalParameterBoundingFeldKey = @"bounding-feld";



@implementation MZCanonicalProteinConstructor

@synthesize canonicalParameters;

-(id) initWithProteinFactory:(MZProteinFactory *)aProteinFactory
{
  self = [super init];
  if (self)
  {
    proteinFactory = aProteinFactory;
    
    [self initDefaultCanonicalParameters];
  }
  return self;
}


-(void) initDefaultCanonicalParameters
{
  NSDictionary *meetingParticipantLocalValue = @{@"provenance": @"screencast-ABCDE-0000000-ABCDE",
                                                 @"type": @"screencast",
                                                 @"display-name":@"Tim"};
  NSDictionary *meetingParticipantRoom1Value = @{@"provenance": @"iPhone-ABCDE-0000000-ABCDE",
                                                 @"type": @"iPhone",
                                                 @"display-name":@"Miguel"};
  NSDictionary *meetingParticipantRoom2Value = @{@"provenance": @"web-ABCDE-0000000-ABCDE",
                                                 @"uid": @"web",
                                                 @"display-name":@"Ivan"};
  NSDictionary *meetingParticipantRemoterValue = @{@"provenance": @"iPad-0000000-ABCDE",
                                                   @"uid": @"iPad",
                                                   @"display-name":@"Zai"};

  self.canonicalParameters = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                              @"iPad-ABCDE-0000000-ABCDE", kCanonicalParameterProvenanceKey,
                              
                              @"ABC", kCanonicalParameterCorrectPassphraseKey,
                              @"DEF", kCanonicalParameterIncorrectPassphraseKey,
                              
                              @"Joe the plumber", kCanonicalParameterLoginUsernameKey,
                              @"0b1ong", kCanonicalParameterLoginCorrectPasswordKey,
                              @"Oblong", kCanonicalParameterLoginIncorrectPasswordKey,
                              
                              @"ASD", kCanonicalParameterPasskeyKey,
                              
                              @6000, kCanonicalParameterUploadMaxWidth,
                              @4000, kCanonicalParameterUploadMaxHeight,
                              @100, kCanonicalParameterUploadImageSizeMB,
                              @100, kCanonicalParameterUploadPDFSizeMB,
                              @100, kCanonicalParameterUploadSizeMB,
                              
                              // Infopresence
                              @"FAKE-MEZZ-ID-11111", kCanonicalParameterInfopresenceParticipant1Key,
                              @"FAKE-MEZZ-ID-22222", kCanonicalParameterInfopresenceParticipant2Key,
                              @"FAKE-MEZZ-ID-33333", kCanonicalParameterInfopresenceParticipant3Key,
                              @"FAKE-MEZZ-ID-44444", kCanonicalParameterInfopresenceParticipant4Key,
                              @"FAKE-MEZZ-ID-22222", kCanonicalParameterInfopresenceIncomingJoinRequestUidKey,
                              @"FAKE-MEZZ-ID-33333", kCanonicalParameterInfopresenceOutgoingJoinRequestUidKey,
                              @{@"uid": @"FAKE-REMOTE-PARTICIPANT-ID-11111",
                                @"display-name":@"Miguel"}, kCanonicalParameterInfopresenceRemoteParticipant1Key,
                              @{@"uid": @"FAKE-REMOTE-PARTICIPANT-ID-22222",
                                @"display-name":@"Zai"}, kCanonicalParameterInfopresenceRemoteParticipant2Key,
                              @"pexip", kCanonicalParameterInfopresenceVideoChatPexipService,
                              @"test-conference-1", kCanonicalParameterInfopresenceVideoChatPexipConferenceUid,
                              @"test-pexip-node.com", kCanonicalParameterInfopresenceVideoChatPexipNodeUid,

                              // Participants (4.0 API)
                              @{@"room-name": @"Fake Local Mezz",
                                @"uid": @"FAKE-MEZZ-ID-LOCAL",
                                @"participants":@[meetingParticipantLocalValue]}, kCanonicalParameterMeetingRoomParticipantLocalKey,
                              @{@"room-name": @"Fake Room 1 Mezz",
                                @"uid": @"FAKE-MEZZ-ID-ROOM-1",
                                @"participants":@[meetingParticipantRoom1Value]}, kCanonicalParameterMeetingRoomParticipant1Key,
                              @{@"room-name": @"Fake Room 2 Mezz",
                                @"uid": @"FAKE-MEZZ-ID-ROOM-2",
                                @"participants":@[meetingParticipantRoom2Value]}, kCanonicalParameterMeetingRoomParticipant2Key,
                              @{@"room-name": @"Fake Room Cloud Mezz",
                                @"uid": @"FAKE-MEZZ-ID-CLOUD",
                                @"participants":@[meetingParticipantRemoterValue]}, kCanonicalParameterMeetingRoomParticipantCloudKey,
                              meetingParticipantLocalValue, kCanonicalParameterMeetingParticipantLocalKey,
                              meetingParticipantRoom1Value, kCanonicalParameterMeetingParticipantRoom1Key,
                              meetingParticipantRoom2Value, kCanonicalParameterMeetingParticipantRoom2Key,
                              meetingParticipantRemoterValue, kCanonicalParameterMeetingParticipantRemoterKey,
                              
                              // Workspace
                              @"ws-0001", kCanonicalParameterWorkspaceUidKey,
                              @"Work this", kCanonicalParameterWorkspaceNameKey,
                              @"Own this", kCanonicalParameterWorkspaceOwnerKey,
                              @"ws-0002", kCanonicalParameterWorkspaceOldUidKey,
                              @10, kCanonicalParameterPresentationNumberOfSlidesKey,
                              @[@"ws-0001", @"ws-0001"], kCanonicalParameterPublicWorkspaceUidKey,
                              @[@"ws-0101", @"ws-0102"], kCanonicalParameterPrivateWorkspaceUidKey,
                              [NSDate date], kCanonicalParameterWorkspaceModifiedKey,
                              
                              // Presentation
                              @3, kCanonicalParameterDeckScrollIndexKey,
                              @3, kCanonicalParameterPresentationScrollIndexKey,
                              
                              @"sl-0001", kCanonicalParameterSlideUidKey,
                              @3, kCanonicalParameterSlideInsertionIndexKey,
                              @"sl-0003", kCanonicalParameterSlideInsertionRightOfKey,
                              @"sl-0004", kCanonicalParameterSlideInsertionLeftOfKey,

                              @"sl-0009", kCanonicalParameterSlideInsertionUidKey,
                              @"sl-0003", kCanonicalParameterSlideDeletionUidKey,
                              @"sl-0003", kCanonicalParameterSlideReorderUidKey,

                              // Assets
                              @"as-0001", kCanonicalParameterAssetUidKey,
                              @"r/global-assets/as-0001/img-thumb.png", kCanonicalParameterAssetThumbnailURIKey,
                              @"r/global-assets/as-0001/orig-image", kCanonicalParameterAssetFullURIKey,
                              
                              // Live Streams
                              @"vi-0001", kCanonicalParameterLiveStreamUid1Key,
                              @"vi-0002", kCanonicalParameterLiveStreamUid2Key,
                              @"vi-0003", kCanonicalParameterLiveStreamUid3Key,
                              
                              // Windshield
                              @"el-0001", kCanonicalParameterWindshieldItemUidKey,
                              @1.7778, kCanonicalParameterWindshieldItemAspectRatioKey,
                              @"as-0001", kCanonicalParameterWindshieldItemContentSourceKey,
                              @"main", kCanonicalParameterWindshieldItemFeldIdKey,
                              @{@"over" : @0,
                                @"up" : @0.1,
                                @"norm" : @0.2,
                                @"feld-id" : @"main" }, kCanonicalParameterWindshieldItemFeldRelativeCoordsKey,
                              @{@"scale" : @1,
                                @"aspect-ratio" : @1.7778,
                                @"feld-id" : @"main" }, kCanonicalParameterWindshieldItemFeldRelativeSizeKey,
                              
                              @"TestUpload.jpg", kCanonicalParameterAssetUploadFilenameKey,
                              
                              @"whiteboard-3cf3e48e-aad3-4893-9772-f271914cf653", kCanonicalParameterWhiteboardUidKey,

                              @[@{@"file-id": @0, @"file-name":@"PDFTestUpload.pdf", @"type": @"pdf"},
                                @{@"file-id": @1, @"file-name":@"PNGTestUpload.png", @"type": @"png"}], kCanonicalParameterFilesArrayKey,
                              @"both", kCanonicalParameterUploadTypeKey,
                              @"0", kCanonicalParameterAssetUploadFileIDKey,

                              // Asset Upload
                              @"as-0001", kCanonicalParameterAssetUid1Key,
                              @"as-0002", kCanonicalParameterAssetUid2Key,
                              @"as-0003", kCanonicalParameterAssetUid3Key,
                              @"as-0004", kCanonicalParameterAssetUid4Key,

                              @"1", kCanonicalParameterFileId1Key,
                              @"2", kCanonicalParameterFileId2Key,
                              @"3", kCanonicalParameterFileId3Key,

                              @"-1", kCanonicalParameterTransactionIdKey,

                              @{@"id" : @"left",
                                @"type" : @"visible",
                                @"cent" : [MZVect doubleVectWithX:-1200.0 y:100.0 z:-2100.0],
                                @"up"   : [MZVect doubleVectWithX:0.0 y:1.0 z:0.0],
                                @"over" : [MZVect doubleVectWithX:1.0 y:0.0 z:0.0],
                                @"norm" : [MZVect doubleVectWithX:0.0 y:0.0 z:1.0],
                                @"phys-size" : [MZVect doubleVectWithX:1208.0 y:679.5],
                                @"px-size" : [MZVect integerVectWithX:1920 y:1080],
                                @"view-dist" : @1450,
                                @"visible-to-all" : @YES }, kCanonicalParameterFeldLeftKey,

                              @{@"id" : @"main",
                                @"type" : @"visible",
                                @"cent" : [MZVect doubleVectWithX:0.0 y:100.0 z:-2100.0],
                                @"up"   : [MZVect doubleVectWithX:0.0 y:1.0 z:0.0],
                                @"over" : [MZVect doubleVectWithX:1.0 y:0.0 z:0.0],
                                @"norm" : [MZVect doubleVectWithX:0.0 y:0.0 z:1.0],
                                @"phys-size" : [MZVect doubleVectWithX:1208.0 y:679.5],
                                @"px-size" : [MZVect integerVectWithX:1920 y:1080],
                                @"view-dist" : @1450,
                                @"visible-to-all" : @YES }, kCanonicalParameterFeldMainKey,

                              @{@"id" : @"right",
                                @"type" : @"visible",
                                @"cent" : [MZVect doubleVectWithX:1200.0 y:100.0 z:-2100.0],
                                @"up"   : [MZVect doubleVectWithX:0.0 y:1.0 z:0.0],
                                @"over" : [MZVect doubleVectWithX:1.0 y:0.0 z:0.0],
                                @"norm" : [MZVect doubleVectWithX:0.0 y:0.0 z:1.0],
                                @"phys-size" : [MZVect doubleVectWithX:1208.0 y:679.5],
                                @"px-size" : [MZVect integerVectWithX:1920 y:1080],
                                @"view-dist" : @1450,
                                @"visible-to-all" : @YES }, kCanonicalParameterFeldRightKey,

                              @{@"id" : @"bounding-feld",
                                @"type" : @"invisible",
                                @"cent" : [MZVect doubleVectWithX:0.0 y:100.0 z:-2100.0],
                                @"up"   : [MZVect doubleVectWithX:0.0 y:1.0 z:0.0],
                                @"over" : [MZVect doubleVectWithX:1.0 y:0.0 z:0.0],
                                @"norm" : [MZVect doubleVectWithX:0.0 y:0.0 z:1.0],
                                @"phys-size" : [MZVect doubleVectWithX:3600.0 y:679.5],
                                @"px-size" : [MZVect integerVectWithX:5760 y:1080],
                                @"view-dist" : @1450,
                                @"visible-to-all" : @YES }, kCanonicalParameterBoundingFeldKey,

                              nil];
  
  // TODO Generate a tiny jpeg as sample upload data
  //[self.canonicalParameters setObject:sampleImageData forKey:kCanonicalParameterAssetUploadImageDataKey];

  // TODO Generate a tiny PDF as sample upload PDF data
  //[self.canonicalParameters setObject:samplePDFData forKey:kCanonicalParameterAssetUploadPDFDataKey];

  [proteinFactory setProvenance:(self.canonicalParameters)[kCanonicalParameterProvenanceKey]];
}


@end

