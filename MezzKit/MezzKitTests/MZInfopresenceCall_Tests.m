//
//  MZInfopresenceCall_Tests.m
//  MezzKit
//
//  Created by miguel on 11/4/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MezzKit.h"

@interface MZInfopresenceCall_Tests : XCTestCase
{
  MZInfopresenceCall *_call;
}
@end

@implementation MZInfopresenceCall_Tests

- (void)setUp
{
  [super setUp];
  _call = [MZInfopresenceCall new];
}

- (void)tearDown
{
  [super tearDown];
}

- (void)testResolutions
{
  [_call updateWithDictionary:@{@"resolution":kMZInfopresenceCallResolutionAccepted}];
  XCTAssertTrue(_call.resolution == MZInfopresenceCallResolutionAccepted, @"Resolution should be 'accepted'");

  [_call updateWithDictionary:@{@"resolution":kMZInfopresenceCallResolutionCanceled}];
  XCTAssertTrue(_call.resolution == MZInfopresenceCallResolutionCanceled, @"Resolution should be 'canceled'");

  [_call updateWithDictionary:@{@"resolution":kMZInfopresenceCallResolutionDeclined}];
  XCTAssertTrue(_call.resolution == MZInfopresenceCallResolutionDeclined, @"Resolution should be 'declined'");

  [_call updateWithDictionary:@{@"resolution":kMZInfopresenceCallResolutionTimeOut}];
  XCTAssertTrue(_call.resolution == MZInfopresenceCallResolutionTimedOut, @"Resolution should be 'timeout'");

  [_call updateWithDictionary:@{@"resolution":kMZInfopresenceCallResolutionTime_Out}];
  XCTAssertTrue(_call.resolution == MZInfopresenceCallResolutionTimedOut, @"Resolution should be 'time-out'");
}


@end
