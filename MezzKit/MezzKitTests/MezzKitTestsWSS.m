//
//  MezzKitTestsWSS.m
//  MezzKit
//
//  Created by Ivan Bella Lopez on 11/05/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

#import "MezzKitTestsWSS.h"
#import "MockPoolConnector.h"
#import "MockProteinSource.h"

#import "MZCommunicator.h"
#import "MZCommunicator+Constants.h"
#import "MZCommunicatorRequestor.h"

#import "OBCommunicatorPoolConfig.h"
#import "OBWSSPoolConnector.h"

#import "MZAssetCache.h"

#import "OCMockObject.h"
#import "OCMArg.h"


@interface MZCommunicator (Internal)

-(void) setupJoinResponseHandler;
-(void) joinSuccessful;

-(void) beginListeningPoolConnectorNotifications;
-(void) endListeningPoolConnectorNotifications;

- (NSURL *)urlForResource:(NSString *)resource;

- (void)checkMezzMetadataAndContinueConnection;

- (void)wssPoolConnectorSuccess:(OBWSSPoolConnector *)connector;

@end


@interface MockPoolConnector (Internal)

// Exposing internal function for unit testing
// To simulate the reception of a protein, call [OBPoolConnector receivedProtein:]
-(void) receivedProtein:(OBProtein*)protein;

@end



@interface MezzKitTestsWSS ()
{
  MZCommunicator *communicator;
  MZSystemModel *systemModel;
  MockPoolConnector *websocketPoolConnector;
  
  id<MockProteinSourceBaseProtocol> proteinSource;
  
  BOOL ensureCommunicatorOnlySendsNonNilProteins;
}

@end


@implementation MezzKitTestsWSS

#pragma mark - Setup

- (void)setUp
{
  [super setUp];

  communicator = [[MZCommunicator alloc] initWithServerUrl:[NSURL URLWithString:@"https://test"]];
  systemModel = [[MZSystemModel alloc] init];
  communicator.systemModel = systemModel;
  
  communicator.communicatorProtocol = OBCommunicatorProtocolWSS;
  websocketPoolConnector = [[MockPoolConnector alloc] initWithPool:MEZZ_WEBSOCKET];
  [communicator addPoolConnector:websocketPoolConnector forPoolName:MEZZ_WEBSOCKET];
}


- (void)tearDown
{
  [communicator endListeningPoolConnectorNotifications];
  communicator = nil;
  websocketPoolConnector = nil;
  
  XCTAssertTrue([communicator pendingTransactions].count == 0, @"Pending transactions should be %d, instead got %ld", 0, (long)[[communicator pendingTransactions] count]);
  [super tearDown];
}


- (void)setupCommunicatorCore
{
  systemModel.myMezzanine.version = @"3.13";
  systemModel.apiVersion = @"3.6";

  proteinSource = [[MockProteinSource alloc] init];
  
  communicator.provenance = [proteinSource parameterForKey:kCanonicalParameterProvenanceKey];
  communicator.proteinFactory = proteinSource;
  communicator.proteinFactory.provenance = communicator.provenance;
  
  // Begin listening to pool events, normally called in [OBCommunicator connect]
  [communicator beginListeningPoolConnectorNotifications];
  
  systemModel.state = MZSystemStateWorkspace;
  
  ensureCommunicatorOnlySendsNonNilProteins = YES;  // Mostly to weed out the sending of old non-valid proteins
}


-(void) setUpUnconnectedCommunicator
{
  [self setupCommunicatorCore];
  communicator.hasJoinedSession = NO;
  communicator.isConnected = NO;
  communicator.isConnecting = NO;
  [communicator setupJoinResponseHandler];
}


-(void) setUpConnectedCommunicator
{
  [self setupCommunicatorCore];
  [communicator joinSuccessful];
}


-(void) setUpConnectedCommunicator_usingJoinProtein
{
  [self setUpUnconnectedCommunicator];
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameClientJoin responseType:@"response"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
}


- (MZWorkspace*)prepareEmptyWorkspace
{
  NSString *workspaceUid = [proteinSource parameterForKey:kCanonicalParameterWorkspaceUidKey];
  
  MZWorkspace *workspace = [[MZWorkspace alloc] init];
  workspace.uid = workspaceUid;
  [systemModel insertObject:workspace inWorkspacesAtIndex:systemModel.countOfWorkspaces];
  systemModel.currentWorkspace = workspace;
  return workspace;
}


#pragma mark - Mezz Metadata

- (void)testMezzMetadataWithCloudInstance
{
  [self setupCommunicatorCore];
  [communicator checkMezzMetadataAndContinueConnection];

  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameMezzMetadata responseType:@"response?mezz-metadata-with-cloud-instance"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];

  XCTAssertTrue(communicator.isARemoteParticipationConnection, @"This connection should be defined as a remote participation one");
  XCTAssertFalse(communicator.systemUseNotification);
  XCTAssertNil(communicator.systemUseNotificationText, @"This connection should have not a system use notification text");
}

- (void)testMezzMetadataWithOutCloudInstance
{
  [self setupCommunicatorCore];
  [communicator checkMezzMetadataAndContinueConnection];

  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameMezzMetadata responseType:@"response?mezz-metadata-without-cloud-instance"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];

  XCTAssertFalse(communicator.isARemoteParticipationConnection, @"This connection should determine that is is not a remote participation one");
  XCTAssertFalse(communicator.systemUseNotification);
  XCTAssertNil(communicator.systemUseNotificationText, @"This connection should have not a system use notification text");
}

- (void)testNoMezzMetadata
{
  [self setupCommunicatorCore];
  [communicator checkMezzMetadataAndContinueConnection];

  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameMezzMetadata responseType:@"response?failed.error-no-metadata-info"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];

  XCTAssertFalse(communicator.isARemoteParticipationConnection, @"This connection cannot determine if it is a remote participation one or not");
}

- (void)testMezzMetadataWithSystemUseNotification
{
  [self setupCommunicatorCore];
  [communicator checkMezzMetadataAndContinueConnection];
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameMezzMetadata responseType:@"response?mezz-metadata-with-system-use-notification"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertTrue(communicator.systemUseNotification);
  XCTAssertNotNil(communicator.systemUseNotificationText, @"This connection should have a system use notification text");
}

- (void)testMezzMetadataWithoutSystemUseNotification
{
  [self setupCommunicatorCore];
  [communicator checkMezzMetadataAndContinueConnection];

  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameMezzMetadata responseType:@"response?mezz-metadata-without-system-use-notification"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];

  XCTAssertFalse(communicator.systemUseNotification);
  XCTAssertNil(communicator.systemUseNotificationText);
}


#pragma mark - API version

- (void)testApiVersionNotNil
{
  [self setUpConnectedCommunicator_usingJoinProtein];
  systemModel.myMezzanine.version = @"3.13";

  XCTAssertNotNil(systemModel.apiVersion);
  XCTAssertNotEqual(systemModel.apiVersion, systemModel.myMezzanine.version);
  XCTAssertEqual(systemModel.apiVersion, @"3.6");
}


- (void)testApiVersionNil
{
  [self setupCommunicatorCore];
  
  // After the setup we need to modify apiVersion to set up the test
  systemModel.apiVersion = nil;
  systemModel.myMezzanine.version = @"3.13";
  
  communicator.hasJoinedSession = NO;
  communicator.isConnected = NO;
  communicator.isConnecting = NO;
  [communicator setupJoinResponseHandler];
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameClientJoin responseType:@"response"];
  OBProtein *p = [proteinSource proteinForKey:key];
  
  [communicator.poolConnector receivedProtein:p];
  
  XCTAssertNotNil(systemModel.apiVersion);
  XCTAssertEqual(systemModel.apiVersion, systemModel.myMezzanine.version);
}


#pragma mark - Join

-(void) testClientJoinSuccessCaseWithTriptychWhiteboards
{
  [self setUpUnconnectedCommunicator];
  [self performClientJoinSuccessWithTriptychWhiteboardsTest];
}

// A comprehensive testcase for join success with a saved workspace
-(void) performClientJoinSuccessWithTriptychWhiteboardsTest
{
  XCTAssertFalse(communicator.isConnected, @"Communicator should not be connected before receiving join protein.");
  XCTAssertFalse(communicator.isConnecting, @"Communicator should not be connecting before receiving join protein.");
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameClientJoin responseType:@"response"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertTrue(communicator.isConnected, @"Communicator should be connected after receiving join success");
  XCTAssertFalse(communicator.isConnecting, @"Communicator should not be connecting after receiving join success");
  
  
  // Check Mezzanine State
  XCTAssertEqual(systemModel.state, MZSystemStateWorkspace, @"System state should be workspace after joining");
  
  
  // Check felds
  NSUInteger numberOfFelds = 3;
  XCTAssertEqual(systemModel.felds.count, numberOfFelds, @"Felds count should be equal");

  MZFeld *leftFeld = [systemModel feldWithName:@"left"];
  XCTAssertTrue([leftFeld.type isEqualToString:@"visible"]);
  XCTAssertTrue([leftFeld.center equalToVect:[MZVect doubleVectWithX:0 y:0 z:0]]);
  XCTAssertTrue([leftFeld.norm equalToVect:[MZVect doubleVectWithX:0 y:0 z:0]]);
  XCTAssertTrue([leftFeld.over equalToVect:[MZVect doubleVectWithX:0 y:0 z:0]]);
  XCTAssertTrue([leftFeld.up equalToVect:[MZVect doubleVectWithX:0 y:0 z:0]]);
  XCTAssertTrue([leftFeld.pixelSize equalToVect:[MZVect integerVectWithX:1920 y:1080]]);
  XCTAssertTrue([leftFeld.physicalSize equalToVect:[MZVect doubleVectWithX:1920 y:1080]]);

  MZFeld *rightFeld = [systemModel feldWithName:@"right"];
  XCTAssertTrue([rightFeld.type isEqualToString:@"visible"]);
  XCTAssertTrue([rightFeld.center equalToVect:[MZVect doubleVectWithX:0 y:0 z:0]]);
  XCTAssertTrue([rightFeld.norm equalToVect:[MZVect doubleVectWithX:0 y:0 z:0]]);
  XCTAssertTrue([rightFeld.over equalToVect:[MZVect doubleVectWithX:0 y:0 z:0]]);
  XCTAssertTrue([rightFeld.up equalToVect:[MZVect doubleVectWithX:0 y:0 z:0]]);
  XCTAssertTrue([rightFeld.pixelSize equalToVect:[MZVect integerVectWithX:1920 y:1080]]);
  XCTAssertTrue([rightFeld.physicalSize equalToVect:[MZVect doubleVectWithX:1920 y:1080]]);

  MZFeld *mainFeld = [systemModel feldWithName:@"main"];
  XCTAssertTrue([mainFeld.type isEqualToString:@"visible"]);
  XCTAssertTrue([mainFeld.center equalToVect:[MZVect doubleVectWithX:0 y:0 z:0]]);
  XCTAssertTrue([mainFeld.norm equalToVect:[MZVect doubleVectWithX:0 y:0 z:0]]);
  XCTAssertTrue([mainFeld.over equalToVect:[MZVect doubleVectWithX:0 y:0 z:0]]);
  XCTAssertTrue([mainFeld.up equalToVect:[MZVect doubleVectWithX:0 y:0 z:0]]);
  XCTAssertTrue([mainFeld.pixelSize equalToVect:[MZVect integerVectWithX:1920 y:1080]]);
  XCTAssertTrue([mainFeld.physicalSize equalToVect:[MZVect doubleVectWithX:1920 y:1080]]);

  // Check workspace
  NSString *workspaceUid = [proteinSource parameterForKey:kCanonicalParameterWorkspaceUidKey];
  NSString *workspaceName = [proteinSource parameterForKey:kCanonicalParameterWorkspaceNameKey];
  
  XCTAssertEqualObjects(systemModel.currentWorkspace.uid, workspaceUid, @"Current workspace's uid should be %@", workspaceUid);
  XCTAssertEqualObjects(systemModel.currentWorkspace.name, workspaceName, @"Current workspace's name should be %@", workspaceName);
  
  
  // Check whiteboards
  NSString *whiteboardUid = [proteinSource parameterForKey:kCanonicalParameterWhiteboardUidKey];
  NSUInteger numberOfWhiteboards = 1;
  XCTAssertEqual(systemModel.whiteboards.count, numberOfWhiteboards, @"Whiteboards should be received after join success");
  if (systemModel.whiteboards.count > 0)
    XCTAssertEqualObjects(systemModel.whiteboards[0][@"uid"], whiteboardUid, @"Whiteboard's uid should be %@", whiteboardUid);
  
  
  // Check upload limits
  NSNumber *uploadMaxWidth = [proteinSource parameterForKey:kCanonicalParameterUploadMaxWidth];
  NSNumber *uploadMaxHeight = [proteinSource parameterForKey:kCanonicalParameterUploadMaxHeight];
  NSNumber *uploadMaxImageSizeMB = [proteinSource parameterForKey:kCanonicalParameterUploadImageSizeMB];
  NSNumber *uploadMaxPDFSizeMB = [proteinSource parameterForKey:kCanonicalParameterUploadPDFSizeMB];
  NSNumber *uploadMaxSizeMB = [proteinSource parameterForKey:kCanonicalParameterUploadSizeMB];
  XCTAssertEqual(systemModel.uploadLimits.maxImageWidth.integerValue, uploadMaxWidth.integerValue, @"Upload max width should be %@", uploadMaxWidth);
  XCTAssertEqual(systemModel.uploadLimits.maxImageHeight.integerValue, uploadMaxHeight.integerValue, @"Upload max height should be %@", uploadMaxHeight);
  XCTAssertEqual(systemModel.uploadLimits.maxImageSizeInMB.integerValue, uploadMaxImageSizeMB.integerValue, @"Upload max image size should be %@", uploadMaxImageSizeMB);
  XCTAssertEqual(systemModel.uploadLimits.maxPDFSizeInMB.integerValue, uploadMaxPDFSizeMB.integerValue, @"Upload max PDF size should be %@", uploadMaxPDFSizeMB);
  XCTAssertEqual(systemModel.uploadLimits.maxGlobalSizeInMB.integerValue, uploadMaxSizeMB.integerValue, @"Upload max global size should be %@", uploadMaxSizeMB);
}

// Below test cases for join success will focus on specific deviations or combinations

-(void) testClientJoinSuccessCaseWithSingleFeld
{
  [self setUpUnconnectedCommunicator];
  [self performClientJoinSuccessWithSingleFeldTest];
}

-(void) performClientJoinSuccessWithSingleFeldTest
{
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameClientJoin responseType:@"response?single-feld"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertTrue(communicator.isConnected, @"Communicator should be connected after receiving join success");
  XCTAssertFalse(communicator.isConnecting, @"Communicator should not be connecting after receiving join success");
  
  
  // Check felds
  NSUInteger numberOfFelds = 1;
  XCTAssertEqual(systemModel.felds.count, numberOfFelds, @"Felds count should be equal");
}


-(void) testClientJoinFailurePassphraseRequired
{
  [self setUpUnconnectedCommunicator];
  [self performClientJoinFailurePassphraseRequiredTest];
}


-(void) performClientJoinFailurePassphraseRequiredTest
{
  XCTAssertFalse(communicator.isConnected, @"Communicator should not be connected before receiving join protein.");
  XCTAssertFalse(communicator.isConnecting, @"Communicator should not be connecting before receiving join protein.");
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameClientJoin responseType:@"response?failed.passphrase-required"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertFalse(communicator.isConnected, @"Communicator should not be connected after receiving join failure");
  XCTAssertFalse(communicator.isConnecting, @"Communicator should not be connecting after receiving join failure");
  
  XCTAssertTrue(systemModel.passphraseRequested, @"Passphrase requested flag should be raised.");
  
  // TODO - check for passphrase timer activation
}


-(void) testClientJoinFailurePassphraseIncorrect
{
  [self setUpUnconnectedCommunicator];
  [self performClientJoinFailurePassphraseIncorrectTest];
}


-(void) performClientJoinFailurePassphraseIncorrectTest
{
  XCTAssertFalse(communicator.isConnected, @"Communicator should not be connected before receiving join protein.");
  XCTAssertFalse(communicator.isConnecting, @"Communicator should not be connecting before receiving join protein.");
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameClientJoin responseType:@"response?failed.incorrect-passphrase.3.0"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertFalse(communicator.isConnected, @"Communicator should not be connected after receiving join failure");
  XCTAssertFalse(communicator.isConnecting, @"Communicator should not be connecting after receiving join failure");
  
  XCTAssertTrue(systemModel.passphraseRequested, @"Passphrase requested flag should be raised.");
  
  // TODO - check for passphrase timer activation
}

-(void) testClientJoinFailurePassphraseDismissed
{
  [self setUpUnconnectedCommunicator];
  [self performClientJoinFailurePassphraseDismissedTest];
}

-(void) performClientJoinFailurePassphraseDismissedTest
{
  XCTAssertFalse(communicator.isConnected, @"Communicator should not be connected before receiving join protein.");
  XCTAssertFalse(communicator.isConnecting, @"Communicator should not be connecting before receiving join protein.");
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameClientJoin responseType:@"response?failed.passphrase-required"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertFalse(communicator.isConnected, @"Communicator should not be connected after receiving join failure");
  XCTAssertFalse(communicator.isConnecting, @"Communicator should not be connecting after receiving join failure");
  
  XCTAssertTrue(systemModel.passphraseRequested, @"Passphrase requested flag should be raised.");
  
  key = [proteinSource proteinKeyForTransactionName:MZTransactionNamePasskeyDisable responseType:@"psa"];
  p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertFalse(systemModel.passphraseRequested, @"Passphrase requested flag should not be needed.");
  XCTAssertNil(systemModel.passphrase, @"Passphrase should not exist.");
}


#pragma mark - Evict Tests

- (void)testClientEvict
{
  [self setUpConnectedCommunicator];
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameClientEvict responseType:@"pm"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertFalse(communicator.isConnected, @"Communicator should no logner be connected.");
  XCTAssertFalse(communicator.isConnecting, @"Communicator should not be connecting after receiving evict");
}



#pragma mark - MezCaps Tests

-(void) testMezCapsWhiteboardAdditions
{
  [self setUpConnectedCommunicator];
  [systemModel reset];
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameMezzanineCapabilities responseType:@"psa"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  // Check whiteboards
  NSString *whiteboardUid = [proteinSource parameterForKey:kCanonicalParameterWhiteboardUidKey];
  NSUInteger numberOfWhiteboards = 1;
  XCTAssertEqual(systemModel.whiteboards.count, numberOfWhiteboards, @"Whiteboards should be received after join success");
  if (systemModel.whiteboards.count > 0)
    XCTAssertEqualObjects(systemModel.whiteboards[0][@"uid"], whiteboardUid, @"Whiteboard's uid should be %@", whiteboardUid);
}


-(void) testMezCapsWhiteboardsReduction
{
  [self setUpConnectedCommunicator];
  
  systemModel.whiteboards = [@[@{@"uid" : @"and"},
                               @{@"uid" : @"neither"},
                               @{@"uid" : @"do"},
                               @{@"uid" : @"these"},
                               ] mutableCopy];
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameMezzanineCapabilities responseType:@"psa"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  // Check whiteboards
  NSString *whiteboardUid = [proteinSource parameterForKey:kCanonicalParameterWhiteboardUidKey];
  NSUInteger numberOfWhiteboards = 1;
  XCTAssertEqual(systemModel.whiteboards.count, numberOfWhiteboards, @"Whiteboards should be received after join success");
  if (systemModel.whiteboards.count > 0)
    XCTAssertEqualObjects(systemModel.whiteboards[0][@"uid"], whiteboardUid, @"Whiteboard's uid should be %@", whiteboardUid);
}



#pragma mark - Passphrase Tests

-(void) testPassphraseEnable
{
  [self setUpConnectedCommunicator];
  [self performPassphraseEnableTest];
}

-(void) performPassphraseEnableTest
{
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNamePasskeyEnable responseType:@"psa"];
  
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertTrue(systemModel.passphraseRequested, @"Passphrase requested should be true after it is enabled.");
  XCTAssertFalse(communicator.hasJoinedSession, @"Communicator should not be joined if passphrase is requested.");
}


-(void) testPassphraseEnabledBySelf
{
  [self setUpConnectedCommunicator];
  [self performPassphraseEnabledBySelfTest];
}

-(void) performPassphraseEnabledBySelfTest
{
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNamePasskeyEnable responseType:@"psa?self-test"];
  
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertFalse(systemModel.passphraseRequested, @"Passphrase requested should be false after it is enabled by self.");
  XCTAssertTrue(communicator.hasJoinedSession, @"Communicator should still be joined if passphrase is enabled by self.");
}


-(void) testPassphraseDisable
{
  [self setUpConnectedCommunicator];
  [self performPassphraseDisableTest];
}

-(void) performPassphraseDisableTest
{
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNamePasskeyDisable responseType:@"psa"];
  
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertFalse(systemModel.passphraseRequested, @"Passphrase requested should be false after it is disabled.");
  XCTAssertTrue(communicator.hasJoinedSession, @"Communicator should still be joined if passphrase is disabled.");
}


#pragma mark - Infopresence Tests

- (void)prepareSystemModelForInfopresenceHackWithMyMezzanine
{
  systemModel.myMezzanine.uid = @"MY-MEZZANINE-TEST-UID";
}

- (void)prepareSystemModelForInfopresenceTests
{
  [self prepareSystemModelForInfopresenceHackWithMyMezzanine];
  
  void (^addRemoteMezzWithParameterKey)(NSString*) = ^(NSString *key)
  {
    NSString *collaborator = [proteinSource parameterForKey:key];
    MZRemoteMezz *remoteMezz = [[MZRemoteMezz alloc] init];
    remoteMezz.uid = collaborator;
    remoteMezz.name = collaborator;
    [systemModel.remoteMezzes addObject:remoteMezz];
  };
  
  addRemoteMezzWithParameterKey(kCanonicalParameterInfopresenceParticipant1Key);
  addRemoteMezzWithParameterKey(kCanonicalParameterInfopresenceParticipant2Key);
  addRemoteMezzWithParameterKey(kCanonicalParameterInfopresenceParticipant3Key);
  addRemoteMezzWithParameterKey(kCanonicalParameterInfopresenceParticipant4Key);
}


-(void) testInfopresenceDetail_FromEmpty
{
  [self setUpConnectedCommunicator];
  [self prepareSystemModelForInfopresenceTests];
  
  XCTAssertTrue(systemModel.infopresence.status == MZInfopresenceStatusInactive, @"Should be inactive at the beginnign of test");
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionDetail responseType:@"psa?v3.0"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertTrue(systemModel.infopresence.status == MZInfopresenceStatusActive, @"Should be active");
  
  NSString *collaborator1 = [proteinSource parameterForKey:kCanonicalParameterInfopresenceParticipant1Key];
  NSString *collaborator2 = [proteinSource parameterForKey:kCanonicalParameterInfopresenceParticipant2Key];
  NSString *collaborator3 = [proteinSource parameterForKey:kCanonicalParameterInfopresenceParticipant3Key];
  NSString *collaborator4 = [proteinSource parameterForKey:kCanonicalParameterInfopresenceParticipant4Key];
  
  MZRemoteMezz *collaborator1Mezz = [systemModel remoteMezzWithUid:collaborator1];
  MZRemoteMezz *collaborator2Mezz = [systemModel remoteMezzWithUid:collaborator2];
  MZRemoteMezz *collaborator3Mezz = [systemModel remoteMezzWithUid:collaborator3];
  MZRemoteMezz *collaborator4Mezz = [systemModel remoteMezzWithUid:collaborator4];
  
  XCTAssertTrue((collaborator1Mezz.collaborationState == MZRemoteMezzCollaborationStateInCollaboration), @"%@ should be in state MZRemoteMezzCollaborationStateInCollaboration", collaborator1);
  XCTAssertTrue((collaborator2Mezz.collaborationState == MZRemoteMezzCollaborationStateInCollaboration), @"%@ should be in state MZRemoteMezzCollaborationStateInCollaboration", collaborator2);
  XCTAssertTrue((collaborator3Mezz.collaborationState == MZRemoteMezzCollaborationStateNotInCollaboration), @"%@ should be in state MZRemoteMezzCollaborationStateNotInCollaboration", collaborator3);
  XCTAssertTrue((collaborator4Mezz.collaborationState == MZRemoteMezzCollaborationStateNotInCollaboration), @"%@ should be in state MZRemoteMezzCollaborationStateNotInCollaboration", collaborator4);
}


-(void) testInfopresenceDetail_NotActive
{
  [self setUpConnectedCommunicator];
  [self prepareSystemModelForInfopresenceTests];
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionDetail responseType:@"psa?not-active"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertFalse(systemModel.infopresence.status == MZInfopresenceStatusActive, @"Should not be in collaboration");
  
  
  NSString *collaborator1 = [proteinSource parameterForKey:kCanonicalParameterInfopresenceParticipant1Key];
  NSString *collaborator2 = [proteinSource parameterForKey:kCanonicalParameterInfopresenceParticipant2Key];
  NSString *collaborator3 = [proteinSource parameterForKey:kCanonicalParameterInfopresenceParticipant3Key];
  NSString *collaborator4 = [proteinSource parameterForKey:kCanonicalParameterInfopresenceParticipant4Key];
  
  MZRemoteMezz *collaborator1Mezz = [systemModel remoteMezzWithUid:collaborator1];
  MZRemoteMezz *collaborator2Mezz = [systemModel remoteMezzWithUid:collaborator2];
  MZRemoteMezz *collaborator3Mezz = [systemModel remoteMezzWithUid:collaborator3];
  MZRemoteMezz *collaborator4Mezz = [systemModel remoteMezzWithUid:collaborator4];
  
  XCTAssertTrue((collaborator1Mezz.collaborationState == MZRemoteMezzCollaborationStateNotInCollaboration), @"%@ should be in state MZRemoteMezzCollaborationStateNotInCollaboration", collaborator1);
  XCTAssertTrue((collaborator2Mezz.collaborationState == MZRemoteMezzCollaborationStateNotInCollaboration), @"%@ should be in state MZRemoteMezzCollaborationStateNotInCollaboration", collaborator2);
  XCTAssertTrue((collaborator3Mezz.collaborationState == MZRemoteMezzCollaborationStateNotInCollaboration), @"%@ should be in state MZRemoteMezzCollaborationStateNotInCollaboration", collaborator3);
  XCTAssertTrue((collaborator4Mezz.collaborationState == MZRemoteMezzCollaborationStateNotInCollaboration), @"%@ should be in state MZRemoteMezzCollaborationStateNotInCollaboration", collaborator4);
  
  XCTAssertEqual(systemModel.infopresence.outgoingCalls.count, 0);
  XCTAssertEqual(systemModel.infopresence.incomingCalls.count, 0);
}


-(void) testInfopresenceDetail_CollaboratorJoined
{
  [self setUpConnectedCommunicator];
  [self prepareSystemModelForInfopresenceTests];
  
  NSString *collaborator1 = [proteinSource parameterForKey:kCanonicalParameterInfopresenceParticipant1Key];
  NSString *collaborator2 = [proteinSource parameterForKey:kCanonicalParameterInfopresenceParticipant2Key];
  NSString *collaborator3 = [proteinSource parameterForKey:kCanonicalParameterInfopresenceParticipant3Key];
  NSString *collaborator4 = [proteinSource parameterForKey:kCanonicalParameterInfopresenceParticipant4Key];
  
  MZRemoteMezz *collaborator1Mezz = [systemModel remoteMezzWithUid:collaborator1];
  MZRemoteMezz *collaborator2Mezz = [systemModel remoteMezzWithUid:collaborator2];
  MZRemoteMezz *collaborator3Mezz = [systemModel remoteMezzWithUid:collaborator3];
  MZRemoteMezz *collaborator4Mezz = [systemModel remoteMezzWithUid:collaborator4];
  collaborator1Mezz.collaborationState = MZRemoteMezzCollaborationStateInCollaboration;
  collaborator2Mezz.collaborationState = MZRemoteMezzCollaborationStateNotInCollaboration;
  collaborator3Mezz.collaborationState = MZRemoteMezzCollaborationStateNotInCollaboration;
  collaborator4Mezz.collaborationState = MZRemoteMezzCollaborationStateNotInCollaboration;
  
  systemModel.infopresence.status = MZInfopresenceStatusActive;
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionDetail responseType:@"psa?v3.0"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  NSArray *collaborators = [systemModel remoteMezzesWithCollaborationState:MZRemoteMezzCollaborationStateInCollaboration];
  XCTAssertTrue((collaborators.count == 2), @"Number of collaborators should be 2");
  
  XCTAssertTrue((collaborator1Mezz.collaborationState == MZRemoteMezzCollaborationStateInCollaboration), @"%@ should be in state MZRemoteMezzCollaborationStateInCollaboration", collaborator1);
  XCTAssertTrue((collaborator2Mezz.collaborationState == MZRemoteMezzCollaborationStateInCollaboration), @"%@ should be in state MZRemoteMezzCollaborationStateInCollaboration", collaborator2);
  
  XCTAssertTrue((collaborator3Mezz.collaborationState == MZRemoteMezzCollaborationStateNotInCollaboration), @"%@ should be in state MZRemoteMezzCollaborationStateNotInCollaboration", collaborator3);
  XCTAssertTrue((collaborator4Mezz.collaborationState == MZRemoteMezzCollaborationStateNotInCollaboration), @"%@ should be in state MZRemoteMezzCollaborationStateNotInCollaboration", collaborator4);
}


-(void) testInfopresenceDetail_CollaboratosLeft
{
  [self setUpConnectedCommunicator];
  [self prepareSystemModelForInfopresenceTests];
  
  NSString *collaborator1 = [proteinSource parameterForKey:kCanonicalParameterInfopresenceParticipant1Key];
  NSString *collaborator2 = [proteinSource parameterForKey:kCanonicalParameterInfopresenceParticipant2Key];
  NSString *collaborator3 = [proteinSource parameterForKey:kCanonicalParameterInfopresenceParticipant3Key];
  NSString *collaborator4 = [proteinSource parameterForKey:kCanonicalParameterInfopresenceParticipant4Key];
  
  MZRemoteMezz *collaborator1Mezz = [systemModel remoteMezzWithUid:collaborator1];
  MZRemoteMezz *collaborator2Mezz = [systemModel remoteMezzWithUid:collaborator2];
  MZRemoteMezz *collaborator3Mezz = [systemModel remoteMezzWithUid:collaborator3];
  MZRemoteMezz *collaborator4Mezz = [systemModel remoteMezzWithUid:collaborator4];
  collaborator1Mezz.collaborationState = MZRemoteMezzCollaborationStateInCollaboration;
  collaborator2Mezz.collaborationState = MZRemoteMezzCollaborationStateInCollaboration;
  collaborator3Mezz.collaborationState = MZRemoteMezzCollaborationStateInCollaboration;
  collaborator4Mezz.collaborationState = MZRemoteMezzCollaborationStateInCollaboration;
  
  systemModel.infopresence.status = MZInfopresenceStatusActive;
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionDetail responseType:@"psa?v3.0"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  NSArray *collaborators = [systemModel remoteMezzesWithCollaborationState:MZRemoteMezzCollaborationStateInCollaboration];
  XCTAssertTrue((collaborators.count == 2), @"Number of collaborators should be 2");
  
  XCTAssertTrue((collaborator1Mezz.collaborationState == MZRemoteMezzCollaborationStateInCollaboration), @"%@ should be in state MZRemoteMezzCollaborationStateInCollaboration", collaborator1);
  XCTAssertTrue((collaborator2Mezz.collaborationState == MZRemoteMezzCollaborationStateInCollaboration), @"%@ should be in state MZRemoteMezzCollaborationStateInCollaboration", collaborator2);
  XCTAssertTrue((collaborator3Mezz.collaborationState == MZRemoteMezzCollaborationStateNotInCollaboration), @"%@ should be in state MZRemoteMezzCollaborationStateNotInCollaboration", collaborator3);
  XCTAssertTrue((collaborator4Mezz.collaborationState == MZRemoteMezzCollaborationStateNotInCollaboration), @"%@ should be in state MZRemoteMezzCollaborationStateNotInCollaboration", collaborator4);
}


-(void) testInfopresenceDetail_CollaborationEnded
{
  [self setUpConnectedCommunicator];
  [self prepareSystemModelForInfopresenceTests];
  
  systemModel.infopresence.status = MZInfopresenceStatusActive;
  
  XCTAssertTrue(systemModel.infopresence.status == MZInfopresenceStatusActive, @"Should be in collaboration at the beginning of test");
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionDetail responseType:@"psa?not-active"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertFalse(systemModel.infopresence.status == MZInfopresenceStatusActive, @"Should not be in collaboration");
  
  NSString *collaborator1 = [proteinSource parameterForKey:kCanonicalParameterInfopresenceParticipant1Key];
  NSString *collaborator2 = [proteinSource parameterForKey:kCanonicalParameterInfopresenceParticipant2Key];
  NSString *collaborator3 = [proteinSource parameterForKey:kCanonicalParameterInfopresenceParticipant3Key];
  NSString *collaborator4 = [proteinSource parameterForKey:kCanonicalParameterInfopresenceParticipant4Key];
  
  MZRemoteMezz *collaborator1Mezz = [systemModel remoteMezzWithUid:collaborator1];
  MZRemoteMezz *collaborator2Mezz = [systemModel remoteMezzWithUid:collaborator2];
  MZRemoteMezz *collaborator3Mezz = [systemModel remoteMezzWithUid:collaborator3];
  MZRemoteMezz *collaborator4Mezz = [systemModel remoteMezzWithUid:collaborator4];
  
  XCTAssertTrue((collaborator1Mezz.collaborationState == MZRemoteMezzCollaborationStateNotInCollaboration), @"%@ should be in state MZRemoteMezzCollaborationStateInCollaboration", collaborator1);
  XCTAssertTrue((collaborator2Mezz.collaborationState == MZRemoteMezzCollaborationStateNotInCollaboration), @"%@ should be in state MZRemoteMezzCollaborationStateInCollaboration", collaborator2);
  XCTAssertTrue((collaborator3Mezz.collaborationState == MZRemoteMezzCollaborationStateNotInCollaboration), @"%@ should be in state MZRemoteMezzCollaborationStateNotInCollaboration", collaborator3);
  XCTAssertTrue((collaborator4Mezz.collaborationState == MZRemoteMezzCollaborationStateNotInCollaboration), @"%@ should be in state MZRemoteMezzCollaborationStateNotInCollaboration", collaborator4);
}


-(void) testInfopresenceDetail_WithVideoChatInfo
{
  [self setUpConnectedCommunicator];
  [self prepareSystemModelForInfopresenceTests];
  
  NSString *videoChatService = [proteinSource parameterForKey:kCanonicalParameterInfopresenceVideoChatPexipService];
  NSString *pexipConference = [proteinSource parameterForKey:kCanonicalParameterInfopresenceVideoChatPexipConferenceUid];
  NSString *pexipNode = [proteinSource parameterForKey:kCanonicalParameterInfopresenceVideoChatPexipNodeUid];
  
  systemModel.infopresence.videoChatService = nil;
  systemModel.infopresence.pexipConference = nil;
  systemModel.infopresence.pexipNode = nil;
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionDetail responseType:@"psa?video-chat"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertTrue(([systemModel.infopresence.videoChatService isEqualToString:videoChatService]), @"Video chat service should be %@", videoChatService);
  XCTAssertTrue(([systemModel.infopresence.pexipConference isEqualToString:pexipConference]), @"Pexip conference ID should be %@", pexipConference);
  XCTAssertTrue(([systemModel.infopresence.pexipNode isEqualToString:pexipNode]), @"Pexip node should be %@", pexipNode);
  
  key = [proteinSource proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionDetail responseType:@"psa?not-active"];
  p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertNil(systemModel.infopresence.videoChatService, @"Video chat service should be nil");
  XCTAssertNil(systemModel.infopresence.pexipConference, @"Pexip conference ID should be nil");
  XCTAssertNil(systemModel.infopresence.pexipNode, @"Pexip node should be nil");
}

-(void) testInfopresenceDetail_NSNullValues
{
  [self setUpConnectedCommunicator];
  [self prepareSystemModelForInfopresenceTests];

  systemModel.infopresence.remoteAccessStatus = MZInfopresenceRemoteAccessStatusInactive;

  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionDetail responseType:@"psa?null-remote-access-values"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];

  XCTAssertNil((systemModel.infopresence.remoteAccessUrl), @"Remote Access URL should be nil and not crash");
  XCTAssertTrue((systemModel.infopresence.remoteAccessStatus == MZInfopresenceRemoteAccessStatusInactive), @"Remote Access Status should be inactive and not crash");
}


#pragma mark - Incoming Join Requests

-(void) testInfopresenceSessionIncomingJoinRequest_ReceivedFirstCall
{
  [self setUpConnectedCommunicator];
  [self prepareSystemModelForInfopresenceTests];
  
  MZInfopresence *infopresence = systemModel.infopresence;
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionIncomingRequest responseType:@"psa"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertTrue((infopresence.incomingCalls.count == 1), @"systemModel.infopresence.incomingCalls.count should be 1");
  
  NSString *incomingRequestUid = [proteinSource parameterForKey:kCanonicalParameterInfopresenceIncomingJoinRequestUidKey];
  XCTAssertEqual(infopresence.incomingCalls.count, 1);
  XCTAssertTrue([infopresence incomingCallWithUid:incomingRequestUid] != nil);
  MZInfopresenceCall *call = infopresence.incomingCalls[0];
  XCTAssertEqualObjects(call.uid, incomingRequestUid);
  XCTAssertEqual(call.type, MZInfopresenceCallTypeJoinRequest);
  
  MZRemoteMezz *remoteMezz = [systemModel remoteMezzWithUid:incomingRequestUid];
  XCTAssertTrue((remoteMezz.collaborationState == MZRemoteMezzCollaborationStateJoining), @"collaborationState should be MZRemoteMezzCollaborationStateJoining");
  XCTAssertTrue((remoteMezz.callResolution == MZInfopresenceCallResolutionUnknown), @"callResolution should be MZInfopresenceCallResolutionUnknown");
  
  XCTAssertEqualObjects([systemModel.infopresence.incomingCalls[0] uid], incomingRequestUid, @"the incoming request should be the first call.");
}


-(void) testInfopresenceSessionIncomingJoinRequest_ReceivedSecondCall
{
  [self setUpConnectedCommunicator];
  [self prepareSystemModelForInfopresenceTests];
  
  MZInfopresence *infopresence = systemModel.infopresence;
  
  // Prepare model to have an existing incoming request
  NSString *existingIncomingRequestUid = [proteinSource parameterForKey:kCanonicalParameterInfopresenceParticipant1Key];
  MZInfopresenceCall *existingCall = [[MZInfopresenceCall alloc] initWithDictionary:@{ @"uid": existingIncomingRequestUid,
                                                                                       @"type": kMZInfopresenceCallTypeJoinRequest }];
  [infopresence.incomingCalls addObject:existingCall];
  [systemModel remoteMezzWithUid:existingIncomingRequestUid].collaborationState = MZRemoteMezzCollaborationStateJoining;
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionIncomingRequest responseType:@"psa"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertTrue((infopresence.incomingCalls.count == 2), @"systemModel.infopresence.incomingCalls.count should be 2");
  
  NSString *incomingRequestUid = [proteinSource parameterForKey:kCanonicalParameterInfopresenceIncomingJoinRequestUidKey];
  
  XCTAssertTrue([infopresence incomingCallWithUid:existingIncomingRequestUid], @"systemModel.infopresence.incomingCalls should contain old call");
  XCTAssertTrue([infopresence incomingCallWithUid:incomingRequestUid], @"systemModel.infopresence.incomingCalls should contain new call");
  
  MZRemoteMezz *remoteMezz1 = [systemModel remoteMezzWithUid:existingIncomingRequestUid];
  XCTAssertTrue((remoteMezz1.collaborationState == MZRemoteMezzCollaborationStateJoining), @"collaborationState should be MZRemoteMezzCollaborationStateJoining");
  XCTAssertTrue((remoteMezz1.callResolution == MZInfopresenceCallResolutionUnknown), @"callResolution should be MZInfopresenceCallResolutionUnknown");
  
  MZRemoteMezz *remoteMezz2 = [systemModel remoteMezzWithUid:incomingRequestUid];
  XCTAssertTrue((remoteMezz2.collaborationState == MZRemoteMezzCollaborationStateJoining), @"collaborationState should be MZRemoteMezzCollaborationStateJoining");
  XCTAssertTrue((remoteMezz2.callResolution == MZInfopresenceCallResolutionUnknown), @"callResolution should be MZInfopresenceCallResolutionUnknown");
  
  MZInfopresenceCall *firstCall = infopresence.incomingCalls[0];
  XCTAssertEqual(firstCall.type, MZInfopresenceCallTypeJoinRequest);
  XCTAssertEqualObjects(firstCall.uid, existingIncomingRequestUid, @"First call should still be the first item in incoming request list.");
  MZInfopresenceCall *secondCall = infopresence.incomingCalls[1];
  XCTAssertEqual(secondCall.type, MZInfopresenceCallTypeJoinRequest);
  XCTAssertEqualObjects(secondCall.uid, incomingRequestUid, @"The new call should be the second one in the incoming request list.");
}


-(void) testInfopresenceSessionIncomingJoinRequestResolved_Accepted
{
  [self setUpConnectedCommunicator];
  [self prepareSystemModelForInfopresenceTests];
  MZInfopresence *infopresence = systemModel.infopresence;
  
  // Prepare model to have an existing incoming request
  NSString *incomingRequestUid = [proteinSource parameterForKey:kCanonicalParameterInfopresenceIncomingJoinRequestUidKey];
  MZInfopresenceCall *existingCall = [[MZInfopresenceCall alloc] initWithDictionary:@{ @"uid": incomingRequestUid,
                                                                                       @"type": kMZInfopresenceCallTypeJoinRequest }];
  [infopresence.incomingCalls addObject:existingCall];
  
  MZRemoteMezz *incomingRequestMezz = [systemModel remoteMezzWithUid:incomingRequestUid];
  incomingRequestMezz.collaborationState = MZRemoteMezzCollaborationStateJoining;
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionIncomingRequestResolve responseType:@"psa"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  MZRemoteMezz *remoteMezz = [systemModel remoteMezzWithUid:incomingRequestUid];
  XCTAssertEqual(remoteMezz.collaborationState, MZRemoteMezzCollaborationStateJoining, @"collaborationState should still be MZRemoteMezzCollaborationStateJoining");
  
  XCTAssertTrue((remoteMezz.callResolution == MZInfopresenceCallResolutionAccepted), @"callResolution should be %lu but is %lu", (unsigned long)MZInfopresenceCallResolutionAccepted, (unsigned long)remoteMezz.collaborationState);
  
  XCTAssertEqual(infopresence.incomingCalls.count, 0, @"Incoming requests should be empty afther call resolution.");
}


-(void) testInfopresenceSessionIncomingJoinRequestResolved_Canceled
{
  [self setUpConnectedCommunicator];
  [self prepareSystemModelForInfopresenceTests];
  MZInfopresence *infopresence = systemModel.infopresence;
  
  NSString *incomingRequestUid = [proteinSource parameterForKey:kCanonicalParameterInfopresenceIncomingJoinRequestUidKey];
  MZInfopresenceCall *existingCall = [[MZInfopresenceCall alloc] initWithDictionary:@{ @"uid": incomingRequestUid,
                                                                                       @"type": kMZInfopresenceCallTypeJoinRequest }];
  [infopresence.incomingCalls addObject:existingCall];
  
  MZRemoteMezz *incomingRequestMezz = [systemModel remoteMezzWithUid:incomingRequestUid];
  incomingRequestMezz.collaborationState = MZRemoteMezzCollaborationStateJoining;
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionIncomingRequestResolve responseType:@"psa?canceled"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  MZRemoteMezz *remoteMezz = [systemModel remoteMezzWithUid:incomingRequestUid];
  XCTAssertTrue((remoteMezz.collaborationState == MZRemoteMezzCollaborationStateNotInCollaboration), @"collaborationState %ld should be %ld", (long)remoteMezz.collaborationState, (long)MZRemoteMezzCollaborationStateNotInCollaboration);
  
  XCTAssertTrue((remoteMezz.callResolution == MZInfopresenceCallResolutionCanceled), @"callResolution should be %ld but is %ld", (long)MZInfopresenceCallResolutionCanceled, (long)remoteMezz.collaborationState);
  
  XCTAssertEqual(infopresence.incomingCalls.count, 0, @"Incoming requests should be empty afther call resolution.");
}


-(void) testInfopresenceSessionIncomingJoinRequestResolved_Declined
{
  [self setUpConnectedCommunicator];
  [self prepareSystemModelForInfopresenceTests];
  MZInfopresence *infopresence = systemModel.infopresence;
  
  NSString *incomingRequestUid = [proteinSource parameterForKey:kCanonicalParameterInfopresenceIncomingJoinRequestUidKey];
  MZInfopresenceCall *existingCall = [[MZInfopresenceCall alloc] initWithDictionary:@{ @"uid": incomingRequestUid,
                                                                                       @"type": kMZInfopresenceCallTypeJoinRequest }];
  [infopresence.incomingCalls addObject:existingCall];
  
  MZRemoteMezz *incomingRequestMezz = [systemModel remoteMezzWithUid:incomingRequestUid];
  incomingRequestMezz.collaborationState = MZRemoteMezzCollaborationStateJoining;
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionIncomingRequestResolve responseType:@"psa?declined"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  
  MZRemoteMezz *remoteMezz = [systemModel remoteMezzWithUid:incomingRequestUid];
  XCTAssertEqual(remoteMezz.collaborationState, MZRemoteMezzCollaborationStateNotInCollaboration);
  
  XCTAssertTrue((remoteMezz.callResolution == MZInfopresenceCallResolutionDeclined), @"callResolution should be %lu but is %lu", (unsigned long)MZInfopresenceCallResolutionDeclined, (unsigned long)(remoteMezz.collaborationState));
  
  XCTAssertEqual(infopresence.incomingCalls.count, 0, @"Incoming requests should be empty afther call resolution.");
}



#pragma mark - Incoming Invite Requests

-(void) testInfopresenceSessionIncomingInvite_ReceivedFirstCall
{
  [self setUpConnectedCommunicator];
  [self prepareSystemModelForInfopresenceTests];
  
  MZInfopresence *infopresence = systemModel.infopresence;
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameInfopresenceIncomingInvite responseType:@"psa"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertTrue((infopresence.incomingCalls.count == 1), @"systemModel.infopresence.incomingCalls.count should be 1");
  
  NSString *incomingInviteUid = [proteinSource parameterForKey:kCanonicalParameterInfopresenceIncomingInviteUidKey];
  MZInfopresenceCall *call = infopresence.incomingCalls[0];
  XCTAssertEqualObjects(call.uid, incomingInviteUid);
  XCTAssertEqual(call.type, MZInfopresenceCallTypeInvite);
  
  MZRemoteMezz *incomingRequestMezz = [systemModel remoteMezzWithUid:call.uid];
  XCTAssertTrue((incomingRequestMezz.callResolution == MZInfopresenceCallResolutionUnknown), @"callResolution should be MZInfopresenceCallResolutionUnknown");
}


-(void) testInfopresenceSessionIncomingInvite_ReceivedSecondCall
{
  [self setUpConnectedCommunicator];
  [self prepareSystemModelForInfopresenceTests];
  
  MZInfopresence *infopresence = systemModel.infopresence;
  
  // Prepare model to have an existing incoming request
  NSString *existingIncomingInviteUid = [proteinSource parameterForKey:kCanonicalParameterInfopresenceParticipant1Key];
  [infopresence.incomingCalls addObject:[[MZInfopresenceCall alloc] initWithDictionary:@{ @"uid":existingIncomingInviteUid,
                                                                                          @"type": kMZInfopresenceCallTypeInvite } ]];
  [systemModel remoteMezzWithUid:existingIncomingInviteUid].collaborationState = MZRemoteMezzCollaborationStateJoining;
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameInfopresenceIncomingInvite responseType:@"psa"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertTrue((infopresence.incomingCalls.count == 2), @"systemModel.infopresence.incomingCalls.count should be 2");
  
  NSString *incomingInviteUid = [proteinSource parameterForKey:kCanonicalParameterInfopresenceIncomingInviteUidKey];
  MZInfopresenceCall *firstCall = infopresence.incomingCalls[0];
  XCTAssertEqualObjects(firstCall.uid, existingIncomingInviteUid);
  XCTAssertEqual(firstCall.type, MZInfopresenceCallTypeInvite);
  
  MZInfopresenceCall *secondCall = infopresence.incomingCalls[1];
  XCTAssertEqualObjects(secondCall.uid, incomingInviteUid);
  XCTAssertEqual(secondCall.type, MZInfopresenceCallTypeInvite);
  
  MZRemoteMezz *remoteMezz1 = [systemModel remoteMezzWithUid:existingIncomingInviteUid];
  XCTAssertTrue((remoteMezz1.collaborationState == MZRemoteMezzCollaborationStateJoining), @"collaborationState should be MZRemoteMezzCollaborationStateJoining");
  XCTAssertTrue((remoteMezz1.callResolution == MZInfopresenceCallResolutionUnknown), @"callResolution should be MZInfopresenceCallResolutionUnknown");
  
  MZRemoteMezz *remoteMezz2 = [systemModel remoteMezzWithUid:incomingInviteUid];
  XCTAssertTrue((remoteMezz2.collaborationState == MZRemoteMezzCollaborationStateNotInCollaboration), @"collaborationState should be MZRemoteMezzCollaborationStateJoining");
  XCTAssertTrue((remoteMezz2.callResolution == MZInfopresenceCallResolutionUnknown), @"callResolution should be MZInfopresenceCallResolutionUnknown");
}


-(void) testInfopresenceSessionIncomingInviteResolved_Accepted
{
  [self setUpConnectedCommunicator];
  [self prepareSystemModelForInfopresenceTests];
  MZInfopresence *infopresence = systemModel.infopresence;
  
  // Prepare model to have an existing incoming request
  NSString *incomingInviteUid = [proteinSource parameterForKey:kCanonicalParameterInfopresenceIncomingInviteUidKey];
  [infopresence.incomingCalls addObject:[[MZInfopresenceCall alloc] initWithDictionary:@{ @"uid":incomingInviteUid,
                                                                                          @"type": kMZInfopresenceCallTypeInvite } ]];
  MZRemoteMezz *incomingRequestMezz = [systemModel remoteMezzWithUid:incomingInviteUid];
  incomingRequestMezz.collaborationState = MZRemoteMezzCollaborationStateInvited;
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameInfopresenceIncomingInviteResolve responseType:@"psa?accepted"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertTrue((infopresence.incomingCalls.count == 0), @"systemModel.infopresence.incomingCalls.count should be 0 after resolution");
  
  MZRemoteMezz *remoteMezz = [systemModel remoteMezzWithUid:incomingInviteUid];
  XCTAssertTrue((remoteMezz.collaborationState == MZRemoteMezzCollaborationStateInvited), @"collaborationState %lu should be %lu", (unsigned long)remoteMezz.collaborationState, (unsigned long)MZRemoteMezzCollaborationStateJoining);
  
  XCTAssertTrue((remoteMezz.callResolution == MZInfopresenceCallResolutionAccepted), @"callResolution should be %lu but is %lu", (unsigned long)MZInfopresenceCallResolutionAccepted, (unsigned long)remoteMezz.collaborationState);
  
  XCTAssertEqual(infopresence.incomingCalls.count, 0, @"Incoming requests should be empty afther call resolution.");
}


-(void) testInfopresenceSessionIncomingInviteResolved_Canceled
{
  [self setUpConnectedCommunicator];
  [self prepareSystemModelForInfopresenceTests];
  MZInfopresence *infopresence = systemModel.infopresence;
  
  NSString *incomingInviteUid = [proteinSource parameterForKey:kCanonicalParameterInfopresenceIncomingInviteUidKey];
  [infopresence.incomingCalls addObject:[[MZInfopresenceCall alloc] initWithDictionary:@{ @"uid":incomingInviteUid,
                                                                                          @"type": kMZInfopresenceCallTypeInvite } ]];
  MZRemoteMezz *incomingRequestMezz = [systemModel remoteMezzWithUid:incomingInviteUid];
  incomingRequestMezz.collaborationState = MZRemoteMezzCollaborationStateInvited;
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameInfopresenceIncomingInviteResolve responseType:@"psa?canceled"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertTrue((infopresence.incomingCalls.count == 0), @"systemModel.infopresence.incomingCalls.count should be 0 after resolution");
  
  MZRemoteMezz *remoteMezz = [systemModel remoteMezzWithUid:incomingInviteUid];
  XCTAssertEqual(remoteMezz.collaborationState, MZRemoteMezzCollaborationStateNotInCollaboration);
  
  XCTAssertTrue((remoteMezz.callResolution == MZInfopresenceCallResolutionCanceled), @"callResolution should be %ld but is %ld", (long)MZInfopresenceCallResolutionCanceled, (long)remoteMezz.collaborationState);
  
  XCTAssertEqual(infopresence.incomingCalls.count, 0, @"Incoming requests should be empty afther call resolution.");
}


-(void) testInfopresenceSessionIncomingInviteResolved_Declined
{
  [self setUpConnectedCommunicator];
  [self prepareSystemModelForInfopresenceTests];
  MZInfopresence *infopresence = systemModel.infopresence;
  
  NSString *incomingInviteUid = [proteinSource parameterForKey:kCanonicalParameterInfopresenceIncomingInviteUidKey];
  [infopresence.incomingCalls addObject:[[MZInfopresenceCall alloc] initWithDictionary:@{ @"uid":incomingInviteUid,
                                                                                          @"type": kMZInfopresenceCallTypeInvite } ]];
  MZRemoteMezz *incomingRequestMezz = [systemModel remoteMezzWithUid:incomingInviteUid];
  incomingRequestMezz.collaborationState = MZRemoteMezzCollaborationStateInvited;
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameInfopresenceIncomingInviteResolve responseType:@"psa?declined"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertTrue((infopresence.incomingCalls.count == 0), @"systemModel.infopresence.incomingCalls.count should be 0 after resolution");
  
  MZRemoteMezz *remoteMezz = [systemModel remoteMezzWithUid:incomingInviteUid];
  XCTAssertEqual(remoteMezz.collaborationState, MZRemoteMezzCollaborationStateNotInCollaboration);
  
  XCTAssertTrue((remoteMezz.callResolution == MZInfopresenceCallResolutionDeclined), @"callResolution should be %lu but is %lu", (unsigned long)MZInfopresenceCallResolutionDeclined, (unsigned long)(remoteMezz.collaborationState));
  
  XCTAssertEqual(infopresence.incomingCalls.count, 0, @"Incoming requests should be empty afther call resolution.");
}


#pragma mark - Outgoing Calls

-(void) testInfopresenceSessionOutgoingJoinRequest
{
  [self setUpConnectedCommunicator];
  [self prepareSystemModelForInfopresenceTests];
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionOutgoingRequest responseType:@"psa"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  NSString *outgoingRequestUid = [proteinSource parameterForKey:kCanonicalParameterInfopresenceOutgoingJoinRequestUidKey];
  MZRemoteMezz *remoteMezz = [systemModel remoteMezzWithUid:outgoingRequestUid];
  XCTAssertTrue((remoteMezz.collaborationState == MZRemoteMezzCollaborationStateJoining), @"collaborationState should be MZRemoteMezzCollaborationStateJoining");
  XCTAssertTrue((remoteMezz.callResolution == MZInfopresenceCallResolutionUnknown), @"callResolution should be MZInfopresenceCallResolutionUnknown");
  
  XCTAssertEqualObjects([systemModel.infopresence.outgoingCalls[0] uid], outgoingRequestUid);
}


-(void) testInfopresenceSessionOutgoingJoinRequest_ErrorResponse
{
  [self setUpConnectedCommunicator];
  [self prepareSystemModelForInfopresenceTests];
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionOutgoingRequest responseType:@"response?error"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  NSString *outgoingRequestUid = [proteinSource parameterForKey:kCanonicalParameterInfopresenceOutgoingJoinRequestUidKey];
  MZRemoteMezz *remoteMezz = [systemModel remoteMezzWithUid:outgoingRequestUid];
  XCTAssertTrue((remoteMezz.collaborationState == MZRemoteMezzCollaborationStateNotInCollaboration), @"collaborationState should be MZRemoteMezzCollaborationStateNotInCollaboration");
  XCTAssertTrue((remoteMezz.callResolution == MZInfopresenceCallResolutionUnknown), @"callResolution should be MZInfopresenceCallResolutionUnknown");
  
  XCTAssertEqual(systemModel.infopresence.outgoingCalls.count, 0, @"There should be no outgoing request.");
}


-(void) testInfopresenceSessionOutgoingJoinRequestResolved
{
  [self setUpConnectedCommunicator];
  [self prepareSystemModelForInfopresenceTests];
  MZInfopresence *infopresence = systemModel.infopresence;
  
  NSString *outgoingRequestUid = [proteinSource parameterForKey:kCanonicalParameterInfopresenceOutgoingJoinRequestUidKey];
  MZRemoteMezz *remoteMezz = [systemModel remoteMezzWithUid:outgoingRequestUid];
  remoteMezz.collaborationState = MZRemoteMezzCollaborationStateJoining;
  [infopresence appendObjectToOutgoingCalls:[[MZInfopresenceCall alloc] initWithDictionary:@{
                                                                                             @"uid": outgoingRequestUid,
                                                                                             @"type": kMZInfopresenceCallTypeJoinRequest
                                                                                             }]];
  
  XCTAssertNotNil([infopresence.outgoingCalls[0] uid]);
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionOutgoingRequestResolve responseType:@"psa"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  
  remoteMezz = [systemModel remoteMezzWithUid:outgoingRequestUid];
  XCTAssertEqual(remoteMezz.collaborationState, MZRemoteMezzCollaborationStateJoining, @"collaborationState should still be MZRemoteMezzCollaborationStateJoining");
  
  XCTAssertTrue((remoteMezz.callResolution == MZInfopresenceCallResolutionAccepted), @"callResolution should be %lu but is %ld", (unsigned long)MZInfopresenceCallResolutionAccepted, (long)remoteMezz.collaborationState);
  
  XCTAssertEqual(infopresence.outgoingCalls.count, 0);
}


-(void) testInfopresenceSessionOutgoingJoinRequestResolved_Cancelled
{
  [self setUpConnectedCommunicator];
  [self prepareSystemModelForInfopresenceTests];
  MZInfopresence *infopresence = systemModel.infopresence;
  
  NSString *outgoingRequestUid = [proteinSource parameterForKey:kCanonicalParameterInfopresenceOutgoingJoinRequestUidKey];
  MZRemoteMezz *remoteMezz = [systemModel remoteMezzWithUid:outgoingRequestUid];
  remoteMezz.collaborationState = MZRemoteMezzCollaborationStateJoining;
  [infopresence appendObjectToOutgoingCalls:[[MZInfopresenceCall alloc] initWithDictionary:@{
                                                                                             @"uid": outgoingRequestUid,
                                                                                             @"type": kMZInfopresenceCallTypeJoinRequest
                                                                                             }]];
  
  XCTAssertEqual(infopresence.outgoingCalls.count, 1);
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionOutgoingRequestResolve responseType:@"psa?canceled"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  remoteMezz = [systemModel remoteMezzWithUid:outgoingRequestUid];
  XCTAssertEqual(remoteMezz.collaborationState, MZRemoteMezzCollaborationStateNotInCollaboration);
  
  XCTAssertTrue((remoteMezz.callResolution == MZInfopresenceCallResolutionCanceled), @"callResolution should be %lu but is %lu", (unsigned long)(MZInfopresenceCallResolutionCanceled), (unsigned long)(remoteMezz.callResolution));
  
  XCTAssertEqual(infopresence.outgoingCalls.count, 0);
}


-(void) testInfopresenceSessionOutgoingJoinRequestResolved_Declined
{
  [self setUpConnectedCommunicator];
  [self prepareSystemModelForInfopresenceTests];
  
  MZInfopresence *infopresence = systemModel.infopresence;
  
  NSString *outgoingRequestUid = [proteinSource parameterForKey:kCanonicalParameterInfopresenceOutgoingJoinRequestUidKey];
  MZRemoteMezz *remoteMezz = [systemModel remoteMezzWithUid:outgoingRequestUid];
  remoteMezz.collaborationState = MZRemoteMezzCollaborationStateJoining;
  [infopresence appendObjectToOutgoingCalls:[[MZInfopresenceCall alloc] initWithDictionary:@{
                                                                                             @"uid": outgoingRequestUid,
                                                                                             @"type": kMZInfopresenceCallTypeJoinRequest
                                                                                             }]];
  
  XCTAssertEqual(infopresence.outgoingCalls.count, 1);
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionOutgoingRequestResolve responseType:@"psa?declined"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  
  remoteMezz = [systemModel remoteMezzWithUid:outgoingRequestUid];
  XCTAssertEqual(remoteMezz.collaborationState, MZRemoteMezzCollaborationStateNotInCollaboration);
  
  XCTAssertTrue((remoteMezz.callResolution == MZInfopresenceCallResolutionDeclined), @"callResolution should be %lu but is %lu", (unsigned long)MZInfopresenceCallResolutionDeclined, (unsigned long)remoteMezz.collaborationState);
  
  XCTAssertEqual(infopresence.outgoingCalls.count, 0);
}


-(void) testInfopresenceSessionOutgoingInvite
{
  [self setUpConnectedCommunicator];
  [self prepareSystemModelForInfopresenceTests];
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameInfopresenceOutgoingInvite responseType:@"psa"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  NSString *outgoingInviteUid = [proteinSource parameterForKey:kCanonicalParameterInfopresenceOutgoingInviteUidKey];
  MZRemoteMezz *remoteMezz = [systemModel remoteMezzWithUid:outgoingInviteUid];
  XCTAssertTrue((remoteMezz.collaborationState == MZRemoteMezzCollaborationStateInvited), @"collaborationState should be MZRemoteMezzCollaborationStateJoining");
  XCTAssertTrue((remoteMezz.callResolution == MZInfopresenceCallResolutionUnknown), @"callResolution should be MZInfopresenceCallResolutionUnknown");
  
  MZInfopresenceCall *call = systemModel.infopresence.outgoingCalls[0];
  XCTAssertEqual(call.resolution, MZInfopresenceCallResolutionUnknown);
  XCTAssertEqualObjects(call.uid, outgoingInviteUid);
}


-(void) testInfopresenceSessionOutgoingInvite_ErrorResponse
{
  [self setUpConnectedCommunicator];
  [self prepareSystemModelForInfopresenceTests];
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameInfopresenceOutgoingInvite responseType:@"response?error"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  NSString *outgoingInviteUid = [proteinSource parameterForKey:kCanonicalParameterInfopresenceOutgoingInviteUidKey];
  MZRemoteMezz *remoteMezz = [systemModel remoteMezzWithUid:outgoingInviteUid];
  XCTAssertTrue((remoteMezz.collaborationState == MZRemoteMezzCollaborationStateNotInCollaboration), @"collaborationState should be MZRemoteMezzCollaborationStateNotInCollaboration");
  XCTAssertTrue((remoteMezz.callResolution == MZInfopresenceCallResolutionUnknown), @"callResolution should be MZInfopresenceCallResolutionUnknown");
  
  XCTAssertEqual(systemModel.infopresence.outgoingCalls.count, 0, @"There should be no outgoing request.");
}


-(void) testInfopresenceSessionOutgoingInviteResolved_Accepted
{
  [self setUpConnectedCommunicator];
  [self prepareSystemModelForInfopresenceTests];
  MZInfopresence *infopresence = systemModel.infopresence;
  
  NSString *outgoingInviteUid = [proteinSource parameterForKey:kCanonicalParameterInfopresenceOutgoingInviteUidKey];
  MZRemoteMezz *remoteMezz = [systemModel remoteMezzWithUid:outgoingInviteUid];
  remoteMezz.collaborationState = MZRemoteMezzCollaborationStateInvited;
  MZInfopresenceCall *call = [[MZInfopresenceCall alloc] initWithDictionary:@{
                                                                              @"uid": outgoingInviteUid,
                                                                              @"type": kMZInfopresenceCallTypeInvite
                                                                              }];
  [infopresence appendObjectToOutgoingCalls:call];
  
  XCTAssertNotNil([infopresence.outgoingCalls[0] uid]);
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameInfopresenceOutgoingInviteResolve responseType:@"psa?accepted"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  
  remoteMezz = [systemModel remoteMezzWithUid:outgoingInviteUid];
  XCTAssertTrue((remoteMezz.collaborationState == MZRemoteMezzCollaborationStateInvited), @"collaborationState should still be MZRemoteMezzCollaborationStateInvited");
  XCTAssertEqual(call.resolution, MZInfopresenceCallResolutionAccepted);
  XCTAssertTrue((remoteMezz.callResolution == MZInfopresenceCallResolutionAccepted), @"callResolution should be %lu but is %ld", (unsigned long)MZInfopresenceCallResolutionAccepted, (long)remoteMezz.collaborationState);
  
  XCTAssertEqual(infopresence.outgoingCalls.count, 0);
}


-(void) testInfopresenceSessionOutgoingInviteResolved_Cancelled
{
  [self setUpConnectedCommunicator];
  [self prepareSystemModelForInfopresenceTests];
  MZInfopresence *infopresence = systemModel.infopresence;
  
  NSString *outgoingInviteUid = [proteinSource parameterForKey:kCanonicalParameterInfopresenceOutgoingInviteUidKey];
  MZRemoteMezz *remoteMezz = [systemModel remoteMezzWithUid:outgoingInviteUid];
  remoteMezz.collaborationState = MZRemoteMezzCollaborationStateInvited;
  MZInfopresenceCall *call = [[MZInfopresenceCall alloc] initWithDictionary:@{
                                                                              @"uid": outgoingInviteUid,
                                                                              @"type": kMZInfopresenceCallTypeInvite
                                                                              }];
  [infopresence appendObjectToOutgoingCalls:call];
  
  XCTAssertEqual(infopresence.outgoingCalls.count, 1);
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameInfopresenceOutgoingInviteResolve responseType:@"psa?canceled"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertEqual(remoteMezz.collaborationState, MZRemoteMezzCollaborationStateNotInCollaboration);
  XCTAssertEqual(call.resolution, MZInfopresenceCallResolutionCanceled);
  XCTAssertTrue((remoteMezz.callResolution == MZInfopresenceCallResolutionCanceled), @"callResolution should be %lu but is %lu", (unsigned long)(MZInfopresenceCallResolutionCanceled), (unsigned long)(remoteMezz.callResolution));
  
  XCTAssertEqual(infopresence.outgoingCalls.count, 0);
}


-(void) testInfopresenceSessionOutgoingInviteResolved_Declined
{
  [self setUpConnectedCommunicator];
  [self prepareSystemModelForInfopresenceTests];
  
  MZInfopresence *infopresence = systemModel.infopresence;
  
  NSString *outgoingInviteUid = [proteinSource parameterForKey:kCanonicalParameterInfopresenceOutgoingInviteUidKey];
  MZRemoteMezz *remoteMezz = [systemModel remoteMezzWithUid:outgoingInviteUid];
  remoteMezz.collaborationState = MZRemoteMezzCollaborationStateInvited;
  MZInfopresenceCall *call = [[MZInfopresenceCall alloc] initWithDictionary:@{
                                                                              @"uid": outgoingInviteUid,
                                                                              @"type": kMZInfopresenceCallTypeInvite
                                                                              }];
  [infopresence appendObjectToOutgoingCalls:call];
  
  XCTAssertEqual(infopresence.outgoingCalls.count, 1);
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameInfopresenceOutgoingInviteResolve responseType:@"psa?declined"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  
  remoteMezz = [systemModel remoteMezzWithUid:outgoingInviteUid];
  XCTAssertEqual(remoteMezz.collaborationState, MZRemoteMezzCollaborationStateNotInCollaboration);
  XCTAssertEqual(call.resolution, MZInfopresenceCallResolutionDeclined);
  XCTAssertTrue((remoteMezz.callResolution == MZInfopresenceCallResolutionDeclined), @"callResolution should be %lu but is %lu", (unsigned long)MZInfopresenceCallResolutionDeclined, (unsigned long)remoteMezz.collaborationState);
  
  XCTAssertEqual(infopresence.outgoingCalls.count, 0);
}


-(void) testInfopresenceRemoteAccessIsStarting
{
  [self setUpConnectedCommunicator];
  [self prepareSystemModelForInfopresenceHackWithMyMezzanine];
  
  MZInfopresence *infopresence = systemModel.infopresence;
  
  XCTAssertTrue((infopresence.remoteAccessStatus == MZInfopresenceRemoteAccessStatusInactive), @"Remote Access state should still be MZInfopresenceRemoteAccessStatusInactive");
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionDetail responseType:@"psa?remote-access-starting"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertTrue((infopresence.remoteAccessStatus == MZInfopresenceRemoteAccessStatusStarting), @"Remote Access state should be MZInfopresenceRemoteAccessStatusStarting");
  XCTAssertNil((infopresence.remoteAccessUrl), @"Remote Access URL is not ready yet and should be nil");
}


-(void) testInfopresenceRemoteAccessGetsActivated
{
  [self setUpConnectedCommunicator];
  [self prepareSystemModelForInfopresenceHackWithMyMezzanine];
  
  MZInfopresence *infopresence = systemModel.infopresence;
  infopresence.remoteAccessStatus = MZInfopresenceRemoteAccessStatusStarting;
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionDetail responseType:@"psa?remote-access-active"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertTrue((infopresence.remoteAccessStatus == MZInfopresenceRemoteAccessStatusActive), @"Remote Access state should be MZInfopresenceRemoteAccessStatusActive");
  XCTAssertNotNil((infopresence.remoteAccessUrl), @"Remote Access URL is not ready yet and should be nil");
}


-(void) testInfopresenceRemoteAccessGetsDeactivated
{
  [self setUpConnectedCommunicator];
  [self prepareSystemModelForInfopresenceHackWithMyMezzanine];
  
  MZInfopresence *infopresence = systemModel.infopresence;
  infopresence.remoteAccessStatus = MZInfopresenceRemoteAccessStatusActive;
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionDetail responseType:@"psa?remote-access-inactive"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertTrue((infopresence.remoteAccessStatus == MZInfopresenceRemoteAccessStatusInactive), @"Remote Access state should be MZInfopresenceRemoteAccessStatusActive");
  XCTAssertNil((infopresence.remoteAccessUrl), @"Remote Access URL is not ready yet and should be nil");
}


- (void)prepareSystemModelForRemoteParticipantsTests
{
  [self prepareSystemModelForInfopresenceHackWithMyMezzanine];
  
  void (^addRemoteParticipantWithParameterKey)(NSString*) = ^(NSString *key)
  {
    MZInfopresence *infopresence = systemModel.infopresence;
    NSDictionary *remoteParticipantDictionary = [proteinSource parameterForKey:key];
    MZRemoteParticipant *remoteParticipant = [MZRemoteParticipant new];
    [remoteParticipant updateWithDictionary:remoteParticipantDictionary];
    [infopresence.remoteParticipants addObject:remoteParticipant];
  };
  
  addRemoteParticipantWithParameterKey(kCanonicalParameterInfopresenceRemoteParticipant1Key);
  addRemoteParticipantWithParameterKey(kCanonicalParameterInfopresenceRemoteParticipant2Key);
}


-(void) testInfopresenceAddFirstRemoteParticipant
{
  [self setUpConnectedCommunicator];
  [self prepareSystemModelForInfopresenceHackWithMyMezzanine];
  
  MZInfopresence *infopresence = systemModel.infopresence;
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionDetail responseType:@"psa?new-remote-participant"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertTrue((infopresence.remoteParticipants.count == 1), @"There should be just one remote participant but there are %lu", (unsigned long)infopresence.remoteParticipants.count);
  
  MZRemoteParticipant *participant = [infopresence.remoteParticipants firstObject];
  
  NSDictionary *remoteParticipant1Dictionary = [proteinSource parameterForKey:kCanonicalParameterInfopresenceRemoteParticipant1Key];
  
  XCTAssertEqual(participant.displayName, remoteParticipant1Dictionary[@"display-name"], @"Remote Participant display name should be %@ but it is %@", remoteParticipant1Dictionary[@"display-name"], participant.displayName);
  XCTAssertEqual(participant.uid, remoteParticipant1Dictionary[@"uid"], @"Remote Participant display name should be %@ but it is %@", remoteParticipant1Dictionary[@"uid"], participant.uid);
}


-(void) testInfopresenceAddSecondRemoteParticipant
{
  [self setUpConnectedCommunicator];
  [self prepareSystemModelForInfopresenceHackWithMyMezzanine];
  
  MZInfopresence *infopresence = systemModel.infopresence;
  NSDictionary *remoteParticipant1Dictionary = [proteinSource parameterForKey:kCanonicalParameterInfopresenceRemoteParticipant1Key];
  MZRemoteParticipant *remoteParticipant1 = [MZRemoteParticipant new];
  [remoteParticipant1 updateWithDictionary:remoteParticipant1Dictionary];
  [infopresence.remoteParticipants addObject:remoteParticipant1];
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionDetail responseType:@"psa?more-new-remote-participants"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertTrue((infopresence.remoteParticipants.count == 2), @"There should be two remote participants but there are %lu", (unsigned long)infopresence.remoteParticipants.count);
  
  MZRemoteParticipant *participant = infopresence.remoteParticipants[1];
  
  NSDictionary *remoteParticipant2Dictionary = [proteinSource parameterForKey:kCanonicalParameterInfopresenceRemoteParticipant2Key];
  
  XCTAssertEqual(participant.displayName, remoteParticipant2Dictionary[@"display-name"], @"Remote Participant display name should be %@ but it is %@", remoteParticipant2Dictionary[@"display-name"], participant.displayName);
  XCTAssertEqual(participant.uid, remoteParticipant2Dictionary[@"uid"], @"Remote Participant display name should be %@ but it is %@", remoteParticipant2Dictionary[@"uid"], participant.uid);
}


-(void) testInfopresenceRemoteParticipantJoin
{
  [self setUpConnectedCommunicator];
  [self prepareSystemModelForRemoteParticipantsTests];
  
  MZInfopresence *infopresence = systemModel.infopresence;
  // Reset participants
  [infopresence.remoteParticipants removeAllObjects];
  
  // First participant joined
  [websocketPoolConnector receivedProtein:[proteinSource proteinForKey:[proteinSource proteinKeyForTransactionName:MZTransactionNameInfopresenceParticipantJoin responseType:@"psa?participant"]]];
  
  XCTAssertEqual(infopresence.remoteParticipants.count, 1);
  
  MZRemoteParticipant *participant = [infopresence.remoteParticipants firstObject];
  NSDictionary *remoteParticipant1Dictionary = [proteinSource parameterForKey:kCanonicalParameterInfopresenceRemoteParticipant1Key];
  
  XCTAssertEqual(participant.displayName, remoteParticipant1Dictionary[@"display-name"], @"Remote Participant display name should be %@ but it is %@", remoteParticipant1Dictionary[@"display-name"], participant.displayName);
  XCTAssertEqual(participant.uid, remoteParticipant1Dictionary[@"uid"], @"Remote Participant display name should be %@ but it is %@", remoteParticipant1Dictionary[@"uid"], participant.uid);
  
  // Second participant joined
  [websocketPoolConnector receivedProtein:[proteinSource proteinForKey:[proteinSource proteinKeyForTransactionName:MZTransactionNameInfopresenceParticipantJoin responseType:@"psa?participant2"]]];
  XCTAssertEqual(infopresence.remoteParticipants.count, 2);
}


-(void) testInfopresenceExistingRemoteParticipantJoin
{
  [self setUpConnectedCommunicator];
  [self prepareSystemModelForRemoteParticipantsTests];
  
  MZInfopresence *infopresence = systemModel.infopresence;
  
  // First existing participant joined
  [websocketPoolConnector receivedProtein:[proteinSource proteinForKey:[proteinSource proteinKeyForTransactionName:MZTransactionNameInfopresenceParticipantJoin responseType:@"psa?participant"]]];
  
  XCTAssertEqual(infopresence.remoteParticipants.count, 2);
  
  // Second participant joined
  [websocketPoolConnector receivedProtein:[proteinSource proteinForKey:[proteinSource proteinKeyForTransactionName:MZTransactionNameInfopresenceParticipantJoin responseType:@"psa?participant2"]]];
  XCTAssertEqual(infopresence.remoteParticipants.count, 2);
}


-(void) testInfopresenceRemoteParticipantLeave
{
  [self setUpConnectedCommunicator];
  [self prepareSystemModelForRemoteParticipantsTests];
  
  MZInfopresence *infopresence = systemModel.infopresence;
  XCTAssertEqual(infopresence.remoteParticipants.count, 2);
  
  [websocketPoolConnector receivedProtein:[proteinSource proteinForKey:[proteinSource proteinKeyForTransactionName:MZTransactionNameInfopresenceParticipantLeave responseType:@"psa?participant"]]];
  
  XCTAssertEqual(infopresence.remoteParticipants.count, 1);
  
  [websocketPoolConnector receivedProtein:[proteinSource proteinForKey:[proteinSource proteinKeyForTransactionName:MZTransactionNameInfopresenceParticipantLeave responseType:@"psa?participant2"]]];
  
  XCTAssertEqual(infopresence.remoteParticipants.count, 0);
}


-(void) testInfopresenceRoomJoin
{
  [self setUpConnectedCommunicator];
  [self prepareSystemModelForInfopresenceTests];
  [self prepareSystemModelForRemoteParticipantsTests];

  MZInfopresence *infopresence = systemModel.infopresence;

  // First room joined
  [websocketPoolConnector receivedProtein:[proteinSource proteinForKey:[proteinSource proteinKeyForTransactionName:MZTransactionNameInfopresenceParticipantJoin responseType:@"psa?room"]]];

  XCTAssertEqual(infopresence.rooms.count, 1);
  NSString *roomParticipant1Uid = [proteinSource parameterForKey:kCanonicalParameterInfopresenceParticipant1Key];
  XCTAssertEqualObjects([[infopresence.rooms objectAtIndex:0] uid], roomParticipant1Uid);

  // Second room joined
  [websocketPoolConnector receivedProtein:[proteinSource proteinForKey:[proteinSource proteinKeyForTransactionName:MZTransactionNameInfopresenceParticipantJoin responseType:@"psa?room2"]]];
  XCTAssertEqual(infopresence.rooms.count, 2);
  NSString *roomParticipant2Uid = [proteinSource parameterForKey:kCanonicalParameterInfopresenceParticipant2Key];
  XCTAssertEqualObjects([[infopresence.rooms objectAtIndex:1] uid], roomParticipant2Uid);
}


-(void) testInfopresenceRoomLeave
{
  [self setUpConnectedCommunicator];
  [self prepareSystemModelForInfopresenceTests];
  [self prepareSystemModelForRemoteParticipantsTests];

  MZInfopresence *infopresence = systemModel.infopresence;
  NSString *roomParticipant1Uid = [proteinSource parameterForKey:kCanonicalParameterInfopresenceParticipant1Key];
  NSString *roomParticipant2Uid = [proteinSource parameterForKey:kCanonicalParameterInfopresenceParticipant2Key];
  infopresence.rooms = [[NSMutableArray alloc] initWithObjects:[systemModel remoteMezzWithUid:roomParticipant1Uid], [systemModel remoteMezzWithUid:roomParticipant2Uid], nil];

  XCTAssertEqual(infopresence.rooms.count, 2);

  [websocketPoolConnector receivedProtein:[proteinSource proteinForKey:[proteinSource proteinKeyForTransactionName:MZTransactionNameInfopresenceParticipantLeave responseType:@"psa?room"]]];

  XCTAssertEqual(infopresence.rooms.count, 1);

  [websocketPoolConnector receivedProtein:[proteinSource proteinForKey:[proteinSource proteinKeyForTransactionName:MZTransactionNameInfopresenceParticipantLeave responseType:@"psa?room2"]]];

  XCTAssertEqual(infopresence.rooms.count, 0);
}


-(void) testInfopresenceOneRemoteParticipantHasLeftViaInfopresenceDetailProtein
{
  [self setUpConnectedCommunicator];
  [self prepareSystemModelForRemoteParticipantsTests];
  
  MZInfopresence *infopresence = systemModel.infopresence;
  XCTAssertTrue((infopresence.remoteParticipants.count == 2), @"There should be two remote participants but there are %lu", (unsigned long)infopresence.remoteParticipants.count);
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionDetail responseType:@"psa?new-remote-participant"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertTrue((infopresence.remoteParticipants.count == 1), @"There should be just one remote participant but there are %lu", (unsigned long)infopresence.remoteParticipants.count);
  
  MZRemoteParticipant *participant = [infopresence.remoteParticipants firstObject];
  
  NSDictionary *remoteParticipant1Dictionary = [proteinSource parameterForKey:kCanonicalParameterInfopresenceRemoteParticipant1Key];
  
  XCTAssertEqual(participant.displayName, remoteParticipant1Dictionary[@"display-name"], @"Remote Participant display name should be %@ but it is %@", remoteParticipant1Dictionary[@"display-name"], participant.displayName);
  XCTAssertEqual(participant.uid, remoteParticipant1Dictionary[@"uid"], @"Remote Participant display name should be %@ but it is %@", remoteParticipant1Dictionary[@"uid"], participant.uid);
}


-(void) testInfopresenceAllRemoteParticipantsHaveLeftViaInfopresenceDetailProtein
{
  [self setUpConnectedCommunicator];
  [self prepareSystemModelForRemoteParticipantsTests];
  
  MZInfopresence *infopresence = systemModel.infopresence;
  XCTAssertTrue((infopresence.remoteParticipants.count == 2), @"There should be two remote participants but there are %lu", (unsigned long)infopresence.remoteParticipants.count);
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionDetail responseType:@"psa?no-remote-participant"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertTrue((infopresence.remoteParticipants.count == 0), @"There should be just one remote participant but there are %lu", (unsigned long)infopresence.remoteParticipants.count);
}

// Bug 16225/16226
- (void)testFinalStateAfterMultipleIncomingInvitesOneIsAcceptedAndJoinedAndOtherInvitesAreDeclined
{
  [self setUpConnectedCommunicator];
  [self prepareSystemModelForInfopresenceTests];
  
  [websocketPoolConnector receivedProtein:[OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceIncomingInvite, @"psa"]
                                                                ingests:@{@"uid": kCanonicalParameterInfopresenceParticipant1Key}]];
  
  [websocketPoolConnector receivedProtein: [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionDetail, @"psa"]
                                                                 ingests:@{ @"active" : @NO,
                                                                            @"incoming-calls" : @[@{@"uid":kCanonicalParameterInfopresenceParticipant1Key,
                                                                                                    @"received-utc":@1460741558.3504021,
                                                                                                    @"type": @"invite"}]
                                                                            }]];
  
  [websocketPoolConnector receivedProtein:[OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceIncomingInvite, @"psa"]
                                                                ingests:@{@"uid": kCanonicalParameterInfopresenceParticipant2Key}]];
  [websocketPoolConnector receivedProtein: [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionDetail, @"psa"]
                                                                 ingests:@{ @"active" : @NO,
                                                                            @"incoming-calls" : @[@{@"uid":kCanonicalParameterInfopresenceParticipant1Key,
                                                                                                    @"received-utc":@1460741558.3504021,
                                                                                                    @"type": @"invite"},
                                                                                                  @{@"uid":kCanonicalParameterInfopresenceParticipant2Key,
                                                                                                    @"received-utc":@1460741560.3504021,
                                                                                                    @"type": @"invite"}]
                                                                            }]];
  
  [websocketPoolConnector receivedProtein: [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceIncomingInviteResolve, @"psa"]
                                                                 ingests:@{@"uid": kCanonicalParameterInfopresenceParticipant1Key,
                                                                           @"resolution": @"accepted"}]];
  
  [websocketPoolConnector receivedProtein: [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionDetail, @"psa"]
                                                                 ingests:@{ @"active" : @NO,
                                                                            @"incoming-calls" : @[@{@"uid":kCanonicalParameterInfopresenceParticipant2Key,
                                                                                                    @"received-utc":@1460741560.3504021,
                                                                                                    @"type": @"invite"}]
                                                                            }]];
  
  [websocketPoolConnector receivedProtein: [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionOutgoingRequest, @"psa"]
                                                                 ingests:@{@"uid": kCanonicalParameterInfopresenceParticipant1Key,
                                                                           @"sent-utc": @1460741562.0001}]];
  
  [websocketPoolConnector receivedProtein: [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionDetail, @"psa"]
                                                                 ingests:@{ @"active" : @NO,
                                                                            @"incoming-calls" : @[@{@"uid":kCanonicalParameterInfopresenceParticipant2Key,
                                                                                                    @"received-utc":@1460741560.3504021,
                                                                                                    @"type": @"invite"}],
                                                                            @"outgoing-calls" : @[@{@"uid":kCanonicalParameterInfopresenceParticipant1Key,
                                                                                                    @"received-utc":@1460741562.0001,
                                                                                                    @"type": @"join-request"}]
                                                                            }]];
  
  XCTAssertTrue((systemModel.infopresence.incomingCalls.count == 1), @"There should be 1 incoming call but there are %lu", (unsigned long)systemModel.infopresence.incomingCalls.count);
  
  [websocketPoolConnector receivedProtein: [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceIncomingInviteResolve, @"psa"]
                                                                 ingests:@{@"uid": kCanonicalParameterInfopresenceParticipant2Key,
                                                                           @"resolution": @"declined"}]];
  
  XCTAssertTrue((systemModel.infopresence.incomingCalls.count == 0), @"There should be no incoming call anymore but there are %lu", (unsigned long)systemModel.infopresence.incomingCalls.count);
}


#pragma mark - Live Streams Tests

- (void)testLiveStreamsDetailFromEmptyToOneToThree
{
  [self setUpConnectedCommunicator];
  MZWorkspace *workspace = [self prepareEmptyWorkspace];
  
  NSString *liveStreamUid1 = [proteinSource parameterForKey:kCanonicalParameterLiveStreamUid1Key];
  NSString *liveStreamUid2 = [proteinSource parameterForKey:kCanonicalParameterLiveStreamUid2Key];
  NSString *liveStreamUid3 = [proteinSource parameterForKey:kCanonicalParameterLiveStreamUid3Key];
  
  
  {
    NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameLiveStreamsDetail responseType:@"psa?1-active"];
    OBProtein *p = [proteinSource proteinForKey:key];
    [websocketPoolConnector receivedProtein:p];
  }
  
  XCTAssertTrue(workspace.liveStreams.count == 1, @"There should be %d live stream, but found %lu", 1, (unsigned long)(workspace.liveStreams.count));
  if (workspace.liveStreams.count > 0)
  {
    MZLiveStream *liveStream = workspace.liveStreams[0];
    XCTAssertEqualObjects(liveStream.uid, liveStreamUid1, @"Should be %@ but is %@", liveStreamUid1, liveStream.uid);
    XCTAssertTrue(liveStream.active, @"Should be active");
  }
  
  
  {
    NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameLiveStreamsDetail responseType:@"psa?3-active"];
    OBProtein *p = [proteinSource proteinForKey:key];
    [websocketPoolConnector receivedProtein:p];
  }
  
  XCTAssertTrue(workspace.liveStreams.count == 3, @"There should be %d live streams, but found %lu", 3, (unsigned long)(workspace.liveStreams.count));
  if (workspace.liveStreams.count == 3)
  {
    MZLiveStream *liveStream = workspace.liveStreams[0];
    XCTAssertEqualObjects(liveStream.uid, liveStreamUid1, @"Should be %@ but is %@", liveStreamUid1, liveStream.uid);
    XCTAssertTrue(liveStream.active, @"Should be active");
    
    liveStream = workspace.liveStreams[1];
    XCTAssertEqualObjects(liveStream.uid, liveStreamUid2, @"Should be %@ but is %@", liveStreamUid2, liveStream.uid);
    XCTAssertTrue(liveStream.active, @"Should be active");
    
    liveStream = workspace.liveStreams[2];
    XCTAssertEqualObjects(liveStream.uid, liveStreamUid3, @"Should be %@ but is %@", liveStreamUid3, liveStream.uid);
    XCTAssertTrue(liveStream.active, @"Should be active");
  }
}


- (void)testLiveStreamsDetailFromOneToTwoActive
{
  [self setUpConnectedCommunicator];
  MZWorkspace *workspace = [self prepareEmptyWorkspace];
  
  NSString *liveStreamUid1 = [proteinSource parameterForKey:kCanonicalParameterLiveStreamUid1Key];
  NSString *liveStreamUid2 = [proteinSource parameterForKey:kCanonicalParameterLiveStreamUid2Key];
  NSString *liveStreamUid3 = [proteinSource parameterForKey:kCanonicalParameterLiveStreamUid3Key];
  
  
  {
    NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameLiveStreamsDetail responseType:@"psa?1-active"];
    OBProtein *p = [proteinSource proteinForKey:key];
    [websocketPoolConnector receivedProtein:p];
  }
  
  XCTAssertTrue(workspace.liveStreams.count == 1, @"There should be %d live stream, but found %lu", 1, (unsigned long)(workspace.liveStreams.count));
  if (workspace.liveStreams.count > 0)
  {
    MZLiveStream *liveStream = workspace.liveStreams[0];
    XCTAssertEqualObjects(liveStream.uid, liveStreamUid1, @"Should be %@ but is %@", liveStreamUid1, liveStream.uid);
    XCTAssertTrue(liveStream.active, @"Should be active");
  }
  
  
  {
    NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameLiveStreamsDetail responseType:@"psa?2-active-1-inactive"];
    OBProtein *p = [proteinSource proteinForKey:key];
    [websocketPoolConnector receivedProtein:p];
  }
  
  XCTAssertTrue(workspace.liveStreams.count == 3, @"There should be %d live streams, but found %lu", 3, (unsigned long)(workspace.liveStreams.count));
  if (workspace.liveStreams.count == 3)
  {
    MZLiveStream *liveStream = workspace.liveStreams[0];
    XCTAssertEqualObjects(liveStream.uid, liveStreamUid1, @"Should be %@ but is %@", liveStreamUid1, liveStream.uid);
    XCTAssertTrue(liveStream.active, @"Should be active");
    
    liveStream = workspace.liveStreams[1];
    XCTAssertEqualObjects(liveStream.uid, liveStreamUid2, @"Should be %@ but is %@", liveStreamUid2, liveStream.uid);
    XCTAssertTrue(liveStream.active, @"Should be active");
    
    liveStream = workspace.liveStreams[2];
    XCTAssertEqualObjects(liveStream.uid, liveStreamUid3, @"Should be %@ but is %@", liveStreamUid3, liveStream.uid);
    XCTAssertTrue(!liveStream.active, @"Should be inactive");
  }
  
}


- (void)testLiveStreamsDetailFromThreeToOne
{
  [self setUpConnectedCommunicator];
  MZWorkspace *workspace = [self prepareEmptyWorkspace];
  
  NSString *liveStreamUid1 = [proteinSource parameterForKey:kCanonicalParameterLiveStreamUid1Key];
  NSString *liveStreamUid2 = [proteinSource parameterForKey:kCanonicalParameterLiveStreamUid2Key];
  NSString *liveStreamUid3 = [proteinSource parameterForKey:kCanonicalParameterLiveStreamUid3Key];
  
  
  {
    NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameLiveStreamsDetail responseType:@"psa?3-active"];
    OBProtein *p = [proteinSource proteinForKey:key];
    [websocketPoolConnector receivedProtein:p];
  }
  
  XCTAssertTrue(workspace.liveStreams.count == 3, @"There should be %d live streams, but found %lu", 3, (unsigned long)(workspace.liveStreams.count));
  if (workspace.liveStreams.count == 3)
  {
    MZLiveStream *liveStream = workspace.liveStreams[0];
    XCTAssertEqualObjects(liveStream.uid, liveStreamUid1, @"Should be %@ but is %@", liveStreamUid1, liveStream.uid);
    XCTAssertTrue(liveStream.active, @"Should be active");
    
    liveStream = workspace.liveStreams[1];
    XCTAssertEqualObjects(liveStream.uid, liveStreamUid2, @"Should be %@ but is %@", liveStreamUid2, liveStream.uid);
    XCTAssertTrue(liveStream.active, @"Should be active");
    
    liveStream = workspace.liveStreams[2];
    XCTAssertEqualObjects(liveStream.uid, liveStreamUid3, @"Should be %@ but is %@", liveStreamUid3, liveStream.uid);
    XCTAssertTrue(liveStream.active, @"Should be active");
  }
  
  
  {
    NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameLiveStreamsDetail responseType:@"psa?1-active"];
    OBProtein *p = [proteinSource proteinForKey:key];
    [websocketPoolConnector receivedProtein:p];
  }
  
  // The following assertion is removed because currently we rely on MZLiveStream objects to not discard
  // objects because remote live streams depend on having its MZLiveStream representation present in the
  // liveStreams list. Therefore, the testing of removal of MZLiveStream objects below needs to be removed.
  /*
   STAssertTrue(workspace.liveStreams.count == 1, @"There should be %ld live stream, but found %ld", 1, workspace.liveStreams.count);
   if (workspace.liveStreams.count > 0)
   {
   MZLiveStream *liveStream = workspace.liveStreams[0];
   STAssertEqualObjects(liveStream.uid, liveStreamUid1, @"Should be %@ but is %@", liveStreamUid1, liveStream.uid);
   STAssertTrue(liveStream.active, @"Should be active");
   }
   */
}


- (void)testLiveStreamsDetailFromThreeToTwoActive
{
  [self setUpConnectedCommunicator];
  MZWorkspace *workspace = [self prepareEmptyWorkspace];
  
  NSString *liveStreamUid1 = [proteinSource parameterForKey:kCanonicalParameterLiveStreamUid1Key];
  NSString *liveStreamUid2 = [proteinSource parameterForKey:kCanonicalParameterLiveStreamUid2Key];
  NSString *liveStreamUid3 = [proteinSource parameterForKey:kCanonicalParameterLiveStreamUid3Key];
  
  
  {
    NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameLiveStreamsDetail responseType:@"psa?3-active"];
    OBProtein *p = [proteinSource proteinForKey:key];
    [websocketPoolConnector receivedProtein:p];
  }
  
  XCTAssertTrue(workspace.liveStreams.count == 3, @"There should be %d live stream, but found %lu", 3, (unsigned long)workspace.liveStreams.count);
  if (workspace.liveStreams.count > 0)
  {
    MZLiveStream *liveStream = workspace.liveStreams[0];
    XCTAssertEqualObjects(liveStream.uid, liveStreamUid1, @"Should be %@ but is %@", liveStreamUid1, liveStream.uid);
    XCTAssertTrue(liveStream.active, @"Should be active");
    
    liveStream = workspace.liveStreams[1];
    XCTAssertEqualObjects(liveStream.uid, liveStreamUid2, @"Should be %@ but is %@", liveStreamUid2, liveStream.uid);
    XCTAssertTrue(liveStream.active, @"Should be active");
    
    liveStream = workspace.liveStreams[2];
    XCTAssertEqualObjects(liveStream.uid, liveStreamUid3, @"Should be %@ but is %@", liveStreamUid3, liveStream.uid);
    XCTAssertTrue(liveStream.active, @"Should be active");
  }
  
  
  {
    NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameLiveStreamsDetail responseType:@"psa?2-active-1-inactive"];
    OBProtein *p = [proteinSource proteinForKey:key];
    [websocketPoolConnector receivedProtein:p];
  }
  
  XCTAssertTrue(workspace.liveStreams.count == 3, @"There should be %d live streams, but found %lu", 3, (unsigned long)workspace.liveStreams.count);
  if (workspace.liveStreams.count == 3)
  {
    MZLiveStream *liveStream = workspace.liveStreams[0];
    XCTAssertEqualObjects(liveStream.uid, liveStreamUid1, @"Should be %@ but is %@", liveStreamUid1, liveStream.uid);
    XCTAssertTrue(liveStream.active, @"Should be active");
    
    liveStream = workspace.liveStreams[1];
    XCTAssertEqualObjects(liveStream.uid, liveStreamUid2, @"Should be %@ but is %@", liveStreamUid2, liveStream.uid);
    XCTAssertTrue(liveStream.active, @"Should be active");
    
    liveStream = workspace.liveStreams[2];
    XCTAssertEqualObjects(liveStream.uid, liveStreamUid3, @"Should be %@ but is %@", liveStreamUid3, liveStream.uid);
    XCTAssertTrue(!liveStream.active, @"Should be inactive");
  }
}


#pragma mark - Porfolio Tests

- (MZPresentation*)preparePresentationWithItems:(NSInteger)numberOfItems
{
  MZWorkspace *workspace = [self prepareEmptyWorkspace];
  MZPresentation *presentation = workspace.presentation;
  
  for (NSInteger i=0; i<numberOfItems; i++)
  {
    MZSlide *item = [[MZSlide alloc] init];
    item.uid = [NSString stringWithFormat:@"sl-000%ld", (long)i];
    item.contentSource = [NSString stringWithFormat:@"as-000%ld", (long)i];
    item.index = i;
    [presentation.slides addObject:item];
  }
  
  return presentation;
}


- (void)checkPresentationSlideIndex:(MZPresentation*)presentation
{
  NSInteger index = 0;
  for (MZSlide *slide in presentation.slides)
  {
    XCTAssertTrue(slide.index == index, @"Slide index should be %ld but is %ld", (long)index, (long)slide.index);
    index++;
  }
}


- (void)testPorfolioItemInsert_Front
{
  [self setUpConnectedCommunicator];
  MZPresentation *presentation = [self preparePresentationWithItems:5];
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNamePortfolioItemInsert responseType:@"psa?index-0"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  
  XCTAssertTrue((presentation.slides.count == 6), @"Slides count should be %d but is %ld", 6, (long)presentation.slides.count);
  
  NSString *slideUid = [proteinSource parameterForKey:kCanonicalParameterSlideInsertionUidKey];
  XCTAssertEqualObjects(((MZSlide*)presentation.slides[0]).uid, slideUid, @"Inserted slide uid should match");
  NSString *assetUid = [proteinSource parameterForKey:kCanonicalParameterAssetUidKey];
  XCTAssertEqualObjects(((MZSlide*)presentation.slides[0]).contentSource, assetUid, @"Inserted slide's contentSource should match");
  
  [self checkPresentationSlideIndex:presentation];
}


- (void)testPorfolioItemInsert_Middle
{
  [self setUpConnectedCommunicator];
  MZPresentation *presentation = [self preparePresentationWithItems:5];
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNamePortfolioItemInsert responseType:@"psa?index-2"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  
  XCTAssertTrue((presentation.slides.count == 6), @"Slides count should be %d but is %lu", 6, (unsigned long)presentation.slides.count);
  
  NSString *slideUid = [proteinSource parameterForKey:kCanonicalParameterSlideInsertionUidKey];
  XCTAssertEqualObjects(((MZSlide*)presentation.slides[2]).uid, slideUid, @"Inserted slide uid should match");
  NSString *assetUid = [proteinSource parameterForKey:kCanonicalParameterAssetUidKey];
  XCTAssertEqualObjects(((MZSlide*)presentation.slides[2]).contentSource, assetUid, @"Inserted slide's contentSource should match");
  
  [self checkPresentationSlideIndex:presentation];
}


- (void)testPorfolioItemInsert_End
{
  [self setUpConnectedCommunicator];
  MZPresentation *presentation = [self preparePresentationWithItems:5];
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNamePortfolioItemInsert responseType:@"psa?index-4"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  
  XCTAssertTrue((presentation.slides.count == 6), @"Slides count should be %d but is %lu", 6, (unsigned long)presentation.slides.count);
  
  NSString *slideUid = [proteinSource parameterForKey:kCanonicalParameterSlideInsertionUidKey];
  XCTAssertEqualObjects(((MZSlide*)presentation.slides[4]).uid, slideUid, @"Inserted slide uid should match");
  NSString *assetUid = [proteinSource parameterForKey:kCanonicalParameterAssetUidKey];
  XCTAssertEqualObjects(((MZSlide*)presentation.slides[4]).contentSource, assetUid, @"Inserted slide's contentSource should match");
  
  [self checkPresentationSlideIndex:presentation];
}


- (void)testPorfolioItemDelete
{
  [self setUpConnectedCommunicator];
  MZPresentation *presentation = [self preparePresentationWithItems:5];
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNamePortfolioItemDelete responseType:@"psa"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  
  XCTAssertTrue((presentation.slides.count == 4), @"Slides count should be %d but is %lu", 4, (unsigned long)presentation.slides.count);
  
  NSString *slideUid = [proteinSource parameterForKey:kCanonicalParameterSlideDeletionUidKey];
  XCTAssertNil([presentation slideWithUid:slideUid], @"Deleted slide should no longer exist in presentation");
  
  [self checkPresentationSlideIndex:presentation];
}


- (void)testPorfolioItemReorder_To0
{
  [self setUpConnectedCommunicator];
  MZPresentation *presentation = [self preparePresentationWithItems:5];
  
  NSString *slideUid = [proteinSource parameterForKey:kCanonicalParameterSlideReorderUidKey];
  MZPortfolioItem *slide = [presentation slideWithUid:slideUid];
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNamePortfolioItemReorder responseType:@"psa?index-0"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertTrue((presentation.slides.count == 5), @"Slides count should be %d but is %lu", 5, (unsigned long)presentation.slides.count);
  
  XCTAssertTrue((slide.index == 0), @"Slide index should be %d but is %ld", 0, (long)slide.index);
  
  [self checkPresentationSlideIndex:presentation];
}


- (void)testPorfolioItemReorder_To4
{
  [self setUpConnectedCommunicator];
  MZPresentation *presentation = [self preparePresentationWithItems:5];
  
  NSString *slideUid = [proteinSource parameterForKey:kCanonicalParameterSlideReorderUidKey];
  MZPortfolioItem *slide = [presentation slideWithUid:slideUid];
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNamePortfolioItemReorder responseType:@"psa?index-4"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertTrue((presentation.slides.count == 5), @"Slides count should be %d but is %ld", 5, (long)presentation.slides.count);
  
  XCTAssertTrue((slide.index == 4), @"Slide index should be %d but is %ld", 4, (long)slide.index);
  
  [self checkPresentationSlideIndex:presentation];
}


- (void)testPorfolioClear
{
  [self setUpConnectedCommunicator];
  MZPresentation *presentation = [self preparePresentationWithItems:5];
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNamePortfolioClear responseType:@"psa"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertTrue((presentation.slides.count == 0), @"Slides count should be %d but is %ld", 0, (long)presentation.slides.count);
  
  [self checkPresentationSlideIndex:presentation];
}

- (void)testPorfolioDownloadSimpleURL
{
  [self setUpConnectedCommunicator];
  MZExportInfo *exportInfo = systemModel.portfolioExport.info;
  exportInfo.state = MZExportStateRequested;

  NSString *workspaceUid = [proteinSource parameterForKey:kCanonicalParameterWorkspaceUidKey];
  NSString *provenance = [proteinSource parameterForKey:kCanonicalParameterProvenanceKey];
  NSDictionary *ingests = @{ @"workspace-uid" : workspaceUid,
                             @"download-url" : @"/r/archive/ds-123/portfolio/workspace"};
  OBProtein *p = [OBProtein proteinWithDescrips:@[MZTransactionNamePortfolioDownload,
                                                  @"response",
                                                  @"to:",
                                                  @[provenance, @(1)]]
                                        ingests:ingests];

  [websocketPoolConnector receivedProtein:p];

  NSString *expectedString = @"https://test/r/archive/ds-123/portfolio/workspace";
  XCTAssertTrue([exportInfo.url.absoluteString isEqualToString:expectedString], @"URL should be %@ but it is %@", expectedString, exportInfo.url.absoluteString);
}

- (void)testPorfolioDownloadURLWithSpaces
{
  [self setUpConnectedCommunicator];
  MZExportInfo *exportInfo = systemModel.portfolioExport.info;
  exportInfo.state = MZExportStateRequested;

  NSString *workspaceUid = [proteinSource parameterForKey:kCanonicalParameterWorkspaceUidKey];
  NSString *provenance = [proteinSource parameterForKey:kCanonicalParameterProvenanceKey];
  NSDictionary *ingests = @{ @"workspace-uid" : workspaceUid,
                             @"download-url" : @"/r/archive/ds-123/portfolio/your very cool   spaced workspace"};
  OBProtein *p = [OBProtein proteinWithDescrips:@[MZTransactionNamePortfolioDownload,
                                                  @"response",
                                                  @"to:",
                                                  @[provenance, @(1)]]
                                        ingests:ingests];

  [websocketPoolConnector receivedProtein:p];

  NSString *expectedString = @"https://test/r/archive/ds-123/portfolio/your%20very%20cool%20%20%20spaced%20workspace";
  XCTAssertTrue([exportInfo.url.absoluteString isEqualToString:expectedString], @"URL should be %@ but it is %@", expectedString, exportInfo.url.absoluteString);
}


- (void)testPorfolioDownloadURLWithManySymbols
{
  [self setUpConnectedCommunicator];
  MZExportInfo *exportInfo = systemModel.portfolioExport.info;
  exportInfo.state = MZExportStateRequested;

  NSString *workspaceUid = [proteinSource parameterForKey:kCanonicalParameterWorkspaceUidKey];
  NSString *provenance = [proteinSource parameterForKey:kCanonicalParameterProvenanceKey];
  NSDictionary *ingests = @{ @"workspace-uid" : workspaceUid,
                             @"download-url" : @"/r/archive/ds-123/portfolio/simbols-like-%-$-&-@-;-<-=->-[-?-+-,-and-emojis-😀-❤️"};
  OBProtein *p = [OBProtein proteinWithDescrips:@[MZTransactionNamePortfolioDownload,
                                                  @"response",
                                                  @"to:",
                                                  @[provenance, @(1)]]
                                        ingests:ingests];

  [websocketPoolConnector receivedProtein:p];

  NSString *expectedString = @"https://test/r/archive/ds-123/portfolio/simbols-like-%25-$-&-@-;-%3C-=-%3E-%5B-?-+-,-and-emojis-%F0%9F%98%80-%E2%9D%A4%EF%B8%8F";
  XCTAssertTrue([exportInfo.url.absoluteString isEqualToString:expectedString], @"URL should be %@ but it is %@", expectedString, exportInfo.url.absoluteString);
}


#pragma mark - Presentation Tests

- (void)testPresentationDetail
{
  [self setUpConnectedCommunicator];
  [self prepareEmptyWorkspace];
  MZPresentation *presentation = [self preparePresentationWithItems:5];
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNamePresentationDetail responseType:@"psa"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertTrue(presentation.active, @"Presentation should be active");
  
  NSInteger presentationScrollIndex = [[proteinSource parameterForKey:kCanonicalParameterPresentationScrollIndexKey] integerValue];
  XCTAssertTrue((presentation.currentSlideIndex == presentationScrollIndex), @"Presentation current slide index should be %ld but is %ld", (long)presentationScrollIndex, (long)presentation.currentSlideIndex);
}

/*
 - (void)testPresentationDetail_WrongWorkspace
 {
 [self setUpConnectedCommunicator];
 [self prepareEmptyWorkspace];
 MZPresentation *presentation = [self preparePresentationWithItems:5];
 
 NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNamePresentationDetail responseType:@"psa?wrong-workspace"];
 OBProtein *p = [proteinSource proteinForKey:key];
 [websocketPoolConnector receivedProtein:p];
 
 
 STAssertFalse(presentation.active, @"Presentation should not be active");
 STAssertTrue((presentation.currentSlideIndex == 0), @"Presentation current slide index should be %ld but is %ld", 0, presentation.currentSlideIndex);
 }
 */

- (void)testPresentationStart
{
  [self setUpConnectedCommunicator];
  [self prepareEmptyWorkspace];
  MZPresentation *presentation = [self preparePresentationWithItems:5];
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNamePresentationStart responseType:@"psa"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertTrue(presentation.active, @"Presentation should be active");
  
  NSInteger presentationScrollIndex = [[proteinSource parameterForKey:kCanonicalParameterPresentationScrollIndexKey] integerValue];
  XCTAssertTrue((presentation.currentSlideIndex == presentationScrollIndex), @"Presentation current slide index should be %ld but is %ld", (long)presentationScrollIndex, (long)presentation.currentSlideIndex);
}

/*
 - (void)testPresentationStart_WrongWorkspace
 {
 [self setUpConnectedCommunicator];
 [self prepareEmptyWorkspace];
 MZPresentation *presentation = [self preparePresentationWithItems:5];
 
 NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNamePresentationStart responseType:@"psa?wrong-workspace"];
 OBProtein *p = [proteinSource proteinForKey:key];
 [websocketPoolConnector receivedProtein:p];
 
 STAssertFalse(presentation.active, @"Presentation should not be active");
 }
 */

- (void)testPresentationStop
{
  [self setUpConnectedCommunicator];
  [self prepareEmptyWorkspace];
  MZPresentation *presentation = [self preparePresentationWithItems:5];
  presentation.active = YES;  // Start with presentation active
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNamePresentationStop responseType:@"psa"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertFalse(presentation.active, @"Presentation should not be active");
}

/*
 - (void)testPresentationStop_WrongWorkspace
 {
 [self setUpConnectedCommunicator];
 [self prepareEmptyWorkspace];
 MZPresentation *presentation = [self preparePresentationWithItems:5];
 presentation.active = YES;  // Start with presentation active
 
 NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNamePresentationStop responseType:@"psa?wrong-workspace"];
 OBProtein *p = [proteinSource proteinForKey:key];
 [websocketPoolConnector receivedProtein:p];
 
 STAssertTrue(presentation.active, @"Presentation should be active");
 }
 */

- (void)testPresentationScroll
{
  [self setUpConnectedCommunicator];
  [self prepareEmptyWorkspace];
  MZPresentation *presentation = [self preparePresentationWithItems:5];
  NSInteger initialScrollIndex = 1;
  presentation.currentSlideIndex = initialScrollIndex;
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNamePresentationScroll responseType:@"psa"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  NSInteger presentationScrollIndex = [[proteinSource parameterForKey:kCanonicalParameterPresentationScrollIndexKey] integerValue];
  XCTAssertTrue((presentation.currentSlideIndex == presentationScrollIndex), @"Presentation current slide index should be %ld but is %ld", (long)presentationScrollIndex, (long)presentation.currentSlideIndex);
}

/*
 - (void)testPresentationScroll_WrongWorkspace
 {
 [self setUpConnectedCommunicator];
 [self prepareEmptyWorkspace];
 MZPresentation *presentation = [self preparePresentationWithItems:5];
 NSInteger initialScrollIndex = 1;
 presentation.currentSlideIndex = initialScrollIndex;
 
 NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNamePresentationScroll responseType:@"psa?wrong-workspace"];
 OBProtein *p = [proteinSource proteinForKey:key];
 [websocketPoolConnector receivedProtein:p];
 
 
 NSInteger presentationScrollIndex = [proteinSource parameterForKey:kCanonicalParameterPresentationScrollIndexKey];
 STAssertTrue((presentation.currentSlideIndex != presentationScrollIndex), @"Presentation current slide index should remain the same at %ld but is %ld", presentationScrollIndex, presentation.currentSlideIndex);
 }
 */


#pragma mark - Windshield Tests

-(MZWindshield*) prepareWorkspaceForWindshieldTests:(NSInteger)initialNumberOfItems
{
  MZWorkspace *workspace = [self prepareEmptyWorkspace];
  
  // Add some initial windshield items
  MZWindshield *windshield = workspace.windshield;
  for (NSInteger i=0; i<initialNumberOfItems; i++)
  {
    MZWindshieldItem *item = [[MZWindshieldItem alloc] init];
    item.uid = [NSString stringWithFormat:@"el-000%ld", (long)i];
    item.assetUid = [NSString stringWithFormat:@"as-000%ld", (long)i];
    item.frame = CGRectMake(0, 0, 1, 1);
    item.surface = [[MZSurface alloc] initWithName:@"main"];
    [windshield insertObject:item inItemsAtIndex:windshield.countOfItems];
  }
  
  return windshield;
}


// For now not separating test from protein injection in the following tests
// to save time. But in the future the idea is that one can run the same tests
// and set up a different version of the communicator.

-(void) testWindshieldDetailFromEmpty
{
  [self setUpConnectedCommunicator];
  
  MZWindshield *windshield = [self prepareWorkspaceForWindshieldTests:0];
  XCTAssertTrue(windshield.items.count == 0, @"Number of items before windshield details should be zero");
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameWindshieldDetail responseType:@"psa"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  NSInteger numberOfItems = 8;
  XCTAssertTrue(windshield.items.count == numberOfItems, @"Number of items after windshield details should be %ld", (long)numberOfItems);
}


-(void) testWindshieldDetailFromLessThanEight
{
  [self setUpConnectedCommunicator];
  
  MZWindshield *windshield = [self prepareWorkspaceForWindshieldTests:4];
  XCTAssertTrue(windshield.items.count == 4, @"Number of items before windshield details should be 4");
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameWindshieldDetail responseType:@"psa"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  NSInteger numberOfItems = 8;
  XCTAssertTrue(windshield.items.count == numberOfItems, @"Number of items after windshield details should be %ld", (long)numberOfItems);
}


-(void) testWindshieldDetailFromMoreThanEight
{
  [self setUpConnectedCommunicator];
  
  MZWindshield *windshield = [self prepareWorkspaceForWindshieldTests:11];
  XCTAssertTrue(windshield.items.count == 11, @"Number of items before windshield details should be 8");
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameWindshieldDetail responseType:@"psa"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  NSInteger numberOfItems = 8;
  XCTAssertTrue(windshield.items.count == numberOfItems, @"Number of items after windshield details should be %ld", (long)numberOfItems);
}


-(void) testWindshieldClearSingle
{
  // Initializing the communicator using a join protein is necessary here because the windshield-clear response handler expects a list of surfaces to be defined.
  [self setUpConnectedCommunicator_usingJoinProtein];
  
  NSInteger numberOfItems = 1;
  MZWindshield *windshield = [self prepareWorkspaceForWindshieldTests:numberOfItems];
  XCTAssertTrue(windshield.items.count == numberOfItems, @"Number of items should be %ld", (long)numberOfItems);
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameWindshieldClear responseType:@"psa"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertTrue(windshield.items.count == 0, @"Number of items after clear should be zero");
}


-(void) testWindshieldClearMultiple
{
  // Initializing the communicator using a join protein is necessary here because the windshield-clear response handler expects a list of surfaces to be defined.
  [self setUpConnectedCommunicator_usingJoinProtein];
  
  NSInteger numberOfItems = 8;
  MZWindshield *windshield = [self prepareWorkspaceForWindshieldTests:numberOfItems];
  XCTAssertTrue(windshield.items.count == numberOfItems, @"Number of items should be %ld", (long)numberOfItems);
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameWindshieldClear responseType:@"psa"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertTrue(windshield.items.count == 0, @"Number of items after clear should be zero");
}


-(void) testWindshieldCreate
{
  [self setUpConnectedCommunicator];
  
  NSInteger numberOfItems = 8;
  MZWindshield *windshield = [self prepareWorkspaceForWindshieldTests:numberOfItems];
  XCTAssertTrue(windshield.items.count == numberOfItems, @"Number of items should be %ld", (long)numberOfItems);
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameWindshieldItemCreate responseType:@"psa"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertTrue(windshield.items.count == (numberOfItems+1), @"Number of items after create should be %ld", (long)numberOfItems+1);
}


-(void) testWindshieldDelete
{
  [self setUpConnectedCommunicator];
  
  NSInteger numberOfItems = 8;
  MZWindshield *windshield = [self prepareWorkspaceForWindshieldTests:numberOfItems];
  XCTAssertTrue(windshield.items.count == numberOfItems, @"Number of items should be %ld", (long)numberOfItems);
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameWindshieldItemDelete responseType:@"psa"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertTrue(windshield.items.count == (numberOfItems-1), @"Number of items after create should be %ld", (long)numberOfItems-1);
}


-(void) testWindshieldTransform
{
  [self setUpConnectedCommunicator];
  
  // TODO
  /*
   MZWindshield *windshield = [self prepareWorkspaceForWindshieldTests:3];
   
   NSNumber* windshieldItemAspectRatio = [proteinSource parameterForKey:kCanonicalParameterWindshieldItemAspectRatioKey];
   NSString* windshieldItemContentSource = [proteinSource parameterForKey:kCanonicalParameterWindshieldItemContentSourceKey];
   NSString* windshieldItemFeldId = [proteinSource parameterForKey:kCanonicalParameterWindshieldItemFeldIdKey];
   OBVect2f64* windshieldItemFeldRelativeCoords = [proteinSource parameterForKey:kCanonicalParameterWindshieldItemFeldRelativeCoordsKey];
   OBVect2f64* windshieldItemFeldRelativeSize = [proteinSource parameterForKey:kCanonicalParameterWindshieldItemFeldRelativeSizeKey];
   */
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameWindshieldItemTransform responseType:@"psa"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
}



#pragma mark - Workspace Tests

-(void) testWorkspaceCreateProtein
{
  [self setUpConnectedCommunicator];
  [self performWorkspaceCreateTest];
}


-(void) performWorkspaceCreateTest
{
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameWorkspaceCreate responseType:@"psa"];
  
  NSString *workspaceUid = [proteinSource parameterForKey:kCanonicalParameterWorkspaceUidKey];
  XCTAssertNotNil(workspaceUid, @"workspaceUid should not be nil");
  NSString *workspaceName = [proteinSource parameterForKey:kCanonicalParameterWorkspaceNameKey];
  XCTAssertNotNil(workspaceName, @"workspaceName should not be nil");
  
  MZWorkspace *existingWorkspace = [systemModel workspaceWithUid:workspaceUid];
  XCTAssertNil(existingWorkspace, @"Existing workspace with uid %@ should be nil", workspaceUid);
  
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  MZWorkspace *newWorkspace = [systemModel workspaceWithUid:workspaceUid];
  XCTAssertEqualObjects(newWorkspace.uid, workspaceUid, @"Workspace uid should be %@", workspaceUid);
  XCTAssertEqualObjects(newWorkspace.name, workspaceName, @"Workspace name should be %@", workspaceName);
  
  XCTAssertFalse([systemModel.currentWorkspace.uid isEqual:workspaceUid], @"Workspace creation should not set current workspace to the newly created workspace.");
}


- (void)testWorkspaceCloseProtein
{
  [self setUpConnectedCommunicator];
  [self performWorkspaceCloseTest];
}


- (void)performWorkspaceCloseTest
{
  NSString *oldWorkspaceUid = [proteinSource parameterForKey:kCanonicalParameterWorkspaceOldUidKey];
  NSString *workspaceUid = [proteinSource parameterForKey:kCanonicalParameterWorkspaceUidKey];
  
  MZWorkspace *workspace = [[MZWorkspace alloc] init];
  workspace.uid = oldWorkspaceUid;
  [systemModel insertObject:workspace inWorkspacesAtIndex:systemModel.countOfWorkspaces];
  systemModel.currentWorkspace = workspace;
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameWorkspaceClose responseType:@"psa"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  MZWorkspace *currentWorkspace = systemModel.currentWorkspace;
  XCTAssertFalse([currentWorkspace.uid isEqual:oldWorkspaceUid], @"Workspace uid should not be %@ after closing", oldWorkspaceUid);
  XCTAssertEqualObjects(currentWorkspace.uid, workspaceUid, @"Workspace uid should be %@ after closing", workspaceUid);
  XCTAssertNil(currentWorkspace.name, @"Workspace name should be nil after closing");
  XCTAssertFalse(currentWorkspace.isSaved, @"Workspace as a result of closing should be unsaved.");
}


- (void)testWorkspaceOpenProtein
{
  [self setUpConnectedCommunicator];
  [self performWorkspaceOpenTest];
}


- (void)performWorkspaceOpenTest
{
  NSString *oldWorkspaceUid = [proteinSource parameterForKey:kCanonicalParameterWorkspaceOldUidKey];
  NSString *workspaceUid = [proteinSource parameterForKey:kCanonicalParameterWorkspaceUidKey];
  NSString *workspaceName = [proteinSource parameterForKey:kCanonicalParameterWorkspaceNameKey];
  
  MZWorkspace *workspace = [[MZWorkspace alloc] init];
  workspace.uid = oldWorkspaceUid;
  [systemModel insertObject:workspace inWorkspacesAtIndex:systemModel.countOfWorkspaces];
  systemModel.currentWorkspace = workspace;
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameWorkspaceOpen responseType:@"psa"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  MZWorkspace *currentWorkspace = systemModel.currentWorkspace;
  XCTAssertFalse([currentWorkspace.uid isEqual:oldWorkspaceUid], @"Workspace uid should not be %@ after opening.", oldWorkspaceUid);
  XCTAssertEqualObjects(currentWorkspace.uid, workspaceUid, @"Workspace uid should be %@ after opening.", workspaceUid);
  XCTAssertEqualObjects(currentWorkspace.name, workspaceName, @"Workspace name should be %@ after opening.", workspaceName);
  XCTAssertTrue(currentWorkspace.isSaved, @"Workspace as a result of opening should be saved.");
}


- (void)testWorkspaceDiscardProtein
{
  [self setUpConnectedCommunicator];
  [self performWorkspaceDiscardTest];
}


- (void)performWorkspaceDiscardTest
{
  NSString *oldWorkspaceUid = [proteinSource parameterForKey:kCanonicalParameterWorkspaceOldUidKey];
  NSString *workspaceUid = [proteinSource parameterForKey:kCanonicalParameterWorkspaceUidKey];
  //  NSString *workspaceName = [proteinSource parameterForKey:kCanonicalParameterWorkspaceNameKey];
  
  MZWorkspace *workspace = [[MZWorkspace alloc] init];
  workspace.uid = oldWorkspaceUid;
  [systemModel insertObject:workspace inWorkspacesAtIndex:systemModel.countOfWorkspaces];
  systemModel.currentWorkspace = workspace;
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameWorkspaceDiscard responseType:@"psa"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  MZWorkspace *currentWorkspace = systemModel.currentWorkspace;
  XCTAssertFalse([currentWorkspace.uid isEqual:oldWorkspaceUid], @"Workspace uid should not be %@ after discarding.", workspaceUid);
  XCTAssertFalse(currentWorkspace.isSaved, @"Workspace as a result of discarding should be unsaved.");
}


- (void)testWorkspaceSaveProtein
{
  [self setUpConnectedCommunicator];
  [self performWorkspaceSaveTest];
}


- (void)performWorkspaceSaveTest
{
  NSString *workspaceUid = [proteinSource parameterForKey:kCanonicalParameterWorkspaceUidKey];
  NSString *workspaceName = [proteinSource parameterForKey:kCanonicalParameterWorkspaceNameKey];
  
  MZWorkspace *workspace = [[MZWorkspace alloc] init];
  workspace.uid = workspaceUid;
  workspace.name = [NSString stringWithFormat:@"%@ (before save)", workspaceName];
  workspace.isSaved = NO;
  [systemModel insertObject:workspace inWorkspacesAtIndex:systemModel.countOfWorkspaces];
  systemModel.currentWorkspace = workspace;
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameWorkspaceSave responseType:@"psa"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  MZWorkspace *currentWorkspace = systemModel.currentWorkspace;
  XCTAssertTrue([currentWorkspace.uid isEqual:workspaceUid], @"Workspace uid should be equal (%@) after saving but got %@", workspaceUid, currentWorkspace.uid);
  XCTAssertEqualObjects(currentWorkspace.name, workspaceName, @"Workspace name should be %@ after saving, but is %@.", workspaceName, currentWorkspace.name);
  XCTAssertTrue(currentWorkspace.isSaved, @"Workspace should be saved after saving.");
}


- (void)testWorkspaceDeleteProtein
{
  [self setUpConnectedCommunicator];
  [self performWorkspaceDeleteTest];
}


- (void)performWorkspaceDeleteTest
{
  NSString *workspaceUid = [proteinSource parameterForKey:kCanonicalParameterWorkspaceUidKey];
  NSString *workspaceName = [proteinSource parameterForKey:kCanonicalParameterWorkspaceNameKey];
  
  MZWorkspace *workspace = [[MZWorkspace alloc] init];
  workspace.uid = workspaceUid;
  workspace.name = workspaceName;
  workspace.isSaved = YES;
  [systemModel insertObject:workspace inWorkspacesAtIndex:systemModel.countOfWorkspaces];
  
  XCTAssertNotNil([systemModel workspaceWithUid:workspaceUid], @"Workspace should be present.");
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameWorkspaceDelete responseType:@"psa"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertNil([systemModel workspaceWithUid:workspaceUid], @"Workspace should have been deleted.");
}


- (void)testWorkspaceRenameProtein
{
  [self setUpConnectedCommunicator];
  [self performWorkspaceRenameTest];
}


- (void)performWorkspaceRenameTest
{
  NSString *workspaceUid = [proteinSource parameterForKey:kCanonicalParameterWorkspaceUidKey];
  NSString *workspaceName = [proteinSource parameterForKey:kCanonicalParameterWorkspaceNameKey];
  
  MZWorkspace *workspace = [[MZWorkspace alloc] init];
  workspace.uid = workspaceUid;
  workspace.name = @"Something random";
  workspace.isSaved = YES;
  [systemModel insertObject:workspace inWorkspacesAtIndex:systemModel.countOfWorkspaces];
  
  XCTAssertNotNil([systemModel workspaceWithUid:workspaceUid], @"Workspace should be present.");
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameWorkspaceRename responseType:@"psa"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  MZWorkspace *workspaceAfter = [systemModel workspaceWithUid:workspaceUid];
  XCTAssertNotNil(workspaceAfter, @"Workspace should be present.");
  XCTAssertEqualObjects(workspaceAfter.name, workspaceName, @"Workspace name should be %@ after rename.", workspaceName);
}


- (void)testWorkspaceDuplicateProtein
{
  [self setUpConnectedCommunicator];
  [self performWorkspaceDuplicateTest];
}


- (void)performWorkspaceDuplicateTest
{
  NSString *oldWorkspaceUid = [proteinSource parameterForKey:kCanonicalParameterWorkspaceOldUidKey];
  NSString *workspaceUid = [proteinSource parameterForKey:kCanonicalParameterWorkspaceUidKey];
  NSString *workspaceName = [proteinSource parameterForKey:kCanonicalParameterWorkspaceNameKey];
  
  MZWorkspace *workspace = [[MZWorkspace alloc] init];
  workspace.uid = oldWorkspaceUid;
  workspace.name = [NSString stringWithFormat:@"%@ before duplicate", workspaceName];
  workspace.isSaved = YES;
  [systemModel insertObject:workspace inWorkspacesAtIndex:systemModel.countOfWorkspaces];
  
  XCTAssertNotNil([systemModel workspaceWithUid:oldWorkspaceUid], @"Workspace should be present before duplicate.");
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameWorkspaceDuplicate responseType:@"psa"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  MZWorkspace *workspaceAfter = [systemModel workspaceWithUid:workspaceUid];
  XCTAssertNotNil(workspaceAfter, @"Workspace should be present after duplicate.");
  XCTAssertEqualObjects(workspaceAfter.uid, workspaceUid, @"Workspace uid should be the same after duplicate.");
  XCTAssertEqualObjects(workspaceAfter.name, workspaceName, @"Workspace name should be '%@' after duplicate.", workspaceName);
}


- (void)testWorkspaceReassignProtein
{
  [self setUpConnectedCommunicator];
  [self performWorkspaceReassignTest];
}


- (void)performWorkspaceReassignTest
{
  NSString *oldWorkspaceUid = [proteinSource parameterForKey:kCanonicalParameterWorkspaceOldUidKey];
  NSString *workspaceUid = [proteinSource parameterForKey:kCanonicalParameterWorkspaceUidKey];
  
  MZWorkspace *workspace = [[MZWorkspace alloc] init];
  workspace.uid = oldWorkspaceUid;
  [systemModel insertObject:workspace inWorkspacesAtIndex:systemModel.countOfWorkspaces];
  
  XCTAssertTrue([systemModel workspaceWithUid:oldWorkspaceUid], @"Workspace with uid '%@' should exist before reassign.", oldWorkspaceUid);
  XCTAssertFalse([systemModel workspaceWithUid:workspaceUid], @"Workspace with uid '%@' should not exist before reassign.", workspaceUid);
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameWorkspaceReassign responseType:@"psa"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertFalse([systemModel workspaceWithUid:oldWorkspaceUid], @"Workspace with uid '%@' should not exist after reassign.", oldWorkspaceUid);
  XCTAssertTrue([systemModel workspaceWithUid:workspaceUid], @"Workspace with uid '%@' should exist after reassign.", workspaceUid);
}


- (void)testWorkspaceModifiedProtein
{
  [self setUpConnectedCommunicator];
  [self performWorkspaceModifiedTest];
}


- (void)performWorkspaceModifiedTest
{
  NSString *workspaceUid = [proteinSource parameterForKey:kCanonicalParameterWorkspaceUidKey];
  
  NSDate *initialModifiedDate = [NSDate dateWithTimeIntervalSinceNow:-3600]; // A little in the past, as mock protein just uses current timestamp
  MZWorkspace *workspace = [[MZWorkspace alloc] init];
  workspace.uid = workspaceUid;
  workspace.modifiedDate = initialModifiedDate;
  [systemModel insertObject:workspace inWorkspacesAtIndex:systemModel.countOfWorkspaces];
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameWorkspaceModified responseType:@"psa"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertTrue([workspace.modifiedDate timeIntervalSinceDate:initialModifiedDate] > 0, @"Workspace's new modified date %@ should be later than initial date %@", workspace.modifiedDate, initialModifiedDate);
  
  NSDate *modifiedDate = [proteinSource parameterForKey:kCanonicalParameterWorkspaceModifiedKey];
  XCTAssertEqual((NSUInteger)workspace.modifiedDate.timeIntervalSince1970, (NSUInteger)modifiedDate.timeIntervalSince1970, @"Modified dates should equal");
}


- (void)testFirstWorkspaceSwitchProtein
{
  [self setUpConnectedCommunicator];
  [self performFirstWorkspaceSwitchTest];
}


- (void)performFirstWorkspaceSwitchTest
{
  NSString *oldWorkspaceUid = [proteinSource parameterForKey:kCanonicalParameterWorkspaceOldUidKey];
  NSString *workspaceUid = [proteinSource parameterForKey:kCanonicalParameterWorkspaceUidKey];
  NSString *workspaceName = [proteinSource parameterForKey:kCanonicalParameterWorkspaceNameKey];
  
  MZWorkspace *workspace = [[MZWorkspace alloc] init];
  workspace.uid = oldWorkspaceUid;
  systemModel.currentWorkspace = workspace;
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameWorkspaceSwitch responseType:@"psa"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertEqualObjects(systemModel.currentWorkspace.uid, workspaceUid, @"Current workspace uid should be correct after switch.");
  XCTAssertEqualObjects(systemModel.currentWorkspace.name, workspaceName, @"Current workspace name should be '%@' after switch.", workspaceName);
  
  XCTAssertTrue([systemModel workspaceWithUid:workspaceUid] != nil, @"Workspace list should contain current workspace since it has content");
}


- (void)testSecondWorkspaceSwitchProtein
{
  [self setUpConnectedCommunicator];
  [self performSecondWorkspaceSwitchTest];
}


- (void)performSecondWorkspaceSwitchTest
{
  NSString *workspaceUid = [proteinSource parameterForKey:kCanonicalParameterWorkspaceUidKey];
  //  NSString *workspaceName = [proteinSource parameterForKey:kCanonicalParameterWorkspaceNameKey];
  NSString *previousWorkspaceUid = [proteinSource parameterForKey:kCanonicalParameterWorkspaceOldUidKey];
  
  MZWorkspace *workspace = [[MZWorkspace alloc] init];
  workspace.uid = workspaceUid;
  [systemModel insertObject:workspace inWorkspacesAtIndex:systemModel.countOfWorkspaces];
  systemModel.currentWorkspace = workspace;
  
  MZWorkspace *oldWorkspace = [[MZWorkspace alloc] init];
  oldWorkspace.uid = previousWorkspaceUid;
  [systemModel insertObject:oldWorkspace inWorkspacesAtIndex:systemModel.countOfWorkspaces];
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameWorkspaceSwitch responseType:@"psa"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertNotNil([systemModel workspaceWithUid:previousWorkspaceUid], @"Previous workspace should exist.");
  XCTAssertNotNil([systemModel workspaceWithUid:workspaceUid], @"Current workspace should exist.");
  
  XCTAssertTrue(![systemModel.currentWorkspace.uid isEqualToString:previousWorkspaceUid], @"Current workspace uid should have already been switched.");
  XCTAssertTrue([systemModel.currentWorkspace.uid isEqualToString:workspaceUid], @"Current workspace uid should be correct after switch.");
}


- (void)testWorkspaceSwitchToEmptyUnsavedWorkspace
{
  [self setUpConnectedCommunicator];
  [self performWorkspaceSwitchToEmptyUnsavedWorkspaceTest];
}


- (void)performWorkspaceSwitchToEmptyUnsavedWorkspaceTest
{
  NSString *oldWorkspaceUid = [proteinSource parameterForKey:kCanonicalParameterWorkspaceOldUidKey];
  NSString *workspaceUid = [proteinSource parameterForKey:kCanonicalParameterWorkspaceUidKey];
  
  MZWorkspace *workspace = [[MZWorkspace alloc] init];
  workspace.uid = oldWorkspaceUid;
  systemModel.currentWorkspace = workspace;
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameWorkspaceSwitch responseType:@"psa?to-empty-workspace"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertEqualObjects(systemModel.currentWorkspace.uid, workspaceUid, @"Current workspace uid should be correct after switch.");
  
  XCTAssertTrue([systemModel workspaceWithUid:workspaceUid] == nil, @"Workspace list should not contain current workspace since it is unsaved");
}

// Bug 17886. https://bugs.oblong.com/show_bug.cgi?id=17886
- (void)testWorkspaceSwitchToEmptyUnsavedWorkspaceNullUid
{
  [self setUpConnectedCommunicator];
  [self performWorkspaceSwitchToEmptyUnsavedWorkspaceTestNullUid];
}


- (void)performWorkspaceSwitchToEmptyUnsavedWorkspaceTestNullUid
{
  NSString *oldWorkspaceUid = [proteinSource parameterForKey:kCanonicalParameterWorkspaceOldUidKey];
  NSString *workspaceUid = [proteinSource parameterForKey:kCanonicalParameterWorkspaceUidKey];
  
  MZWorkspace *workspace = [[MZWorkspace alloc] init];
  workspace.uid = oldWorkspaceUid;
  systemModel.currentWorkspace = workspace;
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameWorkspaceSwitch responseType:@"psa?to-empty-workspace"];
  OBProtein *p = [proteinSource proteinForKey:key];
  
  // Set null the uid
  NSMutableDictionary *ingests = [NSMutableDictionary dictionaryWithDictionary:p.ingests];
  NSMutableDictionary *previousWorkspace = [NSMutableDictionary dictionaryWithDictionary:ingests[@"previous-workspace"]];
  [previousWorkspace setObject:[NSNull null] forKey:@"uid"];
  [ingests setObject:previousWorkspace forKey:@"previous-workspace"];
  p.ingests = ingests;
  
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertEqualObjects(systemModel.currentWorkspace.uid, workspaceUid, @"Current workspace uid should be correct after switch.");
  XCTAssertTrue([systemModel workspaceWithUid:workspaceUid] == nil, @"Workspace list should not contain current workspace since it is unsaved");
}



#pragma mark - Sig in Workspace

- (void)testSignInSucess
{
  [self setUpConnectedCommunicator];
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameClientSignIn responseType:@"client-sign-in-success"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertTrue(systemModel.isSignedIn);
  XCTAssertTrue([systemModel.currentUsername isEqualToString:@"username"]);
  XCTAssertFalse(systemModel.isCurrentUserSuperuser);
}

- (void)testSignInFail
{
  [self setUpConnectedCommunicator];
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameClientSignIn responseType:@"client-sign-in-fail"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertFalse(systemModel.isSignedIn);
  XCTAssertNil(systemModel.currentUsername);
  XCTAssertFalse(systemModel.isCurrentUserSuperuser);
}

- (void)testSignOut
{
  [self setUpConnectedCommunicator];
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameClientSignIn responseType:@"client-sign-in-fail"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  XCTAssertFalse(systemModel.toBeDisconnected);
  XCTAssertFalse(systemModel.isSignedIn);
  XCTAssertNil(systemModel.currentUsername);
  XCTAssertFalse(systemModel.isCurrentUserSuperuser);
}


#pragma mark - Workspace List

- (void)testPublicWorkspaceList
{
  [self setUpConnectedCommunicator];
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameWorkspaceList responseType:@"response?public-workspaces"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  NSArray *publicWorkspaces = [proteinSource parameterForKey:kCanonicalParameterPublicWorkspaceUidKey];
  
  XCTAssertTrue(systemModel.workspaces.count == publicWorkspaces.count, @"Number of workspaces should be %lu, but is %lu.", (unsigned long)publicWorkspaces.count, (unsigned long)systemModel.workspaces.count);
  for (NSString *workspaceUid in publicWorkspaces)
    XCTAssertNotNil([systemModel workspaceWithUid:workspaceUid], @"System model contains workspace");
}


- (void)testPublicAndPrivateWorkspaceList
{
  [self setUpConnectedCommunicator];
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameWorkspaceList responseType:@"response?public-and-private-workspaces"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  NSArray *publicWorkspaces = [proteinSource parameterForKey:kCanonicalParameterPublicWorkspaceUidKey];
  NSArray *privateWorkspaces = [proteinSource parameterForKey:kCanonicalParameterPrivateWorkspaceUidKey];
  NSString *workspaceOwner = [proteinSource parameterForKey:kCanonicalParameterWorkspaceOwnerKey];
  
  XCTAssertTrue(systemModel.workspaces.count == publicWorkspaces.count + privateWorkspaces.count, @"Number of workspaces should be %lu, but is %lu.", (unsigned long)(publicWorkspaces.count + privateWorkspaces.count), (unsigned long)systemModel.workspaces.count);
  for (NSString *workspaceUid in publicWorkspaces)
    XCTAssertNotNil([systemModel workspaceWithUid:workspaceUid], @"System model contains workspace");
  for (NSString *workspaceUid in privateWorkspaces)
  {
    XCTAssertNotNil([systemModel workspaceWithUid:workspaceUid], @"System model contains workspace");
    XCTAssertTrue([[[systemModel workspaceWithUid:workspaceUid] owner] isEqualToString:workspaceOwner], @"Private workspace has correct owner");
  }
}


- (void)testWorkspaceListAfterReceivingPublicThenPrivate
{
  [self setUpConnectedCommunicator];
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameWorkspaceList responseType:@"response?public-workspaces"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  NSString *keyPublicAndPrivate = [proteinSource proteinKeyForTransactionName:MZTransactionNameWorkspaceList responseType:@"response?public-and-private-workspaces"];
  OBProtein *pPublicAndPrivate = [proteinSource proteinForKey:keyPublicAndPrivate];
  [websocketPoolConnector receivedProtein:pPublicAndPrivate];
  
  NSArray *publicWorkspaces = [proteinSource parameterForKey:kCanonicalParameterPublicWorkspaceUidKey];
  NSArray *privateWorkspaces = [proteinSource parameterForKey:kCanonicalParameterPrivateWorkspaceUidKey];
  NSString *workspaceOwner = [proteinSource parameterForKey:kCanonicalParameterWorkspaceOwnerKey];
  
  XCTAssertTrue(systemModel.workspaces.count == publicWorkspaces.count + privateWorkspaces.count, @"Number of workspaces should be %lu, but is %lu.", (unsigned long)(publicWorkspaces.count + privateWorkspaces.count), (unsigned long)systemModel.workspaces.count);
  for (NSString *workspaceUid in publicWorkspaces)
    XCTAssertNotNil([systemModel workspaceWithUid:workspaceUid], @"System model contains workspace");
  for (NSString *workspaceUid in privateWorkspaces)
  {
    XCTAssertNotNil([systemModel workspaceWithUid:workspaceUid], @"System model contains workspace");
    XCTAssertTrue([[[systemModel workspaceWithUid:workspaceUid] owner] isEqualToString:workspaceOwner], @"Private workspace has correct owner");
  }
}


- (void)testWorkspaceListAfterReceivingPrivateThenPublic
{
  [self setUpConnectedCommunicator];
  
  NSString *keyPublicAndPrivate = [proteinSource proteinKeyForTransactionName:MZTransactionNameWorkspaceList responseType:@"response?public-and-private-workspaces"];
  OBProtein *pPublicAndPrivate = [proteinSource proteinForKey:keyPublicAndPrivate];
  [websocketPoolConnector receivedProtein:pPublicAndPrivate];
  
  NSString *key = [proteinSource proteinKeyForTransactionName:MZTransactionNameWorkspaceList responseType:@"response?public-workspaces"];
  OBProtein *p = [proteinSource proteinForKey:key];
  [websocketPoolConnector receivedProtein:p];
  
  NSArray *publicWorkspaces = [proteinSource parameterForKey:kCanonicalParameterPublicWorkspaceUidKey];
  NSArray *privateWorkspaces = [proteinSource parameterForKey:kCanonicalParameterPrivateWorkspaceUidKey];
  //  NSString *workspaceOwner = [proteinSource parameterForKey:kCanonicalParameterWorkspaceOwnerKey];
  
  XCTAssertTrue(systemModel.workspaces.count == publicWorkspaces.count, @"Number of workspaces should be %lu, but is %lu.", (unsigned long)publicWorkspaces.count, (unsigned long)systemModel.workspaces.count);
  for (NSString *workspaceUid in publicWorkspaces)
    XCTAssertNotNil([systemModel workspaceWithUid:workspaceUid], @"System model contains workspace");
  for (NSString *workspaceUid in privateWorkspaces)
  {
    XCTAssertNil([systemModel workspaceWithUid:workspaceUid], @"System model contains workspace");
  }
}


#pragma mark - MZAssetCache Tests

- (void)testAssetCacheStoreAsset
{
  [self setUpConnectedCommunicator];
  [MZAssetCache setSharedCache:[[MZAssetCache alloc] init]];
  
  NSString *assetUid = [proteinSource parameterForKey:kCanonicalParameterAssetUidKey];
  MZAsset *asset = [MZAsset assetWithUid:assetUid];
  MZAssetCache *cache = [MZAssetCache sharedCache];
  
  NSBundle *bundle = [NSBundle bundleForClass:[self class]];
  NSString *path = [bundle pathForResource:@"Test-Image" ofType:@"png"];
  MZImage *image = [[MZImage alloc] initWithContentsOfFile:path];
  
  
  MZImage *imageNull = [cache thumbnailImageForAsset:asset];
  XCTAssertNil(imageNull, @"This image shouldn't be cached");
  
  [cache storeThumbnailImage:image forAsset:asset];
  
  MZImage *imageTwo = [cache thumbnailImageForAsset:asset];
  XCTAssertNotNil(imageTwo, @"This image should be cached");
  
#if TARGET_OS_IPHONE
  NSData *imagePixelsData = (NSData *)CFBridgingRelease(CGDataProviderCopyData(CGImageGetDataProvider(image.CGImage)));
  NSData *otherImagePixelsData = (NSData *)CFBridgingRelease(CGDataProviderCopyData(CGImageGetDataProvider(imageTwo.CGImage)));
#else
  NSData *imagePixelsData = [image TIFFRepresentation];
  NSData *otherImagePixelsData = [imageTwo TIFFRepresentation];
#endif
  
  XCTAssertTrue([imagePixelsData isEqualToData:otherImagePixelsData], @"Images data should be equal in bytes");
  
  [cache removeThumbnailImageForAsset:asset];
  
  imageNull = [cache thumbnailImageForAsset:asset];
  XCTAssertNil(imageNull, @"This image shouldn't be cached anymore");
}


- (void)testAssetCacheStoreThumbnailSlide
{
  [self setUpConnectedCommunicator];
  [MZAssetCache setSharedCache:[[MZAssetCache alloc] init]];
  
  NSString *slideUid = [proteinSource parameterForKey:kCanonicalParameterSlideUidKey];
  NSString *assetUid = [proteinSource parameterForKey:kCanonicalParameterAssetUidKey];
  
  MZSlide *slide = [[MZSlide alloc] init];
  slide.uid = slideUid;
  slide.contentSource = assetUid;
  MZAssetCache *cache = [MZAssetCache sharedCache];
  
  NSBundle *bundle = [NSBundle bundleForClass:[self class]];
  NSString *path = [bundle pathForResource:@"Test-Image" ofType:@"png"];
  MZImage *image = [[MZImage alloc] initWithContentsOfFile:path];
  
  
  MZImage *imageNull = [cache thumbnailImageForSlide:slide];
  XCTAssertNil(imageNull, @"This image shouldn't be cached");
  
  [cache storeThumbnailImage:image forSlide:slide];
  
  MZImage *imageTwo = [cache thumbnailImageForSlide:slide];
  XCTAssertNotNil(imageTwo, @"This image should be cached");
  
#if TARGET_OS_IPHONE
  NSData *imagePixelsData = (NSData *)CFBridgingRelease(CGDataProviderCopyData(CGImageGetDataProvider(image.CGImage)));
  NSData *otherImagePixelsData = (NSData *)CFBridgingRelease(CGDataProviderCopyData(CGImageGetDataProvider(imageTwo.CGImage)));
#else
  NSData *imagePixelsData = [image TIFFRepresentation];
  NSData *otherImagePixelsData = [imageTwo TIFFRepresentation];
#endif
  
  XCTAssertTrue([imagePixelsData isEqualToData:otherImagePixelsData], @"Images data should be equal in bytes");
  
  [cache removeThumbnailImageForSlide:slide];
  
  imageNull = [cache thumbnailImageForSlide:slide];
  XCTAssertNil(imageNull, @"This image shouldn't be cached anymore");
}


- (void)testAssetCacheStoreFullImageSlide
{
  [self setUpConnectedCommunicator];
  [MZAssetCache setSharedCache:[[MZAssetCache alloc] init]];
  
  NSString *slideUid = [proteinSource parameterForKey:kCanonicalParameterSlideUidKey];
  NSString *assetUid = [proteinSource parameterForKey:kCanonicalParameterAssetUidKey];
  
  MZSlide *slide = [[MZSlide alloc] init];
  slide.uid = slideUid;
  slide.contentSource = assetUid;
  MZAssetCache *cache = [MZAssetCache sharedCache];
  
  NSBundle *bundle = [NSBundle bundleForClass:[self class]];
  NSString *path = [bundle pathForResource:@"Test-Image" ofType:@"png"];
  MZImage *image = [[MZImage alloc] initWithContentsOfFile:path];
  
  
  MZImage *imageNull = [cache fullImageForSlide:slide];
  XCTAssertNil(imageNull, @"This image shouldn't be cached");
  
  [cache storeFullImage:image forSlide:slide];
  
  MZImage *imageTwo = [cache fullImageForSlide:slide];
  XCTAssertNotNil(imageTwo, @"This image should be cached");
  
#if TARGET_OS_IPHONE
  NSData *imagePixelsData = (NSData *)CFBridgingRelease(CGDataProviderCopyData(CGImageGetDataProvider(image.CGImage)));
  NSData *otherImagePixelsData = (NSData *)CFBridgingRelease(CGDataProviderCopyData(CGImageGetDataProvider(imageTwo.CGImage)));
#else
  NSData *imagePixelsData = [image TIFFRepresentation];
  NSData *otherImagePixelsData = [imageTwo TIFFRepresentation];
#endif
  
  XCTAssertTrue([imagePixelsData isEqualToData:otherImagePixelsData], @"Images data should be equal in bytes");
  
  [cache removeFullImageForSlide:slide];
  
  imageNull = [cache fullImageForSlide:slide];
  XCTAssertNil(imageNull, @"This image shouldn't be cached anymore");
}


- (void)testAssetCacheStoreOriginalImageSlide
{
  [self setUpConnectedCommunicator];
  [MZAssetCache setSharedCache:[[MZAssetCache alloc] init]];
  
  NSString *slideUid = [proteinSource parameterForKey:kCanonicalParameterSlideUidKey];
  NSString *assetUid = [proteinSource parameterForKey:kCanonicalParameterAssetUidKey];
  
  MZSlide *slide = [[MZSlide alloc] init];
  slide.uid = slideUid;
  slide.contentSource = assetUid;
  MZAssetCache *cache = [MZAssetCache sharedCache];
  
  NSBundle *bundle = [NSBundle bundleForClass:[self class]];
  NSString *path = [bundle pathForResource:@"Test-Image" ofType:@"png"];
  MZImage *image = [[MZImage alloc] initWithContentsOfFile:path];
  
  
  MZImage *imageNull = [cache originalImageForSlide:slide];
  XCTAssertNil(imageNull, @"This image shouldn't be cached");
  
  [cache storeOriginalImage:image forSlide:slide];
  
  MZImage *imageTwo = [cache originalImageForSlide:slide];
  XCTAssertNotNil(imageTwo, @"This image should be cached");
  
#if TARGET_OS_IPHONE
  NSData *imagePixelsData = (NSData *)CFBridgingRelease(CGDataProviderCopyData(CGImageGetDataProvider(image.CGImage)));
  NSData *otherImagePixelsData = (NSData *)CFBridgingRelease(CGDataProviderCopyData(CGImageGetDataProvider(imageTwo.CGImage)));
#else
  NSData *imagePixelsData = [image TIFFRepresentation];
  NSData *otherImagePixelsData = [imageTwo TIFFRepresentation];
#endif
  
  XCTAssertTrue([imagePixelsData isEqualToData:otherImagePixelsData], @"Images data should be equal in bytes");
  
  [cache removeOriginalImageForSlide:slide];
  
  imageNull = [cache originalImageForSlide:slide];
  XCTAssertNil(imageNull, @"This image shouldn't be cached anymore");
}

#pragma mark - URLS, Endpoints Tests

- (void)testPoolConfigurationAndEndPoints
{
  [self setUpConnectedCommunicator];

  SEL selector = NSSelectorFromString(@"createWSSPoolConfiguration:");
  IMP imp = [communicator methodForSelector:selector];
  void (*func)(id, SEL, NSString *) = (void *)imp;
  func(communicator, selector, nil);

  NSArray *poolConfigs = [communicator valueForKey:@"poolConfigs"];
  XCTAssertTrue(poolConfigs.count == 1, @"There should be just one pool config for WSS connections");

  OBCommunicatorPoolConfig* poolConfig = poolConfigs[0];

  XCTAssertTrue([communicator.serverUrl.absoluteString isEqualToString:@"wss://test"], @"Communicator serverUrl should be wss://test");
  XCTAssertTrue([poolConfig.poolName isEqualToString:@"mz-websocket"], @"poolConfig endPoint should be mz-websocket");
  XCTAssertTrue([poolConfig.endPoint isEqualToString:@"/plasma-web-proxy/sockjs/websocket"], @"poolConfig endPoint should be /plasma-web-proxy/sockjs/websocket");

  NSURL *imageUploadURL = [communicator httpImageUploadURL];
  XCTAssertTrue([imageUploadURL.absoluteString isEqualToString:@"https://test/mezzanine-upload/assets"], @"Image Upload URL should be https://test/mezzanine-upload/assets");
}

- (void)testBaseURLForHttpRequests
{
  [self setUpConnectedCommunicator];

  XCTAssertTrue([communicator.serverUrl.absoluteString isEqualToString:@"https://test"], @"Communicator serverUrl should be https://test");
}

- (void)setupTestsForRemoteParticipation
{
  communicator = [[MZCommunicator alloc] initWithServerUrl:[NSURL URLWithString:@"https://joinmz.com/m/123542346"]];
  systemModel = [[MZSystemModel alloc] init];
  communicator.systemModel = systemModel;

  communicator.communicatorProtocol = OBCommunicatorProtocolWSS;
  websocketPoolConnector = [[MockPoolConnector alloc] initWithPool:MEZZ_WEBSOCKET];
  [communicator addPoolConnector:websocketPoolConnector forPoolName:MEZZ_WEBSOCKET];
}

- (void)testPoolConfigurationAndEndPointsForRemoteParticipation
{
  [self setupTestsForRemoteParticipation];
  [self setUpConnectedCommunicator];

//  NOTE: if 'createWSSPoolConfiguration' was a public method we could call this but we're seeing this warning instead...
//  Undeclared selector 'createWSSPoolConfiguration:'
//  [communicator performSelector:@selector(createWSSPoolConfiguration:) withObject:nil];

//  NOTE 2: or this other warning if we extract the creation of the selector
//  PerformSeelctor may cause a leak because its selector is unknown
//  SEL selector = NSSelectorFromString(@"createWSSPoolConfiguration:");
//  [communicator performSelector:selector withObject:nil];

//  FULL explanation for this solution: https://stackoverflow.com/questions/7017281/performselector-may-cause-a-leak-because-its-selector-is-unknown/7017537#7017537
  SEL selector = NSSelectorFromString(@"createWSSPoolConfiguration:");
  IMP imp = [communicator methodForSelector:selector];
  void (*func)(id, SEL, NSString *) = (void *)imp;
  func(communicator, selector, nil);

  NSArray *poolConfigs = [communicator valueForKey:@"poolConfigs"];

  XCTAssertTrue(poolConfigs.count == 1, @"There should be just one pool config for WSS connections");
  OBCommunicatorPoolConfig* poolConfig = poolConfigs[0];

  XCTAssertTrue([communicator.serverUrl.absoluteString isEqualToString:@"wss://joinmz.com/m/123542346"], @"Communicator serverUrl should be wss://joinmz.com/m/123542346");
  XCTAssertTrue([poolConfig.poolName isEqualToString:@"mz-websocket"], @"poolConfig endPoint should be mz-websocket");
  XCTAssertTrue([poolConfig.endPoint isEqualToString:@"/plasma-web-proxy/sockjs/websocket"], @"poolConfig endPoint should be /plasma-web-proxy/sockjs/websocket");

  NSURL *imageUploadURL = [communicator httpImageUploadURL];
  XCTAssertTrue([imageUploadURL.absoluteString isEqualToString:@"https://joinmz.com/m/123542346/mezzanine-upload/assets"], @"Image Upload URL should be joinmz.com/m/123542346");
}

- (void)testBaseURLForHttpRequestsForRemoteParticiation
{
  [self setupTestsForRemoteParticipation];
  [self setUpConnectedCommunicator];

  XCTAssertTrue([communicator.serverUrl.absoluteString isEqualToString:@"https://joinmz.com/m/123542346"], @"Communicator serverUrl should be https://joinmz.com/m/123542346");
}

- (void)testNilUrlForNoResource
{
  XCTAssertNil([communicator urlForResource:nil]);
}

- (void)testNilNSURLBaseURLOnUrlForResource
{
  [self setupTestsForRemoteParticipation];
  [self setUpConnectedCommunicator];

  XCTAssertNil([communicator urlForResource:@"/r/global-assets/as-ae13427d-e206-4ea9-a809-75a4b654f965/img-local-thumb.png"].baseURL);
}


// Bug  17305

- (void)testNotContinueReconnectingIfDisconnected
{
  [self setUpConnectedCommunicator];
  id mockCommunicator = [OCMockObject partialMockForObject:communicator];
  [[mockCommunicator reject] checkMezzMetadataAndContinueConnection];
 
  // Preset condition
  communicator.isConnected = NO;
  [mockCommunicator wssPoolConnectorSuccess:OCMOCK_ANY];
  
  [mockCommunicator verify];
  
  XCTAssertNil(communicator.poolConnector);
  XCTAssertNil(communicator.primaryPool);
}


@end
