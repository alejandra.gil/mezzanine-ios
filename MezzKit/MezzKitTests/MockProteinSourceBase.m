//
//  MockProteinSource.m
//  MezzKit
//
//  Created by Zai Chang on 4/25/13.
//  Copyright (c) 2013 Oblong Industries. All rights reserved.
//

#import "MockProteinSourceBase.h"

#import "MZCommunicator+Constants.h"
#import "MZProtoFactory.h"


@implementation MockProteinSourceBase

-(id) init
{
  if (self = [super init])
  {
    MZProteinFactory *proteinFactory = [[MZProteinFactory alloc] init];
    constructor = [[MZCanonicalProteinConstructor alloc] initWithProteinFactory:proteinFactory];
  }
  return self;
}

-(OBProtein *) proteinForKey:(NSString*)key
{
  // TODO - Brute force as a test ... DON'T continue this patern! (preferably)
  // instead use the FileSystemProteinSource to feed tests pre-generated proteins...
  //
  // Otherwise we'd need to roll serverside proteins into a factory of some sort ...
  //
  NSString *provenance = [self parameterForKey:kCanonicalParameterProvenanceKey];

  NSDictionary *feldsDictionary =  @{
                                     @"left":
                                 @{@"type" : @"visible",
                                   @"cent" : [MZVect doubleVectWithX:0 y:0 z:0],
                                   @"norm" : [MZVect doubleVectWithX:0 y:0 z:0],
                                   @"over" : [MZVect doubleVectWithX:0 y:0 z:0],
                                   @"up" : [MZVect doubleVectWithX:0 y:0 z:0],
                                   @"px-size" : [MZVect integerVectWithX:1920 y:1080],
                                   @"phys-size" : [MZVect doubleVectWithX:1920 y:1080],
                                   },
                                     @"main":
                                 @{@"type" : @"visible",
                                   @"cent" : [MZVect doubleVectWithX:0 y:0 z:0],
                                   @"norm" : [MZVect doubleVectWithX:0 y:0 z:0],
                                   @"over" : [MZVect doubleVectWithX:0 y:0 z:0],
                                   @"up" : [MZVect doubleVectWithX:0 y:0 z:0],
                                   @"px-size" : [MZVect integerVectWithX:1920 y:1080],
                                   @"phys-size" : [MZVect doubleVectWithX:1920 y:1080],
                                   },
                                     @"right":
                                 @{@"type" : @"visible",
                                   @"cent" : [MZVect doubleVectWithX:0 y:0 z:0],
                                   @"norm" : [MZVect doubleVectWithX:0 y:0 z:0],
                                   @"over" : [MZVect doubleVectWithX:0 y:0 z:0],
                                   @"up" : [MZVect doubleVectWithX:0 y:0 z:0],
                                   @"px-size" : [MZVect integerVectWithX:1920 y:1080],
                                    @"phys-size" : [MZVect doubleVectWithX:1920 y:1080],
                                    }
                                     };
  
  NSString *uploadMaxWidth = [self parameterForKey:kCanonicalParameterUploadMaxWidth];
  NSString *uploadMaxHeight = [self parameterForKey:kCanonicalParameterUploadMaxHeight];
  NSString *uploadMaxImageSizeMB = [self parameterForKey:kCanonicalParameterUploadImageSizeMB];
  NSString *uploadMaxPDFSizeMB = [self parameterForKey:kCanonicalParameterUploadPDFSizeMB];
  NSString *uploadMaxSizeMB = [self parameterForKey:kCanonicalParameterUploadSizeMB];
  NSDictionary *uploadLimitsDictionary = @{@"upload-max-width": uploadMaxWidth,
                                           @"upload-max-height": uploadMaxHeight,
                                           @"upload-max-image-size-mb": uploadMaxImageSizeMB,
                                           @"upload-max-pdf-size-mb": uploadMaxPDFSizeMB,
                                           @"upload-max-size-mb": uploadMaxSizeMB,
                                           };

  NSString *whiteboardUid = [self parameterForKey:kCanonicalParameterWhiteboardUidKey];
  NSArray *whiteboardList = @[
                              @{@"uid": whiteboardUid}
                              ];

  NSString *collaborationParticipant1 = [self parameterForKey:kCanonicalParameterInfopresenceParticipant1Key];
  NSString *collaborationParticipant2 = [self parameterForKey:kCanonicalParameterInfopresenceParticipant2Key];
  NSString *collaborationIncomingRequestUid = [self parameterForKey:kCanonicalParameterInfopresenceIncomingJoinRequestUidKey];
  NSString *collaborationOutgoingRequestUid = [self parameterForKey:kCanonicalParameterInfopresenceOutgoingJoinRequestUidKey];
  NSString *remoteParticipantString1 = [self parameterForKey:kCanonicalParameterInfopresenceRemoteParticipant1Key];
  NSString *remoteParticipantString2 = [self parameterForKey:kCanonicalParameterInfopresenceRemoteParticipant2Key];


  NSString *liveStreamUid1 = [self parameterForKey:kCanonicalParameterLiveStreamUid1Key];
  NSString *liveStreamUid2 = [self parameterForKey:kCanonicalParameterLiveStreamUid2Key];
  NSString *liveStreamUid3 = [self parameterForKey:kCanonicalParameterLiveStreamUid3Key];
  NSArray *liveStreamsList3Active = @[
                                    @{@"source-id": liveStreamUid1,
                                      @"active": @YES,
                                      @"display-info": @{@"display-name": @"Video 1"}
                                      },
                                    @{@"source-id": liveStreamUid2,
                                      @"active": @YES,
                                      @"display-info": @{@"display-name": @"Video 2"}
                                      },
                                    @{@"source-id": liveStreamUid3,
                                      @"active": @YES,
                                      @"display-info": @{@"display-name": @"Video 3"}
                                      },
                                    ];

  NSString *assetUid = [self parameterForKey:kCanonicalParameterAssetUidKey];
  NSNumber *presentationScrollIndex = [self parameterForKey:kCanonicalParameterPresentationScrollIndexKey];
  NSDictionary *presentationDictionary = @{
                                           @"active": @YES,
                                           @"current-index" : presentationScrollIndex
                                           };
  
  NSString* windshieldItemUid = [self parameterForKey:kCanonicalParameterWindshieldItemUidKey];
  NSString* windshieldItemContentSource = [self parameterForKey:kCanonicalParameterWindshieldItemContentSourceKey];
  NSDictionary* windshieldItemFeldRelativeCoords = [self parameterForKey:kCanonicalParameterWindshieldItemFeldRelativeCoordsKey];
  NSDictionary* windshieldItemFeldRelativeSize = [self parameterForKey:kCanonicalParameterWindshieldItemFeldRelativeSizeKey];

  NSDictionary *windshieldItemDictionary = @{
                                             @"uid" : windshieldItemUid,
                                             @"content-source" : windshieldItemContentSource,
                                             @"feld-relative-coords" : windshieldItemFeldRelativeCoords,
                                             @"feld-relative-size" : windshieldItemFeldRelativeSize,
                                             };
  
  NSString *workspaceUid = [self parameterForKey:kCanonicalParameterWorkspaceUidKey];
  NSString *workspaceName = [self parameterForKey:kCanonicalParameterWorkspaceNameKey];
  NSString *workspaceOwner = [self parameterForKey:kCanonicalParameterWorkspaceOwnerKey];
  NSString *oldWorkspaceUid = [self parameterForKey:kCanonicalParameterWorkspaceOldUidKey];

  NSDictionary *savedWorkspaceDictionary = @{ @"uid" : workspaceUid,
                                              @"name" : workspaceName,
                                              @"owners" : @[],
                                              @"saved" : @YES,
                                              @"presentation" : presentationDictionary,
                                              @"live-streams" : liveStreamsList3Active
                                              };
  NSDictionary *unsavedWorkspaceDictionary = @{ @"uid" : workspaceUid,
                                                @"saved" : @NO,
                                                @"owners" : @[],
                                                @"presentation" : presentationDictionary,
                                                @"live-streams" : liveStreamsList3Active
                                                };
  NSDictionary *unsavedAndEmptyWorkspaceDictionary = @{ @"uid" : workspaceUid,
                                                        @"saved" : @NO,
                                                        @"owners" : @[],
                                                        @"presentation" : @{},
                                                        @"live-streams" : @[]
                                                        };
  NSDictionary *privateWorkspaceDictionary = @{ @"uid" : workspaceUid,
                                                @"name" : workspaceName,
                                                @"owners" : @[workspaceOwner],
                                                @"saved" : @YES,
                                                @"presentation" : presentationDictionary,
                                                @"live-streams" : liveStreamsList3Active
                                                };

  NSString *assetUid1 = [self parameterForKey:kCanonicalParameterAssetUid1Key];
  NSString *assetUid2 = [self parameterForKey:kCanonicalParameterAssetUid2Key];
  NSString *assetUid3 = [self parameterForKey:kCanonicalParameterAssetUid3Key];
  NSString *assetUid4 = [self parameterForKey:kCanonicalParameterAssetUid4Key];

  NSString *fileId1 = [self parameterForKey:kCanonicalParameterFileId1Key];
  NSString *fileId2 = [self parameterForKey:kCanonicalParameterFileId2Key];
  NSString *fileId3 = [self parameterForKey:kCanonicalParameterFileId3Key];
  
  NSString *infopresenceParticipant1 = [self parameterForKey:kCanonicalParameterInfopresenceParticipant1Key];
  NSString *infopresenceParticipant2 = [self parameterForKey:kCanonicalParameterInfopresenceParticipant2Key];
  NSString *infopresenceIncomingJoinRequestUid = [self parameterForKey:kCanonicalParameterInfopresenceIncomingJoinRequestUidKey];
  NSString *infopresenceOutgoingJoinRequestUid = [self parameterForKey:kCanonicalParameterInfopresenceOutgoingJoinRequestUidKey];
  NSString *infopresenceIncomingInviteUid = [self parameterForKey:kCanonicalParameterInfopresenceIncomingInviteUidKey];
  NSString *infopresenceOutgoingInviteUid = [self parameterForKey:kCanonicalParameterInfopresenceOutgoingInviteUidKey];
  NSDictionary *remoteParticipant1 = [self parameterForKey:kCanonicalParameterInfopresenceRemoteParticipant1Key];
  NSDictionary *remoteParticipant2 = [self parameterForKey:kCanonicalParameterInfopresenceRemoteParticipant2Key];
  
  NSString *infopresenceVideoChatPexipService = [self parameterForKey:kCanonicalParameterInfopresenceVideoChatPexipService];
  NSString *infopresenceVideoChatPexipConferenceUid = [self parameterForKey:kCanonicalParameterInfopresenceVideoChatPexipConferenceUid];
  NSString *infopresenceVideoChatPexipNodeUid = [self parameterForKey:kCanonicalParameterInfopresenceVideoChatPexipNodeUid];

  // Participants API 4.0
  NSDictionary *meetingRoomParticipantLocal = [self parameterForKey:kCanonicalParameterMeetingRoomParticipantLocalKey];
  NSDictionary *meetingRoomParticipant1 = [self parameterForKey:kCanonicalParameterMeetingRoomParticipant1Key];
  NSDictionary *meetingRoomParticipant2 = [self parameterForKey:kCanonicalParameterMeetingRoomParticipant2Key];
  NSDictionary *meetingRoomParticipantCloud = [self parameterForKey:kCanonicalParameterMeetingRoomParticipantCloudKey];
  NSDictionary *meetingParticipantLocal = [self parameterForKey:kCanonicalParameterMeetingParticipantLocalKey];
  NSDictionary *meetingParticipantRoom1 = [self parameterForKey:kCanonicalParameterMeetingParticipantRoom1Key];
  NSDictionary *meetingParticipantRoom2 = [self parameterForKey:kCanonicalParameterMeetingParticipantRoom2Key];
  NSDictionary *meetingParticipantRemoter = [self parameterForKey:kCanonicalParameterMeetingParticipantRemoterKey];

#pragma mark - Client Join

  if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameClientJoin responseType:@"response"]])
  {
    NSDictionary *ingests = @{ @"permission" : @YES,
                               @"felds" : feldsDictionary,
                               @"current-workspace" : savedWorkspaceDictionary,
                               @"whiteboards" : whiteboardList,
                               @"asset-upload-limits" : uploadLimitsDictionary,
                               @"mezzanine-version" : @"3.6"};
    return [OBProtein proteinWithDescrips:@[@"mezzanine",
                                            @"prot-spec v3.6",
                                            @"response",
                                            MZTransactionNameClientJoin,
                                            @"to:",
                                            @[provenance]]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameClientJoin responseType:@"response?single-feld"]])
  {
    NSDictionary *ingests = @{ @"permission" : @YES,
                               @"felds" : @{  @"main":
                                                @{@"type" : @"visible",
                                                  @"cent" : [MZVect doubleVectWithX:0 y:0 z:0],
                                                  @"norm" : [MZVect doubleVectWithX:0 y:0 z:0],
                                                  @"over" : [MZVect doubleVectWithX:0 y:0 z:0],
                                                  @"up" : [MZVect doubleVectWithX:0 y:0 z:0],
                                                  @"px-size" : [MZVect integerVectWithX:1920 y:1080],
                                                  @"phys-size" : [MZVect doubleVectWithX:1920 y:1080],
                                                  } },
                               @"current-workspace" : savedWorkspaceDictionary,
                               @"whiteboards" : whiteboardList,
                               @"asset-upload-limits" : uploadLimitsDictionary,
                               @"mezzanine-version" : @"3.6"};
    return [OBProtein proteinWithDescrips:@[@"mezzanine",
                                            @"prot-spec v3.6",
                                            @"response",
                                            MZTransactionNameClientJoin,
                                            @"to:",
                                            @[provenance,@(self.transactionId)]]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameClientJoin responseType:@"response?failed.passphrase-required"]])
  {
    NSDictionary *ingests = @{ @"permission" : @NO,
                               @"reason" : @"passkey-required",
                               @"mezzanine-version" : @"3.6"};
    return [OBProtein proteinWithDescrips:@[@"mezzanine",
                                            @"prot-spec v3.6",
                                            @"response",
                                            MZTransactionNameClientJoin,
                                            @"to:",
                                            @[provenance,@(self.transactionId)]]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameClientJoin responseType:@"response?failed.incorrect-passphrase"]])
  {
    NSDictionary *ingests = @{ @"permission" : @NO,
                               @"reason" : @"incorrect-passkey",
                               @"mezzanine-version" : @"3.6"};
    return [OBProtein proteinWithDescrips:@[@"mezzanine",
                                            @"prot-spec v3.6",
                                            @"response",
                                            MZTransactionNameClientJoin,
                                            @"to:",
                                            @[provenance,@(self.transactionId)]]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameClientJoin responseType:@"response?failed.incorrect-passphrase.3.0"]])
  {
    NSDictionary *ingests = @{ @"permission" : @NO,
                               @"reason" : @"incorrect-passkey",
                               @"mezzanine-version" : @"3.6"};
    return [OBProtein proteinWithDescrips:@[@"mezzanine",
                                            @"prot-spec v3.6",
                                            @"response",
                                            MZTransactionNameClientJoin,
                                            @"to:",
                                            @[provenance,@(self.transactionId)]]
                                  ingests:ingests];
  }
  

#pragma mark - Mezz Metadata

  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameMezzMetadata responseType:@"response?mezz-metadata-with-cloud-instance"]])
  {
    NSDictionary *ingests = @{ @"cloud-instance" : @(YES) };
    return [OBProtein proteinWithDescrips:@[@"mezzanine",
                                            @"prot-spec v3.6",
                                            @"response",
                                            MZTransactionNameMezzMetadata,
                                            @"from:",
                                            @"native-mezz",
                                            @"to:",
                                            @[provenance,@(self.transactionId)]]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameMezzMetadata responseType:@"response?mezz-metadata-without-cloud-instance"]])
  {
    NSDictionary *ingests = @{ @"cloud-instance" : @(NO) };
    return [OBProtein proteinWithDescrips:@[@"mezzanine",
                                            @"prot-spec v3.6",
                                            @"response",
                                            MZTransactionNameMezzMetadata,
                                            @"from:",
                                            @"native-mezz",
                                            @"to:",
                                            @[provenance,@(self.transactionId)]]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameMezzMetadata responseType:@"response?mezz-metadata-with-system-use-notification"]])
  {
    NSDictionary *ingests = @{ @"system-use-notification" : @true , @"system-use-notification-text" : @"sample warning text" };
    return [OBProtein proteinWithDescrips:@[@"mezzanine",
                                            @"prot-spec v3.6",
                                            @"response",
                                            MZTransactionNameMezzMetadata,
                                            @"from:",
                                            @"native-mezz",
                                            @"to:",
                                            @[provenance,@(self.transactionId)]]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameMezzMetadata responseType:@"response?mezz-metadata-without-system-use-notification"]])
  {
    NSDictionary *ingests = @{ @"system-use-notification" : @false , @"system-use-notification-text" : [NSNull null] };
    return [OBProtein proteinWithDescrips:@[@"mezzanine",
                                            @"prot-spec v3.6",
                                            @"response",
                                            MZTransactionNameMezzMetadata,
                                            @"from:",
                                            @"native-mezz",
                                            @"to:",
                                            @[provenance,@(self.transactionId)]]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameMezzMetadata responseType:@"response?failed.error-no-metadata-info"]])
  {
    NSDictionary *ingests = @{ @"description" : @"Mezzanine did not understand your request.",
                               @"error-code" : @"system",
                               @"summary" : @"Unknown Request"};
    return [OBProtein proteinWithDescrips:@[@"mezzanine",
                                            @"prot-spec v3.6",
                                            @"response",
                                            @"unknown-request",
                                            @"from:",
                                            @"native-mezz",
                                            @"to:",
                                            @[provenance,@(self.transactionId)]]
                                  ingests:ingests];
  }


#pragma mark - Client Management

  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameClientEvict responseType:@"pm"]])
  {
    NSDictionary *ingests = @{ };
    return [OBProtein proteinWithDescrips:@[@"mezzanine",
                                            @"prot-spec v3.6",
                                            @"response",
                                            MZTransactionNameClientEvict,
                                            @"to:",
                                            @[provenance,@(self.transactionId)]]
                                  ingests:ingests];
  }


#pragma mark - Mez-Caps

  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameMezzanineCapabilities responseType:@"psa"]])
  {
    NSDictionary *ingests = @{
                              @"whiteboards" : whiteboardList,
                              @"asset-upload-limits" : uploadLimitsDictionary};
    return [OBProtein proteinWithDescrips:@[@"psa",
                                            MZTransactionNameMezzanineCapabilities]
                                  ingests:ingests];
  }


#pragma mark - Passkey Proteins
  
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNamePasskeyDetails responseType:@"psa"]])
  {
    NSDictionary *ingests = @{ };
    return [OBProtein proteinWithDescrips:@[MZTransactionNamePasskeyDetails, @"psa"]
                                          ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNamePasskeyEnable responseType:@"psa"]])
  {
    NSDictionary *ingests = @{ };
    return [OBProtein proteinWithDescrips:@[MZTransactionNamePasskeyEnable, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNamePasskeyEnable responseType:@"psa?from-self"]])
  {
    NSDictionary *ingests = @{ @"exempt" : provenance };
    return [OBProtein proteinWithDescrips:@[MZTransactionNamePasskeyEnable, @"psa?from-self"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNamePasskeyDisable responseType:@"psa"]])
  {
    NSDictionary *ingests = @{ };
    return [OBProtein proteinWithDescrips:@[MZTransactionNamePasskeyDisable, @"psa"]
                                  ingests:ingests];
  }

  
#pragma mark - Infopresence Proteins
  
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionDetail responseType:@"psa"]])
  {
    NSDictionary *ingests = @{ @"status" : @"active",
                               @"members" : @[collaborationParticipant1, collaborationParticipant2],
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionDetail, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionDetail responseType:@"psa?v3.0"]])
  {
    NSDictionary *ingests = @{ @"status" : @"active",
                               @"rooms" : @[collaborationParticipant1, collaborationParticipant2],
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionDetail, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionDetail responseType:@"psa?not-active"]])
  {
    NSDictionary *ingests = @{ @"status" : @"inactive" };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionDetail, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionDetail responseType:@"psa?existing-incoming-request"]])
  {
    NSDictionary *ingests = @{ @"status" : @"active",
                               @"incoming-requests" : @[collaborationIncomingRequestUid],
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionDetail, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionDetail responseType:@"psa?existing-outgoing-request"]])
  {
    NSDictionary *ingests = @{ @"status" : @"active",
                               @"outgoing-request" : collaborationOutgoingRequestUid,
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionDetail, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionDetail responseType:@"psa?no-remote-participant"]])
  {
    NSDictionary *ingests = @{ @"status" : @"active",
                               @"remote-participants" : @[]
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionDetail, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionDetail responseType:@"psa?new-remote-participant"]])
  {
    NSDictionary *ingests = @{ @"status" : @"active",
                               @"remote-participants" : @[ remoteParticipantString1 ]
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionDetail, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionDetail responseType:@"psa?more-new-remote-participants"]])
  {
    NSDictionary *ingests = @{ @"status" : @"active",
                               @"remote-participants" : @[ remoteParticipantString1, remoteParticipantString2 ]
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionDetail, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionDetail responseType:@"psa?remote-access-active"]])
  {
    NSDictionary *ingests = @{ @"status" : @"active",
                               @"remote-access-status" : @"active",
                               @"remote-access-url" : @"http://www.oblong.com",
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionDetail, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionDetail responseType:@"psa?remote-access-inactive"]])
  {
    NSDictionary *ingests = @{ @"status" : @"active",
                               @"remote-access-status" : @"inactive",
                               @"remote-access-url" : @"",
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionDetail, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionDetail responseType:@"psa?remote-access-starting"]])
  {
    NSDictionary *ingests = @{ @"status" : @"active",
                               @"remote-access-status" : @"starting",
                               @"remote-access-url" : @""
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionDetail, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionLeave responseType:@"psa?neded"]])
  {
    NSDictionary *ingests = @{ @"status" : @"inactive" };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionDetail, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionIncomingRequest responseType:@"psa"]])
  {
    NSDictionary *ingests = @{ @"uid" : collaborationIncomingRequestUid };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionIncomingRequest, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionIncomingRequestResolve responseType:@"psa"]])
  {
    NSDictionary *ingests = @{ @"uid" : collaborationIncomingRequestUid,
                               @"resolution" : @"accepted"
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionIncomingRequestResolve, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionIncomingRequestResolve responseType:@"psa?canceled"]])
  {
    NSDictionary *ingests = @{ @"uid" : collaborationIncomingRequestUid,
                               @"resolution" : @"canceled"
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionIncomingRequestResolve, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionIncomingRequestResolve responseType:@"psa?declined"]])
  {
    NSDictionary *ingests = @{ @"uid" : collaborationIncomingRequestUid,
                               @"resolution" : @"declined"
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionIncomingRequestResolve, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionOutgoingRequest responseType:@"psa"]])
  {
    NSDictionary *ingests = @{ @"uid" : collaborationOutgoingRequestUid,
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionOutgoingRequest, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionOutgoingRequest responseType:@"response?error"]])
  {
    NSDictionary *ingests = @{ @"uid" : collaborationOutgoingRequestUid,
                               @"summary" : @"Infopresence Unavailable",
                               @"description" : @"An Infopresence session is pending."
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionOutgoingRequest, @"response", @"error"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionOutgoingRequestResolve responseType:@"psa"]])
  {
    NSDictionary *ingests = @{ @"uid" : collaborationOutgoingRequestUid,
                               @"resolution" : @"accepted"
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionOutgoingRequestResolve, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionOutgoingRequestResolve responseType:@"psa?canceled"]])
  {
    NSDictionary *ingests = @{ @"uid" : collaborationOutgoingRequestUid,
                               @"resolution" : @"canceled"
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionOutgoingRequestResolve, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionOutgoingRequestResolve responseType:@"psa?declined"]])
  {
    NSDictionary *ingests = @{ @"uid" : collaborationOutgoingRequestUid,
                               @"resolution" : @"declined"
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionOutgoingRequestResolve, @"psa"]
                                  ingests:ingests];
  }


#pragma mark - Windshield Proteins

  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameWindshieldDetail responseType:@"psa"]])
  {
    NSMutableArray *windshieldItems = [NSMutableArray array];
    for (NSInteger i=0; i<8; i++)
    {
      NSMutableDictionary *item = [windshieldItemDictionary mutableCopy];
      [item setObject:[NSString stringWithFormat:@"el-000%ld", (long)i] forKey:@"uid"];
      [windshieldItems addObject:item];
    }
    NSDictionary *ingests = @{ @"windshield-items" : windshieldItems };
    OBProtein *p = [OBProtein proteinWithDescrips:@[MZTransactionNameWindshieldDetail,
                                                    @"psa"]
                                          ingests:ingests];
    return p;
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameWindshieldClear responseType:@"psa"]])
  {
    NSDictionary *ingests = @{ @"surfaces" : @[@"main"] };
    OBProtein *p = [OBProtein proteinWithDescrips:@[MZTransactionNameWindshieldClear,
                                                    @"psa"]
                                          ingests:ingests];
    return p;
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameWindshieldItemCreate responseType:@"psa"]])
  {
    NSDictionary *ingests = windshieldItemDictionary;
    OBProtein *p = [OBProtein proteinWithDescrips:@[MZTransactionNameWindshieldItemCreate,
                                                    @"psa"]
                                          ingests:ingests];
    return p;
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameWindshieldItemDelete responseType:@"psa"]])
  {
    NSDictionary *ingests = @{ @"uid" : windshieldItemUid };
    OBProtein *p = [OBProtein proteinWithDescrips:@[MZTransactionNameWindshieldItemDelete,
                                                    @"psa"]
                                          ingests:ingests];
    return p;
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameWindshieldItemTransform responseType:@"psa"]])
  {
    NSDictionary *ingests = windshieldItemDictionary;
    OBProtein *p = [OBProtein proteinWithDescrips:@[MZTransactionNameWindshieldItemTransform,
                                                    @"psa"]
                                          ingests:ingests];
    return p;
  }
  
  
#pragma mark - Live Streams Proteins
  
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameLiveStreamsDetail responseType:@"psa?3-active"]])
  {
    NSDictionary *ingests = @{ @"live-streams" : liveStreamsList3Active };
    OBProtein *p = [OBProtein proteinWithDescrips:@[MZTransactionNameLiveStreamsDetail,
                                                    @"psa"]
                                          ingests:ingests];
    return p;
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameLiveStreamsDetail responseType:@"psa?1-active"]])
  {
    NSDictionary *ingests = @{ @"live-streams" : @[
                                   @{@"source-id": liveStreamUid1,
                                     @"active": @YES,
                                     @"display-info": @{@"display-name": @"Video 1"}
                                     },
                                   ],
                               };
    OBProtein *p = [OBProtein proteinWithDescrips:@[MZTransactionNameLiveStreamsDetail,
                                                    @"psa"]
                                          ingests:ingests];
    return p;
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameLiveStreamsDetail responseType:@"psa?2-active-1-inactive"]])
  {
    NSDictionary *ingests = @{ @"live-streams" : @[
                                   @{@"source-id": liveStreamUid1,
                                     @"active": @YES,
                                     @"display-info": @{@"display-name": @"Video 1"}
                                     },
                                   @{@"source-id": liveStreamUid2,
                                     @"active": @YES,
                                     @"display-info": @{@"display-name": @"Video 2"}
                                     },
                                   @{@"source-id": liveStreamUid3,
                                     @"active": @NO,
                                     @"display-info": @{@"display-name": @"Video 3"}
                                     },
                                   ],
                               };
    OBProtein *p = [OBProtein proteinWithDescrips:@[MZTransactionNameLiveStreamsDetail,
                                                    @"psa"]
                                          ingests:ingests];
    return p;
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameLiveStreamsDetail responseType:@"psa?3-inactive"]])
  {
    NSDictionary *ingests = @{ @"live-streams" : @[
                                   @{@"source-id": liveStreamUid1,
                                     @"active": @NO,
                                     @"display-info": @{@"display-name": @"Video 1"}
                                     },
                                   @{@"source-id": liveStreamUid2,
                                     @"active": @NO,
                                     @"display-info": @{@"display-name": @"Video 2"}
                                     },
                                   @{@"source-id": liveStreamUid3,
                                     @"active": @NO,
                                     @"display-info": @{@"display-name": @"Video 3"}
                                     },
                                   ],
                               };
    OBProtein *p = [OBProtein proteinWithDescrips:@[MZTransactionNameLiveStreamsDetail,
                                                    @"psa"]
                                          ingests:ingests];
    return p;
  }


#pragma mark - Portfolio Proteins

  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNamePortfolioItemInsert responseType:@"psa?index-0"]])
  {
    NSString *insertedSlideUid = [self parameterForKey:kCanonicalParameterSlideInsertionUidKey];
    NSDictionary *ingests = @{ @"uid" : insertedSlideUid,
                               @"index" : @0,
                               @"content-source" : assetUid,
                             };
    OBProtein *p = [OBProtein proteinWithDescrips:@[MZTransactionNamePortfolioItemInsert,
                                                    @"psa"]
                                          ingests:ingests];
    return p;
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNamePortfolioItemInsert responseType:@"psa?index-2"]])
  {
    NSString *insertedSlideUid = [self parameterForKey:kCanonicalParameterSlideInsertionUidKey];
    NSDictionary *ingests = @{ @"uid" : insertedSlideUid,
                               @"index" : @2,
                               @"content-source" : assetUid,
                               };
    OBProtein *p = [OBProtein proteinWithDescrips:@[MZTransactionNamePortfolioItemInsert,
                                                    @"psa"]
                                          ingests:ingests];
    return p;
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNamePortfolioItemInsert responseType:@"psa?index-4"]])
  {
    NSString *insertedSlideUid = [self parameterForKey:kCanonicalParameterSlideInsertionUidKey];
    NSDictionary *ingests = @{ @"uid" : insertedSlideUid,
                               @"index" : @4,
                               @"content-source" : assetUid,
                               };
    OBProtein *p = [OBProtein proteinWithDescrips:@[MZTransactionNamePortfolioItemInsert,
                                                    @"psa"]
                                          ingests:ingests];
    return p;
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNamePortfolioItemDelete responseType:@"psa"]])
  {
    NSString *deletedSlideUid = [self parameterForKey:kCanonicalParameterSlideDeletionUidKey];
    NSDictionary *ingests = @{ @"uid" : deletedSlideUid };
    OBProtein *p = [OBProtein proteinWithDescrips:@[MZTransactionNamePortfolioItemDelete,
                                                    @"psa"]
                                          ingests:ingests];
    return p;
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNamePortfolioItemReorder responseType:@"psa?index-0"]])
  {
    NSString *reorderedSlideUid = [self parameterForKey:kCanonicalParameterSlideReorderUidKey];
    NSDictionary *ingests = @{ @"uid" : reorderedSlideUid,
                               @"index" : @0,
                               };
    OBProtein *p = [OBProtein proteinWithDescrips:@[MZTransactionNamePortfolioItemReorder,
                                                    @"psa"]
                                          ingests:ingests];
    return p;
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNamePortfolioItemReorder responseType:@"psa?index-4"]])
  {
    NSString *reorderedSlideUid = [self parameterForKey:kCanonicalParameterSlideReorderUidKey];
    NSDictionary *ingests = @{ @"uid" : reorderedSlideUid,
                               @"index" : @4,
                               };
    OBProtein *p = [OBProtein proteinWithDescrips:@[MZTransactionNamePortfolioItemReorder,
                                                    @"psa"]
                                          ingests:ingests];
    return p;
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNamePortfolioClear responseType:@"psa"]])
  {
    NSDictionary *ingests = @{ @"workspace-uid" : workspaceUid };
    OBProtein *p = [OBProtein proteinWithDescrips:@[MZTransactionNamePortfolioClear,
                                                    @"psa"]
                                          ingests:ingests];
    return p;
  }

#pragma mark - Presentation Proteins
  
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNamePresentationDetail responseType:@"psa"]])
  {
    NSDictionary *ingests = @{ @"workspace-uid" : workspaceUid,
                               @"active": @YES,
                               @"current-index" : presentationScrollIndex
                             };
    return [OBProtein proteinWithDescrips:@[MZTransactionNamePresentationDetail, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNamePresentationDetail responseType:@"psa?wrong-workspace"]])
  {
    NSDictionary *ingests = @{ @"workspace-uid" : @"ObvouslyWrongWorkspaceUid",
                               @"active": @YES,
                               @"current-index" : presentationScrollIndex
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNamePresentationDetail, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNamePresentationStart responseType:@"psa"]])
  {
    NSDictionary *ingests = @{ @"workspace-uid" : workspaceUid,
                               @"active": @YES,
                               @"current-index" : presentationScrollIndex
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNamePresentationDetail, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNamePresentationStart responseType:@"psa?wrong-workspace"]])
  {
    NSDictionary *ingests = @{ @"workspace-uid" : @"ObvouslyWrongWorkspaceUid",
                               @"active": @YES,
                               @"current-index" : presentationScrollIndex
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNamePresentationDetail, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNamePresentationStop responseType:@"psa"]])
  {
    NSDictionary *ingests = @{ @"workspace-uid" : workspaceUid,
                               @"active": @NO,
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNamePresentationDetail, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNamePresentationStop responseType:@"psa?wrong-workspace"]])
  {
    NSDictionary *ingests = @{ @"workspace-uid" : @"ObvouslyWrongWorkspaceUid",
                               @"active": @NO,
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNamePresentationDetail, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNamePresentationScroll responseType:@"psa"]])
  {
    NSDictionary *ingests = @{ @"workspace-uid" : workspaceUid,
                               @"current-index" : presentationScrollIndex
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNamePresentationDetail, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNamePresentationScroll responseType:@"psa?wrong-workspace"]])
  {
    NSDictionary *ingests = @{ @"workspace-uid" : @"ObvouslyWrongWorkspaceUid",
                               @"current-index" : presentationScrollIndex
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNamePresentationDetail, @"psa"]
                                  ingests:ingests];
  }

#pragma mark - Workspace Proteins
  
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameWorkspaceSwitch responseType:@"psa"]])
  {
    NSDictionary *ingests = @{ @"current-workspace" : savedWorkspaceDictionary,
                               @"previous-workspace" : @{ @"uid" : oldWorkspaceUid,
                                                          @"destroyed" : [NSNumber numberWithBool:NO]
                                                        }};
    OBProtein *p = [OBProtein proteinWithDescrips:@[MZTransactionNameWorkspaceSwitch,
                                                    @"psa"]
                                          ingests:ingests];
    return p;
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameWorkspaceSwitch responseType:@"psa?to-empty-workspace"]])
  {
    NSDictionary *ingests = @{ @"current-workspace" : unsavedAndEmptyWorkspaceDictionary,
                               @"previous-workspace" : @{ @"uid" : oldWorkspaceUid,
                                                          @"destroyed" : [NSNumber numberWithBool:NO]
                                                          }};
    OBProtein *p = [OBProtein proteinWithDescrips:@[MZTransactionNameWorkspaceSwitch,
                                                    @"psa"]
                                          ingests:ingests];
    return p;
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameWorkspaceOpen responseType:@"psa"]])
  {
    // workspace-state is the response to open
    NSDictionary *ingests = @{ @"current-workspace" : savedWorkspaceDictionary };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameWorkspaceSwitch, @"psa"]
                                          ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameWorkspaceClose responseType:@"psa"]] ||
           [key isEqual:[self proteinKeyForTransactionName:MZTransactionNameWorkspaceDiscard responseType:@"psa"]])
  {
    // workspace-state is the response to close or discard
    NSDictionary *ingests = @{ @"current-workspace" : unsavedWorkspaceDictionary };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameWorkspaceSwitch, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameWorkspaceCreate responseType:@"psa"]] ||
           [key isEqual:[self proteinKeyForTransactionName:MZTransactionNameWorkspaceDuplicate responseType:@"psa"]])
  {
    OBProtein *p = [OBProtein proteinWithDescrips:@[MZTransactionNameWorkspaceCreate,
                                                    @"psa"]
                                          ingests:savedWorkspaceDictionary];
    return p;
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameWorkspaceCreate responseType:@"pm"]] ||
           [key isEqual:[self proteinKeyForTransactionName:MZTransactionNameWorkspaceDuplicate responseType:@"pm"]])
  {
    OBProtein *p = [OBProtein proteinWithDescrips:@[@"mezzanine",
                                                    @"prot-spec v3.6",
                                                    @"pm",
                                                    MZTransactionNameWorkspaceCreate,
                                                    @"to:",
                                                    @[provenance,@(self.transactionId)]]
                                          ingests:privateWorkspaceDictionary];
    return p;
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameWorkspaceDelete responseType:@"psa"]])
  {
    NSDictionary *ingests = @{ @"uid" : workspaceUid };
    OBProtein *p = [OBProtein proteinWithDescrips:@[MZTransactionNameWorkspaceDelete,
                                                    @"psa"]
                                          ingests:ingests];
    return p;
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameWorkspaceReassign responseType:@"psa"]])
  {
    NSDictionary *ingests = @{ @"old-uid" : oldWorkspaceUid,
                               @"new-uid" : workspaceUid, };
    OBProtein *p = [OBProtein proteinWithDescrips:@[MZTransactionNameWorkspaceReassign,
                                                    @"psa"]
                                          ingests:ingests];
    return p;
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameWorkspaceRename responseType:@"psa"]])
  {
    NSDictionary *ingests = savedWorkspaceDictionary;
    OBProtein *p = [OBProtein proteinWithDescrips:@[MZTransactionNameWorkspaceRename,
                                                    @"psa"]
                                          ingests:ingests];
    return p;
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameWorkspaceModified responseType:@"psa"]])
  {
    NSDate *modifiedDate = [self parameterForKey:kCanonicalParameterWorkspaceModifiedKey];
    NSDictionary *ingests = @{ @"uid": workspaceUid,
                               @"mod-date" : [modifiedDate description],
                               @"mod-date-utc" : @((NSInteger)modifiedDate.timeIntervalSince1970) };
    OBProtein *p = [OBProtein proteinWithDescrips:@[MZTransactionNameWorkspaceModified,
                                                    @"psa"]
                                          ingests:ingests];
    return p;
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameWorkspaceSave responseType:@"psa"]])
  {
    NSDictionary *ingests = savedWorkspaceDictionary;
    OBProtein *p = [OBProtein proteinWithDescrips:@[MZTransactionNameWorkspaceSave,
                                                    @"psa"]
                                          ingests:ingests];
    return p;
  }
  
  
#pragma mark - Sign in/out
  
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameClientSignIn responseType:@"client-sign-in-success"]])
  {
    NSDictionary *ingests = @{@"username": @"username",
                              @"superuser" : [NSNumber numberWithBool:NO]};

    OBProtein *p = [OBProtein proteinWithDescrips:@[@"response",
                                                    MZTransactionNameClientSignIn,
                                                    @"to:",
                                                    @[provenance,@(self.transactionId)]]
                                          ingests:ingests];
    return p;
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameClientSignIn responseType:@"client-sign-in-fail"]])
  {
    NSDictionary *ingests = @{@"description": @"This client could not be signed in.",
                              @"error-code" : [NSNumber numberWithInteger:-1],
                              @"summary" : @"Sign In Unsucessful"};
    
    OBProtein *p = [OBProtein proteinWithDescrips:@[@"response",
                                                    MZTransactionNameClientSignIn,
                                                    @"to:",
                                                    @[provenance,@(self.transactionId)],
                                                    @"error"
                                                    ]
                                          ingests:ingests];
    return p;
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameClientSignOut responseType:@"client-sign-out"]])
  {
    OBProtein *p = [OBProtein proteinWithDescrips:@[@"response",
                                                    MZTransactionNameClientSignOut,
                                                    @"to:",
                                                    @[provenance,@(self.transactionId)]]
                                          ingests:@{}];
    return p;
  }

  
#pragma mark - Workspace List

  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameWorkspaceList responseType:@"response?public-workspaces"]])
  {
    NSMutableArray *workspaces = [[NSMutableArray alloc] init];

    NSArray *publicWorkspaceUid = [self parameterForKey:kCanonicalParameterPublicWorkspaceUidKey];

    for (NSString *workspaceUid in publicWorkspaceUid)
    {
      [workspaces addObject:@{@"uid" : workspaceUid,
                              @"name" : workspaceName,
                              @"owners" : @[],
                              @"saved" : @YES,
                              @"presentation" : presentationDictionary
                              }];
    }

    NSDictionary *ingests = @{@"workspaces": workspaces};
    OBProtein *p = [OBProtein proteinWithDescrips:@[@"response",
                                                    MZTransactionNameWorkspaceList,
                                                    @"to:",
                                                    @[provenance,@(self.transactionId)]]
                                          ingests:ingests];
    return p;
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameWorkspaceList responseType:@"response?public-and-private-workspaces"]])
  {
    NSMutableArray *workspaces = [[NSMutableArray alloc] init];

    NSArray *publicWorkspaceUid = [self parameterForKey:kCanonicalParameterPublicWorkspaceUidKey];
    NSArray *privateWorkspaceUid = [self parameterForKey:kCanonicalParameterPrivateWorkspaceUidKey];

    for (NSString *workspaceUid in publicWorkspaceUid)
    {
      [workspaces addObject:@{@"uid" : workspaceUid,
                              @"name" : workspaceName,
                              @"owners" : @[],
                              @"saved" : @YES,
                              @"presentation" : presentationDictionary
                              }];
    }

    for (NSString *workspaceUid in privateWorkspaceUid)
    {
      [workspaces addObject: @{@"uid" : workspaceUid,
                               @"name" : workspaceName,
                               @"owners" : @[workspaceOwner],
                               @"saved" : @YES,
                               @"presentation" : presentationDictionary
                               }];
    }

    NSDictionary *ingests = @{@"workspaces": workspaces};
    OBProtein *p = [OBProtein proteinWithDescrips:@[@"response",
                                                    MZTransactionNameWorkspaceList,
                                                    @"to:",
                                                    @[provenance,@(self.transactionId)]]
                                          ingests:ingests];
    return p;
  }

#pragma mark - Asset Upload Proteins

  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameAssetUploadProvision responseType:@"response?all-images-fit"]])
  {
    NSDictionary *ingests = @{ @"uids" : @[assetUid1, assetUid2, assetUid3],
                               @"files": @[@{@"file-id": fileId1,
                                             @"file-name":@"filename.png",
                                             @"type": @"png",
                                             @"uids": @[assetUid1]},
                                           @{@"file-id": fileId2,
                                             @"file-name":@"filename.png",
                                             @"type": @"png",
                                             @"uids": @[assetUid2]},
                                           @{@"file-id": fileId3,
                                             @"file-name":@"filename.png",
                                             @"type": @"png",
                                             @"uids": @[assetUid3]}
                                           ]
                               };

    OBProtein *p = [OBProtein proteinWithDescrips:@[@"mezzanine",
                                                    @"prot-spec v3.6",
                                                    @"response",
                                                    MZTransactionNameAssetUploadProvision,
                                                    @"to:",
                                                    @[provenance,@(self.transactionId)]]
                                          ingests:ingests];
    return p;
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameAssetUploadProvision responseType:@"response?some-images-fit"]])
  {
    NSDictionary *ingests = @{ @"uids" : @[assetUid1, assetUid2],
                               @"files": @[@{@"file-id": fileId1,
                                             @"file-name":@"filename.png",
                                             @"type": @"png",
                                             @"uids": @[assetUid1]},
                                           @{@"file-id": fileId2,
                                             @"file-name":@"filename.png",
                                             @"type": @"png",
                                             @"uids": @[assetUid2]},
                                           @{@"file-id": fileId3,
                                             @"file-name":@"filename.png",
                                             @"type": @"png",
                                             @"uids": @[]}
                                           ]
                               };

    OBProtein *p = [OBProtein proteinWithDescrips:@[@"mezzanine",
                                                    @"prot-spec v3.6",
                                                    @"response",
                                                    MZTransactionNameAssetUploadProvision,
                                                    @"to:",
                                                    @[provenance,@(self.transactionId)]]
                                          ingests:ingests];
    return p;
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameAssetUploadProvision responseType:@"response?no-images-fit"]])
  {
    NSDictionary *ingests = @{ @"summary": @"no slots available - test summary",
                               @"description": @"no slots available - test description",
                               @"uids" : @[],
                               @"files": @[@{@"file-id": fileId1,
                                             @"file-name":@"filename.png",
                                             @"type": @"png",
                                             @"uids": @[]},
                                           @{@"file-id": fileId2,
                                             @"file-name":@"filename.png",
                                             @"type": @"png",
                                             @"uids": @[]},
                                           @{@"file-id": fileId3,
                                             @"file-name":@"filename.png",
                                             @"type": @"png",
                                             @"uids": @[]}
                                           ]
                               };
    
    OBProtein *p = [OBProtein proteinWithDescrips:@[@"mezzanine",
                                                    @"prot-spec v3.6",
                                                    @"response",
                                                    MZTransactionNameAssetUploadProvision,
                                                    @"to:",
                                                    @[provenance,@(self.transactionId)]]
                                          ingests:ingests];
    return p;
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameAssetRefresh responseType:@"psa?image-asset-1"]])
  {
    OBProtein *p = [OBProtein proteinWithDescrips:@[@"mezzanine",
                                                    @"prot-spec v3.6",
                                                    @"psa",
                                                    MZTransactionNameAssetRefresh,
                                                    @"from:",
                                                    @"native-mezz"]
                                          ingests:@{ @"content-source": assetUid1 }];
    return p;
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameAssetRefresh responseType:@"psa?image-asset-2"]])
  {
    OBProtein *p = [OBProtein proteinWithDescrips:@[@"mezzanine",
                                                    @"prot-spec v3.6",
                                                    @"psa",
                                                    MZTransactionNameAssetRefresh,
                                                    @"from:",
                                                    @"native-mezz"]
                                          ingests:@{ @"content-source": assetUid2 }];
    return p;
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameAssetRefresh responseType:@"psa?image-asset-3"]])
  {
    OBProtein *p = [OBProtein proteinWithDescrips:@[@"mezzanine",
                                                    @"prot-spec v3.6",
                                                    @"psa",
                                                    MZTransactionNameAssetRefresh,
                                                    @"from:",
                                                    @"native-mezz"]
                                          ingests:@{ @"content-source": assetUid3 }];
    return p;
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameAssetRefresh responseType:@"psa?image-asset-4"]])
  {
    OBProtein *p = [OBProtein proteinWithDescrips:@[@"mezzanine",
                                                    @"prot-spec v3.6",
                                                    @"psa",
                                                    MZTransactionNameAssetRefresh,
                                                    @"from:",
                                                    @"native-mezz"]
                                          ingests:@{ @"content-source": assetUid4 }];
    return p;
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameAssetUploadProvision responseType:@"response?pdf-all-pages-fit"]])
  {
    NSDictionary *ingests = @{ @"summary": @"",
                               @"description": @"",
                               @"uids" : @[assetUid1, assetUid2, assetUid3, assetUid4],
                               @"files": @[@{@"file-id": fileId1,
                                             @"file-name":@"filename.pdf",
                                             @"type": @"pdf",
                                             @"pages": @4,
                                             @"uids": @[assetUid1, assetUid2, assetUid3, assetUid4]}
                                           ]
                               };

    OBProtein *p = [OBProtein proteinWithDescrips:@[@"mezzanine",
                                                    @"prot-spec v3.6",
                                                    @"response",
                                                    MZTransactionNameAssetUploadProvision,
                                                    @"to:",
                                                    @[provenance,@(self.transactionId)]]
                                          ingests:ingests];
    return p;
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameAssetUploadProvision responseType:@"response?pdf-does-not-fit"]])
  {
    NSDictionary *ingests = @{ @"summary": @"no slots available - test summary",
                               @"description": @"no slots available - test description",
                               @"uids" : @[],
                               @"files": @[@{@"file-id": fileId1,
                                             @"file-name":@"filename.pdf",
                                             @"type": @"pdf",
                                             @"pages": @4,
                                             @"uids": @[]}
                                           ]
                               };

    OBProtein *p = [OBProtein proteinWithDescrips:@[@"mezzanine",
                                                    @"prot-spec v3.6",
                                                    @"response",
                                                    MZTransactionNameAssetUploadProvision,
                                                    @"to:",
                                                    @[provenance,@(self.transactionId)]]
                                          ingests:ingests];
    return p;
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameAssetUploadProvision responseType:@"pm?asset-upload-pdf"]])
  {
    OBProtein *p = [OBProtein proteinWithDescrips:@[@"mezzanine",
                                                    @"prot-spec v3.6",
                                                    @"pm",
                                                    @"asset-upload-pdf",
                                                    @"from:",
                                                    @"native-mezz",
                                                    @"for:",
                                                    @[provenance, @(self.transactionId)]]
                                          ingests:@{ @"file-ids": @[fileId1] }];
    return p;
  }
  
  
#pragma mark - Infopresence Proteins
  
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionDetail responseType:@"psa"]])
  {
    NSDictionary *ingests = @{ @"status" : @"active",
                               @"members" : @[infopresenceParticipant1, infopresenceParticipant2],
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionDetail, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionDetail responseType:@"psa?v3.0"]])
  {
    NSDictionary *ingests = @{ @"status" : @"active",
                               @"rooms" : @[infopresenceParticipant1, infopresenceParticipant2],
                               @"mezzanine-version" : @"3.0"};
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionDetail, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionDetail responseType:@"psa?not-active"]])
  {
    NSDictionary *ingests = @{ @"status" : @"inactive" };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionDetail, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionDetail responseType:@"psa?existing-incoming-request"]])
  {
    NSDictionary *ingests = @{ @"status" : @"active",
                               @"incoming-calls" : @[ @{ @"uid": infopresenceIncomingJoinRequestUid,
                                                         @"type": kMZInfopresenceCallTypeJoinRequest } ]
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionDetail, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionDetail responseType:@"psa?existing-outgoing-request"]])
  {
    NSDictionary *ingests = @{ @"status" : @"active",
                               @"outgoing-calls" : @[ @{ @"uid": infopresenceOutgoingJoinRequestUid,
                                                         @"type": kMZInfopresenceCallTypeJoinRequest } ]
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionDetail, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionDetail responseType:@"psa?no-remote-participant"]])
  {
    NSDictionary *ingests = @{ @"status" : @"active",
                               @"remote-participants" : @[]
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionDetail, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionDetail responseType:@"psa?new-remote-participant"]])
  {
    NSDictionary *ingests = @{ @"status" : @"active",
                               @"remote-participants" : @[ remoteParticipantString1 ]
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionDetail, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionDetail responseType:@"psa?more-new-remote-participants"]])
  {
    NSDictionary *ingests = @{ @"status" : @"active",
                               @"remote-participants" : @[ remoteParticipantString1, remoteParticipantString2 ]
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionDetail, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionDetail responseType:@"psa?remote-access-active"]])
  {
    NSDictionary *ingests = @{ @"status" : @"active",
                               @"remote-access-status" : @"active",
                               @"remote-access-url" : @"http://www.oblong.com",
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionDetail, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionDetail responseType:@"psa?remote-access-inactive"]])
  {
    NSDictionary *ingests = @{ @"status" : @"active",
                               @"remote-access-status" : @"inactive",
                               @"remote-access-url" : @"",
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionDetail, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionDetail responseType:@"psa?remote-access-starting"]])
  {
    NSDictionary *ingests = @{ @"status" : @"active",
                               @"remote-access-status" : @"starting",
                               @"remote-access-url" : @""
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionDetail, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionDetail responseType:@"psa?video-chat"]])
  {
    NSDictionary *ingests = @{ @"status" : @"active",
                               @"video-chat" : @{ @"service" : infopresenceVideoChatPexipService,
                                                  @"pexip" : @{ @"node" : infopresenceVideoChatPexipNodeUid,
                                                                @"conference" : infopresenceVideoChatPexipConferenceUid
                                                                }
                                                  }
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionDetail, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionDetail responseType:@"psa?null-remote-access-values"]])
  {
    NSDictionary *ingests = @{ @"status" : @"inactive",
                               @"remote-access-status" : [NSNull null],
                               @"remote-access-url" : [NSNull null] };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionDetail, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionLeave responseType:@"psa?neded"]])
  {
    NSDictionary *ingests = @{ @"status" : @"inactive" };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionDetail, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionIncomingRequest responseType:@"psa"]])
  {
    NSDictionary *ingests = @{ @"uid" : infopresenceIncomingJoinRequestUid };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionIncomingRequest, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionIncomingRequestResolve responseType:@"psa"]])
  {
    NSDictionary *ingests = @{ @"uid" : infopresenceIncomingJoinRequestUid,
                               @"resolution" : kMZInfopresenceCallResolutionAccepted
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionIncomingRequestResolve, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionIncomingRequestResolve responseType:@"psa?canceled"]])
  {
    NSDictionary *ingests = @{ @"uid" : infopresenceIncomingJoinRequestUid,
                               @"resolution" : kMZInfopresenceCallResolutionCanceled
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionIncomingRequestResolve, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionIncomingRequestResolve responseType:@"psa?declined"]])
  {
    NSDictionary *ingests = @{ @"uid" : infopresenceIncomingJoinRequestUid,
                               @"resolution" : kMZInfopresenceCallResolutionDeclined
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionIncomingRequestResolve, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionOutgoingRequest responseType:@"psa"]])
  {
    NSDictionary *ingests = @{ @"uid" : infopresenceOutgoingJoinRequestUid,
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionOutgoingRequest, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionOutgoingRequestResolve responseType:@"psa"]])
  {
    NSDictionary *ingests = @{ @"uid" : infopresenceOutgoingJoinRequestUid,
                               @"resolution" : kMZInfopresenceCallResolutionAccepted
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionOutgoingRequestResolve, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionOutgoingRequestResolve responseType:@"psa?canceled"]])
  {
    NSDictionary *ingests = @{ @"uid" : infopresenceOutgoingJoinRequestUid,
                               @"resolution" : kMZInfopresenceCallResolutionCanceled
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionOutgoingRequestResolve, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceSessionOutgoingRequestResolve responseType:@"psa?declined"]])
  {
    NSDictionary *ingests = @{ @"uid" : infopresenceOutgoingJoinRequestUid,
                               @"resolution" : kMZInfopresenceCallResolutionDeclined
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceSessionOutgoingRequestResolve, @"psa"]
                                  ingests:ingests];
  }
		
#pragma mark - Participants joined/left delta proteins
  
  // Participants Joining
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceParticipantJoin responseType:@"psa?participant"]])
  {
    NSDictionary *ingests = @{ @"uid" : remoteParticipant1[@"uid"],
                               @"display-name" : remoteParticipant1[@"display-name"],
                               @"type" : @"remote-participant"
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceParticipantJoin, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceParticipantJoin responseType:@"psa?participant2"]])
  {
    NSDictionary *ingests = @{ @"uid" : remoteParticipant2[@"uid"],
                               @"display-name" : remoteParticipant2[@"display-name"],
                               @"type" : @"remote-participant"
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceParticipantJoin, @"psa"]
                                  ingests:ingests];
  }
  // Rooms Joining
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceParticipantJoin responseType:@"psa?room"]])
  {
    NSDictionary *ingests = @{ @"uid" : infopresenceParticipant1,
                               @"type" : @"room"
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceParticipantJoin, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceParticipantJoin responseType:@"psa?room2"]])
  {
    NSDictionary *ingests = @{ @"uid" : infopresenceParticipant2,
                               @"type" : @"room"
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceParticipantJoin, @"psa"]
                                  ingests:ingests];
  }
  
  // Participants Leaving
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceParticipantLeave responseType:@"psa?participant"]])
  {
    NSDictionary *ingests = @{ @"uid" : remoteParticipant1[@"uid"],
                               @"display-name" : remoteParticipant1[@"display-name"],
                               @"type" : @"remote-participant"
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceParticipantLeave, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceParticipantLeave responseType:@"psa?participant2"]])
  {
    NSDictionary *ingests = @{ @"uid" : remoteParticipant2[@"uid"],
                               @"display-name" : remoteParticipant2[@"display-name"],
                               @"type" : @"remote-participant"
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceParticipantLeave, @"psa"]
                                  ingests:ingests];
  }
  // Rooms Leaving
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceParticipantLeave responseType:@"psa?room"]])
  {
    NSDictionary *ingests = @{ @"uid" : infopresenceParticipant1,
                               @"type" : @"room"
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceParticipantLeave, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceParticipantLeave responseType:@"psa?room2"]])
  {
    NSDictionary *ingests = @{ @"uid" : infopresenceParticipant2,
                               @"type" : @"room"
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceParticipantLeave, @"psa"]
                                  ingests:ingests];
  }
  
  
#pragma mark - Infopresence inivitation proteins
  
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceOutgoingInvite responseType:@"psa"]])
  {
    NSDictionary *ingests = @{ @"uid" : infopresenceOutgoingInviteUid,
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceOutgoingInvite, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceOutgoingInvite responseType:@"response?error"]])
  {
    NSDictionary *ingests = @{ @"uid" : infopresenceOutgoingInviteUid,
                               @"summary" : @"Infopresence Unavailable",
                               @"description" : @"An Infopresence session is pending."
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceOutgoingInvite, @"response", @"error"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceOutgoingInviteResolve responseType:@"psa?accepted"]])
  {
    NSDictionary *ingests = @{ @"uid" : infopresenceOutgoingInviteUid,
                               @"resolution" : kMZInfopresenceCallResolutionAccepted
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceOutgoingInviteResolve, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceOutgoingInviteResolve responseType:@"psa?declined"]])
  {
    NSDictionary *ingests = @{ @"uid" : infopresenceOutgoingInviteUid,
                               @"resolution" : kMZInfopresenceCallResolutionDeclined
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceOutgoingInviteResolve, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceOutgoingInviteResolve responseType:@"psa?time-out"]])
  {
    NSDictionary *ingests = @{ @"uid" : infopresenceOutgoingInviteUid,
                               @"resolution" : kMZInfopresenceCallResolutionTimeOut
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceOutgoingInviteResolve, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceOutgoingInviteResolve responseType:@"psa?canceled"]])
  {
    NSDictionary *ingests = @{ @"uid" : infopresenceOutgoingInviteUid,
                               @"resolution" : kMZInfopresenceCallResolutionCanceled
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceOutgoingInviteResolve, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceIncomingInvite responseType:@"psa"]])
  {
    NSDictionary *ingests = @{ @"uid" : infopresenceIncomingInviteUid,
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceIncomingInvite, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceIncomingInviteResolve responseType:@"psa?accepted"]])
  {
    NSDictionary *ingests = @{ @"uid" : infopresenceIncomingInviteUid,
                               @"resolution" : kMZInfopresenceCallResolutionAccepted
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceIncomingInviteResolve, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceIncomingInviteResolve responseType:@"psa?declined"]])
  {
    NSDictionary *ingests = @{ @"uid" : infopresenceIncomingInviteUid,
                               @"resolution" : kMZInfopresenceCallResolutionDeclined
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceIncomingInviteResolve, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceIncomingInviteResolve responseType:@"psa?time-out"]])
  {
    NSDictionary *ingests = @{ @"uid" : infopresenceIncomingInviteUid,
                               @"resolution" : kMZInfopresenceCallResolutionTimeOut
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceIncomingInviteResolve, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameInfopresenceIncomingInviteResolve responseType:@"psa?canceled"]])
  {
    NSDictionary *ingests = @{ @"uid" : infopresenceIncomingInviteUid,
                               @"resolution" : kMZInfopresenceCallResolutionCanceled
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameInfopresenceIncomingInviteResolve, @"psa"]
                                  ingests:ingests];
  }

#pragma mark - Infopresence Participants List (4.0 API)

  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameParticipantList responseType:@"psa"]])
  {
    NSDictionary *ingests = @{ @"local-room" : meetingRoomParticipantLocal,
                               @"remote-rooms" : @[meetingRoomParticipant1, meetingRoomParticipant2],
                               @"cloud-participants": @[meetingParticipantRemoter]
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameParticipantList, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameParticipantList responseType:@"psa?local-and-remote-participants"]])
  {
    NSDictionary *ingests = @{ @"local-room" : meetingRoomParticipantLocal,
                               @"remote-rooms" : @[meetingRoomParticipant1, meetingRoomParticipant2],
                               @"cloud-participants": @[]
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameParticipantList, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameParticipantList responseType:@"psa?local-and-cloud-participants"]])
  {
    NSDictionary *ingests = @{ @"local-room" : meetingRoomParticipantLocal,
                               @"remote-rooms" : @[],
                               @"cloud-participants": @[meetingParticipantRemoter]
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameParticipantList, @"psa"]
                                  ingests:ingests];
  }
  else if ([key isEqual:[self proteinKeyForTransactionName:MZTransactionNameParticipantList responseType:@"psa?only-local-participants"]])
  {
    NSDictionary *ingests = @{ @"local-room" : meetingRoomParticipantLocal,
                               @"remote-rooms" : @[],
                               @"cloud-participants": @[]
                               };
    return [OBProtein proteinWithDescrips:@[MZTransactionNameParticipantList, @"psa"]
                                  ingests:ingests];
  }

  return nil;
}


-(id) parameterForKey:(NSString*)key
{
  return [constructor.canonicalParameters objectForKey:key];
}


-(NSString *) proteinKeyForTransactionName:(NSString*)transactionName responseType:(NSString*)responseType
{
  if (responseType)
    return [NSString stringWithFormat:@"ob.mezz.client.%@?%@", transactionName, responseType];
  else
    return [NSString stringWithFormat:@"ob.mezz.client.%@?psa", transactionName];
}

@end
