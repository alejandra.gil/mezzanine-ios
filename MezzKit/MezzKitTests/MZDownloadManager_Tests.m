//
//  MZDownloadManager_Tests.m
//  MezzKit
//
//  Created by miguel on 28/11/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MezzKit.h"

@interface MZDownloadManager (Internal)

- (BOOL)validateImageData:(NSData *)imageData type:(NSString *)encodingName;

@end

@interface MZDownloadManager_Tests : XCTestCase

@end

@implementation MZDownloadManager_Tests

- (void)setUp
{
  [super setUp];
  // Put setup code here. This method is called before the invocation of each test method in the class.

  [MZAssetCache setSharedCache:[[MZAssetCache alloc] init]];
}

- (void)tearDown
{
  [MZAssetCache setSharedCache:nil];

  // Put teardown code here. This method is called after the invocation of each test method in the class.
  [super tearDown];
}

- (void)testDownloadManagerOK
{
  NSBundle *bundle = [NSBundle bundleForClass:[self class]];
  NSString *path = [bundle pathForResource:@"Test-Image" ofType:@"png"];
  NSURL *url = [NSURL fileURLWithPath:path];

  NSObject *targetObject = [NSObject new];
  __block MZImage *imageTest = nil;

  MZDownloadManager *downloadManager = [MZDownloadManager sharedLoader];
  [downloadManager unloadImageFromDiskCacheWithURL:url];
  [downloadManager unloadImageFromMemoryCacheWithURL:url];

  MZImage *imageFromInDiskCache = [downloadManager cachedImageInDiskForURL:url];
  MZImage *imageFromMemoryCache = [downloadManager cachedImageForURL:url];
  XCTAssertNil(imageFromInDiskCache, @"Should be nil");
  XCTAssertNil(imageFromMemoryCache, @"Should be nil");

  __block NSNumber *loadingFinishBoolNumber = @NO;
  NSPredicate *loadingFinishedPredicate = [NSPredicate predicateWithFormat:@"boolValue == YES"];
  __block XCTestExpectation *expectation = [self expectationForPredicate:loadingFinishedPredicate evaluatedWithObject:loadingFinishBoolNumber handler:nil];

  [downloadManager loadImageWithURL:url atTarget:targetObject success:^(MZImage *image) {
    imageTest = image;
    XCTAssertNotNil(imageTest, @"Should not be nil");
    loadingFinishBoolNumber = @YES;
    [expectation fulfill];
  } error:^(NSError *error) {
    XCTFail(@"Should download an image");
    loadingFinishBoolNumber = @YES;
    [expectation fulfill];
  }];

  [self waitForExpectationsWithTimeout:5.0 handler:nil];
}


- (void)testDownloadManagerFail
{
  NSURL *url = [NSURL fileURLWithPath:@"Invalid/Test-Image/Path/image.png"];

  NSObject *targetObject = [NSObject new];
  __block MZImage *imageTest = nil;

  MZDownloadManager *downloadManager = [MZDownloadManager sharedLoader];

  MZImage *imageFromInDiskCache = [downloadManager cachedImageInDiskForURL:url];
  MZImage *imageFromMemoryCache = [downloadManager cachedImageForURL:url];
  XCTAssertNil(imageFromInDiskCache, @"Should be nil");
  XCTAssertNil(imageFromMemoryCache, @"Should be nil");

  __block NSNumber *loadingFinishBoolNumber = @NO;
  NSPredicate *loadingFinishedPredicate = [NSPredicate predicateWithFormat:@"boolValue == YES"];
  __block XCTestExpectation *expectation = [self expectationForPredicate:loadingFinishedPredicate evaluatedWithObject:loadingFinishBoolNumber handler:nil];

  [downloadManager loadImageWithURL:url atTarget:targetObject success:^(MZImage *image) {
    XCTFail(@"Should not download an image");
    loadingFinishBoolNumber = @YES;
    [expectation fulfill];
  } error:^(NSError *error) {
    XCTAssertNil(imageTest, @"Should be nil");
    loadingFinishBoolNumber = @YES;
    [expectation fulfill];
  }];

  [self waitForExpectationsWithTimeout:5.0 handler:nil];
}


- (void)testDownloadManagerOnlyInMemoryCache
{
  NSBundle *bundle = [NSBundle bundleForClass:[self class]];
  NSString *path = [bundle pathForResource:@"Test-Image" ofType:@"png"];
  NSURL *url = [NSURL fileURLWithPath:path];

  NSObject *targetObject = [NSObject new];
  __block MZImage *imageTest = nil;

  MZDownloadManager *downloadManager = [MZDownloadManager sharedLoader];
  [downloadManager unloadImageFromDiskCacheWithURL:url];
  [downloadManager unloadImageFromMemoryCacheWithURL:url];

  MZImage *imageFromInDiskCache = [downloadManager cachedImageInDiskForURL:url];
  MZImage *imageFromMemoryCache = [downloadManager cachedImageForURL:url];
  XCTAssertNil(imageFromInDiskCache, @"Should be nil");
  XCTAssertNil(imageFromMemoryCache, @"Should be nil");

  __block NSNumber *loadingFinishBoolNumber = @NO;
  NSPredicate *loadingFinishedPredicate = [NSPredicate predicateWithFormat:@"boolValue == YES"];
  __block XCTestExpectation *expectation = [self expectationForPredicate:loadingFinishedPredicate evaluatedWithObject:loadingFinishBoolNumber handler:nil];

  [downloadManager loadImageWithURL:url atTarget:targetObject success:^(MZImage *image) {
    imageTest = image;
    XCTAssertNotNil(imageTest, @"Should not be nil");
    loadingFinishBoolNumber = @YES;
    [expectation fulfill];
  } error:^(NSError *error) {
    XCTFail(@"Should download an image");
    loadingFinishBoolNumber = @YES;
    [expectation fulfill];
  } canceled:nil cachingOption:MZDownloadCacheTypeOnlyInMemory];

  [self waitForExpectationsWithTimeout:5.0 handler:nil];

  imageFromInDiskCache = [downloadManager cachedImageInDiskForURL:url];
  imageFromMemoryCache = [downloadManager cachedImageForURL:url];

  XCTAssertNil(imageFromInDiskCache, @"Should be nil");
  XCTAssertNotNil(imageFromMemoryCache, @"Should not be nil");
}


- (void)testDownloadManagerOnlyInDiskCache
{
  NSBundle *bundle = [NSBundle bundleForClass:[self class]];
  NSString *path = [bundle pathForResource:@"Test-Image" ofType:@"png"];
  NSURL *url = [NSURL fileURLWithPath:path];

  NSObject *targetObject = [NSObject new];
  __block MZImage *imageTest = nil;

  MZDownloadManager *downloadManager = [MZDownloadManager sharedLoader];
  [downloadManager unloadImageFromDiskCacheWithURL:url];
  [downloadManager unloadImageFromMemoryCacheWithURL:url];

  MZImage *imageFromInDiskCache = [downloadManager cachedImageInDiskForURL:url];
  MZImage *imageFromMemoryCache = [downloadManager cachedImageForURL:url];
  XCTAssertNil(imageFromInDiskCache, @"Should be nil");
  XCTAssertNil(imageFromMemoryCache, @"Should be nil");

  __block NSNumber *loadingFinishBoolNumber = @NO;
  NSPredicate *loadingFinishedPredicate = [NSPredicate predicateWithFormat:@"boolValue == YES"];
  __block XCTestExpectation *expectation = [self expectationForPredicate:loadingFinishedPredicate evaluatedWithObject:loadingFinishBoolNumber handler:nil];

  [downloadManager loadImageWithURL:url atTarget:targetObject success:^(MZImage *image) {
    imageTest = image;
    XCTAssertNotNil(imageTest, @"Should not be nil");
    loadingFinishBoolNumber = @YES;
    [expectation fulfill];
  } error:^(NSError *error) {
    XCTFail(@"Should download an image");
    loadingFinishBoolNumber = @YES;
    [expectation fulfill];
  } canceled:nil cachingOption:MZDownloadCacheTypeOnlyInDisk];

  [self waitForExpectationsWithTimeout:5.0 handler:nil];

  imageFromInDiskCache = [downloadManager cachedImageInDiskForURL:url];
  imageFromMemoryCache = [downloadManager cachedImageForURL:url];

  XCTAssertNotNil(imageFromInDiskCache, @"Should be nil");
  XCTAssertNil(imageFromMemoryCache, @"Should not be nil");
}


- (void)testDownloadManagerPauseStates
{
  NSBundle *bundle = [NSBundle bundleForClass:[self class]];
  NSString *path = [bundle pathForResource:@"Test-Image" ofType:@"png"];
  NSURL *url = [NSURL fileURLWithPath:path];

  NSObject *targetObject = [NSObject new];
  __block MZImage *imageTest = nil;

  MZDownloadManager *downloadManager = [MZDownloadManager sharedLoader];
  [downloadManager unloadImageFromDiskCacheWithURL:url];
  [downloadManager unloadImageFromMemoryCacheWithURL:url];

  MZImage *imageFromInDiskCache = [downloadManager cachedImageInDiskForURL:url];
  MZImage *imageFromMemoryCache = [downloadManager cachedImageForURL:url];
  XCTAssertNil(imageFromInDiskCache, @"Should be nil");
  XCTAssertNil(imageFromMemoryCache, @"Should be nil");

  [MZDownloadManager sharedLoader].pauseDownloads = YES;

  __block NSNumber *loadFailFinishBoolNumber = @NO;
  NSPredicate *loadFailFinishedPredicate = [NSPredicate predicateWithFormat:@"boolValue == YES"];
  __block XCTestExpectation *expectation = [self expectationForPredicate:loadFailFinishedPredicate evaluatedWithObject:loadFailFinishBoolNumber handler:nil];

  [downloadManager loadImageWithURL:url atTarget:targetObject success:^(MZImage *image) {
    XCTFail(@"Should not download an image");
    loadFailFinishBoolNumber = @YES;
    [expectation fulfill];
  } error:^(NSError *error) {
    XCTAssertNil(imageTest, @"Should be nil");
    loadFailFinishBoolNumber = @YES;
    [expectation fulfill];
  }];

  [self waitForExpectationsWithTimeout:5.0 handler:nil];

  [MZDownloadManager sharedLoader].pauseDownloads = NO;

  __block NSNumber *loadingFinishBoolNumber = @NO;
  NSPredicate *loadingFinishedPredicate = [NSPredicate predicateWithFormat:@"boolValue == YES"];
  __block XCTestExpectation *loadingFinishExpectation = [self expectationForPredicate:loadingFinishedPredicate evaluatedWithObject:loadingFinishBoolNumber handler:nil];

  [downloadManager loadImageWithURL:url atTarget:targetObject success:^(MZImage *image) {
    imageTest = image;
    XCTAssertNotNil(imageTest, @"Should not be nil");
    loadingFinishBoolNumber = @YES;
    [loadingFinishExpectation fulfill];
  } error:^(NSError *error) {
    XCTFail(@"Should download an image");
    loadingFinishBoolNumber = @YES;
    [loadingFinishExpectation fulfill];
  }];

  [self waitForExpectationsWithTimeout:5.0 handler:nil];
}


- (void)testValidPNG
{
  NSBundle *bundle = [NSBundle bundleForClass:[self class]];
  NSString *path = [bundle pathForResource:@"Test-Image" ofType:@"png"];
  NSData *imageData = [NSData dataWithContentsOfFile:path];

  MZDownloadManager *downloadManager = [MZDownloadManager sharedLoader];
  XCTAssertTrue ([downloadManager validateImageData:imageData type:@"image/png"]);
  XCTAssertFalse ([downloadManager validateImageData:imageData type:@"image/jpeg"]);
}

- (void)testInvalidPNG
{
  NSBundle *bundle = [NSBundle bundleForClass:[self class]];
  NSString *path = [bundle pathForResource:@"Test-Image-Corrupted" ofType:@"png"];
  NSData *imageData = [NSData dataWithContentsOfFile:path];

  MZDownloadManager *downloadManager = [MZDownloadManager sharedLoader];
  XCTAssertFalse ([downloadManager validateImageData:imageData type:@"image/png"]);
  XCTAssertFalse ([downloadManager validateImageData:imageData type:@"image/jpeg"]);
}

- (void)testValidJPEG
{
  NSBundle *bundle = [NSBundle bundleForClass:[self class]];
  NSString *path = [bundle pathForResource:@"Test-Image" ofType:@"jpg"];
  NSData *imageData = [NSData dataWithContentsOfFile:path];

  MZDownloadManager *downloadManager = [MZDownloadManager sharedLoader];
  XCTAssertFalse ([downloadManager validateImageData:imageData type:@"image/png"]);
  XCTAssertTrue ([downloadManager validateImageData:imageData type:@"image/jpeg"]);
}

- (void)testInvalidJPEG
{
  NSBundle *bundle = [NSBundle bundleForClass:[self class]];
  NSString *path = [bundle pathForResource:@"Test-Image-Corrupted" ofType:@"jpg"];
  NSData *imageData = [NSData dataWithContentsOfFile:path];

  MZDownloadManager *downloadManager = [MZDownloadManager sharedLoader];
  XCTAssertFalse ([downloadManager validateImageData:imageData type:@"image/png"]);
  XCTAssertFalse ([downloadManager validateImageData:imageData type:@"image/jpeg"]);
}

@end
