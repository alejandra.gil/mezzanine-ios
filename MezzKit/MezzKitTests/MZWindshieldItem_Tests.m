//
//  MZWindshieldItem_Tests.m
//  MezzKit
//
//  Created by miguel on 21/10/15.
//  Copyright (c) 2015 Oblong Industries. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MZWindshieldItem.h"
#import "MZSystemModel.h"

@interface MZWindshieldItem_Tests : XCTestCase
{
  MZWindshieldItem *_item;
}

@end

@implementation MZWindshieldItem_Tests

- (NSString*)itemUidForIndex:(NSInteger)i
{
  return [NSString stringWithFormat:@"ws-%ld", (long)i];
}

- (NSString*)itemAssetUidForIndex:(NSInteger)i
{
  return [NSString stringWithFormat:@"as-%ld", (long)i];
}

- (void)setUp
{
  [super setUp];
  _item = [[MZWindshieldItem alloc] init];
  _item.uid = [self itemUidForIndex:0];
  _item.assetUid = [self itemAssetUidForIndex:0];
  _item.feldRelativeCoordinates = @{@"feld-id": @"main",
                                    @"norm": @0,
                                    @"over": @0,
                                    @"up": @0};
  _item.feldRelativeSize = @{@"feld-id": @"main",
                             @"aspect-ratio": @(16.0/9.0),
                             @"scale": @1.0};
}

- (void)tearDown
{
  _item = nil;
  [super tearDown];
}

#pragma mark - Mullion Replacement Tests

- (void)testReplaceLeftMullionRelativeValuesWithMainRelativeOnesWithTransformDictionary
{
  NSDictionary *transformDictionary = @{@"feld-relative-coords":@{@"feld-id": @"left-mullion",
                                                                  @"norm": @0,
                                                                  @"over": @0.2,
                                                                  @"up": @0.4},
                                        @"feld-relative-size":@{@"feld-id": @"main",
                                                                @"aspect-ratio": @(16.0/9.0),
                                                                @"scale": @1.0}};

  [_item updateWithTransformDictionary:transformDictionary];

  XCTAssertTrue([_item.feldRelativeCoordinates[@"feld-id"] isEqualToString:@"main"], @"Feld id assigned should be main but it is %@", _item.feldRelativeCoordinates[@"feld-id"]);
  XCTAssertEqual([_item.feldRelativeCoordinates[@"over"] doubleValue], -0.5, @"Over should be -0.5 but it is %@", _item.feldRelativeCoordinates[@"over"]);
  XCTAssertEqual([_item.feldRelativeCoordinates[@"up"] doubleValue], 0.4, @"Up should be 0.4 but it is %@", _item.feldRelativeCoordinates[@"up"]);
}

- (void)testReplaceRightMullionRelativeValuesWithMainRelativeOnesWithTransformDictionary
{
  NSDictionary *transformDictionary = @{@"feld-relative-coords":@{@"feld-id": @"right-mullion",
                                                                  @"norm": @0,
                                                                  @"over": @0.2,
                                                                  @"up": @0.4},
                                        @"feld-relative-size":@{@"feld-id": @"main",
                                                                @"aspect-ratio": @(16.0/9.0),
                                                                @"scale": @1.0}};

  [_item updateWithTransformDictionary:transformDictionary];

  XCTAssertTrue([_item.feldRelativeCoordinates[@"feld-id"] isEqualToString:@"main"], @"Feld id assigned should be main but it is %@", _item.feldRelativeCoordinates[@"feld-id"]);
  XCTAssertEqual([_item.feldRelativeCoordinates[@"over"] doubleValue], 0.5, @"Over should be 0.5 but it is %@", _item.feldRelativeCoordinates[@"over"]);
  XCTAssertEqual([_item.feldRelativeCoordinates[@"up"] doubleValue], 0.4, @"Up should be 0.4 but it is %@", _item.feldRelativeCoordinates[@"up"]);
}

- (void)testReplaceLeftMullionRelativeValuesWithMainRelativeOnesWithFeldRelCoordsDictionary
{
  NSDictionary *feldRelCoords = @{@"feld-id": @"left-mullion",
                                  @"norm": @0,
                                  @"over": @0.2,
                                  @"up": @0.4};

  _item.feldRelativeCoordinates = feldRelCoords;

  XCTAssertTrue([_item.feldRelativeCoordinates[@"feld-id"] isEqualToString:@"main"], @"Feld id assigned should be main but it is %@", _item.feldRelativeCoordinates[@"feld-id"]);
  XCTAssertEqual([_item.feldRelativeCoordinates[@"over"] doubleValue], -0.5, @"Over should be -0.5 but it is %@", _item.feldRelativeCoordinates[@"over"]);
  XCTAssertEqual([_item.feldRelativeCoordinates[@"up"] doubleValue], 0.4, @"Up should be 0.4 but it is %@", _item.feldRelativeCoordinates[@"up"]);
}

- (void)testReplaceRightMullionRelativeValuesWithMainRelativeOnesWithFeldRelCoordsDictionary
{
  NSDictionary *feldRelCoords = @{@"feld-id": @"right-mullion",
                                  @"norm": @0,
                                  @"over": @0.2,
                                  @"up": @0.4};

  _item.feldRelativeCoordinates = feldRelCoords;

  XCTAssertTrue([_item.feldRelativeCoordinates[@"feld-id"] isEqualToString:@"main"], @"Feld id assigned should be main but it is %@", _item.feldRelativeCoordinates[@"feld-id"]);
  XCTAssertEqual([_item.feldRelativeCoordinates[@"over"] doubleValue], 0.5, @"Over should be 0.5 but it is %@", _item.feldRelativeCoordinates[@"over"]);
  XCTAssertEqual([_item.feldRelativeCoordinates[@"up"] doubleValue], 0.4, @"Up should be 0.4 but it is %@", _item.feldRelativeCoordinates[@"up"]);
}

- (void)testReplaceLeftMullionWhenUpdateSurfaceAndFeldWithDictionaryUsingSystemModel
{
  MZFeld *feld = [MZFeld feldWithName:@"main"];
  MZSurface *surface = [[MZSurface alloc] initWithName:@"main"];
  [surface.felds addObject:feld];
  MZSystemModel *systemModel = [MZSystemModel new];
  [systemModel.surfaces addObject:surface];

  NSDictionary *transformDictionary = @{@"feld-relative-coords":@{@"feld-id": @"left-mullion",
                                                                  @"norm": @0,
                                                                  @"over": @0.2,
                                                                  @"up": @0.4}};

  [_item updateSurfaceAndFeldWithDictionary:transformDictionary usingSystemModel:systemModel];


  XCTAssertEqual(_item.feld, feld, @"Feld assigned to item should be main but it is %@", _item.feld.name);
}

- (void)testReplaceRightMullionWhenUpdateSurfaceAndFeldWithDictionaryUsingSystemModel
{
  MZFeld *feld = [MZFeld feldWithName:@"main"];
  MZSurface *surface = [[MZSurface alloc] initWithName:@"main"];
  [surface.felds addObject:feld];
  MZSystemModel *systemModel = [MZSystemModel new];
  [systemModel.surfaces addObject:surface];

  NSDictionary *transformDictionary = @{@"feld-relative-coords":@{@"feld-id": @"right-mullion",
                                                                  @"norm": @0,
                                                                  @"over": @0.2,
                                                                  @"up": @0.4}};

  [_item updateSurfaceAndFeldWithDictionary:transformDictionary usingSystemModel:systemModel];


  XCTAssertEqual(_item.feld, feld, @"Feld assigned to item should be main but it is %@", _item.feld.name);
}

@end
