//
//  MockMezzanineNavigationController.h
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 02/09/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

#import "MezzanineNavigationController.h"

@interface PushViewControllerInvocation : NSObject
@end

@interface PushViewControllerWithTransitionViewInvocation : NSObject
@end

@interface PopViewControllerWithTransitionViewInvocations : NSObject
@end

@interface MockMezzanineNavigationController : MezzanineNavigationController

@property (nonatomic, strong) NSMutableArray<PushViewControllerInvocation *> *pushViewControllerInvocations;
@property (nonatomic, strong) NSMutableArray<PushViewControllerWithTransitionViewInvocation *> *pushViewControllerWithTransitionViewInvocations;
@property (nonatomic, strong) NSMutableArray<PopViewControllerWithTransitionViewInvocations *> *popViewControllerWithTransitionViewInvocations;

@end
