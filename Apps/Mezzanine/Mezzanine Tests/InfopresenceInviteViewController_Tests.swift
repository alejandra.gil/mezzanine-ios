//
//  InfopresenceInviteViewController_Tests.swift
//  Mezzanine
//
//  Created by miguel on 8/2/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

import XCTest
import UIKit
@testable import Mezzanine

class InfopresenceInviteViewController_Tests: XCTestCase {

  var viewController: InfopresenceInviteViewController?
  var systemModel: MZSystemModel?
  var communicator: MZCommunicator?
  var infopresenceMenuController: InfopresenceMenuController?

  override func setUp() {
    super.setUp()

    systemModel = MZSystemModel()
    communicator = MZCommunicator()
    communicator?.systemModel = systemModel
    infopresenceMenuController = InfopresenceMenuController()

    let remoteMezzes = NSMutableArray()
    for i in 0...6 {
      let remoteMezz = createRemoteMezz(i)
      remoteMezzes.add(remoteMezz)
    }
    systemModel!.remoteMezzes = remoteMezzes

    let myMezz = MZMezzanine()
    myMezz.uid = "My Mezzanine uid"
    myMezz.name = "My Mezzanine name"
    myMezz.version = "3.0"
    systemModel?.infopresence.rooms.add(myMezz)

    viewController = InfopresenceInviteViewController(systemModel: systemModel!, communicator: communicator!, infopresenceMenuController: infopresenceMenuController!)

    // Force view initialization
    _ = viewController?.view
    viewController?.viewWillAppear(false)
  }

  func createRemoteMezz(_ i: Int) -> MZRemoteMezz {
    let remoteMezz = MZRemoteMezz()
    remoteMezz.uid = String(format: "%d", arguments: [i])
    remoteMezz.name = String(format: "Mezzanine %d", arguments: [i])
    remoteMezz.online = true
    remoteMezz.version = "3.0"
    remoteMezz.collaborationState = .notInCollaboration
    return remoteMezz
  }

  override func tearDown() {
      // Put teardown code here. This method is called after the invocation of each test method in the class.
      super.tearDown()
  }

  func testSelectableRemoteMezzes() {
    let vc = viewController! as InfopresenceInviteViewController
    var item0: Bool = vc.indexPathIsSelectable(IndexPath(item: 0, section: 0)) as Bool
    var item1: Bool = vc.indexPathIsSelectable(IndexPath(item: 1, section: 0)) as Bool
    var item2: Bool = vc.indexPathIsSelectable(IndexPath(item: 2, section: 0)) as Bool
    var item3: Bool = vc.indexPathIsSelectable(IndexPath(item: 3, section: 0)) as Bool

    XCTAssertTrue(item0, "Remote mezz #0 should be selectable")
    XCTAssertTrue(item1, "Remote mezz #1 should be selectable")
    XCTAssertTrue(item2, "Remote mezz #2 should be selectable")
    XCTAssertTrue(item3, "Remote mezz #3 should be selectable")

    (systemModel!.remoteMezzes.object(at: 0) as! MZRemoteMezz).online = false
    (systemModel!.remoteMezzes.object(at: 1) as! MZRemoteMezz).collaborationState = .invited
    (systemModel!.remoteMezzes.object(at: 2) as! MZRemoteMezz).collaborationState = .joining
    (systemModel!.remoteMezzes.object(at: 3) as! MZRemoteMezz).collaborationState = .inCollaboration

    item0 = vc.indexPathIsSelectable(IndexPath(item: 0, section: 0)) as Bool
    item1 = vc.indexPathIsSelectable(IndexPath(item: 1, section: 0)) as Bool
    item2 = vc.indexPathIsSelectable(IndexPath(item: 2, section: 0)) as Bool
    item3 = vc.indexPathIsSelectable(IndexPath(item: 3, section: 0)) as Bool

    XCTAssertFalse(item0, "Remote mezz #0 should not be selectable")
    XCTAssertTrue(item1, "Remote mezz #1 should be selectable")
    XCTAssertFalse(item2, "Remote mezz #2 should not be selectable")
    XCTAssertFalse(item3, "Remote mezz #3 should not be selectable")
  }

  func selectRow(_ row: Int) {
    let vc = viewController! as InfopresenceInviteViewController
    if vc.tableView(vc.tableView, willSelectRowAt: IndexPath(item: row, section: 0)) != nil {
      vc.tableView.selectRow(at: IndexPath(item: row, section: 0), animated: false, scrollPosition: UITableViewScrollPosition.top)
      vc.tableView(vc.tableView, didDeselectRowAt: IndexPath(item: row, section: 0))
    }
  }

  func deselectRow(_ row: Int) {
    let vc = viewController! as InfopresenceInviteViewController
    vc.tableView.deselectRow(at: IndexPath(item: row, section: 0), animated: false)
    vc.tableView(vc.tableView, didDeselectRowAt: IndexPath(item: row, section: 0))
  }

  func checkInviteButton(_ selected: Int, maximumErrorEnabled: Bool, maxInvitations: Int) {
    let vc = viewController! as InfopresenceInviteViewController

    if selected > 0 {
      XCTAssertTrue(vc.inviteButton.isEnabled, "Invite Button should be enabled")
    }
    else {
      XCTAssertFalse(vc.inviteButton.isEnabled, "Invite Button should be disabled")
    }

    switch selected {
    case 0:
      XCTAssertEqual(vc.inviteButton.title(for: [UIControlState.normal]), "Infopresence Invite Mezzanines Button Title None".localized.uppercased(), "Invite button should show: "+"Infopresence Invite Mezzanines Button Title None".localized)
      break
    case 1:
      XCTAssertEqual(vc.inviteButton.title(for: [UIControlState.normal]), "Infopresence Invite Mezzanines Button Title Single".localized.uppercased(), "Invite button should show: "+"Infopresence Invite Mezzanines Button Title Single".localized)
      break
    default:
      XCTAssertEqual(vc.inviteButton.title(for: [UIControlState.normal]), String(format: "Infopresence Invite Mezzanines Button Title Mutiple".localized, selected).uppercased(), "Invite button should show: "+String(format: "Infopresence Invite Mezzanines Button Title Mutiple".localized, selected))
    }

    if maximumErrorEnabled == true {
      XCTAssertFalse(vc.maximumInvitationsErrorView.isHidden, "Maximum Invitations View should be visible now")
      if maxInvitations > 0 {
        XCTAssertEqual(vc.maximumInvitationsErrorLabel.text, String(format: "Infopresence Invite Maximum Mezzanines Reached Error Message".localized, maxInvitations).uppercased(), "Maximum Invitations Label should be: "+String(format: "Infopresence Invite Maximum Mezzanines Reached Error Message".localized, maxInvitations))
      }
      else {
        XCTAssertEqual(vc.maximumInvitationsErrorLabel.text, "Infopresence Invite No More Mezzanines Reached Error Message".localized.uppercased(), "Maximum Invitations Label should be: "+"Infopresence Invite No More Mezzanines Reached Error Message")
      }
    }
    else {
      XCTAssertTrue(vc.maximumInvitationsErrorView.isHidden, "Maximum Invitations View should be hidden")
    }
  }



  func testSelectAndDeselectRowsToInviteWithoutInfopresenceSession() {
    checkInviteButton(0, maximumErrorEnabled: false, maxInvitations: 3)

    selectRow(0)
    checkInviteButton(1, maximumErrorEnabled: false, maxInvitations: 3)

    selectRow(1)
    checkInviteButton(2, maximumErrorEnabled: false, maxInvitations: 3)

    selectRow(2)
    checkInviteButton(3, maximumErrorEnabled: false, maxInvitations: 3)

    selectRow(3)
    checkInviteButton(3, maximumErrorEnabled: true, maxInvitations: 3)

    selectRow(4)
    checkInviteButton(3, maximumErrorEnabled: true, maxInvitations: 3)

    selectRow(5)
    checkInviteButton(3, maximumErrorEnabled: true, maxInvitations: 3)

    selectRow(6)
    checkInviteButton(3, maximumErrorEnabled: true, maxInvitations: 3)

    deselectRow(0)
    checkInviteButton(3, maximumErrorEnabled: true, maxInvitations: 3)

    deselectRow(1)
    checkInviteButton(3, maximumErrorEnabled: true, maxInvitations: 3)

    deselectRow(2)
    checkInviteButton(3, maximumErrorEnabled: true, maxInvitations: 3)

    deselectRow(3)
    checkInviteButton(3, maximumErrorEnabled: false, maxInvitations: 3)

    deselectRow(4)
    checkInviteButton(2, maximumErrorEnabled: false, maxInvitations: 3)

    deselectRow(5)
    checkInviteButton(1, maximumErrorEnabled: false, maxInvitations: 3)

    deselectRow(6)
    checkInviteButton(0, maximumErrorEnabled: false, maxInvitations: 3)
  }


  func testSelectAndDeselectRowsToInviteWhileInInfopresenceSessionWithOneMezz() {

    let remoteMezz = systemModel?.remoteMezzes.object(at: 0) as! MZRemoteMezz
    remoteMezz.collaborationState = .inCollaboration
    systemModel?.infopresence.rooms.add(remoteMezz)

    checkInviteButton(0, maximumErrorEnabled: false, maxInvitations: 2)

    selectRow(0)
    checkInviteButton(0, maximumErrorEnabled: false, maxInvitations: 2)

    selectRow(1)
    checkInviteButton(1, maximumErrorEnabled: false, maxInvitations: 2)

    selectRow(2)
    checkInviteButton(2, maximumErrorEnabled: false, maxInvitations: 2)

    selectRow(3)
    checkInviteButton(2, maximumErrorEnabled: true, maxInvitations: 2)

    selectRow(4)
    checkInviteButton(2, maximumErrorEnabled: true, maxInvitations: 2)

    selectRow(5)
    checkInviteButton(2, maximumErrorEnabled: true, maxInvitations: 2)

    selectRow(6)
    checkInviteButton(2, maximumErrorEnabled: true, maxInvitations: 2)

    deselectRow(0)
    checkInviteButton(2, maximumErrorEnabled: true, maxInvitations: 2)

    deselectRow(1)
    checkInviteButton(2, maximumErrorEnabled: true, maxInvitations: 2)

    deselectRow(2)
    checkInviteButton(2, maximumErrorEnabled: true, maxInvitations: 2)

    deselectRow(3)
    checkInviteButton(2, maximumErrorEnabled: true, maxInvitations: 2)

    deselectRow(4)
    checkInviteButton(2, maximumErrorEnabled: false, maxInvitations: 2)

    deselectRow(5)
    checkInviteButton(1, maximumErrorEnabled: false, maxInvitations: 2)

    deselectRow(6)
    checkInviteButton(0, maximumErrorEnabled: false, maxInvitations: 2)
  }

  func testSelectAndDeselectRowsToInviteWhileInInfopresenceSessionWithThreeMezzanines() {

    var remoteMezz = systemModel?.remoteMezzes.object(at: 0) as! MZRemoteMezz
    remoteMezz.collaborationState = .inCollaboration
    systemModel?.infopresence.insert(remoteMezz, inRoomsAt: 1)
    remoteMezz = systemModel?.remoteMezzes.object(at: 1) as! MZRemoteMezz
    remoteMezz.collaborationState = .inCollaboration
    systemModel?.infopresence.insert(remoteMezz, inRoomsAt: 2)
    remoteMezz = systemModel?.remoteMezzes.object(at: 2) as! MZRemoteMezz
    remoteMezz.collaborationState = .inCollaboration
    systemModel?.infopresence.insert(remoteMezz, inRoomsAt: 3)

    checkInviteButton(0, maximumErrorEnabled: true, maxInvitations: 0)

    selectRow(0)
    checkInviteButton(0, maximumErrorEnabled: true, maxInvitations: 0)

    selectRow(1)
    checkInviteButton(0, maximumErrorEnabled: true, maxInvitations: 0)

    selectRow(2)
    checkInviteButton(0, maximumErrorEnabled: true, maxInvitations: 0)

    selectRow(3)
    checkInviteButton(0, maximumErrorEnabled: true, maxInvitations: 0)

    selectRow(4)
    checkInviteButton(0, maximumErrorEnabled: true, maxInvitations: 0)

    selectRow(5)
    checkInviteButton(0, maximumErrorEnabled: true, maxInvitations: 0)

    selectRow(6)
    checkInviteButton(0, maximumErrorEnabled: true, maxInvitations: 0)

    deselectRow(0)
    checkInviteButton(0, maximumErrorEnabled: true, maxInvitations: 0)

    deselectRow(1)
    checkInviteButton(0, maximumErrorEnabled: true, maxInvitations: 0)

    deselectRow(2)
    checkInviteButton(0, maximumErrorEnabled: true, maxInvitations: 0)

    deselectRow(3)
    checkInviteButton(0, maximumErrorEnabled: true, maxInvitations: 0)

    deselectRow(4)
    checkInviteButton(0, maximumErrorEnabled: true, maxInvitations: 0)

    deselectRow(5)
    checkInviteButton(0, maximumErrorEnabled: true, maxInvitations: 0)

    deselectRow(6)
    checkInviteButton(0, maximumErrorEnabled: true, maxInvitations: 0)
  }


  func testInviteButtonUpdateWhenSessionIsFullAfterRemoteMezzDisconnects() {

    var remoteMezz = systemModel?.remoteMezzes.object(at: 0) as! MZRemoteMezz
    remoteMezz.collaborationState = .inCollaboration
    systemModel?.infopresence.insert(remoteMezz, inRoomsAt: 1)
    remoteMezz = systemModel?.remoteMezzes.object(at: 1) as! MZRemoteMezz
    remoteMezz.collaborationState = .inCollaboration
    systemModel?.infopresence.insert(remoteMezz, inRoomsAt: 2)
    remoteMezz = systemModel?.remoteMezzes.object(at: 2) as! MZRemoteMezz
    remoteMezz.collaborationState = .inCollaboration
    systemModel?.infopresence.insert(remoteMezz, inRoomsAt: 3)

    checkInviteButton(0, maximumErrorEnabled: true, maxInvitations: 0)

    selectRow(3)
    checkInviteButton(0, maximumErrorEnabled: true, maxInvitations: 0)

    remoteMezz = systemModel?.remoteMezzes.object(at: 2) as! MZRemoteMezz
    remoteMezz.collaborationState = .notInCollaboration
    systemModel?.infopresence.removeObject(fromRooms: remoteMezz)

    checkInviteButton(1, maximumErrorEnabled: false, maxInvitations: 1)
  }

  func testInviteButtonUpdateWithAllSlotsTakenWhenViewAppears() {

    var remoteMezz = systemModel?.remoteMezzes.object(at: 0) as! MZRemoteMezz
    remoteMezz.collaborationState = .inCollaboration
    systemModel?.infopresence.rooms.add(remoteMezz)
    remoteMezz = systemModel?.remoteMezzes.object(at: 1) as! MZRemoteMezz
    remoteMezz.collaborationState = .inCollaboration
    systemModel?.infopresence.rooms.add(remoteMezz)
    remoteMezz = systemModel?.remoteMezzes.object(at: 2) as! MZRemoteMezz
    remoteMezz.collaborationState = .inCollaboration
    systemModel?.infopresence.rooms.add(remoteMezz)

    viewController = InfopresenceInviteViewController(systemModel: systemModel!, communicator: communicator!, infopresenceMenuController: infopresenceMenuController!)

    // Force view initialization
    _ = viewController?.view

    viewController?.viewWillAppear(false)

    checkInviteButton(0, maximumErrorEnabled: true, maxInvitations: 0)

    remoteMezz = systemModel?.remoteMezzes.object(at: 2) as! MZRemoteMezz
    remoteMezz.collaborationState = .notInCollaboration
    systemModel?.infopresence.removeObject(fromRooms: remoteMezz)

    checkInviteButton(0, maximumErrorEnabled: false, maxInvitations: 1)
  }
  
  func testcheckInviteButtonMaximumInvitesLabelStyledEqually() {
    let vc = viewController! as InfopresenceInviteViewController
    XCTAssertEqual(vc.inviteButton.titleLabel!.font.fontName, vc.maximumInvitationsErrorLabel.font.fontName)
  }
}
