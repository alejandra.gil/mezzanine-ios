//
//  BinCollectionViewController_Tests.swift
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 24/02/2017.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import XCTest

@testable import Mezzanine


class BinCollectionViewController_Tests: XCTestCase {
  
  var controller: BinCollectionViewController!
  
  override func setUp() {
    super.setUp()
    
    let mockSystemModel = MockSystemModel()
    let mockCommunicator = MockMZCommunicator()
    let entity = BinCollectionViewDataSource()
    let interactor = BinCollectionViewInteractor(systemModel: mockSystemModel.systemModel, communicator: mockCommunicator, entity: entity)
    
    controller = BinCollectionViewController(dataSource: BinCollectionViewDataSource())
    controller.interactor = interactor
    _ = controller.view
  }
  
  override func tearDown() {
    super.tearDown()
  }
  
  func testHeaderViewFitsWithCollectionView() {
    XCTAssertTrue(controller.headerViewController.view.frame.width == controller.collectionView.frame.width)
  }
  
  func testPopoverItem() {
    let item = MZPortfolioItem()
    let cell = PortfolioCollectionViewCell()
    let menuItems = [controller.addPlaceItemOnScreen(item),
                     controller.addPresentOrJumpToSlide("", index: 0),
                     controller.addLocalPreview(cell, item: item),
                     controller.addDeleteItem(item)]
    
    let popoverController = controller.addItemsAndShowPopover(menuItems, item: item, cell: cell)
    
    XCTAssertTrue(controller.currentPopover == popoverController)
    XCTAssertTrue((controller.currentPopover!.viewController as? ButtonMenuViewController)!.menuItems.count == menuItems.count)
  }
  
  func testPopoverLiveStreamInLivestreamSection() {
    let item = MZLiveStream()
    let cell = UICollectionViewCell()
    let menuItems = [controller.addLiveStreamHeaderTitle(""),
                     controller.addPlaceItemOnScreen(item),
                     controller.addLocalPreview(cell, item: item)]
    
    let popoverController = controller.addItemsAndShowPopover(menuItems, item: item, cell: cell)
    
    XCTAssertTrue(controller.currentPopover == popoverController)
    XCTAssertTrue((controller.currentPopover!.viewController as? ButtonMenuViewController)!.menuItems.count == menuItems.count)
  }
  
  func testPopoverLiveStreamInItemsSection() {
    let item = MZPortfolioItem()
    let cell = LiveStreamCollectionViewCell()
    let menuItems = [controller.addLiveStreamHeaderTitle(""),
                     controller.addPlaceItemOnScreen(item),
                     controller.addLocalPreview(cell, item: item),
                     controller.addDeleteItem(item)]
    
    let popoverController = controller.addItemsAndShowPopover(menuItems, item: item, cell: cell)
    
    XCTAssertTrue(controller.currentPopover == popoverController)
    XCTAssertTrue((controller.currentPopover!.viewController as? ButtonMenuViewController)!.menuItems.count == menuItems.count)
  }
}
 
