//
//  MezzanineAppDelegate_Tests.m
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 29/09/2016.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MezzanineAppDelegate.h"
#import "MezzanineAppDelegate_Private.h"

#import <OCMock/OCMock.h>

@interface MezzanineAppDelegate_Tests : XCTestCase

@property MezzanineAppDelegate* appDelegate;

@end


@implementation MezzanineAppDelegate_Tests

- (void)setUp
{
  [super setUp];
  _appDelegate = [MezzanineAppDelegate new];
}


- (void)tearDown
{
  [super tearDown];
}


- (void)testUniversalLinkingExpectedURLAndType
{
  NSURL *expectedURL = [NSURL URLWithString:@"https://mezz-in.com/m/1234"];
  NSUserActivity *userActivity = [[NSUserActivity alloc] initWithActivityType:NSUserActivityTypeBrowsingWeb];
  userActivity.webpageURL = expectedURL;
  id appDelegateMock = [OCMockObject partialMockForObject:_appDelegate];
  
  // We attempt to connect if userActivity type is not browsing
  [[appDelegateMock expect] attempToConnectFromURL:OCMOCK_ANY];
  [appDelegateMock application:[UIApplication sharedApplication] continueUserActivity:userActivity restorationHandler:^(NSArray * _Nullable restorableObjects) { }];
  [appDelegateMock verify];
  
  // if URL is correct method should return TRUE
  XCTAssertTrue([[appDelegateMock stub] andReturnValue: [NSNumber numberWithBool:[_appDelegate attempToConnectFromURL:expectedURL]]], @"URL is correct, method should return TRUE");
}

- (void)testUniversalLinkingUnexpectedType
{
  NSUserActivity *userActivity = [[NSUserActivity alloc] initWithActivityType:UIApplicationLaunchOptionsUserActivityTypeKey];
  id appDelegateMock = [OCMockObject partialMockForObject:_appDelegate];
  
  // We shouldn't attempt to connect if userActivity type is not browsing
  [[appDelegateMock reject] attempToConnectFromURL:OCMOCK_ANY];
  [appDelegateMock application:[UIApplication sharedApplication] continueUserActivity:userActivity restorationHandler:^(NSArray * _Nullable restorableObjects) { }];
  [appDelegateMock verify];
}


- (void)testUniversalLinkingMalformedURL
{
  NSURL *expectedURL = [NSURL URLWithString:@"https://mezz-in.com/1234"];
  NSUserActivity *userActivity = [[NSUserActivity alloc] initWithActivityType:NSUserActivityTypeBrowsingWeb];
  userActivity.webpageURL = expectedURL;
  id appDelegateMock = [OCMockObject partialMockForObject:_appDelegate];
  
  [[appDelegateMock reject] attempToConnectFromURL:OCMOCK_ANY];
  [appDelegateMock application:[UIApplication sharedApplication] continueUserActivity:userActivity restorationHandler:^(NSArray * _Nullable restorableObjects) { }];
  [appDelegateMock verify];
}


- (void)testOpenInActionCannotBeUploadIfNotConnected
{
  MezzanineAppContext *appContext = [MezzanineAppContext new];
  appContext.currentCommunicator = nil;
  [_appDelegate setValue:appContext forKey:@"appContext"];
  
  NSURL *fileURL = [NSURL fileURLWithPath:@"path/image"];
  BOOL success = [_appDelegate application:[UIApplication sharedApplication] openURL:fileURL options:@{}];
  XCTAssertFalse(success);
  
  XCTAssertTrue([[UIApplication sharedApplication].keyWindow.rootViewController.presentedViewController isKindOfClass:[UIAlertController class]]);
  NSString *title = ((UIAlertController *)[UIApplication sharedApplication].keyWindow.rootViewController.presentedViewController).title;
  XCTAssertTrue([title isEqualToString:NSLocalizedString(@"Upload NotConnectedError Dialog Title", nil)]);
}


- (void)testOpenInActionCouldBeUploadWhenConnected
{
  MezzanineAppContext *appContext = [MezzanineAppContext new];
  appContext.currentCommunicator = [MZCommunicator new];
  [_appDelegate setValue:appContext forKey:@"appContext"];
  
  NSURL *fileURL = [NSURL fileURLWithPath:@"path/image"];
  BOOL success = [_appDelegate application:[UIApplication sharedApplication] openURL:fileURL options:@{}];
  XCTAssertFalse(success);
  
  XCTAssertTrue([[UIApplication sharedApplication].keyWindow.rootViewController.presentedViewController isKindOfClass:[UIAlertController class]]);
  NSString *title = ((UIAlertController *)[UIApplication sharedApplication].keyWindow.rootViewController.presentedViewController).title;
  XCTAssertTrue([title isEqualToString:NSLocalizedString(@"Upload FileTypeNotSupported Dialog Title", nil)]);
}

#pragma mark - Background / Foreground
// Commented out for now because it crashes calling backgroundTimeRemaining from a background thread.

//- (void)testSendAppToBackgroundAndBackToForegroundUpdatesAppContextState
//{
//  MezzanineAppDelegate *delegate = [UIApplication sharedApplication].delegate;
//  MezzanineAppContext *appContext = [delegate valueForKey:@"appContext"];
//
//  XCTAssertFalse(appContext.inBackground);
//
//  [delegate applicationDidEnterBackground:[UIApplication sharedApplication]];
//  XCTAssertTrue(appContext.inBackground);
//
//  [delegate applicationWillEnterForeground:[UIApplication sharedApplication]];
//  XCTAssertFalse(appContext.inBackground);
//}


@end
