//
//  TestSystemModelFactory.h
//  Mezzanine
//
//  Created by Zai Chang on 12/5/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MZSystemModel.h"

@interface MockSystemModel : NSObject

@property (nonatomic, strong, readonly) MZSystemModel *systemModel;
@property (nonatomic, strong) MZWorkspace *currentWorkspace;

- (NSString*)whiteboardUidForIndex:(NSInteger)index;
- (NSString*)whiteboardDisplayNameForIndex:(NSInteger)index;
- (MZSystemModel*)createTestModelWithWhiteboards:(NSInteger)numberOfWhiteboards;
- (MZSystemModel*)createTestModelWithSlides:(NSInteger)numberOfSlides;
- (MZSystemModel*)createTestModelWithTriptych;
- (MZSystemModel*)createTestModelWithSingleFeld;
- (MZSystemModel*)createTestModelWithDiptych;
- (MZSystemModel*)createTestModelWithTwoByThreeSurface;
- (MZSystemModel*)createTestModelWithOneExtendedWindshieldSurfaceWithOneFeld;
- (MZSystemModel*)createTestModelWithThreeExtendedWindshieldSurfacesWithOneFeld;
- (MZSystemModel*)createTestModelWithOneExtendedWindshieldSurfaceWithOneFeldAndOneItem;
- (MZSystemModel*)createTestModelWithOneSingleFeldAndOneExtendedWindshieldSurfaceWithOneFeld;
- (MZSystemModel*)createTestModelWithTriptychAndOneExtendedWindshieldSurfaceWithOneFeld;

- (void)addLiveStreamsToCurrentWorkspace:(NSInteger)numberOfLiveStreams;
- (void)removeLiveStreamsAtIndex:(NSInteger)index;
- (void)removeAllLiveStreams;
- (void)addSlidesToCurrentWorkspace:(NSInteger)numberOfSlides;
- (void)removeSlidesAtIndex:(NSInteger)index;
- (void)removeAllSlides;

@end
