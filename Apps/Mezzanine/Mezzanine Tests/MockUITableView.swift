//
//  MockUITableView.swift
//  Mezzanine
//
//  Created by Zai Chang on 6/29/15.
//  Copyright (c) 2015 Oblong Industries. All rights reserved.
//

import UIKit

class UITableViewInsertionDeletionInvocation {
	init(indexPaths: [AnyObject], animation: UITableViewRowAnimation) {
		self.indexPaths = indexPaths
		self.animation = animation
	}

	internal var indexPaths: [AnyObject] = []
	internal var animation: UITableViewRowAnimation
}

class MockUITableView: UITableView {
	
	init() {
		insertRowsAtIndexPathsInvocations = Array<UITableViewInsertionDeletionInvocation>();
		deleteRowsAtIndexPathsInvocations = Array<UITableViewInsertionDeletionInvocation>();
		self.selectedPaths = []
		super.init(frame: CGRect.zero, style: .grouped)
	}

	required init?(coder aDecoder: NSCoder) {
		insertRowsAtIndexPathsInvocations = Array<UITableViewInsertionDeletionInvocation>();
		deleteRowsAtIndexPathsInvocations = Array<UITableViewInsertionDeletionInvocation>();
		self.selectedPaths = []
		super.init(frame: CGRect.zero, style: .grouped)
	}
	
	var insertRowsAtIndexPathsInvocations: Array<UITableViewInsertionDeletionInvocation>;
	
	override func insertRows(at indexPaths: [IndexPath], with animation: UITableViewRowAnimation) {
		insertRowsAtIndexPathsInvocations.append(UITableViewInsertionDeletionInvocation(indexPaths: indexPaths as [AnyObject], animation: animation))
	}

	var deleteRowsAtIndexPathsInvocations: Array<UITableViewInsertionDeletionInvocation>;

	override func deleteRows(at indexPaths: [IndexPath], with animation: UITableViewRowAnimation) {
		deleteRowsAtIndexPathsInvocations.append(UITableViewInsertionDeletionInvocation(indexPaths: indexPaths as [AnyObject], animation: animation))
	}

	var selectedPaths: Array<IndexPath>
	
  override var indexPathsForSelectedRows: [IndexPath]? { get {return selectedPaths } }
}

