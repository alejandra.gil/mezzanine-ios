//
//  WorkspaceViewController_Tests.swift
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 11/10/2016.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

import XCTest

@testable import Mezzanine


class MockWorkspaceViewController: WorkspaceViewController {
  
  var isObservingSystemModel = false
  
  override func endObservingSystemModel() {
    isObservingSystemModel = false
    super.endObservingSystemModel()
  }
  
  override func beginObservingSystemModel() {
    isObservingSystemModel = true
    super.beginObservingSystemModel()
  }
}

class WorkspaceViewController_Tests: XCTestCase, WorkspaceViewControllerDelegate {
  
  var workspaceViewController: MockWorkspaceViewController!
  var systemModelFactory: MockSystemModel!
  var communicator: MZCommunicator!
  var appContext: MezzanineAppContext!
  
  override func setUp() {
    super.setUp()
    
    systemModelFactory = MockSystemModel()
    appContext = MezzanineAppContext()
    appContext.applicationModel = MezzanineAppModel()
    appContext.applicationModel.systemModel = systemModelFactory.systemModel
    
    communicator = MZCommunicator()
    communicator.systemModel = systemModelFactory.systemModel
    
    appContext.currentCommunicator = self.communicator
    
    workspaceViewController = MockWorkspaceViewController(systemModel: systemModelFactory.systemModel, communicator: communicator, delegate: self)
    workspaceViewController?.view
  }
  
  override func tearDown() {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    super.tearDown()
  }
  
  
  func testEndSystemModelObservationIfFeldsAreNil() {
    workspaceViewController.systemModel = systemModelFactory.createTestWithSingleFeld()
    XCTAssertTrue(workspaceViewController.isObservingSystemModel)
    
    workspaceViewController.systemModel.felds = nil
    XCTAssertFalse(workspaceViewController.isObservingSystemModel)
  }
  
  
  // MARK: Delegate
  // Actions
  func workspaceViewControllerDidStopPresentation(_ workspaceVC: WorkspaceViewController) { }
  func workspaceViewControllerDidToggleZoom(_ workspaceVC: WorkspaceViewController, mode: Bool, animated: Bool) { }
  
  // OBDragZone
  func workspaceViewControllerDidDraggingWindshieldItem(_ workspaceVC: WorkspaceViewController) { }
  func workspaceViewControllerDidStopDraggingWindshieldItem(_ workspaceVC: WorkspaceViewController) { }
}

