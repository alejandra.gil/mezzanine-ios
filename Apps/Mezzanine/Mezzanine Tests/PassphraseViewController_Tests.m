//
//  PassphraseViewController_Tests.m
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 28/10/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "OCMock.h"
#import "PassphraseViewController.h"
#import "PassphraseViewController_Private.h"
#import "MezzanineAppContext.h"
#import "MezzanineStyleSheet.h"
#import "Mezzanine-Swift.h"

//@interface MockPasskeyCoordinator: PasskeyCoordinator
//
//@property (nonatomic, assign) NSInteger passkeyWasIntroducedCallCount;
//@property (nonatomic, assign) NSInteger passkeyCancelledCount;
//
//@end
//
//@implementation MockPasskeyCoordinator
//
//- (void)passkeyWasIntroduced:(NSString *)passkey {
//  _passkeyWasIntroducedCallCount++;
//}
//
//- (void)passkeyCancelled {
//  _passkeyCancelledCount++;
//}
//
//
//@end

@interface PassphraseViewController_Tests : XCTestCase

@property (nonatomic, strong) PassphraseViewController *controller;
//@property (nonatomic, strong) MockPasskeyCoordinator *coordinator;

@end

@implementation PassphraseViewController_Tests

- (void)setUp
{
  [super setUp];
  
  _controller = [[PassphraseViewController alloc] initWithNibName:@"PassphraseViewController" bundle:nil];
  _controller.numOfFields = 3;
//  _coordinator = [MockPasskeyCoordinator new];
//  _controller.delegate = _coordinator;
  [MezzanineAppContext currentContext].window.rootViewController = self.controller;
  [_controller view];
}


- (void)tearDown
{
  _controller.delegate = nil;
//  _coordinator = nil;
  [MezzanineAppContext currentContext].window.rootViewController = [MezzanineNavigationController new];
  [super tearDown];
}


#pragma mark - IBOutlets

- (void)testTitleLabelOutlet
{
  XCTAssertNotNil(_controller.titleLabel);
  XCTAssertTrue([_controller.titleLabel.text isEqualToString:NSLocalizedString(@"Passkey View Title", nil)]);
}


- (void)testInstructionsLabelOutlet
{
  NSString *t = [NSString stringWithFormat: NSLocalizedString(@"Passkey View Hint", nil), _controller.numOfFields];
  XCTAssertNotNil(_controller.instructionsLabel);
  XCTAssertTrue([_controller.instructionsLabel.text isEqualToString: t]);
}


- (void)testTextFieldsContainerOutlet
{
  XCTAssertNotNil(_controller.textFieldsContainer);
  XCTAssertEqual([_controller.textFieldsContainer.gestureRecognizers count], 1);
  XCTAssertTrue([[_controller.textFieldsContainer.gestureRecognizers firstObject] class] == [UITapGestureRecognizer class]);
}


- (void)testCancelButtonOutlet
{
  XCTAssertNotNil(_controller.cancelButton);
  XCTAssertTrue([[_controller.cancelButton titleForState:UIControlStateNormal] isEqualToString:@"Cancel"]);
  NSArray *actionsArray = [_controller.cancelButton actionsForTarget:_controller forControlEvent:UIControlEventTouchUpInside];
  XCTAssertGreaterThan([actionsArray count], 0);
  XCTAssertTrue([actionsArray containsObject:@"dismiss:"]);
}


- (void)testTapStartOver
{
  [_controller startOver];
  
  for (UITextField *tf in _controller.textFieldArray) {
    XCTAssertEqual([tf.text length], 0);
  }

  XCTAssertTrue([((UITextField *)[_controller.textFieldArray firstObject]) isFirstResponder]);
}


//- (void)testWriteLetters
//{
//  MezzanineStyleSheet *styleSheet = [MezzanineStyleSheet sharedStyleSheet];
//
//  XCTAssertEqual(_controller.numOfFields, [_controller.textFieldArray count]);
//  XCTAssertEqual(_controller.numOfFields, [_controller.underlinesArray count]);
//
//
//  UITextField *tf1 = [_controller.textFieldArray objectAtIndex:0];
//  UITextField *tf2 = [_controller.textFieldArray objectAtIndex:1];
//  UITextField *tf3 = [_controller.textFieldArray objectAtIndex:2];
//
//  UITextField *lineView1 = [_controller.underlinesArray objectAtIndex:0];
//  UITextField *lineView2 = [_controller.underlinesArray objectAtIndex:1];
//  UITextField *lineView3 = [_controller.underlinesArray objectAtIndex:2];
//
//  // Insert
//  [_controller textField:tf1 shouldChangeCharactersInRange:NSMakeRange(0, 0) replacementString:@"1"];
//  XCTAssertTrue([tf2 isFirstResponder]);
//  XCTAssertTrue([lineView2.backgroundColor isEqual:styleSheet.superHighlightColor]);
//  XCTAssertFalse([tf1 isFirstResponder]);
//  XCTAssertFalse([tf3 isFirstResponder]);
//
//  XCTAssertTrue([tf1.text isEqualToString:@"1"]);
//
//  [_controller textField:tf2 shouldChangeCharactersInRange:NSMakeRange(0, 0) replacementString:@"2"];
//  XCTAssertTrue([tf3 isFirstResponder]);
//  XCTAssertTrue([lineView3.backgroundColor isEqual:styleSheet.superHighlightColor]);
//
//  XCTAssertFalse([tf1 isFirstResponder]);
//  XCTAssertFalse([tf2 isFirstResponder]);
//
//  XCTAssertTrue([tf1.text isEqualToString:@"1"]);
//  XCTAssertTrue([tf2.text isEqualToString:@"2"]);
//
//  [_controller textField:tf3 shouldChangeCharactersInRange:NSMakeRange(0, 0) replacementString:@"3"];
//  XCTAssertFalse([tf1 isFirstResponder]);
//  XCTAssertFalse([tf2 isFirstResponder]);
//  XCTAssertFalse([tf3 isFirstResponder]);
//
//  XCTAssertTrue([tf1.text isEqualToString:@"1"]);
//  XCTAssertTrue([tf2.text isEqualToString:@"2"]);
//  XCTAssertTrue([tf3.text isEqualToString:@"3"]);
//
//  XCTAssertTrue(_coordinator.passkeyWasIntroducedCallCount == 1)
//
//  // Delete
//  [_controller textField:tf3 shouldChangeCharactersInRange:NSMakeRange(0, 0) replacementString:@""];
//  XCTAssertTrue([tf3.text isEqualToString:@""]);
//  XCTAssertTrue([tf2 isFirstResponder]);
//  XCTAssertTrue([lineView2.backgroundColor isEqual:styleSheet.superHighlightColor]);
//
//
//  [_controller textField:tf2 shouldChangeCharactersInRange:NSMakeRange(0, 0) replacementString:@""];
//  XCTAssertTrue([tf2.text isEqualToString:@""]);
//  XCTAssertTrue([tf1 isFirstResponder]);
//  XCTAssertTrue([lineView1.backgroundColor isEqual:styleSheet.superHighlightColor]);
//}
//
//
//- (void)testJoinEmptyPassphrase
//{
//  [_controller join];
//
//  XCTAssertTrue(_coordinator.passkeyWasIntroducedCallCount == 0)
//}


- (void)testIncorrectPassphrase
{
  [_controller startOver];
 
  for (UITextField *tf in _controller.textFieldArray) {
    XCTAssertEqual([tf.text length], 0);
  }
}


//- (void)testDismissView
//{
//  [_controller dismiss:nil];
//
//  XCTAssertTrue(_coordinator.passkeyCancelledCount == 1)
//}
//


@end
