//
//  UIView+TestAnimations.h
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 08/10/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (TestAnimations)

+ (void)animateWithDuration:(NSTimeInterval)duration animations:(void (^)(void))animations completion:(void (^)(BOOL finished))completion;

@end
