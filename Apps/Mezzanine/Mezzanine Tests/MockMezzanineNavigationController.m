//
//  MockMezzanineNavigationController.m
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 02/09/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

#import "MockMezzanineNavigationController.h"

@implementation PushViewControllerInvocation : NSObject
@end

@implementation PushViewControllerWithTransitionViewInvocation
@end

@implementation PopViewControllerWithTransitionViewInvocations
@end

@implementation MockMezzanineNavigationController

- (instancetype)initWithRootViewController:(UIViewController *)rootViewController
{
  self = [super initWithRootViewController:rootViewController];
  if (self) {
    _pushViewControllerInvocations = @[].mutableCopy;
    _pushViewControllerWithTransitionViewInvocations = @[].mutableCopy;
    _popViewControllerWithTransitionViewInvocations = @[].mutableCopy;
  }
  return self;
}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
  [super pushViewController:viewController animated:NO];
  [_pushViewControllerInvocations addObject:[PushViewControllerInvocation new]];
}


- (void)pushViewController:(UIViewController *)viewController transitionView:(UIView *)transitionView;
{
  [super pushViewController:viewController animated:NO];
  [_pushViewControllerWithTransitionViewInvocations addObject:[PushViewControllerWithTransitionViewInvocation new]];
}


- (void)popViewControllerWithTransitionView:(UIView *)transitionView
{
  [super popViewControllerAnimated:NO];
  [_popViewControllerWithTransitionViewInvocations addObject:[PopViewControllerWithTransitionViewInvocations new]];
}

@end
