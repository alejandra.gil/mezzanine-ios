//
//  PhotoManager_Tests.swift
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 15/02/2017.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import XCTest

@testable import Mezzanine

class PhotoManager_Tests: XCTestCase {
  
  override func setUp() {
    super.setUp()
    // Put setup code here. This method is called before the invocation of each test method in the class.
  }
  
  override func tearDown() {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    super.tearDown()
  }
  
  func testSaveValidImage() {
    let image = UIImage.imageOfSize(CGSize(width: 30, height: 30))
    PhotoManager.sharedInstance.authorizationStatus { (status, promptedAccess) in
      if status == .authorized {
        PhotoManager.sharedInstance.saveImageToLibrary(image) { (success, error) in
          XCTAssertTrue(success)
        }
      } else {
        PhotoManager.sharedInstance.saveImageToLibrary(image) { (success, error) in
          XCTAssertFalse(success)
        }
      }
    }
  }
  
  // Bug 18149
  func testSaveInvalidImage() {
    PhotoManager.sharedInstance.saveImageToLibrary(UIImage()) { (success, error) in
      XCTAssertFalse(success)
    }
  }
}

extension UIImage {
  class func imageOfSize(_ size: CGSize) -> UIImage {
    UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
    let context = UIGraphicsGetCurrentContext()
    let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
    context!.fill(rect)
    let image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image!
  }
}
