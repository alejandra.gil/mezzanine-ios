//
//  UIView+TestAnimations.m
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 08/10/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

#import "UIView+TestAnimations.h"

@implementation UIView (TestAnimations)

+ (void)animateWithDuration:(NSTimeInterval)duration animations:(void (^)(void))animations completion:(void (^)(BOOL finished))completion {
  if (animations) {
    animations();
  }
  if (completion) {
    completion(YES);
  }
}

@end
