//
//  PrimaryWindshieldViewController_Tests.m
//  Mezzanine
//
//  Created by Miguel Sánchez Valdés on 06/10/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import "XCTestCase+Animations.h"
#import "PrimaryWindshieldViewController.h"
#import "PrimaryWindshieldViewController_Private.h"
#import "WindshieldViewController_Private.h"
#import "MockSystemModel.h"
#import "ImageAssetView.h"
#import "VideoAssetView.h"
#import "MezzanineAppContext.h"
#import "WindshieldPlaceholderView.h"
#import "MezzanineMainViewController.h"
#import "LiveStreamCollectionViewCell.h"
#import "PortfolioCollectionViewCell.h"
#import "WorkspaceContentScrollView.h"
#import "Mezzanine-Swift.h"


@interface PrimaryWindshieldViewController_Tests : XCTestCase
{
  PrimaryWindshieldViewController *_controller;
  MockSystemModel *_systemModelFactory;
}

@end


@implementation PrimaryWindshieldViewController_Tests

-(void) setUp
{
  [super setUp];

  _systemModelFactory = [MockSystemModel new];
  _controller = [[PrimaryWindshieldViewController alloc] init];
}

-(void) tearDown
{
  [_controller setValue:nil forKey:@"placeholderView"];
  _controller.windshield = nil;
  _controller.workspace = nil;
  _controller.systemModel = nil;
  _controller.workspaceViewController = nil;
  _controller = nil;
  _systemModelFactory = nil;

  [MezzanineAppContext currentContext].currentCommunicator = nil;

  [super tearDown];
}


#pragma mark - Helpers methods to setup context

-(void) prepareWindshieldInATriptych
{
  _controller.systemModel = [_systemModelFactory createTestModelWithTriptych];
  _controller.view.frame = CGRectMake(0, 0, 1020, 550);
  _controller.shielderContainerFrame = CGRectMake(0, 100, 1020, 192);
}

-(void) prepareWindshieldInASingleFeld
{
  _controller.systemModel = [_systemModelFactory createTestModelWithSingleFeld];
  _controller.view.frame = CGRectMake(0, 0, 1020, 550);
  _controller.shielderContainerFrame = CGRectMake(220, 50, 800, 450);
}


// This test should be part of MezzKit but those unit tests cannot be run on devices
// It should be tested with all different valid architectures (x86_64, armv7, armv7s, arm64)
-(void) testFeldAspectRatio
{
  [self prepareWindshieldInATriptych];

  // MZFeld has been mocked using OBVect as when initilized NOT using MZProto
  MZFeld *leftFeld = [_controller.systemModel feldWithName:@"left"];
  XCTAssertEqual(leftFeld.aspectRatio, 16.0/9.0, @"Left feld has an incorrect aspect ratio %f instead of %f", leftFeld.aspectRatio, 16.0/9.0);

  MZFeld *mainFeld = [_controller.systemModel feldWithName:@"main"];
  XCTAssertEqual(mainFeld.aspectRatio, 16.0/9.0, @"Main feld has an incorrect aspect ratio %f instead of %f", mainFeld.aspectRatio, 16.0/9.0);

  // MZFeld has been mocked using NSNumber as when initilized using MZProto
  MZFeld *rightFeld = [_controller.systemModel feldWithName:@"right"];
  XCTAssertEqual(rightFeld.aspectRatio, 16.0/9.0, @"Right feld has an incorrect aspect ratio %f instead of %f", rightFeld.aspectRatio, 16.0/9.0);
}

#pragma mark - Tests Coordinate transformations Native -> iOS

-(void) testRectFromNativeWindshieldCoordinatesForFullFeldItemInLeftFeld
{
  [self prepareWindshieldInATriptych];

  MZWindshieldItem *item = [[MZWindshieldItem alloc] init];
  item.frame = CGRectMake(0, 0, 1, 1);
  item.feld = [_controller.systemModel feldWithName:@"left"];

  CGRect fromNative = [_controller rectFromNativeWindshieldCoordinatesForItem:item];

  XCTAssertTrue(CGRectEqualToRect(fromNative, CGRectMake(0, 100, 340, 192)), @"Frame is %@ but it should be %@", NSStringFromCGRect(fromNative), NSStringFromCGRect(CGRectMake(0, 100, 340, 192)));
}

-(void) testRectFromNativeWindshieldCoordinatesForFullFeldItemInMainFeld
{
  [self prepareWindshieldInATriptych];

  MZWindshieldItem *item = [[MZWindshieldItem alloc] init];
  item.frame = CGRectMake(0, 0, 1, 1);
  item.feld = [_controller.systemModel feldWithName:@"main"];

  CGRect fromNative = [_controller rectFromNativeWindshieldCoordinatesForItem:item];

  XCTAssertTrue(CGRectEqualToRect(fromNative, CGRectMake(340, 100, 340, 192)), @"Frame is %@ but it should be %@", NSStringFromCGRect(fromNative), NSStringFromCGRect(CGRectMake(340, 100, 340, 192)));
}

-(void) testRectFromNativeWindshieldCoordinatesForFullFeldItemInRightFeld
{
  [self prepareWindshieldInATriptych];

  MZWindshieldItem *item = [[MZWindshieldItem alloc] init];
  item.frame = CGRectMake(0, 0, 1, 1);
  item.feld = [_controller.systemModel feldWithName:@"right"];

  CGRect fromNative = [_controller rectFromNativeWindshieldCoordinatesForItem:item];

  XCTAssertTrue(CGRectEqualToRect(fromNative, CGRectMake(680, 100, 340, 192)), @"Frame is %@ but it should be %@", NSStringFromCGRect(fromNative), NSStringFromCGRect(CGRectMake(680, 100, 340, 192)));
}

-(void) testRectFromNativeWindshieldCoordinatesForFullFeldItemInSingleFeld
{
  [self prepareWindshieldInASingleFeld];

  MZWindshieldItem *item = [[MZWindshieldItem alloc] init];
  item.frame = CGRectMake(0, 0, 1, 1);
  item.feld = [_controller.systemModel feldWithName:@"main"];

  CGRect fromNative = [_controller rectFromNativeWindshieldCoordinatesForItem:item];

  XCTAssertTrue(CGRectEqualToRect(fromNative, CGRectMake(220, 50, 800, 450)), @"Frame is %@ but it should be %@", NSStringFromCGRect(fromNative), NSStringFromCGRect(CGRectMake(220, 50, 800, 450)));
}

-(void) testRectFromNativeWindshieldCoordinatesForNonFullFeldItemInLeftFeld
{
  [self prepareWindshieldInATriptych];

  MZWindshieldItem *item = [[MZWindshieldItem alloc] init];
  item.frame = CGRectMake(-0.25, 0.25, 0.5, 0.5);
  item.feld = [_controller.systemModel feldWithName:@"left"];

  CGRect fromNative = [_controller rectFromNativeWindshieldCoordinatesForItem:item];

  XCTAssertTrue(CGRectEqualToRect(fromNative, CGRectMake(0, 100, 170, 96)), @"Frame is %@ but it should be %@", NSStringFromCGRect(fromNative), NSStringFromCGRect(CGRectMake(0, 100, 170, 96)));
}

-(void) testRectFromNativeWindshieldCoordinatesForNonFullFeldItemInMainFeld
{
  [self prepareWindshieldInATriptych];

  MZWindshieldItem *item = [[MZWindshieldItem alloc] init];
  item.frame = CGRectMake(-0.25, 0.25, 0.5, 0.5);
  item.feld = [_controller.systemModel feldWithName:@"main"];

  CGRect fromNative = [_controller rectFromNativeWindshieldCoordinatesForItem:item];

  XCTAssertTrue(CGRectEqualToRect(fromNative, CGRectMake(340, 100, 170, 96)), @"Frame is %@ but it should be %@", NSStringFromCGRect(fromNative), NSStringFromCGRect(CGRectMake(340, 100, 170, 96)));
}

-(void) testRectFromNativeWindshieldCoordinatesForNonFullFeldItemInRightFeld
{
  [self prepareWindshieldInATriptych];

  MZWindshieldItem *item = [[MZWindshieldItem alloc] init];
  item.frame = CGRectMake(-0.25, 0.25, 0.5, 0.5);
  item.feld = [_controller.systemModel feldWithName:@"right"];

  CGRect fromNative = [_controller rectFromNativeWindshieldCoordinatesForItem:item];

  XCTAssertTrue(CGRectEqualToRect(fromNative, CGRectMake(680, 100, 170, 96)), @"Frame is %@ but it should be %@", NSStringFromCGRect(fromNative), NSStringFromCGRect(CGRectMake(680, 100, 170, 96)));
}

-(void) testRectFromNativeWindshieldCoordinatesForNonFullFeldItemInSingleFeld
{
  [self prepareWindshieldInASingleFeld];

  MZWindshieldItem *item = [[MZWindshieldItem alloc] init];
  item.frame = CGRectMake(-0.25, 0.25, 0.5, 0.5);
  item.feld = [_controller.systemModel feldWithName:@"main"];

  CGRect fromNative = [_controller rectFromNativeWindshieldCoordinatesForItem:item];

  XCTAssertTrue(CGRectEqualToRect(fromNative, CGRectMake(220, 50, 400, 225)), @"Frame is %@ but it should be %@", NSStringFromCGRect(fromNative), NSStringFromCGRect(CGRectMake(220, 50, 400, 225)));
}


#pragma mark - Tests Coordinate transformations iOS -> Native

-(void) testRectToNativeWindshieldCoordinatesForFullFeldItemInLeftFeld
{
  [self prepareWindshieldInATriptych];

  CGRect toNative = [_controller rectToNativeWindshieldCoordinates:CGRectMake(0, 100, 340, 192) inFeld:[_controller.systemModel feldWithName:@"left"]];

  XCTAssertTrue(CGRectEqualToRect(toNative, CGRectMake(0, 0, 1, 1)), @"Frame is %@ but it should be %@", NSStringFromCGRect(toNative), NSStringFromCGRect(CGRectMake(0, 0, 1, 1)));
}

-(void) testRectToNativeWindshieldCoordinatesForFullFeldItemInMainFeld
{
  [self prepareWindshieldInATriptych];

  CGRect toNative = [_controller rectToNativeWindshieldCoordinates:CGRectMake(340, 100, 340, 192) inFeld:[_controller.systemModel feldWithName:@"main"]];

  XCTAssertTrue(CGRectEqualToRect(toNative, CGRectMake(0, 0, 1, 1)), @"Frame is %@ but it should be %@", NSStringFromCGRect(toNative), NSStringFromCGRect(CGRectMake(0, 0, 1, 1)));
}

-(void) testRectToNativeWindshieldCoordinatesForFullFeldItemInRightFeld
{
  [self prepareWindshieldInATriptych];

  MZWindshieldItem *item = [[MZWindshieldItem alloc] init];
  item.frame = CGRectMake(0, 0, 1, 1);
  item.feld = [_controller.systemModel feldWithName:@"right"];

  CGRect toNative = [_controller rectToNativeWindshieldCoordinates:CGRectMake(680, 100, 340, 192) inFeld:[_controller.systemModel feldWithName:@"right"]];

  XCTAssertTrue(CGRectEqualToRect(toNative, CGRectMake(0, 0, 1, 1)), @"Frame is %@ but it should be %@", NSStringFromCGRect(toNative), NSStringFromCGRect(CGRectMake(0, 0, 1, 1)));
}

-(void) testRectToNativeWindshieldCoordinatesForFullFeldItemInSingleFeld
{
  [self prepareWindshieldInASingleFeld];

  MZWindshieldItem *item = [[MZWindshieldItem alloc] init];
  item.frame = CGRectMake(0, 0, 1, 1);
  item.feld = [_controller.systemModel feldWithName:@"main"];

  CGRect toNative = [_controller rectToNativeWindshieldCoordinates:CGRectMake(220, 50, 800, 450) inFeld:[_controller.systemModel feldWithName:@"main"]];

  XCTAssertTrue(CGRectEqualToRect(toNative, CGRectMake(0, 0, 1, 1)), @"Frame is %@ but it should be %@", NSStringFromCGRect(toNative), NSStringFromCGRect(CGRectMake(0, 0, 1, 1)));
}

-(void) testRectToNativeWindshieldCoordinatesForNonFullFeldItemInLeftFeld
{
  [self prepareWindshieldInATriptych];

  MZWindshieldItem *item = [[MZWindshieldItem alloc] init];
  item.frame = CGRectMake(-0.25, 0.25, 0.5, 0.5);
  item.feld = [_controller.systemModel feldWithName:@"left"];

  CGRect toNative = [_controller rectToNativeWindshieldCoordinates:CGRectMake(0, 100, 170, 96) inFeld:[_controller.systemModel feldWithName:@"left"]];

  XCTAssertTrue(CGRectEqualToRect(toNative, CGRectMake(-0.25, 0.25, 0.5, 0.5)), @"Frame is %@ but it should be %@", NSStringFromCGRect(toNative), NSStringFromCGRect(CGRectMake(-0.25, 0.25, 0.5, 0.5)));
}

-(void) testRectToNativeWindshieldCoordinatesForNonFullFeldItemInMainFeld
{
  [self prepareWindshieldInATriptych];

  MZWindshieldItem *item = [[MZWindshieldItem alloc] init];
  item.frame = CGRectMake(-0.25, 0.25, 0.5, 0.5);
  item.feld = [_controller.systemModel feldWithName:@"main"];

  CGRect toNative = [_controller rectToNativeWindshieldCoordinates:CGRectMake(340, 100, 170, 96) inFeld:[_controller.systemModel feldWithName:@"main"]];

  XCTAssertTrue(CGRectEqualToRect(toNative, CGRectMake(-0.25, 0.25, 0.5, 0.5)), @"Frame is %@ but it should be %@", NSStringFromCGRect(toNative), NSStringFromCGRect(CGRectMake(-0.25, 0.25, 0.5, 0.5)));
}

-(void) testRectToNativeWindshieldCoordinatesForNonFullFeldItemInRightFeld
{
  [self prepareWindshieldInATriptych];

  MZWindshieldItem *item = [[MZWindshieldItem alloc] init];
  item.frame = CGRectMake(-0.25, 0.25, 0.5, 0.5);
  item.feld = [_controller.systemModel feldWithName:@"right"];

  CGRect toNative = [_controller rectToNativeWindshieldCoordinates:CGRectMake(680, 100, 170, 96) inFeld:[_controller.systemModel feldWithName:@"right"]];

  XCTAssertTrue(CGRectEqualToRect(toNative, CGRectMake(-0.25, 0.25, 0.5, 0.5)), @"Frame is %@ but it should be %@", NSStringFromCGRect(toNative), NSStringFromCGRect(CGRectMake(-0.25, 0.25, 0.5, 0.5)));
}

-(void) testRectToNativeWindshieldCoordinatesForNonFullFeldItemInSingleFeld
{
  [self prepareWindshieldInASingleFeld];

  MZWindshieldItem *item = [[MZWindshieldItem alloc] init];
  item.frame = CGRectMake(-0.25, 0.25, 0.5, 0.5);
  item.feld = [_controller.systemModel feldWithName:@"main"];

  CGRect toNative = [_controller rectToNativeWindshieldCoordinates:CGRectMake(220, 50, 400, 225) inFeld:[_controller.systemModel feldWithName:@"main"]];

  XCTAssertTrue(CGRectEqualToRect(toNative, CGRectMake(-0.25, 0.25, 0.5, 0.5)), @"Frame is %@ but it should be %@", NSStringFromCGRect(toNative), NSStringFromCGRect(CGRectMake(-0.25, 0.25, 0.5, 0.5)));
}


#pragma mark - Tests Coordinate transformations windshield view <-> windshield item container view

-(void) testShiftPointFromWindshieldViewToShielderContainer
{
  CGPoint point = CGPointMake(10, 20);
  CGPoint shiftedPoint = [_controller shiftPointFromWindshieldViewToShielderContainer:point];

  XCTAssertEqual(shiftedPoint.x, point.x + _controller.shielderContainerFrame.origin.x, "X component in shifted point from widnshield view should be %f but it is %f", shiftedPoint.x, point.x + _controller.shielderContainerFrame.origin.x);

  XCTAssertEqual(shiftedPoint.y, point.y + _controller.shielderContainerFrame.origin.y, "Y component in shifted point from widnshield view should be %f but it is %f", shiftedPoint.y, point.y + _controller.shielderContainerFrame.origin.y);
}

-(void) testShiftPointFromShielderContainerToWindshieldView
{
  CGPoint point = CGPointMake(10, 20);
  CGPoint shiftedPoint = [_controller shiftPointFromShielderContainerToWindshieldView:point];

  XCTAssertEqual(shiftedPoint.x, point.x - _controller.shielderContainerFrame.origin.x, "X component in shifted point from shielder container should be %f but it is %f", shiftedPoint.x, point.x + _controller.shielderContainerFrame.origin.x);

  XCTAssertEqual(shiftedPoint.y, point.y - _controller.shielderContainerFrame.origin.y, "Y component in shifted point from shielder container should be %f but it is %f", shiftedPoint.y, point.y + _controller.shielderContainerFrame.origin.y);
}


#pragma mark - Loading Items

-(void) setupWindshieldWithOneImageWindshieldItem
{
  MZWindshieldItem *item = [[MZWindshieldItem alloc] init];
  item.uid = @"ws-1111";
  item.assetUid = @"as-1111";
  item.frame = CGRectMake(0, 0, 1, 1);
  item.surface = _controller.systemModel.surfaces[0];
  item.feld = [item.surface felds][0];

  [_controller.systemModel.currentWorkspace.windshield.items addObject:item];

  _controller.workspace = _controller.systemModel.currentWorkspace;
  _controller.windshield = _controller.systemModel.currentWorkspace.windshield;
}

-(void) testLoadAllItems
{
  [self prepareWindshieldInASingleFeld];

  NSMutableArray *items = [_controller valueForKey:@"itemViews"];
  XCTAssertEqual(items.count, 0, @"There should not be any item before getting loaded but there are %lu", (unsigned long)items.count);

  [self setupWindshieldWithOneImageWindshieldItem];
  [_controller loadAllWindshieldItems];

  items = [_controller valueForKey:@"itemViews"];
  XCTAssertEqual(items.count, 1, @"There should be one item after loading all items but there are %lu", (unsigned long)items.count);
}

-(void) testAddOneItemToWindshieldModel
{
  [self prepareWindshieldInASingleFeld];

  _controller.workspace = _controller.systemModel.currentWorkspace;
  _controller.windshield = _controller.systemModel.currentWorkspace.windshield;

  NSMutableArray *items = [_controller valueForKey:@"itemViews"];
  XCTAssertEqual(items.count, 0, @"There should not be any item before getting loaded but there are %lu", (unsigned long)items.count);

  MZWindshieldItem *item = [[MZWindshieldItem alloc] init];
  item.uid = @"ws-1111";
  item.assetUid = @"as-1111";
  item.frame = CGRectMake(0, 0, 1, 1);
  item.surface = _controller.systemModel.surfaces[0];
  item.feld = [item.surface felds][0];

  [_controller.systemModel.currentWorkspace.windshield insertObject:item inItemsAtIndex:0];

  items = [_controller valueForKey:@"itemViews"];
  XCTAssertEqual(items.count, 1, @"There should be one item after loading all items but there are %lu", (unsigned long)items.count);
}

-(void) testRemoveAllItems
{
  [self prepareWindshieldInASingleFeld];
  [self setupWindshieldWithOneImageWindshieldItem];
  [_controller loadAllWindshieldItems];

  NSMutableArray *items = [_controller valueForKey:@"itemViews"];
  XCTAssertEqual(items.count, 1, @"There should be one item after loading all items but there are %lu", (unsigned long)items.count);

  [_controller removeAllItemViews];

  items = [_controller valueForKey:@"itemViews"];
  XCTAssertEqual(items.count, 0, @"There should not be any item before getting loaded but there are %lu", (unsigned long)items.count);
}

-(void) testRemoveOneItemFromWindshieldModel
{
  [self prepareWindshieldInASingleFeld];
  [self setupWindshieldWithOneImageWindshieldItem];
  [_controller loadAllWindshieldItems];

  NSMutableArray *items = [_controller valueForKey:@"itemViews"];
  XCTAssertEqual(items.count, 1, @"There should be one item after loading all items but there are %lu", (unsigned long)items.count);

  [_controller.systemModel.currentWorkspace.windshield removeObjectFromItemsAtIndex:0];

  items = [_controller valueForKey:@"itemViews"];
  XCTAssertEqual(items.count, 0, @"There should not be any item before getting loaded but there are %lu", (unsigned long)items.count);
}


#pragma mark - OBDropZone

- (MZWindshieldItem *)setupItemInWindshield
{
  MZWindshieldItem *item = [MZWindshieldItem new];
  item.uid = @"windshield-item-uid";
  item.aspectRatio = 16.0/9.0;
  return item;
}

- (void)setupWindshieldWithDraggedViewWithItem:(MZWindshieldItem *)item
{
  ImageAssetView *draggedView = [ImageAssetView new];
  draggedView.alpha = 1.0;
  draggedView.asset = item;
  _controller.draggedView = draggedView;
}

- (OBOvum *)setupOvumWithItem:(MZWindshieldItem *)item
{
  ImageAssetView *draggedView = [ImageAssetView new];
  draggedView.alpha = 1.0;
  draggedView.asset = item;

  OBOvum *ovum = [[OBOvum alloc] init];
  ovum.dataObject = item;
  ovum.dragView = draggedView;

  return ovum;
}

- (void)testOvumDroppedToTransformWindshieldItem
{
  [self prepareWindshieldInASingleFeld];

  MZSurface *surface = [_controller.systemModel.surfaces firstObject];
  MZFeld *feld = [surface.felds firstObject];

  MZWindshieldItem *item = [MZWindshieldItem new];
  item.uid = @"ws-1111";
  item.assetUid = @"as-1111";
  item.frame = CGRectMake(0, 0, 1, 1);
  item.surface = surface;
  item.feld = feld;
  item.aspectRatio = 16.0/9.0;
  item.feldRelativeSize = @{@"scale":@1.0};

  CGRect dragViewFrame = CGRectMake(220.0, 50.0, 800.0, 450); // full feld in single feld mezz
  CGPoint windshieldViewCenter = _controller.view.center; //CGPointMake(510.0, 275.0)

  [self setupWindshieldWithDraggedViewWithItem:item];
  OBOvum *ovum = [self setupOvumWithItem:item];
  ovum.source = _controller;
  ovum.dragView.frame = dragViewFrame;
  ovum.dragViewInitialSize = dragViewFrame.size;

  
  id requestor = [OCMockObject niceMockForClass:[MZRequestor class]];
  MZCommunicator *communicator = [MZCommunicator new];
  communicator.requestor = requestor;
  [_controller setCommunicator:communicator];

  [[requestor expect] requestWindshieldItemTransformRequest:OCMOCK_ANY uid:OCMOCK_ANY surfaceName:OCMOCK_ANY feldRelativeCoords:OCMOCK_ANY feldRelativeSize:OCMOCK_ANY];
  

  [_controller ovumDropped:ovum inView:_controller.view atLocation:windshieldViewCenter];

  [requestor verify];
}

- (void)testOvumDroppedToInsertLiveStreamItem
{
  [self prepareWindshieldInASingleFeld];

  MZLiveStream *item = [MZLiveStream new];
  item.uid = @"vi-1111";

  CGRect dragViewFrame = CGRectMake(220.0, 50.0, 800.0, 450); // full feld in single feld mezz
  CGPoint windshieldViewCenter = _controller.view.center; //CGPointMake(510.0, 275.0)

  LiveStreamCollectionViewCell *draggedView = [[LiveStreamCollectionViewCell alloc] initWithFrame:dragViewFrame];
  draggedView.liveStream = item;
  draggedView.imageView.image = [UIImage new];

  OBOvum *ovum = [[OBOvum alloc] init];
  ovum.dataObject = item;
  ovum.dragView = draggedView;
  ovum.dragView.frame = dragViewFrame;

  
  id requestor = [OCMockObject mockForClass:[MZCommunicatorRequestor class]];
  [[requestor expect] requestWindshieldItemCreateRequest:OCMOCK_ANY contentSource:OCMOCK_ANY surfaceName:OCMOCK_ANY feldRelativeCoords:OCMOCK_ANY feldRelativeSize:OCMOCK_ANY];
  
  MZCommunicator *communicator = [MZCommunicator new];
  communicator.requestor = requestor;
  [_controller setCommunicator:communicator];

  [_controller ovumDropped:ovum inView:_controller.view atLocation:windshieldViewCenter];

  [requestor verify];
}


- (void)testOvumDroppedToInsertPortofolioItem
{
  [self prepareWindshieldInASingleFeld];

  MZPortfolioItem *item = [MZPortfolioItem new];
  item.uid = @"sl-1111";
  item.contentSource = @"as-1111";

  CGRect dragViewFrame = CGRectMake(220.0, 50.0, 800.0, 450); // full feld in single feld mezz
  CGPoint windshieldViewCenter = _controller.view.center; //CGPointMake(510.0, 275.0)

  PortfolioCollectionViewCell *draggedView = [[PortfolioCollectionViewCell alloc] initWithFrame:dragViewFrame];
  draggedView.slide = (MZSlide *) item;
  draggedView.imageView.image = [UIImage new];

  OBOvum *ovum = [[OBOvum alloc] init];
  ovum.dataObject = item;
  ovum.dragView = draggedView;
  ovum.dragView.frame = dragViewFrame;

  id requestor = [OCMockObject mockForClass:[MZRequestor class]];
  [[requestor expect] requestWindshieldItemCreateRequest:OCMOCK_ANY contentSource:OCMOCK_ANY surfaceName:OCMOCK_ANY feldRelativeCoords:OCMOCK_ANY feldRelativeSize:OCMOCK_ANY];

  MZCommunicator *communicator = [MZCommunicator new];
  communicator.requestor = requestor;
  [_controller setCommunicator:communicator];

  [_controller ovumDropped:ovum inView:_controller.view atLocation:windshieldViewCenter];

  [requestor verify];
}


- (OBOvum *)prepareOvumWithWinsdshieldItemWithScale:(CGFloat)itemScale dragViewFrame:(CGRect)dragViewFrame inWindshieldWithZoomScale:(CGFloat)zoomScale
{
  MZSurface *surface = [_controller.systemModel.surfaces firstObject];
  MZFeld *feld = [surface.felds firstObject];

  MZWindshieldItem *item = [MZWindshieldItem new];
  item.uid = @"ws-1111";
  item.assetUid = @"as-1111";
  item.frame = CGRectMake(0, 0, itemScale, itemScale);
  item.surface = surface;
  item.feld = feld;
  item.aspectRatio = 16.0/9.0;
  item.feldRelativeSize = @{@"scale":@(itemScale)};

  CGPoint windshieldViewCenter = _controller.view.center; //CGPointMake(510.0, 275.0)

  [self setupWindshieldWithDraggedViewWithItem:item];
  OBOvum *ovum = [self setupOvumWithItem:item];
  ovum.source = _controller;
  ovum.dragView.frame = dragViewFrame;
  ovum.dragViewInitialSize = dragViewFrame.size;

  _controller.draggedView.frame = CGRectMake(0, 0, dragViewFrame.size.width, dragViewFrame.size.height);
  _controller.draggedView.center = windshieldViewCenter;

  CGFloat scale = zoomScale;
  CGPoint point = windshieldViewCenter;
  id workspaceContentScrollView = [OCMockObject mockForClass:[WorkspaceContentScrollView class]];
  [[[workspaceContentScrollView stub] andReturnValue:OCMOCK_VALUE(scale)] zoomScale];
  [[[workspaceContentScrollView stub] andReturnValue:OCMOCK_VALUE(point)] convertPoint:point fromView:_controller.view];


  id workspaceContentView = [OCMockObject niceMockForClass:[UIView class]];
  [[[workspaceContentView stub] andReturnValue:OCMOCK_VALUE(point)] convertPoint:point fromView:_controller.view];


  long indexInteger = 1;
  id workspaceViewController = [OCMockObject niceMockForClass:[WorkspaceViewController class]];
  [[[workspaceViewController stub] andReturn:workspaceContentScrollView] contentScrollView];
  [[[workspaceViewController stub] andReturnValue:OCMOCK_VALUE(indexInteger)] feldIndexAtLocation:point];
  [[[workspaceViewController stub] andReturnValue:@NO] isLocationInCorkboardSpace:windshieldViewCenter];
  [[[workspaceViewController stub] andReturn:workspaceContentView] workspaceContentView];

  _controller.workspaceViewController = workspaceViewController;

  return ovum;
}

- (void)testOvumMovedWhileTransformingAFullScreenWindshieldItemInAZoomedOutTriptych
{
  [self prepareWindshieldInATriptych];

  CGPoint windshieldViewCenter = _controller.view.center; //CGPointMake(510.0, 275.0)

  OBOvum *ovum = [self prepareOvumWithWinsdshieldItemWithScale:1.0 dragViewFrame:CGRectMake(680.0, 100.0, 1020.0, 192.0) inWindshieldWithZoomScale:1.0/3.0];
  [_controller createIndicatorForOvum:ovum atLocation:windshieldViewCenter];

  WindshieldPlaceholderView *placeholderView = [_controller valueForKey:@"placeholderView"];
  XCTAssertEqual(placeholderView.alpha, 0, @"placeholderView should not be visible yet.");

  [_controller ovumMoved:ovum inView:_controller.view atLocation:windshieldViewCenter];
  XCTAssertEqual(placeholderView.alpha, 1, @"placeholderView be visible now.");
}

- (void)testOvumMovedWhileTransformingANotFullScreenWindshieldItemInAZoomedOutTriptych
{
  [self prepareWindshieldInATriptych];

  CGPoint windshieldViewCenter = _controller.view.center; //CGPointMake(510.0, 275.0)

  OBOvum *ovum = [self prepareOvumWithWinsdshieldItemWithScale:0.5 dragViewFrame:CGRectMake(680.0, 100.0, 510.0, 96.0) inWindshieldWithZoomScale:1.0/3.0];
  [_controller createIndicatorForOvum:ovum atLocation:windshieldViewCenter];

  WindshieldPlaceholderView *placeholderView = [_controller valueForKey:@"placeholderView"];
  XCTAssertEqual(placeholderView.alpha, 0, @"placeholderView should not be visible yet.");

  [_controller ovumMoved:ovum inView:_controller.view atLocation:windshieldViewCenter];
  XCTAssertEqual(placeholderView.alpha, 0, @"placeholderView should not be visible still.");
}

- (void)testOvumMovedWhileTransformingAFullScreenWindshieldItemInAZoomedInTriptych
{
  [self prepareWindshieldInATriptych];

  CGPoint windshieldViewCenter = _controller.view.center; //CGPointMake(510.0, 275.0)

  OBOvum *ovum = [self prepareOvumWithWinsdshieldItemWithScale:1.0 dragViewFrame:CGRectMake(680.0, 100.0, 1020.0, 192.0) inWindshieldWithZoomScale:1.0];
  [_controller createIndicatorForOvum:ovum atLocation:windshieldViewCenter];

  WindshieldPlaceholderView *placeholderView = [_controller valueForKey:@"placeholderView"];
  XCTAssertEqual(placeholderView.alpha, 0, @"placeholderView should not be visible yet.");

  [_controller ovumMoved:ovum inView:_controller.view atLocation:windshieldViewCenter];
  XCTAssertEqual(placeholderView.alpha, 1, @"placeholderView be visible now.");
}

- (void)testOvumMovedWhileTransformingANotFullScreenWindshieldItemInAZoomedInTriptych
{
  [self prepareWindshieldInATriptych];

  CGPoint windshieldViewCenter = _controller.view.center; //CGPointMake(510.0, 275.0)

  OBOvum *ovum = [self prepareOvumWithWinsdshieldItemWithScale:0.5 dragViewFrame:CGRectMake(680.0, 100.0, 510.0, 96.0) inWindshieldWithZoomScale:1.0];
  [_controller createIndicatorForOvum:ovum atLocation:windshieldViewCenter];

  WindshieldPlaceholderView *placeholderView = [_controller valueForKey:@"placeholderView"];
  XCTAssertEqual(placeholderView.alpha, 0, @"placeholderView should not be visible yet.");

  [_controller ovumMoved:ovum inView:_controller.view atLocation:windshieldViewCenter];
  XCTAssertEqual(placeholderView.alpha, 0, @"placeholderView should not be visible still.");
}

- (void)testOvumMovedWhileTransformingAFullScreenWindshieldItemInASingleFeldMezz
{
  [self prepareWindshieldInASingleFeld];

  CGPoint windshieldViewCenter = _controller.view.center; //CGPointMake(510.0, 275.0)

  OBOvum *ovum = [self prepareOvumWithWinsdshieldItemWithScale:1.0 dragViewFrame:CGRectMake(220.0, 50.0, 800.0, 450) inWindshieldWithZoomScale:1.0];
  [_controller createIndicatorForOvum:ovum atLocation:windshieldViewCenter];

  WindshieldPlaceholderView *placeholderView = [_controller valueForKey:@"placeholderView"];
  XCTAssertEqual(placeholderView.alpha, 0, @"placeholderView should not be visible yet.");

  [_controller ovumMoved:ovum inView:_controller.view atLocation:windshieldViewCenter];
  XCTAssertEqual(placeholderView.alpha, 1, @"placeholderView be visible now.");
}

- (void)testOvumMovedWhileTransformingANotFullScreenWindshieldItemInASingleFeldMezz
{
  [self prepareWindshieldInASingleFeld];

  CGPoint windshieldViewCenter = _controller.view.center; //CGPointMake(510.0, 275.0)

  OBOvum *ovum = [self prepareOvumWithWinsdshieldItemWithScale:0.5 dragViewFrame:CGRectMake(220.0, 50.0, 400.0, 225) inWindshieldWithZoomScale:1.0];
  [_controller createIndicatorForOvum:ovum atLocation:windshieldViewCenter];

  WindshieldPlaceholderView *placeholderView = [_controller valueForKey:@"placeholderView"];
  XCTAssertEqual(placeholderView.alpha, 0, @"placeholderView should not be visible yet.");

  [_controller ovumMoved:ovum inView:_controller.view atLocation:windshieldViewCenter];
  XCTAssertEqual(placeholderView.alpha, 0, @"placeholderView should not be visible still.");
}


#pragma mark - Indicator

- (void)testScaleInFullScreenMargins
{
  XCTAssertFalse([_controller scaleInFullscreenMargins:1.5], @"1.5 scale should not show full screen margins.");
  XCTAssertFalse([_controller scaleInFullscreenMargins:1.1], @"1.1 scale should not show full screen margins.");
  XCTAssertTrue([_controller scaleInFullscreenMargins:1.09], @"1.1 scale should not show full screen margins.");
  XCTAssertTrue([_controller scaleInFullscreenMargins:0.91], @"1.1 scale should not show full screen margins.");
  XCTAssertFalse([_controller scaleInFullscreenMargins:0.9], @"0.9 scale should not show full screen margins.");
  XCTAssertFalse([_controller scaleInFullscreenMargins:0.5], @"0.5 scale should not show full screen margins.");
}

- (void)testCreateIndicator
{
  [self prepareWindshieldInASingleFeld];
  MZWindshieldItem *item = [self setupItemInWindshield];
  [self setupWindshieldWithDraggedViewWithItem:item];
  OBOvum *ovum = [self setupOvumWithItem:item];
  ovum.source = _controller;

  [_controller createIndicatorForOvum:ovum atLocation:_controller.view.center];

  WindshieldPlaceholderView *placeholderView = [_controller valueForKey:@"placeholderView"];
  XCTAssertEqual(placeholderView.alpha, 0.0, @"Placeholder Indicator alpha chanel should be 0.0");
  XCTAssertTrue(CGRectEqualToRect(_controller.shielderContainerFrame, placeholderView.frame), @"Placeholder frame should be %@ but it is %@", NSStringFromCGRect(_controller.shielderContainerFrame), NSStringFromCGRect(placeholderView.frame));
  XCTAssertTrue([[OBDragDropManager sharedManager].overlayWindow.subviews containsObject:placeholderView], @"OBDragDrop overlay window should contain the placeholder view.");
  XCTAssertEqual(placeholderView.ovum, ovum, @"The ovum linked to the placeholder view should be the same that is currently moved.");
}

- (void)setupWindshieldWithIndicator
{
  [self prepareWindshieldInASingleFeld];
  MZWindshieldItem *item = [self setupItemInWindshield];
  [self setupWindshieldWithDraggedViewWithItem:item];
  OBOvum *ovum = [self setupOvumWithItem:item];
  ovum.source = _controller;

  [_controller createIndicatorForOvum:ovum atLocation:_controller.view.center];
}

- (void)testShowIndicator
{
  [self setupWindshieldWithIndicator];

  WindshieldPlaceholderView *placeholderView = [_controller valueForKey:@"placeholderView"];
  XCTAssertEqual(placeholderView.alpha, 0.0, @"Placeholder Indicator alpha chanel should be 0.0");

  [_controller showPlaceholderView:YES animated:YES];

  XCTAssertEqual(placeholderView.alpha, 1.0, @"Placeholder Indicator alpha chanel should be 1.0");
}

- (void)testHideIndicator
{
  [self setupWindshieldWithIndicator];
  UIView *placeholderView = [_controller valueForKey:@"placeholderView"];
  placeholderView.alpha = 1.0;

  XCTAssertEqual(placeholderView.alpha, 1.0, @"Placeholder Indicator alpha chanel should be 1.0");

  [_controller showPlaceholderView:NO animated:NO];

  XCTAssertEqual(placeholderView.alpha, 0.0, @"Placeholder Indicator alpha chanel should be 0.0");
}

- (void)testUpdatePlaceholderIndicatorWithNewLocationInACorkboard
{
  [self setupWindshieldWithIndicator];

  WindshieldPlaceholderView *placeholderView = [_controller valueForKey:@"placeholderView"];

  id workspaceViewController = [OCMockObject mockForClass:[WorkspaceViewController class]];
  [[[workspaceViewController stub] andReturnValue:@YES] isLocationInCorkboardSpace:CGPointZero];
  _controller.workspaceViewController = workspaceViewController;

  [_controller updatePlaceholderViewForObject:placeholderView.ovum.dataObject withLocation:CGPointZero andScale:1.0];

  XCTAssertEqual(placeholderView.alpha, 0.0, @"Placeholder should hide");

  _controller.workspaceViewController = nil;
}

- (void)testUpdatePlaceholderIndicatorOfNewLocationInTheWindshield
{
  [self setupWindshieldWithIndicator];

  CGPoint point = _controller.view.center;
  id workspaceContentScrollView = [OCMockObject mockForClass:[WorkspaceContentScrollView class]];
  [[[workspaceContentScrollView stub] andReturnValue:OCMOCK_VALUE(point)] convertPoint:point fromView:_controller.view];

  long indexInteger = 0;
  id workspaceViewController = [OCMockObject mockForClass:[WorkspaceViewController class]];
  [[[workspaceViewController stub] andReturn:workspaceContentScrollView] contentScrollView];
  [[[workspaceViewController stub] andReturnValue:@NO] isLocationInCorkboardSpace:_controller.view.center];
  [[[workspaceViewController stub] andReturnValue:OCMOCK_VALUE(indexInteger)] feldIndexAtLocation:point];

  _controller.workspaceViewController = workspaceViewController;

  WindshieldPlaceholderView *placeholderView = [_controller valueForKey:@"placeholderView"];
  [_controller updatePlaceholderViewForObject:placeholderView.ovum.dataObject withLocation:_controller.view.center andScale:1.0];
  XCTAssertEqual(placeholderView.alpha, 1.0, @"Placeholder should be visible");
}

- (void)testUpdatePlaceholderIndicatorOfNewLocationInTheWindshieldNotFullFeld
{
  [self setupWindshieldWithIndicator];

  CGPoint point = _controller.view.center;
  id workspaceContentScrollView = [OCMockObject mockForClass:[WorkspaceContentScrollView class]];
  [[[workspaceContentScrollView stub] andReturnValue:OCMOCK_VALUE(point)] convertPoint:point fromView:_controller.view];

  long indexInteger = 0;
  id workspaceViewController = [OCMockObject mockForClass:[WorkspaceViewController class]];
  [[[workspaceViewController stub] andReturn:workspaceContentScrollView] contentScrollView];
  [[[workspaceViewController stub] andReturnValue:@NO] isLocationInCorkboardSpace:_controller.view.center];
  [[[workspaceViewController stub] andReturnValue:OCMOCK_VALUE(indexInteger)] feldIndexAtLocation:point];

  _controller.workspaceViewController = workspaceViewController;

  WindshieldPlaceholderView *placeholderView = [_controller valueForKey:@"placeholderView"];
  [_controller updatePlaceholderViewForObject:placeholderView.ovum.dataObject withLocation:_controller.view.center andScale:0.5];
  XCTAssertEqual(placeholderView.alpha, 0.0, @"Placeholder should be visible");
}

- (void)testRemovePlaceholderIndicator
{
  [self setupWindshieldWithIndicator];

  WindshieldPlaceholderView *placeholderViewBefore = [_controller valueForKey:@"placeholderView"];
  XCTAssertNotNil(placeholderViewBefore, @"The placeholder indicator should exist");

  [_controller removeIndicator];
  WindshieldPlaceholderView *placeholderViewAfter = [_controller valueForKey:@"placeholderView"];
  XCTAssertNil(placeholderViewAfter, @"The placeholder indicator should be gone after removal.");
}

#pragma mark - Indicator + OBDropZone

- (void)testIndicatorGoneAfterOvumIsDropped
{
  [self setupWindshieldWithIndicator];

  [_controller ovumDropped:[[_controller valueForKey:@"placeholderView"] ovum] inView:_controller.view atLocation:CGPointZero];

  WindshieldPlaceholderView *placeholderView = [_controller valueForKey:@"placeholderView"];
  XCTAssertNil(placeholderView, @"The placeholder indicator should be gone after dropping");
}


#pragma mark - Test Helper Methods

- (void)testScaleOfItemViewAtLocationWhenZoomedIn
{
  [self prepareWindshieldInATriptych];
  MZWindshieldItem *item = [self setupItemInWindshield];
  [self setupWindshieldWithDraggedViewWithItem:item];

  CGPoint point = _controller.view.center;
  CGFloat scale = 1.0;
  id workspaceContentScrollView = [OCMockObject mockForClass:[WorkspaceContentScrollView class]];
  [[[workspaceContentScrollView stub] andReturnValue:OCMOCK_VALUE(point)] convertPoint:point fromView:_controller.view];
  [[[workspaceContentScrollView stub] andReturnValue:OCMOCK_VALUE(scale)] zoomScale];

  long indexInteger = 0;
  id workspaceViewController = [OCMockObject mockForClass:[WorkspaceViewController class]];
  [[[workspaceViewController stub] andReturn:workspaceContentScrollView] contentScrollView];
  [[[workspaceViewController stub] andReturnValue:@NO] isLocationInCorkboardSpace:_controller.view.center];
  [[[workspaceViewController stub] andReturnValue:OCMOCK_VALUE(indexInteger)] feldIndexAtLocation:point];

  _controller.workspaceViewController = workspaceViewController;

  _controller.draggedView.frame = _controller.shielderContainerFrame;
  
  scale = [_controller scaleOfItemView:_controller.draggedView atLocation:_controller.view.center];

  XCTAssertEqual(scale, 1.0, @"scale is %f but it should be 1.0", scale);
}

- (void)testScaleOfItemViewAtLocationWhenZoomedOut
{
  [self prepareWindshieldInATriptych];
  MZWindshieldItem *item = [self setupItemInWindshield];
  [self setupWindshieldWithDraggedViewWithItem:item];

  CGPoint point = _controller.view.center;
  CGFloat scale = 1.0/3.0;
  id workspaceContentScrollView = [OCMockObject mockForClass:[WorkspaceContentScrollView class]];
  [[[workspaceContentScrollView stub] andReturnValue:OCMOCK_VALUE(point)] convertPoint:point fromView:_controller.view];
  [[[workspaceContentScrollView stub] andReturnValue:OCMOCK_VALUE(scale)] zoomScale];

  long indexInteger = 0;
  id workspaceViewController = [OCMockObject mockForClass:[WorkspaceViewController class]];
  [[[workspaceViewController stub] andReturn:workspaceContentScrollView] contentScrollView];
  [[[workspaceViewController stub] andReturnValue:@NO] isLocationInCorkboardSpace:_controller.view.center];
  [[[workspaceViewController stub] andReturnValue:OCMOCK_VALUE(indexInteger)] feldIndexAtLocation:point];

  _controller.workspaceViewController = workspaceViewController;

  _controller.draggedView.frame = _controller.shielderContainerFrame;

  scale = [_controller scaleOfItemView:_controller.draggedView atLocation:_controller.view.center];

  XCTAssertEqual(scale, 1.0, @"scale is %f but it should be 1.0", scale);
}



@end
