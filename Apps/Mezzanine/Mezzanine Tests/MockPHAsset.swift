//
//  MockPHAsset.swift
//  Mezzanine
//
//  Created by miguel on 20/2/17.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import Photos

class MockedPHAsset: PHAsset {

  var overridenPixelWidth: Int = 0
  var overridenPixelHeight: Int = 0

  override var pixelWidth: Int {

    get {
      return overridenPixelWidth
    }

    set {
      overridenPixelWidth = newValue
    }
  }

  override var pixelHeight: Int {

    get {
      return overridenPixelHeight
    }

    set {
      overridenPixelHeight = newValue
    }
  }
}
