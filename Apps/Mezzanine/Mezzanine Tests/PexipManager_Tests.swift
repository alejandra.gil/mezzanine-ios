//
//  PexipManager_Tests.swift
//  Mezzanine
//
//  Created by miguel on 9/8/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//


import XCTest
import AVFoundation
//import PexKit

@testable import Mezzanine

open class mockAVAudioSessionPortDescription : AVAudioSessionPortDescription {

  var myOwnPortType: String = ""

  override open var portType: String {
    get {
      return myOwnPortType
    }
  }
}


class PexipManager_Tests: XCTestCase {

  var pexipManager: MockPexipManager!
  let infopresenceModel = MZInfopresence()

  override func setUp() {
    super.setUp()

    pexipManager = MockPexipManager.init()
    pexipManager.infopresence = infopresenceModel
  }

  func setUpInfopresenceWithConferenceInformation() {
    infopresenceModel.pexipNode = MockPexipManagerConstants.hostURL
    infopresenceModel.pexipConference = MockPexipManagerConstants.conferenceID
  }

  override func tearDown() {
    super.tearDown()
  }

  // MARK: Connection process Tests

  func testJoinWithConferenceURI() {

    self.setUpInfopresenceWithConferenceInformation()

    pexipManager.conferenceMode = .audioOnly

    pexipManager.joinAsRealPexipManager(.audioVideo)
    XCTAssertTrue(pexipManager.conferenceMode == .audioVideo)

    pexipManager.joinAsRealPexipManager(.audioOnly)
    XCTAssertTrue(pexipManager.conferenceMode == .audioOnly)
  }

  func testJoinWithConferenceURIFails() {

    pexipManager.conferenceMode = .audioOnly

    pexipManager.joinAsRealPexipManager(.audioVideo)

    XCTAssertTrue(pexipManager.conferenceMode == .audioOnly)
    XCTAssertTrue(pexipManager.currentConference().token == nil)
  }


  func testContinuingAfterRequestingTokenWithError() {

    pexipManager.conferenceMode = .audioVideo

    let videoSelfView = PexVideoView()
    pexipManager.currentConference().selfVideoView = videoSelfView

    XCTAssertTrue(videoSelfView.alpha == 1.0, "Video self view should be start being visible")

    pexipManager.continueWithConnectionAfterRequestingToken(Conference(), isSwitching: false) {_ in}

    XCTAssertTrue(videoSelfView.alpha == 1.0, "Video self view visibility should not have been modified")
  }

  func testContinuingAfterRequestingToken() {
    pexipManager.isEscalated = true
    pexipManager.conferenceMode = .audioVideo
    pexipManager.setAudioMuted(true)
    pexipManager.setVideoMuted(true)
    pexipManager.currentConference().uri = ConferenceURI(conference: "conference", hostname: "hostname")

    let videoSelfView = PexVideoView()
    pexipManager.currentConference().selfVideoView = videoSelfView

    XCTAssertTrue(videoSelfView.alpha == 1.0, "Video self view should be start being visible")
    XCTAssertTrue(pexipManager.isAudioMuted(), "Conference audio should be muted")
    XCTAssertTrue(pexipManager.isVideoMuted(), "Conference video should be muted")

    pexipManager.continueWithConnectionAfterRequestingToken(pexipManager.currentConference(), isSwitching: false) {_ in}
    
    // continueWithConnectionAfterRequestingToken sets the alpha value in the main thread
    let expectation = XCTestExpectation(description: "videoSelfView.alpha")
    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
      XCTAssertTrue(videoSelfView.alpha == 0.0, "Video self view visibility should be hidden")
      XCTAssertFalse(self.pexipManager.isAudioMuted(), "Conference audio should not be muted")
      XCTAssertFalse(self.pexipManager.isVideoMuted(), "Conference video should not be muted")
      expectation.fulfill()
    }
    
    wait(for: [expectation], timeout: 5.0)
  }

  func testCompletingConnectionAfterEscalating() {
    pexipManager.isEscalated = true
    pexipManager.conferenceMode = .audioVideo
    pexipManager.setAudioMuted(true)
    pexipManager.setVideoMuted(true)

    XCTAssertTrue(pexipManager.isAudioMuted(), "Conference audio should be muted")
    XCTAssertTrue(pexipManager.isVideoMuted(), "Conference video should be muted")

    pexipManager.completeConnectionAfterEscalating(pexipManager.currentConference(), isSwitching: false) {_ in}

    XCTAssertFalse(pexipManager.isAudioMuted(), "Conference audio should not be muted")
    XCTAssertFalse(pexipManager.isVideoMuted(), "Conference video should not be muted")
  }
  
  
  // MARK:  Update Mute States
  
  func testUpdateMuteTestIsSwitchingEscalated() {
    pexipManager.isEscalated = true
    pexipManager.conferenceMode = .audioVideo
    pexipManager.setAudioMuted(true)
    pexipManager.setVideoMuted(true)
    
    XCTAssertTrue(pexipManager.isAudioMuted(), "Conference audio should be muted")
    XCTAssertTrue(pexipManager.isVideoMuted(), "Conference video should be muted")
    
    pexipManager.updateMuteStates(true)
    
    XCTAssertEqual(pexipManager.isAudioMuted(), pexipManager.currentAudioMutedState, "Conference audio should keep state if it's escalated and switching")
    XCTAssertEqual(pexipManager.isVideoMuted(), pexipManager.currentVideoMutedState, "Conference video should keep state if it's escalated and switching")
  }

  func testUpdateMuteTestIsNotSwitchingEscalated() {
    pexipManager.isEscalated = true
    pexipManager.conferenceMode = .audioVideo
    pexipManager.setAudioMuted(true)
    pexipManager.setVideoMuted(true)
    
    XCTAssertTrue(pexipManager.isAudioMuted(), "Conference audio should be muted")
    XCTAssertTrue(pexipManager.isVideoMuted(), "Conference video should be muted")
   
    pexipManager.updateMuteStates(false)
    
    XCTAssertFalse(pexipManager.isAudioMuted(), "Conference audio should keep state initial false, if it it's escalated but no switching")
    XCTAssertFalse(pexipManager.isVideoMuted(), "Conference video should keep state initial false, if it it's escalated but no switching")
  }
  
  func testUpdateMuteTestIsSwitchingNotEscalated() {
    pexipManager.isEscalated = false
    pexipManager.conferenceMode = .audioVideo
    pexipManager.setAudioMuted(true)
    pexipManager.setVideoMuted(true)
    
    XCTAssertTrue(pexipManager.isAudioMuted(), "Conference audio should be muted")
    XCTAssertTrue(pexipManager.isVideoMuted(), "Conference video should be muted")
    
    pexipManager.updateMuteStates(true)
    
    XCTAssertTrue(pexipManager.isAudioMuted(), "Conference audio should keep state initial true, if it it isn't escalated but no switching")
    XCTAssertTrue(pexipManager.isVideoMuted(), "Conference audio should keep state initial true, if it it isn't escalated but no switching")
  }
  
  func testUpdateMuteTestIsNotSwitchingNotEscalated() {
    pexipManager.isEscalated = false
    pexipManager.conferenceMode = .audioVideo
    pexipManager.setAudioMuted(true)
    pexipManager.setVideoMuted(true)
    
    XCTAssertTrue(pexipManager.isAudioMuted(), "Conference audio should be muted")
    XCTAssertTrue(pexipManager.isVideoMuted(), "Conference video should be muted")
    
    pexipManager.updateMuteStates(false)
    
    XCTAssertEqual(pexipManager.isAudioMuted(), pexipManager.currentAudioMutedState, "Conference audio should keep state if it isn't escalated and not switching")
    XCTAssertEqual(pexipManager.isVideoMuted(), pexipManager.currentVideoMutedState, "Conference audio should keep state if it isn't escalated and not switching")
  }

  
  // MARK: Audio Output Tests

  func testUpdateAudioOutputOnJoinTimeIfNeeded() {
    XCTAssertTrue(pexipManager.currentConference().audioOutput == .defaultDevice, "It should use the default audio output in the first place")

    for portDescription in [AVAudioSessionPortBuiltInSpeaker, AVAudioSessionPortBuiltInReceiver] {
      let aPortDescription = mockAVAudioSessionPortDescription()
      aPortDescription.myOwnPortType = portDescription
      pexipManager.updateAudioOutputIfNeeded([aPortDescription])
      XCTAssertTrue(pexipManager.currentConference().audioOutput == .speaker, "It should use the device speaker as audio output")
    }

    for portDescription in [AVAudioSessionPortLineIn, AVAudioSessionPortBuiltInMic, AVAudioSessionPortHeadsetMic, AVAudioSessionPortLineOut, AVAudioSessionPortHeadphones, AVAudioSessionPortBluetoothA2DP, AVAudioSessionPortHDMI, AVAudioSessionPortAirPlay, AVAudioSessionPortBluetoothLE, AVAudioSessionPortBluetoothHFP, AVAudioSessionPortUSBAudio, AVAudioSessionPortCarAudio] {
      let aPortDescription = mockAVAudioSessionPortDescription()
      aPortDescription.myOwnPortType = portDescription
      pexipManager.updateAudioOutputIfNeeded([aPortDescription])
      XCTAssertTrue(pexipManager.currentConference().audioOutput == .defaultDevice, "It should use the specific audio output")
    }
  }

  func testAudioOutputChanges() {
    let oldDeviceUnavailableNotification = Notification.init(name: NSNotification.Name.AVAudioSessionRouteChange, object: nil, userInfo: [AVAudioSessionRouteChangeReasonKey: AVAudioSessionRouteChangeReason.oldDeviceUnavailable.rawValue])
    let NewDeviceAvailableNotification = Notification.init(name: NSNotification.Name.AVAudioSessionRouteChange, object: nil, userInfo: [AVAudioSessionRouteChangeReasonKey: AVAudioSessionRouteChangeReason.newDeviceAvailable.rawValue])

    XCTAssertTrue(pexipManager.currentConference().audioOutput == .defaultDevice, "It should use the default audio output in the first place")

    pexipManager.audioRouteChangeListenerCallback(NewDeviceAvailableNotification)
    XCTAssertTrue(pexipManager.currentConference().audioOutput == .defaultDevice, "It should continue using the default audio output if a headphone is plugged")

    pexipManager.audioRouteChangeListenerCallback(oldDeviceUnavailableNotification)
    XCTAssertTrue(pexipManager.currentConference().audioOutput == .speaker, "It should change to use the speaker when a headphone is unplugged")

    pexipManager.audioRouteChangeListenerCallback(NewDeviceAvailableNotification)
    XCTAssertTrue(pexipManager.currentConference().audioOutput == .defaultDevice, "It should use again the default audio output if a headphone is plugged")
  }


  // MARK: Background / Foreground Tests

  func testBringApplicationToForeground() {
    pexipManager.registerForApplicationWillEnterForegroundNotification()
    expectation(forNotification: NSNotification.Name(rawValue: NSNotification.Name.UIApplicationWillEnterForeground.rawValue), object: nil) { (NSNotification) -> Bool in return true }
    pexipManager.sendApplicationWillEnterForegroundNotification()
    
    waitForExpectations(timeout: 0.2, handler: nil)
    XCTAssertTrue(pexipManager.reconnectMediaIsCalled)
  }

  // MARK: Video Configuration tests

  func testSetupVideoViews() {

    XCTAssertFalse(pexipManager.areVideoViewsConfigured(), "Video views should not be configured")

    let videoView = PexVideoView()
    let videoSelfView = PexVideoView()

    pexipManager.setupVideoViews(videoView, selfVideoView: videoSelfView)

    XCTAssertTrue(pexipManager.areVideoViewsConfigured(), "Video views should be configured")
  }

  // MARK: Conference URI tests

  func testConferenceURIWithNoInformationAndFail() {

    XCTAssertNil(pexipManager.createConferenceURI(), "Conference URI should be nil")
  }

  func testConferenceURICreation() {

    self.setUpInfopresenceWithConferenceInformation()
    let conferenceURI = pexipManager.createConferenceURIAsRealPexipManager()

    XCTAssertNotNil(conferenceURI, "Conference URI should not be nil")
    XCTAssertTrue(conferenceURI!.host == MockPexipManagerConstants.hostURL, "Host of conference URI is incorrect")
    XCTAssertTrue(conferenceURI!.conference == "\(MockPexipManagerConstants.conferenceID)", "Conference of conference URI is incorrect")
  }


  // MARK: Audio and video muted states

  func testAudioInitiallyMutedChangesState() {

    pexipManager.setAudioMuted(true)
    XCTAssertTrue(pexipManager.isAudioMuted(), "Audio should be muted")
    pexipManager.setAudioMuted(false)
    XCTAssertFalse(pexipManager.isAudioMuted(), "Audio should not be muted")
  }

  func testVideoInitiallyMutedChangesState() {
    
    pexipManager.setVideoMuted(true)
    XCTAssertTrue(pexipManager.isVideoMuted(), "Video should be muted")
    pexipManager.setVideoMuted(false)
    XCTAssertFalse(pexipManager.isVideoMuted(), "Video should not be muted")
  }


}
