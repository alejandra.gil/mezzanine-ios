//
//  ParticipantsRosterTableViewController_Tests.swift
//  Mezzanine Tests
//
//  Created by Ivan Bella Lopez on 11/03/2019.
//  Copyright © 2018 Oblong Industries. All rights reserved.
//

import UIKit
import XCTest
@testable import Mezzanine

class ParticipantsRosterTableViewController_Tests: XCTestCase {

  let viewController = ParticipantsRosterTableViewController()
  let helper = MezzanineRoomHelper()

  override func tearDown() {
    viewController.mezzanines.removeAll()
  }

  
  // MARK: Room Sorting tests

  func testRoomTypeAreSortedAlphabetically() {
    viewController.mezzanines = helper.createRemoteMezzanineRooms(count: 3,type: .remote)
    viewController.mezzanines.append(helper.createMezzanineRoom(name: "BMezzanine", type: .remote, participants: []))
    viewController.mezzanines.append(helper.createMezzanineRoom(name: "AMezzanine", type: .remote, participants: []))

    viewController.sortRoomsAlphabetically()
    XCTAssertTrue(viewController.mezzanines.first?.name == "AMezzanine")
    XCTAssertTrue(viewController.mezzanines[1].name == "BMezzanine")
    XCTAssertTrue(viewController.mezzanines.last?.name == "Room3")
  }

  // Sorting is made by name and room type: local, remote, cloud
  func testRoomsSortedbyType() {
    // Local
    viewController.mezzanines.append(helper.createMezzanineRoom(name: "Local0", type: .local, participants: []))

    // Remote
    viewController.mezzanines.append(helper.createMezzanineRoom(name: "Remote0", type: .remote, participants: []))

    // Cloud
    viewController.mezzanines.append(helper.createMezzanineRoom(name: "Cloud0", type: .cloud, participants: []))

    // Local
    viewController.mezzanines.append(helper.createMezzanineRoom(name: "Local1", type: .local, participants: []))

    viewController.sortRoomsAlphabetically()
    XCTAssertTrue(viewController.mezzanines.first?.name == "Local0")
    XCTAssertTrue(viewController.mezzanines.last?.name == "Cloud0")
  }

  func testRoomsAreSortedAfterInsertingDeleting() {
    viewController.mezzanines.append(helper.createMezzanineRoom(name: "Cloud0", type: .cloud, participants: [helper.createParticipant(name: "remote participant")]))
    viewController.mezzanines.append(contentsOf: helper.createRemoteMezzanineRooms(count: 3, type: .remote))

    viewController.sortRoomsAlphabetically()
    XCTAssertTrue(viewController.mezzanines.first?.name == "Room1")
    XCTAssertTrue(viewController.mezzanines.last?.name == "Cloud0")

    viewController.mezzanines.remove(at: viewController.mezzanines.count - 1)
    viewController.mezzanines.append(helper.createMezzanineRoom(name: "AMezzanineRoom", type: .remote, participants: helper.createParticipants(count: 1)))

    viewController.sortRoomsAlphabetically()
    XCTAssertTrue(viewController.mezzanines.first?.name == "AMezzanineRoom")
    XCTAssertTrue(viewController.mezzanines.last?.name == "Room3")
  }


  // MARK: Participants Sorting tests
  func testsParticipantsAreSortedAlphabetically() {
    let room = helper.createMezzanineRoom(name: "Room", type: .local, participants: helper.createParticipants(count: 5))
    viewController.mezzanines.append(room)

    viewController.mezzanines.first?.insert(helper.createParticipant(name: "zParticipant"), inParticipantsAt: 0)
    viewController.tableView.reloadData()

    var participant = (viewController.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! Mezzanine.ParticipantsRosterTableViewCell).participant
    XCTAssertEqual(participant?.displayName, "Participant1")

    participant = (viewController.tableView.cellForRow(at: IndexPath(row: 5, section: 0)) as! Mezzanine.ParticipantsRosterTableViewCell).participant
    XCTAssertEqual(participant?.displayName, "zParticipant")

    viewController.mezzanines.first?.removeObjectFromParticipants(at: 0)
    viewController.tableView.reloadData()

    participant = (viewController.tableView.cellForRow(at: IndexPath(row: 4, section: 0)) as! Mezzanine.ParticipantsRosterTableViewCell).participant
    XCTAssertEqual(participant?.displayName, "Participant5")
  }
}
