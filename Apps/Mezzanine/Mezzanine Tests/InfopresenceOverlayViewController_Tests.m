//
//  InfopresenceOverlayViewController_Tests.m
//  Mezzanine
//
//  Created by miguel on 24/2/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MezzanineAppContext.h"
#import "InfopresenceOverlayViewController.h"
#import "InfopresenceOverlayViewController_Private.h"
#import "MezzanineMainViewController.h"
#import "Mezzanine-Swift.h"

@interface InfopresenceOverlayViewController_Tests : XCTestCase
{
  InfopresenceOverlayViewController *_controller;
  MezzanineAppContext *_appContext;
}

@end

@implementation InfopresenceOverlayViewController_Tests

- (void)setUp
{
  [super setUp];

  _appContext = [[MezzanineAppContext alloc] init];
  _appContext.applicationModel = [[MezzanineAppModel alloc] init];

  MZCommunicator *communicator = [MZCommunicator new];
  communicator.systemModel = _appContext.applicationModel.systemModel;

  _appContext.currentCommunicator = communicator;

  _controller = [[InfopresenceOverlayViewController alloc] initWithNibName:nil bundle:nil];
}

- (void)tearDown
{
  [super tearDown];
}

- (void)testRemoteMezzRemoveFromRemoteMezzCellOn3dot0
{
  _appContext.currentCommunicator.systemModel.myMezzanine.version = @"3.13";
  _controller.systemModel = _appContext.currentCommunicator.systemModel;

  [_controller view];

  RemoteMezzanineTableViewCell *cell = [_controller valueForKey:@"_remoteMezzView"];
  XCTAssertNotNil(cell, @"A RemoteMezzTableViewCell should exist now.");
  XCTAssertNil(cell.remoteMezz, @"A RemoteMezzTableViewCell should have no MZRemoteMezz object assigned.");

  MZInfopresenceCall *call = [MZInfopresenceCall new];
  call.uid = @"remote-mezz-1234";

  MZRemoteMezz *remoteMezz = [[MZRemoteMezz alloc] init];
  remoteMezz.uid = @"remote-mezz-1234";
  remoteMezz.name = @"The Remote Mezz";
  remoteMezz.location = @"Where the tests live";

  [_appContext.infopresenceManager setValue:call forKey:@"currentCall"];
  _controller.remoteMezz = remoteMezz;
  XCTAssertNotNil(cell, @"A RemoteMezzTableViewCell should exist.");
  XCTAssertNotNil(cell.remoteMezz, @"A RemoteMezzTableViewCell should have a MZRemoteMezz object assigned.");

  _controller.remoteMezz = nil;
  XCTAssertNotNil(cell, @"A RemoteMezzTableViewCell should exist.");
  XCTAssertNil(cell.remoteMezz, @"A RemoteMezzTableViewCell should have no MZRemoteMezz object assigned.");
}

@end
