//
//  SurfaceGeometry_Tests.swift
//  Mezzanine
//
//  Created by miguel on 23/2/17.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import XCTest

@testable import Mezzanine

class SurfaceGeometry_Tests: XCTestCase {

  override func setUp() {
    super.setUp()
  }

  override func tearDown() {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    super.tearDown()
  }

  
  func testRectForSurfaceWithOneExtendedSurfaceConfiguration() {

    let systemModel = MockSystemModel().createTestWithOneExtendedWindshieldSurfaceWithOneFeld()!
    let containerRect = CGRect(x: 0, y: 0, width: 500, height: 500)

    let surfaceGeometry = SurfaceGeometry()
    surfaceGeometry.containerRect = containerRect
    surfaceGeometry.surfaces = systemModel.surfaces as NSArray as? [MZSurface]

    let surfaceRect = surfaceGeometry.rectForSurface(systemModel.surfaces[0] as! MZSurface)
    XCTAssertTrue(surfaceRect != CGRect.zero)

    let extendedSurfaceRect = surfaceGeometry.rectForExtendedSurface(systemModel.surfaces[0] as! MZSurface)
    XCTAssertTrue(extendedSurfaceRect != CGRect.zero)

    XCTAssertTrue(extendedSurfaceRect.contains(surfaceRect))
    XCTAssertFalse(surfaceRect.contains(extendedSurfaceRect))
  }


  func testRectForSurfaceWithOneAndOneConfiguration() {

    let systemModel = MockSystemModel().createTestWithOneSingleFeldAndOneExtendedWindshieldSurfaceWithOneFeld()!
    let containerRect = CGRect(x: 0, y: 0, width: 660, height: 500)

    let surfaceGeometry = SurfaceGeometry()
    surfaceGeometry.containerRect = containerRect
    surfaceGeometry.surfaces = systemModel.surfaces as NSArray as? [MZSurface]

    let mainSurfaceRect = surfaceGeometry.rectForSurface(systemModel.surfaces[0] as! MZSurface)
    let sideSurfaceRect = surfaceGeometry.rectForSurface(systemModel.surfaces[1] as! MZSurface)

    XCTAssertTrue(mainSurfaceRect != CGRect.zero)
    XCTAssertTrue(sideSurfaceRect != CGRect.zero)
    XCTAssertFalse(mainSurfaceRect.contains(sideSurfaceRect))
    XCTAssertFalse(sideSurfaceRect.contains(mainSurfaceRect))

    let mainExtendedSurfaceRect = surfaceGeometry.rectForExtendedSurface(systemModel.surfaces[0] as! MZSurface)
    let sideExtendedSurfaceRect = surfaceGeometry.rectForExtendedSurface(systemModel.surfaces[1] as! MZSurface)

    XCTAssertTrue(mainExtendedSurfaceRect != CGRect.zero)
    XCTAssertTrue(sideExtendedSurfaceRect != CGRect.zero)
    XCTAssertFalse(mainExtendedSurfaceRect.contains(sideExtendedSurfaceRect))
    XCTAssertFalse(sideExtendedSurfaceRect.contains(mainExtendedSurfaceRect))

    XCTAssertTrue(mainExtendedSurfaceRect.contains(mainSurfaceRect))
    XCTAssertFalse(mainSurfaceRect.contains(mainExtendedSurfaceRect))

    XCTAssertTrue(sideExtendedSurfaceRect.contains(sideSurfaceRect))
    XCTAssertFalse(sideSurfaceRect.contains(sideExtendedSurfaceRect))

    XCTAssertEqual(round(abs(mainSurfaceRect.maxX - sideSurfaceRect.minX)*10000.0)/10000.0, round(2 * surfaceGeometry.interItemHorizontalMargin * 10000.0) / 10000.0)
    XCTAssertEqual(mainSurfaceRect.width + sideSurfaceRect.width + 4 * surfaceGeometry.interItemHorizontalMargin + 2 * surfaceGeometry.borderHorizontalMargin, containerRect.width)
  }


  func testRectForSurfaceWithThreeAndOneConfiguration() {

    let systemModel = MockSystemModel().createTestWithTriptychAndOneExtendedWindshieldSurfaceWithOneFeld()!
    let containerRect = CGRect(x: 0, y: 0, width: 660, height: 500)

    let surfaceGeometry = SurfaceGeometry()
    surfaceGeometry.containerRect = containerRect
    surfaceGeometry.surfaces = systemModel.surfaces as NSArray as? [MZSurface]

    let mainSurfaceRect = surfaceGeometry.rectForSurface(systemModel.surfaces[0] as! MZSurface)
    let sideSurfaceRect = surfaceGeometry.rectForSurface(systemModel.surfaces[1] as! MZSurface)

    XCTAssertTrue(mainSurfaceRect != CGRect.zero)
    XCTAssertTrue(sideSurfaceRect != CGRect.zero)
    XCTAssertFalse(mainSurfaceRect.contains(sideSurfaceRect))
    XCTAssertFalse(sideSurfaceRect.contains(mainSurfaceRect))

    let mainExtendedSurfaceRect = surfaceGeometry.rectForExtendedSurface(systemModel.surfaces[0] as! MZSurface)
    let sideExtendedSurfaceRect = surfaceGeometry.rectForExtendedSurface(systemModel.surfaces[1] as! MZSurface)

    XCTAssertTrue(mainExtendedSurfaceRect != CGRect.zero)
    XCTAssertTrue(sideExtendedSurfaceRect != CGRect.zero)
    XCTAssertFalse(mainExtendedSurfaceRect.contains(sideExtendedSurfaceRect))
    XCTAssertFalse(sideExtendedSurfaceRect.contains(mainExtendedSurfaceRect))

    XCTAssertTrue(mainExtendedSurfaceRect.contains(mainSurfaceRect))
    XCTAssertFalse(mainSurfaceRect.contains(mainExtendedSurfaceRect))

    XCTAssertTrue(sideExtendedSurfaceRect.contains(sideSurfaceRect))
    XCTAssertFalse(sideSurfaceRect.contains(sideExtendedSurfaceRect))

    XCTAssertEqual(round(abs(mainSurfaceRect.maxX - sideSurfaceRect.minX)*10000.0)/10000.0, round(2 * surfaceGeometry.interItemHorizontalMargin * 10000.0) / 10000.0)
    XCTAssertEqual(mainSurfaceRect.width + sideSurfaceRect.width + 4 * surfaceGeometry.interItemHorizontalMargin + 2 * surfaceGeometry.borderHorizontalMargin, containerRect.width)
  }
}

