//
//  MezzanineAnalyticsManager_Tests.swift
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 06/04/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

import XCTest
@testable import Mezzanine


extension XCTestCase {
  func waitToContinue(_ seconds: Double){
    RunLoop.current.run(until: Date(timeIntervalSinceNow: seconds))
  }
}


class MezzanineAnalyticsManager_Tests: XCTestCase {
  
  let unitTestEventString = "unit test"
  let unitTestAttributes = ["testKey" : "testValue"]
  
  let communicator = MZCommunicator()
  
  var analyticsManager: MezzanineAnalyticsManager!
  var appContext: MezzanineAppContext!

  override func setUp() {
    super.setUp()
    
    analyticsManager = MezzanineAnalyticsManager()
    appContext = MezzanineAppContext.current()
    appContext.currentCommunicator = communicator
  }
  
  override func tearDown() {
    super.tearDown()
    
    analyticsManager = nil
    appContext.mainNavController = MezzanineNavigationController()
    appContext.analyticsViewController = nil
  }
  
  
  // MARK: Navigation tests
  
  func DISABLED_testDialogAppeareancePasskey() {
    setupAppContext()
    
    let passphraseViewController = PassphraseViewController()
    appContext.passphraseViewController = passphraseViewController
    let navigationController = MezzanineNavigationController(rootViewController: UIViewController());
    appContext.window.rootViewController = navigationController
    appContext.mainNavController = navigationController
    appContext.mainNavController.present(passphraseViewController, animated: false, completion: nil)

    appContext.applicationModel.systemModel.state = .workspace
    waitToContinue(0.3)
    let navController = appContext.passphraseViewController.presentedViewController as! UINavigationController
    XCTAssertEqual(navController.topViewController!, appContext.analyticsViewController)
  }

  
  func testDialogAppearanceWithoutPasskey() {
    setupAppContext()
    
    let navigationController = MezzanineNavigationController(rootViewController: UIViewController());
    appContext.window.rootViewController = navigationController
    appContext.mainNavController = navigationController
    
    appContext.applicationModel.systemModel.state = .workspace
    waitToContinue(0.3)
    let navController = appContext.mainNavController.presentedViewController as! UINavigationController
    XCTAssertEqual(navController.topViewController!, appContext.analyticsViewController)
  }
  
  
  func setupAppContext() {
    analyticsManager.optInDialogShown = false
    analyticsManager.amountTimesAppStarted = 2
    
    // Let's have the context set up
    let mockSystemModel = MockSystemModel()
    guard let systemModel = mockSystemModel.createTestWithSingleFeld() else { return }

    systemModel.surfaces = [MZSurface(name: "main")]
    systemModel.state = .notConnected
    let appModel = MezzanineAppModel()
    appModel.systemModel = systemModel
    
    appContext = MezzanineAppContext.current()
    appContext.applicationModel = appModel
  }
  
  
  // MARK: Dialog tests
  
  func testShowOnlyOnceDialog() {
    analyticsManager.optInDialogShown = true
    analyticsManager.amountTimesAppStarted = 4
    XCTAssertFalse(analyticsManager.canShowAnalyticsOptInDialog())
    
    analyticsManager.optInDialogShown = true
    analyticsManager.amountTimesAppStarted = 0
    XCTAssertFalse(analyticsManager.canShowAnalyticsOptInDialog())
    
    analyticsManager.optInDialogShown = false
    analyticsManager.amountTimesAppStarted = 0
    XCTAssertFalse(analyticsManager.canShowAnalyticsOptInDialog())
    
    analyticsManager.optInDialogShown = false
    analyticsManager.amountTimesAppStarted = 4
    XCTAssertTrue(analyticsManager.canShowAnalyticsOptInDialog())
  }
}
