//
//  ParticipantsTableViewController_Tests.swift
//  Mezzanine
//
//  Created by miguel on 15/2/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

import UIKit
import XCTest
@testable import Mezzanine

class ParticipantsTableViewController_Tests: XCTestCase {

  var viewController: ParticipantsTableViewController?
  var systemModel: MZSystemModel?
  var communicator: MZCommunicator?

  override func setUp() {
    super.setUp()

    systemModel = MZSystemModel()
    communicator = MZCommunicator()
    communicator?.systemModel = systemModel

    viewController = ParticipantsTableViewController.init(systemModel: systemModel!, communicator: communicator!)
  }

  override func tearDown() {
      // Put teardown code here. This method is called after the invocation of each test method in the class.
      super.tearDown()
  }


  func testOrderOfDifferentTypesOfParticipants() {
    XCTAssertTrue((viewController?.participants.isEmpty)!, "We should start with an empty list of participants")

    let myMezz = MZRemoteMezz.init()
    myMezz.name = "My Mezz"
    systemModel?.infopresence.insert(myMezz, inRoomsAt: 0)

    XCTAssertTrue(viewController?.participants.count == 1, "List of participants should have 1 item")
    XCTAssertTrue(viewController?.participants[0] as! MZRemoteMezz == myMezz, "Item #0 should be My Mezzanine")

    let remoteMezz1 = MZRemoteMezz.init()
    remoteMezz1.uid = "rm-mz-1"
    remoteMezz1.name = "Remote Mezz 1"
    remoteMezz1.location = "Test Location 1"
    remoteMezz1.company = "Test Company 1"
    remoteMezz1.collaborationState = .inCollaboration
    systemModel?.infopresence.insert(remoteMezz1, inRoomsAt: 1)

    XCTAssertTrue(viewController?.participants.count == 2, "List of participants should have 1 item")
    XCTAssertTrue(viewController?.participants[0] as! MZMezzanine == myMezz, "Item #0 should be My Mezzanine")
    XCTAssertTrue(viewController?.participants[1] as! MZRemoteMezz == remoteMezz1, "Item #1 should be Remote Mezz 1")

    let remoteParticipant1 = MZRemoteParticipant.init()
    remoteParticipant1.uid = "rm-pt-1"
    remoteParticipant1.displayName = "Remote Participant 1"
    systemModel?.infopresence.insert(remoteParticipant1, inRemoteParticipantsAt: 0)

    XCTAssertTrue(viewController?.participants.count == 3, "List of participants should have 3 item")
    XCTAssertTrue(viewController?.participants[0] as! MZMezzanine == myMezz, "Item #0 should be My Mezzanine")
    XCTAssertTrue(viewController?.participants[1] as! MZRemoteMezz == remoteMezz1, "Item #1 should be Remote Mezz 1")
    XCTAssertTrue(viewController?.participants[2] as! MZRemoteParticipant == remoteParticipant1, "Item #2 should be Remote Participant 1")

    let remoteMezz0 = MZRemoteMezz.init()
    remoteMezz0.uid = "rm-mz-0"
    remoteMezz0.name = "Remote Mezz 0"
    remoteMezz0.location = "Test Location 0"
    remoteMezz0.company = "Test Company 0"
    systemModel?.infopresence.insert(remoteMezz0, inRoomsAt: 2)

    XCTAssertTrue(viewController?.participants.count == 4, "List of participants should have 4 item")
    XCTAssertTrue(viewController?.participants[0] as! MZMezzanine == myMezz, "Item #0 should be My Mezzanine")
    XCTAssertTrue(viewController?.participants[1] as! MZRemoteMezz == remoteMezz0, "Item #1 should be Remote Mezz 0")
    XCTAssertTrue(viewController?.participants[2] as! MZRemoteMezz == remoteMezz1, "Item #2 should be Remote Mezz 1")
    XCTAssertTrue(viewController?.participants[3] as! MZRemoteParticipant == remoteParticipant1, "Item #3 should be Remote Participant 1")
  }

  func testOrderOfDifferentTypesOfParticipantsAfterChangingTheirNames() {

    XCTAssertTrue((viewController?.participants.isEmpty)!, "We should start with an empty list of participants")

    let myMezz = MZRemoteMezz.init()
    myMezz.name = "My Mezz"
    systemModel?.infopresence.insert(myMezz, inRoomsAt: 0)

    XCTAssertTrue(viewController?.participants.count == 1, "List of participants should have 1 item")
    XCTAssertTrue(viewController?.participants[0] as! MZRemoteMezz == myMezz, "Item #0 should be My Mezzanine")

    let remoteMezz1 = MZRemoteMezz.init()
    remoteMezz1.uid = "rm-mz-1"
    remoteMezz1.name = "Remote Mezz 1"
    remoteMezz1.location = "Test Location 1"
    remoteMezz1.company = "Test Company 1"
    remoteMezz1.collaborationState = .inCollaboration
    systemModel?.infopresence.insert(remoteMezz1, inRoomsAt: 1)

    let remoteParticipant1 = MZRemoteParticipant.init()
    remoteParticipant1.uid = "rm-pt-1"
    remoteParticipant1.displayName = "Remote Participant 1"
    systemModel?.infopresence.insert(remoteParticipant1, inRemoteParticipantsAt: 0)

    let remoteParticipant0 = MZRemoteParticipant.init()
    remoteParticipant0.uid = "rm-pt-0"
    remoteParticipant0.displayName = "Remote Participant 0"
    systemModel?.infopresence.insert(remoteParticipant0, inRemoteParticipantsAt: 1)

    let remoteMezz0 = MZRemoteMezz.init()
    remoteMezz0.uid = "rm-mz-0"
    remoteMezz0.name = "Remote Mezz 0"
    remoteMezz0.location = "Test Location 0"
    remoteMezz0.company = "Test Company 0"
    systemModel?.infopresence.insert(remoteMezz0, inRoomsAt: 2)

    XCTAssertTrue(viewController?.participants.count == 5, "List of participants should have 1 item")
    XCTAssertTrue(viewController?.participants[0] as! MZMezzanine == myMezz, "Item #1 should be My Mezzanine")
    XCTAssertTrue(viewController?.participants[1] as! MZRemoteMezz == remoteMezz0, "Item #1 should be Remote Mezz 0")
    XCTAssertTrue(viewController?.participants[2] as! MZRemoteMezz == remoteMezz1, "Item #2 should be Remote Mezz 1")
    XCTAssertTrue(viewController?.participants[3] as! MZRemoteParticipant == remoteParticipant0, "Item #5 should be Remote Participant 0")
    XCTAssertTrue(viewController?.participants[4] as! MZRemoteParticipant == remoteParticipant1, "Item #6 should be Remote Participant 1")

    remoteMezz0.name = "Remote Mezz 5"
    remoteParticipant0.displayName = "Remote Participant 3"

    XCTAssertTrue(viewController?.participants.count == 5, "List of participants should have 1 item")
    XCTAssertTrue(viewController?.participants[0] as! MZMezzanine == myMezz, "Item #1 should be My Mezzanine")
    XCTAssertTrue(viewController?.participants[1] as! MZRemoteMezz == remoteMezz1, "Item #2 should be Remote Mezz 1")
    XCTAssertTrue(viewController?.participants[2] as! MZRemoteMezz == remoteMezz0, "Item #1 should be Remote Mezz 0")
    XCTAssertTrue(viewController?.participants[3] as! MZRemoteParticipant == remoteParticipant1, "Item #6 should be Remote Participant 1")
    XCTAssertTrue(viewController?.participants[4] as! MZRemoteParticipant == remoteParticipant0, "Item #5 should be Remote Participant 0")
  }

}
