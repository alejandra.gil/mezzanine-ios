//
//  SessionPageViewController_Tests.swift
//  Mezzanine
//
//  Created by miguel on 21/10/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

import XCTest

@testable import Mezzanine

class SessionPageViewController_Tests: XCTestCase {

  var controller: SessionPageViewController?
  var contentController: UIViewController?
  var systemModelFactory: MockSystemModel?

  override func setUp() {
    super.setUp()
    contentController = UIViewController()
    controller = SessionPageViewController(controller: contentController!, swipeAreaHeight: 50.0, swipeDirection: .left)

    // Force view initialization
    _ = controller!.view
  }

  override func tearDown() {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    super.tearDown()
  }

  func testConfigurePageSwipeAbility() {
    controller!.setSwipeable(false, animated: false)

    XCTAssertTrue(controller!.swipeableAreaHeightConstraint.constant == 0)
    XCTAssertTrue(controller!.swipeableAreaView.alpha == 0.0)

    controller!.setSwipeable(true, animated: false)

    XCTAssertTrue(controller!.swipeableAreaHeightConstraint.constant == 50.0)
    XCTAssertTrue(controller!.swipeableAreaView.alpha == 1.0)
  }
}
