//
//  UIImage+Additions.h
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 14/10/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Additions)

+ (UIImage *)imageOfSize:(CGSize)size filledWithColor:(UIColor *)color;

@end
