//
//  MezzanineDocumentInteractionController_Tests.m
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 14/12/2016.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MezzanineAppContext.h"
#import "MezzanineDocumentInteractionController.h"
#import "UIImage+Additions.h"

@interface MezzanineDocumentInteractionController_Tests : XCTestCase

@property (nonatomic, strong) MZCommunicator *communicator;
@property (nonatomic, strong) MezzanineDocumentInteractionController *controller;
@property (nonatomic, strong) MZSystemModel *systemModel;

@property (nonatomic, strong) NSString *documentsDirectory;
@end

@implementation MezzanineDocumentInteractionController_Tests

- (void)setUp
{
  [super setUp];
  _communicator = [MZCommunicator new];
  _systemModel = [MZSystemModel new];
  
  _communicator.systemModel = _systemModel;
  [MezzanineAppContext currentContext].currentCommunicator = _communicator;
  
  _controller = [MezzanineDocumentInteractionController new];
  
  _documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
}


- (void)tearDown
{
  [super tearDown];
  [self removeImage];
}


- (void)testCanUploadDocumentAtURLSizeExceeded
{
  MZUploadLimits *uploadLimits = [MZUploadLimits new];
  uploadLimits.maxImageWidth = @40;
  uploadLimits.maxImageHeight = @40;
  uploadLimits.maxImageSizeInMB = @(INT_MAX);
  uploadLimits.maxPDFSizeInMB = @(INT_MAX);
  uploadLimits.maxGlobalSizeInMB = @(INT_MAX);
  _systemModel.uploadLimits = uploadLimits;
  
  UIImage *image = [UIImage imageOfSize:CGSizeMake(50, 50) filledWithColor:[UIColor blackColor]];
  [self saveImage:image];
  NSURL *url = [self loadImage];
  NSError *error = nil;
  
  XCTAssertFalse([_controller canUploadDocumentAtURL:url error:&error]);
  XCTAssertNotNil(error);
}


- (void)testCanUploadDocumentAtURLFileSizeExceeded
{
  MZUploadLimits *uploadLimits = [MZUploadLimits new];
  uploadLimits.maxImageWidth = @(INT_MAX);
  uploadLimits.maxImageHeight = @(INT_MAX);
  uploadLimits.maxImageSizeInMB = @1;
  uploadLimits.maxPDFSizeInMB = @1;
  uploadLimits.maxGlobalSizeInMB = @1;
  _systemModel.uploadLimits = uploadLimits;

  CGFloat imageSide = [[UIScreen mainScreen] scale] == 2.0 ? 5000 : 5000 * 2.0;
  UIImage *image = [UIImage imageOfSize:CGSizeMake(imageSide, imageSide) filledWithColor:[UIColor blackColor]];
  [self saveImage:image];
  NSURL *url = [self loadImage];
  NSError *error = nil;
  
  XCTAssertFalse([_controller canUploadDocumentAtURL:url error:&error]);
  XCTAssertNotNil(error);
}


- (void)testCanUploadDocumentAtURLFileCorrect
{
  MZUploadLimits *uploadLimits = [MZUploadLimits new];
  uploadLimits.maxImageWidth = @(40);
  uploadLimits.maxImageHeight = @(40);
  uploadLimits.maxImageSizeInMB = @1;
  uploadLimits.maxPDFSizeInMB = @1;
  uploadLimits.maxGlobalSizeInMB = @1;
  _systemModel.uploadLimits = uploadLimits;
  
  UIImage *image = [UIImage imageOfSize:CGSizeMake(10, 10) filledWithColor:[UIColor blackColor]];
  [self saveImage:image];
  NSURL *url = [self loadImage];
  NSError *error = nil;
  
  XCTAssertTrue([_controller canUploadDocumentAtURL:url error:&error]);
  XCTAssertNil(error);
}


- (void)testCanUploadDocumentAtURLMegapixelsExceeded
{
  MZUploadLimits *uploadLimits = [MZUploadLimits new];
  uploadLimits.maxImageSizeInMP = @1;
  uploadLimits.maxImageSizeInMB = @(INT_MAX);
  uploadLimits.maxPDFSizeInMB = @(INT_MAX);
  uploadLimits.maxGlobalSizeInMB = @(INT_MAX);
  _systemModel.uploadLimits = uploadLimits;
  _systemModel.featureToggles.largeImagesEnabled = YES;
  
  CGFloat imageSide = [[UIScreen mainScreen] scale] == 2.0 ? 600 : 600 * 2.0;
  UIImage *image = [UIImage imageOfSize:CGSizeMake(imageSide, imageSide) filledWithColor:[UIColor blackColor]];
  [self saveImage:image];
  NSURL *url = [self loadImage];
  NSError *error = nil;
  
  XCTAssertFalse([_controller canUploadDocumentAtURL:url error:&error]);
  XCTAssertNotNil(error);
  
  NSString *errorString = [NSString stringWithFormat:NSLocalizedString(@"Image File Upload Exceeded Megapixels Error Dialog Message", nil), @"image.png", uploadLimits.maxImageSizeInMP];
  XCTAssertTrue([error.userInfo[NSLocalizedDescriptionKey] isEqualToString: errorString]);
}


#pragma mark - Private

- (void)saveImage:(UIImage *)image
{
  [UIImagePNGRepresentation(image) writeToFile:[_documentsDirectory stringByAppendingPathComponent:@"image.png"] options:NSAtomicWrite error:nil];
}


-(NSURL *)loadImage
{
  return [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/image.png", _documentsDirectory]];
}


- (void)removeImage
{
  NSString *filePath = [_documentsDirectory stringByAppendingPathComponent:@"image.png"];
  
  NSError *error;
  BOOL success = [[NSFileManager defaultManager] removeItemAtPath:filePath error:&error];
  if (!success)
    NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
}

@end
