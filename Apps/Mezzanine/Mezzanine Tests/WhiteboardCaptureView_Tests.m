//
//  WhiteboardCaptureView_Tests.m
//  Mezzanine
//
//  Created by Zai Chang on 12/4/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "WhiteboardCaptureViewController.h"
#import "MockSystemModel.h"


@interface WhiteboardCaptureView_Tests : XCTestCase
{
  MockSystemModel *_systemModelFactory;

  WhiteboardCaptureViewController *_controller;
  WhiteboardCaptureViewDataSource *_dataSource;
}
@end



@implementation WhiteboardCaptureView_Tests

- (void)setUp
{
  [super setUp];
  
  _systemModelFactory = [[MockSystemModel alloc] init];

  _dataSource = [[WhiteboardCaptureViewDataSource alloc] init];
  _controller = [[WhiteboardCaptureViewController alloc] initWithNibName:nil bundle:nil];
  [_controller view]; // Forcing view to load
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}


- (void)testWhiteboardCaptureViewController
{
  [_controller viewDidLoad]; // Force some initialization
  _dataSource = _controller.dataSource;

  _controller.systemModel = [_systemModelFactory createTestModelWithWhiteboards:0];
  
  XCTAssertEqual(_controller.systemModel, _dataSource.systemModel, @"WhiteboardCaptureViewController should propergate workspace changes to data source");
}


- (void)internalTestWhiteboardCaptureDataSource:(NSInteger)numberOfWhiteboards
{
  _dataSource.systemModel = [_systemModelFactory createTestModelWithWhiteboards:numberOfWhiteboards];
  
  UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
  [_dataSource setupTableView:tableView];
  
  XCTAssertEqual(_dataSource.systemModel.whiteboards.count, [_dataSource tableView:tableView numberOfRowsInSection:0]);
  
  for (NSInteger i=0; i<_dataSource.systemModel.whiteboards.count; i++)
  {
    UITableViewCell *cell = [_dataSource tableView:tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
    XCTAssertEqualObjects(cell.textLabel.text, _dataSource.systemModel.whiteboards[i][@"name"]);
  }
}


- (void)testWhiteboardCaptureDataSourceWithZeroWhiteboards
{
  [self internalTestWhiteboardCaptureDataSource:0];
}


- (void)testWhiteboardCaptureDataSourceWithOneWhiteboard
{
  [self internalTestWhiteboardCaptureDataSource:1];
}

- (void)testWhiteboardCaptureDataSourceWithTwoWhiteboards
{
  [self internalTestWhiteboardCaptureDataSource:2];
}

- (void)testWhiteboardCaptureDataSourceWithThreeWhiteboards
{
  [self internalTestWhiteboardCaptureDataSource:3];
}

- (void)testWhiteboardCaptureDataSourceWithMultipleWhiteboards
{
  [self internalTestWhiteboardCaptureDataSource:4];
}


@end
