//
//  LiveStreamsViewController_Tests.m
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 09/10/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "LiveStreamsViewController.h"
#import "livestreamsViewController_Private.h"

#import "MockOBOvum.h"

#import "MezzanineAppContext.h"
#import "LiveStreamCollectionViewCell.h"
#import "MZWorkspace.h"
#import "UIViewController+FPPopover.h"
#import "ButtonMenuViewController.h"
#import <collections/src/NICollectionViewModel.h>
#import "MezzanineMainViewController.h"
#import "MezzanineStyleSheet.h"
#import "AssetViewController.h"
#import "XCTestCase+Animations.h"
#import "MockMezzanineNavigationController.h"

@class NIMutableCollectionViewModel;

@interface LiveStreamsViewController_Tests : XCTestCase

@property (nonatomic, strong) LiveStreamsViewController *controller;
@property (nonatomic, strong) MockMezzanineNavigationController *mockNavigationController;

@end

@implementation LiveStreamsViewController_Tests

- (void)setUp {
  [super setUp];
  
  self.controller = [[LiveStreamsViewController alloc] initWithNibName:@"LiveStreamsViewController" bundle:nil];
  self.controller.workspace = [MZWorkspace new];
  self.controller.workspace.uid = @"ws-current";
  
  MZCommunicator *communicator = [MZCommunicator new];
  communicator.systemModel = [MZSystemModel new];
  communicator.systemModel.myMezzanine.version = @"3.13";
  [MezzanineAppContext currentContext].currentCommunicator = communicator;
  
  _mockNavigationController = [[MockMezzanineNavigationController alloc] initWithRootViewController:_controller];
  [MezzanineAppContext currentContext].mainNavController = _mockNavigationController;

  //load the view so we can access IBOutlets
  [self.controller view];
}

- (void)tearDown
{
  [MezzanineAppContext currentContext].currentCommunicator = nil;
  [MezzanineAppContext currentContext].mainNavController = [MezzanineNavigationController new];
  [self.controller.workspace.liveStreams removeAllObjects];
  self.controller.workspace = nil;
  self.controller = nil;

  [super tearDown];
}


#pragma mark - IBOutlets

- (void)testTitleLabel
{
  XCTAssertNotNil(self.controller.titleLabel);
  XCTAssertNotNil(self.controller.titleLabelLeftConstraint);
  XCTAssertTrue([self.controller.titleLabel.text isEqualToString:@"LIVE STREAMS"]);
}


- (void)testHintLabel
{
  XCTAssertNotNil(self.controller.hintLabel);
  XCTAssertNotNil(self.controller.hintLabelLeftConstraint);
  XCTAssertTrue([self.controller.hintLabel.text isEqualToString:@"Connect a laptop to Mezzanine to share its screen."]);
}


- (void)testCollectionView
{
  XCTAssertNotNil(self.controller.collectionView);
  //  XCTAssertEqual(self.controller.collectionView.dataSource, self.controller.collectionViewModel);
  XCTAssertEqual(self.controller.collectionView.delegate, self.controller);
}


#pragma mark - Background

- (void)testBackgroundColor
{
  XCTAssertTrue([_controller.view.backgroundColor isEqual:[MezzanineStyleSheet sharedStyleSheet].grey40Color]);
}


#pragma mark - OBvum

- (void)testOvumDragEndedFail
{
  id ob = [OCMockObject niceMockForClass:[OBOvum class]];
  [[[ob stub] andReturn:nil] dataObject];
  
  id mezzRequestor = [OCMockObject niceMockForClass:[MZCommunicatorRequestor class]];
  [[mezzRequestor reject] requestLiveStreamRelinquishRequest:OCMOCK_ANY uid:OCMOCK_ANY];
  
  [self.controller ovumDragEnded:ob];
  
  [mezzRequestor verify];
}


- (void)testOvumDragEndedSuccess
{
  MZLiveStream *slide = [MZLiveStream new];
  slide.uid = @"livestream-uid";
  
  
  id ob = [OCMockObject niceMockForClass:[OBOvum class]];
  [[[ob stub] andReturn:slide] dataObject];
  
  id mezzRequestor = [OCMockObject niceMockForClass:[MZCommunicatorRequestor class]];
  [[mezzRequestor expect] requestLiveStreamRelinquishRequest:self.controller.workspace.uid uid:slide.uid];

  [MezzanineAppContext currentContext].currentCommunicator.requestor = mezzRequestor;
  [self.controller ovumDragEnded:ob];
  
  [mezzRequestor verify];
}


- (void)testOvumDragWillBeginFail
{
  id ob = [OCMockObject niceMockForClass:[OBOvum class]];
  [[[ob stub] andReturn:nil] dataObject];
  
  id mezzRequestor = [OCMockObject niceMockForClass:[MZCommunicatorRequestor class]];
  [[mezzRequestor reject] requestLiveStreamGrabRequest:OCMOCK_ANY uid:OCMOCK_ANY];
  
  [self.controller ovumDragWillBegin:ob];
  
  [mezzRequestor verify];
}


- (void)testOvumDragWillBeginSucces
{
  MZLiveStream *slide = [MZLiveStream new];
  slide.uid = @"livestream-uid";
  
  
  id ob = [OCMockObject niceMockForClass:[OBOvum class]];
  [[[ob stub] andReturn:slide] dataObject];
  
  id mezzRequestor = [OCMockObject niceMockForClass:[MZCommunicatorRequestor class]];
  [[mezzRequestor expect] requestLiveStreamGrabRequest:_controller.workspace.uid uid:slide.uid];
  [MezzanineAppContext currentContext].currentCommunicator.requestor = mezzRequestor;
  
  [self.controller ovumDragWillBegin:ob];
  
  [mezzRequestor verify];
}


- (void)testDragViewWillAppear
{
  UIView *dragView = [UIView new];
  [self.controller dragViewWillAppear:dragView inWindow:[UIWindow new] atLocation:CGPointZero];
  XCTAssertTrue(dragView.alpha == 0.75);
}


- (void)testCreateDragRepresentationOfSourceViewLiveStreamCell
{
  LiveStreamCollectionViewCell *cell = [LiveStreamCollectionViewCell new];
  UIView *view = [self.controller createDragRepresentationOfSourceView:cell inWindow:[UIWindow new]];
  XCTAssertTrue([view isKindOfClass:[cell class]]);
}


- (void)testCreateDragRepresentationOfSourceViewNil
{
  UITableViewCell *cell = [UITableViewCell new];
  UIView *view = [self.controller createDragRepresentationOfSourceView:cell inWindow:[UIWindow new]];
  XCTAssertNil(view);
}


- (void)testCreateOvumFromViewNil
{
  UIView *cellView = [UIView new];
  OBOvum *obvum = [self.controller createOvumFromView:cellView];
  
  XCTAssertNil(obvum);
}


- (void)testCreateOvumFromView
{
  NSInteger numberOfItems = 1;
  [self setUpLivestreamWithNumberOfItems:numberOfItems];
  
  LiveStreamCollectionViewCell *cell = [LiveStreamCollectionViewCell new];
  OBOvum *obvum = [self.controller createOvumFromView:cell];
  
  XCTAssertNotNil(obvum);
}


#pragma mark - UICollectionView

- (void)testCellForCollectionView
{
  NSInteger numberOfItems = 1;
  [self setUpLivestreamWithNumberOfItems:numberOfItems];
  [self.controller resetCollectionViewDataSource];
  
  XCTAssertEqual(numberOfItems, [self.controller.collectionView numberOfItemsInSection:0]);
  
  UICollectionViewCell *cell = [self.controller collectionViewModel:(NICollectionViewModel *)self.controller.collectionViewModel
                 cellForCollectionView:self.controller.collectionView
                           atIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]
                            withObject:[self.controller.workspace.liveStreams lastObject]];
  
  
  XCTAssertNotNil(cell);
  XCTAssertTrue([cell isKindOfClass:[LiveStreamCollectionViewCell class]]);
  
  LiveStreamCollectionViewCell *liveStreamCell = (LiveStreamCollectionViewCell *)cell;
  XCTAssertNotNil(liveStreamCell.liveStream);
  
  XCTAssertTrue([[liveStreamCell gestureRecognizers] count] == 1);
}


- (void)testDidSelectItem
{
  [self setUpLivestreamWithNumberOfItems:3];
  [self.controller resetCollectionViewDataSource];

  [self.controller collectionView:self.controller.collectionView didSelectItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
  
  XCTAssertNotNil(self.controller.currentPopover);
  XCTAssertTrue([self.controller.currentPopover.viewController isKindOfClass:[ButtonMenuViewController class]]);
  
  ButtonMenuViewController *menuViewController = (ButtonMenuViewController *)self.controller.currentPopover.viewController;
  XCTAssertEqual( [menuViewController.menuItems count], 3);
  
  XCTAssertNotNil(self.controller.currentPopover.contentView);
}


#pragma mark - Private methods

- (void)testPlaceItemOnScreen
{
  MZLiveStream *liveStream = [MZLiveStream new];
  liveStream.uid = @"uid";
  
  id mezzRequestor = [OCMockObject niceMockForClass:[MZCommunicatorRequestor class]];
  [[mezzRequestor expect] requestWindshieldItemCreateRequest:_controller.workspace.uid contentSource:liveStream.uid surfaceName:OCMOCK_ANY feldRelativeCoords:OCMOCK_ANY feldRelativeSize:OCMOCK_ANY];
  [MezzanineAppContext currentContext].currentCommunicator.requestor = mezzRequestor;
  
  [self.controller placeItemOnScreen:liveStream];;
  
  [mezzRequestor verify];
}


- (void)testViewStreamNil
{
  XCTAssertTrue(_mockNavigationController.pushViewControllerWithTransitionViewInvocations.count == 0);
  [self.controller openAssetViewerFromCell:nil];
  XCTAssertTrue(_mockNavigationController.pushViewControllerWithTransitionViewInvocations.count == 0);
}


- (void)testViewStreamFromCell
{
  MZLiveStream *liveStream = [MZLiveStream new];
  liveStream.uid = @"uid";
  
  LiveStreamCollectionViewCell *cell = [LiveStreamCollectionViewCell new];
  cell.liveStream = liveStream;
  
  XCTAssertTrue(_mockNavigationController.pushViewControllerWithTransitionViewInvocations.count == 0);
  [self.controller openAssetViewerFromCell:cell];
  XCTAssertTrue(_mockNavigationController.pushViewControllerWithTransitionViewInvocations.count == 1);
}


- (void)testObservingLiveStream
{
  [self setUpLivestreamWithNumberOfItems:1];
  [self.controller resetCollectionViewDataSource];
  MZLiveStream *liveStream = [self.controller.workspace.liveStreams firstObject];
  
  
  [self.controller beginObservingLiveStream:liveStream];
  id observation = [liveStream observationInfo];
  id observer = [[[observation valueForKey:@"_observances"] firstObject] valueForKey:@"_observer"] ;
  XCTAssertEqual(observer, self.controller);
  
  
  [self.controller endObservingLiveStream:liveStream];
  observation = [liveStream observationInfo];
  observer = [[[observation valueForKey:@"_observances"] firstObject] valueForKey:@"_observer"] ;
  XCTAssertNil(observer);
}



- (void)testIsValidStreamTrue
{
  MZLiveStream *item = [MZLiveStream new];
  item.uid = @"vi-bmagic/_local_";
  item.active = YES;
  
  XCTAssertTrue([self.controller isValidLiveStream:item]);
}


- (void)testIsValidStreamNoLocal
{
  MZLiveStream *item = [MZLiveStream new];
  item.uid = @"vi-bmagic/";
  item.active = YES;
  
  XCTAssertFalse([self.controller isValidLiveStream:item]);
}


- (void)testIsValidStreamInactive
{
  MZLiveStream *item = [MZLiveStream new];
  item.uid = @"vi-bmagic/_local_";
  item.active = NO;
  
  XCTAssertFalse([self.controller isValidLiveStream:item]);
}


- (void)testValidStreamsFromModelTrue
{
  NSInteger numberOfLivestreams = 2;
  [self setUpLivestreamWithNumberOfItems:numberOfLivestreams];
  XCTAssertEqual([self.controller.workspace.liveStreams count], numberOfLivestreams);
}


- (void)testValidStreamsFromModelFalse
{
  NSInteger numberOfLivestreams = 2;
  [self setUpLivestreamWithNumberOfItems:numberOfLivestreams];
  ((MZLiveStream *)[self.controller.workspace.liveStreams lastObject]).active = NO;
  ((MZLiveStream *)[self.controller.workspace.liveStreams lastObject]).uid = @"";
  
  XCTAssertEqual([[self.controller validStreamsFromModel] count], numberOfLivestreams - 1);
}


#pragma mark - Helpers

- (void)setUpLivestreamWithNumberOfItems:(NSInteger)numberOfItems
{
  NSMutableArray *array = [@[] mutableCopy];
  for (NSInteger c = 0; c < numberOfItems; c++) {
    
    MZLiveStream * item = [MZLiveStream new];
    item.uid = [NSString stringWithFormat:@"vi-bmagic-%li/_local_", (long)c];
    item.active = YES;
    
    [array addObject: item];
  }
  
  self.controller.workspace.liveStreams = array;
}


@end
