//
//  WorkspaceListViewController_Tests.m
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 19/10/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import "MockMezzanineNavigationController.h"
#import "MockSystemModel.h"
#import "MezzanineAppContext.h"

#import "MezzanineMainViewController.h"
#import "UserSignInViewController.h"
#import "WorkspaceListViewController.h"
#import "WorkspaceListViewController_Private.h"
#import "WorkspaceListCell.h"

#import "FPPopoverController.h"
#import "UIDevice+Machine.h"
#import "XCTestCase+Animations.h"
#import "UITraitCollection+Additions.h"


@interface WorkspaceListViewController_Tests : XCTestCase

@property (nonatomic, strong) WorkspaceListViewController *controller;

@property (nonatomic, strong) NSMutableArray *privateWorkspaces;
@property (nonatomic, strong) NSMutableArray *publicWorkspaces;
@property (nonatomic, strong) MockMezzanineNavigationController *mockNavigationController;

@end


@implementation WorkspaceListViewController_Tests

- (void)setUp {
  
  [super setUp];
  
  //workspaces
  _privateWorkspaces = [@[] mutableCopy];
  _publicWorkspaces = [@[] mutableCopy];
  
  _controller = [WorkspaceListViewController new];
  
  [MezzanineAppContext currentContext].currentCommunicator = [MZCommunicator new];
  _controller.communicator = [MezzanineAppContext currentContext].currentCommunicator;
  _controller.systemModel = [MZSystemModel new];
  _controller.systemModel.myMezzanine.version = @"3.13";

  _mockNavigationController = [[MockMezzanineNavigationController alloc] initWithRootViewController:_controller];

  [MezzanineAppContext currentContext].window.rootViewController = _mockNavigationController;
  
  [_controller view];
}

- (void)tearDown
{
  _privateWorkspaces = nil;
  _publicWorkspaces = nil;
  
  [[NSNotificationCenter defaultCenter] removeObserver:_controller];
  [[NSNotificationCenter defaultCenter] removeObserver:_controller.systemModel];
  
  for (MZWorkspace *workspace in _controller.systemModel.workspaces)
    [[NSNotificationCenter defaultCenter] removeObserver:workspace];
  
  _controller.systemModel = nil;

  [MezzanineAppContext currentContext].currentCommunicator = nil;
  [MezzanineAppContext currentContext].window.rootViewController = [MezzanineNavigationController new];

  [super tearDown];
}


#pragma mark - IBOutlets


- (void)testCollectionViewOutlet
{
  XCTAssertNotNil(_controller.collectionView);
  XCTAssertEqual(_controller.collectionView.dataSource, _controller);
  XCTAssertEqual(_controller.collectionView.delegate, _controller);
}


- (void)testSaveWorkspacesLabelOutlet
{
  XCTAssertNotNil(_controller.title);
  XCTAssertTrue([_controller.title isEqualToString:NSLocalizedString(@"Workspace List Saved Workspaces Title", nil)]);
}


- (void)testWorkspaceNewButtonOutlet
{
  XCTAssertNotNil(_controller.workspaceNewBarButtonItem);
  XCTAssertTrue([[_controller.workspaceNewBarButtonItem title] isEqualToString:@"New"]);
}


- (void)testSignInButtonOutlet
{
  XCTAssertNotNil(_controller.signInBarButtonItem);
  XCTAssertTrue([[_controller.signInBarButtonItem title] isEqualToString:@"Sign In"]);
}


- (void)testDoneButtonOutlet
{
  XCTAssertNotNil(_controller.doneBarButtonItem);
  XCTAssertTrue([[_controller.doneBarButtonItem title] isEqualToString:@"Done"]);
}


- (void)testEmptyWorkspaceListLabelOutlet
{
  XCTAssertNotNil(_controller.emptyWorkspaceListLabel);
  XCTAssertTrue([_controller.emptyWorkspaceListLabel.text isEqualToString:@"No workspaces have been saved yet."]);
}


#pragma mark - Observer updates

- (void)testSignedInChangeSuperuserNoWorkspaces
{
  _controller.systemModel.isCurrentUserSuperuser = YES;
  _controller.systemModel.currentUsername = @"superuser";
  
  NSString *selectedOwner = [_controller valueForKey:@"_selectedOwner"];
  XCTAssertTrue([selectedOwner isEqualToString:_controller.systemModel.currentUsername]);
  XCTAssertTrue([[_controller.signInBarButtonItem title] isEqualToString:_controller.systemModel.currentUsername]);
  
  XCTAssertTrue([_controller.collectionView numberOfItemsInSection:0] == 0);
  XCTAssertFalse(_controller.emptyWorkspaceListLabel.hidden);
}


- (void)testSignInChangeSuperuserWithOnlyPrivateWorkspaces
{
  [self createPrivateWorkspaces:4 withUsername:@"anon-user"];
  [self setWorkspaces:_privateWorkspaces];
  
  _controller.systemModel.isCurrentUserSuperuser = YES;
  _controller.systemModel.currentUsername = @"superuser";
  
  NSString *selectedOwner = [_controller valueForKey:@"_selectedOwner"];
  XCTAssertTrue([selectedOwner isEqualToString:_controller.systemModel.currentUsername]);
  XCTAssertTrue([[_controller.signInBarButtonItem title] isEqualToString:_controller.systemModel.currentUsername]);
  
  XCTAssertTrue([_controller.collectionView numberOfItemsInSection:0] == [_privateWorkspaces count]);
  XCTAssertTrue(_controller.emptyWorkspaceListLabel.hidden);
}


- (void)testSignInChangeSuperuserWithOnlyPublicWorkspaces
{
  [self createPublicWorkspaces:4];
  [self setWorkspaces:_publicWorkspaces];
  
  _controller.systemModel.isCurrentUserSuperuser = YES;
  _controller.systemModel.currentUsername = @"superuser";
  
  NSString *selectedOwner = [_controller valueForKey:@"_selectedOwner"];
  
  XCTAssertTrue([selectedOwner isEqualToString:_controller.systemModel.currentUsername]);
  XCTAssertTrue([[_controller.signInBarButtonItem title] isEqualToString:_controller.systemModel.currentUsername]);
  
  XCTAssertTrue([_controller.collectionView numberOfItemsInSection:0] == [_publicWorkspaces count]);
  XCTAssertTrue(_controller.emptyWorkspaceListLabel.hidden);
}


- (void)testSignInChangeSuperuserWithMixedWorkspaces
{
  [self createPublicWorkspaces:4];
  [self createPrivateWorkspaces:4 withUsername:@"anon-user"];
  
  [self setWorkspaces:_privateWorkspaces];
  [self appendWorkspaces:_publicWorkspaces];
  
  _controller.systemModel.isCurrentUserSuperuser = YES;
  _controller.systemModel.currentUsername = @"superuser";
  
  NSMutableArray *sortedWorkspaces = [_controller valueForKey:@"_sortedWorkspaces"];
  NSString *selectedOwner = [_controller valueForKey:@"_selectedOwner"];
  
  XCTAssertTrue([selectedOwner isEqualToString:_controller.systemModel.currentUsername]);
  XCTAssertTrue([[_controller.signInBarButtonItem title] isEqualToString:_controller.systemModel.currentUsername]);
  
  XCTAssertTrue([_controller.collectionView numberOfItemsInSection:0] == [_publicWorkspaces count] + [_privateWorkspaces count]);
  XCTAssertTrue(_controller.emptyWorkspaceListLabel.hidden);
  
  XCTAssertTrue([((MZWorkspace *)[sortedWorkspaces firstObject]).name isEqualToString:((MZWorkspace *)[_publicWorkspaces firstObject]).name]);
  XCTAssertTrue([((MZWorkspace *)[sortedWorkspaces lastObject]).name isEqualToString:((MZWorkspace *)[_privateWorkspaces lastObject]).name]);
}


- (void)testSignedInChangeRegularUserNoWorkspaces
{
  _controller.systemModel.isCurrentUserSuperuser = NO;
  _controller.systemModel.currentUsername = @"username";
  
  NSString *selectedOwner = [_controller valueForKey:@"_selectedOwner"];
  XCTAssertTrue([selectedOwner isEqualToString:_controller.systemModel.currentUsername]);
  XCTAssertTrue([[_controller.signInBarButtonItem title] isEqualToString:_controller.systemModel.currentUsername]);
  
  XCTAssertTrue([_controller.collectionView numberOfItemsInSection:0] == 0);
  XCTAssertFalse(_controller.emptyWorkspaceListLabel.hidden);
}


- (void)testSignInChangeRegularUserWithOnlyUnknownPrivateWorkspaces
{
  [self createPrivateWorkspaces:4 withUsername:@"anon-user"];
  [self setWorkspaces:_privateWorkspaces];
  
  _controller.systemModel.isCurrentUserSuperuser = NO;
  _controller.systemModel.currentUsername = @"username";
  
  NSString *selectedOwner = [_controller valueForKey:@"_selectedOwner"];
  XCTAssertTrue([selectedOwner isEqualToString:_controller.systemModel.currentUsername]);
  XCTAssertTrue([[_controller.signInBarButtonItem title] isEqualToString:_controller.systemModel.currentUsername]);
  
  XCTAssertTrue([_controller.collectionView numberOfItemsInSection:0] == 0);
  XCTAssertFalse(_controller.emptyWorkspaceListLabel.hidden);
}


- (void)testSignInChangeRegularUserWithOnlyOwnedPrivateWorkspaces
{
  [self createPrivateWorkspaces:4 withUsername:@"username"];
  [self setWorkspaces:_privateWorkspaces];
  
  _controller.systemModel.isCurrentUserSuperuser = NO;
  _controller.systemModel.currentUsername = @"username";
  
  NSString *selectedOwner = [_controller valueForKey:@"_selectedOwner"];
  XCTAssertTrue([selectedOwner isEqualToString:_controller.systemModel.currentUsername]);
  XCTAssertTrue([[_controller.signInBarButtonItem title] isEqualToString:_controller.systemModel.currentUsername]);
  
  XCTAssertTrue([_controller.collectionView numberOfItemsInSection:0] == [_privateWorkspaces count]);
  XCTAssertTrue(_controller.emptyWorkspaceListLabel.hidden);
}


- (void)testSignInChangeRegularUserWithOnlyPublicWorkspaces
{
  [self createPublicWorkspaces:4];
  [self setWorkspaces:_publicWorkspaces];
  
  _controller.systemModel.isCurrentUserSuperuser = NO;
  _controller.systemModel.currentUsername = @"username";
  
  NSString *selectedOwner = [_controller valueForKey:@"_selectedOwner"];
  
  XCTAssertTrue([selectedOwner isEqualToString:_controller.systemModel.currentUsername]);
  XCTAssertTrue([[_controller.signInBarButtonItem title] isEqualToString:_controller.systemModel.currentUsername]);
  
  XCTAssertTrue([_controller.collectionView numberOfItemsInSection:0] == 0);
  XCTAssertFalse(_controller.emptyWorkspaceListLabel.hidden);
}


- (void)testSignInChangeRegularUserWithMixedWorkspaces
{
  NSInteger ownWorkspacesAmount = 2;
  
  [self createPublicWorkspaces:4];
  [self createPrivateWorkspaces:ownWorkspacesAmount withUsername:@"username"];
  [self createPrivateWorkspaces:2 withUsername:@"anon-user"];
  
  [self setWorkspaces:_privateWorkspaces];
  [self appendWorkspaces:_publicWorkspaces];
  
  _controller.systemModel.isCurrentUserSuperuser = NO;
  _controller.systemModel.currentUsername = @"username";
  
  NSMutableArray *sortedWorkspaces = [_controller valueForKey:@"_sortedWorkspaces"];
  NSString *selectedOwner = [_controller valueForKey:@"_selectedOwner"];
  
  XCTAssertTrue([selectedOwner isEqualToString:_controller.systemModel.currentUsername]);
  XCTAssertTrue([[_controller.signInBarButtonItem title] isEqualToString:_controller.systemModel.currentUsername]);
  
  XCTAssertTrue([_controller.collectionView numberOfItemsInSection:0] == ownWorkspacesAmount);
  XCTAssertTrue(_controller.emptyWorkspaceListLabel.hidden);
  
  XCTAssertTrue([((MZWorkspace *)[sortedWorkspaces firstObject]).name isEqualToString:((MZWorkspace *)[_privateWorkspaces firstObject]).name]);
  XCTAssertTrue([((MZWorkspace *)[sortedWorkspaces lastObject]).name isEqualToString:((MZWorkspace *)[_privateWorkspaces lastObject]).name]);
}


- (void)testSignedOutChange
{
  _controller.systemModel.isCurrentUserSuperuser = NO;
  _controller.systemModel.currentUsername = nil;
  
  NSString *selectedOwner = [_controller valueForKey:@"_selectedOwner"];
  XCTAssertNil(selectedOwner);
  XCTAssertTrue([[_controller.signInBarButtonItem title] isEqualToString:@"Sign In"]);
  
  XCTAssertTrue([_controller.collectionView numberOfItemsInSection:0] == 0);
  XCTAssertFalse(_controller.emptyWorkspaceListLabel.hidden);
}


- (void)testUpdateWorkspaceCellBorders
{
  _controller.systemModel.currentWorkspace = [MZWorkspace new];
  
  id cell = [OCMockObject niceMockForClass:[WorkspaceListCell class]];
  
  id partialControllerMock = [OCMockObject partialMockForObject:_controller];
  [[[partialControllerMock stub] andReturn:cell] collectionViewCellForWorkspace:OCMOCK_ANY];
  
  // We expect `updateCellBorder` three times:
  // for key 'currentWorkspace.isLoading'
  [[cell expect] updateCellBorder];
  // for key 'currentWorkspace' with old workspace
  [[cell expect] updateCellBorder];
  // for key 'currentWorkspace' with new workspace
  [[cell expect] updateCellBorder];
  
  _controller.systemModel.currentWorkspace = [MZWorkspace new];
  
  [cell verify];
}


- (void)testInsertRemoveNewWorkspace
{
  MZWorkspace *workspace = [MZWorkspace new];
  id observation;
  NSInteger ocurrences = 0;
  
  // Insertion
  [_controller.systemModel insertWorkspaces:@[workspace] atIndexes:[NSIndexSet indexSetWithIndex:0]];
  observation = [workspace observationInfo];
  
  // There are some other observers but we only care the ones assigned to MezzanineViewController here
  for (id observer in [observation valueForKey:@"_observances"]) {
    if ([observer valueForKey:@"_observer"] == _controller)
      ocurrences++;
  }
  XCTAssertEqual(ocurrences, 1);
  XCTAssertEqual([_controller.collectionView numberOfItemsInSection:0], 1);
  
  // Removal
  [_controller.systemModel removeWorkspacesAtIndexes:[NSIndexSet indexSetWithIndex:0]];
  for (id observer in [observation valueForKey:@"_observances"]) {
    if ([observer valueForKey:@"_observer"] == _controller)
      ocurrences--;
  }
  XCTAssertEqual(ocurrences, 0);
  XCTAssertEqual([_controller.collectionView numberOfItemsInSection:0], 0);
}



- (void)testChangeNameAndSorting
{
  [self createPrivateWorkspaces:2 withUsername:@"username"];
  [self setWorkspaces:_privateWorkspaces];
  
  NSMutableArray *sortedWorkspaces = [@[] mutableCopy];
  
  MZWorkspace *workspace = [MZWorkspace new];
  workspace.name = @"zWorkspace";
  workspace.uid = [NSString stringWithFormat:@"workspace-uid"];
  workspace.owner = @"username";
  
  [_controller.systemModel insertWorkspaces:@[workspace] atIndexes:[NSIndexSet indexSetWithIndex:0]];
  _controller.systemModel.currentUsername = @"username";
  
  sortedWorkspaces = [_controller valueForKey:@"_sortedWorkspaces"];
  XCTAssertTrue([((MZWorkspace *)[sortedWorkspaces lastObject]).name isEqualToString:workspace.name]);
  
  
  MZWorkspace *lastWorkspace = [sortedWorkspaces lastObject];
  [lastWorkspace updateWithDictionary:@{@"name": @"aWorkspace", @"uid": lastWorkspace.uid}];
  sortedWorkspaces = [_controller valueForKey:@"_sortedWorkspaces"];
  XCTAssertTrue([((MZWorkspace *)[sortedWorkspaces firstObject]).name isEqualToString:lastWorkspace.name]);
}


#pragma mark - UICollectionView methods

- (void)testSelectCurrentWorkspace
{
  NSInteger amountOfWorkspaces = 2;
  [self createPublicWorkspaces:amountOfWorkspaces];
  [self setWorkspaces:_publicWorkspaces];
  
  NSMutableArray *sortedWorkspaces = [_controller valueForKey:@"_sortedWorkspaces"];
  _controller.systemModel.currentWorkspace = [sortedWorkspaces firstObject];
  
  id controllerMock = [OCMockObject partialMockForObject:_controller];
  id requestorMock = [OCMockObject niceMockForClass:[MZCommunicatorRequestor class]];
  _controller.communicator.requestor = requestorMock;
  
  [[requestorMock reject] requestWorkspaceOpenRequest:OCMOCK_ANY switchToUid:OCMOCK_ANY];
  [[controllerMock expect] done];
  

  [_controller collectionView:_controller.collectionView didSelectItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
  [controllerMock verify];
}


- (void)testSelectDifferentWorkspace
{
  NSInteger amountOfWorkspaces = 2;
  [self createPublicWorkspaces:amountOfWorkspaces];
  [self setWorkspaces:_publicWorkspaces];
  
  NSMutableArray *sortedWorkspaces = [_controller valueForKey:@"_sortedWorkspaces"];
  _controller.systemModel.currentWorkspace = [sortedWorkspaces firstObject];
  
  id controllerMock = [OCMockObject partialMockForObject:_controller];
  id requestorMock = [OCMockObject niceMockForClass:[MZCommunicatorRequestor class]];
  _controller.communicator.requestor = requestorMock;
  
  [[requestorMock expect] requestWorkspaceOpenRequest:OCMOCK_ANY switchToUid:OCMOCK_ANY];
  [[controllerMock expect] done];

  
  [_controller collectionView:_controller.collectionView didSelectItemAtIndexPath:[NSIndexPath indexPathForItem:1 inSection:0]];
  [controllerMock verify];
}


- (void)testOpenNewWorkspaceUnsaved
{
  NSInteger amountOfWorkspaces = 2;
  [self createPublicWorkspaces:amountOfWorkspaces];
  [self setWorkspaces:_publicWorkspaces];
  
  NSMutableArray *sortedWorkspaces = [_controller valueForKey:@"_sortedWorkspaces"];
  
  MZWorkspace *currentWorkspace = [sortedWorkspaces firstObject];
  currentWorkspace.isSaved = NO;
  [currentWorkspace.windshield.items addObject:[MZItem new]];

  _controller.systemModel.currentWorkspace = currentWorkspace;
  
  id controllerMock = [OCMockObject partialMockForObject:_controller];
  id requestorMock = [OCMockObject niceMockForClass:[MZCommunicatorRequestor class]];
  _controller.communicator.requestor = requestorMock;
  
  [[requestorMock reject] requestWorkspaceOpenRequest:OCMOCK_ANY switchToUid:OCMOCK_ANY];
  [[controllerMock reject] done];
  
  [_controller collectionView:_controller.collectionView didSelectItemAtIndexPath:[NSIndexPath indexPathForItem:1 inSection:0]];
  [controllerMock verify];
  [requestorMock verify];
}


#pragma mark - Public methods

- (void)testScrollToCurrentWorkspace
{
  NSInteger amountOfWorkspaces = 20;
  [self createPublicWorkspaces:amountOfWorkspaces];
  [self setWorkspaces:_publicWorkspaces];
  
  NSMutableArray *sortedWorkspaces = [_controller valueForKey:@"_sortedWorkspaces"];
  
  _controller.systemModel.currentWorkspace = [sortedWorkspaces firstObject];
//  [_controller scrollToCurrentWorkspace:NO];
  XCTAssertTrue(CGPointEqualToPoint(_controller.collectionView.contentOffset, CGPointZero));
  
  _controller.systemModel.currentWorkspace = [sortedWorkspaces lastObject];
//  [_controller scrollToCurrentWorkspace:NO];
  
  WorkspaceListCell *cell = (WorkspaceListCell *)[_controller.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];

  
  XCTAssertTrue([[_controller.collectionView visibleCells] containsObject:cell]);
}


- (void)testNewWorkspaceAction
{
  [_controller newWorkspace];
  
  FPPopoverController *currentPopoverController = [_controller valueForKey:@"currentPopoverController"];
  XCTAssertNotNil(currentPopoverController);
  XCTAssertNotNil(currentPopoverController.view);
  
  XCTAssertNotNil(currentPopoverController.viewController);

  XCTAssertTrue([currentPopoverController.contentView.customTint isEqual:[UIColor whiteColor]]);
}


- (void)testSignInAction
{
  if (_controller.traitCollection.isRegularWidth && _controller.traitCollection.isRegularHeight)
  {
    [_controller signIn];
    FPPopoverController *currentPopoverController = [_controller valueForKey:@"currentPopoverController"];
    XCTAssertNotNil(currentPopoverController);
    XCTAssertNotNil(currentPopoverController.view);
    XCTAssertNotNil(currentPopoverController.viewController);

    XCTAssertTrue([currentPopoverController.contentView.customTint isEqual:[UIColor whiteColor]]);
  }
  else
  {
    XCTAssertTrue(_mockNavigationController.pushViewControllerInvocations.count == 0);
    [_controller signIn];
    XCTAssertTrue(_mockNavigationController.pushViewControllerInvocations.count == 1);
    XCTAssertFalse([UIApplication sharedApplication].statusBarHidden);
  }
}


- (void)testDoneAction
{
  [_controller done];

  id controllerMock = [OCMockObject partialMockForObject:_controller];
  [[controllerMock expect] dismissViewControllerAnimated:YES completion:^{
    
  }];
}


#pragma mark - Workspaces helpers

- (void)createPrivateWorkspaces:(NSInteger)numberOfWorkspaces withUsername:(NSString *)username
{
  for (int c = 0; c < numberOfWorkspaces; c++) {
    
    MZWorkspace *newWorkspace = [MZWorkspace new];
    newWorkspace.uid = [NSString stringWithFormat:@"workspace-private-uid-%i", c];
    newWorkspace.name = [NSString stringWithFormat:@"workspace-private-name-%i", c];
    newWorkspace.owner = username;
    
    [_privateWorkspaces addObject:newWorkspace];
  }
}


- (void)createPublicWorkspaces:(NSInteger)numberOfWorkspaces
{
  for (int c = 0; c < numberOfWorkspaces; c++) {
    
    MZWorkspace *newWorkspace = [MZWorkspace new];
    newWorkspace.uid = [NSString stringWithFormat:@"workspace-public-uid-%i", c];
    newWorkspace.name = [NSString stringWithFormat:@"workspace-public-name-%i", c];
    
    [_publicWorkspaces addObject:newWorkspace];
  }
}


- (void)setWorkspaces:(NSMutableArray *)workspaceArray
{
  NSAssert([workspaceArray count] > 0, @"It should be at least one workspace");
  [_controller.systemModel.workspaces removeAllObjects];
  [self insertWorkspacesToSystemModel:workspaceArray];
}


- (void)appendWorkspaces:(NSMutableArray *)workspaceArray
{
  NSAssert([workspaceArray count] > 0, @"It should be at least one workspace");
  [self insertWorkspacesToSystemModel:workspaceArray];
}


- (void)insertWorkspacesToSystemModel:(NSMutableArray *)workspaceArray
{
  __block NSMutableIndexSet *indexForWorkspaces = [NSMutableIndexSet indexSet];
  [workspaceArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
    [indexForWorkspaces addIndex:idx];
  }];
  
  [_controller.systemModel insertWorkspaces:workspaceArray atIndexes:indexForWorkspaces];
}

@end
