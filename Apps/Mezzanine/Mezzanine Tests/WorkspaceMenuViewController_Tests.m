//
//  WorkspaceMenuViewController_Tests.m
//  Mezzanine
//
//  Created by miguel on 6/4/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MezzanineMainViewController.h"
#import "WorkspaceMenuViewController.h"

@interface MockMZCommunicator : MZCommunicator

@property (nonatomic, assign) BOOL isARemoteParticipationConnection;

@end

@interface WorkspaceMenuViewController_Tests : XCTestCase

@property (nonatomic, strong) WorkspaceMenuViewController *controller;
@property (nonatomic, strong) MZSystemModel *systemModel;
@property (nonatomic, strong) MZWorkspace *workspace;
@property (nonatomic, strong) WorkspaceToolbarViewController *toolbarViewController;

@end


@implementation WorkspaceMenuViewController_Tests

static NSString *WorkspaceName = @"workspace";
static NSString *WorkspaceOwnerName = @"test-user";
static NSString *SignedInUsername = @"test-user";


- (void)setUp
{
  [super setUp];

  _systemModel = [MZSystemModel new];
  _systemModel.myMezzanine.version = @"3.13";

  _workspace = [MZWorkspace new];
  _workspace.name = WorkspaceName;

  MockMZCommunicator *communicator = [MockMZCommunicator new];

  _toolbarViewController = [[WorkspaceToolbarViewController alloc] initWithNibName:@"WorkspaceToolbarViewController" bundle:nil];
  _toolbarViewController.appContext = [MezzanineAppContext currentContext];
  _toolbarViewController.appContext.currentCommunicator = communicator;
  _toolbarViewController.appContext.currentCommunicator.systemModel = _systemModel;

  _controller = [WorkspaceMenuViewController new];
  _controller.systemModel = _systemModel;
  _controller.workspaceToolbarViewController = _toolbarViewController;
}

- (void)tearDown
{
  MezzanineAppContext *appContext = [MezzanineAppContext currentContext];
  [appContext setCurrentCommunicator:nil];
  [appContext.applicationModel reset];
  _controller.workspaceToolbarViewController = nil;
  _controller.systemModel = nil;
  _controller.workspace = nil;
  _controller = nil;

  [super tearDown];
}

- (void)testMenuWithRenameOnPublicWorkspaces
{
  _systemModel.currentUsername = nil;
  _workspace.owner = nil;
  _workspace.isSaved = YES;
  _controller.workspace = _workspace;

  XCTAssertEqual(_controller.menuItems.count, 3, @"Menu should have three options");

  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.buttonTitle contains %@", NSLocalizedString(@"Workspace Rename Button Title", nil)];
  NSArray *filteredArray = [_controller.menuItems filteredArrayUsingPredicate:predicate];

  XCTAssertEqual(filteredArray.count, 1, @"Menu should include Rename option");
}

- (void)testMenuWithRenameOnPrivateWorkspaceIfOwnerIsSignedIn
{
  _systemModel.currentUsername = SignedInUsername;
  _workspace.owner = WorkspaceOwnerName;
  _workspace.isSaved = YES;
  _controller.workspace = _workspace;

  XCTAssertEqual(_controller.menuItems.count, 3, @"Menu should have three options");

  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.buttonTitle contains %@", NSLocalizedString(@"Workspace Rename Button Title", nil)];
  NSArray *filteredArray = [_controller.menuItems filteredArrayUsingPredicate:predicate];

  XCTAssertEqual(filteredArray.count, 1, @"Menu should include Rename option");
}

- (void)testMenuWithoutRenameOnPrivateWorkspaceIfOwnerIsNotSignedIn
{
  _systemModel.currentUsername = nil;
  _workspace.owner = WorkspaceOwnerName;
  _workspace.isSaved = YES;
  _controller.workspace = _workspace;

  XCTAssertEqual(_controller.menuItems.count, 2, @"Menu should have three options");

  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.buttonTitle contains %@", NSLocalizedString(@"Workspace Rename Button Title", nil)];
  NSArray *filteredArray = [_controller.menuItems filteredArrayUsingPredicate:predicate];

  XCTAssertEqual(filteredArray.count, 0, @"Menu should include not Rename option");
}

- (void)testMenuWithoutRenameOnPrivateWorkspaceIfDifferentOwnerIsSignedIn
{
  _systemModel.currentUsername = @"another owner";
  _workspace.owner = WorkspaceOwnerName;
  _workspace.isSaved = YES;
  _controller.workspace = _workspace;

  XCTAssertEqual(_controller.menuItems.count, 2, @"Menu should have three options");

  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.buttonTitle contains %@", NSLocalizedString(@"Workspace Rename Button Title", nil)];
  NSArray *filteredArray = [_controller.menuItems filteredArrayUsingPredicate:predicate];

  XCTAssertEqual(filteredArray.count, 0, @"Menu should include not Rename option");
}

- (void)testMenuWithoutRenameOnPrivateWorkspaceBecauseItIsUnsaved
{
  _workspace.owner = WorkspaceOwnerName;
  _workspace.isSaved = NO;
  _controller.workspace = _workspace;

  XCTAssertEqual(_controller.menuItems.count, 3, @"Menu should have three options");

  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.buttonTitle contains %@", NSLocalizedString(@"Workspace Rename Button Title", nil)];
  NSArray *filteredArray = [_controller.menuItems filteredArrayUsingPredicate:predicate];

  XCTAssertEqual(filteredArray.count, 0, @"Menu should include not Rename option");
}

- (void)testMenuWithoutRenameOnPublicWorkspaceBecauseItIsUnsaved
{
  _workspace.owner = nil;
  _workspace.isSaved = NO;
  _controller.workspace = _workspace;

  XCTAssertEqual(_controller.menuItems.count, 3, @"Menu should have three options");

  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.buttonTitle contains %@", NSLocalizedString(@"Workspace Rename Button Title", nil)];
  NSArray *filteredArray = [_controller.menuItems filteredArrayUsingPredicate:predicate];

  XCTAssertEqual(filteredArray.count, 0, @"Menu should include not Rename option");
}


- (void)testRPMenuWithUnsavedWorkspace
{
  // Fake we are in RP
  ((MockMZCommunicator *)_controller.workspaceToolbarViewController.appContext.currentCommunicator).isARemoteParticipationConnection = YES;

  _workspace.owner = nil;
  _workspace.isSaved = NO;
  _controller.workspace = _workspace;
  
  XCTAssertEqual(_controller.menuItems.count, 2, @"Menu should have three options");
  
  
  NSPredicate *predicate = nil;
  NSArray *filteredArray = nil;
  // Save
  predicate = [NSPredicate predicateWithFormat:@"SELF.buttonTitle contains %@", NSLocalizedString(@"Workspace Save Button Title", nil)];
  filteredArray = [_controller.menuItems filteredArrayUsingPredicate:predicate];
  XCTAssertEqual(filteredArray.count, 1, @"Menu should include Save option");
  XCTAssertTrue(((ButtonMenuItem *)[filteredArray firstObject]).enabled);

  // Discard
  predicate = [NSPredicate predicateWithFormat:@"SELF.buttonTitle contains %@", NSLocalizedString(@"Workspace Discard Button Title", nil)];
  filteredArray = [_controller.menuItems filteredArrayUsingPredicate:predicate];
  XCTAssertEqual(filteredArray.count, 1, @"Menu should include Discard option");
  XCTAssertFalse(((ButtonMenuItem *)[filteredArray firstObject]).enabled);
}

- (void)testRPMenuWithSavedWorkspace
{
  // Fake we are in RP
  ((MockMZCommunicator *)_controller.workspaceToolbarViewController.appContext.currentCommunicator).isARemoteParticipationConnection = YES;
  
  _workspace.owner = nil;
  _workspace.isSaved = YES;
  _controller.workspace = _workspace;
  
  XCTAssertEqual(_controller.menuItems.count, 2, @"Menu should have two options");
  
  NSPredicate *predicate = nil;
  NSArray *filteredArray = nil;
  // Save
  predicate = [NSPredicate predicateWithFormat:@"SELF.buttonTitle contains %@", NSLocalizedString(@"Workspace Rename Button Title", nil)];
  filteredArray = [_controller.menuItems filteredArrayUsingPredicate:predicate];
  XCTAssertEqual(filteredArray.count, 1, @"Menu should include Save option");
  XCTAssertTrue(((ButtonMenuItem *)[filteredArray firstObject]).enabled);
  
  // Discard
  predicate = [NSPredicate predicateWithFormat:@"SELF.buttonTitle contains %@", NSLocalizedString(@"Workspace Close Button Title", nil)];
  filteredArray = [_controller.menuItems filteredArrayUsingPredicate:predicate];
  XCTAssertEqual(filteredArray.count, 1, @"Menu should include Discard option");
  XCTAssertTrue(((ButtonMenuItem *)[filteredArray firstObject]).enabled);
}

@end
