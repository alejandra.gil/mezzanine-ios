//
//  InfopresenceMenuViewController_Tests.swift
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 29/10/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

import UIKit
import XCTest
@testable import Mezzanine

class InfopresenceMenuViewController_Tests: XCTestCase {
  
  var viewController: InfopresenceMenuViewController!
  
  var infopresenceMenuController: InfopresenceMenuController! {
    return InfopresenceMenuController()
  }

  override func setUp() {
    super.setUp()

    let window = UIApplication.shared.delegate?.window!

    let appModel = MezzanineAppModel()

    let appContext = MezzanineAppContext()
    MezzanineAppContext.setCurrent(appContext)
    appContext.applicationModel = appModel
    appContext.window = window

    appContext.applicationModel.systemModel.myMezzanine.version = "3.0"
    appContext.currentCommunicator = MockMZCommunicator()

    viewController = InfopresenceMenuViewController(systemModel: appContext.applicationModel.systemModel, communicator: appContext.currentCommunicator, menuController: infopresenceMenuController)
    viewController?.systemModel.myMezzanine.name = "mezz-name"

    let navigationController = MezzanineNavigationController.init(rootViewController: viewController)
    window?.rootViewController = navigationController
    appContext.mainNavController = navigationController
    appContext.mainNavController.delegate = appContext

    viewController?.view
  }
  
  
  override func tearDown() {

    super.tearDown()
  }
  
  
  func testOutletsAndSetup()
  {
    viewController?.viewDidLoad()
    XCTAssertNotNil(viewController?.participantsSection)
  }

  func testRemoteParticipationLayout()
  {
    viewController.systemModel = nil
    viewController.communicator = nil
    viewController = nil

    guard let appContext = MezzanineAppContext.current() else { return }
    appContext.currentCommunicator.serverUrl = URL.init(string:"https://joinmz.com/m/12345")
    appContext.currentCommunicator.setValue(true, forKey:"isARemoteParticipationConnection")

    viewController = InfopresenceMenuViewController(systemModel: appContext.applicationModel.systemModel, communicator: appContext.currentCommunicator, menuController: infopresenceMenuController)
    viewController?.systemModel.infopresence.remoteParticipantsEnabled = true
    viewController?.systemModel.myMezzanine.name = "mezz-name"

    let navigationController = MezzanineNavigationController.init(rootViewController: viewController)
    UIApplication.shared.delegate?.window?!.rootViewController = navigationController
    appContext.mainNavController = navigationController
    appContext.mainNavController.delegate = appContext

    viewController?.view
    XCTAssertNotNil(viewController?.participantsSection)
  }
}


