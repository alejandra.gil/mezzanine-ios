//
//  RemoteMezzanineTableViewDataSource_Tests.swift
//  Mezzanine
//
//  Created by Zai Chang on 6/26/15.
//  Copyright (c) 2015 Oblong Industries. All rights reserved.
//

import UIKit
import XCTest
@testable import Mezzanine

class RemoteMezzaninesTableViewDataSource_Tests: XCTestCase {

	var dataSource: RemoteMezzaninesTableViewDataSource?
	var systemModel: MZSystemModel?
	var tableView: MockUITableView?
	
	override func setUp() {
		super.setUp()
		
		systemModel = MZSystemModel()

		tableView = MockUITableView()
		
		let remoteMezzes = NSMutableArray()
		for i in 0...4 {
			let remoteMezz = createRemoteMezz(i)
			remoteMezzes.add(remoteMezz)
		}
		systemModel!.remoteMezzes = remoteMezzes
		
		dataSource = RemoteMezzaninesTableViewDataSource(systemModel: systemModel!, tableView: tableView!)
	}
	
	func createRemoteMezz(_ i: Int) -> MZRemoteMezz {
		let remoteMezz = MZRemoteMezz()
		remoteMezz.uid = String(format: "%d", arguments: [i])
		remoteMezz.name = String(format: "Mezzanine %d", arguments: [i])
		remoteMezz.online = true
		remoteMezz.version = "3.0"
		return remoteMezz
	}

	override func tearDown() {
		// Put teardown code here. This method is called after the invocation of each test method in the class.
		super.tearDown()
	}

	func testDataSource() {
		let numberOfRows = dataSource!.tableView(tableView!, numberOfRowsInSection: 0)
		XCTAssertEqual(numberOfRows, 5, "Number of rows should be 5")
		
		// TODO - Fix testing of UITableViewCell and the loading of it from nib in unit tests
		/*
		let cell = dataSource!.tableView(tableView!, cellForRowAtIndexPath: NSIndexPath(row: 0, section: 0)) as! RemoteMezzanineTableViewCell

		let firstRemoteMezz = systemModel!.remoteMezzes[0] as! MZRemoteMezz
		XCTAssertEqual(cell.remoteMezz, firstRemoteMezz, "remoteMezz in cell should equal model object")
		*/
	}
	
	func testRemoteMezzAdded() {
		let row: Int = 5
		let newMezz = createRemoteMezz(row)
		let indexSet = IndexSet(integer: row)
		systemModel!.insertRemoteMezzes([newMezz], at: indexSet)
		
		let numberOfRows = dataSource!.tableView(tableView!, numberOfRowsInSection: 0)
		XCTAssertEqual(numberOfRows, 6, "Number of rows after insertion should be 6")

		let invocationExists = tableView!.insertRowsAtIndexPathsInvocations.count > 0
		XCTAssertTrue(invocationExists, "Invocations should be greater than zero")

    let invocationDeleteDoNotExist = tableView!.deleteRowsAtIndexPathsInvocations.count == 0
    XCTAssertTrue(invocationDeleteDoNotExist, "Delete invocations should be zero")
		
		if (invocationExists) {
			if let invocation = tableView?.insertRowsAtIndexPathsInvocations[0] {
				XCTAssertEqual((invocation.indexPaths[0] as! IndexPath) as IndexPath, IndexPath(row: row, section: 0), String(format: "insertRowsAtIndexPaths should be invoked with indexSet = %@", indexSet as CVarArg));
			}
		}
	}
	
	func testRemoteMezzRemoved() {
		let indexOfMezzToRemove: UInt = 3
		systemModel!.removeObjectFromRemoteMezzes(at: indexOfMezzToRemove)
		
		let numberOfRows = dataSource!.tableView(tableView!, numberOfRowsInSection: 0)
		XCTAssertEqual(numberOfRows, 4, "Number of rows after deletion should be 4")

    let invocationInsertDoNotExist = tableView!.insertRowsAtIndexPathsInvocations.count == 0
    XCTAssertTrue(invocationInsertDoNotExist, "Delete invocations should be zero")

		let invocationExists = tableView!.deleteRowsAtIndexPathsInvocations.count > 0
		XCTAssertTrue(invocationExists, "Invocations should be greater than zero")

		if (invocationExists) {
			if let invocation = tableView?.deleteRowsAtIndexPathsInvocations[0] {
				let indexPath = IndexPath(row: Int(indexOfMezzToRemove), section: 0)
				XCTAssertTrue(invocation.indexPaths[0] as! IndexPath == indexPath);
			}
		}
	}
	
	func testItemAtIndexPath() {
		let item1 = dataSource!.itemAtIndexPath(IndexPath(row: 0, section: 0))
		XCTAssertEqual(item1.uid, String(format: "%d", arguments: [0]))

		let item3 = dataSource!.itemAtIndexPath(IndexPath(row: 2, section: 0))
		XCTAssertEqual(item3.uid, String(format: "%d", arguments: [2]))
	}
	
	func testSelectedItemsNone() {
		tableView?.selectedPaths = []
		
		let selectedItems = dataSource!.selectedItems()
		XCTAssertEqual(selectedItems.count, 0)
	}
	
	func testSelectedItemsSingle() {
		tableView?.selectedPaths = [IndexPath(row: 3, section: 0)]
		
		let selectedItems = dataSource!.selectedItems()
		XCTAssertEqual(selectedItems.count, 1)
		XCTAssertEqual(selectedItems[0], systemModel!.remoteMezzes[3] as? MZRemoteMezz)
	}
	
	func testSelectedItemsMultiple() {
		tableView?.selectedPaths = [IndexPath(row: 1, section: 0), IndexPath(row: 3, section: 0)]
		
		let selectedItems = dataSource!.selectedItems()
		XCTAssertEqual(selectedItems.count, 2)
		XCTAssertEqual(selectedItems[0], systemModel!.remoteMezzes[1] as? MZRemoteMezz)
		XCTAssertEqual(selectedItems[1], systemModel!.remoteMezzes[3] as? MZRemoteMezz)
	}
	
	func testDealloc() {
		weak var dataSourceWeak: RemoteMezzaninesTableViewDataSource? = dataSource!

		dataSource = nil
		tableView?.dataSource = nil
		
		XCTAssertNil(dataSourceWeak, "Weak reference should be deallocated")

		// Insert object to test stopping of system model observation
		let remoteMezzes = systemModel!.remoteMezzes
		systemModel?.insert(MZRemoteMezz(), inRemoteMezzesAt: UInt((remoteMezzes?.count)!))
		XCTAssertEqual(tableView!.insertRowsAtIndexPathsInvocations.count, 0)
		systemModel?.removeObjectFromRemoteMezzes(at: 0)
		XCTAssertEqual(tableView!.deleteRowsAtIndexPathsInvocations.count, 0)
	}

  func testSortingAfterInsertion() {

    let remoteMezz10 = MZRemoteMezz()
    remoteMezz10.uid = "10"
    remoteMezz10.name = "Mezzanine 10"
    remoteMezz10.online = true
    remoteMezz10.version = "3.0"
    systemModel!.insert(remoteMezz10, inRemoteMezzesAt: 5)

    XCTAssertEqual(remoteMezz10, dataSource!.itemAtIndexPath(IndexPath(row: 5, section: 0)))

    let remoteMezz8 = MZRemoteMezz()
    remoteMezz8.uid = "8"
    remoteMezz8.name = "Mezzanine 8"
    remoteMezz8.online = true
    remoteMezz8.version = "3.0"
    systemModel!.insert(remoteMezz8, inRemoteMezzesAt: 6)

    XCTAssertEqual(remoteMezz8, dataSource!.itemAtIndexPath(IndexPath(row: 5, section: 0)))
    XCTAssertEqual(remoteMezz10, dataSource!.itemAtIndexPath(IndexPath(row: 6, section: 0)))
  }

  func testSortingWithCaseInsensitiveAndNaturalOrder() {
    let remoteMezz0 = dataSource!.itemAtIndexPath(IndexPath(row: 0, section: 0))
    let remoteMezz1 = dataSource!.itemAtIndexPath(IndexPath(row: 1, section: 0))
    let remoteMezz2 = dataSource!.itemAtIndexPath(IndexPath(row: 2, section: 0))
    let remoteMezz3 = dataSource!.itemAtIndexPath(IndexPath(row: 3, section: 0))
    let remoteMezz4 = dataSource!.itemAtIndexPath(IndexPath(row: 4, section: 0))

    XCTAssertEqual([remoteMezz0, remoteMezz1, remoteMezz2, remoteMezz3, remoteMezz4], (dataSource?.sortedMezzanines)!)

    // Test by name ordering
    remoteMezz0.name = "b"
    remoteMezz1.name = "A"
    remoteMezz2.name = "c"
    remoteMezz3.name = "B"
    remoteMezz4.name = "a"

    // Case insensitive forces us to consider all combinations of upper and lower same character.
    XCTAssertTrue(([remoteMezz1, remoteMezz4, remoteMezz0, remoteMezz3, remoteMezz2] == (dataSource?.sortedMezzanines)!) ||
                  ([remoteMezz4, remoteMezz1, remoteMezz3, remoteMezz0, remoteMezz2] == (dataSource?.sortedMezzanines)!) ||
                  ([remoteMezz1, remoteMezz4, remoteMezz3, remoteMezz0, remoteMezz2] == (dataSource?.sortedMezzanines)!) ||
                  ([remoteMezz4, remoteMezz1, remoteMezz0, remoteMezz3, remoteMezz2] == (dataSource?.sortedMezzanines)!), "Sorted Mezzanines are "+(dataSource?.sortedMezzanines)!.description)

    // Test by name ordering with uppper and lower cases in different order
    remoteMezz0.name = "E"
    remoteMezz1.name = "d"
    remoteMezz2.name = "e"
    remoteMezz3.name = "f"
    remoteMezz4.name = "D"

    // Case insensitive forces us to consider all combinations of upper and lower same character.
    XCTAssertTrue(([remoteMezz1, remoteMezz4, remoteMezz0, remoteMezz2, remoteMezz3] == (dataSource?.sortedMezzanines)!) ||
                  ([remoteMezz4, remoteMezz1, remoteMezz0, remoteMezz2, remoteMezz3] == (dataSource?.sortedMezzanines)!) ||
                  ([remoteMezz1, remoteMezz4, remoteMezz2, remoteMezz0, remoteMezz3] == (dataSource?.sortedMezzanines)!) ||
                  ([remoteMezz4, remoteMezz1, remoteMezz2, remoteMezz0, remoteMezz3] == (dataSource?.sortedMezzanines)!), "Sorted Mezzanines are "+(dataSource?.sortedMezzanines)!.description)

    // Test by name with natural order
    remoteMezz0.name = "A5"
    remoteMezz1.name = "A4"
    remoteMezz2.name = "a10"
    remoteMezz3.name = "A3"
    remoteMezz4.name = "a33"

    XCTAssertEqual([remoteMezz3, remoteMezz1, remoteMezz0, remoteMezz2, remoteMezz4], (dataSource?.sortedMezzanines)!, "Sorted Mezzanines are "+(dataSource?.sortedMezzanines)!.description)
  }

  func testSortingAfterUpdatingRemoteMezz() {

    let remoteMezz0 = dataSource!.itemAtIndexPath(IndexPath(row: 0, section: 0))
    let remoteMezz1 = dataSource!.itemAtIndexPath(IndexPath(row: 1, section: 0))
    let remoteMezz2 = dataSource!.itemAtIndexPath(IndexPath(row: 2, section: 0))
    let remoteMezz3 = dataSource!.itemAtIndexPath(IndexPath(row: 3, section: 0))
    let remoteMezz4 = dataSource!.itemAtIndexPath(IndexPath(row: 4, section: 0))
    let memoryAddresses = [Unmanaged.passUnretained(remoteMezz0).toOpaque(), Unmanaged.passUnretained(remoteMezz1).toOpaque(), Unmanaged.passUnretained(remoteMezz2).toOpaque(), Unmanaged.passUnretained(remoteMezz3).toOpaque(), Unmanaged.passUnretained(remoteMezz4).toOpaque()].sorted(by: <)

    // Test by name ordering
    remoteMezz0.name = "5"
    remoteMezz1.name = "4"
    remoteMezz2.name = "3"
    remoteMezz3.name = "2"
    remoteMezz4.name = "1"

    XCTAssertEqual(remoteMezz0, dataSource!.itemAtIndexPath(IndexPath(row: 4, section: 0)))
    XCTAssertEqual(remoteMezz1, dataSource!.itemAtIndexPath(IndexPath(row: 3, section: 0)))
    XCTAssertEqual(remoteMezz2, dataSource!.itemAtIndexPath(IndexPath(row: 2, section: 0)))
    XCTAssertEqual(remoteMezz3, dataSource!.itemAtIndexPath(IndexPath(row: 1, section: 0)))
    XCTAssertEqual(remoteMezz4, dataSource!.itemAtIndexPath(IndexPath(row: 0, section: 0)))

    remoteMezz0.name = "0"
    remoteMezz1.name = "0"
    remoteMezz2.name = "0"
    remoteMezz3.name = "0"
    remoteMezz4.name = "0"


    // Test by memory address when some properties are equal and others are nil
    var memoryAddressesSorted = [Unmanaged.passUnretained(dataSource!.itemAtIndexPath(IndexPath(row: 0, section: 0))).toOpaque(),
                                 Unmanaged.passUnretained(dataSource!.itemAtIndexPath(IndexPath(row: 1, section: 0))).toOpaque(),
                                 Unmanaged.passUnretained(dataSource!.itemAtIndexPath(IndexPath(row: 2, section: 0))).toOpaque(),
                                 Unmanaged.passUnretained(dataSource!.itemAtIndexPath(IndexPath(row: 3, section: 0))).toOpaque(),
                                 Unmanaged.passUnretained(dataSource!.itemAtIndexPath(IndexPath(row: 4, section: 0))).toOpaque()]

    XCTAssertEqual(memoryAddresses[0], memoryAddressesSorted[0])
    XCTAssertEqual(memoryAddresses[1], memoryAddressesSorted[1])
    XCTAssertEqual(memoryAddresses[2], memoryAddressesSorted[2])
    XCTAssertEqual(memoryAddresses[3], memoryAddressesSorted[3])
    XCTAssertEqual(memoryAddresses[4], memoryAddressesSorted[4])


    // Test by company ordering
    remoteMezz0.company = "b10"
    remoteMezz1.company = "b01"
    remoteMezz2.company = "b00"
    remoteMezz3.company = "b02"
    remoteMezz4.company = "b011"

    XCTAssertEqual(remoteMezz0, dataSource!.itemAtIndexPath(IndexPath(row: 3, section: 0)))
    XCTAssertEqual(remoteMezz1, dataSource!.itemAtIndexPath(IndexPath(row: 1, section: 0)))
    XCTAssertEqual(remoteMezz2, dataSource!.itemAtIndexPath(IndexPath(row: 0, section: 0)))
    XCTAssertEqual(remoteMezz3, dataSource!.itemAtIndexPath(IndexPath(row: 2, section: 0)))
    XCTAssertEqual(remoteMezz4, dataSource!.itemAtIndexPath(IndexPath(row: 4, section: 0)))


    // Test by location ordering
    remoteMezz0.company = "b10"
    remoteMezz1.company = "b10"
    remoteMezz2.company = "b10"
    remoteMezz3.company = "b10"
    remoteMezz4.company = "b10"

    remoteMezz0.location = "l-9"
    remoteMezz1.location = "l-7"
    remoteMezz2.location = "l-14"
    remoteMezz3.location = "l-0104"
    remoteMezz4.location = "l-5"

    XCTAssertEqual(remoteMezz0, dataSource!.itemAtIndexPath(IndexPath(row: 2, section: 0)))
    XCTAssertEqual(remoteMezz1, dataSource!.itemAtIndexPath(IndexPath(row: 1, section: 0)))
    XCTAssertEqual(remoteMezz2, dataSource!.itemAtIndexPath(IndexPath(row: 3, section: 0)))
    XCTAssertEqual(remoteMezz3, dataSource!.itemAtIndexPath(IndexPath(row: 4, section: 0)))
    XCTAssertEqual(remoteMezz4, dataSource!.itemAtIndexPath(IndexPath(row: 0, section: 0)))


    // Test by memory ordering again when everything else is equal
    remoteMezz0.location = "l-0104"
    remoteMezz1.location = "l-0104"
    remoteMezz2.location = "l-0104"
    remoteMezz3.location = "l-0104"
    remoteMezz4.location = "l-0104"

    memoryAddressesSorted = [Unmanaged.passUnretained(dataSource!.itemAtIndexPath(IndexPath(row: 0, section: 0))).toOpaque(),
                             Unmanaged.passUnretained(dataSource!.itemAtIndexPath(IndexPath(row: 1, section: 0))).toOpaque(),
                             Unmanaged.passUnretained(dataSource!.itemAtIndexPath(IndexPath(row: 2, section: 0))).toOpaque(),
                             Unmanaged.passUnretained(dataSource!.itemAtIndexPath(IndexPath(row: 3, section: 0))).toOpaque(),
                             Unmanaged.passUnretained(dataSource!.itemAtIndexPath(IndexPath(row: 4, section: 0))).toOpaque()]

    XCTAssertEqual(memoryAddresses[0], memoryAddressesSorted[0])
    XCTAssertEqual(memoryAddresses[1], memoryAddressesSorted[1])
    XCTAssertEqual(memoryAddresses[2], memoryAddressesSorted[2])
    XCTAssertEqual(memoryAddresses[3], memoryAddressesSorted[3])
    XCTAssertEqual(memoryAddresses[4], memoryAddressesSorted[4])
  }
}
