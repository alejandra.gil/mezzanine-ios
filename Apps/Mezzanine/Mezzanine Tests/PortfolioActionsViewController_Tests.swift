//
//  PortfolioActionsViewController_Tests.swift
//  Mezzanine
//
//  Created by miguel on 14/2/17.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import XCTest

@testable import Mezzanine

class PortfolioActionsViewController_Tests: XCTestCase {

  var interactor: PortfolioActionsInteractor?
  var controller = PortfolioActionsViewController()
  var systemModel = MZSystemModel()
  var communicator = MZCommunicator()
  var systemModelFactory = MockSystemModel()

  override func setUp() {
    super.setUp()

    let systemModelFactory = MockSystemModel()

    systemModel = systemModelFactory.systemModel
    communicator = MZCommunicator()
    communicator.systemModel = systemModel

    interactor = PortfolioActionsInteractor(systemModel: systemModel, communicator: communicator)
    controller = PortfolioActionsViewController()
    interactor!.presenter = controller
    controller.interactor = interactor!

    _ = controller.view
  }

  override func tearDown() {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    super.tearDown()
  }


  // MARK: Add Menu

  func testPresentAddMenu() {

    controller.add(UIButton())

    XCTAssertNotNil(controller.currentPopover)

    let contentViewController = controller.currentPopover!.viewController as! ButtonMenuViewController
    XCTAssertNotNil(contentViewController)
    XCTAssertGreaterThanOrEqual(contentViewController.menuItems.count, 0)

    for item in contentViewController.menuItems {
      let button = item as! ButtonMenuItem
      XCTAssertTrue(button.enabled)
      XCTAssertNotNil(button.action)
    }
  }

  func testPresentAddMenuNoWhiteboards() {
    let numberOfWhiteboards = 0

    interactor!.systemModel = systemModelFactory.createTest(withWhiteboards: numberOfWhiteboards)
    controller.add(UIButton())

    let contentViewController = controller.currentPopover!.viewController as! ButtonMenuViewController
    XCTAssertNotNil(contentViewController)

    let filteredItems = contentViewController.menuItems.filter(){ ($0 as AnyObject).buttonTitle!.range(of:"Whiteboard") != nil }
    XCTAssertEqual(0, filteredItems.count)
  }

  func testPresentAddMenuSingleWhiteboard() {
    let numberOfWhiteboards = 1

    interactor!.systemModel = systemModelFactory.createTest(withWhiteboards: numberOfWhiteboards)
    controller.add(UIButton())

    let contentViewController = controller.currentPopover!.viewController as! ButtonMenuViewController
    XCTAssertNotNil(contentViewController)

    let filteredItems = contentViewController.menuItems.filter(){ ($0 as AnyObject).buttonTitle!.range(of:"Whiteboard") != nil }
    XCTAssertEqual(1, filteredItems.count)

    for item in contentViewController.menuItems {
      let button = item as! ButtonMenuItem
      XCTAssertTrue(button.enabled)
      XCTAssertNotNil(button.action)
    }
  }

  func testPresentAddMenuMultipleWhiteboard() {
    let numberOfWhiteboards = 4

    interactor!.systemModel = systemModelFactory.createTest(withWhiteboards: numberOfWhiteboards)
    controller.add(UIButton())

    let contentViewController = controller.currentPopover!.viewController as! ButtonMenuViewController
    XCTAssertNotNil(contentViewController)

    let filteredItems = contentViewController.menuItems.filter(){ ($0 as AnyObject).buttonTitle!.range(of:"Whiteboard") != nil }
    XCTAssertEqual(1, filteredItems.count)

    for item in contentViewController.menuItems {
      let button = item as! ButtonMenuItem
      XCTAssertTrue(button.enabled)
      XCTAssertNotNil(button.action)
    }
  }


  // MARK: More Menu

  func testMoreMenuEnabled() {
    let numberOfSlides = 4
    setUpPortfolioWithNumberOfItems(numberOfSlides)
    controller.more(UIButton())

    // Conditions
    let contentViewController = controller.currentPopover!.viewController as! PortfolioMoreMenuViewController
    XCTAssertNotNil(contentViewController)
    XCTAssertEqual(numberOfSlides, interactor!.systemModel.currentWorkspace.presentation.slides.count)

    let filteredItems = contentViewController.menuItems.filter(){ ($0 as AnyObject).buttonTitle!.range(of:"Portfolio Export Button Sign In Title".localized) != nil }
    XCTAssertEqual(filteredItems.count, 0, "Menu should not request to be signed in")

    for item in contentViewController.menuItems {
      let button = item as! ButtonMenuItem
      XCTAssertTrue(button.enabled)
      XCTAssertNotNil(button.action)
    }
  }

  func testMoreMenuDisabled() {
    let numberOfSlides = 0
    setUpPortfolioWithNumberOfItems(numberOfSlides)
    controller.more(UIButton())

    // Conditions
    let contentViewController = controller.currentPopover!.viewController as! PortfolioMoreMenuViewController
    XCTAssertNotNil(contentViewController)
    XCTAssertEqual(numberOfSlides, interactor!.systemModel.currentWorkspace.presentation.slides.count)

    let filteredItems = contentViewController.menuItems.filter(){ ($0 as AnyObject).buttonTitle!.range(of:"Portfolio Export Button Sign In Title".localized) != nil }
    XCTAssertEqual(filteredItems.count, 0, "Menu should not request to be signed in")

    for item in contentViewController.menuItems {
      let button = item as! ButtonMenuItem
      XCTAssertFalse(button.enabled)
    }
  }

  func testMoreMenuWhenDisableDownloadsFlagIsActiveAndUserIsNotSignedIn() {

    let numberOfSlides = 4
    setUpPortfolioWithNumberOfItems(numberOfSlides)

    interactor!.systemModel.featureToggles.disableDownloadsEnabled = true
    interactor!.systemModel.currentUsername = nil

    controller.more(UIButton())

    // Conditions
    let contentViewController = controller.currentPopover!.viewController as! PortfolioMoreMenuViewController
    XCTAssertNotNil(contentViewController)
    XCTAssertEqual(numberOfSlides, interactor!.systemModel.currentWorkspace.presentation.slides.count)

    let filteredItems = contentViewController.menuItems.filter(){ ($0 as AnyObject).buttonTitle!.range(of:"Portfolio Export Button Sign In Title".localized) != nil } as! Array<ButtonMenuItem>
    XCTAssertEqual(filteredItems.count, 1, "Menu should request to be signed in to download")

    for item in contentViewController.menuItems {
      let button = item as! ButtonMenuItem
      if filteredItems.contains(button) {
        XCTAssertFalse(button.enabled)
        XCTAssertNil(button.action)
      }
      else {
        XCTAssertTrue(button.enabled)
        XCTAssertNotNil(button.action)
      }
    }
  }

  func testMoreMenuWhenDisableDownloadsFlagIsActiveAndUserIsSignedIn() {

    let numberOfSlides = 4
    setUpPortfolioWithNumberOfItems(numberOfSlides)

    interactor!.systemModel.featureToggles.disableDownloadsEnabled = true
    interactor!.systemModel.currentUsername = "test-user"

    controller.more(UIButton())

    // Conditions
    let contentViewController = controller.currentPopover!.viewController as! PortfolioMoreMenuViewController
    XCTAssertNotNil(contentViewController)
    XCTAssertEqual(numberOfSlides, interactor!.systemModel.currentWorkspace.presentation.slides.count)

    let filteredItems = contentViewController.menuItems.filter(){ ($0 as AnyObject).buttonTitle!.range(of:"Portfolio Export Button Sign In Title".localized) != nil }
    XCTAssertEqual(filteredItems.count, 0, "Menu should not request to be signed in")

    for item in contentViewController.menuItems {
      let button = item as! ButtonMenuItem
      XCTAssertTrue(button.enabled)
      XCTAssertNotNil(button.action)
    }
  }


  // MARK: Present Action

  func testUpdatePresentationModeStateInactive() {
    let numberOfSlides = 1
    setUpPortfolioWithNumberOfItems(numberOfSlides)
    interactor!.systemModel.currentWorkspace.presentation.active = false
    XCTAssertNotNil(controller.presentButton.titleLabel!.text!.range(of: "Portfolio Present Action Button".localized))
    XCTAssertTrue(controller.view.mask!.frame.intersects(controller.presentButton.frame))
  }

  func testUpdatePresentationModeStateActive() {
    let numberOfSlides = 1
    setUpPortfolioWithNumberOfItems(numberOfSlides)
    interactor!.systemModel.currentWorkspace.presentation.active = true
    XCTAssertNotNil(controller.presentButton.titleLabel!.text!.range(of: "Presentation Stop Button Title".localized))
    XCTAssertTrue(controller.view.mask!.frame.intersects(controller.presentButton.frame))
  }

  func testUpdatePresentationModeStateEmptyPortfolio() {
    let numberOfSlides = 0
    setUpPortfolioWithNumberOfItems(numberOfSlides)
    XCTAssertNotNil(controller.presentButton.titleLabel!.text!.range(of: "Portfolio Present Action Button".localized))
    XCTAssertTrue(controller.view.mask!.frame.intersects(controller.presentButton.frame))
  }


  // MARK: Helpers

  func setUpPortfolioWithNumberOfItems(_ numberOfItems: Int) {

    interactor!.systemModel = systemModelFactory.createTest(withSlides: numberOfItems)
    interactor!.presentation = interactor!.systemModel.currentWorkspace.presentation
  }
}
