//
//  InfopresenceInviteParticipantsViewController_Tests.swift
//  Mezzanine
//
//  Created by miguel on 3/7/15.
//  Copyright (c) 2015 Oblong Industries. All rights reserved.
//

import UIKit
import XCTest
@testable import Mezzanine

class InfopresenceInviteParticipantsViewController_Tests: XCTestCase {

  var viewController: InfopresenceInviteParticipantsViewController?
  var systemModel: MZSystemModel?
  var mailComposer = MezzanineMailComposerController()
  var infopresenceMenuController: InfopresenceMenuController?

  override func setUp() {
    super.setUp()

    systemModel = MZSystemModel()
    infopresenceMenuController = InfopresenceMenuController(containerViewController: UIViewController())
    viewController = InfopresenceInviteParticipantsViewController(systemModel: systemModel!, mailComposer: mailComposer, infopresenceMenuController: infopresenceMenuController!)
    viewController?.view
  }

  override func tearDown() {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    super.tearDown()
  }

  func createInfopresenceWithRemoteParticipationActive() -> MZInfopresence {
    let infopresence = MZInfopresence()
    infopresence.remoteAccessUrl = "http://www.mezz-in.com"
    infopresence.remoteAccessStatus = .active
    return infopresence
  }

  func createInfopresenceWithRemoteParticipationInactive() -> MZInfopresence {
    let infopresence = MZInfopresence()
    infopresence.remoteAccessUrl = nil
    infopresence.remoteAccessStatus = .inactive
    return infopresence
  }

  func createInfopresenceWithRemoteParticipationStarting() -> MZInfopresence {
    let infopresence = MZInfopresence()
    infopresence.remoteAccessUrl = nil
    infopresence.remoteAccessStatus = .starting
    return infopresence
  }

  // TODO Not Working [this class is not key value coding-compliant for the key... all outlets]

//  func testActiveRemoteParticipation() {
//    let infopresence = createInfopresenceWithRemoteParticipationActive()
//    viewController?.infopresenceModel = infopresence
//
//    XCTAssertTrue(viewController!.participantsCanJoinSwitch.on, "When Active, switch should be on")
//  }

  // MARK: Mail Composer tests

  func testCompanyLocationString() {
    XCTAssertTrue(viewController?.companyLocationString() == "", "Company Location string should be an empty string")

    systemModel?.myMezzanine.company = "Test Company"
    XCTAssertTrue(viewController?.companyLocationString() == "Test Company", "Company Location string should be 'Test Company'")

    systemModel?.myMezzanine.location = "Test Location"
    XCTAssertTrue(viewController?.companyLocationString() == "Test Company, Test Location", "Company Location string should be 'Test Company, Test Location'")

    systemModel?.myMezzanine.company = nil
    XCTAssertTrue(viewController?.companyLocationString() == "Test Location", "Company Location string should be 'Test Location'")

    systemModel?.myMezzanine.location = nil
    XCTAssertTrue(viewController?.companyLocationString() == "", "Company Location string should be an empty string")
  }


  func testBuildMailSubject() {
    systemModel?.myMezzanine.name = "Test Mezz"

    XCTAssertTrue(viewController?.buildMailSubject() == String(format: "Infopresence Invite Email Subject Text".localized, "Test Mezz"), "Mail subject should contain the Mezzanine Name")

    systemModel?.myMezzanine.company = "Test Company"
    XCTAssertTrue(viewController?.buildMailSubject() == String(format: "Infopresence Invite Email Subject Text".localized, "Test Company"), "Mail subject should contain  'Test Company'")

    systemModel?.myMezzanine.location = "Test Location"
    XCTAssertTrue(viewController?.buildMailSubject() == String(format: "Infopresence Invite Email Subject Text".localized, "Test Company, Test Location"), "Mail subject should contain 'Test Company, Test Location'")

    systemModel?.myMezzanine.company = nil
    XCTAssertTrue(viewController?.buildMailSubject() == String(format: "Infopresence Invite Email Subject Text".localized, "Test Location"), "Mail subject should contain 'Test Location'")

    systemModel?.myMezzanine.location = nil
    XCTAssertTrue(viewController?.buildMailSubject() == String(format: "Infopresence Invite Email Subject Text".localized, "Test Mezz"), "Mail subject should contain the Mezzanine Name")
  }

  func testBuildHtmlInviteWithTemplate() {
    systemModel?.myMezzanine.name = "Test Mezz"
    let infopresence = createInfopresenceWithRemoteParticipationActive()
    viewController?.infopresenceModel = infopresence
    let htmlTemplate = loadHTMLTemplate()

    XCTAssertTrue(viewController?.buildHtmlInviteWithTemplate(htmlTemplate) == inviteHTMLOnlyWithName, "Mail subject should contain the Mezzanine Name and the URL")

    systemModel?.myMezzanine.company = "Test Company"
    XCTAssertTrue(viewController?.buildHtmlInviteWithTemplate(htmlTemplate) == inviteHTMLOnlyWithCompany, "Mail subject should contain the Mezzanine Name and the URL")

    systemModel?.myMezzanine.location = "Test Location"
    XCTAssertTrue(viewController?.buildHtmlInviteWithTemplate(htmlTemplate) == inviteHTMLWithCompanyAndLocation, "Mail subject should contain the Mezzanine Name and the URL")

    systemModel?.myMezzanine.company = nil
    XCTAssertTrue(viewController?.buildHtmlInviteWithTemplate(htmlTemplate) == inviteHTMLOnlyWithLocation, "Mail subject should contain the Mezzanine Name and the URL")

    systemModel?.myMezzanine.location = nil
    XCTAssertTrue(viewController?.buildHtmlInviteWithTemplate(htmlTemplate) == inviteHTMLOnlyWithName, "Mail subject should contain the Mezzanine Name and the URL")
  }

  func loadHTMLTemplate() -> String {
    let path = Bundle.main.path(forResource: "RPInvitationTemplate", ofType: nil)
    XCTAssertNotNil(path, "RPInvitationTemplate should be available")

    var htmlTemplate = String()
    do {
      htmlTemplate = try NSString(contentsOfFile: path!, encoding: String.Encoding.utf8.rawValue) as String
    }
    catch _ {
      XCTFail("RPInvitationTemplate should be loaded into a string")
    }

    return htmlTemplate
  }

  let inviteHTMLOnlyWithName = "<!DOCTYPE html>\n<html style=\"margin:0; padding:0;\">\n<head>\n\t<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n\t<style type=\"text/css\">\n\t@media(min-width:480px){p{font-size: 28px}}\n\t@media(min-width:800px){p{font-size: 36px}}\n\t@media(max-width:460px){p{font-size: 16px}}\n\t@media(max-width:320px){p{font-size: 12px}}\n\t@media(max-width:480px){img{width: 150px; height: 30px}}\n\t@media(max-width:320px){img{width: 150px; height: 30px}}\n\t</style>\n\t<title>You are invited to a Mezzanine meeting with Test Mezz</title>\n</head>\n<body style=\"margin:0; padding:0;\">\n\t<table bgcolor=\"#3F4770\" border=\"0\" style=\"width: 100%; table-layout: fixed; font-size: 24px; border-collapse:collapse\">\n\t\t<colgroup>\n\t\t<col span=\"1\" style=\"width: 20px;\">\n\t\t<col span=\"1\" style=\"width: auto;\">\n\t\t<col span=\"1\" style=\"width: 20px;\">\n\t</colgroup>\n\t<tbody>\n\t\t<tr>\n\t\t\t<td colspan=\"3\" style=\"margin:0; padding:0; background-color: #FFF; height: 30px\"></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style=\"background-color: #FFF\"></td>\n\t\t\t<td>\n\t\t\t\t<img src=\"https://www.mezz-in.com/images/invitation-header-800.png\" width=\"200\" height=\"40\" alt=\"Oblong Industries logo\" align=\"right\" style=\"margin:20px 20px 20px 0px\"/>\n\t\t\t</td>\n\t\t\t<td style=\"background-color: #FFF\"></td>\n\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style=\"background-color: #FFF\"></td>\n\t\t\t<td style=\"margin:0;\">\n\t\t\t\t<p style=\"font-family: HelveticaNeue, Verdana, sans-serif; color: #FEFEFE; font-weight: bold; padding-left: 40px; padding-right: 20px; margin: 0;\">You\'re invited to<br>join a Mezzanine meeting.</p>\n\t\t\t</td>\n\t\t\t<td style=\"background-color: #FFF\"></td>\n\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style=\"background-color: #FFF\"></td>\n\t\t\t<td style=\"height: 20px\"></td>\n\t\t\t<td style=\"background-color: #FFF\"></td>\n\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style=\"background-color: #FFF\"></td>\n\n\t\t\t<td style=\"width: 30%\">\n\t\t\t\t<div style=\"\n\t\t\t\tpadding-left: 40px;\n\t\t\t\tpadding-right: 20px;\n\t\t\t\tdisplay:inline-block;\n\t\t\t\tbackground-color:#3F4770;\n\t\t\t\twidth: 110px;\n\t\t\t\tmargin: 0;\">\n\t\t\t\t<a href=\"http://www.mezz-in.com\" style=\"\n\t\t\t\tborder-color:#FF7754;\n\t\t\t\tbackground-color: #FF7754;\n\t\t\t\tborder-radius:3px 3px 3px 3px;\n\t\t\t\tborder-width:5px 18px;\n\t\t\t\tborder-style:solid;\n\t\t\t\tfont-family: HelveticaNeue, Verdana, sans-serif;\n\t\t\t\tfont-weight:bold;\n\t\t\t\tfont-size: 14px;\n\t\t\t\twhite-space:nowrap;\n\t\t\t\tcolor:#fff!important;\n\t\t\t\tdisplay:inline-block;\n\t\t\t\ttext-decoration:none;\n\t\t\t\ttext-align:center;\n\t\t\t\twidth: auto;\n\t\t\t\theight: auto;\n\t\t\t\tmargin: 0;\n\t\t\t\tpadding: 0;\n\t\t\t\tvertical-align: middle;\n\t\t\t\tline-height: 26px;\">JOIN MEETING</a>\n\t\t\t</div>\n\t\t</td>\n\t\t<td style=\"background-color: #FFF\"></td>\n\n\t</tr>\n\t<tr>\n\t\t<td style=\"background-color: #FFF\"></td>\n\t\t<td style=\"height: 20px\"></td>\n\t\t<td style=\"background-color: #FFF\"></td>\n\n\t</tr>\n\t<tr>\n\t\t<td style=\"background-color: #FFF\"></td>\n\t\t<td>\n\t\t\t<p style=\"-webkit-text-size-adjust: 100%; font-family: HelveticaNeue, Verdana, sans-serif; font-size: 10px; color: #FEFEFE; padding-left: 40px; padding-right: 20px; margin: 0\"><b></b> | Test Mezz</p>\n\t\t</td>\n\t\t<td style=\"background-color: #FFF\"></td>\n\n\t</tr>\n\t<tr>\n\t\t<td style=\"background-color: #FFF\"></td>\n\t\t<td style=\"height: 20px\"></td>\n\t\t<td style=\"background-color: #FFF\"></td>\n\n\t</tr>\n\t<tr>\n\t\t<td colspan=\"3\" style=\"margin:0; padding:0; background-color: #FFF; height: 30px\"></td>\n\t</tr>\n</tbody>\n</table>\n</body>\n</html>\n"

  let inviteHTMLOnlyWithCompany = "<!DOCTYPE html>\n<html style=\"margin:0; padding:0;\">\n<head>\n\t<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n\t<style type=\"text/css\">\n\t@media(min-width:480px){p{font-size: 28px}}\n\t@media(min-width:800px){p{font-size: 36px}}\n\t@media(max-width:460px){p{font-size: 16px}}\n\t@media(max-width:320px){p{font-size: 12px}}\n\t@media(max-width:480px){img{width: 150px; height: 30px}}\n\t@media(max-width:320px){img{width: 150px; height: 30px}}\n\t</style>\n\t<title>You are invited to a Mezzanine meeting with Test Company</title>\n</head>\n<body style=\"margin:0; padding:0;\">\n\t<table bgcolor=\"#3F4770\" border=\"0\" style=\"width: 100%; table-layout: fixed; font-size: 24px; border-collapse:collapse\">\n\t\t<colgroup>\n\t\t<col span=\"1\" style=\"width: 20px;\">\n\t\t<col span=\"1\" style=\"width: auto;\">\n\t\t<col span=\"1\" style=\"width: 20px;\">\n\t</colgroup>\n\t<tbody>\n\t\t<tr>\n\t\t\t<td colspan=\"3\" style=\"margin:0; padding:0; background-color: #FFF; height: 30px\"></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style=\"background-color: #FFF\"></td>\n\t\t\t<td>\n\t\t\t\t<img src=\"https://www.mezz-in.com/images/invitation-header-800.png\" width=\"200\" height=\"40\" alt=\"Oblong Industries logo\" align=\"right\" style=\"margin:20px 20px 20px 0px\"/>\n\t\t\t</td>\n\t\t\t<td style=\"background-color: #FFF\"></td>\n\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style=\"background-color: #FFF\"></td>\n\t\t\t<td style=\"margin:0;\">\n\t\t\t\t<p style=\"font-family: HelveticaNeue, Verdana, sans-serif; color: #FEFEFE; font-weight: bold; padding-left: 40px; padding-right: 20px; margin: 0;\">You\'re invited to<br>join a Mezzanine meeting.</p>\n\t\t\t</td>\n\t\t\t<td style=\"background-color: #FFF\"></td>\n\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style=\"background-color: #FFF\"></td>\n\t\t\t<td style=\"height: 20px\"></td>\n\t\t\t<td style=\"background-color: #FFF\"></td>\n\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style=\"background-color: #FFF\"></td>\n\n\t\t\t<td style=\"width: 30%\">\n\t\t\t\t<div style=\"\n\t\t\t\tpadding-left: 40px;\n\t\t\t\tpadding-right: 20px;\n\t\t\t\tdisplay:inline-block;\n\t\t\t\tbackground-color:#3F4770;\n\t\t\t\twidth: 110px;\n\t\t\t\tmargin: 0;\">\n\t\t\t\t<a href=\"http://www.mezz-in.com\" style=\"\n\t\t\t\tborder-color:#FF7754;\n\t\t\t\tbackground-color: #FF7754;\n\t\t\t\tborder-radius:3px 3px 3px 3px;\n\t\t\t\tborder-width:5px 18px;\n\t\t\t\tborder-style:solid;\n\t\t\t\tfont-family: HelveticaNeue, Verdana, sans-serif;\n\t\t\t\tfont-weight:bold;\n\t\t\t\tfont-size: 14px;\n\t\t\t\twhite-space:nowrap;\n\t\t\t\tcolor:#fff!important;\n\t\t\t\tdisplay:inline-block;\n\t\t\t\ttext-decoration:none;\n\t\t\t\ttext-align:center;\n\t\t\t\twidth: auto;\n\t\t\t\theight: auto;\n\t\t\t\tmargin: 0;\n\t\t\t\tpadding: 0;\n\t\t\t\tvertical-align: middle;\n\t\t\t\tline-height: 26px;\">JOIN MEETING</a>\n\t\t\t</div>\n\t\t</td>\n\t\t<td style=\"background-color: #FFF\"></td>\n\n\t</tr>\n\t<tr>\n\t\t<td style=\"background-color: #FFF\"></td>\n\t\t<td style=\"height: 20px\"></td>\n\t\t<td style=\"background-color: #FFF\"></td>\n\n\t</tr>\n\t<tr>\n\t\t<td style=\"background-color: #FFF\"></td>\n\t\t<td>\n\t\t\t<p style=\"-webkit-text-size-adjust: 100%; font-family: HelveticaNeue, Verdana, sans-serif; font-size: 10px; color: #FEFEFE; padding-left: 40px; padding-right: 20px; margin: 0\"><b>Test Company</b> | Test Mezz</p>\n\t\t</td>\n\t\t<td style=\"background-color: #FFF\"></td>\n\n\t</tr>\n\t<tr>\n\t\t<td style=\"background-color: #FFF\"></td>\n\t\t<td style=\"height: 20px\"></td>\n\t\t<td style=\"background-color: #FFF\"></td>\n\n\t</tr>\n\t<tr>\n\t\t<td colspan=\"3\" style=\"margin:0; padding:0; background-color: #FFF; height: 30px\"></td>\n\t</tr>\n</tbody>\n</table>\n</body>\n</html>\n"

  let inviteHTMLOnlyWithLocation = "<!DOCTYPE html>\n<html style=\"margin:0; padding:0;\">\n<head>\n\t<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n\t<style type=\"text/css\">\n\t@media(min-width:480px){p{font-size: 28px}}\n\t@media(min-width:800px){p{font-size: 36px}}\n\t@media(max-width:460px){p{font-size: 16px}}\n\t@media(max-width:320px){p{font-size: 12px}}\n\t@media(max-width:480px){img{width: 150px; height: 30px}}\n\t@media(max-width:320px){img{width: 150px; height: 30px}}\n\t</style>\n\t<title>You are invited to a Mezzanine meeting with Test Location</title>\n</head>\n<body style=\"margin:0; padding:0;\">\n\t<table bgcolor=\"#3F4770\" border=\"0\" style=\"width: 100%; table-layout: fixed; font-size: 24px; border-collapse:collapse\">\n\t\t<colgroup>\n\t\t<col span=\"1\" style=\"width: 20px;\">\n\t\t<col span=\"1\" style=\"width: auto;\">\n\t\t<col span=\"1\" style=\"width: 20px;\">\n\t</colgroup>\n\t<tbody>\n\t\t<tr>\n\t\t\t<td colspan=\"3\" style=\"margin:0; padding:0; background-color: #FFF; height: 30px\"></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style=\"background-color: #FFF\"></td>\n\t\t\t<td>\n\t\t\t\t<img src=\"https://www.mezz-in.com/images/invitation-header-800.png\" width=\"200\" height=\"40\" alt=\"Oblong Industries logo\" align=\"right\" style=\"margin:20px 20px 20px 0px\"/>\n\t\t\t</td>\n\t\t\t<td style=\"background-color: #FFF\"></td>\n\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style=\"background-color: #FFF\"></td>\n\t\t\t<td style=\"margin:0;\">\n\t\t\t\t<p style=\"font-family: HelveticaNeue, Verdana, sans-serif; color: #FEFEFE; font-weight: bold; padding-left: 40px; padding-right: 20px; margin: 0;\">You\'re invited to<br>join a Mezzanine meeting.</p>\n\t\t\t</td>\n\t\t\t<td style=\"background-color: #FFF\"></td>\n\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style=\"background-color: #FFF\"></td>\n\t\t\t<td style=\"height: 20px\"></td>\n\t\t\t<td style=\"background-color: #FFF\"></td>\n\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style=\"background-color: #FFF\"></td>\n\n\t\t\t<td style=\"width: 30%\">\n\t\t\t\t<div style=\"\n\t\t\t\tpadding-left: 40px;\n\t\t\t\tpadding-right: 20px;\n\t\t\t\tdisplay:inline-block;\n\t\t\t\tbackground-color:#3F4770;\n\t\t\t\twidth: 110px;\n\t\t\t\tmargin: 0;\">\n\t\t\t\t<a href=\"http://www.mezz-in.com\" style=\"\n\t\t\t\tborder-color:#FF7754;\n\t\t\t\tbackground-color: #FF7754;\n\t\t\t\tborder-radius:3px 3px 3px 3px;\n\t\t\t\tborder-width:5px 18px;\n\t\t\t\tborder-style:solid;\n\t\t\t\tfont-family: HelveticaNeue, Verdana, sans-serif;\n\t\t\t\tfont-weight:bold;\n\t\t\t\tfont-size: 14px;\n\t\t\t\twhite-space:nowrap;\n\t\t\t\tcolor:#fff!important;\n\t\t\t\tdisplay:inline-block;\n\t\t\t\ttext-decoration:none;\n\t\t\t\ttext-align:center;\n\t\t\t\twidth: auto;\n\t\t\t\theight: auto;\n\t\t\t\tmargin: 0;\n\t\t\t\tpadding: 0;\n\t\t\t\tvertical-align: middle;\n\t\t\t\tline-height: 26px;\">JOIN MEETING</a>\n\t\t\t</div>\n\t\t</td>\n\t\t<td style=\"background-color: #FFF\"></td>\n\n\t</tr>\n\t<tr>\n\t\t<td style=\"background-color: #FFF\"></td>\n\t\t<td style=\"height: 20px\"></td>\n\t\t<td style=\"background-color: #FFF\"></td>\n\n\t</tr>\n\t<tr>\n\t\t<td style=\"background-color: #FFF\"></td>\n\t\t<td>\n\t\t\t<p style=\"-webkit-text-size-adjust: 100%; font-family: HelveticaNeue, Verdana, sans-serif; font-size: 10px; color: #FEFEFE; padding-left: 40px; padding-right: 20px; margin: 0\"><b>Test Location</b> | Test Mezz</p>\n\t\t</td>\n\t\t<td style=\"background-color: #FFF\"></td>\n\n\t</tr>\n\t<tr>\n\t\t<td style=\"background-color: #FFF\"></td>\n\t\t<td style=\"height: 20px\"></td>\n\t\t<td style=\"background-color: #FFF\"></td>\n\n\t</tr>\n\t<tr>\n\t\t<td colspan=\"3\" style=\"margin:0; padding:0; background-color: #FFF; height: 30px\"></td>\n\t</tr>\n</tbody>\n</table>\n</body>\n</html>\n"

  let inviteHTMLWithCompanyAndLocation = "<!DOCTYPE html>\n<html style=\"margin:0; padding:0;\">\n<head>\n\t<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n\t<style type=\"text/css\">\n\t@media(min-width:480px){p{font-size: 28px}}\n\t@media(min-width:800px){p{font-size: 36px}}\n\t@media(max-width:460px){p{font-size: 16px}}\n\t@media(max-width:320px){p{font-size: 12px}}\n\t@media(max-width:480px){img{width: 150px; height: 30px}}\n\t@media(max-width:320px){img{width: 150px; height: 30px}}\n\t</style>\n\t<title>You are invited to a Mezzanine meeting with Test Company, Test Location</title>\n</head>\n<body style=\"margin:0; padding:0;\">\n\t<table bgcolor=\"#3F4770\" border=\"0\" style=\"width: 100%; table-layout: fixed; font-size: 24px; border-collapse:collapse\">\n\t\t<colgroup>\n\t\t<col span=\"1\" style=\"width: 20px;\">\n\t\t<col span=\"1\" style=\"width: auto;\">\n\t\t<col span=\"1\" style=\"width: 20px;\">\n\t</colgroup>\n\t<tbody>\n\t\t<tr>\n\t\t\t<td colspan=\"3\" style=\"margin:0; padding:0; background-color: #FFF; height: 30px\"></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style=\"background-color: #FFF\"></td>\n\t\t\t<td>\n\t\t\t\t<img src=\"https://www.mezz-in.com/images/invitation-header-800.png\" width=\"200\" height=\"40\" alt=\"Oblong Industries logo\" align=\"right\" style=\"margin:20px 20px 20px 0px\"/>\n\t\t\t</td>\n\t\t\t<td style=\"background-color: #FFF\"></td>\n\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style=\"background-color: #FFF\"></td>\n\t\t\t<td style=\"margin:0;\">\n\t\t\t\t<p style=\"font-family: HelveticaNeue, Verdana, sans-serif; color: #FEFEFE; font-weight: bold; padding-left: 40px; padding-right: 20px; margin: 0;\">You\'re invited to<br>join a Mezzanine meeting.</p>\n\t\t\t</td>\n\t\t\t<td style=\"background-color: #FFF\"></td>\n\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style=\"background-color: #FFF\"></td>\n\t\t\t<td style=\"height: 20px\"></td>\n\t\t\t<td style=\"background-color: #FFF\"></td>\n\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style=\"background-color: #FFF\"></td>\n\n\t\t\t<td style=\"width: 30%\">\n\t\t\t\t<div style=\"\n\t\t\t\tpadding-left: 40px;\n\t\t\t\tpadding-right: 20px;\n\t\t\t\tdisplay:inline-block;\n\t\t\t\tbackground-color:#3F4770;\n\t\t\t\twidth: 110px;\n\t\t\t\tmargin: 0;\">\n\t\t\t\t<a href=\"http://www.mezz-in.com\" style=\"\n\t\t\t\tborder-color:#FF7754;\n\t\t\t\tbackground-color: #FF7754;\n\t\t\t\tborder-radius:3px 3px 3px 3px;\n\t\t\t\tborder-width:5px 18px;\n\t\t\t\tborder-style:solid;\n\t\t\t\tfont-family: HelveticaNeue, Verdana, sans-serif;\n\t\t\t\tfont-weight:bold;\n\t\t\t\tfont-size: 14px;\n\t\t\t\twhite-space:nowrap;\n\t\t\t\tcolor:#fff!important;\n\t\t\t\tdisplay:inline-block;\n\t\t\t\ttext-decoration:none;\n\t\t\t\ttext-align:center;\n\t\t\t\twidth: auto;\n\t\t\t\theight: auto;\n\t\t\t\tmargin: 0;\n\t\t\t\tpadding: 0;\n\t\t\t\tvertical-align: middle;\n\t\t\t\tline-height: 26px;\">JOIN MEETING</a>\n\t\t\t</div>\n\t\t</td>\n\t\t<td style=\"background-color: #FFF\"></td>\n\n\t</tr>\n\t<tr>\n\t\t<td style=\"background-color: #FFF\"></td>\n\t\t<td style=\"height: 20px\"></td>\n\t\t<td style=\"background-color: #FFF\"></td>\n\n\t</tr>\n\t<tr>\n\t\t<td style=\"background-color: #FFF\"></td>\n\t\t<td>\n\t\t\t<p style=\"-webkit-text-size-adjust: 100%; font-family: HelveticaNeue, Verdana, sans-serif; font-size: 10px; color: #FEFEFE; padding-left: 40px; padding-right: 20px; margin: 0\"><b>Test Company, Test Location</b> | Test Mezz</p>\n\t\t</td>\n\t\t<td style=\"background-color: #FFF\"></td>\n\n\t</tr>\n\t<tr>\n\t\t<td style=\"background-color: #FFF\"></td>\n\t\t<td style=\"height: 20px\"></td>\n\t\t<td style=\"background-color: #FFF\"></td>\n\n\t</tr>\n\t<tr>\n\t\t<td colspan=\"3\" style=\"margin:0; padding:0; background-color: #FFF; height: 30px\"></td>\n\t</tr>\n</tbody>\n</table>\n</body>\n</html>\n"
}
