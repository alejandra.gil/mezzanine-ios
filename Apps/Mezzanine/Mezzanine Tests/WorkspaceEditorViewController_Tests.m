//
//  WorkspaceEditorViewController_Tests.m
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 27/10/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "WorkspaceEditorViewController.h"
#import "MezzanineAppContext.h"
#import "MezzanineStyleSheet.h"

@interface WorkspaceEditorViewController_Tests : XCTestCase

@property (nonatomic, strong) WorkspaceEditorViewController *controller;

@end

@implementation WorkspaceEditorViewController_Tests

- (void)setUp
{
  [super setUp];

  MZCommunicator *communicator = [MZCommunicator new];
  communicator.systemModel = [MZSystemModel new];
  communicator.systemModel.myMezzanine.version = @"3.13";
  [MezzanineAppContext currentContext].currentCommunicator = communicator;

  _controller = [[WorkspaceEditorViewController alloc] initWithWorkspace:[MZWorkspace new]];
  _controller.hintText = @"hintText";

  //load the view so we can access IBOutlets
  [_controller view];
}


- (void)tearDown
{
  [MezzanineAppContext currentContext].currentCommunicator = nil;
  [super tearDown];
}


- (void)testInitWithWorkspace
{
  XCTAssertNotNil(_controller.workspace);
  XCTAssertTrue([_controller.doneButtonTitle isEqualToString:@"Done"]);
}


- (void)testSubviewsAdded
{
  NSArray *subviews = [_controller.view subviews];
  
  XCTAssertTrue([subviews containsObject:_controller.nameTextField]);
  XCTAssertTrue([subviews containsObject:_controller.saveButton]);
  XCTAssertTrue([subviews containsObject:_controller.hintLabel]);
  
  NSArray *actionsArray = [_controller.saveButton actionsForTarget:_controller forControlEvent:UIControlEventTouchUpInside];
  XCTAssertGreaterThan([actionsArray count], 0);
  XCTAssertTrue([actionsArray containsObject:@"save"]);
}

#pragma mark - View style

- (void)testStyle
{
  XCTAssertTrue([_controller.view.backgroundColor isEqual:[UIColor whiteColor]]);
  XCTAssertTrue([_controller.hintLabel.textColor isEqual:[MezzanineStyleSheet sharedStyleSheet].grey90Color]);
}


@end
