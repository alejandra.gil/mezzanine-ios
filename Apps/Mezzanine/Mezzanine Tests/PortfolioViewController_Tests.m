//
//  PortfolioViewController_Tests.m
//  Mezzanine
//
//  Created by Zai Chang on 12/5/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "PortfolioViewController.h"
#import "PortfolioViewController_Private.h"
#import "UIViewController+FPPopover.h"
#import "FPPopoverController.h"
#import "ButtonMenuViewController.h"
#import "PortfolioMoreMenuViewController.h"
#import "MockSystemModel.h"
#import "MockOBOvum.h"
#import "MZLiveStream.h"
#import "LiveStreamCollectionViewCell.h"
#import "PortfolioCollectionViewCell.h"
#import <OCMock/OCMock.h>
#import "MezzanineMainViewController.h"
#import "MezzanineStyleSheet.h"
#import "AssetViewController.h"
#import "XCTestCase+Animations.h"
#import "MockMezzanineNavigationController.h"

@class LiveStreamCollectionViewCell;

@interface PortfolioViewController_Tests : XCTestCase

@property (nonatomic, strong) MockSystemModel *systemModelFactory;
@property (nonatomic, strong) PortfolioViewController *controller;
@property (nonatomic, strong) PortfolioMoreMenuViewController *moreButtonViewController;
@property (nonatomic, strong) MockMezzanineNavigationController *mockNavigationController;

@end


@implementation PortfolioViewController_Tests

- (void)setUp
{
  [super setUp];
  
  self.systemModelFactory = [MockSystemModel new];
  self.controller = [[PortfolioViewController alloc] initWithNibName:@"PortfolioViewController" bundle:nil];
  self.controller.workspace = self.systemModelFactory.currentWorkspace;
  self.controller.systemModel = self.systemModelFactory.systemModel;

  _mockNavigationController = [[MockMezzanineNavigationController alloc] initWithRootViewController:_controller];
  [MezzanineAppContext currentContext].mainNavController = _mockNavigationController;

  //load the view so we can access IBOutlets
  [self.controller view];  
}


- (void)tearDown
{
  [super tearDown];

  _controller.workspace = nil;
  _controller.systemModel = nil;
  _controller.appContext = nil;
  _controller = nil;
  _systemModelFactory = nil;
  _moreButtonViewController = nil;
  
  [MezzanineAppContext currentContext].mainNavController = [MockMezzanineNavigationController new];

}


#pragma mark - Outlets

- (void)testCollectionViewOutlet
{
  XCTAssertNotNil(self.controller.collectionView);
  XCTAssertEqual(self.controller.collectionView.dataSource, self.controller);
  XCTAssertEqual(self.controller.collectionView.delegate, self.controller);
}


- (void)testTitleLabelOutlet
{
  XCTAssertNotNil(self.controller.titleLabel);
  XCTAssertNotNil(self.controller.titleLabelLeftConstraint);
  XCTAssertTrue([self.controller.titleLabel.text isEqualToString:@"PORTFOLIO"]);
  XCTAssertEqual([self.controller.titleLabel.gestureRecognizers count], 1);
  XCTAssertTrue([[self.controller.titleLabel.gestureRecognizers firstObject] class] == [UILongPressGestureRecognizer class]);
}


- (void)testAddButtonOutlet
{
  XCTAssertNotNil(self.controller.addButton);
  XCTAssertTrue([[self.controller.addButton titleForState:UIControlStateNormal] isEqualToString:@"ADD"]);
  NSArray *actionsArray = [self.controller.addButton actionsForTarget:self.controller forControlEvent:UIControlEventTouchUpInside];
  XCTAssertGreaterThan([actionsArray count], 0);
  XCTAssertTrue([actionsArray containsObject:@"add:"]);
  
  XCTAssertNotNil(self.controller.addButtonImageView.image);
}


- (void)testMoreButtonOutlet
{
  XCTAssertNotNil(self.controller.moreButton);
  XCTAssertTrue([[self.controller.moreButton titleForState:UIControlStateNormal] isEqualToString:@"MORE"]);
  NSArray *actionsArray = [self.controller.moreButton actionsForTarget:self.controller forControlEvent:UIControlEventTouchUpInside];
  XCTAssertGreaterThan([actionsArray count], 0);
  XCTAssertTrue([actionsArray containsObject:@"more:"]);
  
  XCTAssertNotNil(self.controller.moreButtonElipsisImageView.image);
}


- (void)testPresentButtonOutlet
{
  XCTAssertNotNil(self.controller.presentButton);
  
  XCTAssertTrue([[self.controller.presentButton titleForState:UIControlStateNormal] isEqualToString:@"PRESENT"]);
  NSArray *actionsArray = [self.controller.presentButton actionsForTarget:self.controller forControlEvent:UIControlEventTouchUpInside];
  XCTAssertGreaterThan([actionsArray count], 0);
  XCTAssertTrue([actionsArray containsObject:@"present:"]);
  
  XCTAssertNotNil(self.controller.presentButtonImageView.image);
  XCTAssertNotNil(self.controller.moreButtonElipsisImageView.image);
}


- (void)testHintLabelOutlet
{
  XCTAssertNotNil(self.controller.hintLabel);
  XCTAssertTrue([self.controller.hintLabel.text isEqualToString:@"Add content to the portfolio from your device to present."]);
}


- (void)testStatusOutlet
{
  XCTAssertNotNil(self.controller.progressPieView);
  XCTAssertNotNil(self.controller.statusView);
  XCTAssertNotNil(self.controller.statusLabel);
}


#pragma mark - Background

- (void)testBackgroundColor
{
  XCTAssertTrue([_controller.view.backgroundColor isEqual:[MezzanineStyleSheet sharedStyleSheet].grey40Color]);
}


#pragma mark - Update status

- (void)testUpdateStatusNoPendingTransactionsNoBatch
{
  self.controller.appContext.currentCommunicator.pendingTransactions = 0;
  [self.controller.appContext.currentCommunicator.uploader cancelAllPendingFileUploads];
  self.controller.preparingImageBatchFromAssetLibrary = NO;
  
  [self.controller updateStatus];
  
  XCTAssertTrue([self.controller.statusLabel.text isEqualToString:@""]);
  XCTAssertTrue(self.controller.progressPieView.state == ProgressPieStateInvisible);
  XCTAssertTrue(self.controller.statusView.hidden);
}


- (void)testUpdateStatusNoPendingTransactionsBatch
{
  self.controller.appContext.currentCommunicator.pendingTransactions = 0;
  [self.controller.appContext.currentCommunicator.uploader cancelAllPendingFileUploads];
  self.controller.preparingImageBatchFromAssetLibrary = YES;
  
  [self.controller updateStatus];
  
  XCTAssertTrue([self.controller.statusLabel.text isEqualToString:@"Preparing images to upload"]);
  XCTAssertTrue(self.controller.progressPieView.state == ProgressPieStateActivity);
  XCTAssertFalse(self.controller.statusView.hidden);
}


- (void)testUpdateStatusUploadingOnePDF
{
  NSInteger transactionsLeft = 1;
  
  NSMutableArray *pendingTransactionsArray = [@[] mutableCopy];
  for (int c = 0; c < transactionsLeft; c++) {
    MZImageUploadTransaction *imageUploadTransaction = [MZImageUploadTransaction new];
    imageUploadTransaction.transactionName = MZPDFReadyTransactionName;
    imageUploadTransaction.workspaceUid = [NSString stringWithFormat:@"ws-000%i", c];
    [pendingTransactionsArray addObject:imageUploadTransaction];
  }

  id uploader = [OCMockObject niceMockForClass:[MZUploader class]];
  [[[uploader stub] andReturnValue:[NSNumber numberWithInteger:0]] numberOfUploadTransactions];

  id mezzComunicator = [OCMockObject niceMockForClass:[MZCommunicator class]];
  [[[mezzComunicator stub] andReturnValue:[NSNumber numberWithInteger:0]] numberOfImagesBeingUploaded];
  [[[mezzComunicator stub] andReturnValue:[NSNumber numberWithInteger:transactionsLeft]] numberOfFilesBeingUploaded];
  
  [[[mezzComunicator stub] andReturn:pendingTransactionsArray] pendingTransactions];
  [[[mezzComunicator stub] andReturn:uploader] requestor];

  MezzanineAppContext *appContext = [MezzanineAppContext new];
  appContext.currentCommunicator = mezzComunicator;
  
  self.controller.appContext = appContext;
  [self.controller updateStatus];
  
  XCTAssertTrue([self.controller.statusLabel.text isEqualToString:@"Uploading PDF"]);
  XCTAssertTrue(self.controller.progressPieView.state == ProgressPieStateActivity);
}


- (void)testUpdateStatusUploadingMultipleImages
{
  NSInteger transactionsLeft = 2;
  
  NSMutableArray *pendingTransactionsArray = [@[] mutableCopy];
  for (int c = 0; c < transactionsLeft; c++) {
    MZImageUploadTransaction *imageUploadTransaction = [MZImageUploadTransaction new];
    imageUploadTransaction.transactionName = MZImageUploadTransactionName;
    imageUploadTransaction.workspaceUid = [NSString stringWithFormat:@"ws-000%i", c];
    [pendingTransactionsArray addObject:imageUploadTransaction];
  }

  id uploader = [OCMockObject niceMockForClass:[MZUploader class]];
  [[[uploader stub] andReturnValue:[NSNumber numberWithInteger:0]] numberOfUploadTransactions];

  id mezzComunicator = [OCMockObject niceMockForClass:[MZCommunicator class]];
  [[[mezzComunicator stub] andReturnValue:[NSNumber numberWithInteger:transactionsLeft]] numberOfImagesBeingUploaded];
  [[[mezzComunicator stub] andReturnValue:[NSNumber numberWithInteger:0]] numberOfFilesBeingUploaded];

  [[[mezzComunicator stub] andReturn:pendingTransactionsArray] pendingTransactions];
  [[[mezzComunicator stub] andReturn:uploader] requestor];

  MezzanineAppContext *appContext = [MezzanineAppContext new];
  appContext.currentCommunicator = mezzComunicator;

  self.controller.appContext = appContext;
  [self.controller updateStatus];
  
  NSString *expectedString = [NSString stringWithFormat:@"Uploading %ld images", (long)transactionsLeft];
  XCTAssertTrue([self.controller.statusLabel.text isEqualToString:expectedString]);
  XCTAssertTrue(self.controller.progressPieView.state == ProgressPieStateActivity);
}


- (void)testPrepareImageBatch
{
  id mezzComunicator = [OCMockObject niceMockForClass:[MZCommunicator class]];
  [[[mezzComunicator stub] andReturnValue:[NSNumber numberWithInteger:0]] numberOfImagesBeingUploaded];
  [[[mezzComunicator stub] andReturnValue:[NSNumber numberWithInteger:0]] numberOfFilesBeingUploaded];
  [[[mezzComunicator stub] andReturn:@[]] pendingTransactions];

  id uploader = [OCMockObject niceMockForClass:[MZUploader class]];
  [[[uploader stub] andReturnValue:[NSNumber numberWithInteger:0]] numberOfUploadTransactions];
  [[[mezzComunicator stub] andReturn:uploader] uploader];

  MezzanineAppContext *appContext = [MezzanineAppContext new];
  appContext.currentCommunicator = mezzComunicator;
  self.controller.appContext = appContext;
  
  self.controller.preparingImageBatchFromAssetLibrary = YES;

  [self.controller updateStatus];
  
  XCTAssertTrue([self.controller.statusLabel.text isEqualToString:@"Preparing images to upload"]);
  XCTAssertTrue(self.controller.progressPieView.state == ProgressPieStateActivity);
}


- (void)testUpdateStatusExport
{
  self.controller.systemModel = [MZSystemModel new];
  self.controller.systemModel.portfolioExport.info.state = MZExportStateDownloading;
  
  [self.controller updateStatus];
  
  XCTAssertTrue([self.controller.statusLabel.text isEqualToString:@"Downloading portfolio PDF"]);
  XCTAssertTrue(self.controller.progressPieView.state == ProgressPieStateProgress);
}


#pragma mark - Present Add menu

- (void)testPresentFirstTime
{
  NSInteger numberOfSlides = 10;
  [self setUpPortfolioWithNumberOfItems:numberOfSlides];

  id mezzRequestor = [OCMockObject mockForClass:[MZCommunicatorRequestor class]];
  [[mezzRequestor expect] requestPresentationStartRequest:_controller.workspace.uid currentIndex:0];
  _controller.appContext = [MezzanineAppContext currentContext];
  _controller.appContext.currentCommunicator = [MZCommunicator new];
  _controller.appContext.currentCommunicator.requestor = mezzRequestor;

  [_controller present:nil];

  [mezzRequestor verify];
  XCTAssertEqual(_controller.workspace.presentation.currentSlideIndex, 0, @"Presentation should start in index 0");
}


- (void)testPresentFromADifferentPosition
{
  NSInteger numberOfSlides = 10;
  [self setUpPortfolioWithNumberOfItems:numberOfSlides];


  id mezzRequestor = [OCMockObject mockForClass:[MZCommunicatorRequestor class]];
  [[mezzRequestor expect] requestPresentationStartRequest:_controller.workspace.uid currentIndex:6];
  _controller.appContext = [MezzanineAppContext currentContext];
  _controller.appContext.currentCommunicator = [MZCommunicator new];
  _controller.appContext.currentCommunicator.requestor = mezzRequestor;
  
  _controller.systemModel.myMezzanine.version = @"3.13";
  _controller.workspace.presentation.currentSlideIndex = 6;

  [_controller present:nil];

  [mezzRequestor verify];
}


- (void)testPresentAfterPausingPresentationOn3_2
{
  NSInteger numberOfSlides = 10;
  [self setUpPortfolioWithNumberOfItems:numberOfSlides];

  id mezzRequestor = [OCMockObject mockForClass:[MZCommunicatorRequestor class]];
  [[mezzRequestor expect] requestPresentationStartRequest:_controller.workspace.uid currentIndex:0];
  _controller.appContext = [MezzanineAppContext currentContext];
  _controller.appContext.currentCommunicator = [MZCommunicator new];
  _controller.appContext.currentCommunicator.requestor = mezzRequestor;

  _controller.systemModel.myMezzanine.version = @"3.13";

  [_controller present:nil];

  _controller.workspace.presentation.currentSlideIndex = 6;

  [[mezzRequestor expect] requestPresentationStopRequest:_controller.workspace.uid];

  [_controller present:nil]; // IBAction call to pause

  [[mezzRequestor expect] requestPresentationStartRequest:_controller.workspace.uid currentIndex:6];
  [[mezzRequestor reject] requestPresentationStartRequest:_controller.workspace.uid currentIndex:0];

  [_controller present:nil]; // and present again

  [mezzRequestor verify];
}


#pragma mark - Present Add menu

- (void)testPresentAddMenu
{
  [self.controller add:nil];
  
  XCTAssertNotNil(self.controller.currentPopover);
  
  ButtonMenuViewController *contentViewController = (ButtonMenuViewController *) self.controller.currentPopover.viewController;
  XCTAssertNotNil(contentViewController);
  XCTAssertTrue([contentViewController isKindOfClass:[ButtonMenuViewController class]]);
  XCTAssertGreaterThanOrEqual(contentViewController.menuItems.count, 0);
  
  for (ButtonMenuItem *button in contentViewController.menuItems) {
    XCTAssertTrue(button.enabled);
    XCTAssertNotNil(button.action);
  }
}


- (void)testPresentAddMenuNoWhiteboards
{
  NSInteger numberOfWhiteboards = 0;
  
  _controller.systemModel = [_systemModelFactory createTestModelWithWhiteboards:numberOfWhiteboards];
  [_controller add:nil];
  
  ButtonMenuViewController *contentViewController = (ButtonMenuViewController *) _controller.currentPopover.viewController;
  XCTAssertTrue([contentViewController isKindOfClass:[ButtonMenuViewController class]]);
  
  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"buttonTitle contains[c] %@", @"Whiteboard"];
  NSArray *filteredItems = [contentViewController.menuItems filteredArrayUsingPredicate:predicate];
  XCTAssertEqual(0, filteredItems.count);
}


- (void)testPresentAddMenuSingleWhiteboard
{
  NSInteger numberOfWhiteboards = 1;
  
  _controller.systemModel = [_systemModelFactory createTestModelWithWhiteboards:numberOfWhiteboards];
  [_controller add:nil];
  
  ButtonMenuViewController *contentViewController = (ButtonMenuViewController *) _controller.currentPopover.viewController;
  XCTAssertTrue([contentViewController isKindOfClass:[ButtonMenuViewController class]]);
  
  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"buttonTitle contains[c] %@", @"Whiteboard"];
  NSArray *filteredItems = [contentViewController.menuItems filteredArrayUsingPredicate:predicate];
  XCTAssertEqual(1, filteredItems.count);
  
  for (ButtonMenuItem *button in contentViewController.menuItems) {
    XCTAssertTrue(button.enabled);
    XCTAssertNotNil(button.action);
  }
}


- (void)testPresentAddMenuMultipleWhiteboard
{
  NSInteger numberOfWhiteboards = 4;
  
  _controller.systemModel = [_systemModelFactory createTestModelWithWhiteboards:numberOfWhiteboards];
  [_controller add:nil];
  
  ButtonMenuViewController *contentViewController = (ButtonMenuViewController *) _controller.currentPopover.viewController;
  XCTAssertTrue([contentViewController isKindOfClass:[ButtonMenuViewController class]]);
  
  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"buttonTitle contains[c] %@", @"Whiteboard"];
  NSArray *filteredItems = [contentViewController.menuItems filteredArrayUsingPredicate:predicate];
  XCTAssertEqual(1, filteredItems.count);
  
  for (ButtonMenuItem *button in contentViewController.menuItems) {
    XCTAssertTrue(button.enabled);
    XCTAssertNotNil(button.action);
  }
}


#pragma mark - Present More menu

- (void)testMoreMenuEnabled
{
  NSInteger numberOfSlides = 4;
  [self setUpPortfolioWithNumberOfItems:numberOfSlides];
  
  // Conditions
  XCTAssertTrue([self.moreButtonViewController isKindOfClass:[PortfolioMoreMenuViewController class]]);
  XCTAssertEqual(numberOfSlides, self.controller.systemModel.currentWorkspace.presentation.slides.count);

  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.buttonTitle contains %@", NSLocalizedString(@"Portfolio Export Button Sign In Title", nil)];
  NSArray *filteredArray = [self.moreButtonViewController.menuItems filteredArrayUsingPredicate:predicate];
  XCTAssertEqual(filteredArray.count, 0, @"Menu should not request to be signed in");

  for (ButtonMenuItem *button in _moreButtonViewController.menuItems) {
    XCTAssertTrue(button.enabled);
    XCTAssertNotNil(button.action);
  }
}


- (void)testMoreMenuDisabled
{
  NSInteger numberOfSlides = 0;
  [self setUpPortfolioWithNumberOfItems:numberOfSlides];
  
  // Conditions
  XCTAssertTrue([self.moreButtonViewController isKindOfClass:[PortfolioMoreMenuViewController class]]);
  XCTAssertEqual(numberOfSlides, self.controller.systemModel.currentWorkspace.presentation.slides.count);

  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.buttonTitle contains %@", NSLocalizedString(@"Portfolio Export Button Sign In Title", nil)];
  NSArray *filteredArray = [self.moreButtonViewController.menuItems filteredArrayUsingPredicate:predicate];
  XCTAssertEqual(filteredArray.count, 0, @"Menu should not request to be signed in");

  for (ButtonMenuItem *button in self.moreButtonViewController.menuItems) {
    XCTAssertFalse(button.enabled);
  }
}


- (void)testMoreMenuWhenDisableDownloadsFlagIsActiveAndUserIsNotSignedIn
{
  self.controller.systemModel.featureToggles.disableDownloadsEnabled = YES;
  self.controller.systemModel.currentUsername = nil;

  NSInteger numberOfSlides = 4;
  [self setUpPortfolioWithNumberOfItems:numberOfSlides];

  // Conditions
  XCTAssertTrue([self.moreButtonViewController isKindOfClass:[PortfolioMoreMenuViewController class]]);
  XCTAssertEqual(numberOfSlides, self.controller.systemModel.currentWorkspace.presentation.slides.count);

  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.buttonTitle contains %@", NSLocalizedString(@"Portfolio Export Button Sign In Title", nil)];
  NSArray *filteredArray = [self.moreButtonViewController.menuItems filteredArrayUsingPredicate:predicate];
  XCTAssertEqual(filteredArray.count, 1, @"Menu should request to be signed in to download");

  for (ButtonMenuItem *button in self.moreButtonViewController.menuItems) {
    if ([filteredArray containsObject:button])
    {
      XCTAssertFalse(button.enabled);
      XCTAssertNil(button.action);
    }
    else
    {
      XCTAssertTrue(button.enabled);
      XCTAssertNotNil(button.action);
    }
  }
}

- (void)testMoreMenuWhenDisableDownloadsFlagIsActiveAndUserIsSignedIn
{
  self.controller.systemModel.featureToggles.disableDownloadsEnabled = YES;
  self.controller.systemModel.currentUsername = @"test-user";

  NSInteger numberOfSlides = 4;
  [self setUpPortfolioWithNumberOfItems:numberOfSlides];

  // Conditions
  XCTAssertTrue([self.moreButtonViewController isKindOfClass:[PortfolioMoreMenuViewController class]]);
  XCTAssertEqual(numberOfSlides, self.controller.systemModel.currentWorkspace.presentation.slides.count);

  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.buttonTitle contains %@", NSLocalizedString(@"Portfolio Export Button Sign In Title", nil)];
  NSArray *filteredArray = [self.moreButtonViewController.menuItems filteredArrayUsingPredicate:predicate];
  XCTAssertEqual(filteredArray.count, 0, @"Menu should not request to be signed in");

  for (ButtonMenuItem *button in _moreButtonViewController.menuItems) {
    XCTAssertTrue(button.enabled);
    XCTAssertNotNil(button.action);
  }
}


- (void)testUpdatePresentationModeStateInactive
{
  [self setUpPortfolioWithNumberOfItems:1];
  self.controller.workspace.presentation.active = NO;
  [self.controller updatePresentationModeState];
  XCTAssertNotEqual([self.controller.presentButton.titleLabel.text rangeOfString:@"Present" options:NSCaseInsensitiveSearch].location, NSNotFound);
  XCTAssertNotNil(self.controller.presentButtonImageView.image);
  XCTAssertTrue(self.controller.presentButton.enabled);
}


- (void)testUpdatePresentationModeStateActive
{
  [self setUpPortfolioWithNumberOfItems:1];
  self.controller.workspace.presentation.active = YES;
  [self.controller updatePresentationModeState];
  XCTAssertNotEqual([self.controller.presentButton.titleLabel.text rangeOfString:@"Stop" options:NSCaseInsensitiveSearch].location, NSNotFound);
  XCTAssertNotNil(self.controller.presentButtonImageView.image);
  XCTAssertTrue(self.controller.presentButton.enabled);
}


- (void)testUpdatePresentationModeStateDisabled
{
  [self setUpPortfolioWithNumberOfItems:0];
  [self.controller updatePresentationModeState];
  XCTAssertFalse(self.controller.presentButton.enabled);
  XCTAssertNotEqual([self.controller.presentButton.titleLabel.text rangeOfString:@"Present" options:NSCaseInsensitiveSearch].location, NSNotFound);
  XCTAssertNotNil(self.controller.presentButtonImageView.image);
}


#pragma mark - Add items to portfolio

- (void)testAddMenuCameraOption
{
  [_controller add:nil];
  
  ButtonMenuViewController *contentViewController = (ButtonMenuViewController *) _controller.currentPopover.viewController;
  XCTAssertTrue([contentViewController isKindOfClass:[ButtonMenuViewController class]]);
}

- (void)testAddMenuPhotoLibraryOption
{
  [_controller add:nil];
  
  ButtonMenuViewController *contentViewController = (ButtonMenuViewController *) _controller.currentPopover.viewController;
  XCTAssertTrue([contentViewController isKindOfClass:[ButtonMenuViewController class]]);
  
}


#pragma mark - Delete portfolio items

- (void)testDeleteSinglePortfolioItem
{
  NSInteger numberOfSlides = 3;
  [self setUpPortfolioWithNumberOfItems:numberOfSlides];
  
  XCTAssertEqual(numberOfSlides, [self.controller.collectionView numberOfItemsInSection:0]);
  
  [self.controller.presentation removeObjectFromSlidesAtIndex:0];
  [self.controller.collectionView layoutIfNeeded];
  
  XCTAssertNotNil(self.controller.collectionView);
  XCTAssertEqual(numberOfSlides - 1, [self.controller.collectionView numberOfItemsInSection:0]);
  XCTAssertNil([self.controller.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:numberOfSlides - 1 inSection:0]]);
 
  for (ButtonMenuItem *button in self.moreButtonViewController.menuItems) {
    XCTAssertTrue(button.enabled);
  }
}


- (void)testDeleteAllPortfolioItems
{
  NSInteger numberOfSlides = 3;
  [self setUpPortfolioWithNumberOfItems:numberOfSlides];
  
  // Conditions
  XCTAssertTrue([self.moreButtonViewController isKindOfClass:[PortfolioMoreMenuViewController class]]);
  XCTAssertEqual(numberOfSlides, [self.controller.collectionView numberOfItemsInSection:0]);
  
  [self.controller.presentation removeAllSlides];
  [self.controller.collectionView layoutIfNeeded];

  XCTAssertEqual(0, [self.controller.collectionView numberOfItemsInSection:0]);
  XCTAssertNil([self.controller.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]]);

  for (ButtonMenuItem *button in self.moreButtonViewController.menuItems) {
    XCTAssertFalse(button.enabled);
  }
}


#pragma mark - Drag portfolio items

- (void)testDragLiveStreamItem
{
  NSInteger numberOfSlides = 1;
  [self setUpPortfolioWithNumberOfItems:numberOfSlides];

  NSInteger insertionIndex = 0;
  MockOBOvum *ovum = [MockOBOvum new];
  [ovum setOBOvumLiveStreamDataObject:[MZLiveStream new] withIndex:insertionIndex];

  [self.controller.presentation insertObject:ovum.dataObject inSlidesAtIndex:insertionIndex];
  
  // Necessary set of calls to update model&UI
  [self.controller.presentation updateSlideIndexes];
  [self.controller.collectionView layoutIfNeeded];
  
  XCTAssertEqual(numberOfSlides + 1, [self.controller.collectionView numberOfItemsInSection:0]);
  
  UICollectionViewCell *livestreamItemCell = [self.controller.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:insertionIndex inSection:0]];
  XCTAssertEqual([livestreamItemCell class], [LiveStreamCollectionViewCell class]);
  NSInteger livestreamIndex = [[(LiveStreamCollectionViewCell *) livestreamItemCell liveStream] index];

  insertionIndex++;
  UICollectionViewCell *portfolioItemCell = [self.controller.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:insertionIndex inSection:0]];
  XCTAssertEqual([portfolioItemCell class], [PortfolioCollectionViewCell class]);
  NSInteger portfolioIndex = [[(PortfolioCollectionViewCell *) portfolioItemCell slide] index];
  
  XCTAssertGreaterThan(portfolioIndex, livestreamIndex);
  XCTAssertGreaterThan(portfolioItemCell.frame.origin.x, livestreamItemCell.frame.origin.x);
}


- (void)testDragPortfolioItem
{
  NSInteger numberOfSlides = 2;
  [self setUpPortfolioWithNumberOfItems:numberOfSlides];
  
  MZPortfolioItem *portfolioItem = [self.controller.presentation.slides firstObject];
  [self.controller.presentation moveSlide:portfolioItem toIndex:numberOfSlides-1];
  
  // Necessary set of calls to update model&UI
  [self.controller.presentation updateSlideIndexes];
  [self.controller.collectionView layoutIfNeeded];
  
  UICollectionViewCell *portfolioFirstItemCell = [self.controller.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
  UICollectionViewCell *portfolioSecondItemCell = [self.controller.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];

  XCTAssertEqual([portfolioFirstItemCell class], [PortfolioCollectionViewCell class]);
  XCTAssertGreaterThan([[(PortfolioCollectionViewCell *)portfolioSecondItemCell slide] index], [[(PortfolioCollectionViewCell *)portfolioFirstItemCell slide] index]);

  XCTAssertEqual([portfolioSecondItemCell class], [PortfolioCollectionViewCell class]);
  XCTAssertGreaterThan(portfolioSecondItemCell.frame.origin.x, portfolioFirstItemCell.frame.origin.x);
}


#pragma mark - Helpers

- (void)setUpPortfolioWithNumberOfItems:(NSInteger)numberOfItems
{
  self.controller.systemModel = [self.systemModelFactory createTestModelWithSlides:numberOfItems];
  [self.controller setPresentation:self.controller.systemModel.currentWorkspace.presentation];
  [self.controller.workspace setPresentation:self.controller.systemModel.currentWorkspace.presentation];
  
  [self.controller more:nil];
  
  // Set presentation to the menu
  self.moreButtonViewController = (PortfolioMoreMenuViewController *) _controller.currentPopover.viewController;
  [self.moreButtonViewController setSystemModel:self.controller.systemModel];
}


#pragma mark - UICollectionViewDelegate

- (void)testSelectItem
{
  NSInteger numberOfSlides = 2;
  [self setUpPortfolioWithNumberOfItems:numberOfSlides];
  
  [self.controller collectionView:self.controller.collectionView didSelectItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
  
  XCTAssertNotNil(self.controller.currentPopover);
  XCTAssertTrue([self.controller.currentPopover.viewController isKindOfClass:[ButtonMenuViewController class]]);
  
  ButtonMenuViewController *contentViewController = (ButtonMenuViewController *) self.controller.currentPopover.viewController;
  XCTAssertNotNil(contentViewController);
  XCTAssertGreaterThan(contentViewController.menuItems.count, 0);
  
  for (ButtonMenuItem *button in contentViewController.menuItems) {
    XCTAssertTrue(button.enabled);
    XCTAssertNotNil(button.action);
  }
}


- (void)testPresentFromOrJumpToSlide
{
  NSInteger numberOfSlides = 2;
  [self setUpPortfolioWithNumberOfItems:numberOfSlides];

  _controller.presentation.active = NO;
  [self.controller collectionView:self.controller.collectionView didSelectItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
  ButtonMenuViewController *contentViewController = (ButtonMenuViewController *) self.controller.currentPopover.viewController;
  NSArray *buttonTitles = [contentViewController.menuItems valueForKeyPath:@"buttonTitle"];
  XCTAssertTrue([buttonTitles containsObject:NSLocalizedString(@"Portfolio Item Present From Slide", nil)], @"There should be a button to start presenting from the selected item");

  _controller.presentation.active = YES;
  [self.controller collectionView:self.controller.collectionView didSelectItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
  contentViewController = (ButtonMenuViewController *) self.controller.currentPopover.viewController;
  buttonTitles = [contentViewController.menuItems valueForKeyPath:@"buttonTitle"];
  XCTAssertTrue([buttonTitles containsObject:NSLocalizedString(@"Portfolio Item Jump To Slide", nil)], @"There should be a button to continue the presentation from the selected item");
}


#pragma mark - OBOvum

- (void)testCreateDragRepresentationOfSourceViewPortfolioCell
{
  PortfolioCollectionViewCell *cell = [PortfolioCollectionViewCell new];
  UIView *view = [self.controller createDragRepresentationOfSourceView:cell inWindow:[UIWindow new]];
  XCTAssertTrue([view isKindOfClass:[cell class]]);
}


- (void)testCreateDragRepresentationOfSourceViewLiveStreamCell
{
  LiveStreamCollectionViewCell *cell = [LiveStreamCollectionViewCell new];
  UIView *view = [self.controller createDragRepresentationOfSourceView:cell inWindow:[UIWindow new]];
  XCTAssertTrue([view isKindOfClass:[cell class]]);
}


- (void)testCreateDragRepresentationOfSourceViewNil
{
  UITableViewCell *cell = [UITableViewCell new];
  UIView *view = [self.controller createDragRepresentationOfSourceView:cell inWindow:[UIWindow new]];
  XCTAssertNil(view);
}


- (void)testCreateOvumFromViewNil
{
  UIView *cellView = [UIView new];
  OBOvum *obvum = [self.controller createOvumFromView:cellView];
  
  XCTAssertNil(obvum);
}


- (void)testCreateOvumFromView
{
  NSInteger numberOfSlides = 1;
  [self setUpPortfolioWithNumberOfItems:numberOfSlides];
  
  XCTAssertEqual(numberOfSlides, [self.controller.collectionView numberOfItemsInSection:0]);
  UICollectionViewCell *cell = [UICollectionViewCell new];
  OBOvum *obvum = [self.controller createOvumFromView:cell];
  
  XCTAssertNotNil(obvum);
}


- (void)testDragViewWillAppear
{
  UIView *dragView = [UIView new];
  [self.controller dragViewWillAppear:dragView inWindow:[UIWindow new] atLocation:CGPointZero];
  XCTAssertTrue(dragView.alpha == 0.75);
}


- (void)testOvumDragWillBeginFail
{
  id ob = [OCMockObject niceMockForClass:[OBOvum class]];
  [[[ob stub] andReturn:nil] dataObject];
  
  id mezzRequestor = [OCMockObject niceMockForClass:[MZCommunicatorRequestor class]];
  [[mezzRequestor reject] requestPortfolioItemGrabRequest:@"" uid:@""];
  _controller.appContext.currentCommunicator = [MZCommunicator new];
  [MezzanineAppContext currentContext].currentCommunicator.requestor = mezzRequestor;

  [self.controller ovumDragWillBegin:ob];
  
  [mezzRequestor verify];
}


- (void)testOvumDragWillBeginSucces
{
  MZSlide *slide = [MZSlide new];
  slide.uid = @"slide-uid";
  
  
  id ob = [OCMockObject niceMockForClass:[OBOvum class]];
  [[[ob stub] andReturn:slide] dataObject];
  
  id mezzRequestor = [OCMockObject niceMockForClass:[MZCommunicatorRequestor class]];
  [[mezzRequestor expect] requestPortfolioItemGrabRequest:self.controller.workspace.uid uid:slide.uid];
  [MezzanineAppContext currentContext].currentCommunicator = [MZCommunicator new];
  [MezzanineAppContext currentContext].currentCommunicator.requestor = mezzRequestor;
  
  [self.controller ovumDragWillBegin:ob];
  
  [mezzRequestor verify];
}


- (void)testOvumDragEndedFail
{
  id ob = [OCMockObject niceMockForClass:[OBOvum class]];
  [[[ob stub] andReturn:nil] dataObject];
  
  id mezzRequestor = [OCMockObject niceMockForClass:[MZCommunicatorRequestor class]];
  [[mezzRequestor reject] requestPortfolioItemRelinquishRequest:@"" uid:@""];
  
  [self.controller ovumDragEnded:ob];
  
  [mezzRequestor verify];
}


- (void)testOvumDragEndedSuccess
{
  MZSlide *slide = [MZSlide new];
  slide.uid = @"slide-uid";
  
  
  id ob = [OCMockObject niceMockForClass:[OBOvum class]];
  [[[ob stub] andReturn:slide] dataObject];
  
  id mezzRequestor = [OCMockObject mockForClass:[MZCommunicatorRequestor class]];
  [[mezzRequestor expect] requestPortfolioItemRelinquishRequest:self.controller.workspace.uid uid:slide.uid];
  [MezzanineAppContext currentContext].currentCommunicator = [MZCommunicator new];
  [MezzanineAppContext currentContext].currentCommunicator.requestor = mezzRequestor;
  
  [self.controller ovumDragEnded:ob];
  
  [mezzRequestor verify];
}

- (void)testOvumEnteredNil
{
  XCTAssertEqual(OBDropActionNone, [self.controller ovumEntered:nil inView:nil atLocation:CGPointZero]);
}



- (void)testOvumEnteredLiveStream
{
  id ob = [OCMockObject niceMockForClass:[OBOvum class]];
  [[[ob stub] andReturn:[MZLiveStream new]] dataObject];
  
  XCTAssertEqual(OBDropActionCopy, [self.controller ovumEntered:ob inView:nil atLocation:CGPointZero]);
}


- (void)testOvumEnteredPortfolioItem
{
  id ob = [OCMockObject niceMockForClass:[OBOvum class]];
  [[[ob stub] andReturn:[MZPortfolioItem new]] dataObject];
  
  XCTAssertEqual(OBDropActionNone, [self.controller ovumEntered:nil inView:nil atLocation:CGPointZero]);
}


- (void)testOvumMovedIndexNil
{
  id ob = [OCMockObject niceMockForClass:[OBOvum class]];
  [self.controller ovumMoved:ob inView:nil atLocation:CGPointZero];
  UIView *insertionMarker = [_controller valueForKey:@"_insertionMarker"];
  XCTAssertNotNil(insertionMarker);
}


- (void)testOvumMovedShowMarker
{
  id ob = [OCMockObject niceMockForClass:[OBOvum class]];
  [[[ob stub] andReturn:[MZLiveStream new]] dataObject];
  [self.controller ovumMoved:ob inView:nil atLocation:CGPointZero];
  UIView *insertionMarker = [_controller valueForKey:@"_insertionMarker"];
  XCTAssertNotNil(insertionMarker);
}


- (void)testOvumMovedDoNothing
{
  id ob = [OCMockObject niceMockForClass:[OBOvum class]];
  [[[ob stub] andReturn:[MZPortfolioItem new]] dataObject];
  [self.controller ovumMoved:ob inView:nil atLocation:CGPointZero];
  UIView *insertionMarker = [_controller valueForKey:@"_insertionMarker"];
  XCTAssertNil(insertionMarker);
}


#pragma mark - Private methods tests

- (void)testOpenAssetSlideNil
{
  XCTAssertTrue(_mockNavigationController.pushViewControllerWithTransitionViewInvocations.count == 0);
  [self.controller openAssetViewerFromCell:nil];
  XCTAssertTrue(_mockNavigationController.pushViewControllerWithTransitionViewInvocations.count == 0);
}


- (void)testOpenPortfolioCell
{
  PortfolioCollectionViewCell *cell = [PortfolioCollectionViewCell new];
  self.controller.appContext = [MezzanineAppContext currentContext];

  XCTAssertTrue(_mockNavigationController.pushViewControllerWithTransitionViewInvocations.count == 0);
  [self.controller openAssetViewerFromCell:cell];
  XCTAssertTrue(_mockNavigationController.pushViewControllerWithTransitionViewInvocations.count == 1);
}


- (void)testOpenLivestreamCell
{
  LiveStreamCollectionViewCell *cell = [LiveStreamCollectionViewCell new];
  self.controller.appContext = [MezzanineAppContext currentContext];

  XCTAssertTrue(_mockNavigationController.pushViewControllerWithTransitionViewInvocations.count == 0);
  [self.controller openAssetViewerFromCell:cell];
  XCTAssertTrue(_mockNavigationController.pushViewControllerWithTransitionViewInvocations.count == 1);
}


- (void)testClearPortfolioAction
{
  [self.controller clearPortfolio];
  
  UIAlertController *alertController = [_controller valueForKey:@"_currentAlertController"];

  XCTAssertNotNil(alertController);
  XCTAssertTrue([alertController.title isEqualToString:@"Delete All Portfolio Items"]);
  XCTAssertTrue(alertController.actions.count == 2);
}


- (void)testExportPortfolioAction
{
  id mezzRequestor = [OCMockObject niceMockForClass:[MZCommunicatorRequestor class]];
  [[mezzRequestor expect] requestPortfolioDownloadRequest:_controller.workspace.uid];
  
  MezzanineAppContext *appContext = [MezzanineAppContext new];
  appContext.currentCommunicator = [MZCommunicator new];
  appContext.currentCommunicator.requestor = mezzRequestor;
  self.controller.appContext = appContext;
  
  [self.controller exportPortfolio];
  
  [mezzRequestor verify];
}

- (void)testShowInsertionMarkerAtIndex
{
  NSInteger index = 0;
  NSArray *collectionViewSubviews = [self.controller.collectionView subviews];
  
  [self.controller showInsertionMarkerAtIndex:index];
  UIView *insertionMarker = [_controller valueForKey:@"_insertionMarker"];
  XCTAssertNotNil(insertionMarker);
  
  id insertionMarkerView = [[self.controller.collectionView subviews] lastObject];
  XCTAssertTrue([insertionMarkerView isEqual:insertionMarker]);
  XCTAssertEqual([[self.controller.collectionView subviews] count], [collectionViewSubviews count] + 1);
}


- (void)testHideInsertionIndicator
{
  [self.controller showInsertionMarkerAtIndex:0];
  UIView *insertionMarker = [_controller valueForKey:@"_insertionMarker"];
  XCTAssertNotNil(insertionMarker);
  
  [self.controller hideInsertionIndicator];
  insertionMarker = [_controller valueForKey:@"_insertionMarker"];
  XCTAssertFalse([[self.controller.collectionView subviews] containsObject:insertionMarker]);
  XCTAssertNil(insertionMarker);
}


- (void)testInsertionIndexForLocationZero
{
  XCTAssertEqual(0, [self.controller insertionIndexForLocation:CGPointZero]);
}


- (void)testInsertionIndexForLocationMax
{
  NSInteger numberOfItems = 3;
  [self setUpPortfolioWithNumberOfItems:numberOfItems];
  
  XCTAssertEqual(numberOfItems, [self.controller insertionIndexForLocation:CGPointMake(INT16_MAX, INT16_MAX)]);
}


@end
