//
//  InfopresenceManager_Tests.m
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 15/12/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "MezzanineAppContext.h"
#import "MezzanineInfopresenceManager.h"
#import "MezzanineInfopresenceManager_Private.h"

#import "InfopresenceOverlayViewController.h"
#import "InfopresenceOverlayViewController_Private.h"

#import "Mezzanine-Swift.h"

@interface InfopresenceManager_Tests : XCTestCase

@property (nonatomic, strong) InfopresenceOverlayViewController *controller;
@property (nonatomic, strong) MezzanineInfopresenceManager *infopresenceManager;
@property (nonatomic, strong) MZSystemModel *systemModel;

@end

@implementation InfopresenceManager_Tests

- (void)setUp
{
  [super setUp];

  MezzanineAppContext *appContext = [[MezzanineAppContext alloc] init];
  appContext.applicationModel = [[MezzanineAppModel alloc] init];
  [appContext setupInfopresenceOverlay];
  [MezzanineAppContext setCurrentContext:appContext];
  _systemModel = appContext.applicationModel.systemModel;
  _systemModel.myMezzanine.version = @"3.13";
  _infopresenceManager  = appContext.infopresenceManager;
  _controller = [_infopresenceManager valueForKey:@"infopresenceOverlayViewController"];

  MZCommunicator *communicator = [MZCommunicator new];
  [appContext setCurrentCommunicator:communicator];
}

- (void)tearDown
{
  MezzanineAppContext *appContext = [MezzanineAppContext currentContext];
  [appContext setCurrentCommunicator:nil];
  appContext.applicationModel = nil;
  [_infopresenceManager reset];
  [super tearDown];
}

// 1. No outgoings neither incomings -> set currentCall to nil, dismiss/hide any possible thing
// 2. No outgoings but incomings -> present first incoming call
// 3. Outgoings but no incomings -> if it's join wait with the overlay, if not present outgoing
// 4. Outgoing and incomings -> outgoings have priority, hold the incomings until there's no outgoing

#pragma mark -
#pragma mark 1. No IncomingCalls, no OutgoingCalls tests

- (void)testNoIncomingCallsNeitherOutgoingCalls
{
  NSString *incomingCallUid0 = @"incomingCall-0";
  [_systemModel.infopresence appendObjectToIncomingCalls:[self createInfopresenceInviteCallWithUid:incomingCallUid0]];
  [_systemModel.infopresence removeObjectFromIncomingCallsAtIndex:0];
  [_systemModel removeAllRemoteMezzanines];

  // System
  XCTAssertTrue([_systemModel.remoteMezzes count] == 0);
  
  UIAlertController *alertController = [_infopresenceManager valueForKey:@"incomingRequestAlertController"];
  XCTAssertNil(alertController);
  
  XCTAssertNil([_infopresenceManager valueForKey:@"infopresenceInterruptedAlertController"]);
  XCTAssertNil([_infopresenceManager valueForKey:@"currentCallObservers"]);
  
  // Manager
  MZInfopresenceCall *currentCall = [_infopresenceManager valueForKey:@"currentCall"];
  XCTAssertNil(currentCall);
}


#pragma mark -
#pragma mark 2. IncomingCalls only tests
#pragma mark Join Request IncomingCall tests

- (void)testIncomingInfopresenceRequest
{
  NSString *incomingCallUid = @"incomingCall-0";
  
  [_systemModel.infopresence appendObjectToIncomingCalls:[self createInfopresenceJoinRequestCallWithUid:incomingCallUid]];
  
  UIAlertController *alertController = [_infopresenceManager valueForKey:@"incomingRequestAlertController"];
  XCTAssertNotNil(alertController);
  XCTAssertTrue([alertController.message containsString:[self mezzNameFromUid:incomingCallUid]]);
  
  XCTAssertNil([_infopresenceManager valueForKey:@"infopresenceInterruptedAlertController"]);
}


- (void)testIncomingInfopresenceRequestMultiple
{
  NSString *incomingCallUid0 = @"incomingCall-0";
  NSString *incomingCallUid1 = @"incomingCall-1";

  [_systemModel.infopresence appendObjectToIncomingCalls:[self createInfopresenceJoinRequestCallWithUid:incomingCallUid0]];
  [_systemModel.infopresence appendObjectToIncomingCalls:[self createInfopresenceJoinRequestCallWithUid:incomingCallUid1]];

  // System
  XCTAssertTrue([_systemModel.remoteMezzes count] == 2);
  
  UIAlertController *alertController = [_infopresenceManager valueForKey:@"incomingRequestAlertController"];
  XCTAssertNotNil(alertController);
  XCTAssertTrue([alertController.message containsString:[self mezzNameFromUid:incomingCallUid0]]);
  
  XCTAssertNil([_infopresenceManager valueForKey:@"infopresenceInterruptedAlertController"]);
}


#pragma mark Invite Outgoing Call tests

- (void)testOutgoingInviteAMezz
{
  NSString *outgoingCallUid = @"outgoingCall-0";
  [_systemModel.infopresence appendObjectToOutgoingCalls:[self createInfopresenceInviteCallWithUid:outgoingCallUid]];
  
  // Manager
  MZInfopresenceCall *currentCall = [_infopresenceManager valueForKey:@"currentCall"];
  XCTAssertNil(currentCall);
  
  // Alpha
  XCTAssertEqual(_controller.view.alpha, 0);
}


- (void)testOutgoingInviteMultipleMezzes
{
  NSString *outgoingCallUid = @"outgoingCall-0";
  [_systemModel.infopresence appendObjectToOutgoingCalls:[self createInfopresenceInviteCallWithUid:outgoingCallUid]];
  
  NSString *outgoingCallUid1 = @"outgoingCall-1";
  [_systemModel.infopresence appendObjectToOutgoingCalls:[self createInfopresenceInviteCallWithUid:outgoingCallUid1]];
  
  // System
  XCTAssertTrue([_systemModel.remoteMezzes count] == 2);
  
  // Manager
  MZInfopresenceCall *currentCall = [_infopresenceManager valueForKey:@"currentCall"];
  XCTAssertNil(currentCall);
  
  // Alpha
  XCTAssertEqual(_controller.view.alpha, 0);
}

#pragma mark Incoming Invite tests

- (void)testIncomingInfopresenceInvite
{
  NSString *incomingCallUid = @"incomingCall-0";
  
  [_systemModel.infopresence appendObjectToIncomingCalls:[self createInfopresenceInviteCallWithUid:incomingCallUid]];
  
  UIAlertController *alertController = [_infopresenceManager valueForKey:@"incomingRequestAlertController"];
  XCTAssertNotNil(alertController);
  XCTAssertTrue([alertController.message containsString:[self mezzNameFromUid:incomingCallUid]]);
  
  XCTAssertNil([_infopresenceManager valueForKey:@"infopresenceInterruptedAlertController"]);
  
  // Manager
  MZInfopresenceCall *currentCall = [_infopresenceManager valueForKey:@"currentCall"];
  XCTAssertNil(currentCall);
}


- (void)testIncomingInfopresenceMultipleInvite
{
  NSString *incomingCallUid0 = @"incomingCall-0";
  NSString *incomingCallUid1 = @"incomingCall-1";
  
  [_systemModel.infopresence appendObjectToIncomingCalls:[self createInfopresenceInviteCallWithUid:incomingCallUid0]];
  [_systemModel.infopresence appendObjectToIncomingCalls:[self createInfopresenceInviteCallWithUid:incomingCallUid1]];
  
  // System
  XCTAssertTrue([_systemModel.remoteMezzes count] == 2);
  
  UIAlertController *alertController = [_infopresenceManager valueForKey:@"incomingRequestAlertController"];
  XCTAssertNotNil(alertController);
  XCTAssertTrue([alertController.message containsString:[self mezzNameFromUid:incomingCallUid0]]);
  
  XCTAssertNil([_infopresenceManager valueForKey:@"infopresenceInterruptedAlertController"]);
  
  // Manager
  MZInfopresenceCall *currentCall = [_infopresenceManager valueForKey:@"currentCall"];
  XCTAssertNil(currentCall);
}


#pragma mark -
#pragma mark 3. OutgoingCalls only tests
#pragma mark Join Request OutgoingCall tests

- (void)testJoiningAMezz
{
  NSString *outgoingCallUid = @"outgoingCall-0";
  [_systemModel.infopresence appendObjectToOutgoingCalls:[self createInfopresenceJoinRequestCallWithUid:outgoingCallUid]];
  
  // Manager
  MZInfopresenceCall *currentCall = [_infopresenceManager valueForKey:@"currentCall"];
  XCTAssertEqual(currentCall.uid, outgoingCallUid);
  
  // Title and button
  XCTAssertTrue([_controller.label.text isEqualToString:@"Joining…"]);
  XCTAssertTrue([[_controller.button titleForState:UIControlStateNormal] isEqualToString:@"CANCEL"]);
  
  // Cell
  RemoteMezzanineTableViewCell *cell = [_controller valueForKey:@"_remoteMezzView"];
  XCTAssertTrue([cell.nameLabel.text isEqualToString:[self mezzNameFromUid:outgoingCallUid]]);
  
  // Alpha
  XCTAssertEqual(_controller.view.alpha, 1);
}


- (void)testJoiningAMezzMultiple
{
  NSString *outgoingCallUid = @"outgoingCall-0";
  [_systemModel.infopresence appendObjectToOutgoingCalls:[self createInfopresenceJoinRequestCallWithUid:outgoingCallUid]];
  
  NSString *outgoingCallUid1 = @"outgoingCall-1";
  [_systemModel.infopresence appendObjectToOutgoingCalls:[self createInfopresenceJoinRequestCallWithUid:outgoingCallUid1]];
  
  // System
  XCTAssertTrue([_systemModel.remoteMezzes count] == 2);
  
  // Manager
  MZInfopresenceCall *currentCall = [_infopresenceManager valueForKey:@"currentCall"];
  XCTAssertEqual(currentCall.uid, outgoingCallUid);
  
  // Title and button
  XCTAssertTrue([_controller.label.text isEqualToString:@"Joining…"]);
  XCTAssertTrue([[_controller.button titleForState:UIControlStateNormal] isEqualToString:@"CANCEL"]);
  
  // Cell
  RemoteMezzanineTableViewCell *cell = [_controller valueForKey:@"_remoteMezzView"];
  XCTAssertTrue([cell.nameLabel.text isEqualToString:[self mezzNameFromUid:outgoingCallUid]]);
  
  // Alpha
  XCTAssertEqual(_controller.view.alpha, 1);
}


- (void)testCancelJoiningAMezz
{
  NSString *outgoingCallUid = @"outgoingCall-0";
  
  MZInfopresenceCall *call = [self createInfopresenceJoinRequestCallWithUid:outgoingCallUid];
  
  [_systemModel.infopresence appendObjectToOutgoingCalls:call];
  
  MZRemoteMezz *mezz = [_systemModel remoteMezzWithUid:call.uid];
  
  call.resolution = MZInfopresenceCallResolutionCanceled;
  mezz.callResolution = MZInfopresenceCallResolutionCanceled;
  mezz.collaborationState = MZRemoteMezzCollaborationStateNotInCollaboration;
  
  [_systemModel.infopresence removeOutgoingCallsAtIndexes:[NSIndexSet indexSetWithIndex:0]];
  MZInfopresenceCall *currentCall = [_infopresenceManager valueForKey:@"currentCall"];
  XCTAssertNil(currentCall);
  
  // Title and button
  XCTAssertTrue([_controller.label.text isEqualToString:@"Join Request Canceled"]);
  XCTAssertTrue([[_controller.button titleForState:UIControlStateNormal] isEqualToString:@"CANCEL"]);
  
  // Cell
  RemoteMezzanineTableViewCell *cell = [_controller valueForKey:@"_remoteMezzView"];
  XCTAssertTrue([cell.nameLabel.text isEqualToString:[self mezzNameFromUid:outgoingCallUid]]);
  
  // Alpha
  XCTAssertEqual(_controller.view.alpha, 0);
}


#pragma mark -
#pragma mark 4. OutgoingCalls and OutgoingCalls tests

- (void)testOutgoingCallBeforeIncomingCall
{
  NSString *outgoingCallUid = @"outgoingCall-0";
  NSString *incomingCallUid = @"incomingCall-0";

  [_systemModel.infopresence appendObjectToOutgoingCalls:[self createInfopresenceJoinRequestCallWithUid:outgoingCallUid]];
  [_systemModel.infopresence appendObjectToIncomingCalls:[self createInfopresenceJoinRequestCallWithUid:incomingCallUid]];
  
  // System
  XCTAssertTrue([_systemModel.remoteMezzes count] == 2);
  
  // Manager
  MZInfopresenceCall *currentCall = [_infopresenceManager valueForKey:@"currentCall"];
  XCTAssertEqual(currentCall.uid, outgoingCallUid);
  
  // Title and button
  XCTAssertTrue([_controller.label.text isEqualToString:@"Joining…"]);
  XCTAssertTrue([[_controller.button titleForState:UIControlStateNormal] isEqualToString:@"CANCEL"]);
  
  // Cell
  RemoteMezzanineTableViewCell *cell = [_controller valueForKey:@"_remoteMezzView"];
  XCTAssertTrue([cell.nameLabel.text isEqualToString:[self mezzNameFromUid:outgoingCallUid]]);
  
  // Alpha
  XCTAssertEqual(_controller.view.alpha, 1);
  
  
  UIAlertController *alertController = [_infopresenceManager valueForKey:@"incomingRequestAlertController"];
  XCTAssertNil(alertController);
  
}


- (void)testOutgoingInviteCallBeforeIncomingInviteCall
{
  NSString *outgoingCallUid = @"outgoingCall-0";
  NSString *incomingCallUid = @"incomingCall-0";
  
  [_systemModel.infopresence appendObjectToOutgoingCalls:[self createInfopresenceInviteCallWithUid:outgoingCallUid]];
  [_systemModel.infopresence appendObjectToIncomingCalls:[self createInfopresenceInviteCallWithUid:incomingCallUid]];
  
  // System
  XCTAssertTrue([_systemModel.remoteMezzes count] == 2);
  
  // Manager
  MZInfopresenceCall *currentCall = [_infopresenceManager valueForKey:@"currentCall"];
  XCTAssertNil(currentCall);
  
  // Alpha
  XCTAssertEqual(_controller.view.alpha, 0);
  
  UIAlertController *alertController = [_infopresenceManager valueForKey:@"incomingRequestAlertController"];
  XCTAssertNotNil(alertController);
}


// See bug 15409.
// iOS send join request to A
// B invites iOS
// A declines iOS
// B invite should be displayed
- (void)testInviteCallOnHoldAlertAfterJoinRequestDeclined
{
  NSString *outgoingCallUid = @"outgoingCall-0";
  NSString *incomingCallUid = @"incomingCall-0";
  
  [_systemModel.infopresence appendObjectToOutgoingCalls:[self createInfopresenceJoinRequestCallWithUid:outgoingCallUid]];
  [_systemModel.infopresence appendObjectToIncomingCalls:[self createInfopresenceInviteCallWithUid:incomingCallUid]];
  
  // System
  XCTAssertTrue([_systemModel.remoteMezzes count] == 2);
  
  // Manager
  MZInfopresenceCall *currentCall = [_infopresenceManager valueForKey:@"currentCall"];
  XCTAssertEqual(currentCall.uid, outgoingCallUid);
  
  // Decline outgoing call
  MZInfopresenceCall *call = [_systemModel.infopresence outgoingCallWithUid:outgoingCallUid];
  call.resolution = MZInfopresenceCallResolutionDeclined;
  _infopresenceManager.currentCall = nil;
  
  [_systemModel.infopresence removeObjectFromOutgoingCalls:call];
  
  UIAlertController *alertController = [_infopresenceManager valueForKey:@"incomingRequestAlertController"];
  XCTAssertNotNil(alertController, @"Should show alertView for the incoming call waiting");
}

// This case should not be possible because after recieving an incoming call any user
// interaction is blocked by the alert but leaving it around until we implement a
// non-blocking system for infopresence alerts

//- (void)testIncomingCallBeforeOutgoingCall
//{
//  NSString *outgoingCallUid = @"outgoingCall-0";
//  NSString *incomingCallUid = @"incomingCall-0";
//  
//  [_systemModel.infopresence appendObjectToIncomingCalls:[self createInfopresenceJoinRequestCallWithUid:incomingCallUid]];
//  [_systemModel.infopresence appendObjectToOutgoingCalls:[self createInfopresenceJoinRequestCallWithUid:outgoingCallUid]];
//  
//  
//  // System
//  XCTAssertTrue([_systemModel.remoteMezzes count] == 2);
//  
//  // Manager
//  MZInfopresenceCall *currentCall = [_infopresenceManager valueForKey:@"currentCall"];
//  XCTAssertEqual(currentCall.uid, incomingCallUid);
//
//  // Title and button
//  XCTAssertTrue([_controller.label.text isEqualToString:@"Joining…"]);
//  XCTAssertTrue([[_controller.button titleForState:UIControlStateNormal] isEqualToString:@"CANCEL"]);
//  
//  // Cell
//  RemoteMezzanineTableViewCell *cell = [_controller valueForKey:@"_remoteMezzView"];
//  XCTAssertTrue([cell.nameLabel.text isEqualToString:[self mezzNameFromUid:incomingCallUid]]);
//  
//  // Alpha
//  XCTAssertEqual(_controller.view.alpha, 1);
//  
//  
//  UIAlertController *alertController = [_infopresenceManager valueForKey:@"incomingRequestAlertController"];
//  XCTAssertNotNil(alertView);
//  XCTAssertTrue([alertView.message containsString:[self mezzNameFromUid:incomingCallUid]]);
//  
//  XCTAssertNil([_infopresenceManager valueForKey:@"infopresenceInterruptedAlertController"]);
//}


#pragma mark - Alerts

- (void)testAlertsWithTwoIncomingJoinRequests
{
  NSString *incomingCall0Uid = @"incomingCall-0";
  NSString *incomingCall1Uid = @"incomingCall-1";

  [_systemModel.infopresence appendObjectToIncomingCalls:[self createInfopresenceJoinRequestCallWithUid:incomingCall0Uid]];

  // Manager
  MZInfopresenceCall *firstCurrentCall = [_infopresenceManager valueForKey:@"currentCall"];
  XCTAssertNil(firstCurrentCall);

  // Alpha
  XCTAssertEqual(_controller.view.alpha, 0);

  UIAlertController *alertController = [_infopresenceManager valueForKey:@"incomingRequestAlertController"];
  XCTAssertNotNil(alertController);

  [_systemModel.infopresence appendObjectToIncomingCalls:[self createInfopresenceJoinRequestCallWithUid:incomingCall1Uid]];

  // Manager
  MZInfopresenceCall *secondCurrentCall = [_infopresenceManager valueForKey:@"currentCall"];
  XCTAssertNil(secondCurrentCall);
  XCTAssertEqual(firstCurrentCall, secondCurrentCall, @"The same remote mezzanine is still being joined");

  // Alpha
  XCTAssertEqual(_controller.view.alpha, 0);

  UIAlertController *alertController2 = [_infopresenceManager valueForKey:@"incomingRequestAlertController"];
  XCTAssertNotNil(alertController2);
  XCTAssertEqual(alertController, alertController2, @"The same alert should still be shown");

  [_infopresenceManager setValue:nil forKey:@"currentCall"];
  [_systemModel.infopresence removeObjectFromIncomingCallsAtIndex:0];

  // Manager
  MZInfopresenceCall *thirdCurrentCall = [_infopresenceManager valueForKey:@"currentCall"];
  XCTAssertNil(thirdCurrentCall);
  XCTAssertEqual(secondCurrentCall, thirdCurrentCall, @"A new remote mezzanine should be being joined");

  // Alpha
  XCTAssertEqual(_controller.view.alpha, 0);

  UIAlertController *alertController3 = [_infopresenceManager valueForKey:@"incomingRequestAlertController"];
  XCTAssertNotNil(alertController3);
  XCTAssertNotEqual(alertController2, alertController3, @"A new alert should be presented");

  [_infopresenceManager setValue:nil forKey:@"currentCall"];
  [_systemModel.infopresence removeObjectFromIncomingCallsAtIndex:0];

  // Manager
  MZInfopresenceCall *fourthCurrentCall = [_infopresenceManager valueForKey:@"currentCall"];
  XCTAssertNil(fourthCurrentCall);

  // Alpha
  XCTAssertEqual(_controller.view.alpha, 0);

  UIAlertController *alertController4 = [_infopresenceManager valueForKey:@"incomingRequestAlertController"];
  XCTAssertNil(alertController4);
}


- (void)testAlertsWithAJoinRequestFollowedByAnInvite
{
  NSString *incomingJoinRequestCall0Uid = @"incomingJoinRequestCall-0";
  NSString *incomingInviteCall1Uid = @"incomingInviteCall-1";

  [_systemModel.infopresence appendObjectToIncomingCalls:[self createInfopresenceJoinRequestCallWithUid:incomingJoinRequestCall0Uid]];

  // Manager
  MZInfopresenceCall *firstCurrentCall = [_infopresenceManager valueForKey:@"currentCall"];
  XCTAssertNil(firstCurrentCall);

  // Alpha
  XCTAssertEqual(_controller.view.alpha, 0);

  UIAlertController *alertController = [_infopresenceManager valueForKey:@"incomingRequestAlertController"];
  XCTAssertNotNil(alertController);

  [_systemModel.infopresence appendObjectToIncomingCalls:[self createInfopresenceInviteCallWithUid:incomingInviteCall1Uid]];

  // Manager
  MZInfopresenceCall *secondCurrentCall = [_infopresenceManager valueForKey:@"currentCall"];
  XCTAssertNil(secondCurrentCall);
  XCTAssertEqual(firstCurrentCall, secondCurrentCall, @"The same remote mezzanine is still being joined");

  // Alpha
  XCTAssertEqual(_controller.view.alpha, 0);

  UIAlertController *alertController2 = [_infopresenceManager valueForKey:@"incomingRequestAlertController"];
  XCTAssertNotNil(alertController2);
  XCTAssertEqual(alertController, alertController2, @"The same alert should still be shown");

  [_infopresenceManager setValue:nil forKey:@"currentCall"];
  [_systemModel.infopresence removeObjectFromIncomingCallsAtIndex:0];

  // Manager
  MZInfopresenceCall *thirdCurrentCall = [_infopresenceManager valueForKey:@"currentCall"];
  XCTAssertNil(thirdCurrentCall);
  XCTAssertEqual(secondCurrentCall, thirdCurrentCall, @"A new remote mezzanine should be being joined");

  // Alpha
  XCTAssertEqual(_controller.view.alpha, 0);

  UIAlertController *alertController3 = [_infopresenceManager valueForKey:@"incomingRequestAlertController"];
  XCTAssertNotNil(alertController3);
  XCTAssertNotEqual(alertController2, alertController3, @"A new alert should be presented");

  [_systemModel.infopresence removeObjectFromIncomingCallsAtIndex:0];

  // Manager
  MZInfopresenceCall *fourthCurrentCall = [_infopresenceManager valueForKey:@"currentCall"];
  XCTAssertNil(fourthCurrentCall);

  // Alpha
  XCTAssertEqual(_controller.view.alpha, 0);

  UIAlertController *alertController4 = [_infopresenceManager valueForKey:@"incomingRequestAlertController"];
  XCTAssertNil(alertController4);
}


- (void)testAlertsWithTwoInvites
{
  NSString *incomingInviteCall0Uid = @"incomingInviteCall-0";
  NSString *incomingInviteCall1Uid = @"incomingInviteCall-1";

  [_systemModel.infopresence appendObjectToIncomingCalls:[self createInfopresenceInviteCallWithUid:incomingInviteCall0Uid]];

  // Manager
  MZInfopresenceCall *firstCurrentCall = [_infopresenceManager valueForKey:@"currentCall"];
  XCTAssertNil(firstCurrentCall);

  // Alpha
  XCTAssertEqual(_controller.view.alpha, 0);

  UIAlertController *alertController = [_infopresenceManager valueForKey:@"incomingRequestAlertController"];
  XCTAssertNotNil(alertController);

  [_systemModel.infopresence appendObjectToIncomingCalls:[self createInfopresenceInviteCallWithUid:incomingInviteCall1Uid]];

  // Manager
  MZInfopresenceCall *secondCurrentCall = [_infopresenceManager valueForKey:@"currentCall"];
  XCTAssertNil(secondCurrentCall);

  // Alpha
  XCTAssertEqual(_controller.view.alpha, 0);

  UIAlertController *alertController2 = [_infopresenceManager valueForKey:@"incomingRequestAlertController"];
  XCTAssertNotNil(alertController2);
  XCTAssertEqual(alertController, alertController2, @"The same alert should still be shown");

  [_systemModel.infopresence removeObjectFromIncomingCallsAtIndex:0];

  // Manager
  MZInfopresenceCall *thirdCurrentCall = [_infopresenceManager valueForKey:@"currentCall"];
  XCTAssertNil(thirdCurrentCall);

  // Alpha
  XCTAssertEqual(_controller.view.alpha, 0);

  UIAlertController *alertController3 = [_infopresenceManager valueForKey:@"incomingRequestAlertController"];
  XCTAssertNotNil(alertController3);
  XCTAssertNotEqual(alertController2, alertController3, @"A new alert should be presented");

  [_systemModel.infopresence removeObjectFromIncomingCallsAtIndex:0];

  // Manager
  MZInfopresenceCall *fourthCurrentCall = [_infopresenceManager valueForKey:@"currentCall"];
  XCTAssertNil(fourthCurrentCall);

  // Alpha
  XCTAssertEqual(_controller.view.alpha, 0);

  UIAlertController *alertController4 = [_infopresenceManager valueForKey:@"incomingRequestAlertController"];
  XCTAssertNil(alertController4);
}


- (void)testAlertsWithAnInviteFollowedByAJoinRequest
{
  NSString *incomingInviteCall0Uid = @"incomingInviteCall-0";
  NSString *incomingJoinRequestCall1Uid = @"incomingJoinRequestCall-1";

  [_systemModel.infopresence appendObjectToIncomingCalls:[self createInfopresenceInviteCallWithUid:incomingInviteCall0Uid]];

  // Manager
  MZInfopresenceCall *firstCurrentCall = [_infopresenceManager valueForKey:@"currentCall"];
  XCTAssertNil(firstCurrentCall);

  // Alpha
  XCTAssertEqual(_controller.view.alpha, 0);

  UIAlertController *alertController = [_infopresenceManager valueForKey:@"incomingRequestAlertController"];
  XCTAssertNotNil(alertController);

  [_systemModel.infopresence appendObjectToIncomingCalls:[self createInfopresenceJoinRequestCallWithUid:incomingJoinRequestCall1Uid]];

  // Manager
  MZInfopresenceCall *secondCurrentCall = [_infopresenceManager valueForKey:@"currentCall"];
  XCTAssertNil(secondCurrentCall);

  // Alpha
  XCTAssertEqual(_controller.view.alpha, 0);

  UIAlertController *alertController2 = [_infopresenceManager valueForKey:@"incomingRequestAlertController"];
  XCTAssertNotNil(alertController2);
  XCTAssertEqual(alertController, alertController2, @"The same alert should still be shown");

  [_systemModel.infopresence removeObjectFromIncomingCallsAtIndex:0];

  // Manager
  MZInfopresenceCall *thirdCurrentCall = [_infopresenceManager valueForKey:@"currentCall"];
  XCTAssertNil(thirdCurrentCall);

  // Alpha
  XCTAssertEqual(_controller.view.alpha, 0);

  UIAlertController *alertController3 = [_infopresenceManager valueForKey:@"incomingRequestAlertController"];
  XCTAssertNotNil(alertController3);
  XCTAssertNotEqual(alertController2, alertController3, @"A new alert should be presented");

  [_systemModel.infopresence removeObjectFromIncomingCallsAtIndex:0];

  // Manager
  MZInfopresenceCall *fourthCurrentCall = [_infopresenceManager valueForKey:@"currentCall"];
  XCTAssertNil(fourthCurrentCall);

  // Alpha
  XCTAssertEqual(_controller.view.alpha, 0);

  UIAlertController *alertController4 = [_infopresenceManager valueForKey:@"incomingRequestAlertController"];
  XCTAssertNil(alertController4);
}


#pragma mark - Interrupted alerts

- (void)testIncomingInviteAlertMessageWithSavedWorkspaceAndNotInInfopresence
{
  _systemModel.infopresence.status = MZInfopresenceStatusInactive;

  _systemModel.currentWorkspace = [MZWorkspace new];
  _systemModel.currentWorkspace.isSaved = YES;

  NSString *incomingInviteCall0Uid = @"incomingInviteCall-0";
  [_systemModel.infopresence appendObjectToIncomingCalls:[self createInfopresenceInviteCallWithUid:incomingInviteCall0Uid]];

  UIAlertController *inviteAlertController = [_infopresenceManager valueForKey:@"incomingRequestAlertController"];
  XCTAssertNotNil(inviteAlertController);
  XCTAssertTrue([inviteAlertController.title isEqualToString:NSLocalizedString(@"Infopresence Invite Dialog Title", nil)]);
  NSString *messageWithRemoteMezz = [NSString stringWithFormat:NSLocalizedString(@"Infopresence Invite Dialog Message", nil), [self mezzNameFromUid:incomingInviteCall0Uid]];
  XCTAssertTrue([inviteAlertController.message isEqualToString:messageWithRemoteMezz]);
}

- (void)testIncomingInviteAlertMessageWithSavedWorkspaceAndInfopresenceActive
{
  NSString *remoteMezzUid = @"test-alert-mezz-0";
  [self addRemoteMezz:remoteMezzUid];
  [_systemModel.infopresence appendObjectToRooms:[_systemModel remoteMezzWithUid:remoteMezzUid]];
  _systemModel.infopresence.status = MZInfopresenceStatusActive;

  _systemModel.currentWorkspace = [MZWorkspace new];
  _systemModel.currentWorkspace.isSaved = YES;

  NSString *incomingInviteCall0Uid = @"incomingInviteCall-0";
  [_systemModel.infopresence appendObjectToIncomingCalls:[self createInfopresenceInviteCallWithUid:incomingInviteCall0Uid]];

  UIAlertController *inviteAlertController = [_infopresenceManager valueForKey:@"incomingRequestAlertController"];
  XCTAssertNotNil(inviteAlertController);
  XCTAssertTrue([inviteAlertController.title isEqualToString:NSLocalizedString(@"Infopresence Invite Dialog Title", nil)]);
  NSString *messageWithRemoteMezz = [NSString stringWithFormat:NSLocalizedString(@"Infopresence Invite End Current Session Dialog Message", nil), [self mezzNameFromUid:incomingInviteCall0Uid]];
  XCTAssertTrue([inviteAlertController.message isEqualToString:messageWithRemoteMezz]);
}

- (void)testIncomingInviteAlertMessageWithUnsavedWorkspaceAndNotInInfopresence
{
  _systemModel.infopresence.status = MZInfopresenceStatusInactive;

  _systemModel.currentWorkspace = [MZWorkspace new];
  _systemModel.currentWorkspace.isSaved = NO;

  NSString *incomingInviteCall0Uid = @"incomingInviteCall-0";
  [_systemModel.infopresence appendObjectToIncomingCalls:[self createInfopresenceInviteCallWithUid:incomingInviteCall0Uid]];

  UIAlertController *inviteAlertController = [_infopresenceManager valueForKey:@"incomingRequestAlertController"];
  XCTAssertNotNil(inviteAlertController);
  XCTAssertTrue([inviteAlertController.title isEqualToString:NSLocalizedString(@"Infopresence Invite Dialog Title", nil)]);
  NSString *messageWithRemoteMezz = [NSString stringWithFormat:NSLocalizedString(@"Infopresence Invite Discard Workspace Dialog Message", nil), [self mezzNameFromUid:incomingInviteCall0Uid]];
  XCTAssertTrue([inviteAlertController.message isEqualToString:messageWithRemoteMezz]);
}

- (void)testIncomingInviteAlertMessageWithUnsavedWorkspaceAndInfopresenceActive
{
  NSString *remoteMezzUid = @"test-alert-mezz-0";
  [self addRemoteMezz:remoteMezzUid];
  [_systemModel.infopresence appendObjectToRooms:[_systemModel remoteMezzWithUid:remoteMezzUid]];
  _systemModel.infopresence.status = MZInfopresenceStatusActive;

  _systemModel.currentWorkspace = [MZWorkspace new];
  _systemModel.currentWorkspace.isSaved = NO;

  NSString *incomingInviteCall0Uid = @"incomingInviteCall-0";
  [_systemModel.infopresence appendObjectToIncomingCalls:[self createInfopresenceInviteCallWithUid:incomingInviteCall0Uid]];

  UIAlertController *inviteAlertController = [_infopresenceManager valueForKey:@"incomingRequestAlertController"];
  XCTAssertNotNil(inviteAlertController);
  XCTAssertTrue([inviteAlertController.title isEqualToString:NSLocalizedString(@"Infopresence Invite Dialog Title", nil)]);
  NSString *messageWithRemoteMezz = [NSString stringWithFormat:NSLocalizedString(@"Infopresence Invite Discard Workspace And End Current Session Dialog Message", nil), [self mezzNameFromUid:incomingInviteCall0Uid]];
  XCTAssertTrue([inviteAlertController.message isEqualToString:messageWithRemoteMezz]);
}


- (void)testInterruptedAlertAppearsAndDisappears
{
  NSString *interruptedMezzUid = @"test-interrupted-mezz-0";
  [self addRemoteMezz:interruptedMezzUid];

  [_systemModel.infopresence appendObjectToRooms:[_systemModel remoteMezzWithUid:interruptedMezzUid]];
  _systemModel.infopresence.status = MZInfopresenceStatusActive;

  MZRemoteMezz *remoteMezz = [_systemModel remoteMezzWithUid:interruptedMezzUid];
  remoteMezz.collaborationState = MZInfopresenceStatusActive;

  _systemModel.infopresence.status = MZInfopresenceStatusInterrupted;

  UIAlertController *interruptedAlertController = [_infopresenceManager valueForKey:@"infopresenceInterruptedAlertController"];
  XCTAssertNotNil(interruptedAlertController);

  _systemModel.infopresence.status = MZInfopresenceStatusActive;

  interruptedAlertController = [_infopresenceManager valueForKey:@"infopresenceInterruptedAlertController"];
  XCTAssertNil(interruptedAlertController);

  _systemModel.infopresence.status = MZInfopresenceStatusInterrupted;

  interruptedAlertController = [_infopresenceManager valueForKey:@"infopresenceInterruptedAlertController"];
  XCTAssertNotNil(interruptedAlertController);

  _systemModel.infopresence.status = MZInfopresenceStatusInactive;

  interruptedAlertController = [_infopresenceManager valueForKey:@"infopresenceInterruptedAlertController"];
  XCTAssertNil(interruptedAlertController);
}


#pragma mark - Reseting Infopresence Manager

- (void)testInviteAlertAfterResetInfopresenceManager
{
  _systemModel.infopresence.status = MZInfopresenceStatusInactive;
  NSString *incomingInviteCall0Uid = @"incomingInviteCall-0";
  [_systemModel.infopresence appendObjectToIncomingCalls:[self createInfopresenceInviteCallWithUid:incomingInviteCall0Uid]];

  UIAlertController *inviteAlertController = [_infopresenceManager valueForKey:@"incomingRequestAlertController"];
  XCTAssertNotNil(inviteAlertController, @"Infopresence alert should exist.");

  [_infopresenceManager reset];

  XCTAssertNil([_infopresenceManager valueForKey:@"incomingRequestAlertController"], @"Infopresence alert should not being referenced anymore");
}


- (void)testInfopresenceInterruptedAlertsAfterResetInfopresenceManager
{
  NSString *interruptedMezzUid = @"test-interrupted-mezz-0";
  [self addRemoteMezz:interruptedMezzUid];

  [_systemModel.infopresence appendObjectToRooms:[_systemModel remoteMezzWithUid:interruptedMezzUid]];
  _systemModel.infopresence.status = MZInfopresenceStatusActive;

  MZRemoteMezz *remoteMezz = [_systemModel remoteMezzWithUid:interruptedMezzUid];
  remoteMezz.collaborationState = MZInfopresenceStatusActive;

  _systemModel.infopresence.status = MZInfopresenceStatusInterrupted;

  UIAlertController *interruptedAlertController = [_infopresenceManager valueForKey:@"infopresenceInterruptedAlertController"];

  XCTAssertNotNil(interruptedAlertController, @"Infopresence alert should exist.");

  [_infopresenceManager reset];

  XCTAssertNil([_infopresenceManager valueForKey:@"infopresenceInterruptedAlertController"], @"Infopresence alert should not being referenced anymore");
}


- (void)testAlertsWithAnOutgoingInviteFollowedByAIncomingInvite
{
  NSString *outgoingInviteCall0Uid = @"outgoingInviteCall-0";
  NSString *incomingInviteCall0Uid = @"incomingInviteCall-0";

  [_systemModel.infopresence appendObjectToOutgoingCalls:[self createInfopresenceInviteCallWithUid:outgoingInviteCall0Uid]];

  MZInfopresenceCall *firstCurrentCall = [_infopresenceManager valueForKey:@"currentCall"];
  XCTAssertNil(firstCurrentCall);
  XCTAssertEqual(_controller.view.alpha, 0);

  [_systemModel.infopresence appendObjectToIncomingCalls:[self createInfopresenceInviteCallWithUid:incomingInviteCall0Uid]];

  MZInfopresenceCall *secondCurrentCall = [_infopresenceManager valueForKey:@"currentCall"];
  XCTAssertNil(secondCurrentCall);
  XCTAssertEqual(_controller.view.alpha, 0);

  UIAlertController *alertController = [_infopresenceManager valueForKey:@"incomingRequestAlertController"];
  XCTAssertNotNil(alertController);
}


#pragma mark - InfopresenceCall helpers

- (void)testInfopresenceOverlay
{
  MZRemoteMezz *remoteMezz = [[MZRemoteMezz alloc] init];
  remoteMezz.uid = @"remote-mezz-1234";
  remoteMezz.name = @"The Remote Mezz";
  remoteMezz.location = @"Where the tests live";

  UIWindow *overlayWindow = [_infopresenceManager valueForKey:@"overlayWindow"];

  XCTAssertNotNil(_controller, @"InfopresenceOverlay should have been initialized already");
  XCTAssertTrue(overlayWindow.hidden, @"InfopresenceOverlay should be invisible");
  XCTAssertNil(_controller.remoteMezz, @"There should not be any remote mezz assigned yet");

  [_infopresenceManager showInfopresenceOverlayJoining:remoteMezz];
  XCTAssertNotNil(_controller, @"InfopresenceOverlay should have been initialized already");
  XCTAssertFalse(overlayWindow.hidden, @"InfopresenceOverlay should be visible");
  XCTAssertNotNil(_controller.remoteMezz, @"There should be a remote mezz already assigned");

  [_infopresenceManager hideInfopresenceOverlayAnimated:NO];
  XCTAssertNotNil(_controller, @"InfopresenceOverlay should have been initialized already");
  XCTAssertTrue(overlayWindow.hidden, @"InfopresenceOverlay should be invisible");
  XCTAssertNil(_controller.remoteMezz, @"There should be a remote mezz already assigned");
}


#pragma mark - Locked session 

- (void)testIncomingInviteArrivesWhileSessionIsLocked
{
  _systemModel.infopresence.status = MZInfopresenceStatusInactive;
  _systemModel.passphraseRequested = YES;

  NSString *incomingInviteCall0Uid = @"incomingInviteCall-0";
  [_systemModel.infopresence appendObjectToIncomingCalls:[self createInfopresenceInviteCallWithUid:incomingInviteCall0Uid]];

  UIAlertController *inviteAlertController = [_infopresenceManager valueForKey:@"incomingRequestAlertController"];
  XCTAssertNil(inviteAlertController);
}


- (void)testIncomingJoinRequestArrivesWhileSessionIsLocked
{
  _systemModel.infopresence.status = MZInfopresenceStatusInactive;
  _systemModel.passphraseRequested = YES;

  NSString *incomingJoinRequestCall0Uid = @"incomingJoinRequestCall-0";
  [_systemModel.infopresence appendObjectToIncomingCalls:[self createInfopresenceJoinRequestCallWithUid:incomingJoinRequestCall0Uid]];

  UIAlertController *inviteAlertController = [_infopresenceManager valueForKey:@"incomingRequestAlertController"];
  XCTAssertNil(inviteAlertController);
}


- (void)testIncomingJoinRequestArrivesWhileSessionIsLockedAndCancelsToDismissPassphraseViewController
{
  _systemModel.infopresence.status = MZInfopresenceStatusInactive;
  _systemModel.passphraseRequested = YES;

  NSString *incomingInviteCall0Uid = @"incomingInviteCall-0";
  [_systemModel.infopresence appendObjectToIncomingCalls:[self createInfopresenceInviteCallWithUid:incomingInviteCall0Uid]];

  UIAlertController *inviteAlertController = [_infopresenceManager valueForKey:@"incomingRequestAlertController"];
  XCTAssertNil(inviteAlertController);

  _infopresenceManager.communicator.isConnected = NO;
  _systemModel.passphraseRequested = NO;

  inviteAlertController = [_infopresenceManager valueForKey:@"incomingRequestAlertController"];
  XCTAssertNil(inviteAlertController);
}


- (void)testIncomingInviteIsPresentedAfterUnlocking
{
  _systemModel.infopresence.status = MZInfopresenceStatusInactive;
  _systemModel.passphraseRequested = YES;

  NSString *incomingInviteCall0Uid = @"incomingInviteCall-0";
  [_systemModel.infopresence appendObjectToIncomingCalls:[self createInfopresenceInviteCallWithUid:incomingInviteCall0Uid]];

  UIAlertController *inviteAlertController = [_infopresenceManager valueForKey:@"incomingRequestAlertController"];
  XCTAssertNil(inviteAlertController);

  _infopresenceManager.communicator.isConnected = YES;
  _systemModel.passphraseRequested = NO;

  inviteAlertController = [_infopresenceManager valueForKey:@"incomingRequestAlertController"];
  XCTAssertNotNil(inviteAlertController);
}


- (void)testIncomingJoinRequestIsPresentedAfterUnlocking
{
  _systemModel.infopresence.status = MZInfopresenceStatusInactive;
  _systemModel.passphraseRequested = YES;

  NSString *incomingJoinRequestCall0Uid = @"incomingJoinRequestCall-0";
  [_systemModel.infopresence appendObjectToIncomingCalls:[self createInfopresenceJoinRequestCallWithUid:incomingJoinRequestCall0Uid]];

  UIAlertController *inviteAlertController = [_infopresenceManager valueForKey:@"incomingRequestAlertController"];
  XCTAssertNil(inviteAlertController);

  _infopresenceManager.communicator.isConnected = YES;
  _systemModel.passphraseRequested = NO;

  inviteAlertController = [_infopresenceManager valueForKey:@"incomingRequestAlertController"];
  XCTAssertNotNil(inviteAlertController);
}


- (void)testOutgoingJoiningOverlayIsVisibleAfterUnlocking
{
  _systemModel.infopresence.status = MZInfopresenceStatusInactive;
  _systemModel.passphraseRequested = YES;

  NSString *outgoingCallUid = @"outgoingCall-0";
  [_systemModel.infopresence appendObjectToOutgoingCalls:[self createInfopresenceJoinRequestCallWithUid:outgoingCallUid]];

  UIWindow *overlayWindow = [_infopresenceManager valueForKey:@"overlayWindow"];

  XCTAssertNotNil(_controller, @"InfopresenceOverlay should have been initialized already");
  XCTAssertTrue(overlayWindow.hidden, @"InfopresenceOverlay should be invisible");

  _infopresenceManager.communicator.isConnected = YES;
  _systemModel.passphraseRequested = NO;

  XCTAssertFalse(overlayWindow.hidden, @"InfopresenceOverlay should be visible now");
}


// Bug 16225/16226
- (void)testMultipleIncomingInvitesOneIsAcceptedNoMoreAlertsShouldBeShown
{
  NSString *incomingCallUid0 = @"incomingCall-0";
  NSString *incomingCallUid1 = @"incomingCall-1";

  MZInfopresenceCall *incomingInvite0 = [self createInfopresenceInviteCallWithUid:incomingCallUid0];
  MZInfopresenceCall *incomingInvite1 = [self createInfopresenceInviteCallWithUid:incomingCallUid1];

  [_systemModel.infopresence appendObjectToIncomingCalls:incomingInvite0];
  [_systemModel.infopresence appendObjectToIncomingCalls:incomingInvite1];

  // System
  XCTAssertTrue([_systemModel.remoteMezzes count] == 2);

  UIAlertController *alertController = [_infopresenceManager valueForKey:@"incomingRequestAlertController"];
  XCTAssertNotNil(alertController);
  XCTAssertTrue([alertController.message containsString:[self mezzNameFromUid:incomingCallUid0]]);

  incomingInvite0.resolution = MZInfopresenceCallResolutionAccepted;

  [_systemModel.infopresence removeObjectFromIncomingCallsAtIndex:0];

  MZInfopresenceCall *outgoingCall0 = [self createInfopresenceJoinRequestCallWithUid:incomingCallUid0];
  [_systemModel.infopresence appendObjectToOutgoingCalls:outgoingCall0];

  incomingInvite1.resolution = MZInfopresenceCallResolutionDeclined;
  [_systemModel.infopresence removeObjectFromIncomingCallsAtIndex:0];

  outgoingCall0.resolution = MZInfopresenceCallResolutionAccepted;
  [_systemModel.infopresence removeObjectFromOutgoingCallsAtIndex:0];

  alertController = [_infopresenceManager valueForKey:@"incomingRequestAlertController"];
  XCTAssertNil(alertController);
}


#pragma mark - InfopresenceCall helpers

- (NSString *)mezzNameFromUid:(NSString *)uid
{
  return [NSString stringWithFormat:@"remoteMezz-%@", uid];

}


- (void)addRemoteMezz:(NSString *)uid
{
  MZRemoteMezz *remoteMezz = [MZRemoteMezz new];
  remoteMezz.uid = uid;
  remoteMezz.name = [self mezzNameFromUid:uid];
  [_systemModel.remoteMezzes addObject:remoteMezz];
}


- (MZInfopresenceCall *)createInfopresenceJoinRequestCallWithUid:(NSString *)uid
{
  MZInfopresenceCall *infopresenceCall = [MZInfopresenceCall new];
  infopresenceCall.uid = uid;
  infopresenceCall.type = MZInfopresenceCallTypeJoinRequest;
  
  [self addRemoteMezz:uid];

  return infopresenceCall;
}


- (MZInfopresenceCall *)createInfopresenceInviteCallWithUid:(NSString *)uid
{
  MZInfopresenceCall *infopresenceCall = [MZInfopresenceCall new];
  infopresenceCall.uid = uid;
  infopresenceCall.type = MZInfopresenceCallTypeInvite;

  [self addRemoteMezz:uid];

  return infopresenceCall;
}


- (void)removeInfopresenceCall:(MZInfopresenceCall *)infopresenceCall
{
  [_systemModel.infopresence removeOutgoingCallsAtIndexes:[NSIndexSet indexSetWithIndex:0]];
}

@end
