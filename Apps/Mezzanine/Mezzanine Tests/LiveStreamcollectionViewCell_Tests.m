//
//  LiveStreamcollectionViewCell_Tests.m
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 13/10/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import "UIImage+Additions.h"

#import "LiveStreamCollectionViewCell.h"
#import "LiveStreamCollectionViewCell_Private.h"

#import "MezzanineAppContext.h"
#import "MZDownloadManager.h"

@interface LiveStreamcollectionViewCell_Tests : XCTestCase
{
  CGRect _cellRect;
}

@property (nonatomic, strong) LiveStreamCollectionViewCell *cell;
@property (nonatomic, strong) MZLiveStream *initialLiveStream;
@property (nonatomic, strong) VideoPlaceholderView *videoPlaceholderView;

@end

static float CellSide = 200;


@implementation LiveStreamcollectionViewCell_Tests

- (void)setUp {
  [super setUp];
  MZCommunicator *communicator = [MZCommunicator new];
  communicator.communicatorProtocol = OBCommunicatorProtocolTCPS;
  communicator.serverUrl = [[NSURL alloc] initWithString:@"https://test"];
  [MezzanineAppContext currentContext].currentCommunicator = communicator;

  
  _cellRect = CGRectMake(0, 0, CellSide, CellSide);
  
  self.initialLiveStream = [MZLiveStream new];
  self.initialLiveStream.active = YES;
  self.initialLiveStream.thumbURL = @"https://mezzdelpi/r/global-assets/as-f233458f-87fd-4b20-a62b-57478f445cda/img-local-thumb.png";
  self.initialLiveStream.uid = @"vi-bmagic/_local_";
  self.initialLiveStream.displayName = @"live-stream-displayName";
  self.initialLiveStream.remoteMezzName = @"live-stream-remoteMezzName";
  
  self.cell = [[LiveStreamCollectionViewCell alloc] initWithFrame:_cellRect];
  [self.cell setLiveStream:self.initialLiveStream];
  self.cell.imageView.image = [UIImage imageOfSize:CGSizeMake(CellSide, CellSide) filledWithColor:[UIColor blackColor]];

  self.videoPlaceholderView = [self.cell valueForKey:@"_videoPlaceholderView"];
}


- (void)tearDown {
  [super tearDown];
  
  [MezzanineAppContext currentContext].currentCommunicator = nil;
  
  self.initialLiveStream.thumbURL = nil;
  self.initialLiveStream = nil;
  
  self.cell.imageView = nil;
  self.cell.liveStream = nil;
  self.cell = nil;
}


- (void)testPrepareForReuse
{
  [self.cell prepareForReuse];
  
  XCTAssertNil(self.cell.imageView.image);
  XCTAssertNil(self.cell.liveStream);
}


- (void)testSetLiveStream
{
  id observation = [self.initialLiveStream observationInfo];
  XCTAssertEqual([[observation valueForKey:@"_observances"] count], 3);

  for (id observer in [observation valueForKey:@"_observances"]) {
    XCTAssertEqual([observer valueForKey:@"_observer"], self.cell);
  }

  MZLiveStream *liveStreamNew = [MZLiveStream new];
  [self.cell setLiveStream:liveStreamNew];
  
  observation = [self.initialLiveStream observationInfo];
  XCTAssertEqual([[observation valueForKey:@"_observances"] count], 0);
}


- (void)testNotificationDisplayName
{
  XCTAssertEqual(self.videoPlaceholderView.sourceName, self.initialLiveStream.displayName);
  XCTAssertEqual(self.videoPlaceholderView.sourceOrigin, self.initialLiveStream.remoteMezzName);

  NSString *newDisplayName = @"newDisplayName";
  self.initialLiveStream.displayName = newDisplayName;
  
  XCTAssertEqual(self.videoPlaceholderView.sourceName, newDisplayName);
  XCTAssertEqual(self.videoPlaceholderView.sourceOrigin, self.initialLiveStream.remoteMezzName);
}


- (void)testNotificationActiveInvalidLiveStream
{
  XCTAssertNotNil(self.cell.imageView.image);
  
  self.initialLiveStream.active = NO;
  
  XCTAssertNil(self.cell.imageView.image);
  XCTAssertFalse(self.videoPlaceholderView.hidden);
}


- (void)testNotificationUpdateThumbURLRemote
{
  XCTAssertNotNil(self.cell.imageView.image);
  
  self.initialLiveStream.thumbURL = nil;
  
  XCTAssertNil(self.cell.imageView.image);
  XCTAssertFalse(self.videoPlaceholderView.hidden);
  
  // We treat MZDownloadManager as a black box.
  // Mock for MZDownloadManager singleton to return a fake image (we assume url will contain an image)
  id mzDownloadManager = [OCMockObject partialMockForObject:[MZDownloadManager sharedLoader]];
  
  // Allows to return a given block instead of the original block implementation, so we return the image
  [[[mzDownloadManager stub] andDo:^(NSInvocation *invocation) {
    self.cell.imageView.image = [UIImage imageOfSize:CGSizeMake(CellSide, CellSide) filledWithColor:[UIColor blackColor]];
    self.videoPlaceholderView.hidden = YES;

  }] loadImageWithURL:OCMOCK_ANY atTarget:OCMOCK_ANY success:OCMOCK_ANY error:OCMOCK_ANY canceled:OCMOCK_ANY cachingOption:1];
  
  // We only care about setting a well formatted URL rather than a valid asset URL
  self.initialLiveStream.thumbURL = @"https://mezzdelpi/r/global-assets/as-f233458f-87fd-4b20-a62b-57478f445cda/img-local-thumb.png";
  
  XCTAssertNotNil(self.cell.imageView.image);
  XCTAssertTrue(self.videoPlaceholderView.hidden);
}


- (void)testNotificationUpdateThumbURLLocal
{
  NSBundle *bundle = [NSBundle bundleForClass:[self class]];
  NSString *path = [bundle pathForResource:@"Test-Image-Mezzanine" ofType:@"png"];
  NSURL *url = [NSURL fileURLWithPath:path];
  
  self.initialLiveStream.thumbURL = [url absoluteString];

  NSPredicate *videoPlaceholderViewHidden = [NSPredicate predicateWithFormat:@"videoPlaceholderView.hidden == YES"];
  [self expectationForPredicate:videoPlaceholderViewHidden evaluatedWithObject:self handler:nil];
  [self waitForExpectationsWithTimeout:2.0 handler:nil];

  XCTAssertNotNil(self.cell.imageView.image);
  XCTAssertTrue(self.videoPlaceholderView.hidden);
}

@end
