//
//  XCTestCase+Animations.h
//  Mezzanine
//
//  Created by Miguel Sánchez Valdés on 08/10/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface XCTestCase (Animations)

-(void) waitForAnimations:(CGFloat)time;

@end
