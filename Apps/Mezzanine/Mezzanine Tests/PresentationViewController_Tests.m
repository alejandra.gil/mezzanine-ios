//
//  PresentationViewController_Tests.m
//  Mezzanine
//
//  Created by Miguel Sánchez Valdés on 28/10/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import "PresentationViewController.h"
#import "PresentationViewController_Private.h"
#import "MezzanineMainViewController.h"
#import "MockSystemModel.h"
#import "XCTestCase+Animations.h"
#import "UIDevice+Machine.h"
#import "MezzanineStyleSheet.h"


@interface PresentationViewController_Tests : XCTestCase
{
  PresentationViewController *_controller;
  MockSystemModel *_systemModelFactory;
  id _mockPresentationDelegate;
}

@end

@implementation PresentationViewController_Tests

- (void)setUp
{
  [super setUp];
  
  _systemModelFactory = [MockSystemModel new];
  _controller = [PresentationViewController new];
  _controller.systemModel = _systemModelFactory.systemModel;
  
  _mockPresentationDelegate = [OCMockObject niceMockForProtocol:@protocol(PresentationViewControllerDelegate)];
  _controller.delegate = _mockPresentationDelegate;
  
  MZCommunicator *communicator = [MZCommunicator new];
  communicator.systemModel = [MZSystemModel new];
  communicator.systemModel.myMezzanine.version = @"3.13";
  [MezzanineAppContext currentContext].currentCommunicator = communicator;

}

- (void)tearDown
{
  [MezzanineAppContext currentContext].currentCommunicator = nil;
  _controller.systemModel = nil;
  [super tearDown];
}


#pragma mark - Background

// TODO: review after Teamwork UI development is finished
//- (void)testBackgroundColor
//{
//  [self preparePresentationInASingleFeld];
//  [_controller refreshFeldBoundViews];
//  
//  UIView *feldBoundsContainer = [[_controller.view subviews] firstObject];
//  UIView *view = [[feldBoundsContainer subviews] firstObject];
//  XCTAssertTrue([view.backgroundColor isEqual:[MezzanineStyleSheet sharedStyleSheet].grey60Color]);
//}


#pragma mark - Helpers methods to setup context

-(void) preparePresentationInATriptych
{
  _controller.systemModel = [_systemModelFactory createTestModelWithTriptych];
  [_systemModelFactory createTestModelWithSlides:10];
  _controller.presentation = _controller.systemModel.currentWorkspace.presentation;
  _controller.view.frame = CGRectMake(0, 100, 1020, 192);
}

-(void) prepareEmptyPresentationInATriptych
{
  _controller.systemModel = [_systemModelFactory createTestModelWithTriptych];
  [_systemModelFactory createTestModelWithSlides:0];
  _controller.presentation = _controller.systemModel.currentWorkspace.presentation;
  _controller.view.frame = CGRectMake(0, 100, 1020, 192);
}

-(void) preparePresentationInASingleFeld
{
  _controller.systemModel = [_systemModelFactory createTestModelWithSingleFeld];
  [_systemModelFactory createTestModelWithSlides:10];
  _controller.presentation = _controller.systemModel.currentWorkspace.presentation;
  _controller.view.frame = CGRectMake(220, 50, 800, 450);
}

-(void) prepareEmptyPresentationInASingleFeld
{
  _controller.systemModel = [_systemModelFactory createTestModelWithSingleFeld];
  [_systemModelFactory createTestModelWithSlides:0];
  _controller.presentation = _controller.systemModel.currentWorkspace.presentation;
  _controller.view.frame = CGRectMake(220, 50, 800, 450);
}


#pragma mark - Slide Actions Tests

- (void)testNextSlideFromValidPosition
{
  [self preparePresentationInATriptych];
  [_controller setValue:@5 forKey:@"currentSlideIndex"];

  id mezzRequestor = [OCMockObject niceMockForClass:[MZCommunicatorRequestor class]];
  [MezzanineAppContext currentContext].currentCommunicator.requestor = mezzRequestor;
  _controller.communicator = [MezzanineAppContext currentContext].currentCommunicator;
  [[mezzRequestor expect] requestPresentationScrollRequest:OCMOCK_ANY currentIndex:6];

  
  [_controller nextSlide];

  XCTAssertEqual(_controller.currentSlideIndex, 6, @"Current slide is %ld but should be %d", (long)_controller.currentSlideIndex, 6);

  [mezzRequestor verify];
}

- (void)testNextSlideFromLastSlide
{
  [self preparePresentationInATriptych];
  [_controller setValue:@9 forKey:@"currentSlideIndex"];

  id mezzRequestor = [OCMockObject mockForClass:[MZCommunicatorRequestor class]];
  [[mezzRequestor reject] requestPresentationScrollRequest:nil currentIndex:8];
  [[mezzRequestor reject] requestPresentationScrollRequest:nil currentIndex:9];
  [[mezzRequestor reject] requestPresentationScrollRequest:nil currentIndex:10];
  _controller.communicator.requestor = mezzRequestor;

  [_controller nextSlide];

  XCTAssertEqual(_controller.currentSlideIndex, 9, @"Current slide is %ld but should be %d", (long)_controller.currentSlideIndex, 9);

  [mezzRequestor verify];
}

- (void)testNextSlideWithNoSlides
{
  [self prepareEmptyPresentationInATriptych];

  id mezzRequestor = [OCMockObject mockForClass:[MZCommunicatorRequestor class]];
  [[mezzRequestor reject] requestPresentationScrollRequest:nil currentIndex:0];
  [[mezzRequestor reject] requestPresentationScrollRequest:nil currentIndex:1];
  _controller.communicator.requestor = mezzRequestor;

  [_controller nextSlide];

  XCTAssertEqual(_controller.currentSlideIndex, 0, @"Current slide is %ld but should be %d", (long)_controller.currentSlideIndex, 0);

  [mezzRequestor verify];
}

- (void)testPreviousSlideFromValidPosition
{
  [self preparePresentationInATriptych];
  [_controller setValue:@5 forKey:@"currentSlideIndex"];
  
  id mezzRequestor = [OCMockObject niceMockForClass:[MZCommunicatorRequestor class]];
  [MezzanineAppContext currentContext].currentCommunicator.requestor = mezzRequestor;
  _controller.communicator = [MezzanineAppContext currentContext].currentCommunicator;
  [[mezzRequestor expect] requestPresentationScrollRequest:OCMOCK_ANY currentIndex:4];
  

  [_controller previousSlide];

  XCTAssertEqual(_controller.currentSlideIndex, 4, @"Current slide is %ld but should be %d", (long)_controller.currentSlideIndex, 4);

  [mezzRequestor verify];
}

- (void)testPreviousSlideFromFirstSlide
{
  [self preparePresentationInATriptych];
  [_controller setValue:@0 forKey:@"currentSlideIndex"];

  id mezzRequestor = [OCMockObject mockForClass:[MZCommunicatorRequestor class]];
  [[mezzRequestor reject] requestPresentationScrollRequest:nil currentIndex:-1];
  [[mezzRequestor reject] requestPresentationScrollRequest:nil currentIndex:0];
  [[mezzRequestor reject] requestPresentationScrollRequest:nil currentIndex:1];
  _controller.communicator.requestor = mezzRequestor;

  [_controller previousSlide];

  XCTAssertEqual(_controller.currentSlideIndex, 0, @"Current slide is %ld but should be %d", (long)_controller.currentSlideIndex, 0);

  [mezzRequestor verify];
}

- (void)testPreviousSlideWithNoSlides
{
  [self prepareEmptyPresentationInATriptych];

  id mezzRequestor = [OCMockObject mockForClass:[MZCommunicatorRequestor class]];
  [[mezzRequestor reject] requestPresentationScrollRequest:nil currentIndex:-1];
  [[mezzRequestor reject] requestPresentationScrollRequest:nil currentIndex:0];;
  _controller.communicator.requestor = mezzRequestor;

  [_controller previousSlide];

  XCTAssertEqual(_controller.currentSlideIndex, 0, @"Current slide is %ld but should be %d", (long)_controller.currentSlideIndex, 0);

  [mezzRequestor verify];
}

- (void)testGoToSlideWithValidPosition
{
  [self preparePresentationInATriptych];
  [_controller goToSlideAtIndex:5];
  XCTAssertEqual(_controller.currentSlideIndex, 5, @"Current slide is %ld but should be %d", (long)_controller.currentSlideIndex, 5);
}

- (void)testGoToSlideWithAboveUpperBounds
{
  [self preparePresentationInATriptych];
  [_controller setValue:@1 forKey:@"currentSlideIndex"];
  [_controller goToSlideAtIndex:10];
  XCTAssertEqual(_controller.currentSlideIndex, 1, @"Current slide is %ld but should be %d", (long)_controller.currentSlideIndex, 1);
}

- (void)testGoToSlideUnderLowerBounds
{
  [self preparePresentationInATriptych];
  [_controller setValue:@1 forKey:@"currentSlideIndex"];
  [_controller goToSlideAtIndex:-1];
  XCTAssertEqual(_controller.currentSlideIndex, 1, @"Current slide is %ld but should be %d", (long)_controller.currentSlideIndex, 1);
}

- (void)testGoToSlideWithNoSlides
{
  [self prepareEmptyPresentationInATriptych];
  [_controller goToSlideAtIndex:5];
  XCTAssertEqual(_controller.currentSlideIndex, 0, @"Current slide is %ld but should be %d", (long)_controller.currentSlideIndex, 0);
}


#pragma mark - Slide Image Loading

- (void)testLoadImageForSlideLayer
{
  [self preparePresentationInATriptych];

  NSBundle *bundle = [NSBundle bundleForClass:[self class]];
  NSString *path = [bundle pathForResource:@"Test-Image-Mezzanine" ofType:@"png"];
  NSURL *url = [NSURL fileURLWithPath:path];

  id mezzComunicator = [OCMockObject mockForClass:[MZCommunicator class]];
  [[[mezzComunicator stub] andReturn:url] urlForResource:OCMOCK_ANY];
  _controller.communicator = mezzComunicator;

  MZPortfolioItem *slide = _controller.presentation.slides.firstObject;
  slide.imageAvailable = YES;
  slide.thumbURL = url.absoluteString;

  SlideLayer *slideLayer = [SlideLayer layer];

  XCTAssertNil(slideLayer.slideImage, @"The image should not be set yet");
  XCTAssertTrue(slideLayer.slideImageNeedsRefresh, @"The slideLayer should need to be refreshed");

  [_controller loadImageForSlide:slide inLayer:slideLayer];

  NSPredicate *slideImageNotNil = [NSPredicate predicateWithFormat:@"slideImage != nil"];
  [self expectationForPredicate:slideImageNotNil evaluatedWithObject:slideLayer handler:nil];
  [self waitForExpectationsWithTimeout:2.0 handler:nil];

  XCTAssertNotNil(slideLayer.slideImage, @"The image should have been set by now");
  XCTAssertFalse(slideLayer.slideImageNeedsRefresh, @"The slideLayer should not need to be refreshed");
}

- (void)testLoadFullImageForSlideLayer
{
  [self prepareEmptyPresentationInATriptych];

  NSBundle *bundle = [NSBundle bundleForClass:[self class]];
  NSString *path = [bundle pathForResource:@"Test-Image-Mezzanine" ofType:@"png"];
  NSURL *url = [NSURL fileURLWithPath:path];

  id mezzComunicator = [OCMockObject mockForClass:[MZCommunicator class]];
  [[[mezzComunicator stub] andReturn:url] urlForResource:OCMOCK_ANY];
  _controller.communicator = mezzComunicator;

  MZPortfolioItem *slide = [MZPortfolioItem new];
  slide.fullURL = url.absoluteString;
  slide.imageAvailable = YES;

  SlideLayer *slideLayer = [SlideLayer layer];
  slideLayer.slide = slide;

  XCTAssertFalse(slideLayer.isDisplayingFullImage, @"The full image should not be displayed yet");
  XCTAssertFalse(slideLayer.isLoadingFullImage, @"The slideLayer should had started downloading yet");

  [_controller loadFullImageForSlideLayer:slideLayer];

  NSPredicate *slideFullImageContentsNotNil = [NSPredicate predicateWithFormat:@"contentLayer.contents != nil"];
  [self expectationForPredicate:slideFullImageContentsNotNil evaluatedWithObject:slideLayer handler:nil];
  [self waitForExpectationsWithTimeout:10.0 handler:nil];

  XCTAssertTrue(slideLayer.isDisplayingFullImage, @"The full image should have been displayed");
  XCTAssertFalse(slideLayer.isLoadingFullImage, @"The slideLayer should had finished downloading it.");
}

- (void)testUnloadFullImageForSlideLayer
{
  NSBundle *bundle = [NSBundle bundleForClass:[self class]];
  NSString *path = [bundle pathForResource:@"Test-Image-Mezzanine" ofType:@"png"];

  MZPortfolioItem *slide = [MZPortfolioItem new];
  SlideLayer *slideLayer = [SlideLayer layer];
  slideLayer.slide = slide;

  NSURL *url = [NSURL fileURLWithPath:path];
  slide.fullURL = url.absoluteString;

  id mezzComunicator = [OCMockObject mockForClass:[MZCommunicator class]];
  [[[mezzComunicator stub] andReturn:url] urlForResource:OCMOCK_ANY];
  _controller.communicator = mezzComunicator;

  [slideLayer setFullSlideImage:[UIImage imageWithContentsOfFile:path]];

  XCTAssertTrue(slideLayer.isDisplayingFullImage, @"The full image should have been displayed");
  XCTAssertFalse(slideLayer.isLoadingFullImage, @"The slideLayer should had finished downloading it.");

  [_controller unloadFullImageForSlideLayer:slideLayer];

  XCTAssertFalse(slideLayer.isDisplayingFullImage, @"The full image should have been displayed");
  XCTAssertFalse(slideLayer.isLoadingFullImage, @"The slideLayer should had finished downloading it.");
}

- (void)testShouldDisplayFullImageForLayerOfVideoSlide
{
  [self preparePresentationInATriptych];

  MZPortfolioItem *slide = _controller.presentation.slides.firstObject;
  slide.contentSource = @"vi-1234";

  SlideLayer *slideLayer = [SlideLayer layer];
  slideLayer.slide = slide;

  XCTAssertFalse([_controller shouldDisplayFullImageForLayer:slideLayer], @"Video slide layer should not be taken into account when querying about full or thumb image");
}

- (void)testShouldDisplayFullImageForLayerOfImageSlideInATriptychAndZoomedOut
{
  [self preparePresentationInATriptych];

  CGFloat scale = 1.0;
  [[[_mockPresentationDelegate stub] andReturnValue:OCMOCK_VALUE(scale)] presentationViewControllerZoomState];

  [_controller setValue:@2 forKey:@"currentSlideIndex"];

  MZPortfolioItem *slide;
  SlideLayer *slideLayer = [SlideLayer layer];

  if ([[UIDevice currentDevice] isIPad])
  {
    slide = _controller.presentation.slides[0];
    slideLayer.slide = slide;
    XCTAssertFalse([_controller shouldDisplayFullImageForLayer:slideLayer], @"Non visible slide on the left side should be full slide");

    slide = _controller.presentation.slides[1];
    slideLayer.slide = slide;
    XCTAssertTrue([_controller shouldDisplayFullImageForLayer:slideLayer], @"Visible slide should be full slide");

    slide = _controller.presentation.slides[2];
    slideLayer.slide = slide;
    XCTAssertTrue([_controller shouldDisplayFullImageForLayer:slideLayer], @"Visible slide should be full slide");

    slide = _controller.presentation.slides[3];
    slideLayer.slide = slide;
    XCTAssertTrue([_controller shouldDisplayFullImageForLayer:slideLayer], @"Visible slide should be full slide");

    slide = _controller.presentation.slides[4];
    slideLayer.slide = slide;
    XCTAssertFalse([_controller shouldDisplayFullImageForLayer:slideLayer], @"Non visible slide on the right side should be full slide");
  }
  else
  {
    slide = _controller.presentation.slides[0];
    slideLayer.slide = slide;
    XCTAssertFalse([_controller shouldDisplayFullImageForLayer:slideLayer], @"Non visible slide on the left side should be full slide");

    slide = _controller.presentation.slides[1];
    slideLayer.slide = slide;
    XCTAssertFalse([_controller shouldDisplayFullImageForLayer:slideLayer], @"Visible slide when zoomed out on iPhone should not be full slide");

    slide = _controller.presentation.slides[2];
    slideLayer.slide = slide;
    XCTAssertFalse([_controller shouldDisplayFullImageForLayer:slideLayer], @"Visible slide when zoomed out on iPhone should not be full slide");

    slide = _controller.presentation.slides[3];
    slideLayer.slide = slide;
    XCTAssertFalse([_controller shouldDisplayFullImageForLayer:slideLayer], @"Visible slide when zoomed out on iPhone should not be full slide");

    slide = _controller.presentation.slides[4];
    slideLayer.slide = slide;
    XCTAssertFalse([_controller shouldDisplayFullImageForLayer:slideLayer], @"Non visible slide on the right side should be full slide");
  }
}

- (void)testShouldDisplayFullImageForLayerOfImageSlideInATriptychAndZoomedIn
{
  [self preparePresentationInATriptych];

  CGFloat scale = 0.0;
  [[[_mockPresentationDelegate stub] andReturnValue:OCMOCK_VALUE(scale)] presentationViewControllerZoomState];

  [_controller setValue:@2 forKey:@"currentSlideIndex"];

  MZPortfolioItem *slide;
  SlideLayer *slideLayer = [SlideLayer layer];

  if ([[UIDevice currentDevice] isIPad])
  {
    slide = _controller.presentation.slides[0];
    slideLayer.slide = slide;
    XCTAssertFalse([_controller shouldDisplayFullImageForLayer:slideLayer], @"Non visible slide on the left side should be full slide");

    slide = _controller.presentation.slides[1];
    slideLayer.slide = slide;
    XCTAssertFalse([_controller shouldDisplayFullImageForLayer:slideLayer], @"Visible slide should be full slide");

    slide = _controller.presentation.slides[2];
    slideLayer.slide = slide;
    XCTAssertTrue([_controller shouldDisplayFullImageForLayer:slideLayer], @"Visible slide should be full slide");

    slide = _controller.presentation.slides[3];
    slideLayer.slide = slide;
    XCTAssertFalse([_controller shouldDisplayFullImageForLayer:slideLayer], @"Visible slide should be full slide");

    slide = _controller.presentation.slides[4];
    slideLayer.slide = slide;
    XCTAssertFalse([_controller shouldDisplayFullImageForLayer:slideLayer], @"Non visible slide on the right side should be full slide");
  }
  else
  {
    slide = _controller.presentation.slides[0];
    slideLayer.slide = slide;
    XCTAssertFalse([_controller shouldDisplayFullImageForLayer:slideLayer], @"Non visible slide on the left side should be full slide");

    slide = _controller.presentation.slides[1];
    slideLayer.slide = slide;
    XCTAssertFalse([_controller shouldDisplayFullImageForLayer:slideLayer], @"Visible slide when zoomed in on iPhone should not be full slide");

    slide = _controller.presentation.slides[2];
    slideLayer.slide = slide;
    XCTAssertTrue([_controller shouldDisplayFullImageForLayer:slideLayer], @"Visible slide when zoomed in on iPhone should not be full slide");

    slide = _controller.presentation.slides[3];
    slideLayer.slide = slide;
    XCTAssertFalse([_controller shouldDisplayFullImageForLayer:slideLayer], @"Visible slide when zoomed in on iPhone should not be full slide");

    slide = _controller.presentation.slides[4];
    slideLayer.slide = slide;
    XCTAssertFalse([_controller shouldDisplayFullImageForLayer:slideLayer], @"Non visible slide on the right side should be full slide");
  }
}

- (void)testShouldDisplayFullImageForLayerOfImageSlideInASingleFeldAndZoomedOut
{
  [self preparePresentationInASingleFeld];

  CGFloat scale = 1.0;
  [[[_mockPresentationDelegate stub] andReturnValue:OCMOCK_VALUE(scale)] presentationViewControllerZoomState];

  [_controller setValue:@2 forKey:@"currentSlideIndex"];

  MZPortfolioItem *slide;
  SlideLayer *slideLayer = [SlideLayer layer];

  if ([[UIDevice currentDevice] isIPad])
  {
    slide = _controller.presentation.slides[0];
    slideLayer.slide = slide;
    XCTAssertFalse([_controller shouldDisplayFullImageForLayer:slideLayer], @"Non visible slide on the left side should be full slide");

    slide = _controller.presentation.slides[1];
    slideLayer.slide = slide;
    XCTAssertFalse([_controller shouldDisplayFullImageForLayer:slideLayer], @"Visible slide should be full slide");

    slide = _controller.presentation.slides[2];
    slideLayer.slide = slide;
    XCTAssertTrue([_controller shouldDisplayFullImageForLayer:slideLayer], @"Visible slide should be full slide");

    slide = _controller.presentation.slides[3];
    slideLayer.slide = slide;
    XCTAssertFalse([_controller shouldDisplayFullImageForLayer:slideLayer], @"Visible slide should be full slide");

    slide = _controller.presentation.slides[4];
    slideLayer.slide = slide;
    XCTAssertFalse([_controller shouldDisplayFullImageForLayer:slideLayer], @"Non visible slide on the right side should be full slide");
  }
  else
  {
    slide = _controller.presentation.slides[0];
    slideLayer.slide = slide;
    XCTAssertFalse([_controller shouldDisplayFullImageForLayer:slideLayer], @"Non visible slide on the left side should be full slide");

    slide = _controller.presentation.slides[1];
    slideLayer.slide = slide;
    XCTAssertFalse([_controller shouldDisplayFullImageForLayer:slideLayer], @"Visible slide when zoomed out on iPhone should not be full slide");

    slide = _controller.presentation.slides[2];
    slideLayer.slide = slide;
    XCTAssertFalse([_controller shouldDisplayFullImageForLayer:slideLayer], @"Visible slide when zoomed out on iPhone should not be full slide");

    slide = _controller.presentation.slides[3];
    slideLayer.slide = slide;
    XCTAssertFalse([_controller shouldDisplayFullImageForLayer:slideLayer], @"Visible slide when zoomed out on iPhone should not be full slide");

    slide = _controller.presentation.slides[4];
    slideLayer.slide = slide;
    XCTAssertFalse([_controller shouldDisplayFullImageForLayer:slideLayer], @"Non visible slide on the right side should be full slide");
  }
}

- (void)testShouldDisplayFullImageForLayerOfImageSlideInASingleFeldAndZoomedIn
{
  [self preparePresentationInASingleFeld];

  CGFloat scale = 0.0;
  [[[_mockPresentationDelegate stub] andReturnValue:OCMOCK_VALUE(scale)] presentationViewControllerZoomState];

  [_controller setValue:@2 forKey:@"currentSlideIndex"];

  MZPortfolioItem *slide;
  SlideLayer *slideLayer = [SlideLayer layer];

  if ([[UIDevice currentDevice] isIPad])
  {
    slide = _controller.presentation.slides[0];
    slideLayer.slide = slide;
    XCTAssertFalse([_controller shouldDisplayFullImageForLayer:slideLayer], @"Non visible slide on the left side should be full slide");

    slide = _controller.presentation.slides[1];
    slideLayer.slide = slide;
    XCTAssertFalse([_controller shouldDisplayFullImageForLayer:slideLayer], @"Visible slide should be full slide");

    slide = _controller.presentation.slides[2];
    slideLayer.slide = slide;
    XCTAssertTrue([_controller shouldDisplayFullImageForLayer:slideLayer], @"Visible slide should be full slide");

    slide = _controller.presentation.slides[3];
    slideLayer.slide = slide;
    XCTAssertFalse([_controller shouldDisplayFullImageForLayer:slideLayer], @"Visible slide should be full slide");

    slide = _controller.presentation.slides[4];
    slideLayer.slide = slide;
    XCTAssertFalse([_controller shouldDisplayFullImageForLayer:slideLayer], @"Non visible slide on the right side should be full slide");
  }
  else
  {
    slide = _controller.presentation.slides[0];
    slideLayer.slide = slide;
    XCTAssertFalse([_controller shouldDisplayFullImageForLayer:slideLayer], @"Non visible slide on the left side should be full slide");

    slide = _controller.presentation.slides[1];
    slideLayer.slide = slide;
    XCTAssertFalse([_controller shouldDisplayFullImageForLayer:slideLayer], @"Visible slide when zoomed in on iPhone should not be full slide");

    slide = _controller.presentation.slides[2];
    slideLayer.slide = slide;
    XCTAssertTrue([_controller shouldDisplayFullImageForLayer:slideLayer], @"Visible slide when zoomed in on iPhone should not be full slide");

    slide = _controller.presentation.slides[3];
    slideLayer.slide = slide;
    XCTAssertFalse([_controller shouldDisplayFullImageForLayer:slideLayer], @"Visible slide when zoomed in on iPhone should not be full slide");

    slide = _controller.presentation.slides[4];
    slideLayer.slide = slide;
    XCTAssertFalse([_controller shouldDisplayFullImageForLayer:slideLayer], @"Non visible slide on the right side should be full slide");
  }
}


#pragma mark - Feld Support Tests

- (void)testFeldSizeInTryptich
{
  [self prepareEmptyPresentationInATriptych];

  CGSize expectedSize = CGSizeMake(340.0, 192.0);
  CGSize feldSize = [_controller getFeldSize];

  XCTAssertTrue(CGSizeEqualToSize(expectedSize, feldSize), @"Feld size should be %@ but it is %@", NSStringFromCGSize(expectedSize), NSStringFromCGSize(feldSize));
}

- (void)testFeldSizeInSingleFeld
{
  [self prepareEmptyPresentationInASingleFeld];

  CGSize expectedSize = CGSizeMake(800.0, 450.0);
  CGSize feldSize = [_controller getFeldSize];

  XCTAssertTrue(CGSizeEqualToSize(expectedSize, feldSize), @"Feld size should be %@ but it is %@", NSStringFromCGSize(expectedSize), NSStringFromCGSize(feldSize));
}

- (void)testFrameForFeldAtIndexInTryptich
{
  [self prepareEmptyPresentationInATriptych];

  CGRect expectedFrame = CGRectMake(0, 0, 340.0, 191.0);
  CGRect feldFrame = [_controller frameForFeldAtIndex:0];
  XCTAssertTrue(CGRectEqualToRect(expectedFrame, feldFrame), @"Feld size should be %@ but it is %@", NSStringFromCGRect(expectedFrame), NSStringFromCGRect(feldFrame));

  expectedFrame = CGRectMake(340.0, 0, 340.0, 191.0);
  feldFrame = [_controller frameForFeldAtIndex:1];
  XCTAssertTrue(CGRectEqualToRect(expectedFrame, feldFrame), @"Feld size should be %@ but it is %@", NSStringFromCGRect(expectedFrame), NSStringFromCGRect(feldFrame));

  expectedFrame = CGRectMake(680.0, 0, 340.0, 191.0);
  feldFrame = [_controller frameForFeldAtIndex:2];
  XCTAssertTrue(CGRectEqualToRect(expectedFrame, feldFrame), @"Feld size should be %@ but it is %@", NSStringFromCGRect(expectedFrame), NSStringFromCGRect(feldFrame));
}

- (void)testFrameForFeldAtIndexInASingleFeld
{
  [self prepareEmptyPresentationInASingleFeld];

  CGRect expectedFrame = CGRectMake(0, 0, 800.0, 450.0);
  CGRect feldFrame = [_controller frameForFeldAtIndex:0];

  XCTAssertTrue(CGRectEqualToRect(expectedFrame, feldFrame), @"Feld frame should be %@ but it is %@", NSStringFromCGRect(expectedFrame), NSStringFromCGRect(feldFrame));
}


#pragma mark - Slide Layers Management Tests

- (void)testFrameForSlideAtIndexInATriptych
{
  [self preparePresentationInATriptych];

  CGRect expectedFrame = CGRectMake(12, 7, 316, 177);
  CGRect slideFrame = [_controller frameForSlideAtIndex:0];
  XCTAssertTrue(CGRectEqualToRect(expectedFrame, slideFrame), @"Slide frame should be %@ but it is %@", NSStringFromCGRect(expectedFrame), NSStringFromCGRect(slideFrame));

  expectedFrame = CGRectMake(352, 7, 316, 177);
  slideFrame = [_controller frameForSlideAtIndex:1];
  XCTAssertTrue(CGRectEqualToRect(expectedFrame, slideFrame), @"Slide frame should be %@ but it is %@", NSStringFromCGRect(expectedFrame), NSStringFromCGRect(slideFrame));

  expectedFrame = CGRectMake(692, 7, 316, 177);
  slideFrame = [_controller frameForSlideAtIndex:2];
  XCTAssertTrue(CGRectEqualToRect(expectedFrame, slideFrame), @"Slide frame should be %@ but it is %@", NSStringFromCGRect(expectedFrame), NSStringFromCGRect(slideFrame));

  expectedFrame = CGRectMake(1032, 7, 316, 177);
  slideFrame = [_controller frameForSlideAtIndex:3];
  XCTAssertTrue(CGRectEqualToRect(expectedFrame, slideFrame), @"Slide frame should be %@ but it is %@", NSStringFromCGRect(expectedFrame), NSStringFromCGRect(slideFrame));

  expectedFrame = CGRectMake(1372, 7, 316, 177);
  slideFrame = [_controller frameForSlideAtIndex:4];
  XCTAssertTrue(CGRectEqualToRect(expectedFrame, slideFrame), @"Slide frame should be %@ but it is %@", NSStringFromCGRect(expectedFrame), NSStringFromCGRect(slideFrame));
}

- (void)testFrameForSlideAtIndexInASingleFeld
{
  [self preparePresentationInASingleFeld];

  CGRect expectedFrame = CGRectMake(12, 6, 776, 436);
  CGRect slideFrame = [_controller frameForSlideAtIndex:0];
  XCTAssertTrue(CGRectEqualToRect(expectedFrame, slideFrame), @"Slide frame should be %@ but it is %@", NSStringFromCGRect(expectedFrame), NSStringFromCGRect(slideFrame));

  expectedFrame = CGRectMake(812, 6, 776, 436);
  slideFrame = [_controller frameForSlideAtIndex:1];
  XCTAssertTrue(CGRectEqualToRect(expectedFrame, slideFrame), @"Slide frame should be %@ but it is %@", NSStringFromCGRect(expectedFrame), NSStringFromCGRect(slideFrame));

  expectedFrame = CGRectMake(1612, 6, 776, 436);
  slideFrame = [_controller frameForSlideAtIndex:2];
  XCTAssertTrue(CGRectEqualToRect(expectedFrame, slideFrame), @"Slide frame should be %@ but it is %@", NSStringFromCGRect(expectedFrame), NSStringFromCGRect(slideFrame));

  expectedFrame = CGRectMake(2412, 6, 776, 436);
  slideFrame = [_controller frameForSlideAtIndex:3];
  XCTAssertTrue(CGRectEqualToRect(expectedFrame, slideFrame), @"Slide frame should be %@ but it is %@", NSStringFromCGRect(expectedFrame), NSStringFromCGRect(slideFrame));

  expectedFrame = CGRectMake(3212, 6, 776, 436);
  slideFrame = [_controller frameForSlideAtIndex:4];
  XCTAssertTrue(CGRectEqualToRect(expectedFrame, slideFrame), @"Slide frame should be %@ but it is %@", NSStringFromCGRect(expectedFrame), NSStringFromCGRect(slideFrame));
}

- (void)testFrameForSlideAreaInATriptych
{
  [self preparePresentationInATriptych];

  CGRect expectedFrame = CGRectMake(0, 0, 10200, 192);
  CGRect slidesAreaFrame = [_controller frameForSlidesArea];
  XCTAssertTrue(CGRectEqualToRect(expectedFrame, slidesAreaFrame), @"Slides Area frame should be %@ but it is %@", NSStringFromCGRect(expectedFrame), NSStringFromCGRect(slidesAreaFrame));
}

- (void)testFrameForSlideAreaInASingleFeld
{
  [self preparePresentationInASingleFeld];

  CGRect expectedFrame = CGRectMake(0, 0, 8000, 450);
  CGRect slidesAreaFrame = [_controller frameForSlidesArea];
  XCTAssertTrue(CGRectEqualToRect(expectedFrame, slidesAreaFrame), @"Slides Area frame should be %@ but it is %@", NSStringFromCGRect(expectedFrame), NSStringFromCGRect(slidesAreaFrame));
}

- (void)testLoadAllSlides
{
  [self prepareEmptyPresentationInATriptych];

  NSMutableArray *items = [_controller valueForKey:@"slideLayers"];
  XCTAssertEqual(items.count, 0, @"There should not be any slide before getting loaded but there are %lu", (unsigned long)items.count);

  [_systemModelFactory createTestModelWithSlides:10];
  [_controller loadAllSlides];

  XCTAssertEqual(items.count, 10, @"There should be one slide after loading all slide but there are %lu", (unsigned long)items.count);
}

- (void)testRemoveAllSlides
{
  [self preparePresentationInATriptych];
  [_controller loadAllSlides];

  NSMutableArray *items = [_controller valueForKey:@"slideLayers"];
  XCTAssertEqual(items.count, 10, @"There should be 10 slides after loading all of them but there are %lu", (unsigned long)items.count);

  [_controller removeAllSlideLayers];

  XCTAssertEqual(items.count, 0, @"There should not be any slide after removing them all but there are %lu", (unsigned long)items.count);
}


#pragma mark - Observation Tests

- (void)testAddOneSlideToPresentationModel
{
  [self prepareEmptyPresentationInATriptych];

  NSMutableArray *items = [_controller valueForKey:@"slideLayers"];
  XCTAssertEqual(items.count, 0, @"There should not be any slide before getting loaded but there are %lu", (unsigned long)items.count);

  MZPortfolioItem *item = [[MZPortfolioItem alloc] init];
  item.uid = @"sl-1111";

  [_controller.systemModel.currentWorkspace.presentation insertObject:item inSlidesAtIndex:0];

  XCTAssertEqual(items.count, 1, @"There should be one slide after loading all slide but there are %lu", (unsigned long)items.count);
}

- (void)testRemoveOneSlideToPresentationModel
{
  [self preparePresentationInATriptych];
  [_controller loadAllSlides];

  NSMutableArray *items = [_controller valueForKey:@"slideLayers"];
  XCTAssertEqual(items.count, 10, @"There should be 10 slides after loading all of them but there are %lu", (unsigned long)items.count);

  [_controller.systemModel.currentWorkspace.presentation removeObjectFromSlidesAtIndex:0];

  XCTAssertEqual(items.count, 9, @"There should be 9 slides after removing one but there are %lu", (unsigned long)items.count);
}

- (void)testNewPresentationCurrentSlideIndex
{
  [self preparePresentationInATriptych];
  [_controller loadAllSlides];

  XCTAssertEqual(_controller.currentSlideIndex, 0, @"Current slide is %ld but should be %d", (long)_controller.currentSlideIndex, 0);

  _controller.presentation.currentSlideIndex = 4;

  XCTAssertEqual(_controller.currentSlideIndex, 4, @"Current slide is %ld but should be %d", (long)_controller.currentSlideIndex, 4);
}

- (void)testSlidesVisibilityOnPresentationActiveState
{
  [self preparePresentationInATriptych];
  [_controller loadAllSlides];

  _controller.presentation.active = NO;

  for (SlideLayer *slideLayer in [_controller valueForKey:@"slideLayers"])
    XCTAssertTrue(slideLayer.hidden, @"Slide should not be visible when presentation is not active");

  _controller.presentation.active = YES;

  for (SlideLayer *slideLayer in [_controller valueForKey:@"slideLayers"])
    XCTAssertFalse(slideLayer.hidden, @"Slide should be visible when presentation is active");
}

@end
