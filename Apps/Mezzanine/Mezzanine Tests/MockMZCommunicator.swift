//
//  MockMZCommunicator.swift
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 30/10/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

import Foundation

class MockMZCommunicator: MZCommunicator {

  var mockIsARemoteParticipationConnection: Bool = false

  override var isARemoteParticipationConnection: Bool {

    get {
      return mockIsARemoteParticipationConnection
    }

    set {
      mockIsARemoteParticipationConnection = newValue
    }
  }

  override init() {
    super.init()
    self.requestor = MockMZCommunicatorRequestor(communicator: self)
  }
  
}


class MockMZCommunicatorRequestor: MZCommunicatorRequestor {
  
  var transactionRequested: MZTransaction?
  
  override func requestPasskeyEnableRequest() -> MZTransaction! {
    transactionRequested = MZTransaction(transactionId: "mock-transaction-id", transactionName: "requestPassphraseEnable", requestPoolName: "mock-poolname")
    return transactionRequested;
  }
  
  override func requestPasskeyDisableRequest() -> MZTransaction! {
    transactionRequested = MZTransaction(transactionId: "mock-transaction-id", transactionName: "requestPassphraseDisable", requestPoolName: "mock-poolname")
    return transactionRequested;
  }

  override func requestPortfolioClearRequest(_ workspaceUid: String) -> MZTransaction! {
    transactionRequested = MZTransaction(transactionId: "mock-transaction-id", transactionName: "requestPortfolioClear", requestPoolName: "mock-poolname")
    return transactionRequested;
  }

  override func requestPortfolioDownloadRequest(_ workspaceUid: String) -> MZTransaction! {
    transactionRequested = MZTransaction(transactionId: "mock-transaction-id", transactionName: "requestPortfolioDownload", requestPoolName: "mock-poolname")
    return transactionRequested;
  }

  override func requestWhiteboardCaptureRequest(_ whiteboardUid: String, workspaceUid: String) -> MZTransaction! {
    transactionRequested = MZTransaction(transactionId: "mock-transaction-id", transactionName: "requestWhiteboardCapture", requestPoolName: "mock-poolname")
    return transactionRequested;
  }
  
  override func requestPresentationStopRequest(_ workspaceUid: String!) -> MZTransaction! {
    transactionRequested = MZTransaction(transactionId: "mock-transaction-id", transactionName: "requestPresentationStop", requestPoolName: "mock-poolname")
    return transactionRequested;
  }
  
  override func requestWindshieldItemCreateRequest(_ workspaceUid: String!, contentSource: String!, surfaceName: String!, feldRelativeCoords: FeldRelativeCoords!, feldRelativeSize: FeldRelativeSize!) -> MZTransaction! {
    transactionRequested = MZTransaction(transactionId: "mock-transaction-id", transactionName: "requestWindshieldItemCreate", requestPoolName: "mock-poolname")
    return transactionRequested
  }
  
  override func requestPresentationStartRequest(_ workspaceUid: String!, currentIndex: Int) -> MZTransaction! {
    transactionRequested = MZTransaction(transactionId: "mock-transaction-id", transactionName: "requestPresentationStart", requestPoolName: "mock-poolname")
    return transactionRequested
  }
  
  override func requestPortfolioItemDeleteRequest(_ workspaceUid: String!, uid: String!) -> MZTransaction! {
    transactionRequested = MZTransaction(transactionId: "mock-transaction-id", transactionName: "requestPortfolioItemDelete", requestPoolName: "mock-poolname")
    return transactionRequested
  }
  
  override func requestLiveStreamGrabRequest(_ workspaceUid: String!, uid: String!) -> MZTransaction! {
    transactionRequested = MZTransaction(transactionId: "mock-transaction-id", transactionName: "requestLiveStreamGrab", requestPoolName: "mock-poolname")
    return transactionRequested
  }
  
  override func requestPortfolioItemGrabRequest(_ workspaceUid: String!, uid: String!) -> MZTransaction! {
    transactionRequested = MZTransaction(transactionId: "mock-transaction-id", transactionName: "requestPortfolioItemGrab", requestPoolName: "mock-poolname")
    return transactionRequested
  }
  
  override func requestLiveStreamRelinquishRequest(_ workspaceUid: String!, uid: String!) -> MZTransaction! {
    transactionRequested = MZTransaction(transactionId: "mock-transaction-id", transactionName: "requestLiveStreamRelinquish", requestPoolName: "mock-poolname")
    return transactionRequested
  }
  
  override func requestPortfolioItemRelinquishRequest(_ workspaceUid: String!, uid: String!) -> MZTransaction! {
    transactionRequested = MZTransaction(transactionId: "mock-transaction-id", transactionName: "requestPortfolioItemRelinquish", requestPoolName: "mock-poolname")
    return transactionRequested
  }
  
  override func requestPortfolioItemInsertRequest(_ workspaceUid: String!, contentSource: String!, index: Int) -> MZTransaction! {
    transactionRequested = MZTransaction(transactionId: "mock-transaction-id", transactionName: "requestPortfolioItemInsert", requestPoolName: "mock-poolname")
    return transactionRequested
  }
  
  override func requestPortfolioItemReorderRequest(_ workspaceUid: String!, uid: String!, index: Int) -> MZTransaction! {
    transactionRequested = MZTransaction(transactionId: "mock-transaction-id", transactionName: "requestPortfolioItemReorder", requestPoolName: "mock-poolname")
    return transactionRequested
  }
  
  override func requestWindshieldItemDeleteRequest(_ workspaceUid: String!, uid: String!) -> MZTransaction! {
    transactionRequested = MZTransaction(transactionId: "mock-transaction-id", transactionName: "requestWindshieldItemDelete", requestPoolName: "mock-poolname")
    return transactionRequested
  }
}
