//
//  FullWindshieldViewController_Tests.m
//  Mezzanine
//
//  Created by miguel on 8/3/17.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "FullWindshieldViewController.h"
#import "WindshieldViewController_Private.h"
#import "MockSystemModel.h"
#import "MezzanineAppContext.h"

#import "XCTestCase+Animations.h"

#import "Mezzanine-Swift.h"

@interface FullWindshieldViewController_Tests : XCTestCase
{
  FullWindshieldViewController *_controller;
  MockSystemModel *_systemModelFactory;
  WorkspaceGeometry *_workspaceGeometry;
}

@end

@implementation FullWindshieldViewController_Tests

- (void)setUp
{
  [super setUp];

  _systemModelFactory = [MockSystemModel new];
  _controller = [FullWindshieldViewController new];

  MZCommunicator *communicator = [MZCommunicator new];
  communicator.systemModel = [MZSystemModel new];
  communicator.systemModel.myMezzanine.version = @"3.13";
  [MezzanineAppContext currentContext].currentCommunicator = communicator;
}

- (void)tearDown
{
  _controller.windshield = nil;
  _controller.workspace = nil;
  _controller.systemModel = nil;
  _controller = nil;
  _systemModelFactory = nil;
  [MezzanineAppContext currentContext].currentCommunicator = nil;

  [super tearDown];
}


#pragma mark - Helpers methods to setup context

-(void) prepareExtendedWindshieldWithOneSurfaceWithOneFeld
{
  _controller.systemModel = [_systemModelFactory createTestModelWithOneExtendedWindshieldSurfaceWithOneFeld];

  _workspaceGeometry = [WorkspaceGeometry new];
  _workspaceGeometry.surfaces = _controller.systemModel.surfaces;
  [_workspaceGeometry containerRectSetter:CGRectMake(0, 0, 5952, 2160)];
  _controller.workspaceGeometry = _workspaceGeometry;

  [_controller.view setFrame:CGRectMake(0, 0, 5952, 1080)];
}

-(void) prepareExtendedWindshieldWithThreeSurfacesWithOneFeld
{
  _controller.systemModel = [_systemModelFactory createTestModelWithThreeExtendedWindshieldSurfacesWithOneFeld];

  _workspaceGeometry = [WorkspaceGeometry new];
  _workspaceGeometry.surfaces = _controller.systemModel.surfaces;
  [_workspaceGeometry containerRectSetter:CGRectMake(0, 0, 10176, 2160)];
  _controller.workspaceGeometry = _workspaceGeometry;

  [_controller.view setFrame:CGRectMake(0, 0, 10176, 2160)];
}

-(void) prepareExtendedWindshieldWithOneSurfaceWithOneFeldAndOneItem
{
  _controller.systemModel = [_systemModelFactory createTestModelWithOneExtendedWindshieldSurfaceWithOneFeldAndOneItem];

  _workspaceGeometry = [WorkspaceGeometry new];
  _workspaceGeometry.surfaces = _controller.systemModel.surfaces;
  [_workspaceGeometry containerRectSetter:CGRectMake(0, 0, 660, 500)];
  _controller.workspaceGeometry = _workspaceGeometry;

  [_controller.view setFrame:CGRectMake(0, 0, 660, 500)];
}


#pragma mark - Tests Coordinate transformations iOS -> Native

-(void) testRectToNativeWindshieldCoordinatesForFullFeldItemInSurfaceWithOneFeld
{
  [self prepareExtendedWindshieldWithOneSurfaceWithOneFeld];

  CGRect rectToTest = CGRectMake(2016, 540, 1920, 1080);
  MZFeld *feld = [[_controller.systemModel.surfaces firstObject] felds][0];
  CGRect toNative = [_controller rectToNativeWindshieldCoordinates:rectToTest inFeld:feld];
  XCTAssertTrue(CGRectEqualToRect(toNative, CGRectMake(0, 0, 1, 1)), @"Frame is %@ but it should be %@", NSStringFromCGRect(toNative), NSStringFromCGRect(CGRectMake(0, 0, 1, 1)));
}

-(void) testRectToNativeWindshieldCoordinatesForFullFeldItemInFirstOfThreeSurfacesWithOneFeld
{
  [self prepareExtendedWindshieldWithThreeSurfacesWithOneFeld];

  CGRect rectToTest = CGRectMake(2016, 540, 1920, 1080);

  MZFeld *feld = [_controller.systemModel.surfaces[0] felds][0];
  CGRect toNative = [self roundFrame:[_controller rectToNativeWindshieldCoordinates:rectToTest inFeld:feld]];
  XCTAssertTrue(CGRectEqualToRect(toNative, CGRectMake(0, 0, 1, 1)), @"Frame is %@ but it should be %@", NSStringFromCGRect(toNative), NSStringFromCGRect(CGRectMake(0, 0, 1, 1)));
}

-(void) testRectToNativeWindshieldCoordinatesForFullFeldItemInSecondOfThreeSurfacesWithOneFeld
{
  [self prepareExtendedWindshieldWithThreeSurfacesWithOneFeld];

  CGRect rectToTest = CGRectMake(3936, 540, 1920, 1080);
  MZFeld *feld = [_controller.systemModel.surfaces[1] felds][0];
  CGRect toNative = [self roundFrame:[_controller rectToNativeWindshieldCoordinates:rectToTest inFeld:feld]];
  XCTAssertTrue(CGRectEqualToRect(toNative, CGRectMake(0, 0, 1, 1)), @"Frame is %@ but it should be %@", NSStringFromCGRect(toNative), NSStringFromCGRect(CGRectMake(0, 0, 1, 1)));
}

-(void) testRectToNativeWindshieldCoordinatesForFullFeldItemInThirdOfThreeSurfacesWithOneFeld
{
  [self prepareExtendedWindshieldWithThreeSurfacesWithOneFeld];

  CGRect rectToTest = CGRectMake(5856, 540, 1920, 1080);
  MZFeld *feld = [_controller.systemModel.surfaces[2] felds][0];
  CGRect toNative = [self roundFrame:[_controller rectToNativeWindshieldCoordinates:rectToTest inFeld:feld]];
  XCTAssertTrue(CGRectEqualToRect(toNative, CGRectMake(0, 0, 1, 1)), @"Frame is %@ but it should be %@", NSStringFromCGRect(toNative), NSStringFromCGRect(CGRectMake(0, 0, 1, 1)));
}

#pragma mark - Tests Coordinate transformations Native -> iOS

-(void) testRectFromNativeWindshieldCoordinatesForFullFeldItemInSurfaceWithOneFeld
{
  [self prepareExtendedWindshieldWithOneSurfaceWithOneFeld];

  MZWindshieldItem *item = [[MZWindshieldItem alloc] init];
  item.frame = CGRectMake(0, 0, 1, 1);
  item.surface = [_controller.systemModel.surfaces firstObject];
  item.feld = [item.surface felds][0];

  CGRect rectToTest = CGRectMake(2016, 540, 1920, 1080);
  CGRect fromNative = [_controller rectFromNativeWindshieldCoordinatesForItem:item];
  XCTAssertTrue(CGRectEqualToRect(fromNative, rectToTest), @"Frame is %@ but it should be %@", NSStringFromCGRect(fromNative), NSStringFromCGRect(rectToTest));
}

-(void) testRectFromNativeWindshieldCoordinatesForFullFeldItemInFirstOfThreeSurfacesWithOneFeld
{
  [self prepareExtendedWindshieldWithThreeSurfacesWithOneFeld];

  MZWindshieldItem *item = [[MZWindshieldItem alloc] init];
  item.frame = CGRectMake(0, 0, 1, 1);
  item.surface = _controller.systemModel.surfaces[0];
  item.feld = [item.surface felds][0];

  CGRect rectToTest = CGRectMake(2016, 540, 1920, 1080);
  CGRect fromNative = [self roundFrame:[_controller rectFromNativeWindshieldCoordinatesForItem:item]];
  XCTAssertTrue(CGRectEqualToRect(fromNative, rectToTest), @"Frame is %@ but it should be %@", NSStringFromCGRect(fromNative), NSStringFromCGRect(rectToTest));
}

-(void) testRectFromNativeWindshieldCoordinatesForFullFeldItemInSecondOfThreeSurfacesWithOneFeld
{
  [self prepareExtendedWindshieldWithThreeSurfacesWithOneFeld];

  MZWindshieldItem *item = [[MZWindshieldItem alloc] init];
  item.frame = CGRectMake(0, 0, 1, 1);
  item.surface = _controller.systemModel.surfaces[1];
  item.feld = [item.surface felds][0];

  CGRect rectToTest = CGRectMake(4128, 540, 1920, 1080);
  CGRect fromNative = [self roundFrame:[_controller rectFromNativeWindshieldCoordinatesForItem:item]];
  XCTAssertTrue(CGRectEqualToRect(fromNative, rectToTest), @"Frame is %@ but it should be %@", NSStringFromCGRect(fromNative), NSStringFromCGRect(rectToTest));
}

-(void) testRectFromNativeWindshieldCoordinatesForFullFeldItemInThirdOfThreeSurfacesWithOneFeld
{
  [self prepareExtendedWindshieldWithThreeSurfacesWithOneFeld];

  MZWindshieldItem *item = [[MZWindshieldItem alloc] init];
  item.frame = CGRectMake(0, 0, 1, 1);
  item.surface = _controller.systemModel.surfaces[2];
  item.feld = [item.surface felds][0];

  CGRect rectToTest = CGRectMake(6240, 540, 1920, 1080);
  CGRect fromNative = [self roundFrame:[_controller rectFromNativeWindshieldCoordinatesForItem:item]];
  XCTAssertTrue(CGRectEqualToRect(fromNative, rectToTest), @"Frame is %@ but it should be %@", NSStringFromCGRect(fromNative), NSStringFromCGRect(rectToTest));
}


#pragma mark - Feld and Surface Locations

-(void) testClosestFeldAtLocationInsideAFeldInASurfaceWithAFeld
{
  [self prepareExtendedWindshieldWithOneSurfaceWithOneFeld];

  MZSurface *surface = [_controller.systemModel.surfaces firstObject];
  MZFeld *feldInSurface = [[surface felds] firstObject];

  CGRect surfaceRect = [_controller.workspaceGeometry rectForSurface:surface];
  CGPoint pointToTest = CGPointMake(2500, 1000);

  XCTAssertTrue(CGRectContainsPoint(surfaceRect, pointToTest), @"Point to test should be inside the feld and surface");

  MZFeld *feldAtLocation = [_controller closestFeldAtLocation:pointToTest];

  XCTAssertEqual(feldInSurface, feldAtLocation, @"Felds should be the same");
  XCTAssertNotNil(feldAtLocation, @"The feld should be found at that place");
}

-(void) testClosestFeldAtLocationOutsideAFeldInASurfaceWithAFeld
{
  [self prepareExtendedWindshieldWithOneSurfaceWithOneFeld];

  MZSurface *surface = [_controller.systemModel.surfaces firstObject];
  MZFeld *feldInSurface = [[surface felds] firstObject];

  CGRect surfaceRect = [_controller.workspaceGeometry rectForSurface:surface];
  CGPoint pointToTest = CGPointMake(0, 0);

  XCTAssertFalse(CGRectContainsPoint(surfaceRect, pointToTest), @"Point to test should be outside the feld and surface");

  MZFeld *feldAtLocation = [_controller closestFeldAtLocation:CGPointMake(0, 0)];

  XCTAssertEqual(feldInSurface, feldAtLocation, @"Felds should be the same");
  XCTAssertNotNil(feldAtLocation, @"The feld should be found at that place");
}

-(void) testClosestFeldAtLocationInsideAFeldInTheSecondSurfaceWithAFeld
{
  [self prepareExtendedWindshieldWithThreeSurfacesWithOneFeld];

  MZSurface *surface = _controller.systemModel.surfaces[1];
  MZFeld *feldInSurface = [[surface felds] firstObject];

  CGRect surfaceRect = [_controller.workspaceGeometry rectForSurface:surface];
  CGPoint pointToTest = CGPointMake(4500, 1000);

  XCTAssertTrue(CGRectContainsPoint(surfaceRect, pointToTest), @"Point to test should be inside the feld and surface");

  MZFeld *feldAtLocation = [_controller closestFeldAtLocation:pointToTest];

  XCTAssertEqual(feldInSurface, feldAtLocation, @"Felds should be the same");
  XCTAssertNotNil(feldAtLocation, @"The feld should be found at that place");
}

-(void) testFeldAtLocationOutsideAFeldInTheSecondSurfaceWithAFeld
{
  [self prepareExtendedWindshieldWithThreeSurfacesWithOneFeld];

  MZSurface *surface = _controller.systemModel.surfaces[1];
  MZFeld *feldInSurface = [[surface felds] firstObject];

  CGRect surfaceRect = [_controller.workspaceGeometry rectForSurface:surface];
  CGRect feldRect = [_controller.workspaceGeometry rectForFeld:feldInSurface];
  CGPoint pointToTest = CGPointMake(4500, 450);

  XCTAssertFalse(CGRectContainsPoint(surfaceRect, pointToTest), @"Point to test should be outside the feld and surface");

  MZFeld *feldAtLocation = [_controller closestFeldAtLocation:pointToTest];

  XCTAssertEqual(feldInSurface, feldAtLocation, @"Felds should be the same");
  XCTAssertNotNil(feldAtLocation, @"The feld should be found at that place");
}


#pragma mark - Loading Items

-(void) setupExtendedWindshieldWithOneImageWindshieldItem
{
  MZWindshieldItem *item = [[MZWindshieldItem alloc] init];
  item.assetUid = @"as-1111";
  item.frame = CGRectMake(0, 0, 1, 1);
  item.surface = _controller.systemModel.surfaces[0];
  item.feld = [item.surface felds][0];

  [_controller.systemModel.currentWorkspace.windshield insertObject:item inItemsAtIndex:0];

  _controller.workspace = _controller.systemModel.currentWorkspace;
  _controller.windshield = _controller.systemModel.currentWorkspace.windshield;
}

-(void) testLoadAllItems
{
  [self prepareExtendedWindshieldWithOneSurfaceWithOneFeld];

  NSMutableArray *items = [_controller valueForKey:@"itemViews"];
  XCTAssertEqual(items.count, 0, @"There should not be any item before getting loaded but there are %lu", (unsigned long)items.count);

  [self setupExtendedWindshieldWithOneImageWindshieldItem];
  [_controller loadAllWindshieldItems];

  items = [_controller valueForKey:@"itemViews"];
  XCTAssertEqual(items.count, 1, @"There should be one item after loading all items but there are %lu", (unsigned long)items.count);
}

-(void) testAddOneItemToWindshieldModel
{
  [self prepareExtendedWindshieldWithOneSurfaceWithOneFeld];

  _controller.workspace = _controller.systemModel.currentWorkspace;
  _controller.windshield = _controller.systemModel.currentWorkspace.windshield;

  NSMutableArray *items = [_controller valueForKey:@"itemViews"];
  XCTAssertEqual(items.count, 0, @"There should not be any item before getting loaded but there are %lu", (unsigned long)items.count);

  MZWindshieldItem *item = [[MZWindshieldItem alloc] init];
  item.assetUid = @"as-1111";
  item.frame = CGRectMake(0, 0, 1, 1);
  item.surface = _controller.systemModel.surfaces[0];
  item.feld = [item.surface felds][0];

  [_controller.systemModel.currentWorkspace.windshield insertObject:item inItemsAtIndex:0];

  items = [_controller valueForKey:@"itemViews"];
  XCTAssertEqual(items.count, 1, @"There should be one item after loading all items but there are %lu", (unsigned long)items.count);
}

-(void) testRemoveAllItems
{
  [self prepareExtendedWindshieldWithOneSurfaceWithOneFeld];
  [self setupExtendedWindshieldWithOneImageWindshieldItem];
  [_controller loadAllWindshieldItems];

  NSMutableArray *items = [_controller valueForKey:@"itemViews"];
  XCTAssertEqual(items.count, 1, @"There should be one item after loading all items but there are %lu", (unsigned long)items.count);

  [_controller removeAllItemViews];

  items = [_controller valueForKey:@"itemViews"];
  XCTAssertEqual(items.count, 0, @"There should not be any item before getting loaded but there are %lu", (unsigned long)items.count);
}

-(void) testRemoveOneItemFromWindshieldModel
{
  [self prepareExtendedWindshieldWithOneSurfaceWithOneFeld];
  [self setupExtendedWindshieldWithOneImageWindshieldItem];
  [_controller loadAllWindshieldItems];

  NSMutableArray *items = [_controller valueForKey:@"itemViews"];
  XCTAssertEqual(items.count, 1, @"There should be one item after loading all items but there are %lu", (unsigned long)items.count);

  [_controller.systemModel.currentWorkspace.windshield removeObjectFromItemsAtIndex:0];

  [self waitForAnimations:0.25];

  items = [_controller valueForKey:@"itemViews"];
  XCTAssertEqual(items.count, 0, @"There should not be any item before getting loaded but there are %lu", (unsigned long)items.count);
}


#pragma mark - Helpers

- (CGRect)roundFrame:(CGRect)rect
{
  rect.origin.x = round(rect.origin.x);
  rect.origin.y = round(rect.origin.y);
  rect.size.width = round(rect.size.width);
  rect.size.height = round(rect.size.height);

  return rect;
}

- (CGFloat)roundFloatNearestPixel:(CGFloat)value
{
  CGFloat n = [UIScreen mainScreen].scale;
  return round(value * n) / n;
}

@end
