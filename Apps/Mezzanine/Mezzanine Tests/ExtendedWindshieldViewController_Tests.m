//
//  ExtendedWindshieldViewController_Tests.m
//  Mezzanine
//
//  Created by Miguel Sánchez Valdés on 07/10/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "XCTestCase+Animations.h"
#import "ExtendedWindshieldViewController.h"
#import "ExtendedWindshieldViewController_Private.h"
#import "WindshieldViewController_Private.h"
#import "MockSystemModel.h"
#import "MezzanineStyleSheet.h"
#import "MezzanineAppContext.h"
#import "Mezzanine-Swift.h"

@interface ExtendedWindshieldViewController_Tests : XCTestCase
{
  ExtendedWindshieldViewController *_controller;
  MockSystemModel *_systemModelFactory;
}
@end

@implementation ExtendedWindshieldViewController_Tests

-(void) setUp
{
  [super setUp];

  _systemModelFactory = [MockSystemModel new];
  _controller = [ExtendedWindshieldViewController new];
  
  MZCommunicator *communicator = [MZCommunicator new];
  communicator.systemModel = [MZSystemModel new];
  communicator.systemModel.myMezzanine.version = @"3.13";
  [MezzanineAppContext currentContext].currentCommunicator = communicator;

}

-(void) tearDown
{
  _controller.windshield = nil;
  _controller.workspace = nil;
  _controller.systemModel = nil;
  _controller = nil;
  _systemModelFactory = nil;
  [MezzanineAppContext currentContext].currentCommunicator = nil;

  [super tearDown];
}


#pragma mark - Helpers methods to setup context

-(void) prepareExtendedWindshieldWithOneSurfaceWithOneFeld
{
  _controller.systemModel = [_systemModelFactory createTestModelWithOneExtendedWindshieldSurfaceWithOneFeld];
  [_controller.view setFrame:CGRectMake(0, 0, 1000, 1000)];
  [_controller refreshLayout];
}

-(void) prepareExtendedWindshieldWithThreeSurfacesWithOneFeld
{
  _controller.systemModel = [_systemModelFactory createTestModelWithThreeExtendedWindshieldSurfacesWithOneFeld];
  [_controller.view setFrame:CGRectMake(0, 0, 1000, 1000)];
  [_controller refreshLayout];
}

-(void) prepareExtendedWindshieldWithOneSurfaceWithOneFeldAndOneItem
{
  _controller.systemModel = [_systemModelFactory createTestModelWithOneExtendedWindshieldSurfaceWithOneFeldAndOneItem];
  [_controller.view setFrame:CGRectMake(0, 0, 500, 500)];
  [_controller refreshLayout];
}


#pragma mark - Background colors

- (void)testBackgroundColor
{
  [self prepareExtendedWindshieldWithOneSurfaceWithOneFeld];
  
  UIView *feldBoundsContainer = [_controller valueForKey:@"feldBoundViewsContainer"];
  UIView *view = [[feldBoundsContainer subviews] firstObject];
  
  XCTAssertTrue([view isKindOfClass:[SurfaceView class]], @"Only a view of SurfaceView type should be included in the feld bounds container");
  XCTAssertTrue([view.backgroundColor isEqual:[MezzanineStyleSheet sharedStyleSheet].grey60Color]);
}


#pragma mark - View Layouts

- (void)testCreateOneSurfaceView
{
  [self prepareExtendedWindshieldWithOneSurfaceWithOneFeld];

  UIView *feldBoundsContainer = [_controller valueForKey:@"feldBoundViewsContainer"];
  XCTAssertEqual([[feldBoundsContainer subviews] count], 1, @"There should be just 1 surface view but there are %lu", (unsigned long)[[feldBoundsContainer subviews] count]);

  UIView *view = [[feldBoundsContainer subviews] firstObject];
  XCTAssertTrue([view isKindOfClass:[SurfaceView class]], @"Only a view of SurfaceView type should be included in the feld bounds container");
  XCTAssertTrue(CGRectContainsRect(CGRectMake(0, 0, 1000, 1000), view.frame), @"The feld view should be inscribed in its container view but it's not");
  XCTAssertTrue(CGRectEqualToRect(CGRectMake(20, 230, 960, 540), view.frame), @"The feld view frame should be equal to %@ but it is %@", NSStringFromCGRect(CGRectMake(20, 230, 960, 540)), NSStringFromCGRect(view.frame));
}

- (void)testCreateMultipleSurfaceView
{
  [self prepareExtendedWindshieldWithThreeSurfacesWithOneFeld];

  UIView *feldBoundsContainer = [_controller valueForKey:@"feldBoundViewsContainer"];
  XCTAssertEqual([[feldBoundsContainer subviews] count], 3, @"There should be 3 surface views but there are %lu", (unsigned long)[[feldBoundsContainer subviews] count]);

  UIView *view = [feldBoundsContainer subviews][0];
  XCTAssertTrue([view isKindOfClass:[SurfaceView class]], @"Only a view of SurfaceView type should be included in the feld bounds container");
  XCTAssertTrue(CGRectContainsRect(CGRectMake(0, 0, 333, 1000), view.frame), @"The feld view should be inscribed in its container view but it's not");
  XCTAssertTrue(CGRectEqualToRect(CGRectMake(20, 417, 293, 164), view.frame), @"The feld view frame should be equal to %@ but it is %@", NSStringFromCGRect(CGRectMake(20, 417, 293, 164)), NSStringFromCGRect(view.frame));

  view = [feldBoundsContainer subviews][1];
  XCTAssertTrue([view isKindOfClass:[SurfaceView class]], @"Only a view of SurfaceView type should be included in the feld bounds container");
  XCTAssertTrue(CGRectContainsRect(CGRectMake(334, 0, 333, 1000), view.frame), @"The feld view should be inscribed in its container view but it's not");
  XCTAssertTrue(CGRectEqualToRect(CGRectMake(353, 417, 293, 164), view.frame), @"The feld view frame should be equal to %@ but it is %@", NSStringFromCGRect(CGRectMake(20, 214, 293, 164)), NSStringFromCGRect(view.frame));

  view = [feldBoundsContainer subviews][2];
  XCTAssertTrue([view isKindOfClass:[SurfaceView class]], @"Only a view of SurfaceView type should be included in the feld bounds container");
  XCTAssertTrue(CGRectContainsRect(CGRectMake(667, 0, 333, 1000), view.frame), @"The feld view should be inscribed in its container view but it's not");
  XCTAssertTrue(CGRectEqualToRect(CGRectMake(686, 417, 293, 164), view.frame), @"The feld view frame should be equal to %@ but it is %@", NSStringFromCGRect(CGRectMake(20, 214, 293, 164)), NSStringFromCGRect(view.frame));
}


#pragma mark - Tests Coordinate transformations iOS -> Native

-(void) testRectToNativeWindshieldCoordinatesForFullFeldItemInSurfaceWithOneFeld
{
  [self prepareExtendedWindshieldWithOneSurfaceWithOneFeld];

  MZFeld *feld = [[_controller.systemModel.surfaces firstObject] felds][0];
  CGRect toNative = [_controller rectToNativeWindshieldCoordinates:CGRectMake(20, 230, 960, 540) inFeld:feld];
  XCTAssertTrue(CGRectEqualToRect(toNative, CGRectMake(0, 0, 1, 1)), @"Frame is %@ but it should be %@", NSStringFromCGRect(toNative), NSStringFromCGRect(CGRectMake(0, 0, 1, 1)));
}

-(void) testRectToNativeWindshieldCoordinatesForFullFeldItemInFirstOfThreeSurfacesWithOneFeld
{
  [self prepareExtendedWindshieldWithThreeSurfacesWithOneFeld];

  MZFeld *feld = [_controller.systemModel.surfaces[0] felds][0];
  CGRect toNative = [_controller rectToNativeWindshieldCoordinates:CGRectMake(20, 417, 293, 164) inFeld:feld];
  XCTAssertTrue(CGRectEqualToRect(toNative, CGRectMake(0, 0, 1, 1)), @"Frame is %@ but it should be %@", NSStringFromCGRect(toNative), NSStringFromCGRect(CGRectMake(0, 0, 1, 1)));
}

-(void) testRectToNativeWindshieldCoordinatesForFullFeldItemInSecondOfThreeSurfacesWithOneFeld
{
  [self prepareExtendedWindshieldWithThreeSurfacesWithOneFeld];

  MZFeld *feld = [_controller.systemModel.surfaces[1] felds][0];
  CGRect toNative = [_controller rectToNativeWindshieldCoordinates:CGRectMake(353, 417, 293, 164) inFeld:feld];
  XCTAssertTrue(CGRectEqualToRect(toNative, CGRectMake(0, 0, 1, 1)), @"Frame is %@ but it should be %@", NSStringFromCGRect(toNative), NSStringFromCGRect(CGRectMake(0, 0, 1, 1)));
}

-(void) testRectToNativeWindshieldCoordinatesForFullFeldItemInThirdOfThreeSurfacesWithOneFeld
{
  [self prepareExtendedWindshieldWithThreeSurfacesWithOneFeld];

  MZFeld *feld = [_controller.systemModel.surfaces[2] felds][0];
  CGRect toNative = [_controller rectToNativeWindshieldCoordinates:CGRectMake(686, 417, 293, 164) inFeld:feld];
  XCTAssertTrue(CGRectEqualToRect(toNative, CGRectMake(0, 0, 1, 1)), @"Frame is %@ but it should be %@", NSStringFromCGRect(toNative), NSStringFromCGRect(CGRectMake(0, 0, 1, 1)));
}

-(void) testRectToNativeWindshieldCoordinatesForNonFullFeldItemInSurfaceWithOneFeld
{
  [self prepareExtendedWindshieldWithOneSurfaceWithOneFeld];

  MZFeld *feld = [[[_controller.systemModel.surfaces firstObject] felds] firstObject];
  CGRect toNative = [_controller rectToNativeWindshieldCoordinates:CGRectMake(20, 230, 480, 270) inFeld:feld];
  XCTAssertTrue(CGRectEqualToRect(toNative, CGRectMake(-0.25, 0.25, 0.5, 0.5)), @"Frame is %@ but it should be %@", NSStringFromCGRect(toNative), NSStringFromCGRect(CGRectMake(-0.25, 0.25, 0.5, 0.5)));
}

-(void) testRectToNativeWindshieldCoordinatesForNonFullFeldItemInFirstOfThreeSurfacesWithOneFeld
{
  [self prepareExtendedWindshieldWithThreeSurfacesWithOneFeld];

  MZFeld *feld = [_controller.systemModel.surfaces[0] felds][0];
  CGRect toNative = [_controller rectToNativeWindshieldCoordinates:CGRectMake(20, 417, 146.5, 82) inFeld:feld];
  XCTAssertTrue(CGRectEqualToRect(toNative, CGRectMake(-0.25, 0.25, 0.5, 0.5)), @"Frame is %@ but it should be %@", NSStringFromCGRect(toNative), NSStringFromCGRect(CGRectMake(-0.25, 0.25, 0.5, 0.5)));
}

-(void) testRectToNativeWindshieldCoordinatesForNonFullFeldItemInSecondOfThreeSurfacesWithOneFeld
{
  [self prepareExtendedWindshieldWithThreeSurfacesWithOneFeld];

  MZFeld *feld = [_controller.systemModel.surfaces[1] felds][0];
  CGRect toNative = [_controller rectToNativeWindshieldCoordinates:CGRectMake(353, 417, 146.5, 82) inFeld:feld];
  XCTAssertTrue(CGRectEqualToRect(toNative, CGRectMake(-0.25, 0.25, 0.5, 0.5)), @"Frame is %@ but it should be %@", NSStringFromCGRect(toNative), NSStringFromCGRect(CGRectMake(-0.25, 0.25, 0.5, 0.5)));
}

-(void) testRectToNativeWindshieldCoordinatesForNonFullFeldItemInThirdOfThreeSurfacesWithOneFeld
{
  [self prepareExtendedWindshieldWithThreeSurfacesWithOneFeld];

  MZFeld *feld = [_controller.systemModel.surfaces[2] felds][0];
  CGRect toNative = [_controller rectToNativeWindshieldCoordinates:CGRectMake(686, 417, 146.5, 82) inFeld:feld];
  XCTAssertTrue(CGRectEqualToRect(toNative, CGRectMake(-0.25, 0.25, 0.5, 0.5)), @"Frame is %@ but it should be %@", NSStringFromCGRect(toNative), NSStringFromCGRect(CGRectMake(-0.25, 0.25, 0.5, 0.5)));
}


#pragma mark - Tests Coordinate transformations Native -> iOS

-(void) testRectFromNativeWindshieldCoordinatesForFullFeldItemInSurfaceWithOneFeld
{
  [self prepareExtendedWindshieldWithOneSurfaceWithOneFeld];

  MZWindshieldItem *item = [[MZWindshieldItem alloc] init];
  item.frame = CGRectMake(0, 0, 1, 1);
  item.surface = [_controller.systemModel.surfaces firstObject];
  item.feld = [item.surface felds][0];

  CGRect fromNative = [_controller rectFromNativeWindshieldCoordinatesForItem:item];
  XCTAssertTrue(CGRectEqualToRect(fromNative, CGRectMake(20, 230, 960, 540)), @"Frame is %@ but it should be %@", NSStringFromCGRect(fromNative), NSStringFromCGRect(CGRectMake(20, 230, 960, 540)));
}

-(void) testRectFromNativeWindshieldCoordinatesForFullFeldItemInFirstOfThreeSurfacesWithOneFeld
{
  [self prepareExtendedWindshieldWithThreeSurfacesWithOneFeld];

  MZWindshieldItem *item = [[MZWindshieldItem alloc] init];
  item.frame = CGRectMake(0, 0, 1, 1);
  item.surface = _controller.systemModel.surfaces[0];
  item.feld = [item.surface felds][0];

  CGRect fromNative = [_controller rectFromNativeWindshieldCoordinatesForItem:item];
  XCTAssertTrue(CGRectEqualToRect(fromNative, CGRectMake(20, 417, 293, 164)), @"Frame is %@ but it should be %@", NSStringFromCGRect(fromNative), NSStringFromCGRect(CGRectMake(20, 417, 293, 164)));
}

-(void) testRectFromNativeWindshieldCoordinatesForFullFeldItemInSecondOfThreeSurfacesWithOneFeld
{
  [self prepareExtendedWindshieldWithThreeSurfacesWithOneFeld];

  MZWindshieldItem *item = [[MZWindshieldItem alloc] init];
  item.frame = CGRectMake(0, 0, 1, 1);
  item.surface = _controller.systemModel.surfaces[1];
  item.feld = [item.surface felds][0];

  CGRect fromNative = [_controller rectFromNativeWindshieldCoordinatesForItem:item];
  XCTAssertTrue(CGRectEqualToRect(fromNative, CGRectMake(353, 417, 293, 164)), @"Frame is %@ but it should be %@", NSStringFromCGRect(fromNative), NSStringFromCGRect(CGRectMake(353, 417, 293, 164)));
}

-(void) testRectFromNativeWindshieldCoordinatesForFullFeldItemInThirdOfThreeSurfacesWithOneFeld
{
  [self prepareExtendedWindshieldWithThreeSurfacesWithOneFeld];

  MZWindshieldItem *item = [[MZWindshieldItem alloc] init];
  item.frame = CGRectMake(0, 0, 1, 1);
  item.surface = _controller.systemModel.surfaces[2];
  item.feld = [item.surface felds][0];

  CGRect fromNative = [_controller rectFromNativeWindshieldCoordinatesForItem:item];
  XCTAssertTrue(CGRectEqualToRect(fromNative, CGRectMake(686, 417, 293, 164)), @"Frame is %@ but it should be %@", NSStringFromCGRect(fromNative), NSStringFromCGRect(CGRectMake(686, 417, 293, 164)));
}

-(void) testRectFromNativeWindshieldCoordinatesForNonFullFeldItemInSurfaceWithOneFeld
{
  [self prepareExtendedWindshieldWithOneSurfaceWithOneFeld];

  MZWindshieldItem *item = [[MZWindshieldItem alloc] init];
  item.frame = CGRectMake(-0.25, 0.25, 0.5, 0.5);
  item.surface = [_controller.systemModel.surfaces firstObject];
  item.feld = [item.surface felds][0];

  CGRect fromNative = [_controller rectFromNativeWindshieldCoordinatesForItem:item];
  XCTAssertTrue(CGRectEqualToRect(fromNative, CGRectMake(20, 230, 480, 270)), @"Frame is %@ but it should be %@", NSStringFromCGRect(fromNative), NSStringFromCGRect(CGRectMake(20, 230, 480, 270)));
}

-(void) testRectFromNativeWindshieldCoordinatesForNonFullFeldItemInFirstOfThreeSurfacesWithOneFeld
{
  [self prepareExtendedWindshieldWithThreeSurfacesWithOneFeld];

  MZWindshieldItem *item = [[MZWindshieldItem alloc] init];
  item.frame = CGRectMake(-0.25, 0.25, 0.5, 0.5);
  item.surface = _controller.systemModel.surfaces[0];
  item.feld = [item.surface felds][0];

  CGRect fromNative = [_controller rectFromNativeWindshieldCoordinatesForItem:item];
  XCTAssertTrue(CGRectEqualToRect(fromNative, CGRectMake(20, 417, 146.5, 82)), @"Frame is %@ but it should be %@", NSStringFromCGRect(fromNative), NSStringFromCGRect(CGRectMake(20, 417, 146.5, 82)));
}

-(void) testRectFromNativeWindshieldCoordinatesForNonFullFeldItemInSecondOfThreeSurfacesWithOneFeld
{
  [self prepareExtendedWindshieldWithThreeSurfacesWithOneFeld];

  MZWindshieldItem *item = [[MZWindshieldItem alloc] init];
  item.frame = CGRectMake(-0.25, 0.25, 0.5, 0.5);
  item.surface = _controller.systemModel.surfaces[1];
  item.feld = [item.surface felds][0];

  CGRect fromNative = [_controller rectFromNativeWindshieldCoordinatesForItem:item];
  XCTAssertTrue(CGRectEqualToRect(fromNative, CGRectMake(353, 417, 146.5, 82)), @"Frame is %@ but it should be %@", NSStringFromCGRect(fromNative), NSStringFromCGRect(CGRectMake(353, 417, 146.5, 82)));
}

-(void) testRectFromNativeWindshieldCoordinatesForNonFullFeldItemInThirdOfThreeSurfacesWithOneFeld
{
  [self prepareExtendedWindshieldWithThreeSurfacesWithOneFeld];

  MZWindshieldItem *item = [[MZWindshieldItem alloc] init];
  item.frame = CGRectMake(-0.25, 0.25, 0.5, 0.5);
  item.surface = _controller.systemModel.surfaces[2];
  item.feld = [item.surface felds][0];

  CGRect fromNative = [_controller rectFromNativeWindshieldCoordinatesForItem:item];
  XCTAssertTrue(CGRectEqualToRect(fromNative, CGRectMake(686, 417, 146.5, 82)), @"Frame is %@ but it should be %@", NSStringFromCGRect(fromNative), NSStringFromCGRect(CGRectMake(686, 417, 146.5, 82)));
}


#pragma mark - Tests Coordinate transformations extended windshield view <-> windshield item container (surface) view

//TODO

#pragma mark - Feld and Surface Locations

-(void) testClosestFeldAtLocationInsideAFeldInASurfaceWithAFeld
{
  [self prepareExtendedWindshieldWithOneSurfaceWithOneFeld];

  MZSurface *surface = [_controller.systemModel.surfaces firstObject];
  MZFeld *feldInSurface = [[surface felds] firstObject];

  UIView *surfaceView = [_controller surfaceViewForSurface:surface];
  CGPoint pointToTest = CGPointMake(250, 250);

  XCTAssertTrue(CGRectContainsPoint(surfaceView.frame, pointToTest), @"Point to test should be inside the feld and surface");

  MZFeld *feldAtLocation = [_controller closestFeldAtLocation:CGPointMake(250, 250)];

  XCTAssertEqual(feldInSurface, feldAtLocation, @"Felds should be the same");
  XCTAssertNotNil(feldAtLocation, @"The feld should be found at that place");
}

-(void) testClosestFeldAtLocationOutsideAFeldInASurfaceWithAFeld
{
  [self prepareExtendedWindshieldWithOneSurfaceWithOneFeld];

  MZSurface *surface = [_controller.systemModel.surfaces firstObject];
  MZFeld *feldInSurface = [[surface felds] firstObject];

  UIView *surfaceView = [_controller surfaceViewForSurface:surface];
  CGPoint pointToTest = CGPointMake(0, 0);

  XCTAssertFalse(CGRectContainsPoint(surfaceView.frame, pointToTest), @"Point to test should be outside the feld and surface");

  MZFeld *feldAtLocation = [_controller closestFeldAtLocation:CGPointMake(0, 0)];

  XCTAssertEqual(feldInSurface, feldAtLocation, @"Felds should be the same");
  XCTAssertNotNil(feldAtLocation, @"The feld should be found at that place");
}

-(void) testClosestFeldAtLocationInsideAFeldInTheSecondSurfaceWithAFeld
{
  [self prepareExtendedWindshieldWithThreeSurfacesWithOneFeld];

  MZSurface *surface = _controller.systemModel.surfaces[1];
  MZFeld *feldInSurface = [[surface felds] firstObject];

  UIView *surfaceView = [_controller surfaceViewForSurface:surface];
  CGPoint pointToTest = CGPointMake(500, 500);

  XCTAssertTrue(CGRectContainsPoint(surfaceView.frame, pointToTest), @"Point to test should be inside the feld and surface");

  MZFeld *feldAtLocation = [_controller closestFeldAtLocation:pointToTest];

  XCTAssertEqual(feldInSurface, feldAtLocation, @"Felds should be the same");
  XCTAssertNotNil(feldAtLocation, @"The feld should be found at that place");
}

-(void) testFeldAtLocationOutsideAFeldInTheSecondSurfaceWithAFeld
{
  [self prepareExtendedWindshieldWithThreeSurfacesWithOneFeld];

  MZSurface *surface = _controller.systemModel.surfaces[1];
  MZFeld *feldInSurface = [[surface felds] firstObject];

  UIView *surfaceView = [_controller surfaceViewForSurface:surface];
  CGPoint pointToTest = CGPointMake(500, 0);

  XCTAssertFalse(CGRectContainsPoint(surfaceView.frame, pointToTest), @"Point to test should be outside the feld and surface");

  MZFeld *feldAtLocation = [_controller closestFeldAtLocation:pointToTest];

  XCTAssertEqual(feldInSurface, feldAtLocation, @"Felds should be the same");
  XCTAssertNotNil(feldAtLocation, @"The feld should be found at that place");
}


#pragma mark - Loading Items

-(void) setupExtendedWindshieldWithOneImageWindshieldItem
{
  MZWindshieldItem *item = [[MZWindshieldItem alloc] init];
  item.assetUid = @"as-1111";
  item.frame = CGRectMake(0, 0, 1, 1);
  item.surface = _controller.systemModel.surfaces[0];
  item.feld = [item.surface felds][0];

  [_controller.systemModel.currentWorkspace.windshield.items addObject:item];

  _controller.workspace = _controller.systemModel.currentWorkspace;
  _controller.windshield = _controller.systemModel.currentWorkspace.windshield;
}

-(void) testLoadAllItems
{
  [self prepareExtendedWindshieldWithOneSurfaceWithOneFeld];

  NSMutableArray *items = [_controller valueForKey:@"itemViews"];
  XCTAssertEqual(items.count, 0, @"There should not be any item before getting loaded but there are %lu", (unsigned long)items.count);

  [self setupExtendedWindshieldWithOneImageWindshieldItem];
  [_controller loadAllWindshieldItems];

  items = [_controller valueForKey:@"itemViews"];
  XCTAssertEqual(items.count, 1, @"There should be one item after loading all items but there are %lu", (unsigned long)items.count);
}

-(void) testAddOneItemToWindshieldModel
{
  [self prepareExtendedWindshieldWithOneSurfaceWithOneFeld];

  _controller.workspace = _controller.systemModel.currentWorkspace;
  _controller.windshield = _controller.systemModel.currentWorkspace.windshield;

  NSMutableArray *items = [_controller valueForKey:@"itemViews"];
  XCTAssertEqual(items.count, 0, @"There should not be any item before getting loaded but there are %lu", (unsigned long)items.count);

  MZWindshieldItem *item = [[MZWindshieldItem alloc] init];
  item.assetUid = @"as-1111";
  item.frame = CGRectMake(0, 0, 1, 1);
  item.surface = _controller.systemModel.surfaces[0];
  item.feld = [item.surface felds][0];

  [_controller.systemModel.currentWorkspace.windshield insertObject:item inItemsAtIndex:0];

  items = [_controller valueForKey:@"itemViews"];
  XCTAssertEqual(items.count, 1, @"There should be one item after loading all items but there are %lu", (unsigned long)items.count);
}

-(void) testRemoveAllItems
{
  [self prepareExtendedWindshieldWithOneSurfaceWithOneFeld];
  [self setupExtendedWindshieldWithOneImageWindshieldItem];
  [_controller loadAllWindshieldItems];

  NSMutableArray *items = [_controller valueForKey:@"itemViews"];
  XCTAssertEqual(items.count, 1, @"There should be one item after loading all items but there are %lu", (unsigned long)items.count);

  [_controller removeAllItemViews];

  items = [_controller valueForKey:@"itemViews"];
  XCTAssertEqual(items.count, 0, @"There should not be any item before getting loaded but there are %lu", (unsigned long)items.count);
}

-(void) testRemoveOneItemFromWindshieldModel
{
  [self prepareExtendedWindshieldWithOneSurfaceWithOneFeld];
  [self setupExtendedWindshieldWithOneImageWindshieldItem];
  [_controller loadAllWindshieldItems];

  NSMutableArray *items = [_controller valueForKey:@"itemViews"];
  XCTAssertEqual(items.count, 1, @"There should be one item after loading all items but there are %lu", (unsigned long)items.count);

  [_controller.systemModel.currentWorkspace.windshield removeObjectFromItemsAtIndex:0];

  [self waitForAnimations:0.25];

  items = [_controller valueForKey:@"itemViews"];
  XCTAssertEqual(items.count, 0, @"There should not be any item before getting loaded but there are %lu", (unsigned long)items.count);
}


@end
