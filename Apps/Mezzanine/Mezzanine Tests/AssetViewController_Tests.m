//
//  AssetViewController_Tests.m
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 31/03/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "AssetViewController.h"
#import "ButtonMenuViewController.h"


@interface AssetViewController_Tests : XCTestCase

@property (nonatomic, strong) AssetViewController *controller;

@end

@implementation AssetViewController_Tests

- (void)setUp {
    [super setUp];
  
  _controller = [AssetViewController new];
  _controller.appContext = [MezzanineAppContext currentContext];
  [MezzanineAppContext currentContext].mainNavController = [[MezzanineNavigationController alloc] initWithRootViewController:_controller];
  
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}


// Test for 16031
- (void)testSharePopover
{
  [_controller showOptions];
  FPPopoverController *popover = [_controller valueForKey:@"_currentPopoverController"];
  NSArray *menuItemsArray = ((ButtonMenuViewController *)popover.viewController).menuItems;

  XCTAssertTrue([((ButtonMenuItem*)[menuItemsArray firstObject]).buttonTitle isEqualToString:NSLocalizedString(@"Asset Viewer Upload Image Action Message", nil)]);
  XCTAssertTrue([((ButtonMenuItem*)[menuItemsArray lastObject]).buttonTitle isEqualToString:NSLocalizedString(@"Asset Viewer Save To Library Action Message", nil)]);

}

- (void)testSharePopoverWhenDisableDownloadsFlagIsEnabled
{
  MZSystemModel *systemModel = _controller.appContext.applicationModel.systemModel;
  systemModel.featureToggles.disableDownloadsEnabled = YES;

  [_controller showOptions];
  FPPopoverController *popover = [_controller valueForKey:@"_currentPopoverController"];
  NSArray *menuItemsArray = ((ButtonMenuViewController *)popover.viewController).menuItems;

  XCTAssertTrue([((ButtonMenuItem*)[menuItemsArray firstObject]).buttonTitle isEqualToString:NSLocalizedString(@"Asset Viewer Upload Image Action Message", nil)]);
  XCTAssertTrue([((ButtonMenuItem*)[menuItemsArray lastObject]).buttonTitle isEqualToString:NSLocalizedString(@"Asset Viewer Sign In To Save To Library Action Message", nil)]);
}

- (void)testSharePopoverWhenDisableDownloadsFlagIsEnabledAndUserIsSignedIn
{
  MZSystemModel *systemModel = _controller.appContext.applicationModel.systemModel;
  systemModel.featureToggles.disableDownloadsEnabled = YES;
  systemModel.currentUsername = @"Test-User";

  [_controller showOptions];
  FPPopoverController *popover = [_controller valueForKey:@"_currentPopoverController"];
  NSArray *menuItemsArray = ((ButtonMenuViewController *)popover.viewController).menuItems;

  XCTAssertTrue([((ButtonMenuItem*)[menuItemsArray firstObject]).buttonTitle isEqualToString:NSLocalizedString(@"Asset Viewer Upload Image Action Message", nil)]);
  XCTAssertTrue([((ButtonMenuItem*)[menuItemsArray lastObject]).buttonTitle isEqualToString:NSLocalizedString(@"Asset Viewer Save To Library Action Message", nil)]);
}


@end
