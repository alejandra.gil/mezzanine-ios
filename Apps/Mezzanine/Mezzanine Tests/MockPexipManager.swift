//
//  MockPexipManager.swift
//  Mezzanine
//
//  Created by miguel on 8/7/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

import UIKit
@testable import Mezzanine
//import PexKit

struct PexipJoinInvocation {
  var conferenceMode: PexipConferenceMode
}
struct PexipLeaveInvocation {}

struct MockPexipManagerConstants{
  static let hostURL = "vtc.pexip-test-node.com"
  static let conferenceID = "1234567"
}


class MockPexipManager: PexipManager {

  var joinInvocations = Array<PexipJoinInvocation>()
  var leaveInvocations = Array<PexipLeaveInvocation>()
  var reconnectMediaIsCalled = false
  
  override init() {
    super.init()
  }

  func joinAsRealPexipManager(_ conferenceMode: PexipConferenceMode) {
    super.join(conferenceMode) {_ in}
  }

  func createConferenceURIAsRealPexipManager() -> ConferenceURI? {
    return super.createConferenceURI()
  }

  override func join(_ conferenceMode: PexipConferenceMode, displayName: String, completion: @escaping (_ result: String) -> Void) {
    joinInvocations.append(PexipJoinInvocation(conferenceMode: conferenceMode))
  }
  
  override func leave() {
    leaveInvocations.append(PexipLeaveInvocation())
  }
  
  override func reconnectMedia() {
    reconnectMediaIsCalled = true
  }

  override func createConferenceURI() -> ConferenceURI? {
    if infopresence?.pexipConference != nil && infopresence?.pexipNode != nil {
      return ConferenceURI(conference: MockPexipManagerConstants.conferenceID, hostname: MockPexipManagerConstants.hostURL)
    }
    else {
      return nil
    }
  }
  
  override func isCameraAvailable() -> Bool {
      return true
  }
  
  final func registerForApplicationWillEnterForegroundNotification() {
    NotificationCenter.default.addObserver(forName: NSNotification.Name.UIApplicationWillEnterForeground, object: nil, queue: OperationQueue()) {
      [unowned self] notification in
      self.reconnectMedia()
    }
  }
  
  final func sendApplicationWillEnterForegroundNotification() {
    NotificationCenter.default.post(name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
  }
}

