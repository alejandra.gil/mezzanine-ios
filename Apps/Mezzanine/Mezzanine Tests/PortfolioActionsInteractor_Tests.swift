//
//  PortfolioActionsInteractor_Tests.swift
//  Mezzanine
//
//  Created by miguel on 14/2/17.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import XCTest
import Photos

@testable import Mezzanine

class PortfolioActionsInteractor_Tests: XCTestCase {

  var interactor: PortfolioActionsInteractor?
  var addMenuInteractor: PortfolioAddMenuInteractor?
  var controller = PortfolioActionsViewController()
  var systemModel = MZSystemModel()
  var communicator = MZCommunicator()
  var systemModelFactory = MockSystemModel()

  override func setUp() {
    super.setUp()

    let systemModelFactory = MockSystemModel()

    systemModel = systemModelFactory.systemModel
    communicator = MockMZCommunicator()
    communicator.systemModel = systemModel

    interactor = PortfolioActionsInteractor(systemModel: systemModel, communicator: communicator)
    addMenuInteractor = PortfolioAddMenuInteractor(systemModel: systemModel, communicator:communicator)

    controller = PortfolioActionsViewController()
    interactor!.presenter = controller
    controller.interactor = interactor!

    _ = controller.view
}

  override func tearDown() {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    super.tearDown()
  }
  

  // MARK: State

  func testDefineCurrentStateForEmptyPortfolio() {
  
    XCTAssertTrue(interactor!.presenter!.state == .initial)
    
    systemModel.currentWorkspace.presentation.slides.removeAllObjects()
    interactor!.defineCurrentState()

    XCTAssertTrue(interactor!.presenter!.state == .emptyPortfolio)

    systemModel.currentWorkspace.presentation.slides.add(MZSlide())
    systemModel.currentWorkspace.presentation.active = false
    interactor!.defineCurrentState()

    XCTAssertTrue(interactor!.presenter!.state == .paused)

    systemModel.currentWorkspace.presentation.active = true
    interactor!.defineCurrentState()

    XCTAssertTrue(interactor!.presenter!.state == .active)
  }


  // MARK: Requests

  func testPlayPresentation() {

    systemModel.currentWorkspace.presentation.active = false
    XCTAssertFalse(systemModel.currentWorkspace.presentation.active)
    interactor!.startOrPausePresentation()

    let requestor = communicator.requestor as! MockMZCommunicatorRequestor
    XCTAssertTrue(requestor.transactionRequested!.transactionName == "requestPresentationStart")

    XCTAssertTrue(systemModel.currentWorkspace.presentation.active)
  }

  func testPausePresentation() {

    systemModel.currentWorkspace.presentation.active = true
    XCTAssertTrue(systemModel.currentWorkspace.presentation.active)
    interactor!.startOrPausePresentation()

    let requestor = communicator.requestor as! MockMZCommunicatorRequestor
    XCTAssertTrue(requestor.transactionRequested!.transactionName == "requestPresentationStop")

    XCTAssertFalse(systemModel.currentWorkspace.presentation.active)
  }

  func testCaptureWhiteboardAtIndex() {

    systemModel.whiteboards = [["uid": "1"]]

    addMenuInteractor!.captureWhiteboardAtIndex(0)

    let requestor = communicator.requestor as! MockMZCommunicatorRequestor
    XCTAssertTrue(requestor.transactionRequested!.transactionName == "requestWhiteboardCapture")
  }

  func testExportPortfolio() {

    interactor!.exportPortfolio()

    let requestor = communicator.requestor as! MockMZCommunicatorRequestor
    XCTAssertTrue(requestor.transactionRequested!.transactionName == "requestPortfolioDownload")
  }

  func testClearPortfolio() {

    interactor!.clearPortfolio()

    let requestor = communicator.requestor as! MockMZCommunicatorRequestor
    XCTAssertTrue(requestor.transactionRequested!.transactionName == "requestPortfolioClear")
  }


  // MARK: Uploads

  func testPrepareUploadQueueWithValidImageSizes() {

    initalizeUploadLimits()

    let mockPhotoAsset = MockedPHAsset()
    mockPhotoAsset.pixelWidth = 10
    mockPhotoAsset.pixelHeight = 10

    let photoExceedingFileSize = PhotoAsset(photoAsset: mockPhotoAsset)
    photoExceedingFileSize.filesize = 1*1024*1024

    let output = addMenuInteractor!.prepareUploadQueue([photoExceedingFileSize])

    XCTAssertEqual(output.uploadInfoArray.count, 1)
    XCTAssertEqual(output.imagesExceeding.fileSize, 0)
    XCTAssertEqual(output.imagesExceeding.pixelSize, 0)
  }

  func testPrepareUploadQueueWithImageExceedingFileSize() {

    initalizeUploadLimits()

    let mockPhotoAsset = MockedPHAsset()
    mockPhotoAsset.pixelWidth = 10
    mockPhotoAsset.pixelHeight = 10

    let photoExceedingFileSize = PhotoAsset(photoAsset: mockPhotoAsset)
    photoExceedingFileSize.filesize = 10*1024*1024

    let output = addMenuInteractor!.prepareUploadQueue([photoExceedingFileSize])

    XCTAssertEqual(output.uploadInfoArray.count, 0)
    XCTAssertEqual(output.imagesExceeding.fileSize, 1)
    XCTAssertEqual(output.imagesExceeding.pixelSize, 0)
  }

  func testPrepareUploadQueueWithImageExceedingPixelSize() {

    initalizeUploadLimits()

    let mockPhotoAsset = MockedPHAsset()
    mockPhotoAsset.pixelWidth = 5000
    mockPhotoAsset.pixelHeight = 5000

    let photoExceedingPixelSize = PhotoAsset(photoAsset: mockPhotoAsset)
    photoExceedingPixelSize.filesize = 1*1024*1024

    let output = addMenuInteractor!.prepareUploadQueue([photoExceedingPixelSize])

    XCTAssertEqual(output.uploadInfoArray.count, 0)
    XCTAssertEqual(output.imagesExceeding.fileSize, 0)
    XCTAssertEqual(output.imagesExceeding.pixelSize, 1)
  }

  func testPrepareUploadQueueWithImageExceedingPixelSizeAndFileSize() {

    initalizeUploadLimits()

    let mockPhotoAsset = MockedPHAsset()
    mockPhotoAsset.pixelWidth = 5000
    mockPhotoAsset.pixelHeight = 5000

    let photoExceedingPixelSize = PhotoAsset(photoAsset: mockPhotoAsset)
    photoExceedingPixelSize.filesize = 10*1024*1024

    let output = addMenuInteractor!.prepareUploadQueue([photoExceedingPixelSize])

    XCTAssertEqual(output.uploadInfoArray.count, 0)
    XCTAssertEqual(output.imagesExceeding.fileSize, 1)
    XCTAssertEqual(output.imagesExceeding.pixelSize, 1)
  }

  func testErrorMessageForUploadsExceedingLimits() {

    systemModel.featureToggles.largeImagesEnabled = false
    systemModel.featureToggles.immenseImagesEnabled = false

    initalizeUploadLimits()

    var errorMessage: String? = addMenuInteractor!.errorMessageForUploadsExceedingLimits((pixelSize: 0, fileSize: 0))
    XCTAssertNil(errorMessage)

    errorMessage = addMenuInteractor!.errorMessageForUploadsExceedingLimits((pixelSize: 1, fileSize: 1))
    XCTAssertEqual(errorMessage, String(format: "Multiple Images Upload File And Pixel Size Error Dialog Message".localized, NSNumber(value: 3), NSNumber(value: 1), NSNumber(value: 2)))

    errorMessage = addMenuInteractor!.errorMessageForUploadsExceedingLimits((pixelSize: 0, fileSize: 1))
    XCTAssertEqual(errorMessage, String(format: "Single Image Upload File Size Error Dialog Message".localized, NSNumber(value: 3)))

    errorMessage = addMenuInteractor!.errorMessageForUploadsExceedingLimits((pixelSize: 0, fileSize: 2))
    XCTAssertEqual(errorMessage, String(format: "Multiple Images Upload File Size Error Dialog Message".localized, NSNumber(value: 3)))

    errorMessage = addMenuInteractor!.errorMessageForUploadsExceedingLimits((pixelSize: 1, fileSize: 0))
    XCTAssertEqual(errorMessage, String(format: "Single Image Upload Pixel Dimension Error Dialog Message".localized, NSNumber(value: 1), NSNumber(value: 2)))

    errorMessage = addMenuInteractor!.errorMessageForUploadsExceedingLimits((pixelSize: 2, fileSize: 0))
    XCTAssertEqual(errorMessage, String(format: "Multiple Images Upload Pixel Dimension Error Dialog Message".localized, NSNumber(value: 1), NSNumber(value: 2)))
  }

  func testErrorMessageForUploadsExceedingLimitsWhenLargeOrImmenseImagesAreEnabled() {

    systemModel.featureToggles.largeImagesEnabled = true
    systemModel.featureToggles.immenseImagesEnabled = true

    initalizeUploadLimits()

    var errorMessage: String? = addMenuInteractor!.errorMessageForUploadsExceedingLimits((pixelSize: 0, fileSize: 0))
    XCTAssertNil(errorMessage)

    errorMessage = addMenuInteractor!.errorMessageForUploadsExceedingLimits((pixelSize: 1, fileSize: 1))
    XCTAssertEqual(errorMessage, String(format: "Multiple Images Upload File And Exceeded Megapixels Error Dialog Message".localized, NSNumber(value: 3), NSNumber(value: 4)))

    errorMessage = addMenuInteractor!.errorMessageForUploadsExceedingLimits((pixelSize: 0, fileSize: 1))
    XCTAssertEqual(errorMessage, String(format: "Single Image Upload File Size Error Dialog Message".localized, NSNumber(value: 3)))

    errorMessage = addMenuInteractor!.errorMessageForUploadsExceedingLimits((pixelSize: 0, fileSize: 2))
    XCTAssertEqual(errorMessage, String(format: "Multiple Images Upload File Size Error Dialog Message".localized, NSNumber(value: 3)))

    errorMessage = addMenuInteractor!.errorMessageForUploadsExceedingLimits((pixelSize: 1, fileSize: 0))
    XCTAssertEqual(errorMessage, String(format: "Single Image Upload Exceeded Megapixels Error Dialog Message".localized, NSNumber(value: 4)))

    errorMessage = addMenuInteractor!.errorMessageForUploadsExceedingLimits((pixelSize: 2, fileSize: 0))
    XCTAssertEqual(errorMessage, String(format: "Multiple Images Upload Exceeded Megapixels Error Dialog Message".localized, NSNumber(value: 4)))
  }

  func initalizeUploadLimits() {
    let uploadLimits = systemModel.uploadLimits
    uploadLimits?.maxImageWidth = NSNumber(value: 1 as Int32)
    uploadLimits?.maxImageHeight = NSNumber(value: 2 as Int32)
    uploadLimits?.maxImageSizeInMB = NSNumber(value: 3 as Int32)
    uploadLimits?.maxImageSizeInMP = NSNumber(value: 4 as Int32)
  }


  // MARK: Upload Limits

  func testCanUploadImageWithPixelSize() {

    systemModel.uploadLimits.maxImageSizeInMP = 10
    XCTAssertFalse(addMenuInteractor!.canUploadImageWithPixelSize(CGSize(width: 1024*10, height: 1024)))
    XCTAssertTrue(addMenuInteractor!.canUploadImageWithPixelSize(CGSize(width: 1024*9, height: 1024)))
    XCTAssertFalse(addMenuInteractor!.canUploadImageWithPixelSize(CGSize(width: 0, height: 0)))
  }

  func testCanUploadImageWithFileSize() {

    systemModel.uploadLimits.maxImageSizeInMB = 10
    XCTAssertFalse(addMenuInteractor!.canUploadImageWithFileSize(1024*10*1024))
    XCTAssertTrue(addMenuInteractor!.canUploadImageWithFileSize(1024*9*1024))
    XCTAssertFalse(addMenuInteractor!.canUploadImageWithFileSize(0))
  }

  func testCanUploadImageWithPixelDimension() {

    systemModel.uploadLimits.maxImageWidth = 10
    systemModel.uploadLimits.maxImageHeight = 20

    XCTAssertFalse(addMenuInteractor!.canUploadImageWithPixelDimension(CGSize(width: 10, height: 21)))
    XCTAssertFalse(addMenuInteractor!.canUploadImageWithPixelDimension(CGSize(width: 11, height: 20)))
    XCTAssertFalse(addMenuInteractor!.canUploadImageWithPixelDimension(CGSize(width: 21, height: 10)))
    XCTAssertFalse(addMenuInteractor!.canUploadImageWithPixelDimension(CGSize(width: 20, height: 11)))

    XCTAssertTrue(addMenuInteractor!.canUploadImageWithPixelDimension(CGSize(width: 10, height: 20)))
    XCTAssertTrue(addMenuInteractor!.canUploadImageWithPixelDimension(CGSize(width: 20, height: 10)))

    XCTAssertFalse(addMenuInteractor!.canUploadImageWithPixelDimension(CGSize(width: 10, height: 0)))
    XCTAssertFalse(addMenuInteractor!.canUploadImageWithPixelDimension(CGSize(width: 0, height: 10)))
  }
}
