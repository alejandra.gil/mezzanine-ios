//
//  File.swift
//  Mezzanine Tests
//
//  Created by Ivan Bella Lopez on 11/03/2019.
//  Copyright © 2019 Oblong Industries. All rights reserved.
//

import Foundation

class MezzanineRoomHelper {

  func createMezzanineRoom(name: String, type: MZMezzanineRoomType, participants: Array<MZParticipant>) -> MZMezzanine {
    let mezzanine = MZMezzanine()
    mezzanine.name = name
    mezzanine.roomType = type
    mezzanine.uid = String(format:"%f", CACurrentMediaTime())
    mezzanine.insertParticipants(participants, at: NSIndexSet(indexesIn: NSMakeRange(0, participants.count)) as IndexSet)

    return mezzanine
  }

  func createRemoteMezzanineRooms(count: Int, type: MZMezzanineRoomType) -> Array<MZMezzanine> {
    var arrayMezzanineRooms = Array<MZMezzanine>()

    for i in 1...count  {
      arrayMezzanineRooms.append(createMezzanineRoom(name: String(format: "Room%i", i),
                                                     type: type,
                                                     participants: [createParticipant(name: String(format: "Participant%i", i))]))
    }

    return arrayMezzanineRooms
  }

  func createParticipant(name: String) -> MZParticipant {
    let participant = MZParticipant()
    participant.displayName = name
    participant.uid = String(format:"%f", CACurrentMediaTime())

    return participant
  }

  func createParticipants(count: Int) -> Array<MZParticipant> {
    var arrayParticipant = Array<MZParticipant>()

    for i in 1...count  {
      arrayParticipant.append(createParticipant(name: String(format: "Participant%i", i)))
    }

    return arrayParticipant
  }
}
