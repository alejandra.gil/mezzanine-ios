//
//  MockOBOvum.h
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 28/09/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

#import "OBDragDropManager.h"

@interface MockOBOvum : OBOvum

- (void)setOBOvumLiveStreamDataObject:(MZItem *)item withIndex:(NSInteger)index;
- (void)setOBOvumPortfolioDataObject:(MZItem *)item withIndex:(NSInteger)index;

@end
