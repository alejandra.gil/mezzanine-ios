//
//  MezzanineMainViewController_Tests.m
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 03/10/2016.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MezzanineMainViewController.h"
#import "MezzanineMainViewController_Private.h"

#import "MockSystemModel.h"
#import "MockMezzanineNavigationController.h"

#import "Mezzanine-Swift.h"

@interface MezzanineMainViewController_Tests : XCTestCase

@property (nonatomic, strong) MezzanineMainViewController *controller;
@property (nonatomic, strong) MockSystemModel *systemModelFactory;

@end

@implementation MezzanineMainViewController_Tests

- (void)setUp
{
  [super setUp];
  
  _systemModelFactory = [MockSystemModel new];
  
  _controller = [[MezzanineMainViewController alloc] initWithNibName:@"MezzanineMainViewController" bundle:nil];
  _controller.workspace = self.systemModelFactory.currentWorkspace;
  _controller.systemModel = self.systemModelFactory.systemModel;
  
  [MezzanineAppContext currentContext].mainNavController = [[MockMezzanineNavigationController alloc] initWithRootViewController:_controller];
  [MezzanineAppContext currentContext].currentCommunicator = [MZCommunicator new];
  
  [_controller view];
}


- (void)tearDown
{
  [super tearDown];
  
  [[MezzanineAppContext currentContext].mainNavController popToRootViewControllerAnimated:NO];
  [UIApplication sharedApplication].statusBarHidden = NO;
}

@end
