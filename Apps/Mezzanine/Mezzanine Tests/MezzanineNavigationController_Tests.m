//
//  MezzanineNavigationController_Tests.m
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 07/11/2016.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MezzanineNavigationController.h"
#import "MezzanineConnectionViewController.h"
#import "AssetViewController.h"
#import <UIKit/UIGestureRecognizerSubclass.h>
#import "Mezzanine-Swift.h"

@interface MezzanineNavigationController_Tests : XCTestCase

@property (nonnull, strong) MezzanineNavigationController *navigationController;


@end

@implementation MezzanineNavigationController_Tests

- (void)setUp
{
  [super setUp];
  
  _navigationController = [[MezzanineNavigationController alloc] initWithRootViewController:[UIViewController new]];
  [MezzanineAppContext currentContext].window.rootViewController = _navigationController;
  [_navigationController view];
}


- (void)tearDown
{
  [super tearDown];
}


- (void)testMezzanineConnectionViewControllerCantPop
{
  [_navigationController pushViewController:[MezzanineConnectionViewController new] animated:NO];
  UIGestureRecognizer *recognizer = [_navigationController.view.gestureRecognizers firstObject];
  recognizer.state = UIGestureRecognizerStateBegan;
  XCTAssertTrue(recognizer.state = UIGestureRecognizerStateFailed);
}


- (void)testMezzanineRootViewControllerCantPop
{
  [_navigationController pushViewController:[MezzanineRootViewController new] animated:NO];
  UIGestureRecognizer *recognizer = [_navigationController.view.gestureRecognizers firstObject];
  recognizer.state = UIGestureRecognizerStateBegan;
  
  XCTAssertTrue(recognizer.state = UIGestureRecognizerStateFailed);
}


- (void)testAssetViewControllerCantPop
{
  [_navigationController pushViewController:[AssetViewController new] animated:NO];
  UIGestureRecognizer *recognizer = [_navigationController.view.gestureRecognizers firstObject];
  recognizer.state = UIGestureRecognizerStateBegan;
  
  XCTAssertTrue(recognizer.state = UIGestureRecognizerStateFailed);
}


- (void)testPushedViewControllerCanPop
{
  [_navigationController pushViewController:[UIViewController new] animated:NO];
  UIGestureRecognizer *recognizer = [_navigationController.view.gestureRecognizers firstObject];
  recognizer.state = UIGestureRecognizerStateBegan;
  
  XCTAssertTrue(recognizer.state = UIGestureRecognizerStateBegan);
}


- (void)testModalViewControllerCanPop
{
  [_navigationController presentViewController:[UIViewController new] animated:NO completion:nil];
  UIGestureRecognizer *recognizer = [_navigationController.view.gestureRecognizers firstObject];
  recognizer.state = UIGestureRecognizerStateBegan;
  
  XCTAssertTrue(recognizer.state = UIGestureRecognizerStateBegan);
}

@end
