//
//  UIImage+Additions.m
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 14/10/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

#import "UIImage+Additions.h"

@implementation UIImage (Additions)


+ (UIImage *)imageOfSize:(CGSize)size filledWithColor:(UIColor *)color {
  UIGraphicsBeginImageContextWithOptions(size, NO, 0.0);
  CGContextRef context = UIGraphicsGetCurrentContext();
  CGRect rect = (CGRect){CGPointZero, size};
  CGContextFillRect(context, rect);
  UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  return image;
}

@end
