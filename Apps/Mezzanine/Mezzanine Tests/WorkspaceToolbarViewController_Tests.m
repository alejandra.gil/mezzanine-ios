//
//  WorkspaceToolbarViewController_Tests.m
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 04/11/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "WorkspaceToolbarViewController.h"
#import "WorkspaceToolbarViewController_Private.h"
#import "UIViewController+FPPopover.h"

#import "MockSystemModel.h"
#import "MezzanineAppContext.h"
#import "WorkspaceMenuViewController.h"
#import "MezzanineMainViewController.h"

#import "OCMock.h"
#import "UIDevice+Machine.h"
#import "UITraitCollection+Additions.h"
#import "Mezzanine-Swift.h"

@interface MockMZCommunicator : MZCommunicator

@property (nonatomic, assign) BOOL isARemoteParticipationConnection;

@end


@interface WorkspaceToolbarViewController_Tests : XCTestCase

@property (nonatomic, strong) WorkspaceToolbarViewController *controller;
@property (nonatomic, assign) BOOL isRegular;

@end

static NSString *WorkspaceName = @"workspace";

@implementation WorkspaceToolbarViewController_Tests

- (void)setUp {
  [super setUp];

  MezzanineAppContext *appContext = [MezzanineAppContext currentContext];
  MZCommunicator *communicator = [MZCommunicator new];
  
  MZSystemModel *systemModel = [MZSystemModel new];
  systemModel.myMezzanine.version = @"3.13";
  systemModel.myMezzanine.name = @"myMezzanine";

  MZWorkspace *workspace = [MZWorkspace new];
  workspace.name = WorkspaceName;
  
  _controller = [[WorkspaceToolbarViewController alloc] initWithNibName:@"WorkspaceToolbarViewController" bundle:nil];
  
  
  _controller.systemModel = systemModel;
  _controller.systemModel.currentWorkspace = workspace;
  _controller.communicator = communicator;
  _controller.appContext = appContext;
  
  appContext.window.rootViewController = _controller;
  
  _isRegular = _controller.traitCollection.isRegularWidth && _controller.traitCollection.isRegularHeight;

}


- (void)tearDown
{
  _controller.systemModel = nil;
  _controller.systemModel.currentWorkspace = nil;
  _controller.communicator = nil;

  [MezzanineAppContext currentContext].window.rootViewController = [MezzanineNavigationController new];

  [super tearDown];
}


#pragma mark - Outlets 

- (void)testWorkspaceNameButtonOutletNotSaved
{
  [self startWithInfopresence:NO];
  _controller.systemModel.currentWorkspace.isSaved = NO;
  
  XCTAssertNotNil(_controller.workspaceNameButton);
  XCTAssertTrue([[_controller.workspaceNameButton titleForState:UIControlStateNormal] isEqualToString:NSLocalizedString(@"Workspace Unsaved Button Title", nil)]);
  NSArray *actionsArray = [_controller.workspaceNameButton actionsForTarget:_controller forControlEvent:UIControlEventTouchUpInside];
  XCTAssertGreaterThan([actionsArray count], 0);
  XCTAssertTrue([actionsArray containsObject:@"workspaceDetails:"]);
}


- (void)testWorkspaceNameButtonOutletSaved
{
  [self startWithInfopresence:NO];
  _controller.systemModel.currentWorkspace.isSaved = YES;
  
  XCTAssertNotNil(_controller.workspaceNameButton);
  XCTAssertTrue([[_controller.workspaceNameButton titleForState:UIControlStateNormal] isEqualToString:WorkspaceName]);
  NSArray *actionsArray = [_controller.workspaceNameButton actionsForTarget:_controller forControlEvent:UIControlEventTouchUpInside];
  XCTAssertGreaterThan([actionsArray count], 0);
  XCTAssertTrue([actionsArray containsObject:@"workspaceDetails:"]);
}


- (void)testWorkspaceLeaveButton
{
  [self startWithInfopresence:NO];
  
  XCTAssertNotNil(_controller.participantsButton);
  XCTAssertNotNil(_controller.leaveButton);
  XCTAssertTrue([[_controller.leaveButton titleForState:UIControlStateNormal] isEqualToString:@"Leave"]);
  NSArray *actionsArray = [_controller.leaveButton actionsForTarget:_controller forControlEvent:UIControlEventTouchUpInside];
  XCTAssertGreaterThan([actionsArray count], 0);
  XCTAssertTrue([actionsArray containsObject:@"mezzanineDetails:"]);
}


- (void)testWorkspaceSessionButton
{
  [self startWithInfopresence:YES];

  XCTAssertNotNil(_controller.participantsButton);
  XCTAssertTrue([_controller.participantsButton titleForState:UIControlStateNormal] == nil);
  NSArray *actionsArray =actionsArray = [_controller.participantsButton actionsForTarget:_controller forControlEvent:UIControlEventTouchUpInside];
  
  XCTAssertGreaterThan([actionsArray count], 0);
  XCTAssertTrue([actionsArray containsObject:@"participantsList:"]);
}


- (void)testChangeWorkspaceName
{
  [self startWithInfopresence:NO];
  XCTAssertNotNil(_controller.workspaceNameButton);
  
  _controller.systemModel.currentWorkspace.isSaved = NO;
  XCTAssertTrue([[_controller.workspaceNameButton titleForState:UIControlStateNormal] isEqualToString:NSLocalizedString(@"Workspace Unsaved Button Title", nil)]);
  
  _controller.systemModel.currentWorkspace.name = @"currentWorkspaceName";
  _controller.systemModel.currentWorkspace.isSaved = YES;
  
  XCTAssertTrue([[_controller.workspaceNameButton titleForState:UIControlStateNormal] isEqualToString:@"currentWorkspaceName"]);
  
}


- (void)testWorkspaceIsLoading
{
  [self startWithInfopresence:YES];
  _controller.systemModel.currentWorkspace.isLoading = YES;
}


#pragma mark - Actions

- (void)testWorkspaceDetails
{
  [_controller workspaceDetails:nil];
  
  XCTAssertNotNil(_controller.currentPopover);
  XCTAssertTrue([_controller.currentPopover.viewController isKindOfClass:[WorkspaceMenuViewController class]]);
}


- (void)testParticipantsList
{
  [self startWithInfopresence:YES];

  id toolbarDelegate = [OCMockObject niceMockForProtocol:@protocol(WorkspaceToolbarViewControllerDelegate)];
  _controller.delegate = toolbarDelegate;
  
  [[toolbarDelegate expect] workspaceToolbarOpenInfopresence];

  [_controller participantsList:nil];

  [toolbarDelegate verify];
}

- (void)testMezzanineNameLocalConnection
{
  MockMZCommunicator *mockCommunicator = [MockMZCommunicator new];
  mockCommunicator.isARemoteParticipationConnection = NO;
  _controller.communicator = mockCommunicator;
  
  [_controller updateMezzanineName];
  XCTAssertTrue([_controller.mezzanineRoomLabel.text isEqualToString:_controller.systemModel.myMezzanine.name]);
}

- (void)testMezzanineNameRemoteConnection
{
  MockMZCommunicator *mockCommunicator = [MockMZCommunicator new];
  mockCommunicator.isARemoteParticipationConnection = YES;
  _controller.communicator = mockCommunicator;

  [_controller updateMezzanineName];
  XCTAssertTrue([_controller.mezzanineRoomLabel.text isEqualToString:@"Mezzanine"]);
}


#pragma mark - Helpers

- (void)startWithInfopresence:(BOOL)infopresenceEnabled
{
  _controller.systemModel.infopresence.infopresenceInstalled = infopresenceEnabled;
  [_controller viewDidLayoutSubviews];
}


- (NSString *)mezzNameFromUid:(NSString *)uid
{
  return [NSString stringWithFormat:@"remoteMezz-%@", uid];

}


- (void)addRemoteMezz:(NSString *)uid
{
  MZRemoteMezz *remoteMezz = [MZRemoteMezz new];
  remoteMezz.uid = uid;
  remoteMezz.name = [self mezzNameFromUid:uid];
  [_controller.systemModel.remoteMezzes addObject:remoteMezz];
}


- (MZInfopresenceCall *)createInfopresenceJoinRequestCallWithUid:(NSString *)uid
{
  MZInfopresenceCall *infopresenceCall = [MZInfopresenceCall new];
  infopresenceCall.uid = uid;
  infopresenceCall.type = MZInfopresenceCallTypeJoinRequest;

  [self addRemoteMezz:uid];

  return infopresenceCall;
}


- (MZInfopresenceCall *)createInfopresenceInviteCallWithUid:(NSString *)uid
{
  MZInfopresenceCall *infopresenceCall = [MZInfopresenceCall new];
  infopresenceCall.uid = uid;
  infopresenceCall.type = MZInfopresenceCallTypeInvite;

  [self addRemoteMezz:uid];

  return infopresenceCall;
}


- (void)removeInfopresenceCall:(MZInfopresenceCall *)infopresenceCall
{
  [_controller.systemModel.infopresence removeOutgoingCallsAtIndexes:[NSIndexSet indexSetWithIndex:0]];
}


@end
