//
//  MezzanineExportManager_Tests.m
//  Mezzanine
//
//  Created by miguel on 26/11/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MezzanineExportManager.h"
#import "MezzanineExportManager_Private.h"
#import "MezzanineStyleSheet.h"

@interface MezzanineExportManager_Tests : XCTestCase
{
  MezzanineExportManager *_exportManager;
}

@end

@implementation MezzanineExportManager_Tests

- (void)setUp
{
  [super setUp];
  _exportManager = [MezzanineExportManager new];
}

- (void)tearDown
{
  // Dismiss remaining alerts
  UIAlertController *alertController = [_exportManager valueForKey:@"exportCompleteAlertController"];
  if (alertController)
    [alertController dismissViewControllerAnimated:NO completion:nil];

  UIViewController *viewController = [UIApplication sharedApplication].keyWindow.rootViewController.presentedViewController;
  if ([viewController isKindOfClass:[UIAlertController class]])
    [(UIAlertController *)viewController dismissViewControllerAnimated:NO completion:nil];

  [super tearDown];
}

#pragma mark - Upload

- (void)testCanUploadFileCorrectSize
{
  
}


#pragma mark - Observation changes

- (void)testObservingExportInfoState
{
  MZExportInfo *exportInfo = [MZExportInfo new];

  id observation = [exportInfo observationInfo];
  id observer = [[[observation valueForKey:@"_observances"] firstObject] valueForKey:@"_observer"] ;
  XCTAssertNil(observer);

  _exportManager.portfolioExportInfo = exportInfo;
  observation = [exportInfo observationInfo];
  observer = [[[observation valueForKey:@"_observances"] firstObject] valueForKey:@"_observer"] ;
  XCTAssertEqualObjects(observer, _exportManager);

  _exportManager.portfolioExportInfo = nil;
  observation = [exportInfo observationInfo];
  observer = [[[observation valueForKey:@"_observances"] firstObject] valueForKey:@"_observer"] ;
  XCTAssertNil(observer);
}

- (void)testExportInfoStateReadyUpdateAndFileSizeIsAboveMaximum
{
  MZExportInfo *exportInfo = [MZExportInfo new];
  exportInfo.filesizeInBytes = 10;
  _exportManager.portfolioExportInfo = exportInfo;
  _exportManager.maximumFileSizeInBytes = 5;

  _exportManager.portfolioExportInfo.state = MZExportStateReady;

  UIAlertController *alertController = [_exportManager valueForKey:@"exportCompleteAlertController"];
  XCTAssertEqual(alertController.actions.count, 2, @"Alert should contain only 2 actions (Cancel and Open In...)");
}

- (void)testExportInfoStateReadyUpdateAndFileSizeIsBelowMaximum
{
  MZExportInfo *exportInfo = [MZExportInfo new];
  exportInfo.filesizeInBytes = 10;
  _exportManager.portfolioExportInfo = exportInfo;
  _exportManager.maximumFileSizeInBytes = 15;

  _exportManager.portfolioExportInfo.state = MZExportStateReady;

  UIAlertController *alertController = [_exportManager valueForKey:@"exportCompleteAlertController"];
  XCTAssertEqual(alertController.actions.count, 3, @"Alert should contain 3 actions (Cancel, Preview and Open In...)");
  XCTAssertEqual(exportInfo.state, MZExportStateNone, @"After finishing the export the state should go back to None");
}

- (void)testExportInfoStateFailed
{
  MZExportInfo *exportInfo = [MZExportInfo new];
  exportInfo.errorMessage = @"Error Test Message";
  exportInfo.errorTitle = @"Error Test Title";

  _exportManager.portfolioExportInfo = exportInfo;

  _exportManager.portfolioExportInfo.state = MZExportStateFailed;

  UIAlertController *alertController = [_exportManager valueForKey:@"exportCompleteAlertController"];
  XCTAssertEqual(alertController.actions.count, 1, @"Alert should contain only 1 button (Cancel)");
  XCTAssertEqual(alertController.title, exportInfo.errorTitle, @"Alert error title should be %@ but it is %@", exportInfo.errorTitle, alertController.title);
  XCTAssertEqual(alertController.message, exportInfo.errorMessage, @"Alert error message should be %@ but it is %@", exportInfo.errorMessage, alertController.message);

  XCTAssertEqual(exportInfo.state, MZExportStateNone, @"After finishing the export the state should go back to None");
}


#pragma mark - Open In Tests

- (void)testLoadOpenInMenuForFile
{
  NSString *testFilePath = @"testFilePath";
  [_exportManager.filesToPreview addObject:testFilePath];
  [_exportManager loadOpenInMenuForFile:testFilePath];

  UIDocumentInteractionController *openInDocInteractionController = [_exportManager valueForKey:@"openInDocInteractionController"];
  XCTAssertNotNil(openInDocInteractionController, @"A new UIDocumentInteractionController should have been instantiated");
  XCTAssertTrue([openInDocInteractionController.URL.relativeString isEqualToString:testFilePath], @"URL should have been generated from the filepath");
}


- (void)testPresentAlertForNoPDFReaderAppAvailable
{
  _exportManager.openInDocInteractionController = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath: @"testFilePath"]];
  [_exportManager presentAlertForNoPDFReaderAppAvailable];

  XCTAssertNil(_exportManager.openInDocInteractionController, @"The openInDocInteractionController should be nil");
}


#pragma mark QLPreviewControllerDataSource methods

- (void)testNumberOfPreviewItemsInPreviewController
{
  QLPreviewController *preview = [[QLPreviewController alloc] init];

  [_exportManager.filesToPreview addObject:@"firstFilePath"];
  XCTAssertEqual([_exportManager numberOfPreviewItemsInPreviewController:preview], 1, @"There should be 1 file");

  [_exportManager.filesToPreview addObject:@"secondFilePath"];
  XCTAssertEqual([_exportManager numberOfPreviewItemsInPreviewController:preview], 2, @"There should be 2 files");

  [_exportManager.filesToPreview removeObjectAtIndex:0];
  XCTAssertEqual([_exportManager numberOfPreviewItemsInPreviewController:preview], 1, @"There should be 1 file");
}

- (void)testPreviewControllerPreviewItemAtIndex
{
  QLPreviewController *preview = [[QLPreviewController alloc] init];

  [_exportManager.filesToPreview addObject:@"firstFilePath"];
  [_exportManager.filesToPreview addObject:@"secondFilePath"];

  XCTAssertEqualObjects([(NSURL *)[_exportManager previewController:preview previewItemAtIndex:0] relativeString], [_exportManager.filesToPreview objectAtIndex:0], @"URL should have been generated from the filepath");
  XCTAssertEqualObjects([(NSURL *)[_exportManager previewController:preview previewItemAtIndex:1] relativeString], [_exportManager.filesToPreview objectAtIndex:1], @"URL should have been generated from the filepath");
  XCTAssertNotEqualObjects([(NSURL *)[_exportManager previewController:preview previewItemAtIndex:0] relativeString], [_exportManager.filesToPreview objectAtIndex:1], @"URL should be different for different indexes and files");
  XCTAssertNotEqualObjects([(NSURL *)[_exportManager previewController:preview previewItemAtIndex:1] relativeString], [_exportManager.filesToPreview objectAtIndex:0], @"URL should be different for different indexes and files");
}


#pragma mark - QLPreviewControllerDelegate Tests

- (void)testPreviewControllerDidDismiss
{
  NSString *testFolderPath = @"/tmp/tests";
  NSString *testFilePath = @"/tmp/tests/Test-Image.png";
  [_exportManager.filesToPreview addObject:testFilePath];

  _exportManager.preview = [[QLPreviewController alloc] init];

  NSFileManager *fileManager = [NSFileManager defaultManager];
  if (![fileManager fileExistsAtPath:testFolderPath])
  {
    NSError *error = nil;
    [fileManager createDirectoryAtPath:testFolderPath withIntermediateDirectories:YES attributes:nil error:&error];
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    NSString *path = [bundle pathForResource:@"Test-Image-Mezzanine" ofType:@"png"];

    [fileManager copyItemAtPath:path toPath:testFilePath error:&error];
    XCTAssertTrue([fileManager fileExistsAtPath:testFilePath], @"Test image should be copied to destination path to start the test.");
  }

  [_exportManager previewControllerDidDismiss:_exportManager.preview];

  XCTAssertNil(_exportManager.preview, @"The QL Preview controller should be nil");

  XCTAssertEqual(_exportManager.filesToPreview.count, 0, @"files to preview should be 0");
  XCTAssertFalse([fileManager fileExistsAtPath:testFilePath], @"Test image should have been removed.");

  NSError *error = nil;
  [fileManager removeItemAtPath:testFolderPath error:&error];
  XCTAssertFalse([fileManager fileExistsAtPath:testFolderPath], @"Test folder path should have been removed to clean up things after the test.");
}


#pragma mark - Notifications

- (void)testRotateDeviceDocumentInteractionControllerNil
{
  _exportManager.openInDocInteractionController = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath: @"testFilePath"]];
  [_exportManager documentInteractionControllerWillPresentOpenInMenu:_exportManager.openInDocInteractionController];
  
  [[NSNotificationCenter defaultCenter] postNotificationName:UIDeviceOrientationDidChangeNotification object:nil];
  
  XCTAssertNil(_exportManager.openInDocInteractionController, @"The openInDocInteractionController should be nil");
}


#pragma mark - UIDocumentInteractionControllerDelegate Tests

- (void)testDocumentInteractionControllerWillPresentOpenInMenu
{
  _exportManager.openInDocInteractionController = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath: @"testFilePath"]];
  [_exportManager documentInteractionControllerWillPresentOpenInMenu:_exportManager.openInDocInteractionController];
  
  XCTAssertEqual([UIButton appearance].tintColor, [MezzanineStyleSheet sharedStyleSheet].defaultSystemBlue, @"UIButton appearance should set to default OS blue");
}


- (void)testDocumentInteractionControllerDidDismissOpenInMenu
{
  _exportManager.openInDocInteractionController = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath: @"testFilePath"]];
  [_exportManager documentInteractionControllerDidDismissOpenInMenu:_exportManager.openInDocInteractionController];

  XCTAssertNil(_exportManager.openInDocInteractionController, @"The openInDocInteractionController should be nil");
  XCTAssertEqual([UIButton appearance].tintColor, [UIColor whiteColor], @"UIButton appearance should be back to white");
}

- (void)testDocumentInteractionControllerDidEndSendingToApplication
{
  _exportManager.openInDocInteractionController = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath: @"testFilePath"]];
  [_exportManager documentInteractionController:_exportManager.openInDocInteractionController didEndSendingToApplication:nil];

  XCTAssertNil(_exportManager.openInDocInteractionController, @"The openInDocInteractionController should be nil");
}


@end
