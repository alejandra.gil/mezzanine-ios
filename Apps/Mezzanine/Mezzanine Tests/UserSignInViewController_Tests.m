//
//  UserSignInViewController_Tests.m
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 15/10/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import "UserSignInViewController.h"
#import "MezzanineAppContext.h"
#import "MezzanineStyleSheet.h"
#import "UIDevice+Machine.h"
#import "UITraitCollection+Additions.h"

@interface UserSignInViewController_Tests : XCTestCase

@property (nonatomic, strong) UserSignInViewController *controller;


@end

@implementation UserSignInViewController_Tests

- (void)setUp {
  [super setUp];
  
  self.controller = [[UserSignInViewController alloc] initWithNibName:@"UserSignInViewController" bundle:nil];

  UIWindow *window = [[UIApplication sharedApplication].delegate window];

  MezzanineAppModel *appModel = [MezzanineAppModel new];
  MZSystemModel *systemModel = appModel.systemModel;
  MZCommunicator *communicator = [MZCommunicator new];
  communicator.systemModel = systemModel;
  communicator.systemModel.myMezzanine.version = @"3.13";

  window.rootViewController = self.controller;
}


- (void)tearDown
{
  [super tearDown];
}


#pragma mark - IBOutlets

- (void)testUsernameFieldOutlet
{
  XCTAssertNotNil(self.controller.usernameField);
  XCTAssertTrue([self.controller.usernameField.text length] == 0);
  XCTAssertTrue([self.controller.usernameField.placeholder isEqualToString:@"Username"]);
}


- (void)testPasswordFieldOutlet
{
  XCTAssertNotNil(self.controller.passwordField);
  XCTAssertTrue([self.controller.passwordField.text length] == 0);
  XCTAssertTrue([self.controller.passwordField.placeholder isEqualToString:@"Password"]);
}


- (void)testSignInLabelOutlet
{
  XCTAssertNotNil(self.controller.signInLabel);
  XCTAssertTrue([self.controller.signInLabel.text isEqualToString:@"Sign in to create or access your private workspaces"]);
}


- (void)testSignInButtonOutlet
{
  XCTAssertNotNil(self.controller.signInButton);
  XCTAssertTrue([[self.controller.signInButton titleForState:UIControlStateNormal] isEqualToString:@"Sign In"]);
  NSArray *actionsArray = [self.controller.signInButton actionsForTarget:self.controller forControlEvent:UIControlEventTouchUpInside];
  XCTAssertGreaterThan([actionsArray count], 0);
  XCTAssertTrue([actionsArray containsObject:@"signIn:"]);
}


- (void)testTopConstraintOutlet
{
  XCTAssertNotNil(self.controller.topConstraint);
}


- (void)testScrollViewBottomConstraintOutlet
{
  XCTAssertNotNil(self.controller.scrollViewBottomConstraint);
}


#pragma mark - Actions

- (void)testSignInSuccess
{
  self.controller.usernameField.text = @"username";
  self.controller.passwordField.text = @"oblongoblong";
  
  [self.controller signIn:nil];

  XCTAssertTrue(self.controller.presentedViewController  == nil);
}

- (void)testSignInFail
{
  XCTAssertTrue(self.controller.presentedViewController  == nil);
 
  [self.controller signIn:nil];
  XCTAssertTrue([self.controller.presentedViewController isKindOfClass:[UIAlertController class]]);
}


#pragma mark - UITextFieldDelegate

- (void)testTextFieldReturnUsername
{
  BOOL shouldReturn = [self.controller textFieldShouldReturn:self.controller.usernameField];

  XCTAssertTrue([self.controller.passwordField isFirstResponder]);
  XCTAssertTrue(shouldReturn);
}


- (void)testTextFieldReturnPassword
{
  id controllerMock = [OCMockObject partialMockForObject:self.controller];
  [[controllerMock expect] signIn:nil];
  
  BOOL shouldReturn = [self.controller textFieldShouldReturn:self.controller.passwordField];
  
  [controllerMock verify];
  XCTAssertTrue(shouldReturn);
}

#pragma mark - View style

- (void)testStyle
{
  MezzanineStyleSheet *styleSheet = [MezzanineStyleSheet sharedStyleSheet];

  // Style is set here since traitCollection is still unknown for viewDidLoad
  [_controller viewDidAppear:NO];
  
  if (_controller.traitCollection.isRegularWidth && _controller.traitCollection.isRegularHeight)
  {
    XCTAssertTrue([_controller.view.backgroundColor isEqual:[UIColor whiteColor]]);
    XCTAssertTrue([_controller.signInLabel.textColor isEqual:styleSheet.grey90Color]);
  } else
  {
    XCTAssertTrue([_controller.view.backgroundColor isEqual:styleSheet.darkestFillColor]);
    XCTAssertTrue([_controller.signInLabel.textColor isEqual:[UIColor whiteColor]]);
  }
}

@end
