//
//  MockOBOvum.m
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 28/09/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

#import "MockOBOvum.h"

@implementation MockOBOvum

- (void)setOBOvumLiveStreamDataObject:(MZItem *)item withIndex:(NSInteger)index
{
  item.uid = [NSString stringWithFormat:@"mzitem-uid-%li", (long)index];
  item.index = index;
  item.contentSource = @"vi-test-item"; // isVideo == YES

  self.dataObject = item;
}


- (void)setOBOvumPortfolioDataObject:(MZItem *)item withIndex:(NSInteger)index
{
  item.uid = [NSString stringWithFormat:@"mzitem-uid-%li", (long)index];
  item.index = index;
  item.contentSource = @"as-test-item"; // isVideo == NO

  self.dataObject = item;
}

@end
