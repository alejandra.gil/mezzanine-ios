//
//  ParticipantsRosterTableViewCell_Tests.swift
//  Mezzanine Tests
//
//  Created by Ivan Bella Lopez on 08/10/2018.
//  Copyright © 2018 Oblong Industries. All rights reserved.
//

import UIKit
import XCTest
@testable import Mezzanine

class ParticipantsRosterTableViewCell_Tests: XCTestCase {

  var cell: ParticipantsRosterTableViewCell?
  let styleSheet = MezzanineStyleSheet()

  override func setUp() {
    super.setUp()

    let nib = UINib(nibName: "ParticipantsRosterTableViewCell", bundle: nil)
    let objects = nib.instantiate(withOwner: nil, options: nil)
    cell = (objects[0] as! ParticipantsRosterTableViewCell)
  }

  override func tearDown() {
    super.tearDown()
  }


  func testParticipantIPhone() {
    let participant = createParticipant(type: "iPhone");
    cell?.participant = participant
    XCTAssertEqual(cell!.participantNameLabel.text, participant.displayName)

    cell?.roomType = MZMezzanineRoomType.local
    cell?.updateParticipantTypeImageView()
    XCTAssertTrue(cell!.participantTypeImageView.accessibilityIdentifier == "participant-inroom")

    cell?.roomType = MZMezzanineRoomType.remote
    cell?.updateParticipantTypeImageView()
    XCTAssertTrue(cell!.participantTypeImageView.accessibilityIdentifier == "participant-inroom")

    cell?.roomType = MZMezzanineRoomType.cloud
    cell?.updateParticipantTypeImageView()
    XCTAssertTrue(cell!.participantTypeImageView.accessibilityIdentifier == "participant-mobile")
  }

  func testParticipantIpad() {
    let participant = createParticipant(type: "iPad");
    cell?.participant = participant
    XCTAssertEqual(cell!.participantNameLabel.text, participant.displayName)

    cell?.roomType = MZMezzanineRoomType.local
    cell?.updateParticipantTypeImageView()
    XCTAssertTrue(cell!.participantTypeImageView.accessibilityIdentifier == "participant-inroom")

    cell?.roomType = MZMezzanineRoomType.remote
    cell?.updateParticipantTypeImageView()
    XCTAssertTrue(cell!.participantTypeImageView.accessibilityIdentifier == "participant-inroom")

    cell?.roomType = MZMezzanineRoomType.cloud
    cell?.updateParticipantTypeImageView()
    XCTAssertTrue(cell!.participantTypeImageView.accessibilityIdentifier == "participant-mobile")
  }

  func testParticipantBrowser() {
    let participant = createParticipant(type: "browser");
    cell?.participant = participant
    XCTAssertEqual(cell!.participantNameLabel.text, participant.displayName)

    cell?.roomType = MZMezzanineRoomType.local
    cell?.updateParticipantTypeImageView()
    XCTAssertTrue(cell!.participantTypeImageView.accessibilityIdentifier == "participant-inroom")

    cell?.roomType = MZMezzanineRoomType.remote
    cell?.updateParticipantTypeImageView()
    XCTAssertTrue(cell!.participantTypeImageView.accessibilityIdentifier == "participant-inroom")

    cell?.roomType = MZMezzanineRoomType.cloud
    cell?.updateParticipantTypeImageView()
    XCTAssertTrue(cell!.participantTypeImageView.accessibilityIdentifier == "participant-laptop")
  }


  // MARK: Helpers

  func createParticipant(type: String) -> MZParticipant {
    let participant: MZParticipant = MZParticipant()
    participant.displayName = "participant"
    participant.type = type

    return participant
  }
}

