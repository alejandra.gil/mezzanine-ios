//
//  OBImagePicker_Tests.m
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 24/02/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

//#import "OBImagePickerController.h"
//#import "OBImageSelectionViewController.h"
//#import "OBImageSelectionViewController_Private.h"
//#import "MezzanineAppContext.h"
//#import "OBAlbumsController.h"
//#import "OBImageCell.h"

@interface OBImagePicker_Tests : XCTestCase

//@property (nonatomic, strong) OBImagePickerController *controller;
//
//- (void)cancelSelection:(id)sender;
//- (void)finishSelection;
//- (void)selectAllItems;
//- (void)deselectAllItems;
//
@end

@implementation OBImagePicker_Tests

- (void)setUp {
    [super setUp];
  
//  _controller = [OBImagePickerController new];
//  [MezzanineAppContext currentContext].window.rootViewController = _controller;
//
//  [_controller view];
}


- (void)tearDown
{
//  [MezzanineAppContext currentContext].window.rootViewController = [UINavigationController new];
  [super tearDown];
}


//- (void)testAlbumNavigationButtonsNoPhotos
//{
//  [_controller.topViewController view];
//  
//  XCTAssertNotNil(_controller.topViewController.navigationItem.rightBarButtonItem);
//  UIBarButtonItem *btn = _controller.topViewController.navigationItem.rightBarButtonItem;
//  XCTAssertTrue(btn.action == @selector(cancelSelection:));
//}
//
//
//- (void)testAlbumNavigationButtonsPhotosSelected
//{
//  _controller.selectedItems = [@[@"1"] mutableCopy];
//  [_controller.topViewController viewDidAppear:NO];
//  
//  XCTAssertNotNil(_controller.topViewController.navigationItem.rightBarButtonItem);
//  UIBarButtonItem *btn = _controller.topViewController.navigationItem.rightBarButtonItem;
//  XCTAssertTrue(btn.action == @selector(finishSelection));
//}
//
//
//- (void)testNoAlbums
//{
//  _controller.assetsGroups = [@[] mutableCopy];
//  
//  OBAlbumsController *albumsVC = (OBAlbumsController *)_controller.topViewController;
//  XCTAssertNotNil(albumsVC.tableView);
//  XCTAssertTrue([albumsVC.tableView numberOfRowsInSection:0] == 0);
//}
//
//
//- (void)testLoadedAlbums
//{
//  _controller.assetsGroups = [@[@"1",@"2",@"3",@"4"] mutableCopy];
//  
//  OBAlbumsController *albumsVC = (OBAlbumsController *)_controller.topViewController;
//  XCTAssertNotNil(albumsVC.tableView);
//  XCTAssertTrue([albumsVC.tableView numberOfRowsInSection:0] == 4);
//}
//
//
//- (void)testPhotosNavigationButtons
//{
//  OBImageSelectionViewController *imageSelectionViewController = [[OBImageSelectionViewController alloc] initWithImagePickerController:_controller];
//  [_controller pushViewController:imageSelectionViewController animated:NO];
// 
//  [imageSelectionViewController view];
//
//  XCTAssertTrue([imageSelectionViewController.navigationItem.rightBarButtonItems count] == 2);
//
//  UIBarButtonItem *button1 = [imageSelectionViewController.navigationItem.rightBarButtonItems firstObject];
//  XCTAssertTrue(button1.action == @selector(finishSelection));
//  XCTAssertTrue([button1.title isEqualToString:NSLocalizedString(@"Generic Button Title Cancel", nil)]);
//  
//  UIBarButtonItem *button2 = [imageSelectionViewController.navigationItem.rightBarButtonItems lastObject];
//  XCTAssertTrue(button2.action == @selector(selectAllItems));
//  XCTAssertTrue([button2.title isEqualToString:NSLocalizedString(@"OBImagePicker All Action Button", nil)]);
//
//}
//
//
//
//- (void)testPhotosNavigationButtonsSelectedPhotos
//{
//  OBImageSelectionViewController *imageSelectionViewController = [[OBImageSelectionViewController alloc] initWithImagePickerController:_controller];
//  [_controller pushViewController:imageSelectionViewController animated:NO];
//
//  NSMutableArray *assetsArray = [@[] mutableCopy];
//  for (int c = 0; c < 3; c++) {
//    [assetsArray addObject:[OBImageAsset new]];
//  }
//  
//  imageSelectionViewController.assets = [NSMutableArray arrayWithArray:assetsArray];
//  [imageSelectionViewController view];
//  
//  [imageSelectionViewController selectAllItems];
//  
//  XCTAssertTrue([imageSelectionViewController.navigationItem.rightBarButtonItems count] == 2);
//
//  UIBarButtonItem *button1 = [imageSelectionViewController.navigationItem.rightBarButtonItems firstObject];
//  XCTAssertTrue(button1.action == @selector(finishSelection));
//  XCTAssertTrue([button1.title isEqualToString:NSLocalizedString(@"OBImagePicker Upload Action Button", nil)]);
//  
//  UIBarButtonItem *button2 = [imageSelectionViewController.navigationItem.rightBarButtonItems lastObject];
//  XCTAssertTrue(button2.action == @selector(deselectAllItems));
//  XCTAssertTrue([button2.title isEqualToString:NSLocalizedString(@"OBImagePicker None Action Button", nil)]);
//}
//


#pragma mark - Stub methods

- (void)cancelSelection:(id)sender {}
- (void)finishSelection {}
- (void)selectAllItems {}
- (void)deselectAllItems {}

@end
