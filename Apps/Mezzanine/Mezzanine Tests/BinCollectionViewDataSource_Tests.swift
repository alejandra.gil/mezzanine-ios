//
//  BinCollectionViewDataSource_Tests.swift
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 24/02/2017.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import XCTest

@testable import Mezzanine

class BinCollectionViewDataSource_Tests: XCTestCase {
  
  var controller: UIViewController!
  var collectionView: UICollectionView!
  var dataSource: BinCollectionViewDataSource!
  var mockSystemModel: MockSystemModel!
  
  override func setUp() {
    super.setUp()
    
    mockSystemModel = MockSystemModel()
    dataSource = BinCollectionViewDataSource()
    dataSource.workspace = mockSystemModel.systemModel.currentWorkspace
    
    let collectionViewLayout = BinCollectionViewFlowLayout()
    collectionViewLayout.itemSize = CGSize(width: 180,height: 100)
    
    collectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: 800, height: 200), collectionViewLayout: collectionViewLayout)
    collectionView.contentSize = CGSize(width: 3000, height: 200)
    collectionView.dataSource = dataSource
    collectionView.register(LiveStreamCollectionViewCell.self, forCellWithReuseIdentifier: "liveStreamCellIdentifier")
    collectionView.register(PortfolioCollectionViewCell.self, forCellWithReuseIdentifier: "itemCellIdentifier")
    collectionView.register(PlaceholderCollectionViewCell.self, forCellWithReuseIdentifier: "placeholderCellIdentifier")
    
    controller = UIViewController()
    controller.view.addSubview(collectionView)
    
    UIApplication.shared.keyWindow!.rootViewController = controller
    _ = controller.view
  }
  
  override func tearDown() {
    super.tearDown()
    
  }
  
  // MARK: Number of cells
  
  func testMixAmountNumberOfCellsLocalConnection() {
    dataSource.hasLiveStreamsSection = true
    mockSystemModel.addSlides(toCurrentWorkspace: 4)
    mockSystemModel.addLiveStreams(toCurrentWorkspace: 4)
    
    XCTAssertEqual(collectionView.numberOfSections, 2)
    XCTAssertEqual(collectionView.numberOfItems(inSection: 0), 4)
    XCTAssertEqual(collectionView.numberOfItems(inSection: 1), 4)
    
  }
  
  func testNumberOfItemCellsLocalConnection() {
    dataSource.hasLiveStreamsSection = true
    mockSystemModel.addSlides(toCurrentWorkspace: 4)
    
    XCTAssertEqual(collectionView.numberOfSections, 2)
    XCTAssertEqual(collectionView.numberOfItems(inSection: 0), 4)
  }
  
  func testNumberOfActiveLiveStreamsLocalConnection() {
    dataSource.hasLiveStreamsSection = true
    mockSystemModel.addLiveStreams(toCurrentWorkspace: 4)
    
    XCTAssertEqual(collectionView.numberOfSections, 2)
    XCTAssertEqual(collectionView.numberOfItems(inSection: 0), 4)
    XCTAssertEqual(collectionView.numberOfItems(inSection: 1), 1)
  }
  
  func testNumberOfNonActiveLiveStreamsLocalConnection() {
    dataSource.hasLiveStreamsSection = true
    mockSystemModel.addLiveStreams(toCurrentWorkspace: 4)
    for liveStream in dataSource.workspace.liveStreams {
      (liveStream as AnyObject).setValue(false, forKey: "active")
    }
    
    XCTAssertEqual(collectionView.numberOfSections, 2)
    XCTAssertEqual(collectionView.numberOfItems(inSection: 0), 1)
    XCTAssertEqual(collectionView.numberOfItems(inSection: 1), 1)
  }
  
  func testNumberOfNonLocalLiveStreamsLocalConnection() {
    dataSource.hasLiveStreamsSection = true
    mockSystemModel.addLiveStreams(toCurrentWorkspace: 4)
    for liveStream in dataSource.workspace.liveStreams {
      (liveStream as AnyObject).setValue(false, forKey: "local")
    }
    
    XCTAssertEqual(collectionView.numberOfSections, 2)
    XCTAssertEqual(collectionView.numberOfItems(inSection: 0), 1)
    XCTAssertEqual(collectionView.numberOfItems(inSection: 1), 1)
  }
  
  func testNumberOfNoItemsOrLiveStreamsLocalConnection() {
    dataSource.hasLiveStreamsSection = true
    
    XCTAssertEqual(collectionView.numberOfSections, 2)
    XCTAssertEqual(collectionView.numberOfItems(inSection: 0), 1)
    XCTAssertEqual(collectionView.numberOfItems(inSection: 1), 1)
  }
  
  
  func testMixAmountNumberOfCellsCloudConnection() {
    dataSource.hasLiveStreamsSection = false
    mockSystemModel.addSlides(toCurrentWorkspace: 4)
    mockSystemModel.addLiveStreams(toCurrentWorkspace: 4)
    
    XCTAssertEqual(collectionView.numberOfSections, 1)
    XCTAssertEqual(collectionView.numberOfItems(inSection: 0), 4)
  }
  
  func testNumberOfItemCellsCloudConnection() {
    dataSource.hasLiveStreamsSection = false
    mockSystemModel.addSlides(toCurrentWorkspace: 4)
    
    XCTAssertEqual(collectionView.numberOfSections, 1)
    XCTAssertEqual(collectionView.numberOfItems(inSection: 0), 4)
  }
  
  func testNumberOfActiveLiveStreamsCloudConnection() {
    dataSource.hasLiveStreamsSection = false
    mockSystemModel.addLiveStreams(toCurrentWorkspace: 4)
    
    XCTAssertEqual(collectionView.numberOfSections, 1)
    XCTAssertEqual(collectionView.numberOfItems(inSection: 0), 1)
  }
  
  func testNumberOfNonActiveLiveStreamsCloudConnection() {
    dataSource.hasLiveStreamsSection = false
    mockSystemModel.addLiveStreams(toCurrentWorkspace: 4)
    for liveStream in dataSource.workspace.liveStreams {
      (liveStream as AnyObject).setValue(false, forKey: "active")
    }
    
    XCTAssertEqual(collectionView.numberOfSections, 1)
    XCTAssertEqual(collectionView.numberOfItems(inSection: 0), 1)
  }
  
  func testNumberOfNonLocalLiveStreamsCloudConnection() {
    dataSource.hasLiveStreamsSection = false
    mockSystemModel.addLiveStreams(toCurrentWorkspace: 4)
    for liveStream in dataSource.workspace.liveStreams {
      (liveStream as AnyObject).setValue(false, forKey: "local")
    }
    
    XCTAssertEqual(collectionView.numberOfSections, 1)
    XCTAssertEqual(collectionView.numberOfItems(inSection: 0), 1)
  }
  
  func testNumberOfNoItemsOrLiveStreamsCloudConnection() {
    dataSource.hasLiveStreamsSection = false
    
    XCTAssertEqual(collectionView.numberOfSections, 1)
    XCTAssertEqual(collectionView.numberOfItems(inSection: 0), 1)
  }
  

  // MARK: Type of cells
  
  func testOnlyLiveStreamsCells() {
    mockSystemModel.addLiveStreams(toCurrentWorkspace: 4)
    dataSource.hasLiveStreamsSection = true
    collectionView.layoutIfNeeded()
    
    for index in 0...collectionView.numberOfItems(inSection: 0) - 1 {
      let cell = collectionView.cellForItem(at: IndexPath(item: index, section: 0))
      print(cell is LiveStreamCollectionViewCell)
      XCTAssertTrue(cell is LiveStreamCollectionViewCell)
    }
  }
  
  func testOnlyItemsCells() {
    mockSystemModel.addSlides(toCurrentWorkspace: 4)
    dataSource.hasLiveStreamsSection = false
    collectionView.layoutIfNeeded()
    
    for index in 0...collectionView.numberOfItems(inSection: 0) - 1 {
      let cell = collectionView.cellForItem(at: IndexPath(item: index, section: 0))
      XCTAssertTrue(cell is PortfolioCollectionViewCell)
    }
  }
  
  func testItemsAndLiveStreamsCells() {
    mockSystemModel.addLiveStreams(toCurrentWorkspace: 4)
    mockSystemModel.addSlides(toCurrentWorkspace: 4)
    dataSource.hasLiveStreamsSection = true
    collectionView.layoutIfNeeded()
    
    for index in 0...collectionView.numberOfItems(inSection: 1) - 1 {
      let cell = collectionView.cellForItem(at: IndexPath(item: index, section: 1))
      XCTAssertTrue(cell is PortfolioCollectionViewCell)
    }
    
    for index in 0...collectionView.numberOfItems(inSection: 0) - 1 {
      let cell = collectionView.cellForItem(at: IndexPath(item: index, section: 0))
      XCTAssertTrue(cell is LiveStreamCollectionViewCell)
    }
  }
  
  func testNoItems() {
    dataSource.hasLiveStreamsSection = false
    collectionView.layoutIfNeeded()
    
    let cell = collectionView.cellForItem(at: IndexPath(item: 0, section: 0))
    XCTAssertTrue(cell is PlaceholderCollectionViewCell)
    XCTAssertTrue(collectionView.numberOfSections == 1)

  }
  
  func testNoItemsCloudConnection() {
    mockSystemModel.addLiveStreams(toCurrentWorkspace: 4)
    dataSource.hasLiveStreamsSection = false
    collectionView.layoutIfNeeded()
    
    // Item placeholder should be shown
    XCTAssertTrue(collectionView.numberOfItems(inSection: 0) == 1)
  }
  
  func testCellsCloudConnection() {
    mockSystemModel.addSlides(toCurrentWorkspace: 4)
    dataSource.hasLiveStreamsSection = false
    collectionView.layoutIfNeeded()
    
    for index in 0...collectionView.numberOfItems(inSection: 0) - 1 {
      let cell = collectionView.cellForItem(at: IndexPath(item: index, section: 0))
      XCTAssertTrue(cell is PortfolioCollectionViewCell)
    }
  }
}
