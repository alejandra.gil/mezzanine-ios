//
//  FeldGeometry_Tests.swift
//  Mezzanine
//
//  Created by miguel on 27/2/17.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import XCTest

@testable import Mezzanine

class FeldGeometry_Tests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
  func testRectForFeldForSurfaceWithOneFeld() {

    let systemModel = MockSystemModel().createTestWithSingleFeld()
    let surfaces = systemModel?.surfaces as! NSArray as? [MZSurface]
    let containerRect = CGRect(x: 0, y: 0, width: 496, height: 279)

    let feldGeometry = FeldGeometry()
    feldGeometry.containerRect = containerRect
    feldGeometry.surface = surfaces?.first

    let feldRect = feldGeometry.rectForFeld(surfaces?.first?.felds[0] as! MZFeld)
    XCTAssertTrue(feldRect != CGRect.zero)

    XCTAssertTrue(containerRect.contains(feldRect))
    XCTAssertTrue(feldRect.contains(containerRect))
    XCTAssertTrue(feldRect == containerRect)
  }


  func testRectForFeldForSurfaceWithTriptych() {

    let systemModel = MockSystemModel().createTestWithTriptych()
    let surfaces = (systemModel?.surfaces as! NSArray as? [MZSurface])!
    let containerRect = CGRect(x: 0, y: 0, width: 1208.0, height:250.0)

    let feldGeometry = FeldGeometry()
    feldGeometry.containerRect = containerRect
    feldGeometry.surface = surfaces.first

    let feldRectLeft = feldGeometry.rectForFeld(surfaces.first?.felds[0] as! MZFeld)
    XCTAssertTrue(feldRectLeft != CGRect.zero)
    XCTAssertTrue(containerRect.contains(feldRectLeft))
    XCTAssertFalse(feldRectLeft.contains(containerRect))
    XCTAssertTrue(feldRectLeft == CGRect(x: 0, y: 0, width: 400, height: 225))

    let feldRectMain = feldGeometry.rectForFeld(surfaces.first?.felds[1] as! MZFeld)
    XCTAssertTrue(feldRectMain != CGRect.zero)
    XCTAssertTrue(containerRect.contains(feldRectMain))
    XCTAssertFalse(feldRectMain.contains(containerRect))
    XCTAssertTrue(feldRectMain == CGRect(x: 404, y: 0, width: 400, height: 225))

    let feldRectRight = feldGeometry.rectForFeld(surfaces.first?.felds[2] as! MZFeld)
    XCTAssertTrue(feldRectRight != CGRect.zero)
    XCTAssertTrue(containerRect.contains(feldRectRight))
    XCTAssertFalse(feldRectRight.contains(containerRect))
    XCTAssertTrue(feldRectRight == CGRect(x: 808, y: 0, width: 400, height: 225))
  }


  func testRectForFeldForSurfaceWithDiptych() {

    let systemModel = MockSystemModel().createTestWithDiptych()
    let surfaces = (systemModel?.surfaces as! NSArray as? [MZSurface])!
    let containerRect = CGRect(x: 0, y: 0, width: 804, height: 225)

    let feldGeometry = FeldGeometry()
    feldGeometry.containerRect = containerRect
    feldGeometry.surface = surfaces.first

    let feldRectMain = feldGeometry.rectForFeld(surfaces.first?.felds[0] as! MZFeld)
    XCTAssertTrue(feldRectMain != CGRect.zero)
    XCTAssertTrue(containerRect.contains(feldRectMain))
    XCTAssertFalse(feldRectMain.contains(containerRect))
    XCTAssertTrue(feldRectMain == CGRect(x: 0, y: 0, width: 400, height: 225))

    let feldRectRight = feldGeometry.rectForFeld(surfaces.first?.felds[1] as! MZFeld)
    XCTAssertTrue(feldRectRight != CGRect.zero)
    XCTAssertTrue(containerRect.contains(feldRectRight))
    XCTAssertFalse(feldRectRight.contains(containerRect))
    XCTAssertTrue(feldRectRight == CGRect(x: 404, y: 0, width: 400, height: 225))
  }


  func testRectForFeldForSurfaceWithThreeByTwoSurface() {

    let systemModel = MockSystemModel().createTestWithTwoByThreeSurface()
    let surfaces = (systemModel?.surfaces as! NSArray as? [MZSurface])!
    let containerRect = CGRect(x: 0, y: 0, width: 1208.0, height:460.0)

    let feldGeometry = FeldGeometry()
    feldGeometry.containerRect = containerRect
    feldGeometry.surface = surfaces.first

    let feldRectLeft = feldGeometry.rectForFeld(surfaces.first?.felds[0] as! MZFeld)
    XCTAssertTrue(feldRectLeft != CGRect.zero)
    XCTAssertTrue(containerRect.contains(feldRectLeft))
    XCTAssertFalse(feldRectLeft.contains(containerRect))
    XCTAssertTrue(feldRectLeft == CGRect(x: 0, y: 0, width: 400, height: 225))

    let feldRectMain = feldGeometry.rectForFeld(surfaces.first?.felds[1] as! MZFeld)
    XCTAssertTrue(feldRectMain != CGRect.zero)
    XCTAssertTrue(containerRect.contains(feldRectMain))
    XCTAssertFalse(feldRectMain.contains(containerRect))
    XCTAssertTrue(feldRectMain == CGRect(x: 404, y: 0, width: 400, height: 225))

    let feldRectRight = feldGeometry.rectForFeld(surfaces.first?.felds[2] as! MZFeld)
    XCTAssertTrue(feldRectRight != CGRect.zero)
    XCTAssertTrue(containerRect.contains(feldRectRight))
    XCTAssertFalse(feldRectRight.contains(containerRect))
    XCTAssertTrue(feldRectRight == CGRect(x: 808, y: 0, width: 400, height: 225))

    let bottomFeldRectLeft = feldGeometry.rectForFeld(surfaces.first?.felds[3] as! MZFeld)
    XCTAssertTrue(bottomFeldRectLeft != CGRect.zero)
    XCTAssertTrue(containerRect.contains(bottomFeldRectLeft))
    XCTAssertFalse(bottomFeldRectLeft.contains(containerRect))
    XCTAssertTrue(bottomFeldRectLeft == CGRect(x: 0, y: 229, width: 400, height: 225))

    let bottomFeldRectMain = feldGeometry.rectForFeld(surfaces.first?.felds[4] as! MZFeld)
    XCTAssertTrue(bottomFeldRectMain != CGRect.zero)
    XCTAssertTrue(containerRect.contains(bottomFeldRectMain))
    XCTAssertFalse(bottomFeldRectMain.contains(containerRect))
    XCTAssertTrue(bottomFeldRectMain == CGRect(x: 404, y: 229, width: 400, height: 225))

    let bottomFeldRectRight = feldGeometry.rectForFeld(surfaces.first?.felds[5] as! MZFeld)
    XCTAssertTrue(bottomFeldRectRight != CGRect.zero)
    XCTAssertTrue(containerRect.contains(bottomFeldRectRight))
    XCTAssertFalse(bottomFeldRectRight.contains(containerRect))
    XCTAssertTrue(bottomFeldRectRight == CGRect(x: 808, y: 229, width: 400, height: 225))
  }


  func testClosestFeldWithTwoByThreeSurface() {

    let systemModel = MockSystemModel().createTestWithTwoByThreeSurface()
    let surfaces = (systemModel?.surfaces as! NSArray as? [MZSurface])!
    let containerRect = CGRect(x: 0, y: 0, width: 537, height: 202)

    let feldGeometry = FeldGeometry()
    feldGeometry.containerRect = containerRect
    feldGeometry.surface = surfaces.first

    let topLeftFeld = surfaces.first?.felds[0] as! MZFeld
    let topMiddleFeld = surfaces.first?.felds[1] as! MZFeld
    let topRightFeld = surfaces.first?.felds[2] as! MZFeld
    let bottomLeftFeld = surfaces.first?.felds[3] as! MZFeld
    let bottomMiddleFeld = surfaces.first?.felds[4] as! MZFeld
    let bottomRightFeld = surfaces.first?.felds[5] as! MZFeld

    XCTAssertEqual(topLeftFeld, feldGeometry.closestFeldForLocation(CGPoint(x: 50, y:50)))
    XCTAssertEqual(topLeftFeld, feldGeometry.closestFeldForLocation(CGPoint(x: 177, y:50)))
    XCTAssertEqual(topMiddleFeld, feldGeometry.closestFeldForLocation(CGPoint(x: 181, y:50)))
    XCTAssertEqual(topMiddleFeld, feldGeometry.closestFeldForLocation(CGPoint(x: 220, y:50)))
    XCTAssertEqual(topMiddleFeld, feldGeometry.closestFeldForLocation(CGPoint(x: 358, y:50)))
    XCTAssertEqual(topRightFeld, feldGeometry.closestFeldForLocation(CGPoint(x: 362, y:50)))
    XCTAssertEqual(topRightFeld, feldGeometry.closestFeldForLocation(CGPoint(x: 420, y:50)))
    XCTAssertEqual(topRightFeld, feldGeometry.closestFeldForLocation(CGPoint(x: 620, y:50)))

    XCTAssertEqual(bottomLeftFeld, feldGeometry.closestFeldForLocation(CGPoint(x: 50, y:150)))
    XCTAssertEqual(bottomLeftFeld, feldGeometry.closestFeldForLocation(CGPoint(x: 177, y:150)))
    XCTAssertEqual(bottomMiddleFeld, feldGeometry.closestFeldForLocation(CGPoint(x: 181, y:150)))
    XCTAssertEqual(bottomMiddleFeld, feldGeometry.closestFeldForLocation(CGPoint(x: 220, y:150)))
    XCTAssertEqual(bottomMiddleFeld, feldGeometry.closestFeldForLocation(CGPoint(x: 358, y:150)))
    XCTAssertEqual(bottomRightFeld, feldGeometry.closestFeldForLocation(CGPoint(x: 362, y:150)))
    XCTAssertEqual(bottomRightFeld, feldGeometry.closestFeldForLocation(CGPoint(x: 420, y:150)))
    XCTAssertEqual(bottomRightFeld, feldGeometry.closestFeldForLocation(CGPoint(x: 620, y:150)))

    XCTAssertEqual(topLeftFeld, feldGeometry.closestFeldForLocation(CGPoint(x: 50, y:100)))
    XCTAssertEqual(topLeftFeld, feldGeometry.closestFeldForLocation(CGPoint(x: 177, y:100)))
    XCTAssertEqual(bottomMiddleFeld, feldGeometry.closestFeldForLocation(CGPoint(x: 181, y:102)))
    XCTAssertEqual(topMiddleFeld, feldGeometry.closestFeldForLocation(CGPoint(x: 220, y:100)))
    XCTAssertEqual(topMiddleFeld, feldGeometry.closestFeldForLocation(CGPoint(x: 358, y:100)))
    XCTAssertEqual(bottomRightFeld, feldGeometry.closestFeldForLocation(CGPoint(x: 362, y:102)))
    XCTAssertEqual(topRightFeld, feldGeometry.closestFeldForLocation(CGPoint(x: 420, y:100)))
    XCTAssertEqual(bottomRightFeld, feldGeometry.closestFeldForLocation(CGPoint(x: 620, y:102)))
  }

  func testFeldsContainedInRect() {
    let systemModel = MockSystemModel().createTestWithTwoByThreeSurface()
    let surfaces = (systemModel?.surfaces as! NSArray as? [MZSurface])!
    let containerRect = CGRect(x: 0, y: 0, width: 537, height: 202)

    let feldGeometry = FeldGeometry()
    feldGeometry.containerRect = containerRect
    feldGeometry.surface = surfaces.first

    let topLeftFeld = surfaces.first?.felds[0] as! MZFeld
    let topMiddleFeld = surfaces.first?.felds[1] as! MZFeld
    let topRightFeld = surfaces.first?.felds[2] as! MZFeld
    let bottomLeftFeld = surfaces.first?.felds[3] as! MZFeld
    let bottomMiddleFeld = surfaces.first?.felds[4] as! MZFeld
    let bottomRightFeld = surfaces.first?.felds[5] as! MZFeld

    XCTAssertEqual([topLeftFeld, topMiddleFeld, topRightFeld, bottomLeftFeld, bottomMiddleFeld, bottomRightFeld], feldGeometry.feldsContainedInRect(CGRect(x: 0, y: 0, width: 600, height: 300))!)
    XCTAssertEqual([topLeftFeld, topMiddleFeld], feldGeometry.feldsContainedInRect(CGRect(x: 0, y: 0, width: 420, height: 150))!)
    XCTAssertEqual([], feldGeometry.feldsContainedInRect(CGRect(x: 50, y: 50, width: 120, height: 150))!)
  }

  func testFeldsIntersectingWithRect() {
    let systemModel = MockSystemModel().createTestWithTwoByThreeSurface()
    let surfaces = (systemModel?.surfaces as! NSArray as? [MZSurface])!
    let containerRect = CGRect(x: 0, y: 0, width: 537, height: 202)

    let feldGeometry = FeldGeometry()
    feldGeometry.containerRect = containerRect
    feldGeometry.surface = surfaces.first

    let topLeftFeld = surfaces.first?.felds[0] as! MZFeld
    let topMiddleFeld = surfaces.first?.felds[1] as! MZFeld
    let topRightFeld = surfaces.first?.felds[2] as! MZFeld
    let bottomLeftFeld = surfaces.first?.felds[3] as! MZFeld
    let bottomMiddleFeld = surfaces.first?.felds[4] as! MZFeld
    let bottomRightFeld = surfaces.first?.felds[5] as! MZFeld

    XCTAssertEqual([topLeftFeld, topMiddleFeld, topRightFeld, bottomLeftFeld, bottomMiddleFeld, bottomRightFeld], feldGeometry.feldsIntersectingWithRect(CGRect(x: 0, y: 0, width: 420, height: 150))!)
    XCTAssertEqual([topLeftFeld, bottomLeftFeld], feldGeometry.feldsIntersectingWithRect(CGRect(x: 50, y: 50, width: 120, height: 150))!)
  }

}
