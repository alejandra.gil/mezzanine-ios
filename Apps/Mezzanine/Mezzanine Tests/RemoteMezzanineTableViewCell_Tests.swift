//
//  RemoteMezzanineTableViewCell_Tests.swift
//  Mezzanine
//
//  Created by Zai Chang on 6/29/15.
//  Copyright (c) 2015 Oblong Industries. All rights reserved.
//

import UIKit
import XCTest
@testable import Mezzanine

class RemoteMezzanineTableViewCell_Tests: XCTestCase {

	var dataSource: RemoteMezzaninesTableViewDataSource?
	var cell: RemoteMezzanineTableViewCell?

	override func setUp() {
		super.setUp()
		
		let nib = UINib(nibName: "RemoteMezzanineTableViewCell", bundle: nil)
		let objects = nib.instantiate(withOwner: nil, options: nil)
		cell = (objects[0] as! RemoteMezzanineTableViewCell)
	}
	
	override func tearDown() {
		// Put teardown code here. This method is called after the invocation of each test method in the class.
		super.tearDown()
	}

	func createRemoteMezz() -> MZRemoteMezz {
		let remoteMezz = MZRemoteMezz()
		remoteMezz.uid = "Remote Mezz Uid"
		remoteMezz.name = "Remote Mezz Name"
		remoteMezz.online = true
		remoteMezz.version = "3.0"
    remoteMezz.location = "Remote Mezz Location"
    remoteMezz.collaborationState = .notInCollaboration
		return remoteMezz
	}

	func testDataModel() {
		let remoteMezz = createRemoteMezz()
		cell?.remoteMezz = remoteMezz
		
		// TODO - Fix testing of UITableViewCell and the loading of it from nib in unit tests
		// Testing of optionals using hint from article: http://natashatherobot.com/unit-test-optionals-swift/
		XCTAssertNotNil(cell?.nameLabel, "Name label should not be nil")
		if let nameLabel = cell?.nameLabel {
			XCTAssertEqual(nameLabel.text!, remoteMezz.name, "Name label should show the room name")
		}
		
		XCTAssertNotNil(cell?.locationLabel, "Location label should not be nil")
		if let locationLabel = cell?.locationLabel {
			XCTAssertEqual(locationLabel.text!, remoteMezz.location, "Location label should show the room location")
		}

    XCTAssertNotNil(cell?.statusLabel, "Status label should not be nil")
    if let statusLabel = cell?.statusLabel {
      XCTAssertNil(statusLabel.text, "Status label text should be empty")
    }
	}
	
	func testDataModelObservation() {

    let styleSheet = MezzanineStyleSheet.shared()!

    let remoteMezz = createRemoteMezz()
		cell?.remoteMezz = remoteMezz


    XCTAssertNotNil(cell?.nameLabel, "Name label should not be nil")
    if let nameLabel = cell?.nameLabel {
      XCTAssertEqual(nameLabel.text!, remoteMezz.name, "Name label should show the room name")
      remoteMezz.name = "New Room Name"
      XCTAssertEqual(nameLabel.text!, remoteMezz.name, "Name label should show the new room name")
    }

    XCTAssertNotNil(cell?.locationLabel, "Location label should not be nil")
    if let locationLabel = cell?.locationLabel {
      XCTAssertEqual(locationLabel.text!, remoteMezz.location, "Location label should show the room location")
      remoteMezz.location = "New Mezz Location"
      XCTAssertEqual(locationLabel.text!, remoteMezz.location, "Location label should show the new room location")
    }

    XCTAssertNotNil(cell?.statusLabel, "Status label should not be nil")
    XCTAssertNotNil(cell?.iconModifierImageView, "Icon Modifier ImageView should not be nil")
    if let statusLabel = cell?.statusLabel, let iconModifier = cell?.iconModifierImageView {

      XCTAssertNil(statusLabel.text, "Status label text should be empty")
      XCTAssertNil(iconModifier.image, "Icon modifier be empty")

      remoteMezz.collaborationState = .joining
      XCTAssertEqual(statusLabel.text, "Infopresence Collaboration State Joining".localized, "Status label text should be: "+"Infopresence Collaboration State Joining".localized)
      XCTAssertTrue(image(iconModifier.image!, isEqualTo:UIImage(named: "mobile-infopresence-time-icon.png")!), "Icon modifier should be a clock")
      XCTAssertEqual(statusLabel.textColor, styleSheet.yellowHighlightColor, "Status label text should be yellow")

      remoteMezz.collaborationState = .inCollaboration
      XCTAssertEqual(statusLabel.text, "Infopresence Collaboration State InCollaboration".localized, "Status label text should be: "+"Infopresence Collaboration State InCollaboration".localized)
      XCTAssertTrue(image(iconModifier.image!, isEqualTo:UIImage(named: "mobile-infopresence-checkmark-icon.png")!), "Icon modifier should be a checkmark")
      XCTAssertEqual(statusLabel.textColor, styleSheet.greenHighlightColor, "Status label text should be green")

      remoteMezz.collaborationState = .invited
      XCTAssertEqual(statusLabel.text, "Infopresence Collaboration State Invited".localized, "Status label text should be: "+"Infopresence Collaboration State Invited".localized)
      XCTAssertTrue(image(iconModifier.image!, isEqualTo:UIImage(named: "mobile-infopresence-time-icon.png")!), "Icon modifier should be a clock")
      XCTAssertEqual(statusLabel.textColor, styleSheet.yellowHighlightColor, "Status label text should be yellow")

      remoteMezz.collaborationState = .notInCollaboration
      remoteMezz.online = false
      XCTAssertEqual(statusLabel.text, "Infopresence Collaboration State Offline".localized, "Status label text should be: "+"Infopresence Collaboration State Offline".localized)
      XCTAssertNil(iconModifier.image, "Icon modifier be empty")
      XCTAssertEqual(statusLabel.textColor, styleSheet.mediumFillColor, "Status label text should be gray")
    }
	}


	func testPrepareForReuse() {
		
	}


  // Helper Methods

  func image(_ image1: UIImage, isEqualTo image2: UIImage) -> Bool {
    let data1: Data = UIImagePNGRepresentation(image1)!
    let data2: Data = UIImagePNGRepresentation(image2)!
    return (data1 == data2)
  }

}
