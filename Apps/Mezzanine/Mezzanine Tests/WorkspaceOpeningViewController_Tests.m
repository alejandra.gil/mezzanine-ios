//
//  WorkspaceOpeningViewController_Tests.m
//  Mezzanine
//
//  Created by Miguel Sánchez Valdés on 28/10/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "WorkspaceOpeningViewController.h"
#import "WorkspaceListCell.h"
#import "XCTestCase+Animations.h"
#import "MezzanineStyleSheet.h"

@interface WorkspaceOpeningViewController_Tests : XCTestCase
{
  WorkspaceOpeningViewController *_controller;
}

@end

@implementation WorkspaceOpeningViewController_Tests

- (void)setUp
{
  [super setUp];
  _controller = [WorkspaceOpeningViewController new];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
  [super tearDown];
}

#pragma mark - View Tests

- (void)testOutlets
{
  [_controller loadView];

  XCTAssertNotNil(_controller.cellContainerView, @"cellContainerView outlet should not be nil");
  XCTAssertNotNil(_controller.cellContainerWidthConstraint, @"cellContainerWidthConstraint outlet should not be nil");
  XCTAssertNotNil(_controller.cellContainerHeightConstraint, @"cellContainerHeightConstraint outlet should not be nil");
  XCTAssertNotNil(_controller.label, @"label outlet should not be nil");
}

- (WorkspaceListCell *)workspaceListCellFromController
{
  WorkspaceListCell *workspaceCell = nil;
  for (UIView *view in _controller.cellContainerView.subviews)
  {
    if ([view isKindOfClass:[WorkspaceListCell class]])
      workspaceCell = (WorkspaceListCell *) view;
  }
  return workspaceCell;
}

- (void)testWorkspaceCell
{
  [_controller view];
  WorkspaceListCell *workspaceCell = [self workspaceListCellFromController];

  XCTAssertNotNil(workspaceCell, @"cellContainerView should contain a WorkspaceListCell subview");
  XCTAssertTrue(workspaceCell.isOpeningViewCell, @"workspaceCell should be flag as openingViewCell");
  XCTAssertTrue(workspaceCell.optionsButton.hidden, @"workspaceCell option buttons should be hidden");
}

- (void)testStyle
{
  MezzanineStyleSheet *styleSheet = [MezzanineStyleSheet sharedStyleSheet];

  XCTAssertTrue([_controller.view.backgroundColor isEqual:[styleSheet.selectedBlueColor colorWithAlphaComponent:0.9]]);

  UIView *cellContainerView = [_controller valueForKey:@"cellContainerView"];
  XCTAssertTrue([cellContainerView.backgroundColor isEqual:[UIColor clearColor]]);
}


#pragma mark - Observation

- (void)testSystemModelObservers
{
  MZSystemModel *systemModel = [MZSystemModel new];
  systemModel.currentWorkspace = [MZWorkspace new];

  id observation = [systemModel observationInfo];
  XCTAssertNil(observation, @"The system model properties (currentWorkspace.isLoading) should not be observed yet.");

  _controller.systemModel = systemModel;

  observation = [systemModel observationInfo];
  NSArray *observances = [observation valueForKey:@"_observances"];
  XCTAssertEqual([observances count], 1, @"The system model properties should be observed now.");
  id observer = [observances firstObject];
  XCTAssertEqual([observer valueForKey:@"_observer"], _controller, @"The system model properties should be observed by the controller.");

  _controller.systemModel = nil;
  observation = [systemModel observationInfo];
  XCTAssertNil(observation, @"System model should not be observed anymore.");
}

- (void)testCurrentWorkspaceChangeUpdatesView
{
  [_controller view];
  WorkspaceListCell *workspaceCell = [self workspaceListCellFromController];

  MZSystemModel *systemModel = [MZSystemModel new];
  systemModel.currentWorkspace = [MZWorkspace new];
  systemModel.currentWorkspace.isLoading = YES;

  XCTAssertNil(workspaceCell.workspace, @"Workspace cell should not have any workspace assigned to it.");

  _controller.systemModel = systemModel;

  XCTAssertEqual(workspaceCell.workspace, systemModel.currentWorkspace, @"Workspace cell be the same than the current one that is loading.");
}

@end
