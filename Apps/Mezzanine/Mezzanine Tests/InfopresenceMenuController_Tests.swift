//
//  InfopresenceMenuController_Tests.swift
//  Mezzanine
//
//  Created by miguel on 19/2/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

import UIKit
import XCTest
@testable import Mezzanine

class InfopresenceMenuController_Tests: XCTestCase {

  var controller: InfopresenceMenuController?
  let styleSheet = MezzanineStyleSheet()

  override func setUp() {

    super.setUp()

    let appModel = MezzanineAppModel()
    let systemModel = appModel.systemModel
    let communicator = MZCommunicator()
    
    let workspaceViewController = WorkspaceViewController.init(systemModel: systemModel!, communicator: communicator, delegate: self)

    let window = UIApplication.shared.delegate?.window!
    let navigationController = MezzanineNavigationController.init(rootViewController: workspaceViewController)
    window?.rootViewController = navigationController

    communicator.systemModel = systemModel

    let appContext = MezzanineAppContext()
    MezzanineAppContext.setCurrent(appContext)
    appContext.applicationModel = appModel
    appContext.currentCommunicator = communicator;
    appContext.window = window
    appContext.mainNavController = navigationController
    appContext.mainNavController.delegate = appContext

    controller = InfopresenceMenuController.init(containerViewController: workspaceViewController)
    controller!.systemModel = systemModel
    controller!.communicator = communicator
    controller!.showMenuViewController()
  }

  override func tearDown() {

    super.tearDown()
  }

  func DISABLED_testHideMenuWithMultiplePanels() {

    if controller?.containerViewController!.traitCollection.isRegularWidth() == true && controller?.containerViewController!.traitCollection.isRegularHeight() == true {

      controller?.showInviteMezzaninesMenu()
      var workspaceViewController = self.controller?.containerViewController?.navigationController?.topViewController as! WorkspaceViewController
      XCTAssertNotNil(workspaceViewController)

      controller?.hideInfopresenceMenu()
      workspaceViewController = self.controller?.containerViewController?.navigationController?.topViewController as! WorkspaceViewController
      XCTAssertNotNil(workspaceViewController)
    }
    else
    {
      controller?.showInviteMezzaninesMenu()

      let completionPushExpectation = self.expectation(description: "WaitingForPush")
      let delayInSeconds = 5.0
      let popTime = DispatchTime.now() + Double(Int64(delayInSeconds * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
      DispatchQueue.main.asyncAfter(deadline: popTime)  {

        let infopresenceInviteViewController = self.controller?.containerViewController?.navigationController?.topViewController as! InfopresenceInviteViewController
        XCTAssertNotNil(infopresenceInviteViewController)
        completionPushExpectation.fulfill()
      }
      waitForExpectations(timeout: delayInSeconds, handler: nil)

      controller?.hideInfopresenceMenu()
      let completionPopExpectation = self.expectation(description: "WaitingForPop")
      DispatchQueue.main.asyncAfter(deadline: popTime)  {

        let workspaceViewController = self.controller?.containerViewController?.navigationController?.topViewController as! WorkspaceViewController
        XCTAssertNotNil(workspaceViewController)
        completionPopExpectation.fulfill()
      }
      waitForExpectations(timeout: delayInSeconds, handler: nil)
    }
  }
}

extension InfopresenceMenuController_Tests: WorkspaceViewControllerDelegate {
  func workspaceViewControllerDidStopPresentation(_ workspaceVC: WorkspaceViewController) {}
  
  func workspaceViewControllerDidDraggingWindshieldItem(_ workspaceVC: WorkspaceViewController) {}
  
  func workspaceViewControllerDidStopDraggingWindshieldItem(_ workspaceVC: WorkspaceViewController) {}
  
  func workspaceViewControllerDidToggleZoom(_ workspaceVC: WorkspaceViewController, mode: Bool, animated: Bool) {}
}
