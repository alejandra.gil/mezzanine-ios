//
//  MezzanineSessionViewController_Tests.swift
//  Mezzanine
//
//  Created by miguel on 21/10/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

import XCTest

@testable import Mezzanine

class MezzanineSessionViewController_Tests: XCTestCase {

  var controller: MezzanineSessionViewController?
  var systemModelFactory: MockSystemModel?
  var pexipManager: MockPexipManager?
  var mezzanineMainViewController: MezzanineMainViewController?
  var audioOnlyViewController: AudioOnlyViewController?
  var mockCommunicator: MockMZCommunicator?

  override func setUp() {
    super.setUp()

    systemModelFactory = MockSystemModel()
    pexipManager = MockPexipManager()
    mockCommunicator = MockMZCommunicator()
    mockCommunicator?.displayName = "Test Display Name"
    MezzanineAppContext.current().currentCommunicator = mockCommunicator

    mezzanineMainViewController = MezzanineMainViewController()
    mezzanineMainViewController!.appContext = MezzanineAppContext.current()
    mezzanineMainViewController!.communicator = mockCommunicator
    mezzanineMainViewController!.systemModel = systemModelFactory?.systemModel

    audioOnlyViewController = AudioOnlyViewController(pexipManager: pexipManager!, communicator: mockCommunicator!)
  }

  override func tearDown() {
    super.tearDown()
  }

  func setupLocalSession() {
    controller = MezzanineSessionViewController(pexipManager: pexipManager!, communicator: mockCommunicator!, mezzanineMainViewController: mezzanineMainViewController!, audioOnlyViewController: nil, initAudioOnlySession: false)

    MezzanineAppContext.current().mainNavController = MockMezzanineNavigationController.init(rootViewController: controller!)

    // Force view initialization
    _ = controller!.view
    controller!.viewWillAppear(false)
  }

  func setupMezzInSessionUsingWiFi() {
    mockCommunicator!.networkConnectionType = .wiFi
    mockCommunicator!.mockIsARemoteParticipationConnection = true
    controller = MezzanineSessionViewController(pexipManager: pexipManager!, communicator: mockCommunicator!, mezzanineMainViewController: mezzanineMainViewController!, audioOnlyViewController: audioOnlyViewController, initAudioOnlySession: false)
    MezzanineAppContext.current().mainNavController = MockMezzanineNavigationController.init(rootViewController: controller!)

    // Force view initialization
    _ = controller!.view
    controller!.viewWillAppear(false)
  }

  func setupMezzInSessionUsingCellularDataAsFullContent() {
    mockCommunicator!.networkConnectionType = .cellular
    mockCommunicator!.mockIsARemoteParticipationConnection = true
    controller = MezzanineSessionViewController(pexipManager: pexipManager!, communicator: mockCommunicator!, mezzanineMainViewController: mezzanineMainViewController!, audioOnlyViewController: audioOnlyViewController, initAudioOnlySession: false)
    MezzanineAppContext.current().mainNavController = MockMezzanineNavigationController.init(rootViewController: controller!)

    // Force view initialization
    _ = controller!.view
    controller!.viewWillAppear(false)
  }

  func setupMezzInSessionUsingCellularDataAsAudioOnly() {
    mockCommunicator!.networkConnectionType = .cellular
    mockCommunicator!.mockIsARemoteParticipationConnection = true
    controller = MezzanineSessionViewController(pexipManager: pexipManager!, communicator: mockCommunicator!, mezzanineMainViewController: mezzanineMainViewController!, audioOnlyViewController: audioOnlyViewController, initAudioOnlySession: true)
    MezzanineAppContext.current().mainNavController = MockMezzanineNavigationController.init(rootViewController: controller!)

    // Force view initialization
    _ = controller!.view
    controller!.viewWillAppear(false)
  }

  func testSetupLocalSession() {
    setupLocalSession()
    guard let controller = controller else { return }
    XCTAssertNotNil(controller.fullContentSessionPageViewController, "FullContentSessionPageViewController should exist")
    XCTAssertNil(controller.audioOnlySessionPageViewController, "AudioOnlySessionPageViewController should not exist")
    XCTAssertNil(controller.restrictedPageViewController.dataSource, "RestrictedPageViewController data source should not be defined")
    XCTAssertNil(controller.restrictedPageViewController.delegate, "RestrictedPageViewController delegate should not be defined")
  }

  func testSetupMezzInSessionOverWiFi() {
    setupMezzInSessionUsingWiFi()
    guard let controller = controller else { return }
    XCTAssertNotNil(controller.fullContentSessionPageViewController, "FullContentSessionPageViewController should exist")
    XCTAssertNotNil(controller.audioOnlySessionPageViewController, "AudioOnlySessionPageViewController should exist")
    XCTAssertNil(controller.restrictedPageViewController.dataSource, "RestrictedPageViewController data source should not be defined")
    XCTAssertNil(controller.restrictedPageViewController.delegate, "RestrictedPageViewController delegate should not be defined")
  }

  func testSetupMezzInSessionOverCellularDataAsFullContent() {
    setupMezzInSessionUsingCellularDataAsFullContent()
    guard let controller = controller else { return }
    XCTAssertNotNil(controller.fullContentSessionPageViewController, "FullContentSessionPageViewController should exist")
    XCTAssertNotNil(controller.audioOnlySessionPageViewController, "AudioOnlySessionPageViewController should exist")
    XCTAssertNotNil(controller.restrictedPageViewController.dataSource, "RestrictedPageViewController data source should be defined")
    XCTAssertNotNil(controller.restrictedPageViewController.delegate, "RestrictedPageViewController delegate should be be defined")
    XCTAssertEqual(controller.currentPage, controller.fullContentSessionPageViewController, "Initial Page should be Full Content")
  }

  func testSetupMezzInSessionOverCellularDataAsAudioOnly() {
    setupMezzInSessionUsingCellularDataAsAudioOnly()
    guard let controller = controller else { return }
    XCTAssertNotNil(controller.fullContentSessionPageViewController, "FullContentSessionPageViewController should exist")
    XCTAssertNotNil(controller.audioOnlySessionPageViewController, "AudioOnlySessionPageViewController should exist")
    XCTAssertNotNil(controller.restrictedPageViewController.dataSource, "RestrictedPageViewController data source should be defined")
    XCTAssertNotNil(controller.restrictedPageViewController.delegate, "RestrictedPageViewController delegate should be be defined")
    XCTAssertEqual(controller.currentPage, controller.audioOnlySessionPageViewController, "Initial Page should be Audio Session")
  }

  func testDisableSwipeAreaWhenChangePexipManagerStates() {
    setupMezzInSessionUsingCellularDataAsFullContent()
    guard let controller = controller else { return }
    guard let pexipManager = pexipManager else { return }

    // It starts in with a PexipConferenceState.Disconnected by default
    XCTAssertTrue(controller.restrictedPageViewController.swipeAreaHeight == 0)

    pexipManager.conferenceState = .connected
    XCTAssertTrue(controller.restrictedPageViewController.swipeAreaHeight > 0)

    pexipManager.conferenceState = .connecting
    XCTAssertTrue(controller.restrictedPageViewController.swipeAreaHeight == 0)

    pexipManager.conferenceState = .connected
    XCTAssertTrue(controller.restrictedPageViewController.swipeAreaHeight > 0)

    pexipManager.conferenceState = .disconnected
    XCTAssertTrue(controller.restrictedPageViewController.swipeAreaHeight == 0)
  }

  func testPageViewControllerNavigationViaItsDataSource() {
    setupMezzInSessionUsingCellularDataAsFullContent()
    guard let controller = controller else { return }

    let afterFullContentVC = controller.pageViewController(controller.restrictedPageViewController, viewControllerAfter: controller.fullContentSessionPageViewController)

    XCTAssertTrue(afterFullContentVC == controller.audioOnlySessionPageViewController, "After FullContent page there should be AudioOnly page")

    let afterAudioOnlyVC = controller.pageViewController(controller.restrictedPageViewController, viewControllerAfter: controller.audioOnlySessionPageViewController!)

    XCTAssertNil(afterAudioOnlyVC, "After AudioOnly page there should be nothing")

    let beforeFullContentVC = controller.pageViewController(controller.restrictedPageViewController, viewControllerBefore: controller.fullContentSessionPageViewController)

    XCTAssertNil(beforeFullContentVC, "Before FullContent page there should be nothing")

    let beforeAudioOnlyVC = controller.pageViewController(controller.restrictedPageViewController, viewControllerBefore: controller.audioOnlySessionPageViewController!)

    XCTAssertTrue(beforeAudioOnlyVC == controller.fullContentSessionPageViewController, "Before AudioOnly page there should be FullContent page")
  }

  func testPageViewControllerNavigationViaItsDelegate() {
    setupMezzInSessionUsingCellularDataAsFullContent()
    guard let controller = controller else { return }
    guard let pexipManager = pexipManager else { return }
    let infopresence = MZInfopresence()
    infopresence.pexipNode = "test.node.com"
    infopresence.pexipConference = "testConferece"
    pexipManager.infopresence = infopresence

    XCTAssertTrue(pexipManager.joinInvocations.count == 0, "It should not have joined yet")

    controller.pageViewController(controller.restrictedPageViewController, didFinishAnimating: true, previousViewControllers: [controller.fullContentSessionPageViewController], transitionCompleted: false)

    XCTAssertTrue(pexipManager.joinInvocations.count == 0, "It should not have joined yet")

    controller.pageViewController(controller.restrictedPageViewController, didFinishAnimating: true, previousViewControllers: [controller.fullContentSessionPageViewController], transitionCompleted: true)

    XCTAssertTrue(pexipManager.joinInvocations.count == 1, "It should have joined a new conference")
    XCTAssertTrue(pexipManager.joinInvocations[0].conferenceMode == .audioOnly, "An AudioOnly session")

    controller.pageViewController(controller.restrictedPageViewController, didFinishAnimating: true, previousViewControllers: [controller.audioOnlySessionPageViewController!], transitionCompleted: false)

    XCTAssertTrue(pexipManager.joinInvocations.count == 1, "It should have joined only once before")

    controller.pageViewController(controller.restrictedPageViewController, didFinishAnimating: true, previousViewControllers: [controller.audioOnlySessionPageViewController!], transitionCompleted: true)

    XCTAssertTrue(pexipManager.joinInvocations.count == 2, "It should have joined a second conference")
    XCTAssertTrue(pexipManager.joinInvocations[1].conferenceMode == .audioVideo, "An AudioVideo session")
  }

  func testShouldShowSessionSwipeBar() {
    mockCommunicator!.networkConnectionType = .wiFi
    mockCommunicator!.mockIsARemoteParticipationConnection = true
    controller = MezzanineSessionViewController(pexipManager: pexipManager!, communicator: mockCommunicator!, mezzanineMainViewController: mezzanineMainViewController!, audioOnlyViewController: audioOnlyViewController, initAudioOnlySession: false)
    XCTAssertFalse(controller!.shouldShowMultipleSessionPages())

    mockCommunicator!.networkConnectionType = .wiFi
    mockCommunicator!.mockIsARemoteParticipationConnection = false
    controller = MezzanineSessionViewController(pexipManager: pexipManager!, communicator: mockCommunicator!, mezzanineMainViewController: mezzanineMainViewController!, audioOnlyViewController: audioOnlyViewController, initAudioOnlySession: false)
    XCTAssertFalse(controller!.shouldShowMultipleSessionPages())

    mockCommunicator!.networkConnectionType = .cellular
    mockCommunicator!.mockIsARemoteParticipationConnection = false
    controller = MezzanineSessionViewController(pexipManager: pexipManager!, communicator: mockCommunicator!, mezzanineMainViewController: mezzanineMainViewController!, audioOnlyViewController: audioOnlyViewController, initAudioOnlySession: false)
    XCTAssertFalse(controller!.shouldShowMultipleSessionPages())

    mockCommunicator!.networkConnectionType = .cellular
    mockCommunicator!.mockIsARemoteParticipationConnection = true
    controller = MezzanineSessionViewController(pexipManager: pexipManager!, communicator: mockCommunicator!, mezzanineMainViewController: mezzanineMainViewController!, audioOnlyViewController: audioOnlyViewController, initAudioOnlySession: false)
    XCTAssertTrue(controller!.shouldShowMultipleSessionPages())

    mockCommunicator!.networkConnectionType = .cellular
    mockCommunicator!.mockIsARemoteParticipationConnection = false
    controller = MezzanineSessionViewController(pexipManager: pexipManager!, communicator: mockCommunicator!, mezzanineMainViewController: mezzanineMainViewController!, audioOnlyViewController: audioOnlyViewController, initAudioOnlySession: true)
    XCTAssertFalse(controller!.shouldShowMultipleSessionPages())

    mockCommunicator!.networkConnectionType = .cellular
    mockCommunicator!.mockIsARemoteParticipationConnection = true
    controller = MezzanineSessionViewController(pexipManager: pexipManager!, communicator: mockCommunicator!, mezzanineMainViewController: mezzanineMainViewController!, audioOnlyViewController: audioOnlyViewController, initAudioOnlySession: true)
    XCTAssertTrue(controller!.shouldShowMultipleSessionPages())
  }

  func testJoinPexipConferenceForCurrentPage() {

    guard let pexipManager = pexipManager else { return }
    let infopresence = MZInfopresence()
    pexipManager.infopresence = infopresence
    setupMezzInSessionUsingWiFi()
    guard let controller = controller else { return }

    XCTAssertTrue(pexipManager.joinInvocations.count == 0, "It should not have joined yet")

    controller.joinPexipConferenceForCurrentPage()
    XCTAssertTrue(pexipManager.joinInvocations.count == 0, "It should not have joined yet")

    infopresence.pexipNode = "test.node.com"
    XCTAssertTrue(pexipManager.joinInvocations.count == 0, "It should not have joined yet")

    infopresence.pexipConference = "testConferece"
    XCTAssertTrue(pexipManager.joinInvocations.count == 1, "It should have joined a new conference")

    pexipManager.conferenceState = .connecting
    infopresence.pexipConference = "testNewConferece"
    XCTAssertTrue(pexipManager.joinInvocations.count == 2, "It should have joined again, although it is ignored because it is still connecting the previous one")

    pexipManager.conferenceState = .connected
    infopresence.pexipConference = "testAnotherNewConferece"
    XCTAssertTrue(pexipManager.joinInvocations.count == 3, "It should have joined the second conference")
  }
}
