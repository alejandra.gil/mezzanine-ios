//
//  HideableWindow_Tests.swift
//  Mezzanine
//
//  Created by miguel on 25/4/17.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import XCTest

@testable import Mezzanine

class HideableWindow_Tests: XCTestCase {

  var hideableWindow: HideableWindow?

  override func setUp() {
    super.setUp()
    hideableWindow = HideableWindow()
  }

  override func tearDown() {

    super.tearDown()
  }

  func testInitializationOrder() {

    guard var hideableWindow = hideableWindow else {
      XCTFail()
      return
    }

    let viewController = UIViewController()

    hideableWindow = HideableWindow()
    hideableWindow.isHidden = false
    hideableWindow.rootViewController = viewController
    XCTAssertNotNil(hideableWindow.rootViewController)

    hideableWindow = HideableWindow()
    hideableWindow.rootViewController = viewController
    hideableWindow.isHidden = false
    XCTAssertNotNil(hideableWindow.rootViewController)

    hideableWindow = HideableWindow()
    hideableWindow.isHidden = true
    hideableWindow.rootViewController = viewController
    XCTAssertNil(hideableWindow.rootViewController)

    hideableWindow = HideableWindow()
    hideableWindow.rootViewController = viewController
    hideableWindow.isHidden = true
    XCTAssertNil(hideableWindow.rootViewController)
  }

  func testKeepRootViewControllerAfterSingleHiddenStateChange() {

    guard let hideableWindow = hideableWindow else {
      XCTFail()
      return
    }

    let viewController = UIViewController()

    hideableWindow.isHidden = false
    hideableWindow.rootViewController = viewController
    XCTAssertNotNil(hideableWindow.rootViewController)

    hideableWindow.isHidden = true
    XCTAssertNil(hideableWindow.rootViewController)

    hideableWindow.isHidden = false
    XCTAssertNotNil(hideableWindow.rootViewController)
  }

  func testKeepRootViewControllerAfterMultipleHiddenStateChangesInARow() {

    guard let hideableWindow = hideableWindow else {
      XCTFail()
      return
    }

    let viewController = UIViewController()

    hideableWindow.isHidden = false
    hideableWindow.rootViewController = viewController
    XCTAssertNotNil(hideableWindow.rootViewController)

    hideableWindow.isHidden = true
    hideableWindow.isHidden = true
    XCTAssertNil(hideableWindow.rootViewController)

    hideableWindow.isHidden = false
    hideableWindow.isHidden = false
    XCTAssertNotNil(hideableWindow.rootViewController)

    hideableWindow.isHidden = true
    hideableWindow.isHidden = true
    XCTAssertNil(hideableWindow.rootViewController)
  }
}
