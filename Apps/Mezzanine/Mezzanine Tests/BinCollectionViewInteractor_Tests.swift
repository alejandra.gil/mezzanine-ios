//
//  BinCollectionViewInteractor_Tests.swift
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 24/02/2017.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import XCTest

@testable import Mezzanine

class BinCollectionViewInteractor_Tests: XCTestCase {
  
  var interactor: BinCollectionViewInteractor!
  var mockSystemModel: MockSystemModel!
  var mockCommunicator: MockMZCommunicator!
  var entity = BinCollectionViewDataSource()
  var controller: BinCollectionViewController!
  
  override func setUp() {
    super.setUp()
    
    mockSystemModel = MockSystemModel()
    mockCommunicator = MockMZCommunicator()
    interactor = BinCollectionViewInteractor(systemModel: mockSystemModel.systemModel, communicator: mockCommunicator, entity: entity)
    
    controller = BinCollectionViewController(dataSource: entity)
    controller.interactor = interactor
    
    interactor.presenter = controller
    
    _ = controller.view
  }
  
  override func tearDown() {
    super.tearDown()
    
    entity.workspace = nil
  }
  
  
  // MARK: Observation tests
  
  func testSystemModelObservation() {
    XCTAssertTrue(interactor.systemModelObserverArray.count == 2)
    interactor.systemModel = nil
    XCTAssertTrue(interactor.systemModelObserverArray.count == 0)
  }
  
  func testWorkspaceObservation() {
    XCTAssertTrue(interactor.workspaceObserverArray.count == 1)
    interactor.workspace = nil
    XCTAssertTrue(interactor.workspaceObserverArray.count == 0)
  }
  
  func testPresentationObservation() {
    XCTAssertTrue(interactor.presentationObserverArray.count == 3)
    interactor.presentation = nil
    XCTAssertTrue(interactor.presentationObserverArray.count == 0)
  }
  
  func testLivestreamObservation() {
    mockSystemModel.addLiveStreams(toCurrentWorkspace: 4)
    XCTAssertTrue(interactor.liveStreamsObserverArray.count == 4)
    interactor.workspace = nil
    XCTAssertTrue(interactor.liveStreamsObserverArray.count == 0)
  }
  
  // MARK: Entity changes
  
  func testUpdateEntity() {
    XCTAssertEqual(entity.workspace, interactor.workspace)
  }
  
  func testUpdateEntityLivestreamChanges() {
    mockSystemModel.addLiveStreams(toCurrentWorkspace: 4)
    XCTAssertEqual(entity.workspace.liveStreams.count, interactor.workspace.liveStreams.count)
    
    mockSystemModel.removeLiveStreams(at: 2)
    XCTAssertEqual(entity.workspace.liveStreams.count, interactor.workspace.liveStreams.count)
    
    mockSystemModel.removeAllLiveStreams()
    XCTAssertEqual(entity.workspace.liveStreams.count, interactor.workspace.liveStreams.count)
    XCTAssertEqual(entity.workspace.liveStreams.count, 0)
  }
  
  func testUpdateEntityWorkspaceChanges() {
    let workspace = MZWorkspace()
    workspace.name = "Upcoming workspace"
    mockSystemModel.currentWorkspace = workspace
    XCTAssertEqual(entity.workspace.name, interactor.workspace.name)
  }
  
  func testUpdateEntitySlidesChanges() {
    mockSystemModel.addLiveStreams(toCurrentWorkspace: 4)
    XCTAssertEqual(entity.workspace.presentation.numberOfSlides, interactor.workspace.presentation.numberOfSlides)
    
    mockSystemModel.removeSlides(at: 2)
    XCTAssertEqual(entity.workspace.presentation.numberOfSlides, interactor.workspace.presentation.numberOfSlides)
    
    mockSystemModel.removeAllSlides()
    XCTAssertEqual(entity.workspace.presentation.numberOfSlides, interactor.workspace.presentation.numberOfSlides)
    XCTAssertEqual(entity.workspace.presentation.numberOfSlides, 0)
  }
  
  func testUpdateEntityPresentationChanges() {
    mockSystemModel.createTest(withSlides: 10)
    interactor.workspace.presentation.active = true
    XCTAssertTrue(entity.workspace.presentation.active)
    
    interactor.workspace.presentation.active = false
    XCTAssertFalse(entity.workspace.presentation.active)
    
    interactor.workspace.presentation.currentSlideIndex = 9
    XCTAssertEqual(entity.workspace.presentation.currentSlideIndex, 9)
  }
  
  
  // MARK: View changes
  
  func testItemsCellUpdatesCloudConnection() {    
    // Insertion
    mockSystemModel.addSlides(toCurrentWorkspace: 4)
    XCTAssertEqual(controller.collectionView.numberOfItems(inSection: 0), interactor.workspace.presentation.numberOfSlides)
    
    // Deletion
    mockSystemModel.removeSlides(at: 2)
    XCTAssertEqual(controller.collectionView.numberOfItems(inSection: 0), interactor.workspace.presentation.numberOfSlides)
    
    mockSystemModel.removeAllSlides()
    XCTAssertEqual(controller.collectionView.numberOfItems(inSection: 0), 1)
  }
  
  func testLiveStreamsCellUpdatesLocalConnection() {
    // Insertion
    mockSystemModel.addLiveStreams(toCurrentWorkspace: 4)
    interactor.workspace = mockSystemModel.currentWorkspace
    XCTAssertEqual(controller.collectionView.numberOfItems(inSection: 0), interactor.entity.validLiveStreams().count)
    
    // Deletion
    mockSystemModel.removeLiveStreams(at: 0)
    controller.collectionView.reloadData()
    XCTAssertEqual(controller.collectionView.numberOfItems(inSection: 0), interactor.entity.validLiveStreams().count)
    
    mockSystemModel.removeAllLiveStreams()
    controller.collectionView.reloadData()
    XCTAssertEqual(controller.collectionView.numberOfItems(inSection: 0), 1)
  }
  
  func testItemsCellUpdatesLocalConnection() {

    // Insertion
    mockSystemModel.addSlides(toCurrentWorkspace: 4)
    XCTAssertEqual(controller.collectionView.numberOfItems(inSection: 0), interactor.workspace.presentation.numberOfSlides)
    
    // Deletion
    mockSystemModel.removeSlides(at: 2)
    XCTAssertEqual(controller.collectionView.numberOfItems(inSection: 0), interactor.workspace.presentation.numberOfSlides)
    
    mockSystemModel.removeAllSlides()
    XCTAssertEqual(controller.collectionView.numberOfItems(inSection: 0), 1)
  }
  
  func testPresentationCurrentIndexActive() {
    mockSystemModel.addSlides(toCurrentWorkspace: 4)
    interactor.workspace.presentation.active = true
    interactor.workspace.presentation.currentSlideIndex = 1
    
    XCTAssertTrue(controller.currentSlideViewActive)
    XCTAssertTrue(controller.currentSlideViewIndex == 1)
    XCTAssertTrue(controller.currentSlideFrameView.alpha == 1)
  }
  
  
  // MARK: Requests
  
  func testPlacePortfolioItemOnScreen() {
    mockSystemModel.addSlides(toCurrentWorkspace: 4)
    let item = interactor.getPresentationItem(0)
    interactor.placeItemOnScreen(item)
    
    XCTAssertNotNil(item.uid)
    XCTAssertNotNil(item.contentSource)
    
    let requestor = mockCommunicator.requestor as! MockMZCommunicatorRequestor
    XCTAssertTrue(requestor.transactionRequested!.transactionName == "requestWindshieldItemCreate")
  }
  
  func testPlaceLiveStreamOnScreen() {
    mockSystemModel.addLiveStreams(toCurrentWorkspace: 4)
    let item = interactor.getLiveStreamItem(0)
    interactor.placeItemOnScreen(item)
    
    XCTAssertNotNil(item.uid)
    XCTAssertNil(item.contentSource)
    
    let requestor = mockCommunicator.requestor as! MockMZCommunicatorRequestor
    XCTAssertTrue(requestor.transactionRequested!.transactionName == "requestWindshieldItemCreate")
  }
  
  func testPresentOrJumpToSlideAtIndex() {
    mockSystemModel.addSlides(toCurrentWorkspace: 4)
    interactor.presentOrJumpToItemAtIndex(3)
    
    let requestor = mockCommunicator.requestor as! MockMZCommunicatorRequestor
    XCTAssertTrue(requestor.transactionRequested!.transactionName == "requestPresentationStart")
  }
  
  func testDeleteSlide() {
    mockSystemModel.addSlides(toCurrentWorkspace: 4)
    interactor.deleteItem(interactor.getPresentationItem(0))
    
    let requestor = mockCommunicator.requestor as! MockMZCommunicatorRequestor
    XCTAssertTrue(requestor.transactionRequested!.transactionName == "requestPortfolioItemDelete")
  }
  
  func testGrabItem() {
    interactor.grabItem(MZPortfolioItem())
    
    let requestor = mockCommunicator.requestor as! MockMZCommunicatorRequestor
    XCTAssertTrue(requestor.transactionRequested!.transactionName == "requestPortfolioItemGrab")
  }
  
  func testGrabLiveStreamItem() {
    interactor.grabItem(MZLiveStream())
    
    let requestor = mockCommunicator.requestor as! MockMZCommunicatorRequestor
    XCTAssertTrue(requestor.transactionRequested!.transactionName == "requestLiveStreamGrab")
  }
  
  func testRelinquishItem() {
    interactor.relinquishItem(MZPortfolioItem())
    
    let requestor = mockCommunicator.requestor as! MockMZCommunicatorRequestor
    XCTAssertTrue(requestor.transactionRequested!.transactionName == "requestPortfolioItemRelinquish")
  }
  
  func testRelinquishLiveStreamItem() {
    interactor.relinquishItem(MZLiveStream())
    
    let requestor = mockCommunicator.requestor as! MockMZCommunicatorRequestor
    XCTAssertTrue(requestor.transactionRequested!.transactionName == "requestLiveStreamRelinquish")
  }
  
  func testInsertItemSuccess() {
    interactor.insertItem(MZLiveStream(), index: 0)
    let requestor = mockCommunicator.requestor as! MockMZCommunicatorRequestor
    XCTAssertTrue(requestor.transactionRequested!.transactionName == "requestPortfolioItemInsert")
  }
  
  func testInsertItemFail() {
    interactor.insertItem(MZPortfolioItem(), index: 0)
    
    let requestor = mockCommunicator.requestor as! MockMZCommunicatorRequestor
    XCTAssertNil(requestor.transactionRequested)
  }
  
  func testReorderItemSuccess() {
    interactor.reorderItem(MZPortfolioItem(), index: 0)
    
    let requestor = mockCommunicator.requestor as! MockMZCommunicatorRequestor
    XCTAssertTrue(requestor.transactionRequested!.transactionName == "requestPortfolioItemReorder")
  }
  
  func testReorderItemFail() {
    interactor.reorderItem(MZLiveStream(), index: 0)
    
    let requestor = mockCommunicator.requestor as! MockMZCommunicatorRequestor
    XCTAssertNil(requestor.transactionRequested)
  }
}

