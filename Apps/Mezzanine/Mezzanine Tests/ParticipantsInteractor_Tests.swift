//
//  ParticipantsInteractor_Tests.swift
//  Mezzanine Tests
//
//  Created by Ivan Bella Lopez on 08/03/2019.
//  Copyright © 2019 Oblong Industries. All rights reserved.
//

import XCTest
@testable import Mezzanine

class ParticipantsInteractor_Tests: XCTestCase {

  var participantsInteractor =  ParticipantsInteractor()
  let helper = MezzanineRoomHelper()

  override func setUp() {
    // Creates a systemModel with its own mezzanine room
    let systemModel = MZSystemModel()
    systemModel.myMezzanine.uid = String(format:"%f", CACurrentMediaTime())
    systemModel.myMezzanine.name = "myMezzanine"
    systemModel.myMezzanine.roomType = MZMezzanineRoomType.local
    systemModel.infopresence = MZInfopresence()
    systemModel.featureToggles.participantRoster = true

    participantsInteractor.systemModel = systemModel
  }

  override func tearDown() {
    participantsInteractor.systemModel = nil
  }

  func testmyMezzanine() {
    guard let systemModel = participantsInteractor.systemModel else {
      XCTFail("Missing system model")
      return
    }

    systemModel.myMezzanine.participants = NSMutableArray(array:  helper.createParticipants(count: 5))

    XCTAssertEqual(participantsInteractor.totalRooms(), 1)
    XCTAssertEqual(participantsInteractor.totalPeople(), 5)
    XCTAssertEqual(participantsInteractor.mezzanines.count, 1)
  }

  func testParticipantsRemoteRoomsWithParticipants() {
    guard let systemModel = participantsInteractor.systemModel else {
      XCTFail("Missing system model")
      return
    }

    systemModel.myMezzanine.participants = NSMutableArray(array:  helper.createParticipants(count: 1))
    let room = helper.createMezzanineRoom(name: "mezzanineRoom", type: .remote, participants: helper.createParticipants(count: 5))
    let room2 = helper.createMezzanineRoom(name: "mezzanineRoom", type: .remote, participants: helper.createParticipants(count: 1))
    systemModel.infopresence.insert(room, inRoomsAt: 0)
    systemModel.infopresence.insert(room2, inRoomsAt: 1)
    XCTAssertEqual(participantsInteractor.totalRooms(), 3)
    XCTAssertEqual(participantsInteractor.totalPeople(), 7)
    XCTAssertEqual(participantsInteractor.mezzanines.count, 3)

    room2.removeParticipants(at: NSIndexSet(indexesIn: NSMakeRange(0, 1)) as IndexSet)
    XCTAssertEqual(participantsInteractor.totalRooms(), 3)
    XCTAssertEqual(participantsInteractor.totalPeople(), 6)
    XCTAssertEqual(participantsInteractor.mezzanines.count, 3)

    systemModel.infopresence.removeRooms(at: NSIndexSet(indexesIn: NSMakeRange(0, 1)) as IndexSet)
    XCTAssertEqual(participantsInteractor.totalRooms(), 2)
    XCTAssertEqual(participantsInteractor.totalPeople(), 1)
    XCTAssertEqual(participantsInteractor.mezzanines.count, 2)
  }

  func testParticipantsRemoteRoomsWithoutParticipants() {
    guard let systemModel = participantsInteractor.systemModel else {
      XCTFail("Missing system model")
      return
    }

    let room = helper.createMezzanineRoom(name: "mezzanineRoom", type: .remote, participants: Array())
    systemModel.infopresence.insert(room, inRoomsAt: 0)

    XCTAssertEqual(participantsInteractor.totalRooms(), 2)
    XCTAssertEqual(participantsInteractor.totalPeople(), 0)
    XCTAssertEqual(participantsInteractor.mezzanines.count, 2)
  }
  

  func testParticipantsCloudRoomWithParticipants() {
    guard let systemModel = participantsInteractor.systemModel else {
      XCTFail("Missing system model")
      return
    }

    systemModel.myMezzanine.participants = NSMutableArray(array:  helper.createParticipants(count: 1))
    let room = helper.createMezzanineRoom(name: "mezzanineRoom", type: .remote, participants: helper.createParticipants(count: 5))
    let room2 = helper.createMezzanineRoom(name: "mezzanineRoom", type: .cloud, participants: helper.createParticipants(count: 1))
    systemModel.infopresence.insert(room, inRoomsAt: 0)
    systemModel.infopresence.insert(room2, inRoomsAt: 1)

    XCTAssertEqual(participantsInteractor.totalRooms(), 3)
    XCTAssertEqual(participantsInteractor.mezzanines.count, 3)

    room2.removeParticipants(at: NSIndexSet(indexesIn: NSMakeRange(0, 1)) as IndexSet)
    XCTAssertEqual(participantsInteractor.totalPeople(), 6)
  }

  func testParticipantsCloudRoomWithoutParticipants() {
    guard let systemModel = participantsInteractor.systemModel else {
      XCTFail("Missing system model")
      return
    }

    let room = helper.createMezzanineRoom(name: "mezzanineRoom", type: .cloud, participants: Array())
    systemModel.infopresence.insert(room, inRoomsAt: 0)

    XCTAssertEqual(participantsInteractor.totalRooms(), 1)
    XCTAssertEqual(participantsInteractor.totalPeople(), 0)
    XCTAssertEqual(participantsInteractor.mezzanines.count, 1)
  }

  func testPeopleChanges() {
    guard let systemModel = participantsInteractor.systemModel else {
      XCTFail("Missing system model")
      return
    }

    systemModel.myMezzanine.participants = NSMutableArray(array:  helper.createParticipants(count: 1))
    let room = helper.createMezzanineRoom(name: "mezzanineRoom", type: .remote, participants: helper.createParticipants(count: 5))
    let room2 = helper.createMezzanineRoom(name: "mezzanineRoom", type: .remote, participants: helper.createParticipants(count: 5))
    let room3 = helper.createMezzanineRoom(name: "mezzanineRoom", type: .cloud, participants: helper.createParticipants(count: 1))
    systemModel.infopresence.insert(room, inRoomsAt: 0)
    systemModel.infopresence.insert(room2, inRoomsAt: 1)
    systemModel.infopresence.insert(room3, inRoomsAt: 2)
    XCTAssertEqual(participantsInteractor.totalRooms(), 4)

    room2.removeParticipants(at: NSIndexSet(indexesIn: NSMakeRange(0, 1)) as IndexSet)
    room3.removeParticipants(at: NSIndexSet(indexesIn: NSMakeRange(0, 1)) as IndexSet)
    room2.insertParticipants(helper.createParticipants(count: 2), at:NSIndexSet(indexesIn: NSMakeRange(0, 2)) as IndexSet)
    XCTAssertEqual(participantsInteractor.totalPeople(), 12)
  }
}
