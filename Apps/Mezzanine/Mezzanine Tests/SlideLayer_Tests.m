//
//  SlideLayer_Tests.m
//  Mezzanine
//
//  Created by miguel on 25/9/15.
//  Copyright (c) 2015 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "SlideLayer.h"

@interface SlideLayer_Tests : XCTestCase
{
  SlideLayer *_layer;
}

@end

@implementation SlideLayer_Tests

- (void)setUp
{
  [super setUp];
  _layer = [SlideLayer layer];
}

- (void)tearDown
{
  // Put teardown code here. This method is called after the invocation of each test method in the class.
  [super tearDown];
}

- (void)prepareTestWithVideoSlide
{
  MZSlide *item = [MZSlide new];
  item.uid = @"sl-000";
  item.contentSource = @"vi-000";
  item.index = 0;
  _layer.slide = item;
}

- (void)testVideoSlideWithEmtpySourceOriginString
{
  [self prepareTestWithVideoSlide];

  MZLiveStream *liveStream = [MZLiveStream new];
  liveStream.active = YES;  // Otherwise it won't show up in the bin
  liveStream.displayInfo = @{@"display-name": [NSString stringWithFormat:@"Stream Test"]};

  _layer.liveStream = liveStream;

  XCTAssertNil(_layer.placeholderLayer.sourceOrigin, @"VideoPlaceholderLayer origin info should be nil");
}

- (void)testVideoSlideWithSourceOriginString
{
  [self prepareTestWithVideoSlide];

  NSString *remoteMezzName = @"Remote Mezz Test";
  MZLiveStream *liveStream = [MZLiveStream new];
  liveStream.active = YES;  // Otherwise it won't show up in the bin
  liveStream.displayInfo = @{@"display-name": [NSString stringWithFormat:@"Stream Test"]};
  liveStream.remoteMezzName = remoteMezzName;

  _layer.liveStream = liveStream;

  NSString *sourceOrigingString = [NSString stringWithFormat:@"From %@", remoteMezzName];
  XCTAssertTrue([_layer.placeholderLayer.sourceOrigin isEqualToString:sourceOrigingString], @"VideoPlaceholderLayer origin info should be nil");
}

- (void)testObservingLiveStream
{
  MZLiveStream *liveStream = [MZLiveStream new];
  liveStream.active = YES;  // Otherwise it won't show up in the bin
  liveStream.fullURL = @"http://www.testUrl.com";

  _layer.liveStream = liveStream;

  id observation = [liveStream observationInfo];
  XCTAssertEqual([[observation valueForKey:@"_observances"] count], 2);

  _layer.liveStream = nil;
  observation = [liveStream observationInfo];
  XCTAssertNil(observation);
}

@end
