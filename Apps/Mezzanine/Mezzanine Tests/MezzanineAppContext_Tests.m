//
//  MezzanineAppContext_Tests.m
//  Mezzanine
//
//  Created by miguel on 23/2/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import "MezzanineAppContext.h"
#import "MezzanineMainViewController.h"
#import "MezzanineMainViewController_Private.h"
#import "MockSystemModel.h"
#import "PortfolioViewController.h"
#import "OBDragDropManager.h"
#import "WindshieldViewController.h"
#import "PresentationViewController.h"
#import "ExtendedWindshieldViewController.h"

#import "Mezzanine-Swift.h"

@interface MockMZCommunicator : MZCommunicator

@property (nonatomic, assign) BOOL isARemoteParticipationConnection;

@end

@implementation MockMZCommunicator

@synthesize isARemoteParticipationConnection;

@end

@class MezzanineRootViewController;

@interface MezzanineAppContext ()

- (MezzanineMainViewController *)mezzanineViewController;
- (void)showWorkspaceView:(BOOL)animated;
- (void)unhookViewControllersFromModel;
- (void)continueWithWSSConnection:(NSString *)serverName;
- (void)continueWithTCPSConnection:(NSURL *)url;
- (void)connectionViewController:(MezzanineConnectionViewController*)controller requestsAsyncConnectionAttemptTo:(NSURL*)endPointURL success:(void (^)(void))successBlock error:(void (^)(NSError *))errorBlock;
- (void)addCommunicatorObservationTokensFor:(MezzanineConnectionViewController *)controller success:(void (^)(void))successBlock error:(void (^)(NSError *))errorBlock;
- (void)failedToConnect:(NSError*)error;
- (void)setAudioOnlyMode:(BOOL)active;
- (void)resetImageAvailability;

@end


@interface MezzanineAppContext_Tests : XCTestCase
{
  MezzanineAppContext *_appContext;
}

@end

@implementation MezzanineAppContext_Tests

- (void)setUp
{
  [super setUp];

  _appContext = [[MezzanineAppContext alloc] init];
  MezzanineAppModel *applicationModel = [[MezzanineAppModel alloc] init];
  applicationModel.systemModel = [[MockSystemModel new] createTestModelWithDiptych];
  _appContext.applicationModel = applicationModel;
  _appContext.mainNavController = [MezzanineNavigationController new];
  _appContext.mainNavController.delegate = _appContext;
}

- (void)tearDown
{
  
}

#pragma mark - Communicator setter

- (void)testCommunicatorSetterToInfopresenceManager
{
  XCTAssertNil(_appContext.infopresenceManager.communicator, @"AppContext shouldn't have assigned a communicator to the Infopresence Manager yet");

  [_appContext connectionViewController:nil requestsAsyncConnectionAttemptTo:nil success:nil error:nil];

  XCTAssertNotNil(_appContext.infopresenceManager.communicator, @"AppContext should have assigned a communicator to the Infopresence Manager already");
}




#pragma mark - Observation tests

- (void)testWorkspaceViewControllerAppContextObservation
{
  XCTAssertNil([_appContext mezzanineViewController], @"MezzanineViewController should not exist yet");
  [_appContext showWorkspaceView:NO];
  XCTAssertNotNil([_appContext mezzanineViewController], @"MezzanineViewController should exist");
  XCTAssertNotNil([[_appContext mezzanineViewController] appContext], @"MezzanineViewController should observe app context");
  [_appContext unhookViewControllersFromModel];
  XCTAssertNil([[_appContext mezzanineViewController] appContext], @"MezzanineViewController should not observe app context anymore");
}

- (void)testRootViewControllerAppSubviewsContextObservation
{
  XCTAssertNil([_appContext mezzanineViewController], @"MezzanineViewController should not exist yet");
  [_appContext showWorkspaceView:NO];

  MezzanineRootViewController *rootViewController = (MezzanineRootViewController *)[_appContext mezzanineViewController];
  
  XCTAssertNotNil([rootViewController workspaceToolbarViewController].appContext, @"WorkspaceToolbarViewController should observe app context");
  XCTAssertNotNil([rootViewController mezzanineMainViewController].appContext, @"MezzanineMainViewController should observe app context");

  [_appContext unhookViewControllersFromModel];
  
  XCTAssertNil([rootViewController workspaceToolbarViewController].appContext, @"WorkspaceToolbarViewController should not observe app context");
  XCTAssertNil([rootViewController mezzanineMainViewController].appContext, @"MezzanineMainViewController should not observe app context");
}


- (void)testRootViewControllerAppSubviewsSystemModelObservation
{
  XCTAssertNil([_appContext mezzanineViewController], @"MezzanineViewController should not exist yet");
  [_appContext showWorkspaceView:NO];
  
  MezzanineRootViewController *rootViewController = (MezzanineRootViewController *)[_appContext mezzanineViewController];
  
  XCTAssertNotNil([rootViewController workspaceToolbarViewController].systemModel, @"WorkspaceToolbarViewController should observe MZSystemModel");
  XCTAssertNotNil([rootViewController mezzanineMainViewController].systemModel, @"MezzanineMainViewController should observe MZSystemModel");
  
  [_appContext unhookViewControllersFromModel];
  
  XCTAssertNil([rootViewController workspaceToolbarViewController].systemModel, @"WorkspaceToolbarViewController should not observe MZSystemModel");
  XCTAssertNil([rootViewController mezzanineMainViewController].systemModel, @"MezzanineMainViewController should not observe MZSystemModel");
}


- (void)testRootViewControllerAppSubviewsCommunicatorObservation
{
  XCTAssertNil([_appContext mezzanineViewController], @"MezzanineViewController should not exist yet");
  _appContext.currentCommunicator = [MockMZCommunicator new];
  [_appContext showWorkspaceView:NO];
  
  MezzanineRootViewController *rootViewController = (MezzanineRootViewController *)[_appContext mezzanineViewController];
  
  XCTAssertNotNil([rootViewController workspaceToolbarViewController].communicator, @"WorkspaceToolbarViewController should observe MZCommunicator");
  XCTAssertNotNil([rootViewController mezzanineMainViewController].communicator, @"MezzanineMainViewController should observe MZCommunicator");
  
  [_appContext unhookViewControllersFromModel];
  
  XCTAssertNil([rootViewController workspaceToolbarViewController].communicator, @"WorkspaceToolbarViewController should not observe MZCommunicator");
  XCTAssertNil([rootViewController mezzanineMainViewController].communicator, @"MezzanineMainViewController should not observe MZCommunicator");
}


#pragma mark - Device Configuration

- (void)testRPShouldntRunOniOS8
{
  [_appContext connectionViewController:nil
       requestsAsyncConnectionAttemptTo:[NSURL URLWithString:@"https://joinmz.com/m/1234"]
                                success:nil
                                  error:nil];
  
  id appContextMock = [OCMockObject partialMockForObject:_appContext];
  
  if (![[UIDevice currentDevice] isIOS9OrAbove])
    [[appContextMock expect] failedToConnect:OCMOCK_ANY];
  else
    [[appContextMock reject] failedToConnect:OCMOCK_ANY];
}

- (void)testSetDeviceToNotSleepOnRepomoteParticipation
{
  XCTAssertFalse([UIApplication sharedApplication].idleTimerDisabled, @"Device should go to sleep");

  MockMZCommunicator *mockCommunicator = [[MockMZCommunicator alloc] initWithServerUrl:nil];
  mockCommunicator.isARemoteParticipationConnection = YES;
  _appContext.currentCommunicator = mockCommunicator;

  [_appContext addCommunicatorObservationTokensFor:nil success:nil error:nil];
  [[NSNotificationCenter defaultCenter] postNotificationName:MZCommunicatorReceviedInitialStateNotification object:mockCommunicator];

  XCTAssertTrue([UIApplication sharedApplication].idleTimerDisabled, @"Device should go to sleep");

  [[NSNotificationCenter defaultCenter] postNotificationName:MZCommunicatorDidDisconnectNotification object:mockCommunicator];

  XCTAssertFalse([UIApplication sharedApplication].idleTimerDisabled, @"Device should go to sleep");
}

- (void)testSetDeviceShouldBehaveAsDefaultOnLocalSessions
{
  XCTAssertFalse([UIApplication sharedApplication].idleTimerDisabled, @"Device should go to sleep");

  MockMZCommunicator *mockCommunicator = [[MockMZCommunicator alloc] initWithServerUrl:nil];
  mockCommunicator.isARemoteParticipationConnection = NO;
  _appContext.currentCommunicator = mockCommunicator;

  [_appContext addCommunicatorObservationTokensFor:nil success:nil error:nil];
  [[NSNotificationCenter defaultCenter] postNotificationName:MZCommunicatorReceviedInitialStateNotification object:mockCommunicator];

  XCTAssertFalse([UIApplication sharedApplication].idleTimerDisabled, @"Device should go to sleep");

  [[NSNotificationCenter defaultCenter] postNotificationName:MZCommunicatorDidDisconnectNotification object:mockCommunicator];

  XCTAssertFalse([UIApplication sharedApplication].idleTimerDisabled, @"Device should go to sleep");
}

- (void)testAudioOnlyActivationStates
{
  [_appContext setAudioOnlyMode:YES];
  XCTAssertTrue([MZDownloadManager sharedLoader].pauseDownloads, @"MZDownloadManager should have paused its downloads");

  [_appContext setAudioOnlyMode:NO];
  XCTAssertFalse([MZDownloadManager sharedLoader].pauseDownloads, @"MZDownloadManager should have paused its downloads");
}


#pragma mark - Connect warning

- (void)testDisplayConnectWarning
{
  MockMZCommunicator *mockCommunicator = [[MockMZCommunicator alloc] initWithServerUrl:nil];
  _appContext.currentCommunicator = mockCommunicator;
  mockCommunicator.systemUseNotification = true;
  mockCommunicator.systemUseNotificationText = @"test";
  
  _appContext.applicationModel.systemModel.state = MZSystemStateWorkspace;

  XCTAssertNotNil(_appContext.systemUseNotificationViewController, @"SystemUseNotificationViewController should be displayed");
}


#pragma mark - in Background

- (void)testSendAppToBackgroundAndBackToForeground
{
  [_appContext setupDragDropManager];
  [_appContext initPopupInfoView];
  [_appContext initProgressView];
  [_appContext setupInfopresenceOverlay];

  UIWindow *infopresenceOverlayWindow = [_appContext.infopresenceManager valueForKey:@"overlayWindow"];
  XCTAssertTrue([OBDragDropManager sharedManager].overlayWindow.hidden);
  XCTAssertTrue(_appContext.popupInfoWindow.hidden);
  XCTAssertTrue(_appContext.progressWindow.hidden);
  XCTAssertTrue(infopresenceOverlayWindow.hidden);

  [_appContext setInBackground:YES];

  XCTAssertFalse([OBDragDropManager sharedManager].overlayWindow.hidden);
  XCTAssertFalse(_appContext.popupInfoWindow.hidden);
  XCTAssertFalse(_appContext.progressWindow.hidden);
  XCTAssertFalse(infopresenceOverlayWindow.hidden);

  [_appContext setInBackground:NO];

  XCTAssertTrue([OBDragDropManager sharedManager].overlayWindow.hidden);
  XCTAssertTrue(_appContext.popupInfoWindow.hidden);
  XCTAssertTrue(_appContext.progressWindow.hidden);
  XCTAssertTrue(infopresenceOverlayWindow.hidden);
}


#pragma mark - RP

- (void)testResetServerURLIfRemoteParticipationIsCancelled
{
  MZCommunicator *communicator = [MZCommunicator new];
  communicator.serverUrl = [NSURL URLWithString:@"https://mezz-in.com/m/666"];
  _appContext.currentCommunicator = communicator;
  
  RemoteParticipationJoinViewController *remoteParticipationJoinViewController = [RemoteParticipationJoinViewController new];
  remoteParticipationJoinViewController.delegate = (id <RemoteParticipationJoinViewControllerDelegate>)_appContext;
  remoteParticipationJoinViewController.communicator = _appContext.currentCommunicator;
  
  [remoteParticipationJoinViewController.delegate remoteParticipationJoinViewControllerShouldCancelConnection:remoteParticipationJoinViewController];
  XCTAssertNil(_appContext.currentCommunicator.serverUrl);
}

@end
