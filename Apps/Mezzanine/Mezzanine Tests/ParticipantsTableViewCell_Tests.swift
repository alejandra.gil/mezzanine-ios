//
//  ParticipantsTableViewCell_Tests.swift
//  Mezzanine
//
//  Created by miguel on 17/2/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

import UIKit
import XCTest
@testable import Mezzanine

class ParticipantsTableViewCell_Tests: XCTestCase {

  var cell: ParticipantsTableViewCell?
  var systemModel: MZSystemModel?
  var communicator: MZCommunicator?
  let styleSheet = MezzanineStyleSheet()

  override func setUp() {
    super.setUp()

    systemModel = MZSystemModel()
    communicator = MZCommunicator()
    communicator?.systemModel = systemModel


    let nib = UINib(nibName: "ParticipantsTableViewCell", bundle: nil)
    let objects = nib.instantiate(withOwner: nil, options: nil)
    cell = (objects[0] as! ParticipantsTableViewCell)

    cell?.communicator = communicator
    cell?.systemModel = systemModel
  }

  override func tearDown() {
    super.tearDown()
  }

  func testSetInvitation() {

    let invite = createInfopresenceInviteCallWithUid("test-uid")
    cell!.participant = invite

    XCTAssertFalse(cell!.cancelInvitationButton.isHidden, "Cancel button should be hidden")
    XCTAssertTrue(cell!.participantTypeImageView.tintColor == styleSheet.grey90Color)
  }

  func testSetRemoteMezz() {

    let remoteMezz = MZRemoteMezz()
    remoteMezz.uid = "test-uid"
    remoteMezz.name = mezzNameFromUid("test-uid")

    cell!.participant = remoteMezz

    XCTAssertTrue(cell!.cancelInvitationButton.isHidden, "Cancel button should be hidden")
    XCTAssertTrue(cell!.participantTypeImageView.tintColor == styleSheet.grey190Color)
  }

  func testSetLocalMezz() {

    let mezzanine = MZMezzanine()
    mezzanine.uid = "test-uid"
    mezzanine.name = mezzNameFromUid("test-uid")

    cell!.participant = mezzanine

    XCTAssertTrue(cell!.cancelInvitationButton.isHidden, "Cancel button should be hidden")
    XCTAssertTrue(cell!.participantTypeImageView.tintColor == styleSheet.grey190Color)
  }


//MARK - Helpers

  func mezzNameFromUid(_ uid: String) -> String {

    return "remoteMezz-"+uid
  }

  func addRemoteMezz(_ uid: String) {

    let remoteMezz = MZRemoteMezz()
    remoteMezz.uid = uid
    remoteMezz.name = mezzNameFromUid(uid)
    systemModel?.remoteMezzes.add(remoteMezz)
  }

  func createInfopresenceJoinRequestCallWithUid(_ uid: String) -> MZInfopresenceCall{

    let infopresenceCall = MZInfopresenceCall()
    infopresenceCall.uid = uid
    infopresenceCall.type = .joinRequest

    addRemoteMezz(uid)
    return infopresenceCall
  }

  func createInfopresenceInviteCallWithUid(_ uid: String) -> MZInfopresenceCall{

    let infopresenceCall = MZInfopresenceCall()
    infopresenceCall.uid = uid
    infopresenceCall.type = .invite

    addRemoteMezz(uid)
    return infopresenceCall
  }

}
