//
//  TestSystemModelFactory.m
//  Mezzanine
//
//  Created by Zai Chang on 12/5/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import "MockSystemModel.h"

@interface MockSystemModel ()

@property (nonatomic, strong) MZSystemModel *systemModel;

@end

@implementation MockSystemModel

- (instancetype)init
{
  if (self = [super init]) {
    self.systemModel = [MZSystemModel new];
    self.currentWorkspace = [MZWorkspace new];

    self.currentWorkspace.uid = @"ws-current";
    self.systemModel.currentWorkspace = self.currentWorkspace;
    self.systemModel.myMezzanine.version = @"3.13";
  }
  
  return self;
}


- (NSString*)whiteboardUidForIndex:(NSInteger)index
{
  return [NSString stringWithFormat:@"wb-%ld", (long)index];
}


- (NSString*)whiteboardDisplayNameForIndex:(NSInteger)index
{
  return [NSString stringWithFormat:@"Whiteboard %ld", (long)index];
}


- (MZSystemModel*)createTestModelWithWhiteboards:(NSInteger)numberOfWhiteboards
{

  for (NSInteger i=0; i<numberOfWhiteboards; i++)
  {
    // Urgh still using NSDictionary ... should refactor
    NSDictionary *whiteboardDict = @{@"uid": [self whiteboardUidForIndex:i],
                                     @"name": [self whiteboardDisplayNameForIndex:i]};
    [self.systemModel.whiteboards addObject:whiteboardDict];
  }
  
  return self.systemModel;
}


- (MZSystemModel*)createTestModelWithSlides:(NSInteger)numberOfSlides
{
  self.systemModel.currentWorkspace.presentation.slides = [@[] mutableCopy];
  
  for (NSInteger c = 0; c < numberOfSlides; c++) {
    
    MZPortfolioItem * item = [MZPortfolioItem new];
    item.index = c;
    item.uid = [NSString stringWithFormat:@"uid-%li", (long)c];
    
    [self.systemModel.currentWorkspace.presentation.slides addObject:item];
  }
  
  return self.systemModel;
}

- (MZSystemModel*)createTestModelWithTriptych
{
  MZFeld *mainFeld = [MZFeld feldWithName:@"main"];
  MZFeld *leftFeld = [MZFeld feldWithName:@"left"];
  MZFeld *rightFeld = [MZFeld feldWithName:@"right"];

  NSDictionary *leftFeldDictionary = @{@"id" : @"left",
                                       @"type" : @"visible",
                                       @"cent" : [MZVect doubleVectWithX:-1212.0 y:100.0 z:-2100.0],
                                       @"up"   : [MZVect doubleVectWithX:0.0 y:1.0 z:0.0],
                                       @"over" : [MZVect doubleVectWithX:1.0 y:0.0 z:0.0],
                                       @"norm" : [MZVect doubleVectWithX:0.0 y:0.0 z:1.0],
                                       @"phys-size" : [MZVect doubleVectWithX:1200.0 y:675.0],
                                       @"px-size" : [MZVect integerVectWithX:1920 y:1080],
                                       @"view-dist" : @1450,
                                       @"visible-to-all" : @YES };

  NSDictionary *mainFeldDictionary = @{@"id" : @"main",
                                       @"type" : @"visible",
                                       @"cent" : [MZVect doubleVectWithX:0.0 y:100.0 z:-2100.0],
                                       @"up"   : [MZVect doubleVectWithX:0.0 y:1.0 z:0.0],
                                       @"over" : [MZVect doubleVectWithX:1.0 y:0.0 z:0.0],
                                       @"norm" : [MZVect doubleVectWithX:0.0 y:0.0 z:1.0],
                                       @"phys-size" : [MZVect doubleVectWithX:1200.0 y:675.0],
                                       @"px-size" : [MZVect integerVectWithX:1920 y:1080],
                                       @"view-dist" : @1450,
                                       @"visible-to-all" : @YES };

  NSDictionary *rightFeldDictionary = @{@"id" : @"right",
                                        @"type" : @"visible",
                                        @"cent" : @[@1212.0, @100.0, @(-2100.0)],
                                        @"up"   : @[@0.0, @1.0, @0.0],
                                        @"over" : @[@1.0, @0.0, @0.0],
                                        @"norm" : @[@0.0, @0.0, @1.0],
                                        @"phys-size" : @[@1200.0, @675.0],
                                        @"px-size" : @[@1920, @1080],
                                        @"view-dist" : @1450,
                                        @"visible-to-all" : @YES };

  [leftFeld updateWithDictionary:leftFeldDictionary];
  [mainFeld updateWithDictionary:mainFeldDictionary];
  [rightFeld updateWithDictionary:rightFeldDictionary];


  [self.systemModel.felds addObjectsFromArray:@[leftFeld, mainFeld, rightFeld]];

  MZSurface *mainSurface = [[MZSurface alloc] initWithName:@"main"];
  [mainSurface updateWithDictionary:@{@"bounding-feld": @{@"id" : @"bounding-feld",
                                                          @"type" : @"visible",
                                                          @"cent" : [MZVect doubleVectWithX:1200.0 y:100.0 z:-2100.0],
                                                          @"up"   : [MZVect doubleVectWithX:0.0 y:1.0 z:0.0],
                                                          @"over" : [MZVect doubleVectWithX:1.0 y:0.0 z:0.0],
                                                          @"norm" : [MZVect doubleVectWithX:0.0 y:0.0 z:1.0],
                                                          @"phys-size" : [MZVect doubleVectWithX:3624.0 y:675.0],
                                                          @"px-size" : [MZVect integerVectWithX:5760 y:1080],
                                                          @"view-dist" : @1450,
                                                          @"visible-to-all" : @YES },
                                      @"felds": @[leftFeldDictionary, mainFeldDictionary, rightFeldDictionary]}];
  [self.systemModel.surfaces addObject:mainSurface];
  
  return self.systemModel;
}

- (MZSystemModel*)createTestModelWithSingleFeld
{
  NSDictionary *feldDictionary = @{@"id" : @"main",
                                   @"type" : @"visible",
                                   @"cent" : [MZVect doubleVectWithX:0.0 y:100.0 z:-2100.0],
                                   @"up"   : [MZVect doubleVectWithX:0.0 y:1.0 z:0.0],
                                   @"over" : [MZVect doubleVectWithX:1.0 y:0.0 z:0.0],
                                   @"norm" : [MZVect doubleVectWithX:0.0 y:0.0 z:1.0],
                                   @"phys-size" : [MZVect doubleVectWithX:1200.0 y:675.0],
                                   @"px-size" : [MZVect integerVectWithX:1920 y:1080],
                                   @"view-dist" : @1450,
                                   @"visible-to-all" : @YES };

  MZFeld *mainFeld = [MZFeld feldWithName:@"main"];

  [mainFeld updateWithDictionary:feldDictionary];

  [self.systemModel.felds addObject:mainFeld];

  MZSurface *mainSurface = [[MZSurface alloc] initWithName:@"main"];
  [mainSurface updateWithDictionary:@{@"bounding-feld": feldDictionary,
                                      @"felds": @[feldDictionary]}];
  [self.systemModel.surfaces addObject:mainSurface];

  return self.systemModel;
}

- (MZSystemModel*)createTestModelWithDiptych {

  MZFeld *mainFeld = [MZFeld feldWithName:@"main"];
  MZFeld *rightFeld = [MZFeld feldWithName:@"right"];

  NSDictionary *mainFeldDictionary = @{@"id" : @"main",
                                       @"type" : @"visible",
                                       @"cent" : [MZVect doubleVectWithX:0.0 y:100.0 z:-2100.0],
                                       @"up"   : [MZVect doubleVectWithX:0.0 y:1.0 z:0.0],
                                       @"over" : [MZVect doubleVectWithX:1.0 y:0.0 z:0.0],
                                       @"norm" : [MZVect doubleVectWithX:0.0 y:0.0 z:1.0],
                                       @"phys-size" : [MZVect doubleVectWithX:1200.0 y:675.0],
                                       @"px-size" : [MZVect integerVectWithX:1920 y:1080],
                                       @"view-dist" : @1450,
                                       @"visible-to-all" : @YES };

  NSDictionary *rightFeldDictionary = @{@"id" : @"right",
                                        @"type" : @"visible",
                                        @"cent" : @[@1212.0, @100.0, @(-2100.0)],
                                        @"up"   : @[@0.0, @1.0, @0.0],
                                        @"over" : @[@1.0, @0.0, @0.0],
                                        @"norm" : @[@0.0, @0.0, @1.0],
                                        @"phys-size" : @[@1200.0, @675.0],
                                        @"px-size" : @[@1920, @1080],
                                        @"view-dist" : @1450,
                                        @"visible-to-all" : @YES };

  [mainFeld updateWithDictionary:mainFeldDictionary];
  [rightFeld updateWithDictionary:rightFeldDictionary];

  MZSurface *mainSurface = [[MZSurface alloc] initWithName:@"main"];
  [mainSurface updateWithDictionary:@{@"bounding-feld": @{@"id" : @"bounding-feld",
                                                          @"type" : @"visible",
                                                          @"cent" : [MZVect doubleVectWithX:1200.0 y:100.0 z:-2100.0],
                                                          @"up"   : [MZVect doubleVectWithX:0.0 y:1.0 z:0.0],
                                                          @"over" : [MZVect doubleVectWithX:1.0 y:0.0 z:0.0],
                                                          @"norm" : [MZVect doubleVectWithX:0.0 y:0.0 z:1.0],
                                                          @"phys-size" : [MZVect doubleVectWithX:2412.0 y:675.0],
                                                          @"px-size" : [MZVect integerVectWithX:5760 y:1080],
                                                          @"view-dist" : @1450,
                                                          @"visible-to-all" : @YES },
                                      @"felds": @[mainFeldDictionary, rightFeldDictionary]}];
  [self.systemModel.surfaces addObject:mainSurface];

  return self.systemModel;
}

- (MZSystemModel*)createTestModelWithTwoByThreeSurface
{
  MZFeld *topMainFeld = [MZFeld feldWithName:@"top-main"];
  MZFeld *topLeftFeld = [MZFeld feldWithName:@"top-left"];
  MZFeld *topRightFeld = [MZFeld feldWithName:@"top-right"];
  MZFeld *bottomMainFeld = [MZFeld feldWithName:@"bottom-main"];
  MZFeld *bottomLeftFeld = [MZFeld feldWithName:@"bottom-left"];
  MZFeld *bottomRightFeld = [MZFeld feldWithName:@"bottom-right"];

  NSDictionary *topLeftFeldDictionary = @{@"id" : @"top-left",
                                          @"type" : @"visible",
                                          @"cent" : [MZVect doubleVectWithX:-1212.0 y:100.0 z:-2100.0],
                                          @"up"   : [MZVect doubleVectWithX:0.0 y:1.0 z:0.0],
                                          @"over" : [MZVect doubleVectWithX:1.0 y:0.0 z:0.0],
                                          @"norm" : [MZVect doubleVectWithX:0.0 y:0.0 z:1.0],
                                          @"phys-size" : [MZVect doubleVectWithX:1200.0 y:675.0],
                                          @"px-size" : [MZVect integerVectWithX:1920 y:1080],
                                          @"view-dist" : @1450,
                                          @"visible-to-all" : @YES };

  NSDictionary *topMainFeldDictionary = @{@"id" : @"top-main",
                                          @"type" : @"visible",
                                          @"cent" : [MZVect doubleVectWithX:0.0 y:100.0 z:-2100.0],
                                          @"up"   : [MZVect doubleVectWithX:0.0 y:1.0 z:0.0],
                                          @"over" : [MZVect doubleVectWithX:1.0 y:0.0 z:0.0],
                                          @"norm" : [MZVect doubleVectWithX:0.0 y:0.0 z:1.0],
                                          @"phys-size" : [MZVect doubleVectWithX:1200.0 y:675.0],
                                          @"px-size" : [MZVect integerVectWithX:1920 y:1080],
                                          @"view-dist" : @1450,
                                          @"visible-to-all" : @YES };

  NSDictionary *topRightFeldDictionary = @{@"id" : @"top-right",
                                           @"type" : @"visible",
                                           @"cent" : @[@1212.0, @100.0, @(-2100.0)],
                                           @"up"   : @[@0.0, @1.0, @0.0],
                                           @"over" : @[@1.0, @0.0, @0.0],
                                           @"norm" : @[@0.0, @0.0, @1.0],
                                           @"phys-size" : @[@1200.0, @675.0],
                                           @"px-size" : @[@1920, @1080],
                                           @"view-dist" : @1450,
                                           @"visible-to-all" : @YES };

  NSDictionary *bottomLeftFeldDictionary = @{@"id" : @"bottom-left",
                                             @"type" : @"visible",
                                             @"cent" : [MZVect doubleVectWithX:-1212.0 y:787.0 z:-2100.0],
                                             @"up"   : [MZVect doubleVectWithX:0.0 y:1.0 z:0.0],
                                             @"over" : [MZVect doubleVectWithX:1.0 y:0.0 z:0.0],
                                             @"norm" : [MZVect doubleVectWithX:0.0 y:0.0 z:1.0],
                                             @"phys-size" : [MZVect doubleVectWithX:1200.0 y:675.0],
                                             @"px-size" : [MZVect integerVectWithX:1920 y:1080],
                                             @"view-dist" : @1450,
                                             @"visible-to-all" : @YES };

  NSDictionary *bottomMainFeldDictionary = @{@"id" : @"bottom-main",
                                             @"type" : @"visible",
                                             @"cent" : [MZVect doubleVectWithX:0.0 y:787.0 z:-2100.0],
                                             @"up"   : [MZVect doubleVectWithX:0.0 y:1.0 z:0.0],
                                             @"over" : [MZVect doubleVectWithX:1.0 y:0.0 z:0.0],
                                             @"norm" : [MZVect doubleVectWithX:0.0 y:0.0 z:1.0],
                                             @"phys-size" : [MZVect doubleVectWithX:1200.0 y:675.0],
                                             @"px-size" : [MZVect integerVectWithX:1920 y:1080],
                                             @"view-dist" : @1450,
                                             @"visible-to-all" : @YES };

  NSDictionary *bottomRightFeldDictionary = @{@"id" : @"bottom-right",
                                              @"type" : @"visible",
                                              @"cent" : @[@1212.0, @787.0, @(-2100.0)],
                                              @"up"   : @[@0.0, @1.0, @0.0],
                                              @"over" : @[@1.0, @0.0, @0.0],
                                              @"norm" : @[@0.0, @0.0, @1.0],
                                              @"phys-size" : @[@1200.0, @675.0],
                                              @"px-size" : @[@1920, @1080],
                                              @"view-dist" : @1450,
                                              @"visible-to-all" : @YES };

  [topLeftFeld updateWithDictionary:topLeftFeldDictionary];
  [topMainFeld updateWithDictionary:topMainFeldDictionary];
  [topRightFeld updateWithDictionary:topRightFeldDictionary];
  [bottomLeftFeld updateWithDictionary:bottomLeftFeldDictionary];
  [bottomMainFeld updateWithDictionary:bottomMainFeldDictionary];
  [bottomRightFeld updateWithDictionary:bottomRightFeldDictionary];


  [self.systemModel.felds addObjectsFromArray:@[topLeftFeld, topMainFeld, topRightFeld, bottomLeftFeld, bottomMainFeld, bottomRightFeld]];

  MZSurface *mainSurface = [[MZSurface alloc] initWithName:@"main"];
  [mainSurface updateWithDictionary:@{@"bounding-feld": @{@"id" : @"bounding-feld",
                                                          @"type" : @"visible",
                                                          @"cent" : [MZVect doubleVectWithX:1200.0 y:100.0 z:-2100.0],
                                                          @"up"   : [MZVect doubleVectWithX:0.0 y:1.0 z:0.0],
                                                          @"over" : [MZVect doubleVectWithX:1.0 y:0.0 z:0.0],
                                                          @"norm" : [MZVect doubleVectWithX:0.0 y:0.0 z:1.0],
                                                          @"phys-size" : [MZVect doubleVectWithX:3624.0 y:1362],
                                                          @"px-size" : [MZVect integerVectWithX:5760 y:1080],
                                                          @"view-dist" : @1450,
                                                          @"visible-to-all" : @YES },
                                      @"felds": @[topLeftFeldDictionary, topMainFeldDictionary, topRightFeldDictionary, bottomLeftFeldDictionary, bottomMainFeldDictionary, bottomRightFeldDictionary]}];
  [self.systemModel.surfaces addObject:mainSurface];

  return self.systemModel;


}

- (MZSystemModel*)createTestModelWithOneExtendedWindshieldSurfaceWithOneFeld
{
  NSDictionary *feldDictionary = @{@"id" : @"main",
                                   @"type" : @"visible",
                                   @"cent" : [MZVect doubleVectWithX:0.0 y:100.0 z:-2100.0],
                                   @"up"   : [MZVect doubleVectWithX:0.0 y:1.0 z:0.0],
                                   @"over" : [MZVect doubleVectWithX:1.0 y:0.0 z:0.0],
                                   @"norm" : [MZVect doubleVectWithX:0.0 y:0.0 z:1.0],
                                   @"phys-size" : [MZVect doubleVectWithX:1920 y:1080],
                                   @"px-size" : [MZVect integerVectWithX:1920 y:1080],
                                   @"view-dist" : @1450,
                                   @"visible-to-all" : @YES };

  MZSurface *surface = [[MZSurface alloc] initWithName:@"surface-1"];

  [surface updateWithDictionary:@{@"bounding-feld": feldDictionary,
                                  @"felds": @[feldDictionary]}];

  [self.systemModel.surfaces addObject:surface];
  return self.systemModel;
}

- (MZSystemModel*)createTestModelWithOneExtendedWindshieldSurfaceWithOneFeldAndOneItem
{
  MZSystemModel *systemModel = [self createTestModelWithOneExtendedWindshieldSurfaceWithOneFeld];

  MZWindshieldItem *item = [[MZWindshieldItem alloc] init];
  item.assetUid = @"as-1111";
  item.frame = CGRectMake(0, 0, 1, 1);
  item.surface = self.systemModel.surfaces[0];
  item.feld = [item.surface felds][0];

  [systemModel.currentWorkspace.windshield.items addObject:item];

  return systemModel;
}


- (MZSystemModel*)createTestModelWithThreeExtendedWindshieldSurfacesWithOneFeld
{
  NSArray *feldDictionariesArray = @[@{@"id" : @"left",
                                       @"type" : @"visible",
                                       @"cent" : [MZVect doubleVectWithX:-1920 y:100.0 z:-2100.0],
                                       @"up"   : [MZVect doubleVectWithX:0.0 y:1.0 z:0.0],
                                       @"over" : [MZVect doubleVectWithX:1.0 y:0.0 z:0.0],
                                       @"norm" : [MZVect doubleVectWithX:0.0 y:0.0 z:1.0],
                                       @"phys-size" : [MZVect doubleVectWithX:1920 y:1080],
                                       @"px-size" : [MZVect integerVectWithX:1920 y:1080],
                                       @"view-dist" : @1450,
                                       @"visible-to-all" : @YES },
                                     @{@"id" : @"main",
                                       @"type" : @"visible",
                                       @"cent" : [MZVect doubleVectWithX:0.0 y:100.0 z:-2100.0],
                                       @"up"   : [MZVect doubleVectWithX:0.0 y:1.0 z:0.0],
                                       @"over" : [MZVect doubleVectWithX:1.0 y:0.0 z:0.0],
                                       @"norm" : [MZVect doubleVectWithX:0.0 y:0.0 z:1.0],
                                       @"phys-size" : [MZVect doubleVectWithX:1920 y:1080],
                                       @"px-size" : [MZVect integerVectWithX:1920 y:1080],
                                       @"view-dist" : @1450,
                                       @"visible-to-all" : @YES },
                                     @{@"id" : @"right",
                                       @"type" : @"visible",
                                       @"cent" : [MZVect doubleVectWithX:1920 y:100.0 z:-2100.0],
                                       @"up"   : [MZVect doubleVectWithX:0.0 y:1.0 z:0.0],
                                       @"over" : [MZVect doubleVectWithX:1.0 y:0.0 z:0.0],
                                       @"norm" : [MZVect doubleVectWithX:0.0 y:0.0 z:1.0],
                                       @"phys-size" : [MZVect doubleVectWithX:1920 y:1080],
                                       @"px-size" : [MZVect integerVectWithX:1920 y:1080],
                                       @"view-dist" : @1450,
                                       @"visible-to-all" : @YES }];

  for (NSInteger i = 0 ; i < feldDictionariesArray.count ; i++)
  {
    MZSurface *surface = [[MZSurface alloc] initWithName:[NSString stringWithFormat:@"surface-%ld", (long)i]];
    [surface updateWithDictionary:@{@"bounding-feld": feldDictionariesArray[i],
                                    @"felds": @[feldDictionariesArray[i]]}];
    [self.systemModel.surfaces addObject:surface];
  }

  return self.systemModel;
}

- (MZSystemModel*)createTestModelWithOneSingleFeldAndOneExtendedWindshieldSurfaceWithOneFeld
{
  NSDictionary *feldDictionary = @{@"id" : @"main",
                                   @"type" : @"visible",
                                   @"cent" : [MZVect doubleVectWithX:0.0 y:100.0 z:-2100.0],
                                   @"up"   : [MZVect doubleVectWithX:0.0 y:1.0 z:0.0],
                                   @"over" : [MZVect doubleVectWithX:1.0 y:0.0 z:0.0],
                                   @"norm" : [MZVect doubleVectWithX:0.0 y:0.0 z:1.0],
                                   @"phys-size" : [MZVect doubleVectWithX:1200.0 y:675.0],
                                   @"px-size" : [MZVect integerVectWithX:1920 y:1080],
                                   @"view-dist" : @1450,
                                   @"visible-to-all" : @YES };

  MZFeld *mainFeld = [MZFeld feldWithName:@"main"];

  [mainFeld updateWithDictionary:feldDictionary];


  MZSurface *mainSurface = [[MZSurface alloc] initWithName:@"main"];
  [mainSurface updateWithDictionary:@{@"bounding-feld": feldDictionary,
                                      @"felds": @[feldDictionary]}];
  [self.systemModel.surfaces addObject:mainSurface];


  NSDictionary *extendedSurfaceFeldDictionary = @{@"id" : @"main",
                                                  @"type" : @"visible",
                                                  @"cent" : [MZVect doubleVectWithX:0.0 y:100.0 z:-2100.0],
                                                  @"up"   : [MZVect doubleVectWithX:0.0 y:1.0 z:0.0],
                                                  @"over" : [MZVect doubleVectWithX:1.0 y:0.0 z:0.0],
                                                  @"norm" : [MZVect doubleVectWithX:0.0 y:0.0 z:1.0],
                                                  @"phys-size" : [MZVect doubleVectWithX:1200.0 y:675.0],
                                                  @"px-size" : [MZVect integerVectWithX:1920 y:1080],
                                                  @"view-dist" : @1450,
                                                  @"visible-to-all" : @YES };

  MZSurface *extendedSurface = [[MZSurface alloc] initWithName:@"surface-1"];

  [extendedSurface updateWithDictionary:@{@"bounding-feld": extendedSurfaceFeldDictionary,
                                          @"felds": @[extendedSurfaceFeldDictionary]}];

  [self.systemModel.surfaces addObject:extendedSurface];

  return self.systemModel;
}

- (MZSystemModel*)createTestModelWithTriptychAndOneExtendedWindshieldSurfaceWithOneFeld
{

  MZFeld *mainFeld = [MZFeld feldWithName:@"main"];
  MZFeld *leftFeld = [MZFeld feldWithName:@"left"];
  MZFeld *rightFeld = [MZFeld feldWithName:@"right"];

  NSDictionary *leftFeldDictionary = @{@"id" : @"left",
                                       @"type" : @"visible",
                                       @"cent" : [MZVect doubleVectWithX:-1212.0 y:100.0 z:-2100.0],
                                       @"up"   : [MZVect doubleVectWithX:0.0 y:1.0 z:0.0],
                                       @"over" : [MZVect doubleVectWithX:1.0 y:0.0 z:0.0],
                                       @"norm" : [MZVect doubleVectWithX:0.0 y:0.0 z:1.0],
                                       @"phys-size" : [MZVect doubleVectWithX:1200.0 y:675.0],
                                       @"px-size" : [MZVect integerVectWithX:1920 y:1080],
                                       @"view-dist" : @1450,
                                       @"visible-to-all" : @YES };

  NSDictionary *mainFeldDictionary = @{@"id" : @"main",
                                       @"type" : @"visible",
                                       @"cent" : [MZVect doubleVectWithX:0.0 y:100.0 z:-2100.0],
                                       @"up"   : [MZVect doubleVectWithX:0.0 y:1.0 z:0.0],
                                       @"over" : [MZVect doubleVectWithX:1.0 y:0.0 z:0.0],
                                       @"norm" : [MZVect doubleVectWithX:0.0 y:0.0 z:1.0],
                                       @"phys-size" : [MZVect doubleVectWithX:1200.0 y:675.0],
                                       @"px-size" : [MZVect integerVectWithX:1920 y:1080],
                                       @"view-dist" : @1450,
                                       @"visible-to-all" : @YES };

  NSDictionary *rightFeldDictionary = @{@"id" : @"right",
                                        @"type" : @"visible",
                                        @"cent" : @[@1212.0, @100.0, @(-2100.0)],
                                        @"up"   : @[@0.0, @1.0, @0.0],
                                        @"over" : @[@1.0, @0.0, @0.0],
                                        @"norm" : @[@0.0, @0.0, @1.0],
                                        @"phys-size" : @[@1200.0, @675.0],
                                        @"px-size" : @[@1920, @1080],
                                        @"view-dist" : @1450,
                                        @"visible-to-all" : @YES };

  [leftFeld updateWithDictionary:leftFeldDictionary];
  [mainFeld updateWithDictionary:mainFeldDictionary];
  [rightFeld updateWithDictionary:rightFeldDictionary];

  MZSurface *mainSurface = [[MZSurface alloc] initWithName:@"main"];
  [mainSurface updateWithDictionary:@{@"bounding-feld": @{@"id" : @"bounding-feld",
                                                          @"type" : @"visible",
                                                          @"cent" : [MZVect doubleVectWithX:1200.0 y:100.0 z:-2100.0],
                                                          @"up"   : [MZVect doubleVectWithX:0.0 y:1.0 z:0.0],
                                                          @"over" : [MZVect doubleVectWithX:1.0 y:0.0 z:0.0],
                                                          @"norm" : [MZVect doubleVectWithX:0.0 y:0.0 z:1.0],
                                                          @"phys-size" : [MZVect doubleVectWithX:3624.0 y:675.0],
                                                          @"px-size" : [MZVect integerVectWithX:5760 y:1080],
                                                          @"view-dist" : @1450,
                                                          @"visible-to-all" : @YES },
                                      @"felds": @[leftFeldDictionary, mainFeldDictionary, rightFeldDictionary]}];
  [self.systemModel.surfaces addObject:mainSurface];

  NSDictionary *extendedSurfaceFeldDictionary = @{@"id" : @"main",
                                                  @"type" : @"visible",
                                                  @"cent" : [MZVect doubleVectWithX:0.0 y:100.0 z:-2100.0],
                                                  @"up"   : [MZVect doubleVectWithX:0.0 y:1.0 z:0.0],
                                                  @"over" : [MZVect doubleVectWithX:1.0 y:0.0 z:0.0],
                                                  @"norm" : [MZVect doubleVectWithX:0.0 y:0.0 z:1.0],
                                                  @"phys-size" : [MZVect doubleVectWithX:1200.0 y:675.0],
                                                  @"px-size" : [MZVect integerVectWithX:1920 y:1080],
                                                  @"view-dist" : @1450,
                                                  @"visible-to-all" : @YES };

  MZSurface *extendedSurface = [[MZSurface alloc] initWithName:@"surface-1"];

  [extendedSurface updateWithDictionary:@{@"bounding-feld": extendedSurfaceFeldDictionary,
                                          @"felds": @[extendedSurfaceFeldDictionary]}];

  [self.systemModel.surfaces addObject:extendedSurface];
  
  return self.systemModel;
}


#pragma mark - LiveStreams content

- (void)addLiveStreamsToCurrentWorkspace:(NSInteger)numberOfLiveStreams
{
  for (int c = 0; c < numberOfLiveStreams; c++) {
    //  local: vi-bmagic-0/_local_
    //  VTC: vi-teleconf/_vtc_
    //  remote: vi-bmagic-2/8vW5f_VlRv68dzVrYp7ibQ
    //  screencast: vi-decoded-reach-vid-5796deb5-dad6-5a9a-87d1-b790de789361-189060094750155981/_local_
  
    MZLiveStream *liveStream = [MZLiveStream new];
    [liveStream setUid:[NSString stringWithFormat:@"vi-bmagic-%i/_local_]", c]];
    liveStream.active = YES;
    [liveStream setValue:@YES forKey:@"local"];
    
    [_systemModel.currentWorkspace insertObject:liveStream inLiveStreamsAtIndex:c];
  }
}


- (void)removeLiveStreamsAtIndex:(NSInteger)index
{
  if (_systemModel.currentWorkspace.liveStreams.count - 1 >= index) {
    [_systemModel.currentWorkspace removeObjectFromLiveStreamsAtIndex:index];
  }
}


- (void)removeAllLiveStreams
{
  [_systemModel.currentWorkspace removeAllLiveStreams];
}


#pragma mark - Presentation slides

- (void)addSlidesToCurrentWorkspace:(NSInteger)numberOfSlides
{
  for (NSInteger c = 0; c < numberOfSlides; c++) {
    
    MZPortfolioItem * item = [MZPortfolioItem new];
    item.index = c;
    item.uid = [NSString stringWithFormat:@"uid-%li", (long)c];
    item.contentSource = [NSString stringWithFormat:@"contentSource-%li", (long)c];

    [_systemModel.currentWorkspace.presentation insertObject:item inSlidesAtIndex:c];
  }
  
}


- (void)removeSlidesAtIndex:(NSInteger)index
{
  if (_systemModel.currentWorkspace.presentation.numberOfSlides - 1 >= index) {
    [_systemModel.currentWorkspace.presentation removeObjectFromSlidesAtIndex:index];
  }
}


- (void)removeAllSlides
{
  [_systemModel.currentWorkspace.presentation removeAllSlides];
}

@end
