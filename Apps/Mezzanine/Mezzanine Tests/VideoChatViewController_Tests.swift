//
//  VideoChatViewController_Tests.swift
//  Mezzanine
//
//  Created by miguel on 8/7/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

import XCTest
@testable import Mezzanine

class VideoChatViewController_Tests: XCTestCase {

  var viewController: VideoChatViewController!
  let pexipManager = MockPexipManager()

  override func setUp() {
    
    super.setUp()

    let infopresenceModel = MZInfopresence()
    pexipManager.infopresence = infopresenceModel

    viewController = VideoChatViewController.init(pexipManager: pexipManager)
    viewController?.view
  }

  override func tearDown() {

    super.tearDown()
  }

//  func testJoinCalls() {
//
//    let joinInvocationDidNotHappened = pexipManager.joinInvocations.count == 0
//    XCTAssertTrue(joinInvocationDidNotHappened, "It should not have joined yet")
//
//    viewController.startJoiningProcess()
//
//    let joinInvocationHappened = pexipManager.joinInvocations.count == 1
//    XCTAssertTrue(joinInvocationHappened, "It should have joined already")
//  }

  func testLeaveCall() {

    let leaveInvocationDidNotHappened = pexipManager.leaveInvocations.count == 0
    XCTAssertTrue(leaveInvocationDidNotHappened, "It should not have left yet")

    viewController = nil
    
    // For some reason 32-bit devices needs to fake a delay for the 'deinit' to be call in Swift
    let delay = DispatchTime.now() + Double(Int64(0.0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
    DispatchQueue.main.asyncAfter(deadline: delay)  {
      let leaveInvocationHappened = self.pexipManager.leaveInvocations.count == 1
      XCTAssertTrue(leaveInvocationHappened, "It should have left already")
    }
  }
}
