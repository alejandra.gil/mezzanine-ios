//
//  XCTestCase+Animations.m
//  Mezzanine
//
//  Created by Miguel Sánchez Valdés on 08/10/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

#import "XCTestCase+Animations.h"

@implementation XCTestCase (Animations)

-(void) waitForAnimations:(CGFloat)time
{
  NSTimeInterval timeout = time * 2 + 0.1;
  NSDate *loopUntil = [NSDate dateWithTimeIntervalSinceNow:timeout];
  while ([loopUntil timeIntervalSinceNow] > 0)
    [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:loopUntil];
}

@end
