//
//  WindshieldViewController_Tests.m
//  Mezzanine
//
//  Created by Miguel Sánchez Valdés on 06/10/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "WindshieldViewController.h"
#import "WindshieldViewController_Private.h"
#import "MockMezzanineNavigationController.h"
#import "MockSystemModel.h"
#import "ImageAssetView.h"
#import "VideoAssetView.h"
#import <OCMock/OCMock.h>
#import "MezzanineAppContext.h"
#import "MezzanineMainViewController.h"
#import "UIViewController+FPPopover.h"
#import "ButtonMenuViewController.h"
#import "MezzanineStyleSheet.h"
#import "AssetViewController.h"
#import "UIImage+Additions.h"
#import "XCTestCase+Animations.h"
#import "WorkspaceContentScrollView.h"
#import "Mezzanine-Swift.h"

@interface WindshieldViewController (debug)

- (void)checkShielderGrab:(MZWindshieldItem*)item;
- (void)checkShielderRelinquish:(MZWindshieldItem*)item;

@end

@interface WindshieldViewController_Tests : XCTestCase
{
  WindshieldViewController *_controller;
  MockSystemModel *_systemModelFactory;
  MockMezzanineNavigationController *_mockNavigationController;
}

@end


@implementation WindshieldViewController_Tests

- (void)setUp
{
  [super setUp];

  _systemModelFactory = [MockSystemModel new];
  _controller = [[WindshieldViewController alloc] init];
  _mockNavigationController = [[MockMezzanineNavigationController alloc] initWithRootViewController:_controller];
  [MezzanineAppContext currentContext].mainNavController = _mockNavigationController;
}

- (void)tearDown
{
  _controller.windshield = nil;
  _controller.workspace = nil;
  _controller.systemModel = nil;
  _controller = nil;
  _systemModelFactory = nil;

  [MezzanineAppContext currentContext].currentCommunicator = nil;
  [MezzanineAppContext currentContext].mainNavController = [MezzanineNavigationController new];

  [super tearDown];
}


#pragma mark - OBOvumSource

- (void)testCreateOvumFromImageAssetView
{
  CGRect frameOfView = CGRectMake(0, 0, 20, 20);
  ImageAssetView *imageView = [[ImageAssetView alloc] initWithFrame:frameOfView];

  OBOvum *ovum = [_controller createOvumFromView:imageView];

  XCTAssertNotNil(ovum, @"ovum should exist");
  XCTAssertFalse(ovum.isCentered, @"ovum should not be centered under the finger after creation");
  XCTAssertTrue(ovum.shouldScale, @"ovum should be scalable");

  CGRect frameBeforeDrag = [[_controller valueForKey:@"frameBeforeDrag"] CGRectValue];
  XCTAssertTrue(CGRectEqualToRect(frameBeforeDrag, frameOfView), @"frameBeforeDrag should be %@ but it is %@", NSStringFromCGRect(frameOfView), NSStringFromCGRect(frameBeforeDrag));
  XCTAssertFalse([[_controller valueForKey:@"hasExitedFromWindshield"] boolValue], @"hasExitedFromWindshield should be false but it is true");
  XCTAssertEqual(_controller.draggedView, imageView, @"Windshield draggedView should be the same as the one used for creating the ovum");
}

- (void)testCreateOvumFromVideoAssetView
{
  CGRect frameOfView = CGRectMake(0, 0, 20, 20);
  VideoAssetView *videoView = [[VideoAssetView alloc] initWithFrame:frameOfView];

  OBOvum *ovum = [_controller createOvumFromView:videoView];

  XCTAssertNotNil(ovum, @"ovum should exist");
  XCTAssertFalse(ovum.isCentered, @"ovum should not be centered under the finger after creation");
  XCTAssertTrue(ovum.shouldScale, @"ovum should be scalable");

  CGRect frameBeforeDrag = [[_controller valueForKey:@"frameBeforeDrag"] CGRectValue];
  XCTAssertTrue(CGRectEqualToRect(frameBeforeDrag, frameOfView), @"frameBeforeDrag should be %@ but it is %@", NSStringFromCGRect(frameOfView), NSStringFromCGRect(frameBeforeDrag));
  XCTAssertFalse([[_controller valueForKey:@"hasExitedFromWindshield"] boolValue], @"hasExitedFromWindshield should be false but it is true");
  XCTAssertEqual(_controller.draggedView, videoView, @"Windshield draggedView should be the same as the one used for creating the ovum");
}

- (void)testCreateOvumFromAnotherKindOfView
{
  CGRect frameOfView = CGRectMake(0, 0, 20, 20);
  UIView *view = [[UIView alloc] initWithFrame:frameOfView];

  OBOvum *ovum = [_controller createOvumFromView:view];

  XCTAssertNil(ovum, @"ovum should exist");
  XCTAssertNotEqual(_controller.draggedView, view, @"Windshield draggedView should not be the same as the one used for creating the ovum");
}

- (void)testCreateDragRepresentationOfImageSourceView
{
  CGRect frameOfAsset = CGRectMake(20, 20, 70, 70);
  MZWindshieldItem *item = [self setupItemInWindshield];
  ImageAssetView *originalAssetView = [ImageAssetView new];
  originalAssetView.asset = item;
  originalAssetView.frame = frameOfAsset;

  UIView *resultingView = [_controller createDragRepresentationOfSourceView:originalAssetView inWindow:[OBDragDropManager sharedManager].overlayWindow];
  XCTAssertTrue([resultingView isKindOfClass:[ImageAssetView class]], @"The resulting image view should be of class ImageAssetView");

  ImageAssetView *resultingImageAssetView = (ImageAssetView *)resultingView;
  XCTAssertTrue([resultingImageAssetView.contentView.backgroundColor isEqual:[MezzanineStyleSheet sharedStyleSheet].ovumBackgroundColor], @"Content view background color should have the ovum background color defined in Mezzanine Style Sheet");
  XCTAssertTrue([resultingImageAssetView.imageView.backgroundColor isEqual:[UIColor clearColor]], @"Image view background color should transparent");
  XCTAssertEqual(resultingImageAssetView.asset, item, @"Ovum dragged view item should be the same than the source view");
  XCTAssertFalse(resultingImageAssetView.userInteractionEnabled, @"Ovum dragged view user interaction should be disabled");
  XCTAssertTrue(CGSizeEqualToSize(frameOfAsset.size, resultingImageAssetView.frame.size), @"Ovum dragged view size should be the same as the original asset view");
}

- (void)testCreateDragRepresentationOfVideoSourceView
{
  CGRect frameOfAsset = CGRectMake(20, 20, 70, 70);
  NSString *sourceName = @"source-name";
  NSString *sourceOrigin = @"source-origin";
  BOOL isLocalVideo = YES;

  MZWindshieldItem *item = [self setupItemInWindshield];
  MZLiveStream *liveStreamInItem = [MZLiveStream new];
  VideoAssetView *originalAssetView = [VideoAssetView new];
  originalAssetView.frame = frameOfAsset;
  originalAssetView.asset = item;
  originalAssetView.liveStream = liveStreamInItem;
  originalAssetView.placeholderView.sourceName = sourceName;
  originalAssetView.placeholderView.sourceOrigin = sourceOrigin;
  originalAssetView.placeholderView.isLocalVideo = isLocalVideo;
  originalAssetView.imageView.backgroundColor = [UIColor redColor];

  UIView *resultingView = [_controller createDragRepresentationOfSourceView:originalAssetView inWindow:[OBDragDropManager sharedManager].overlayWindow];
  XCTAssertTrue([resultingView isKindOfClass:[VideoAssetView class]], @"The resulting image view should be of class VideoAssetView");

  VideoAssetView *resultingImageAssetView = (VideoAssetView *)resultingView;
  XCTAssertTrue([resultingImageAssetView.contentView.backgroundColor isEqual:[UIColor clearColor]], @"Content view background color should have the ovum background color defined in Mezzanine Style Sheet");
  XCTAssertTrue([resultingImageAssetView.imageView.backgroundColor isEqual:originalAssetView.imageView.backgroundColor], @"Image view background color should transparent");
  XCTAssertEqual(resultingImageAssetView.asset, item, @"Ovum dragged view item should be the same than the source view");
  XCTAssertEqual(resultingImageAssetView.liveStream, liveStreamInItem, @"Ovum dragged view livestream should be the same than the source view");
  XCTAssertFalse(resultingImageAssetView.userInteractionEnabled, @"Ovum dragged view user interaction should be disabled");
  XCTAssertTrue(CGSizeEqualToSize(frameOfAsset.size, resultingImageAssetView.frame.size), @"Ovum dragged view size should be the same as the original asset view");
  XCTAssertEqual(resultingImageAssetView.placeholderView.sourceName, sourceName, @"Placeholder view source name should be %@ but it is %@", sourceName, resultingImageAssetView.placeholderView.sourceName);
  XCTAssertEqual(resultingImageAssetView.placeholderView.sourceOrigin, sourceOrigin, @"Placeholder view source origin should be %@ but it is %@", sourceOrigin, resultingImageAssetView.placeholderView.sourceOrigin);
  XCTAssertEqual(resultingImageAssetView.placeholderView.isLocalVideo, isLocalVideo, @"Placeholder should be of a local video but it is not");
}

- (MZWindshieldItem *)setupItemInWindshield
{
  MZWindshieldItem *item = [MZWindshieldItem new];
  item.uid = @"windshield-item-uid";
  return item;
}

- (void)setupWindshieldWithDraggedViewWithItem:(MZWindshieldItem *)item
{
  ImageAssetView *draggedView = [ImageAssetView new];
  draggedView.alpha = 1.0;
  draggedView.asset = item;
  _controller.draggedView = draggedView;
}

- (OBOvum *)setupOvumWithItem:(MZWindshieldItem *)item
{
  ImageAssetView *draggedView = [ImageAssetView new];
  draggedView.alpha = 1.0;
  draggedView.asset = item;

  OBOvum *ovum = [[OBOvum alloc] init];
  ovum.dataObject = item;
  ovum.dragView = draggedView;

  return ovum;
}

- (void)testOvumDragWillBegin
{
  MZWindshieldItem *item = [self setupItemInWindshield];
  [self setupWindshieldWithDraggedViewWithItem:item];
  OBOvum *ovum = [self setupOvumWithItem:item];
  ovum.dragView.alpha = 0.5;

  CGPoint testLocation = CGPointMake(100, 100);
  [_controller dragViewWillAppear:ovum.dragView inWindow:[OBDragDropManager sharedManager].overlayWindow atLocation:testLocation];

  XCTAssertTrue(ovum.dragView.alpha == 1.0, @"Ovum dragged view should be completely not transparent");
  XCTAssertTrue(CGPointEqualToPoint(ovum.dragView.center, testLocation), @"Ovum dragged view should be centered at provided location");
}

- (void)testOvumDragEndedOutsideAValidDropZoneWithAWindshieldItem
{
  MZWindshieldItem *item = [self setupItemInWindshield];
  [self setupWindshieldWithDraggedViewWithItem:item];
  OBOvum *ovum = [self setupOvumWithItem:item];
  ovum.dropAction = OBDropActionNone;

  [_controller checkShielderGrab:item];

  id requestor = [OCMockObject mockForClass:[MZRequestor class]];
  [[requestor expect] requestWindshieldItemRelinquishRequest:_controller.workspace.uid uid:item.uid];
  MZCommunicator *communicator = [MZCommunicator new];
  communicator.requestor = requestor;
  [_controller setCommunicator:communicator];

  [_controller ovumDragEnded:ovum];

  [requestor verify];

  XCTAssertNil(_controller.draggedView, @"Windshield draggedView should be nil");
}


- (void)testOvumDragEndedOutsideAValidDropZoneWithAnotherKindOfItem
{
  MZPortfolioItem *item = [MZPortfolioItem new];
  item.uid = @"portfolio-item-uid";

  OBOvum *ovum = [[OBOvum alloc] init];
  ovum.dropAction = OBDropActionNone;
  ovum.dataObject = item;

  id mezzRequestor = [OCMockObject mockForClass:[MZCommunicatorRequestor class]];
  [[mezzRequestor reject] requestPortfolioItemRelinquishRequest:_controller.workspace.uid uid:item.uid];
  [MezzanineAppContext currentContext].currentCommunicator.requestor = mezzRequestor;

  [_controller ovumDragEnded:ovum];

  [mezzRequestor verify];

  XCTAssertNil(_controller.draggedView, @"Windshield draggedView should be nil");
}

- (void)testOvumDragEndedOnDeleteArea
{
  MZWindshieldItem *item = [self setupItemInWindshield];
  [self setupWindshieldWithDraggedViewWithItem:item];
  OBOvum *ovum = [self setupOvumWithItem:item];
  ovum.dropAction = OBDropActionDelete;

  [_controller checkShielderGrab:item];
  [_controller ovumDragEnded:ovum];

  XCTAssertNotNil(_controller.draggedView, @"Windshield draggedView should not be nil");
}

- (void)testOvumDragEndedInsideAnotherValidDropZone
{
  UIView *anotherValidDropZoneView = [UIView new];

  MZWindshieldItem *item = [self setupItemInWindshield];
  [self setupWindshieldWithDraggedViewWithItem:item];
  OBOvum *ovum = [self setupOvumWithItem:item];
  ovum.dropAction = OBDropActionCopy;
  ovum.currentDropHandlingView = anotherValidDropZoneView;

  [_controller checkShielderGrab:item];
  [_controller ovumDragEnded:ovum];

  XCTAssertNil(_controller.draggedView, @"Windshield draggedView should be nil");
}


- (void)testOvumDragEndedAfterFingersHaveLeftScreen
{
  MZWindshieldItem *item = [self setupItemInWindshield];
  [self setupWindshieldWithDraggedViewWithItem:item];
  OBOvum *ovum = [self setupOvumWithItem:item];
  ovum.dropAction = OBDropActionCopy;
  ovum.currentDropHandlingView = _controller.view;

  [_controller checkShielderGrab:item];
  [_controller ovumDragEnded:ovum];

  XCTAssertTrue(ovum.dragView.alpha == 0.0, @"Ovum dragged view should be have an alpha chanel = 0.0");
  XCTAssertNil(_controller.draggedView, @"Windshield draggedView should be nil");
}

- (void)testOvumDragEndedCorrectlyInsideTheWindshield
{
  MZWindshieldItem *item = [self setupItemInWindshield];
  [self setupWindshieldWithDraggedViewWithItem:item];
  OBOvum *ovum = [self setupOvumWithItem:item];
  ovum.dropAction = OBDropActionCopy;
  ovum.currentDropHandlingView = _controller.view;
  ovum.dragView = nil;

  [_controller checkShielderGrab:item];
  [_controller ovumDragEnded:ovum];

  XCTAssertNotNil(_controller.draggedView, @"Windshield draggedView should not be nil");
}


#pragma mark - OBDropZone

- (void)testOvumEnteredLiveStream
{
  id ob = [OCMockObject niceMockForClass:[OBOvum class]];
  [[[ob stub] andReturn:[MZLiveStream new]] dataObject];

  XCTAssertEqual(OBDropActionCopy, [_controller ovumEntered:ob inView:_controller.view atLocation:CGPointZero]);
}

- (void)testOvumEnteredMZAsset
{
  id ob = [OCMockObject niceMockForClass:[OBOvum class]];
  [[[ob stub] andReturn:[MZAsset new]] dataObject];

  XCTAssertEqual(OBDropActionCopy, [_controller ovumEntered:ob inView:_controller.view atLocation:CGPointZero]);
}

- (void)testOvumEnteredPortfolioItem
{
  id ob = [OCMockObject niceMockForClass:[OBOvum class]];
  [[[ob stub] andReturn:[MZPortfolioItem new]] dataObject];

  id workspaceViewController = [WorkspaceViewController new];
  _controller.workspaceViewController = workspaceViewController;

  XCTAssertEqual(OBDropActionCopy, [_controller ovumEntered:ob inView:_controller.view atLocation:CGPointZero]);
}

- (void)testOvumEnteredNonValidItem
{
  id ob = [OCMockObject niceMockForClass:[OBOvum class]];
  [[[ob stub] andReturn:[MZFeld new]] dataObject];

  XCTAssertEqual(OBDropActionNone, [_controller ovumEntered:ob inView:_controller.view atLocation:CGPointZero]);
}

- (void)testOvumEnteredMZWindshieldItemAfterExiting
{
  id ob = [OCMockObject niceMockForClass:[OBOvum class]];
  [[[ob stub] andReturn:[MZWindshieldItem new]] dataObject];

  CGRect frameOfOvumDragView = CGRectMake(20, 20, 50, 50);
  UIView *ovumDraggedView = [[UIView alloc] initWithFrame:frameOfOvumDragView];
  [[[ob stub] andReturn:ovumDraggedView] dragView];

  ImageAssetView *draggedView = [[ImageAssetView alloc] initWithFrame:CGRectMake(120, 120, 50, 50)];
  draggedView.alpha = 0.0;
  _controller.draggedView = draggedView;

  [_controller setValue:@YES forKey:@"hasExitedFromWindshield"];

  XCTAssertEqual(OBDropActionCopy, [_controller ovumEntered:ob inView:_controller.view atLocation:CGPointZero]);

  XCTAssertTrue(CGRectEqualToRect(_controller.draggedView.frame, frameOfOvumDragView), @"frameBeforeDrag should be %@ but it is %@", NSStringFromCGRect(frameOfOvumDragView), NSStringFromCGRect(_controller.draggedView.frame));
  XCTAssertFalse([[_controller valueForKey:@"hasExitedFromWindshield"] boolValue], @"hasExitedFromWindshield should be false but it is true");
  XCTAssertTrue(_controller.draggedView.alpha == 1.0, @"Windshield dragged view should be have an alpha chanel = 1.0");
}


- (void)testOvumExited
{
  id ob = [OCMockObject niceMockForClass:[OBOvum class]];
  [[[ob stub] andReturn:[MZWindshieldItem new]] dataObject];

  ImageAssetView *draggedView = [[ImageAssetView alloc] initWithFrame:CGRectMake(120, 120, 50, 50)];
  draggedView.alpha = 0.0;
  _controller.draggedView = draggedView;

  [_controller setValue:@NO forKey:@"hasExitedFromWindshield"];

  [_controller ovumExited:ob inView:_controller.view atLocation:CGPointZero];

  XCTAssertTrue([[_controller valueForKey:@"hasExitedFromWindshield"] boolValue], @"hasExitedFromWindshield should be false but it is true");
  XCTAssertTrue(_controller.draggedView.alpha == 0.0, @"Windshield dragged view should be have an alpha chanel = 1.0");
}


- (void)testOvumMovedWindshieldItem
{
  MZWindshieldItem *item = [self setupItemInWindshield];
  [self setupWindshieldWithDraggedViewWithItem:item];
  OBOvum *ovum = [self setupOvumWithItem:item];
  ovum.currentDropHandlingView = _controller.view;

  CGRect newFrame = CGRectMake(20, 20, 50, 50);
  ovum.dragView.frame = newFrame;
  _controller.draggedView.frame = CGRectMake(120, 120, 50, 50);

  XCTAssertEqual(OBDropActionCopy, [_controller ovumMoved:ovum inView:_controller.view atLocation:CGPointZero]);
  XCTAssertTrue(CGRectEqualToRect(_controller.draggedView.frame, newFrame), @"Widnshield draggedView should be %@ but it is %@", NSStringFromCGRect(newFrame), NSStringFromCGRect(_controller.draggedView.frame));
}

- (void)testOvumMovedLiveStream
{
  MZLiveStream *item = [MZLiveStream new];
  item.uid = @"livestream-uid";
  OBOvum *ovum = [[OBOvum alloc] init];
  ovum.dataObject = item;
  XCTAssertEqual(OBDropActionCopy, [_controller ovumMoved:ovum inView:_controller.view atLocation:CGPointZero]);
}

- (void)testOvumMovedPortfolioItem
{
  MZPortfolioItem *item = [MZPortfolioItem new];
  item.uid = @"portfolio-item-uid";
  OBOvum *ovum = [[OBOvum alloc] init];
  ovum.dataObject = item;
  XCTAssertEqual(OBDropActionCopy, [_controller ovumMoved:ovum inView:_controller.view atLocation:CGPointZero]);
}

- (void)testOvumMovedNonValidItem
{
  MZWorkspace *item = [MZWorkspace new];
  item.uid = @"workspace-uid";
  OBOvum *ovum = [[OBOvum alloc] init];
  ovum.dataObject = item;
  XCTAssertEqual(OBDropActionNone, [_controller ovumMoved:ovum inView:_controller.view atLocation:CGPointZero]);
}


#pragma mark - UIGestureRecognizer Actions Tests

- (void)testWindshieldImageItemTapped
{
  MZWindshieldItem *item = [self setupItemInWindshield];
  ImageAssetView *tappedView = [ImageAssetView new];
  tappedView.asset = item;

  id gestureRecognizer = [OCMockObject niceMockForClass:[UIGestureRecognizer class]];
  [[[gestureRecognizer stub] andReturn:tappedView] view];

  [_controller windshieldItemTapped:gestureRecognizer];

  FPPopoverController *popover = [_controller currentPopover];
  XCTAssertNotNil(popover, @"A popover should have been created");

  XCTAssertTrue([popover.viewController isKindOfClass:[ButtonMenuViewController class]], @"The popover should contain a ButtonMenuViewController");

  ButtonMenuViewController *buttonMenuController = (ButtonMenuViewController *)popover.viewController;

  XCTAssertEqual(buttonMenuController.menuItems.count, 2, @"The menu should have 2 options but has %lu", (unsigned long)buttonMenuController.menuItems.count);

  for (ButtonMenuItem *item in buttonMenuController.menuItems)
  {
    XCTAssertTrue([item isKindOfClass:[ButtonMenuItem class]], @"All items in the menu should be of kind ButtonMenuItem");
  }

  XCTAssertTrue([[[buttonMenuController.menuItems lastObject] buttonColor] isEqual:[MezzanineStyleSheet sharedStyleSheet].redHighlightColor], @"The last item should be styled in red because it is a deletion button");
}

- (void)testWindshieldVideoItemTapped
{
  MZWindshieldItem *item = [self setupItemInWindshield];
  VideoAssetView *tappedView = [VideoAssetView new];
  tappedView.asset = item;

  id gestureRecognizer = [OCMockObject niceMockForClass:[UIGestureRecognizer class]];
  [[[gestureRecognizer stub] andReturn:tappedView] view];

  [_controller windshieldItemTapped:gestureRecognizer];

  FPPopoverController *popover = [_controller currentPopover];
  XCTAssertNotNil(popover, @"A popover should have been created");

  XCTAssertTrue([popover.viewController isKindOfClass:[ButtonMenuViewController class]], @"The popover should contain a ButtonMenuViewController");

  ButtonMenuViewController *buttonMenuController = (ButtonMenuViewController *)popover.viewController;

  XCTAssertEqual(buttonMenuController.menuItems.count, 3, @"The menu should have 3 options but has %lu", (unsigned long)buttonMenuController.menuItems.count);

  XCTAssertTrue([[buttonMenuController.menuItems firstObject] isKindOfClass:[ButtonMenuTextItem class]], @"The first item in the menu should be a the title (ButtonMenuTextItem)");
  XCTAssertTrue([buttonMenuController.menuItems[1] isKindOfClass:[ButtonMenuItem class]], @"The second item should be of kind ButtonMenuItem");
  XCTAssertTrue([buttonMenuController.menuItems[2] isKindOfClass:[ButtonMenuItem class]], @"The third item should be a of kind ButtonMenuItem");

  XCTAssertTrue([[[buttonMenuController.menuItems lastObject] buttonColor] isEqual:[MezzanineStyleSheet sharedStyleSheet].redHighlightColor], @"The last item should be styled in red because it is a deletion button");
}


#pragma mark - Asset Viewing Tests

- (void)testOpenAssetViewerForNilItem
{
  XCTAssertTrue(_mockNavigationController.pushViewControllerWithTransitionViewInvocations.count == 0);
  [_controller openAssetViewerForAssetView:nil];
  XCTAssertTrue(_mockNavigationController.pushViewControllerWithTransitionViewInvocations.count == 0);
}


- (void)testOpenAssetViewerForVideo
{
  XCTAssertTrue(_mockNavigationController.pushViewControllerWithTransitionViewInvocations.count == 0);

  VideoAssetView *asset = [VideoAssetView new];
  asset.liveStream = [MZLiveStream new];
  asset.liveStream.thumbnail = [UIImage new];
  
  [_controller openAssetViewerForAssetView:asset];
  XCTAssertTrue(_mockNavigationController.pushViewControllerWithTransitionViewInvocations.count == 1);
}


- (void)testOpenAssetViewerForImage
{
  XCTAssertTrue(_mockNavigationController.pushViewControllerWithTransitionViewInvocations.count == 0);

  ImageAssetView *asset = [ImageAssetView new];
  asset.asset = [MZAsset new];
  asset.imageView = [[UIImageView alloc] initWithImage:[UIImage imageOfSize:CGSizeMake(1, 1) filledWithColor:[UIColor redColor]]];
  
  [_controller openAssetViewerForAssetView:asset];
  XCTAssertTrue(_mockNavigationController.pushViewControllerWithTransitionViewInvocations.count == 1);
}


#pragma mark - UIGestureRecognizerDelegate Tests

- (void)testWindshieldGestureRecognizersShouldRecognizeSimultaneouslyWithWorkspaceContentScrollViewPanningGesture
{
  id panGestureRecognizer = [OCMockObject mockForClass:[UIPanGestureRecognizer class]];

  id workspaceContentScrollView = [OCMockObject mockForClass:[WorkspaceContentScrollView class]];
  [[[workspaceContentScrollView stub] andReturn:panGestureRecognizer] panGestureRecognizer];

  id workspaceViewController = [OCMockObject mockForClass:[WorkspaceViewController class]];
  [[[workspaceViewController stub] andReturn:workspaceContentScrollView] contentScrollView];

  _controller.workspaceViewController = workspaceViewController;

  XCTAssertTrue([_controller gestureRecognizer:[UIGestureRecognizer new] shouldRecognizeSimultaneouslyWithGestureRecognizer:panGestureRecognizer], @"Windshield should recognize simultaneously the panning gesture attached to the workspace content scroll view.");

  XCTAssertFalse([_controller gestureRecognizer:[UIGestureRecognizer new] shouldRecognizeSimultaneouslyWithGestureRecognizer:[UIGestureRecognizer new]], @"Windshield should not recognize simultaneously any other gesture.");
}


@end
