//
//  PlanarWorkspaceViewController_Tests.swift
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 27/07/2017.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import XCTest

@testable import Mezzanine

class MockPlanarWorkspaceViewController: PlanarWorkspaceViewController {
  
  override func mainSurface() -> MZSurface? {
    let surface = MZSurface(name: "main")
    surface?.felds.add(MZFeld())
    
    return surface
  }
  
}

class PlanarWorkspaceViewController_Tests: XCTestCase {
  
  var controller: MockPlanarWorkspaceViewController!
  
  override func setUp() {
    super.setUp()
    
    controller = MockPlanarWorkspaceViewController()
    
    _ = controller.view
  }
  
  override func tearDown() {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    super.tearDown()
  }

  
  func testUpdateWelcomeScreenIfPresentationIsNil() {
    // Forced initial setup
    controller.workspaceGeometry = WorkspaceGeometry()
    controller.fullWindshieldViewController = FullWindshieldViewController() 
    controller.fullWindshieldViewController!.windshield = MZWindshield()
    controller.containerScrollView = UIScrollView()
    controller.welcomeHintView = WelcomeHintView()
    controller.welcomeHintView.alpha = 0.0
    
    // Crash condition. Bug 18965
    controller.presentationViewController?.presentation = nil
    
    // Action
    controller.updateWelcomeHintView()
    
    
    // Expectations
    XCTAssertTrue(controller.welcomeHintView.alpha == 0.0)
    XCTAssertTrue(controller.welcomeHintView.frame.origin == CGPoint.zero)
    XCTAssertTrue(controller.welcomeHintView.frame.size == CGSize.zero)
  }
  
  func testUpdateWelcomeScreenIfPresentationIsAvailable() {
    // Forced initial setup
    controller.workspaceGeometry = WorkspaceGeometry()
    controller.fullWindshieldViewController = FullWindshieldViewController()
    controller.fullWindshieldViewController!.windshield = MZWindshield()
    controller.containerScrollView = UIScrollView()
    controller.presentationViewController = TWPresentationViewController()
    controller.presentationViewController!.presentation = MZPresentation()
    controller.welcomeHintView = WelcomeHintView()
    controller.welcomeHintView.alpha = 0.0
    
    // Action
    controller.updateWelcomeHintView()
    
    // Expectations
    XCTAssertTrue(controller.welcomeHintView.alpha == 1.0)
    XCTAssertFalse(controller.welcomeHintView.frame.origin != CGPoint.zero)
    XCTAssertFalse(controller.welcomeHintView.frame.size != CGSize.zero)
  }
}
