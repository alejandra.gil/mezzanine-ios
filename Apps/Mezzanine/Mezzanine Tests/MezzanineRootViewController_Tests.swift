//
//  MezzanineRootViewController_Tests.swift
//  Mezzanine
//
//  Created by miguel on 12/10/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

import XCTest

@testable import Mezzanine

class MezzanineRootViewController_Tests: XCTestCase {

  var controller: MezzanineRootViewController!
  var systemModelFactory: MockSystemModel?

  let delay = 0.5
  
  override func setUp() {
    super.setUp()

    systemModelFactory = MockSystemModel()
    let systemModel = MockSystemModel().createTestWithTriptych()

    let communicator = MockMZCommunicator()
    communicator.systemModel = systemModel

    let appContext = MezzanineAppContext.current()
    appContext?.currentCommunicator = communicator
    appContext?.applicationModel.systemModel = systemModel

    controller = MezzanineRootViewController.init(nibName: "MezzanineRootViewController", bundle: nil)
    controller.systemModel = systemModel
    controller.appContext = appContext
    controller.communicator = communicator

    MezzanineAppContext.current().applicationModel.systemModel.state = .workspace
    let navigationController = MezzanineNavigationController(rootViewController: controller);
    MezzanineAppContext.current().window.rootViewController = navigationController
    MezzanineAppContext.current().mainNavController = navigationController
    
    // Force view initialization
    _ = controller.view
    controller.viewWillAppear(false)
  }

  override func tearDown() {
    super.tearDown()
  }


  // MARK: Status Bar state

  func testStatusBarStateWithInfopresenceInitial() {
    XCTAssertFalse(controller.prefersStatusBarHidden)
    showInfopresenceMenu()

    let infopresenceMenuExistsPredicate = NSPredicate(format:"menuViewController != nil")
    expectation(for: infopresenceMenuExistsPredicate, evaluatedWith: controller.infopresenceMenuController!, handler: nil)
    waitForExpectations(timeout: 3.0, handler: nil)
    XCTAssertNotNil(controller.infopresenceMenuController!.menuViewController)

    let infopresenceMenuVisiblePredicate = NSPredicate(format:"isViewLoaded == true")
    expectation(for: infopresenceMenuVisiblePredicate, evaluatedWith: controller.infopresenceMenuController!.menuViewController!, handler: nil)
    waitForExpectations(timeout: 3.0, handler: nil)
    XCTAssertTrue(controller.infopresenceMenuController!.menuViewController!.isViewLoaded)

    let controllerPrefersStatusBarHiddenPredicate = NSPredicate(format:"prefersStatusBarHidden == false")
    expectation(for: controllerPrefersStatusBarHiddenPredicate, evaluatedWith: controller, handler: nil)
    waitForExpectations(timeout: 3.0, handler: nil)
    XCTAssertFalse(controller.prefersStatusBarHidden)
  }

  func testStatusBarStateWithInfopresenceAfterPushingRegularViewController() {

    if (controller.traitCollection.horizontalSizeClass == .compact) {
      return
    }

    XCTAssertFalse(controller.prefersStatusBarHidden)
    showInfopresenceMenu()

    let viewController = UIViewController()
    MezzanineAppContext.current().mainNavController.pushViewController(viewController, animated: false)

    let visibleViewControllerPredicate = NSPredicate(format:"visibleViewController == %@", viewController)
    expectation(for: visibleViewControllerPredicate, evaluatedWith: MezzanineAppContext.current().mainNavController, handler: nil)
    waitForExpectations(timeout: 3.0, handler: nil)
    XCTAssertEqual(MezzanineAppContext.current().mainNavController.visibleViewController, viewController)

    let controllerPrefersStatusBarHiddenPredicate = NSPredicate(format:"prefersStatusBarHidden == false")
    expectation(for: controllerPrefersStatusBarHiddenPredicate, evaluatedWith: controller, handler: nil)
    waitForExpectations(timeout: 3.0, handler: nil)
    XCTAssertFalse(controller.prefersStatusBarHidden)
  }

  func testStatusBarStateWithInfopresenceAfterPushingCompactViewController() {

    if (controller.traitCollection.horizontalSizeClass == .regular) {
     return
    }

    XCTAssertFalse(controller.prefersStatusBarHidden)
    showInfopresenceMenu()
    controller.infopresenceMenuController!.showInviteParticipantsMenu()

    let inviteParticipantsMenuExistsPredicate = NSPredicate(format:"inviteParticipantsViewController != nil")
    expectation(for: inviteParticipantsMenuExistsPredicate, evaluatedWith: controller.infopresenceMenuController!, handler: nil)
    waitForExpectations(timeout: 3.0, handler: nil)
    XCTAssertNotNil(controller.infopresenceMenuController!.inviteParticipantsViewController)

    let controllerPrefersStatusBarHiddenPredicate = NSPredicate(format:"prefersStatusBarHidden == false")
    expectation(for: controllerPrefersStatusBarHiddenPredicate, evaluatedWith: controller, handler: nil)
    waitForExpectations(timeout: 3.0, handler: nil)
    XCTAssertFalse(controller.prefersStatusBarHidden)
  }

  func testStatusBarStateWithInfopresenceAfterPushingAndPopping() {

    if (controller.traitCollection.horizontalSizeClass == .regular) {
      return
    }

    XCTAssertFalse(controller.prefersStatusBarHidden)
    showInfopresenceMenu()

    let viewController = UIViewController()
    MezzanineAppContext.current().mainNavController.pushViewController(viewController, animated: false)

    let visibleViewControllerPredicate = NSPredicate(format:"visibleViewController == %@", viewController)
    expectation(for: visibleViewControllerPredicate, evaluatedWith: MezzanineAppContext.current().mainNavController, handler: nil)
    waitForExpectations(timeout: 3.0, handler: nil)
    XCTAssertEqual(MezzanineAppContext.current().mainNavController.visibleViewController, viewController)

    MezzanineAppContext.current().mainNavController.popViewController(animated: false)

    let newVisibleViewControllerPredicate = NSPredicate(format:"visibleViewController == %@", controller)
    expectation(for: newVisibleViewControllerPredicate, evaluatedWith: MezzanineAppContext.current().mainNavController, handler: nil)
    waitForExpectations(timeout: 3.0, handler: nil)
    XCTAssertEqual(MezzanineAppContext.current().mainNavController.visibleViewController, controller)

    let controllerPrefersStatusBarHiddenPredicate = NSPredicate(format:"prefersStatusBarHidden == false")
    expectation(for: controllerPrefersStatusBarHiddenPredicate, evaluatedWith: controller, handler: nil)
    waitForExpectations(timeout: 3.0, handler: nil)
    XCTAssertFalse(controller!.prefersStatusBarHidden)
  }
  
  
  // MARK: SettingsFeatureToggles.sharedInstance.teamworkUI
  
  func testDroppedItemToDeleteToogleOn() {
    SettingsFeatureToggles.sharedInstance.teamworkUI = true
    controller.mezzanineMainViewController = MezzanineBaseRoomViewController()
    _ = controller.view
    
    let ovum = OBOvum()
    ovum.dataObject = MZWindshieldItem()
    controller!.ovumDropped(ovum, in: controller!.dragDropDeletionArea, atLocation: CGPoint.zero)
    
    let requestor = controller!.communicator.requestor as! MockMZCommunicatorRequestor
    XCTAssertTrue(requestor.transactionRequested!.transactionName == "requestWindshieldItemDelete")
  }


  // MARK: Private

  func showInfopresenceMenu() {
    let infopresenceMenuController = InfopresenceMenuController.init(containerViewController:controller!)
    infopresenceMenuController.delegate = controller;
    infopresenceMenuController.systemModel = controller!.systemModel;
    infopresenceMenuController.communicator = controller!.communicator;
    infopresenceMenuController.showMenuViewController()
    controller!.infopresenceMenuController = infopresenceMenuController
  }
}
