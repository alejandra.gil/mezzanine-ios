//
//  MezzanineConnectionViewController_Tests.m
//  Mezzanine
//
//  Created by Miguel Sánchez Valdés on 23/10/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import "MezzanineConnectionViewController.h"
#import "MezzanineConnectionViewController_Private.h"
#import "UIDevice+Machine.h"
#import "OBAppData.h"
#import "MezzanineAppContext.h"
#import "UITraitCollection+Additions.h"

@interface MockMZCommunicator : MZCommunicator

@property (nonatomic, assign) BOOL isARemoteParticipationConnection;

@end

@interface MezzanineConnectionViewController_Tests : XCTestCase
{
  MezzanineConnectionViewController *_controller;
}

@end

@implementation MezzanineConnectionViewController_Tests

- (void)setUp
{
  [super setUp];
  
  UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
  _controller = [storyboard instantiateViewControllerWithIdentifier:@"Connection View"];
  [_controller view];

  [MezzanineAppContext currentContext].applicationModel.systemModel.state = MZSystemStateNotConnected;
  [MezzanineAppContext currentContext].connectionViewController = _controller;

  NSString *bundlePath = [[NSBundle bundleForClass:[self class]] resourcePath];
  [NSBundle bundleWithPath:bundlePath];
}


- (void)tearDown
{
  _controller = nil;
  [super tearDown];
}


#pragma mark - IBOutlets Tests

- (void)testPoolNameField
{
  XCTAssertNotNil(_controller.poolNameField);
}


- (void)testDetailsButton
{
  XCTAssertNotNil(_controller.recentConnectionsButton);
}


- (void)testConnectButton
{
  XCTAssertNotNil(_controller.connectButton);
}


- (void)testActivityIndicator
{
  XCTAssertNotNil(_controller.activityIndicator);
}


- (void)testAppVersionLabel
{
  XCTAssertNotNil(_controller.appVersionLabel);
}


#pragma mark - UI Elements State Updates Tests

- (void)testPoolnameSanitization
{
  //URL based
  _controller.poolNameField.text = @"https://joinmz.com/m/123456789";
  XCTAssertTrue([[_controller sanitizePoolname] isEqualToString:@"joinmz.com/m/123456789"]);

  _controller.poolNameField.text = @"https://joinmz.com/m/123456789#QSA";
  XCTAssertTrue([[_controller sanitizePoolname] isEqualToString:@"joinmz.com/m/123456789#QSA"]);
  
  _controller.poolNameField.text = @"wss://joinmz.com/m/123456789";
  XCTAssertTrue([[_controller sanitizePoolname] isEqualToString:@"joinmz.com/m/123456789"]);
  
  _controller.poolNameField.text = @"https://mezzbcn";
  XCTAssertTrue([[_controller sanitizePoolname] isEqualToString:@"mezzbcn"]);
  
  //String based
  _controller.poolNameField.text = @"test.com";
  XCTAssertTrue([[_controller sanitizePoolname] isEqualToString:@"test.com"]);
  
  _controller.poolNameField.text = @"pumba#QWE";
  XCTAssertTrue([[_controller sanitizePoolname] isEqualToString:@"pumba#QWE"]);
}


- (void)checkInterfaceEnabled
{
  NSString *notConnectedTitle = NSLocalizedString(@"Connect Button Not Connected", nil);
  XCTAssertEqual([_controller.connectButton titleForState:UIControlStateNormal], notConnectedTitle, @"Connect Button label should say %@", notConnectedTitle);
  XCTAssertEqual([_controller.connectButton titleForState:UIControlStateDisabled], notConnectedTitle, @"Connect Button label should say %@", notConnectedTitle);
  XCTAssertFalse([_controller.activityIndicator isAnimating], @"Activity indicator should not be animating");
  XCTAssertTrue(_controller.poolNameField.enabled, @"poolNameField should be enabled");
  XCTAssertTrue(_controller.recentConnectionsButton.enabled, @"detailsButton should be enabled");
  XCTAssertTrue(_controller.connectButton.enabled, @"connectButton should be enabled");
  XCTAssertTrue(_controller.appVersionLabel.userInteractionEnabled, @"appVersionLabel user interaction should be enabled");
}


- (void)checkInterfaceDisabled
{
  NSString *connectingTitle = NSLocalizedString(@"Connect Button Connecting", nil);
  XCTAssertEqual([_controller.connectButton titleForState:UIControlStateNormal], connectingTitle, @"Connect Button label should say %@", connectingTitle);
  XCTAssertEqual([_controller.connectButton titleForState:UIControlStateDisabled], connectingTitle, @"Connect Button label should say %@", connectingTitle);
  XCTAssertTrue([_controller.activityIndicator isAnimating], @"Activity indicator should be animating");
  XCTAssertFalse(_controller.poolNameField.enabled, @"poolNameField should be disabled");
  XCTAssertFalse(_controller.recentConnectionsButton.enabled, @"detailsButton should be disabled");
  XCTAssertFalse(_controller.connectButton.enabled, @"connectButton should be disabled");
  XCTAssertFalse(_controller.appVersionLabel.userInteractionEnabled, @"appVersionLabel user interaction should be disabled");
}


- (void)testSetInterfaceEnabled
{
  [_controller setInterfaceEnabled:YES];
  [self checkInterfaceEnabled];

  [_controller setInterfaceEnabled:NO];
  [self checkInterfaceDisabled];
}


- (void)testUpdateConnectButton
{
  _controller.poolNameField.text = nil;
  [_controller updateConnectButton];
  XCTAssertFalse(_controller.connectButton.enabled, @"Emtpy pool name textfield should disable the connection button");

  _controller.poolNameField.text = @"test-pool-name";
  [_controller updateConnectButton];
  XCTAssertTrue(_controller.connectButton.enabled, @"Pool name textfield with text should enable the connection button");
}


- (void)testUpdateHistoryButton
{
  OBAppData *appData = [OBAppData sharedData];
  XCTAssertEqual(appData.recentPools.count, 0, @"Recent pools list should start empty");

  [_controller updateHistoryButton];
  XCTAssertTrue([_controller.recentConnectionsButton isHidden], @"If the recent pool list is empty, details button should be hidden");

  [appData noteRecentPool:@"test-pool-name"];

  [_controller updateHistoryButton];
  XCTAssertFalse([_controller.recentConnectionsButton isHidden], @"If the recent pool contains any item, details button should be visible");

  [appData clearRecentPools];
}


#pragma mark - Connection Process Tests

- (void)testConnect
{
  _controller.poolNameField.text = @"pool-test";

  [_controller connect];

  [self checkInterfaceDisabled];

  [[OBAppData sharedData] clearRecentPools];

  XCTAssertNil([_controller valueForKey:@"recentConnectionsPopover"], @"Recent connections popover should be gone by now");
}


- (void)testCancelConnection
{
  id delegateMock = [OCMockObject niceMockForProtocol:@protocol(MezzanineConnectionViewControllerDelegate)];
  [[delegateMock expect] connectionViewControllerWasCancelled:_controller];
  _controller.delegate = delegateMock;

  [_controller cancel];

  [delegateMock verify];
}


#pragma mark - URL Connection process

- (void)testConnectionFromURL
{
  // Start connection from URL
  XCTAssertNil(_controller.connectionAlertController);
  
  id delegateMock = [OCMockObject niceMockForProtocol:@protocol(MezzanineConnectionViewControllerDelegate)];
  [[delegateMock expect] connectionViewController:_controller requestsAsyncConnectionAttemptTo:OCMOCK_ANY
                                          success:OCMOCK_ANY
                                            error:OCMOCK_ANY];
  _controller.delegate = delegateMock;
  
  [_controller connectToServerAtURL:[NSURL URLWithString:@"tcp://pool-test"] from:OCMOCK_ANY];
  XCTAssertNotNil(_controller.connectionAlertController);
  [self checkInterfaceDisabled];
  [delegateMock verify];
  
  
  // Cancel a connection
  UIAlertAction *alertAction = _controller.connectionAlertController.actions.firstObject;
  void (^cancelActionBlock)(id obj) = [alertAction valueForKey:@"handler"];
  cancelActionBlock(alertAction);

  XCTAssertNil(_controller.connectionAlertController);
  [self checkInterfaceEnabled];
  
  [[OBAppData sharedData] clearRecentPools];
  XCTAssertNil([_controller valueForKey:@"recentConnectionsPopover"], @"Recent connections popover should be gone by now");
}


#pragma mark - Details (Recent Connection popover) Tests

- (void)testDetails
{
  [_controller showRecentConnections];

  FPPopoverController *popoverController = [_controller valueForKey:@"recentConnectionsPopover"];
  if (_controller.traitCollection.isRegularWidth && _controller.traitCollection.isRegularHeight)
  {
    XCTAssertNotNil(popoverController, @"Recent connections should be presented in a popover");
  }
  else
  {
    XCTAssertNil(popoverController, @"Recent connections should not be presented in a popover");
    XCTAssertNotEqual(_controller, [_controller.navigationController visibleViewController], @"Connection controller should have been replaced with the recent connections one");
  }
}


#pragma mark - UITextField delegate

- (void)testTextFieldShouldReturn
{
  _controller.poolNameField.text = @"pool-test";

  BOOL shouldReturn = [_controller textFieldShouldReturn:_controller.poolNameField];

  XCTAssertTrue(shouldReturn, @"poolNameField textFieldShouldReturn should return YES");
}


- (void)testTextFieldShouldBeginEditing
{
  BOOL shouldBeginEditing = [_controller textFieldShouldBeginEditing:_controller.poolNameField];
  XCTAssertTrue(shouldBeginEditing, @"poolNameField textFieldShouldBeginEditing should return YES");
}

- (void)testTextFieldShouldChangeCharactersInRangeReplacementString
{
  BOOL shouldChangeCharactersInRangeReplacementString = [_controller textField:_controller.poolNameField shouldChangeCharactersInRange:NSMakeRange(0, 0) replacementString:@"pool-test"];
  XCTAssertTrue(shouldChangeCharactersInRangeReplacementString, @"poolNameField textFieldShouldBeginEditing should return YES");
}


#pragma mark - Stripping protocol

- (void)testHTTPSProtocolStrippedWhenPasted
{
  _controller.poolNameField.text = @"";
  _controller.connectButton.enabled = NO;

  NSString *fullServerURL = @"https://joinmz.com/m/12345";
  [UIPasteboard generalPasteboard].string = fullServerURL;

  BOOL shouldChangeCharactersInRangeReplacementString = [_controller textField:_controller.poolNameField shouldChangeCharactersInRange:NSMakeRange(0, 0) replacementString:fullServerURL];
  XCTAssertFalse(shouldChangeCharactersInRangeReplacementString, @"poolNameField textFieldShouldBeginEditing should return NO");
  XCTAssertTrue([_controller.poolNameField.text isEqualToString:@"joinmz.com/m/12345"], @"poolName should not contain 'https://' anymore");
  XCTAssertTrue(_controller.connectButton.enabled, @"Connect button should be enabled");

  [UIPasteboard generalPasteboard].string = @"";
}

- (void)testHTTPSProtocolNotStrippedWhenTyped
{
  _controller.poolNameField.text = @"";
  _controller.connectButton.enabled = NO;

  NSString *fullServerURL = @"https://joinmz.com/m/12345";

  BOOL shouldChangeCharactersInRangeReplacementString = [_controller textField:_controller.poolNameField shouldChangeCharactersInRange:NSMakeRange(0, 0) replacementString:fullServerURL];
  XCTAssertTrue(shouldChangeCharactersInRangeReplacementString, @"poolNameField textFieldShouldBeginEditing should return YES");
}

- (void)testHTTPSProtocolStrippedWhenConnecting
{
  _controller.poolNameField.text = @"https://joinmz.com/m/12345";
  _controller.connectButton.enabled = NO;

  [_controller connect];

  XCTAssertTrue([_controller.poolNameField.text isEqualToString:@"joinmz.com/m/12345"], @"poolName should not contain 'https://' anymore");
}


#pragma mark - Observation

- (void)testObservers
{
  NSMutableArray *observers = [_controller valueForKey:@"observationTokens"];
  XCTAssertNil(observers, @"There should be no observer");
  [_controller viewWillAppear:NO];
  observers = [_controller valueForKey:@"observationTokens"];
  XCTAssertEqual(observers.count, 5, @"There should be 5 observers");
  [_controller viewWillDisappear:NO];
  XCTAssertEqual(observers.count, 0, @"There should be no observer");
}


#pragma mark - RP URL storage

- (void)testSaveRPURLToRecentList
{
  id appDataMock = [OCMockObject partialMockForObject:[OBAppData sharedData]];
  [[appDataMock reject] noteRecentPool:OCMOCK_ANY];
  
  MezzanineAppContext *appContext = [MezzanineAppContext currentContext];
  MockMZCommunicator *communicator = [MockMZCommunicator new];
  communicator.isARemoteParticipationConnection = true;
  appContext.currentCommunicator = communicator;
  
  [_controller connectSucceeded:nil];
  [appDataMock verify];
}


- (void)testSaveMezzanineURLToRecentList
{
  id appDataMock = [OCMockObject partialMockForObject:[OBAppData sharedData]];
  [[appDataMock expect] noteRecentPool:OCMOCK_ANY];
  
  MezzanineAppContext *appContext = [MezzanineAppContext currentContext];
  MZCommunicator *communicator = [MZCommunicator new];
  appContext.currentCommunicator = communicator;
  
  [_controller connectSucceeded:nil];
  [appDataMock verify];
}


#pragma mark - Set last connected Mezzanine

- (void)testSetLastSavedMezzRP
{
  OBAppData *appData = [OBAppData sharedData];
  [appData clearRecentPools];
  
  MezzanineAppContext *appContext = [MezzanineAppContext currentContext];
  MockMZCommunicator *communicator = [MockMZCommunicator new];
  communicator.isARemoteParticipationConnection = true;
  appContext.currentCommunicator = communicator;

  _controller.poolNameField.text = @"https://mezz-in.com/m/12345";
  [_controller connect];
  XCTAssertTrue([_controller.poolNameField.text isEqualToString:@"mezz-in.com/m/12345"]);
  
  [_controller connectSucceeded:[NSURL URLWithString:@"mezz-in.com/m/12345"]];
  [_controller viewWillAppear:NO];
  XCTAssertTrue([_controller.poolNameField.text isEqualToString:@"mezz-in.com/m/12345"]);
  XCTAssertEqual(appData.recentPools.count, 0, @"Recent pools list should be empty");

}

- (void)testSetLastSavedMezz
{
  OBAppData *appData = [OBAppData sharedData];
  [appData clearRecentPools];
  
  MezzanineAppContext *appContext = [MezzanineAppContext currentContext];
  MockMZCommunicator *communicator = [MockMZCommunicator new];
  appContext.currentCommunicator = communicator;

  _controller.poolNameField.text = @"https://mezzbcn";
  [_controller connect];
  XCTAssertTrue([_controller.poolNameField.text isEqualToString:@"mezzbcn"]);
  
  [_controller connectSucceeded:[NSURL URLWithString:@"mezzbcn"]];
  [_controller viewWillAppear:NO];
  XCTAssertTrue([_controller.poolNameField.text isEqualToString:@"mezzbcn"]);
  XCTAssertEqual(appData.recentPools.count, 1, @"Recent pools list should have one pool");
  XCTAssertTrue([[appData.recentPools firstObject] isEqualToString:@"mezzbcn"]);

  [appData clearRecentPools];
}

@end
