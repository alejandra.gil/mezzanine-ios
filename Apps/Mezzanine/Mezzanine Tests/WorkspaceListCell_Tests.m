//
//  WorkspaceListCell_Tests.m
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 27/10/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "WorkspaceListCell.h"
#import "WorkspaceListCell_Private.h"

#import "MezzanineStyleSheet.h"
#import "ButtonMenuViewController.h"
#import "WorkspaceEditorViewController.h"

#import "MezzanineAppContext.h"

@interface WorkspaceListCell_Tests : XCTestCase

@property (nonatomic, strong) WorkspaceListCell *cell;

@end

static float CellSize = 200;

@implementation WorkspaceListCell_Tests

- (void)setUp
{
  [super setUp];
  
  _cell = [[WorkspaceListCell alloc] initWithFrame:(CGRect){CGPointZero,CGSizeMake(CellSize, CellSize)}];
  
  MZSystemModel *systemModel = [MZSystemModel new];
  _cell.systemModel = systemModel;

  MZCommunicator *communicator = [[MZCommunicator alloc] init];
  communicator.systemModel = systemModel;
  systemModel.myMezzanine.version = @"3.13";
  [MezzanineAppContext currentContext].currentCommunicator = communicator;
}


- (void)tearDown
{
  [MezzanineAppContext currentContext].currentCommunicator = nil;
  [super tearDown];
}


- (void)testSubviewsAdded
{
  NSArray *subviews = [_cell.contentView subviews];
  
  UIView *infoView = [_cell valueForKey:@"infoView"];
  UIView *actionsView = [_cell valueForKey:@"actionsView"];
  XCTAssertTrue([subviews containsObject:infoView]);
  XCTAssertTrue([subviews containsObject:actionsView]);
  
  XCTAssertTrue([[infoView subviews] containsObject:[_cell valueForKey:@"titleLabel"]]);
  XCTAssertTrue([[infoView subviews] containsObject:[_cell valueForKey:@"dateLabel"]]);
  XCTAssertTrue([[infoView subviews] containsObject:[_cell valueForKey:@"ownerLabel"]]);
  XCTAssertTrue([[infoView subviews] containsObject:[_cell valueForKey:@"imageView"]]);
  XCTAssertTrue([[actionsView subviews] containsObject:_cell.optionsButton]);
}


- (void)testOpeningViewCell
{
  _cell.isOpeningViewCell = YES;
  
  UIView *infoView = [_cell valueForKey:@"infoView"];
  XCTAssertTrue([infoView.backgroundColor isEqual:[UIColor whiteColor]]);
  XCTAssertEqual(_cell.contentView.layer.borderWidth, 0.0);
  XCTAssertEqual(_cell.contentView.layer.borderColor, [MezzanineStyleSheet sharedStyleSheet].highlightedTextColor.CGColor);

  MezzanineStyleSheet *styleSheet = [MezzanineStyleSheet sharedStyleSheet];
  UILabel *titleLabel = [_cell valueForKey:@"titleLabel"];
  UILabel *dateLabel = [_cell valueForKey:@"dateLabel"];
  UILabel *ownerLabel = [_cell valueForKey:@"ownerLabel"];

  XCTAssertTrue([titleLabel.textColor isEqual:styleSheet.grey90Color]);
  XCTAssertTrue([dateLabel.textColor isEqual:styleSheet.grey90Color]);
  XCTAssertTrue([ownerLabel.textColor isEqual:styleSheet.grey90Color]);
}


- (void)testUpdateCellBorderCurrentWorkspace
{
  MZWorkspace *workspace = [MZWorkspace new];
  workspace.uid = @"uid";
  
  _cell.workspace = workspace;
  _cell.systemModel.currentWorkspace = workspace;
  
  [_cell updateCellBorder];
  
  UIView *infoView = [_cell valueForKey:@"infoView"];
  XCTAssertTrue([infoView.backgroundColor isEqual:[MezzanineStyleSheet sharedStyleSheet].grey50Color]);
  XCTAssertEqual(_cell.contentView.layer.borderWidth, 4.0);
  XCTAssertEqual(_cell.contentView.layer.borderColor, [MezzanineStyleSheet sharedStyleSheet].defaultHighlightColor.CGColor);
}


- (void)testUpdateCellBorderCurrentWorkspaceLoading
{
  MZWorkspace *workspace = [MZWorkspace new];
  workspace.uid = @"uid";
  
  _cell.workspace = workspace;
  _cell.systemModel.currentWorkspace = workspace;
  
  _cell.workspace.isLoading = YES;
  [_cell updateCellBorder];
  
  UIView *infoView = [_cell valueForKey:@"infoView"];
  XCTAssertTrue([infoView.backgroundColor isEqual:[MezzanineStyleSheet sharedStyleSheet].grey90Color]);
  XCTAssertEqual(_cell.contentView.layer.borderWidth, 0.0);
  XCTAssertEqual(_cell.contentView.layer.borderColor, [MezzanineStyleSheet sharedStyleSheet].highlightedTextColor.CGColor);
}


- (void)testUpdateCellBorderNotCurrentWorkspaceNotHighlighted
{
  [_cell updateCellBorder];
  
  UIView *infoView = [_cell valueForKey:@"infoView"];
  XCTAssertTrue([infoView.backgroundColor isEqual:[MezzanineStyleSheet sharedStyleSheet].grey70Color]);
  XCTAssertEqual(_cell.contentView.layer.borderWidth, 0.0);
  XCTAssertEqual(_cell.contentView.layer.borderColor, [MezzanineStyleSheet sharedStyleSheet].defaultHoverColor.CGColor);
}


- (void)testUpdateCellBorderNotCurrentWorkspaceHighlighted
{
  _cell.highlighted = YES;
  [_cell updateCellBorder];
  
  UIView *infoView = [_cell valueForKey:@"infoView"];
  XCTAssertTrue([infoView.backgroundColor isEqual:[MezzanineStyleSheet sharedStyleSheet].grey90Color]);
  XCTAssertEqual(_cell.contentView.layer.borderWidth, 0.0);
  XCTAssertEqual(_cell.contentView.layer.borderColor, [MezzanineStyleSheet sharedStyleSheet].defaultHoverColor.CGColor);
}


- (void)testPrepareForReuse
{
  [_cell prepareForReuse];
  
  XCTAssertNil(_cell.systemModel);
  XCTAssertNil(_cell.workspace);
  XCTAssertEqual([((UILabel *)[_cell valueForKey:@"dateLabel"]).text length], 0);
  XCTAssertEqual([((UILabel *)[_cell valueForKey:@"titleLabel"]).text length], 0);
  XCTAssertNil(((UIImageView *)[_cell valueForKey:@"imageView"]).image);
}


- (void)testUpdateWorkspaceProperties
{
  MZWorkspace *workspace = [MZWorkspace new];
  workspace.uid = @"uid";
  workspace.name = @"name";
  
  _cell.workspace = workspace;
  _cell.systemModel.currentWorkspace = workspace;
  
  XCTAssertTrue([((UILabel *)[_cell valueForKey:@"titleLabel"]).text isEqualToString:workspace.name]);
  
  workspace.name = @"name2";
  XCTAssertTrue([((UILabel *)[_cell valueForKey:@"titleLabel"]).text isEqualToString:workspace.name]);

  workspace.modifiedDate = [NSDate date];
  XCTAssertNotNil(((UIImageView *)[_cell valueForKey:@"imageView"]).image);
  
  workspace.modifiedDateString = @"modified date";
  XCTAssertTrue([((UILabel *)[_cell valueForKey:@"dateLabel"]).text isEqualToString:workspace.modifiedDateString]);

  workspace.isBeingEdited = YES;
  XCTAssertEqual(_cell.alpha, 0);
}


- (void)testShowOptionsAction
{
  [_cell showWorkspaceOptions];
  FPPopoverController *popoverController = [_cell valueForKey:@"currentPopoverController"];
  
  XCTAssertNotNil(popoverController);
  XCTAssertTrue([popoverController.viewController isKindOfClass:[ButtonMenuViewController class]]);
  XCTAssertEqual(popoverController.delegate, _cell);
  
  ButtonMenuViewController *contentViewController = (ButtonMenuViewController *) popoverController.viewController;
  XCTAssertNotNil(contentViewController);
  XCTAssertGreaterThan(contentViewController.menuItems.count, 0);
  
  for (ButtonMenuItem *button in contentViewController.menuItems) {
    XCTAssertTrue(button.enabled);
    XCTAssertNotNil(button.action);
  }
}


- (void)testDuplicateWorkspaceAction
{
  [_cell duplicateWorkspace];
  FPPopoverController *popoverController = [_cell valueForKey:@"currentPopoverController"];
  
  XCTAssertNotNil(popoverController);
  XCTAssertTrue([popoverController.viewController isKindOfClass:[WorkspaceEditorViewController class]]);
  XCTAssertEqual(popoverController.delegate, _cell);
  WorkspaceEditorViewController *contentViewController = (WorkspaceEditorViewController *) popoverController.viewController;
  XCTAssertNotNil(contentViewController);
  XCTAssertTrue([contentViewController.doneButtonTitle isEqualToString:NSLocalizedString(@"Workspace Menu Duplicate Button", nil)]);

  XCTAssertTrue([contentViewController.view.backgroundColor isEqual:[UIColor whiteColor]]);
  XCTAssertTrue([popoverController.contentView.customTint isEqual:[UIColor whiteColor]]);
}


- (void)testRenameWorkspaceAction
{
  [_cell renameWorkspace];
  FPPopoverController *popoverController = [_cell valueForKey:@"currentPopoverController"];
  
  XCTAssertNotNil(popoverController);
  XCTAssertTrue([popoverController.viewController isKindOfClass:[WorkspaceEditorViewController class]]);
  XCTAssertEqual(popoverController.delegate, _cell);
  WorkspaceEditorViewController *contentViewController = (WorkspaceEditorViewController *) popoverController.viewController;
  XCTAssertNotNil(contentViewController);
  XCTAssertTrue([contentViewController.doneButtonTitle isEqualToString:NSLocalizedString(@"Workspace Menu Rename Button", nil)]);

  XCTAssertTrue([contentViewController.view.backgroundColor isEqual:[UIColor whiteColor]]);
  XCTAssertTrue([popoverController.contentView.customTint isEqual:[UIColor whiteColor]]);
}


@end
