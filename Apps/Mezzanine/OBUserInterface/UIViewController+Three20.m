//
//  UIThree20Helpers.m
//  Mezzanine
//
//  Created by Zai Chang on 10/14/10.
//  Copyright 2010 Oblong Industries. All rights reserved.
//

#import "UIViewController+Three20.h"


@implementation UIViewController (TTStyle)

-(TTButton*) ttButtonWithStyle:(NSString*)style placeholder:(UIView*)placeholder
{
  if (!NSClassFromString(@"TTButton"))
    return nil;
  
  UIButton *placeholderButton = nil;
  if ([placeholder isKindOfClass:[UIButton class]])
    placeholderButton = (UIButton*)placeholder;
  
  TTButton *ttButton = [TTButton buttonWithStyle:style];
  ttButton.alpha = placeholder.alpha;
  ttButton.autoresizingMask = placeholder.autoresizingMask;
  if ([placeholder isKindOfClass:[UIControl class]])
  {
    UIControl *control = (UIControl*)placeholder;
    ttButton.enabled = control.enabled;
  }
  ttButton.frame = placeholder.frame;
  ttButton.hidden = placeholder.hidden;
  ttButton.tag = placeholder.tag;
  ttButton.contentMode = placeholder.contentMode;
  
  if (placeholderButton)
  {
    [ttButton setTitle:[placeholderButton titleForState:UIControlStateNormal] forState:UIControlStateNormal];
    
    NSArray *actions = [placeholderButton actionsForTarget:self forControlEvent:UIControlEventTouchUpInside];
    for (NSString *action in actions)
      [ttButton addTarget:self action:NSSelectorFromString(action) forControlEvents:UIControlEventTouchUpInside];

    ttButton.isAccessibilityElement = placeholderButton.isAccessibilityElement;
    ttButton.accessibilityIdentifier = placeholderButton.accessibilityIdentifier;
    ttButton.accessibilityLabel = placeholderButton.accessibilityLabel;
  }
  
  [placeholder.superview insertSubview:ttButton aboveSubview:placeholder];
  [placeholder removeFromSuperview];
  
  return ttButton;
}

@end

