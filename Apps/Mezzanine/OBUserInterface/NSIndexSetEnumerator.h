//
//  NSIndexSetEnumerator.h
//
//  From http://www.cocoadev.com/index.pl?NSIndexSet

#import <Foundation/Foundation.h>


@interface NSIndexSetEnumerator : NSObject 
{
  NSUInteger m_uCurrentPosition;
  
  NSUInteger m_uIndexCount;
  NSUInteger *m_puIndexes;
  
}

// Basic initializers and deallocator

+ (id) enumeratorWithIndexSet:(NSIndexSet *)theSet;
+ (id) enumeratorWithIndexSet:(NSIndexSet *)theSet range:(NSRange)myRange;
- (id) initWithIndexSet:(NSIndexSet *)theSet range:(NSRange)myRange;
- (void) dealloc;

- (void) reset;
- (NSUInteger) count;
- (BOOL) nextIndex:(NSUInteger *)pValue;
- (NSUInteger) indexAtIndex:(NSUInteger)i;

@end
