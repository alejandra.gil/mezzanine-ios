//
//  OBAppData.h
//  iPhoneTest
//
//  Created by Zai Chang on 9/10/10.
//  Copyright 2010 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface OBAppData : NSObject 
{
  // Saved data
	NSMutableArray *recentPools;
  
  // Transient data
  NSString *currentPool;
}
@property (nonatomic,readonly) NSMutableArray *recentPools;

@property (nonatomic, copy) NSString *currentPool;

+(OBAppData*) sharedData;

-(void) noteRecentPool:(NSString*)pool; // Uses default limit of 10
-(void) noteRecentPool:(NSString*)pool limit:(NSInteger)limit;
-(void) clearRecentPools;
-(void) save;

@end
