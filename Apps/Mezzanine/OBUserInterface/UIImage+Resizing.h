//
//  UIImage+Resizing.h
//  iPhoneTest
//
//  Created by Zai Chang on 9/17/10.
//  Copyright 2010 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>


CGRect ProportionalResizeInto( CGRect srcRect, const CGRect inRect );
CGRect ProportionalResizeIntoAndCenter (CGRect srcRect, const CGRect inRect);

@interface UIImage (Resizing)

-(UIImage *) resizeImage:(CGSize) newSize;
-(UIImage *) resizeImagePreservingAspectRatio:(CGSize) newSize;

@end
