//
//  UIDevice+Machine.h
//  OBUserInterface
//
//  Created by Zehao Chang on 10/13/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


/// UIDevice+Machine
///
/// Convenience cateogry on UIDevice for various device and OS version checks
///

@interface UIDevice(Machine)

-(NSString*) machine;

-(BOOL) isIOSSimulator;

-(BOOL) isIPodTouch;
-(BOOL) isIPod2ndGen;
-(BOOL) isIPod3rdGenOrNewer;
-(BOOL) isIPod4thGenOrNewer;
-(BOOL) isIPod5thGenOrNewer;

-(BOOL) isIPhone3GS;
-(BOOL) isIPhone3GSOrNewer;
-(BOOL) isIPhone4OrNewer;
-(BOOL) isIPhone4SOrNewer;
-(BOOL) isIPhone5OrNewer;

-(BOOL) isIPad;
-(BOOL) isIPad2OrNewer;
-(BOOL) isIPad3OrNewer;
-(BOOL) isIOS4;
-(BOOL) isIOS5OrAbove;
-(BOOL) isIOS6OrAbove;
-(BOOL) isIOS7OrAbove;
-(BOOL) isIOS71OrAbove;
-(BOOL) isIOS8OrAbove;
-(BOOL) isIOS81OrAbove;
-(BOOL) isIOS9OrAbove;
-(BOOL) isIOS10OrAbove;

@end
