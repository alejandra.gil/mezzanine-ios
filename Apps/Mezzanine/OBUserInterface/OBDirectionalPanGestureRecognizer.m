//
//  OBDirectionalPanGestureRecognizer.m
//  OBUserInterface
//
//  Created by Zai Chang on 2/17/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import "OBDirectionalPanGestureRecognizer.h"
#import <UIKit/UIGestureRecognizerSubclass.h>


static CGFloat kOBDirectionalPanThreshold = 5;


@implementation OBDirectionalPanGestureRecognizer

@synthesize direction;
@synthesize angleThreshold;

-(id) initWithTarget:(id)target action:(SEL)action
{
  self = [super initWithTarget:target action:action];
  if (self)
  {
    angleThreshold = 45.0;
  }
  return self;
}


- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event 
{
  [super touchesMoved:touches withEvent:event];
  
  if (self.state == UIGestureRecognizerStateFailed)
    return;
  
  if (!_drag)
  {
    CGPoint nowPoint = [[touches anyObject] locationInView:self.view];
    CGPoint prevPoint = [[touches anyObject] previousLocationInView:self.view];
    _moveX += prevPoint.x - nowPoint.x;
    _moveY += prevPoint.y - nowPoint.y;
    CGFloat distance = sqrt(_moveX * _moveX + _moveY * _moveY);
    
    CGFloat angle = atan2(_moveY, _moveX) * 180.0 / M_PI;
    
    if (fabs(distance) > kOBDirectionalPanThreshold && abs(_moveY) > abs(_moveX))
    {
      CGFloat angleFromAxis = fabs(fabs(angle) - 90.0);
      
      //DLog(@"OBDirectionalPanGestureRecognizer angle = %f, angleFromAxis = %f", angle, angleFromAxis);
      if ((direction & OBDirectionalPanLeft) ||
          (direction & OBDirectionalPanRight) ||
          (angleFromAxis > angleThreshold) ||  // Angle threshold direction test
          (direction == OBDirectionalPanUp && _moveY < 0) ||  // Incorrect direction test
          (direction == OBDirectionalPanDown && _moveY > 0))  // Incorrect direction test
      {
        self.state = UIGestureRecognizerStateFailed;
      }
      else 
      {
        _drag = YES;
      }
    }
    else if (fabs(distance) > kOBDirectionalPanThreshold && abs(_moveX) > abs(_moveY))
    {
      CGFloat angleFromAxis = fabs(angle);
      if (angleFromAxis > 90.0)
        angleFromAxis -= 180.0;
      
      //DLog(@"OBDirectionalPanGestureRecognizer angle = %f, angleFromAxis = %f", angle, angleFromAxis);
      if ((direction & OBDirectionalPanUp) ||
          (direction & OBDirectionalPanDown) ||
          (angleFromAxis > angleThreshold) ||  // Angle threshold direction test
          (direction == OBDirectionalPanLeft && _moveX > 0) ||  // Incorrect direction test
          (direction == OBDirectionalPanRight && _moveX < 0))  // Incorrect direction test
      {
        self.state = UIGestureRecognizerStateFailed;
      }
      else 
      {
        _drag = YES;
      }
    }
  }
}

- (void)reset 
{
  [super reset];
  _drag = NO;
  _moveX = 0;
  _moveY = 0;
}


@end
