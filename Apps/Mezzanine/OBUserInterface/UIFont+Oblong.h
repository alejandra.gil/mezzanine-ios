//
//  UIFont+Oblong.h
//  OBUserInterface
//
//  Created by Zai Chang on 6/28/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (Oblong)

// These functions find and uses DIN fonts if its available in the application bundle
// If these aren't found they default to the system default font
+(UIFont*) dinOfSize:(CGFloat)fontSize;
+(UIFont*) dinLightOfSize:(CGFloat)fontSize;
+(UIFont*) dinMediumOfSize:(CGFloat)fontSize;
+(UIFont*) dinBoldOfSize:(CGFloat)fontSize;
+(UIFont*) dinBlackOfSize:(CGFloat)fontSize;

+(UIFont*) dinItalicOfSize:(CGFloat)fontSize;
+(UIFont*) dinItalicLightOfSize:(CGFloat)fontSize;
+(UIFont*) dinItalicMediumOfSize:(CGFloat)fontSize;
+(UIFont*) dinItalicBoldOfSize:(CGFloat)fontSize;
+(UIFont*) dinItalicBlackOfSize:(CGFloat)fontSize;

@end
