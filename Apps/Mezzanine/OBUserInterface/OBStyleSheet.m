//
//  OBStyleSheet.m
//  Mezzanine
//
//  Created by Zai Chang on 11/19/10.
//  Copyright 2010 Oblong Industries. All rights reserved.
//

#import "OBStyleSheet.h"
#import "Three20Style.h"
#import "UIColorAdditions.h"

@interface OBStyleSheet (TTDefaultStyleSheetInternals) 

- (UIColor*)toolbarButtonColorWithTintColor:(UIColor*)color forState:(UIControlState)state;
- (UIColor*)toolbarButtonTextColorForState:(UIControlState)state;

@end



@implementation OBStyleSheet

#pragma mark - Oblong Colors

-(UIColor *) feldBackingColor { return RGBCOLOR(49, 49, 59); }
-(UIColor *) textColor { return [UIColor colorWithWhite:0.8 alpha:1.0]; }



#pragma mark - Toolbar Button Styles

-(TTStyle*) redToolbarButton:(UIControlState)state
{
  return [self toolbarButton:state withTintColor:RGBCOLOR(160, 10, 10)];
}

-(TTStyle*) blueToolbarButton:(UIControlState)state
{
  return [self toolbarButton:state withTintColor:RGBCOLOR(10, 10, 160)];
}

-(TTStyle*) toolbarButton:(UIControlState)state withTintColor:(UIColor*)tintColor
{
  return [self toolbarButtonForState:state
                               shape:[TTRoundedRectangleShape shapeWithRadius:4.5]
                           tintColor:tintColor
                                font:nil];
}

-(TTStyle*) translucentBlackToolbarButton:(UIControlState)state
{
  TTShape* shape = [TTRoundedRectangleShape shapeWithRadius:4.5];
  UIColor* tintColor = RGBACOLOR(10, 10, 10, 64);
  UIFont* font = [UIFont dinOfSize:12.0];
  
  UIColor* stateTintColor = [self toolbarButtonColorWithTintColor:tintColor forState:state];
  UIColor* stateTextColor = [self toolbarButtonTextColorForState:state];
  
  return
  [TTShapeStyle styleWithShape:shape next:
   [TTInsetStyle styleWithInset:UIEdgeInsetsMake(2, 0, 1, 0) next:
    [TTShadowStyle styleWithColor:RGBACOLOR(255,255,255,0.18) blur:0 offset:CGSizeMake(0, 1) next:
     [TTReflectiveFillStyle styleWithColor:stateTintColor next:
      [TTBevelBorderStyle styleWithHighlight:[stateTintColor multiplyHue:1 saturation:0.9 value:0.7]
                                      shadow:[stateTintColor multiplyHue:1 saturation:0.5 value:0.6]
                                       width:1 lightSource:270 next:
       [TTInsetStyle styleWithInset:UIEdgeInsetsMake(0, -1, 0, -1) next:
        [TTBevelBorderStyle styleWithHighlight:nil shadow:RGBACOLOR(0,0,0,0.15)
                                         width:1 lightSource:270 next:
         [TTBoxStyle styleWithPadding:UIEdgeInsetsMake(8, 8, 8, 8) next:
          [TTImageStyle styleWithImageURL:nil defaultImage:nil
                              contentMode:UIViewContentModeScaleToFill size:CGSizeZero next:
           [TTTextStyle styleWithFont:font
                                color:stateTextColor shadowColor:[UIColor colorWithWhite:0 alpha:0.4]
                         shadowOffset:CGSizeMake(0, -1) next:nil]]]]]]]]]];
}

-(TTStyle*) transparentButtonStyle2:(UIControlState)state
{
  CGFloat alpha = 48.0/255.0;
  TTShape* shape = [TTRoundedRectangleShape shapeWithRadius:TT_ROUNDED];
  UIColor* tintColor = RGBACOLOR(10, 10, 10, alpha/255.0);
  UIFont* font = [UIFont dinOfSize:12.0];
  
  //  UIColor* borderColor = [[tintColor copyWithAlpha:255.0/255.0] autorelease];
  //  if (state & UIControlStateHighlighted || state & UIControlStateSelected)
  //    borderColor = [borderColor addHue:0 saturation:1 value:0.2];
  //  else
  //    borderColor = [borderColor multiplyHue:1 saturation:1.6 value:0.97];
  
  UIColor* borderColor;
  if (state & UIControlStateHighlighted || state & UIControlStateSelected)
    borderColor = RGBCOLOR(0.8, 0.8, 0.8);
  else
    borderColor = RGBCOLOR(1.0, 1.0, 1.0);
  
  UIColor *backgroundColor;
  if (state & UIControlStateHighlighted || state & UIControlStateSelected)
    backgroundColor = [tintColor copyWithAlpha:1.0];
  else
    backgroundColor = [tintColor multiplyHue:1 saturation:1.6 value:0.97];
  
  UIColor* stateTextColor;
  if (state & UIControlStateDisabled)
    stateTextColor = [UIColor colorWithWhite:0.85 alpha:0.4];
  else if (state & UIControlStateHighlighted || state & UIControlStateSelected)
    stateTextColor = [UIColor colorWithWhite:1.0 alpha:1.0];
  else
    stateTextColor = [UIColor colorWithWhite:0.85 alpha:1.0];
  
  
  return
  [TTShapeStyle styleWithShape:shape next:
   [TTInsetStyle styleWithInset:UIEdgeInsetsMake(1, 0, 1, 0) next:
    [TTShadowStyle styleWithColor:RGBACOLOR(255,255,255,0.18) blur:0 offset:CGSizeMake(0, 1) next:
     [TTSolidFillStyle styleWithColor:backgroundColor next:
      [TTBevelBorderStyle styleWithHighlight:[borderColor multiplyHue:1 saturation:0.9 value:0.7]
                                      shadow:[borderColor multiplyHue:1 saturation:0.5 value:0.6]
                                       width:1 lightSource:270 next:
       [TTBoxStyle styleWithPadding:UIEdgeInsetsMake(8, 8, 8, 8) next:
        [TTImageStyle styleWithImageURL:nil defaultImage:nil
                            contentMode:UIViewContentModeScaleToFill size:CGSizeZero next:
         [TTTextStyle styleWithFont:font
                              color:stateTextColor shadowColor:[UIColor colorWithWhite:0 alpha:0.4]
                       shadowOffset:CGSizeMake(0, -1) next:nil]]]]]]]];
}



-(TTStyle*) transparentButtonStyle:(UIControlState)state
{
  TTShape* shape = [TTRoundedRectangleShape shapeWithRadius:TT_ROUNDED];
  //UIColor* tintColor = RGBCOLOR(128, 10, 10);
  UIColor* tintColor = RGBCOLOR(128, 128, 128);
  UIFont* font = [UIFont dinOfSize:12.0];
  
  
  UIColor* borderColor = [tintColor copyWithAlpha:255.0/255.0];
  //  if (state & UIControlStateHighlighted || state & UIControlStateSelected)
  //    borderColor = [borderColor addHue:0 saturation:1 value:0.8];
  //  else
  borderColor = [borderColor multiplyHue:1 saturation:1.6 value:0.97];
  
  UIColor *backgroundColor = [tintColor copyWithAlpha:0.2];
  if (state & UIControlStateHighlighted || state & UIControlStateSelected)
    backgroundColor = [[tintColor multiplyHue:1 saturation:1.6 value:0.97] copyWithAlpha:0.3];
  else
    backgroundColor = backgroundColor;
  
  UIColor* stateTextColor;
  if (state & UIControlStateDisabled)
    stateTextColor = [UIColor colorWithWhite:0.85 alpha:0.4];
  else if (state & UIControlStateHighlighted || state & UIControlStateSelected)
    //    stateTextColor = [UIColor colorWithWhite:1.0 alpha:1.0];
    stateTextColor = [tintColor multiplyHue:1 saturation:1 value:2.0];
  else
    //    stateTextColor = [UIColor colorWithWhite:0.85 alpha:1.0];
    stateTextColor = [tintColor multiplyHue:2.0 saturation:1 value:2.0];
  
  
  return
  [TTShapeStyle styleWithShape:shape next:
   [TTInsetStyle styleWithInset:UIEdgeInsetsMake(1, 0, 1, 0) next:
    [TTShadowStyle styleWithColor:RGBACOLOR(255,255,255,0.18) blur:0 offset:CGSizeMake(0, 1) next:
     [TTSolidFillStyle styleWithColor:backgroundColor next:
      [TTBevelBorderStyle styleWithHighlight:[borderColor multiplyHue:1 saturation:1 value:1]
                                      shadow:[borderColor multiplyHue:1 saturation:0.5 value:0.8]
                                       width:1 lightSource:270 next:
       [TTBoxStyle styleWithPadding:UIEdgeInsetsMake(8, 8, 8, 8) next:
        [TTImageStyle styleWithImageURL:nil defaultImage:nil
                            contentMode:UIViewContentModeScaleToFill size:CGSizeZero next:
         [TTTextStyle styleWithFont:font
                              color:stateTextColor shadowColor:[UIColor colorWithWhite:0 alpha:0.4]
                       shadowOffset:CGSizeMake(0, -1) next:nil]]]]]]]];
}



#pragma mark Matte Styles

-(TTStyle*) matteGradientButton:(UIControlState)state shape:(TTShape*)shape tintColor:(UIColor*)tintColor borderColor:(UIColor*)borderColor textColor:(UIColor*)textColor
{
	UIColor *primaryColor = state == UIControlStateHighlighted ? [tintColor multiplyHue:1.0 saturation:1.2 value:1.0] : tintColor;
	UIColor *secondaryColor = [primaryColor addHue:1.0 saturation:1.0 value:1.1];
	UIColor *shadowColor = [primaryColor multiplyHue:1 saturation:0.5 value:0.6];
	
	return [TTShapeStyle styleWithShape:shape next:
          [TTSolidBorderStyle styleWithColor:borderColor width:2.0 next:
           [TTLinearGradientFillStyle styleWithColor1:secondaryColor color2:primaryColor next:
            [TTBoxStyle styleWithPadding:UIEdgeInsetsMake(3, 6, 3, 6) next:
             [TTTextStyle styleWithFont:nil color:textColor shadowColor:shadowColor shadowOffset:CGSizeMake(0, -1) next:
              nil]]]]];
}

-(TTStyle*) blackGradientRoundedButton:(UIControlState)state
{
	return [self matteGradientButton:state
                             shape:[TTRoundedRectangleShape shapeWithRadius:5.0]
                         tintColor:RGBCOLOR(0, 0, 0)
                       borderColor:RGBCOLOR(255, 255, 255)
                         textColor:RGBCOLOR(255, 255, 255)];
}

-(TTStyle*) matteButton:(UIControlState)state shape:(TTShape*)shape tintColor:(UIColor*)tintColor borderColor:(UIColor*)borderColor textColor:(UIColor*)textColor
{
  UIColor *(^adjustColor)(UIColor *color, UIControlState state) = ^(UIColor *color, UIControlState state) {
    if (state == UIControlStateHighlighted)
      return [color multiplyHue:1.0 saturation:1.0 value:1.3];
    else if (state == UIControlStateDisabled)
      return [color multiplyHue:1.0 saturation:0.42 value:0.42];
    return color;
  };
  
	UIColor *adjustedTintColor = adjustColor(tintColor, state);
	UIColor *adjustedBorderColor = adjustColor(borderColor, state);
	UIColor *adjustedTextColor = adjustColor(textColor, state);
  
  CGFloat borderWidth = (state == UIControlStateHighlighted) ? 2.0 : 1.0;
  
  UIFont *font = [UIFont dinMediumOfSize:16.0];
	
	return [TTShapeStyle styleWithShape:shape next:
           [TTSolidBorderStyle styleWithColor:adjustedBorderColor width:borderWidth next:
            [TTSolidFillStyle styleWithColor:adjustedTintColor next:
             [TTBoxStyle styleWithPadding:UIEdgeInsetsMake(3, 6, 3, 6) next:
              [TTTextStyle styleWithFont:font color:adjustedTextColor next:
               nil]]]]];
}

-(TTStyle*) darkRoundedButton:(UIControlState)state
{
	return [self matteButton:state
                     shape:[TTRoundedRectangleShape shapeWithRadius:5.0]
                 tintColor:[self feldBackingColor]
               borderColor:RGBCOLOR(255, 255, 255)
                 textColor:RGBCOLOR(255, 255, 255)];
}

-(TTStyle*) blackRoundedButton:(UIControlState)state
{
	return [self matteButton:state
                     shape:[TTRoundedRectangleShape shapeWithRadius:5.0]
                 tintColor:[UIColor blackColor]
               borderColor:RGBCOLOR(255, 255, 255)
                 textColor:RGBCOLOR(255, 255, 255)];
}

-(TTStyle*) blackRoundedButtonDim:(UIControlState)state
{
	return [self matteButton:state
                     shape:[TTRoundedRectangleShape shapeWithRadius:5.0]
                 tintColor:RGBCOLOR(0, 0, 0)
               borderColor:[UIColor colorWithRed:0.66 green:0.66 blue:0.66 alpha:1.0]
                 textColor:[UIColor colorWithRed:0.66 green:0.66 blue:0.66 alpha:1.0]];
}

-(TTStyle*) blackTransluscentRoundedButton:(UIControlState)state
{
	return [self matteButton:state
                     shape:[TTRoundedRectangleShape shapeWithRadius:5.0]
                 tintColor:RGBACOLOR(0, 0, 0, 0.66)
               borderColor:[UIColor colorWithRed:0.66 green:0.66 blue:0.66 alpha:1.0]
                 textColor:[UIColor colorWithRed:0.66 green:0.66 blue:0.66 alpha:1.0]];
}

-(TTStyle*) blackTransluscentRoundedButtonDim:(UIControlState)state
{
	return [self matteButton:state
                     shape:[TTRoundedRectangleShape shapeWithRadius:5.0]
                 tintColor:RGBACOLOR(0, 0, 0, 0.66)
               borderColor:[UIColor colorWithRed:0.66 green:0.66 blue:0.66 alpha:1.0]
                 textColor:[UIColor colorWithRed:0.66 green:0.66 blue:0.66 alpha:1.0]];
}


-(TTStyle*) redTransluscentRoundedButton:(UIControlState)state
{
	return [self matteButton:state
                     shape:[TTRoundedRectangleShape shapeWithRadius:5.0]
                 tintColor:[UIColor colorWithRed:0.24 green:0.0 blue:0.0 alpha:1.0]
               borderColor:[UIColor redColor]
                 textColor:RGBCOLOR(255, 255, 255)];
}


/*
 // For reference ... from TTDefaultStyleSheet:
 
 - (TTStyle*)toolbarButtonForState:(UIControlState)state shape:(TTShape*)shape
 tintColor:(UIColor*)tintColor font:(UIFont*)font {
 UIColor* stateTintColor = [self toolbarButtonColorWithTintColor:tintColor forState:state];
 UIColor* stateTextColor = [self toolbarButtonTextColorForState:state];
 
 return
 [TTShapeStyle styleWithShape:shape next:
 [TTInsetStyle styleWithInset:UIEdgeInsetsMake(2, 0, 1, 0) next:
 [TTShadowStyle styleWithColor:RGBACOLOR(255,255,255,0.18) blur:0 offset:CGSizeMake(0, 1) next:
 [TTReflectiveFillStyle styleWithColor:stateTintColor next:
 [TTBevelBorderStyle styleWithHighlight:[stateTintColor multiplyHue:1 saturation:0.9 value:0.7]
 shadow:[stateTintColor multiplyHue:1 saturation:0.5 value:0.6]
 width:1 lightSource:270 next:
 [TTInsetStyle styleWithInset:UIEdgeInsetsMake(0, -1, 0, -1) next:
 [TTBevelBorderStyle styleWithHighlight:nil shadow:RGBACOLOR(0,0,0,0.15)
 width:1 lightSource:270 next:
 [TTBoxStyle styleWithPadding:UIEdgeInsetsMake(8, 8, 8, 8) next:
 [TTImageStyle styleWithImageURL:nil defaultImage:nil
 contentMode:UIViewContentModeScaleToFill size:CGSizeZero next:
 [TTTextStyle styleWithFont:font
 color:stateTextColor shadowColor:[UIColor colorWithWhite:0 alpha:0.4]
 shadowOffset:CGSizeMake(0, -1) next:nil]]]]]]]]]];
 }
 */

-(TTStyle*) closeButtonImage:(UIControlState)state
{
  return
  [TTBoxStyle styleWithMargin:UIEdgeInsetsMake(-2, 0, 0, 0) next:
   [TTImageStyle styleWithImageURL:@"bundle://Three20.bundle/images/closeButton.png" defaultImage:nil contentMode:UIViewContentModeCenter
                              size:CGSizeMake(10,10) next:nil]];  
}

-(TTStyle*) redCloseButtonStyle:(UIControlState)state
{
  return
  [TTShapeStyle styleWithShape:[TTRoundedRectangleShape shapeWithRadius:TT_ROUNDED] next:
   [TTInsetStyle styleWithInset:UIEdgeInsetsMake(1, 1, 1, 1) next:
    [TTShadowStyle styleWithColor:RGBACOLOR(0,0,0,0.5) blur:2 offset:CGSizeMake(0, 3) next:
     [TTSolidFillStyle styleWithColor:RGBCOLOR(0.7, 0, 0) next:
      [TTInsetStyle styleWithInset:UIEdgeInsetsMake(-1, -1, -1, -1) next:
       [TTSolidBorderStyle styleWithColor:[UIColor whiteColor] width:2 next:
        [TTPartStyle styleWithName:@"image" style:TTSTYLE(closeButtonImage:) next:
         nil]]]]]]];  
}

-(TTStyle*) blackCloseButtonStyle:(UIControlState)state
{
  return
  [TTShapeStyle styleWithShape:[TTRoundedRectangleShape shapeWithRadius:TT_ROUNDED] next:
   [TTInsetStyle styleWithInset:UIEdgeInsetsMake(1, 1, 1, 1) next:
    [TTShadowStyle styleWithColor:RGBACOLOR(0,0,0,0.5) blur:2 offset:CGSizeMake(0, 3) next:
     [TTSolidFillStyle styleWithColor:[UIColor blackColor] next:
      [TTInsetStyle styleWithInset:UIEdgeInsetsMake(-1, -1, -1, -1) next:
       [TTSolidBorderStyle styleWithColor:[UIColor whiteColor] width:2 next:
        [TTPartStyle styleWithName:@"image" style:TTSTYLE(closeButtonImage:) next:
         nil]]]]]]];  
}

@end
