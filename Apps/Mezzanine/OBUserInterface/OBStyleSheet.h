//
//  OBStyleSheet.h
//  Mezzanine
//
//  Created by Zai Chang on 11/19/10.
//  Copyright 2010 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TTDefaultStyleSheet.h"
#import "UIFont+Oblong.h"

#define TTSTYLE_TOOLBARBUTTON @"toolbarButton:"
#define TTSTYLE_TOOLBARBACKBUTTON @"toolbarBackButton:"
#define TTSTYLE_TOOLBARFORWARDBUTTON @"toolbarForwardButton:"
#define TTSTYLE_TOOLBARROUNDBUTTON @"toolbarRoundButton:"

#define TTSTYLE_BLACKTOOLBARBUTTON @"blackToolbarButton:"
#define TTSTYLE_GRAYTOOLBARBUTTON @"grayToolbarButton:"
#define TTSTYLE_BLACKTOOLBARFORWARDBUTTON @"blackToolbarForwardButton:"
#define TTSTYLE_BLACKTOOLBARROUNDBUTTON @"blackToolbarRoundButton:"

#define TTSTYLE_REDTOOLBARBUTTON @"redToolbarButton:"
#define TTSTYLE_BLUETOOLBARBUTTON @"blueToolbarButton:"
#define TTSTYLE_TRANSLUCENTBLACKTOOLBARBUTTON @"translucentBlackToolbarButton:"


/// OBStyleSheet
///
/// A collection of TTStyles to be used across Oblong iOS apps
/// To use, place the following line in your app delegate:
/// [TTStyleSheet setGlobalStyleSheet:[[[OBStyleSheet alloc] init] autorelease]];
///
/// If you wish to define your own styles, either register your custom style
/// sheet (which may subclass from OBStyleSheet) or add a category to OBStyleSheet
/// with your custom TTStyle methods
///
@interface OBStyleSheet : TTDefaultStyleSheet
{
}

@property (nonatomic, strong, readonly) UIColor *feldBackingColor;

-(TTStyle*) redToolbarButton:(UIControlState)state;
-(TTStyle*) blueToolbarButton:(UIControlState)state;
-(TTStyle*) translucentBlackToolbarButton:(UIControlState)state;

-(TTStyle*) toolbarButton:(UIControlState)state withTintColor:(UIColor*)tintColor;

-(TTStyle*) redCloseButtonStyle:(UIControlState)state;
-(TTStyle*) blackCloseButtonStyle:(UIControlState)state;

// Matte style buttons
-(TTStyle*) matteGradientButton:(UIControlState)state shape:(TTShape*)shape tintColor:(UIColor*)tintColor borderColor:(UIColor*)borderColor textColor:(UIColor*)textColor;
-(TTStyle*) matteButton:(UIControlState)state shape:(TTShape*)shape tintColor:(UIColor*)tintColor borderColor:(UIColor*)borderColor textColor:(UIColor*)textColor;

-(TTStyle*) darkRoundedButton:(UIControlState)state;
-(TTStyle*) blackRoundedButton:(UIControlState)state;

@end
