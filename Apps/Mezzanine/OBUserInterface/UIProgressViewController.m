//
//  UIProgressViewController.m
//  Mezzanine
//
//  Created by Zai Chang on 10/12/10.
//  Copyright 2010 Oblong Industries. All rights reserved.
//

#import "UIProgressViewController.h"


@implementation UIProgressViewController

@synthesize titleLabel;
@synthesize activityIndicatorView;
@synthesize progressView;
@synthesize darkeningVeil;

@synthesize darkenBackground;
@synthesize isIndeterminate;
@dynamic progress;

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}
*/

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation 
{
  // Do not allow rotation if the window is hidden.
  // This is to prevent UIStatusBar or UIActionSheet from rotating when they shouldn't
  // because I'm embedding this view controller inside a separate UIWindow.
  // All UIWindows receive orientation change regardless of visibility or topmost
  // and UI elements such as UIActionSheet or UIStatusBar will rotate even if
  // a hidden window is the one allowing for the orientation change
  if (self.view.window.hidden)
    return NO;
  
  return YES;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload 
{
  [super viewDidUnload];
  
  self.titleLabel = nil;
  self.activityIndicatorView = nil;
  self.progressView = nil;
  self.darkeningVeil = nil;
}



-(void) setDarkenBackground:(BOOL)darken
{
  darkenBackground = darken;
  
  darkeningVeil.hidden = !darkenBackground;
}

-(void) setIsIndeterminate:(BOOL)indeterminate
{
  isIndeterminate = indeterminate;
  
  if (isIndeterminate)
  {
    progressView.hidden = YES;
    activityIndicatorView.hidden = NO;
    [activityIndicatorView startAnimating];
  }
  else 
  {
    progressView.hidden = NO;
    activityIndicatorView.hidden = YES;
  }

}

-(void) setProgress:(CGFloat)progress
{
  progressView.progress = progress;
}

@end
