//
//  OBRecentConnectionsViewController.m
//  Snoe
//
//  Created by Zai Chang on 9/21/10.
//  Copyright 2010 Oblong Industries. All rights reserved.
//

#import "OBRecentConnectionsViewController.h"
#import "OBAppData.h"
#import "MezzanineStyleSheet.h"
#import "NSString+VersionChecking.h"
#import "UITraitCollection+Additions.h"

#define _kMAXROWS 5 

@implementation OBRecentConnectionsViewController

#pragma mark - View lifecycle

- (void)viewDidLoad
{
  [super viewDidLoad];
  
  self.view.accessibilityIdentifier = @"ConnectionHistoryView";
  self.view.accessibilityLabel = @"Connection History";
  
  self.navigationItem.title = NSLocalizedString(@"Recent Servers Title", nil);;
  
  // Since the new appearance for now is only here, I didn't want to change it MezzanineStyleSheet
  NSDictionary *barButtonTextAttributes = @{NSFontAttributeName: [UIFont dinBoldOfSize:[[MezzanineStyleSheet sharedStyleSheet] buttonSmallFontSize]],
                                            NSForegroundColorAttributeName: [[MezzanineStyleSheet sharedStyleSheet] horizonBlueColorActive] };
  [[UIBarButtonItem appearance] setTitleTextAttributes:barButtonTextAttributes forState:UIControlStateNormal];

  if (!(self.traitCollection.isRegularHeight && self.traitCollection.isRegularWidth))
  {
    UIImage *img = [UIImage imageNamed:@"caret-dismissal"];
    UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelButton.bounds = CGRectMake( 0, 0, img.size.width, img.size.height);
    [cancelButton setImage:img forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(cancel) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:cancelButton];
    self.navigationItem.leftBarButtonItem.accessibilityIdentifier = @"ConnectionHistoryViewCancelButton";
    self.navigationItem.leftBarButtonItem.accessibilityLabel = @"Cancel";
  }
  
  self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:[NSLocalizedString(@"Generic Button Title Clear", nil) uppercaseString] style:UIBarButtonItemStylePlain target:self action:@selector(clear)];
  self.navigationItem.rightBarButtonItem.accessibilityIdentifier = @"ConnectionHistoryViewClearButton";
  self.navigationItem.rightBarButtonItem.accessibilityLabel = @"Clear Connection History";
  [self updateClearButtonStatus];
  
  
  self.tableView.backgroundColor = [MezzanineStyleSheet sharedStyleSheet].grey30Color;
  self.tableView.separatorColor = [MezzanineStyleSheet sharedStyleSheet].grey70Color;
  self.tableView.scrollEnabled = [_recentConnections count] >= _kMAXROWS ? YES : NO;
  self.tableView.rowHeight = 44.0;
  
  // iOS 8: allow edge-to-edge separator lines
  if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)])
    self.tableView.separatorInset = UIEdgeInsetsZero;
  
}


- (CGSize)preferredContentSize
{
  // Set height to max is 5 rows.
  NSInteger numOfMaxRows = MIN([_recentConnections count], _kMAXROWS);
  
  if ([[UIDevice currentDevice] isIPad])
    numOfMaxRows ++;
  
  // 19.0 is the arrow height minus 1px to hide the last separator
  return CGSizeMake(320, numOfMaxRows * self.tableView.rowHeight + 19.0);
}


- (void)updateClearButtonStatus
{
  self.navigationItem.rightBarButtonItem.enabled = (_recentConnections.count > 0);
}


#pragma mark - Adaptative layout

- (BOOL)shouldAutorotate
{
  return NO;
}


- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
  return self.traitCollection.isiPad ? UIInterfaceOrientationMaskAll : UIInterfaceOrientationMaskPortrait;
}


- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
  return self.traitCollection.isiPad ? UIInterfaceOrientationUnknown : UIInterfaceOrientationPortrait;
}


#pragma mark -UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
  return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  if (_recentConnections)
    return self.traitCollection.isiPad ? [_recentConnections count] + 1 : [_recentConnections count];
  return 0;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  static NSString *CellIdentifier = @"Cell";
  static NSString *HeaderCellIdentifier = @"HeaderCell";
  
  if (indexPath.row == 0 && self.traitCollection.isiPad)
  {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:HeaderCellIdentifier];
    
    if (!cell)
    {
      cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:HeaderCellIdentifier];
      [self configureCell:cell forRow:indexPath];
    }
    return cell;

  } else {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
    {
      cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
      [self configureCell:cell forRow:indexPath];
    }
    return cell;

  }
  
  return  nil;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
  
  if (editingStyle == UITableViewCellEditingStyleDelete) {
    OBAppData *appData = [OBAppData sharedData];
    [[appData recentPools] removeObjectAtIndex:indexPath.row];
    [appData save];
    
    [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
  }
}


- (void)configureCell:(UITableViewCell *)cell forRow:(NSIndexPath *)indexPath
{
  // iOS 8: allow edge-to-edge separator lines
  if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    cell.layoutMargins = UIEdgeInsetsZero;
  
  if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)])
    cell.preservesSuperviewLayoutMargins = NO;
  
  cell.textLabel.textColor = [UIColor whiteColor];
 
  if (indexPath.row == 0 && self.traitCollection.isiPad)
  {
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.font = [UIFont dinOfSize:18.0];
    cell.backgroundColor = [[MezzanineStyleSheet sharedStyleSheet] grey20Color];
    cell.textLabel.text = NSLocalizedString(@"Recent Servers Title", nil);
    
    UIButton *clearButton = [[UIButton alloc] initWithFrame:CGRectMake( 0, 0, 40.0 , 30.0)];
    clearButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    [clearButton addTarget:self action:@selector(clear) forControlEvents:UIControlEventTouchUpInside];
    [clearButton setTitle:[NSLocalizedString(@"Generic Button Title Clear", nil) uppercaseString] forState:UIControlStateNormal];
    [[MezzanineStyleSheet sharedStyleSheet] stylizeCancelButtonPopover:clearButton];

    cell.accessoryView = clearButton;
  }
  else
  {
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.font = [UIFont dinMediumOfSize:16.0];
    
    UIView *selectedBackgroundView = [[UIView alloc] initWithFrame:CGRectZero];
    selectedBackgroundView.backgroundColor = [MezzanineStyleSheet sharedStyleSheet].selectedBlueColor;
    cell.selectedBackgroundView = selectedBackgroundView;
    
    NSInteger row = self.traitCollection.isiPad ? indexPath.row - 1 : indexPath.row;
    NSString *pool = [_recentConnections objectAtIndex:row];
    cell.textLabel.text = pool;
  }
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  if (self.traitCollection.isiPad && indexPath.row == 0) {
    return;
  }
  else if (_recentConnections)
  {
    NSInteger row = self.traitCollection.isiPad ? indexPath.row - 1 : indexPath.row;
    NSString *pool = [_recentConnections objectAtIndex:row];
    if ([_delegate respondsToSelector:@selector(recentConnectionSelected:)])
      [_delegate recentConnectionSelected:pool];
  }
}


#pragma mark - navigationItem Actions

- (void)cancel
{
  if ([_delegate respondsToSelector:@selector(recentConnectionsCancelled)])
    [_delegate recentConnectionsCancelled];
}


- (void)clear
{
  __weak typeof(self) __self = self;
  UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Clear Recent Servers", nil)
                                                                           message:NSLocalizedString(@"Are you sure you want to remove all recent servers from the list?", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
  UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil)
                                                         style:UIAlertActionStyleCancel
                                                       handler:nil];

  UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Okay", nil)
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * _Nonnull action) {
                                                         [[OBAppData sharedData] clearRecentPools];
                                                         [__self.tableView reloadData];
                                                         [__self updateClearButtonStatus];
                                                         
                                                         if ([__self.delegate respondsToSelector:@selector(recentConnectionsCleared)])
                                                           [__self.delegate recentConnectionsCleared];
                                                       }];
  [alertController addAction:cancelAction];
  [alertController addAction:confirmAction];
  [self presentViewController:alertController animated:YES completion:nil];
}

@end

