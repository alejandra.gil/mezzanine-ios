//
//  OBRecentConnectionsViewController.h
//  Snoe
//
//  Created by Zai Chang on 9/21/10.
//  Copyright 2010 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OBRecentConnectionsViewController : UITableViewController

@property (nonatomic, weak) id delegate;
@property (nonatomic, strong) NSArray *recentConnections;
@property (nonatomic, assign) CGFloat fontSize;

@end


@protocol OBRecentConnectionsViewControllerDelegate

@optional

- (void)recentConnectionSelected:(NSString*)pool;
- (void)recentConnectionsCancelled;
- (void)recentConnectionsCleared;

@end