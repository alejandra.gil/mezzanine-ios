//
//  UIThree20Helpers.h
//  Mezzanine
//
//  Created by Zai Chang on 10/14/10.
//  Copyright 2010 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OBStyleSheet.h"
#import "TTButton.h"


@interface UIViewController (TTStyle)

-(TTButton*) ttButtonWithStyle:(NSString*)style placeholder:(UIView*)placeHolderButton;

@end

