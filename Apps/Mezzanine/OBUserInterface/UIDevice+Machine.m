//
//  UIDevice+Machine.m
//  Places
//
//  Created by Zehao Chang on 10/13/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "UIDevice+Machine.h"
#include <sys/types.h>
#include <sys/sysctl.h>


@implementation UIDevice(Machine)

-(NSString*) machine
{

#if TARGET_IPHONE_SIMULATOR
  BOOL DEVICE_IS_SIMULATOR = YES;
#else
  BOOL DEVICE_IS_SIMULATOR = NO;
#endif

  NSString *machineString = nil;

  if (DEVICE_IS_SIMULATOR == YES)
  {
    // this neat trick is found at http://kelan.io/2015/easier-getenv-in-swift/
    machineString = [NSProcessInfo processInfo].environment[@"SIMULATOR_MODEL_IDENTIFIER"];
  }

  if (DEVICE_IS_SIMULATOR == NO || machineString == nil)
  {
    size_t size;

    // Set 'oldp' parameter to NULL to get the size of the data
    // returned so we can allocate appropriate amount of space
    sysctlbyname("hw.machine", NULL, &size, NULL, 0);

    // Allocate the space to store name
    char *name = malloc(size);
    
    // Get the platform name
    sysctlbyname("hw.machine", name, &size, NULL, 0);
    
    // Place name into a string
    machineString = [NSString stringWithCString:name encoding:NSUTF8StringEncoding];
    
    // Done with this
    free(name);
  }
	return machineString;
}

-(NSString*) machineMajorVersionString
{
	NSArray *components = [[self machine] componentsSeparatedByString:@","];
	if ([components count] != 2)
		return nil;
	return [[components objectAtIndex:0] stringByTrimmingCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]];
}

-(BOOL) isIOSSimulator
{
	return [[self model] hasSuffix:@"Simulator"];
}

-(BOOL) isIPodTouch
{
	return [[self model] caseInsensitiveCompare:@"iPod Touch"] == NSOrderedSame;
}

-(BOOL) isIPod2ndGen
{
	return [self isIPodTouch] && [[self machineMajorVersionString] isEqual:@"2"];
}

-(BOOL) isIPod3rdGenOrNewer
{
	return [self isIPodTouch] && [[self machineMajorVersionString] integerValue] >= 3;
}

-(BOOL) isIPod4thGenOrNewer
{
	return [self isIPodTouch] && [[self machineMajorVersionString] integerValue] >= 4;
}

-(BOOL) isIPod5thGenOrNewer
{
	return [self isIPodTouch] && [[self machineMajorVersionString] integerValue] >= 5;
}

-(BOOL) isIPhone
{
	return [[self model] caseInsensitiveCompare:@"iPhone"] == NSOrderedSame;
}

-(BOOL) isIPhone3GS
{
	return [self isIPhone] && [[self machineMajorVersionString] isEqual:@"2"];
}

-(BOOL) isIPhone3GSOrNewer
{
	return [self isIPhone] && [[self machineMajorVersionString] integerValue] >= 2;
}

-(BOOL) isIPhone4OrNewer
{
	return [self isIPhone] && [[self machineMajorVersionString] integerValue] >= 3;
}

-(BOOL) isIPhone4SOrNewer
{
	return [self isIPhone] && [[self machineMajorVersionString] integerValue] >= 4;
}

-(BOOL) isIPhone5OrNewer
{
	return [self isIPhone] && [[self machineMajorVersionString] integerValue] >= 5;
}

-(BOOL) isIPad
{
	return UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad;
}

-(BOOL) isIPad2OrNewer
{
  return [self isIPad] && [[self machineMajorVersionString] integerValue] >= 2;
}

-(BOOL) isIPad3OrNewer
{
  return [self isIPad] && [[self machineMajorVersionString] integerValue] >= 3;
}

-(NSInteger) majorSystemVersion
{
	return [[[[self systemVersion] componentsSeparatedByString:@"."] objectAtIndex:0] integerValue];
}

-(NSInteger) minorSystemVersion
{
	return [[[[self systemVersion] componentsSeparatedByString:@"."] objectAtIndex:1] integerValue];
}

-(BOOL) isIOS4
{
	return [self majorSystemVersion] >= 4;
	/*
    if ([[self systemVersion] compare:@"4.0" options:NSNumericSearch] == NSOrderedAscending)
    {
		//NSLog(@"Version less than 4.0: %@", currSysVer);
        return NO;
    }
	else 
	{
		//NSLog(@"Version greater or equal to 4.0: %@", currSysVer);
		return YES;
    }
	 */
}

-(BOOL) isIOS5OrAbove
{
	return [self majorSystemVersion] >= 5;
}

-(BOOL) isIOS6OrAbove
{
	return [self majorSystemVersion] >= 6;
}

-(BOOL) isIOS7OrAbove
{
	return [self majorSystemVersion] >= 7;
}

-(BOOL) isIOS71OrAbove
{
	return (([self majorSystemVersion] > 7) || (([self majorSystemVersion] == 7) && ([self minorSystemVersion] >= 1)));
}

-(BOOL) isIOS8OrAbove
{
	return [self majorSystemVersion] >= 8;
}

-(BOOL) isIOS81OrAbove
{
	return (([self majorSystemVersion] > 8) || (([self majorSystemVersion] == 8) && ([self minorSystemVersion] >= 1)));
}

-(BOOL) isIOS9OrAbove
{
  return [self majorSystemVersion] >= 9;
}


-(BOOL) isIOS10OrAbove
{
  return [self majorSystemVersion] >= 10;
}



@end
