//
//  OBAppData.m
//  iPhoneTest
//
//  Created by Zai Chang on 9/10/10.
//  Copyright 2010 Oblong Industries. All rights reserved.
//

#import "OBAppData.h"

#define RecentPoolsArrayKey @"RecentPools"
#define RecentPoolsDataKey @"RecentPoolsData"

@implementation OBAppData

@synthesize recentPools;
@synthesize currentPool;

+(OBAppData*) sharedData
{
	static OBAppData *_appData = NULL;
	if (_appData == NULL)
		_appData = [[OBAppData alloc] init];
	return _appData;
}


-(id) init
{
	if (self = [super init])
	{
    NSMutableArray * poolsArray = nil;

    // Check Bug 12507
    // Due to a bug in iOS 8.0 affecting apps built with iOS 7.1 SDK objects like
    // NSMutableArray or NSString need to be encapsulated into NSData to be saved
    // at the standard user defaults, otherwise they are not really stored.

    NSData* poolsData = [[NSUserDefaults standardUserDefaults] objectForKey:RecentPoolsDataKey];
    if (poolsData == nil)
    {
      poolsArray = [[NSUserDefaults standardUserDefaults] objectForKey:RecentPoolsArrayKey];
      if (poolsArray == nil)
        poolsArray = [NSMutableArray new];
    }
    else
    {
      poolsArray = [NSKeyedUnarchiver unarchiveObjectWithData:poolsData];
    }

    // Besides that, iOS 8.0, 8.1 SDKs convert NSMutableArray to NSArray when
    // stored / retrieved from NSUserDefaults, so what follows is needed to let it be mutable
    recentPools = [NSMutableArray arrayWithArray:poolsArray];
	}
  return self;
}




-(void) noteRecentPool:(NSString*)pool
{
  static NSInteger maximumNumberOfPools = 50;
  [self noteRecentPool:pool limit:maximumNumberOfPools];
}


-(void) noteRecentPool:(NSString*)pool limit:(NSInteger)limit
{
  NSMutableArray *toRemove = [NSMutableArray array];
  for (NSString *existingRecentPool in recentPools)
  {
    // Remove duplicates from the list by case insensitive matching
    if ([pool compare:existingRecentPool options:NSCaseInsensitiveSearch] == NSOrderedSame)
      [toRemove addObject:existingRecentPool];
  }
  [recentPools removeObjectsInArray:toRemove];

  [recentPools insertObject:pool atIndex:0];

  // Limit number of pools
  if ([recentPools count] > limit)
    [recentPools removeObjectsInRange:NSMakeRange(limit, recentPools.count - limit)];

  [self save];
}


-(void) clearRecentPools
{
  [recentPools removeAllObjects];
  [self save];
}


-(void) save
{
  [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:recentPools] forKey:RecentPoolsDataKey];
  [[NSUserDefaults standardUserDefaults] synchronize];
}

@end

