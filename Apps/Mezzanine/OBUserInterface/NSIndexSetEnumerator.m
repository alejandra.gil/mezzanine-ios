//
//  NSIndexSetEnumerator.m
//

#import "NSIndexSetEnumerator.h"


@implementation NSIndexSetEnumerator

+ (id) enumeratorWithIndexSet:(NSIndexSet *)theSet
{
  NSRange myRange = NSMakeRange([theSet firstIndex], [theSet lastIndex] + 1);
  return [NSIndexSetEnumerator enumeratorWithIndexSet:theSet range:myRange];
}

+ (id) enumeratorWithIndexSet:(NSIndexSet *)theSet range:(NSRange)myRange
{
  return [[NSIndexSetEnumerator alloc] initWithIndexSet:theSet range:myRange];
}

- (id) initWithIndexSet:(NSIndexSet *)theSet range:(NSRange)myRange
{
  self = [super init];
  if (!self) 
  {
    return nil;
  }
  
  m_uCurrentPosition = -1;
  
  m_uIndexCount = [theSet count];
  m_puIndexes = (NSUInteger *)malloc(sizeof(unsigned long) * m_uIndexCount);
  
  [theSet getIndexes:m_puIndexes maxCount:m_uIndexCount inIndexRange:&myRange];
  
  return self;
}

-(void) dealloc
{
  free(m_puIndexes);
}

-(BOOL)nextIndex:(NSUInteger *)pValue
{
  m_uCurrentPosition++;
  if (m_uCurrentPosition >= m_uIndexCount)
    return FALSE;
  *pValue = m_puIndexes[m_uCurrentPosition];
  return TRUE;
}

- (NSUInteger) indexAtIndex:(NSUInteger)i
{
  if (i >= m_uIndexCount)
    return NSNotFound;
  return m_puIndexes[i];
}

-(void)reset
{
  m_uCurrentPosition = -1;
}

-(NSUInteger)count
{
  return m_uIndexCount;
}

@end
