//
//  UIProgressViewController.h
//  Mezzanine
//
//  Created by Zai Chang on 10/12/10.
//  Copyright 2010 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UIProgressViewController : UIViewController 
{
  UILabel *titleLabel;
  UIActivityIndicatorView *activityIndicatorView;
  UIProgressView *progressView;
  UIView *darkeningVeil;
  
  BOOL darkenBackground;
  BOOL isIndeterminate;
}
@property (nonatomic,strong) IBOutlet UILabel *titleLabel;
@property (nonatomic,strong) IBOutlet UIActivityIndicatorView *activityIndicatorView;
@property (nonatomic,strong) IBOutlet UIProgressView *progressView;
@property (nonatomic,strong) IBOutlet UIView *darkeningVeil;

@property (nonatomic, assign) BOOL darkenBackground;
@property (nonatomic, assign) BOOL isIndeterminate;
@property (nonatomic, assign) CGFloat progress;

@end
