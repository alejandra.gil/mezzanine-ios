//
//  OBDirectionalPanGestureRecognizer.h
//  OBUserInterface
//
//  Created by Zai Chang on 2/17/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>


/// Extends UIPanGestureRecognizer such that it only recognizes a gesture
/// in a particular direction.
/// Based on http://stackoverflow.com/questions/7100884/uipangesturerecognizer-only-vertical-or-horizontal

typedef enum 
{
  OBDirectionalPanUp = 1 << 0,
  OBDirectionalPanDown = 1 << 1,
  OBDirectionalPanLeft = 1 << 2,
  OBDirectionalPanRight = 1 << 3,
  OBDirectionalPanVertical = OBDirectionalPanUp | OBDirectionalPanDown,
  OBDirectionalPanHorizontal = OBDirectionalPanLeft | OBDirectionalPanRight
} OBDirectionalPanDirection;


@interface OBDirectionalPanGestureRecognizer : UIPanGestureRecognizer
{
  BOOL _drag;
  int _moveX;
  int _moveY;
}
@property (nonatomic, assign) OBDirectionalPanDirection direction;

// Defines the angle from axis, in degrees, within which the gesture is recognized
// Defaults to 45 ie. the cone of permissibility is 90 degrees wide
@property (nonatomic, assign) CGFloat angleThreshold;

@end
