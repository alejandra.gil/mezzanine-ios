//
//  UIImage+Resizing.m
//  iPhoneTest
//
//  Created by Zai Chang on 9/17/10.
//  Copyright 2010 Oblong Industries. All rights reserved.
//

#import "UIImage+Resizing.h"


CGRect ProportionalResizeInto( CGRect srcRect, const CGRect inRect )
{
	CGFloat	sx = CGRectGetWidth(srcRect);
	CGFloat	sy = CGRectGetHeight(srcRect);
	CGFloat	dx = CGRectGetWidth(inRect);
	CGFloat	dy = CGRectGetHeight(inRect);
	
	if( sx != 0 && sy != 0 )
	{
		dx	= dx / sx;	//if ((ws>1)&&(!allowScaleUp))	wscale	= 1.0f;
		dy	= dy / sy;	//if ((hs>1)&&(!allowScaleUp))	hscale	= 1.0f;
		
		// take smaller of the scales!
		CGFloat	scale	= (dx < dy)	? dx : dy;
		
		srcRect.size.width	= (sx * scale);
		srcRect.size.height	= (sy * scale);
	}
	else
	{
		//Assert(0);
		srcRect.size.width	= 0;
		srcRect.size.height	= 0;
	}
	return srcRect;
}

CGRect ProportionalResizeIntoAndCenter (CGRect srcRect, const CGRect inRect)
{
  CGRect resizedRect = ProportionalResizeInto(srcRect, inRect);
  resizedRect.origin.x = inRect.origin.x;
  resizedRect.origin.y = inRect.origin.y;
  if (resizedRect.size.width < inRect.size.width)
    resizedRect.origin.x += (inRect.size.width-resizedRect.size.width)/2;
  if (resizedRect.size.height < inRect.size.height)
    resizedRect.origin.y += (inRect.size.height-resizedRect.size.height)/2;
  return resizedRect;
}


@implementation UIImage (Resizing)

// With help from http://vocaro.com/trevor/blog/2009/10/12/resize-a-uiimage-the-right-way/
// 
-(UIImage *) resizeImage:(CGSize) newSize
{
	CGImageRef imageRef = [self CGImage];
//	CGImageAlphaInfo alphaInfo = CGImageGetAlphaInfo(imageRef);
  CGBitmapInfo bitmapInfo = CGImageGetBitmapInfo(imageRef);
	CGColorSpaceRef colorSpaceInfo = CGColorSpaceCreateDeviceRGB();
	
//	if (alphaInfo == kCGImageAlphaNone)
//		alphaInfo = kCGImageAlphaNoneSkipLast;

	CGFloat width = newSize.width;
  CGFloat height = newSize.height;
	
  UIImageOrientation sourceOrientation = self.imageOrientation;
  
//  CGRect targetRect = (self.imageOrientation == UIImageOrientationUp || self.imageOrientation == UIImageOrientationDown) ?
//      CGRectMake(0, 0, width, height) : CGRectMake(0, 0, height, width);
  CGRect targetRect = CGRectMake(0, 0, width, height);
  CGRect drawRect = CGRectMake(0, 0, width, height);
	  
  CGContextRef context = CGBitmapContextCreate(NULL, targetRect.size.width, targetRect.size.height, CGImageGetBitsPerComponent(imageRef), CGImageGetBytesPerRow(imageRef), colorSpaceInfo, bitmapInfo);
  
  CGAffineTransform transform = CGAffineTransformIdentity;
  
  switch (sourceOrientation) {
    case UIImageOrientationUp:
      // Nothing to do
      break;
    
    case UIImageOrientationDown:           // EXIF = 3
    case UIImageOrientationDownMirrored:   // EXIF = 4
      transform = CGAffineTransformTranslate(transform, newSize.width, newSize.height);
      transform = CGAffineTransformRotate(transform, M_PI);
      break;
      
    case UIImageOrientationLeft:           // EXIF = 6
    case UIImageOrientationLeftMirrored:   // EXIF = 5
      transform = CGAffineTransformTranslate(transform, newSize.width, 0);
      transform = CGAffineTransformRotate(transform, M_PI_2);
      drawRect = CGRectMake(0, 0, newSize.height, newSize.width);
      break;
      
    case UIImageOrientationRight:          // EXIF = 8
    case UIImageOrientationRightMirrored:  // EXIF = 7
      transform = CGAffineTransformTranslate(transform, 0, newSize.height);
      transform = CGAffineTransformRotate(transform, -M_PI_2);
      drawRect = CGRectMake(0, 0, newSize.height, newSize.width);
      break;
      
    case UIImageOrientationUpMirrored:
      // Currently not handled
      break;
  }
  
  switch (self.imageOrientation) {
    case UIImageOrientationUpMirrored:     // EXIF = 2
    case UIImageOrientationDownMirrored:   // EXIF = 4
      transform = CGAffineTransformTranslate(transform, newSize.width, 0);
      transform = CGAffineTransformScale(transform, -1, 1);
      break;
      
    case UIImageOrientationLeftMirrored:   // EXIF = 5
    case UIImageOrientationRightMirrored:  // EXIF = 7
      transform = CGAffineTransformTranslate(transform, newSize.height, 0);
      transform = CGAffineTransformScale(transform, -1, 1);
      break;
    
    default:
      break;
  }
  
  CGContextConcatCTM(context, transform);
  
	
	CGContextDrawImage(context, drawRect, imageRef);
	CGImageRef ref = CGBitmapContextCreateImage(context);
	UIImage *result = [UIImage imageWithCGImage:ref];
	
	CGContextRelease(context);
	CGImageRelease(ref);
  CGColorSpaceRelease(colorSpaceInfo);

	return result;	
}

-(UIImage *) resizeImagePreservingAspectRatio:(CGSize) newSize
{
  CGSize imageSize = [self size];
  CGRect sourceRect = CGRectMake(0, 0, imageSize.width, imageSize.height);
  CGRect targetRect = ProportionalResizeInto(sourceRect, CGRectMake(0, 0, newSize.width, newSize.height));
  
  return [self resizeImage:targetRect.size];
}

@end
