//
//  UIFont+Oblong.m
//  OBUserInterface
//
//  Created by Zai Chang on 6/28/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import "UIFont+Oblong.h"


@implementation UIFont (Oblong)

// When using preferredContentSizeCategory, remember that relevant controllers need to
// listen to UIContentSizeCategoryDidChangeNotification and update the interface accordingly
+(NSInteger) fontSizeAdjustment
{
  UIApplication *application = [UIApplication sharedApplication];
  if (![application respondsToSelector:@selector(preferredContentSizeCategory)])
    return 0;
  
  NSString *preferredContentSizeCategory = application.preferredContentSizeCategory;
  if ([preferredContentSizeCategory isEqual:UIContentSizeCategoryExtraSmall])
    return -3;
  if ([preferredContentSizeCategory isEqual:UIContentSizeCategorySmall])
    return -2;
  if ([preferredContentSizeCategory isEqual:UIContentSizeCategoryMedium])
    return -1;
  if ([preferredContentSizeCategory isEqual:UIContentSizeCategoryLarge])
    return 0;
  if ([preferredContentSizeCategory isEqual:UIContentSizeCategoryExtraLarge])
    return 1;
  if ([preferredContentSizeCategory isEqual:UIContentSizeCategoryExtraExtraLarge])
    return 2;
  if ([preferredContentSizeCategory isEqual:UIContentSizeCategoryExtraExtraExtraLarge])
    return 3;
  if ([preferredContentSizeCategory isEqual:UIContentSizeCategoryAccessibilityMedium])
    return 4;
  if ([preferredContentSizeCategory isEqual:UIContentSizeCategoryAccessibilityLarge])
    return 6;
  if ([preferredContentSizeCategory isEqual:UIContentSizeCategoryAccessibilityExtraLarge])
    return 8;
  if ([preferredContentSizeCategory isEqual:UIContentSizeCategoryAccessibilityExtraExtraLarge])
    return 10;
  if ([preferredContentSizeCategory isEqual:UIContentSizeCategoryAccessibilityExtraExtraExtraLarge])
    return 12;
  
  return 0;
}


+(UIFont*) fontWithNameWithFallback:(NSString*)fontName ofSize:(CGFloat)fontSize respectUserContentSizeSetting:(BOOL)respectUserContentSizeSetting
{
  if (respectUserContentSizeSetting)
    fontSize = [self fontSizeAdjustment] + fontSize;
  
  UIFont *font = [UIFont fontWithName:fontName size:fontSize];
  if (font == nil)
    font = [UIFont systemFontOfSize:fontSize];
  return font;
}

+(UIFont*) dinOfSize:(CGFloat)fontSize
{
  return [self fontWithNameWithFallback:@"DINOT" ofSize:fontSize respectUserContentSizeSetting:YES];
}

+(UIFont*) dinLightOfSize:(CGFloat)fontSize
{
  return [self fontWithNameWithFallback:@"DINOT-Light" ofSize:fontSize respectUserContentSizeSetting:YES];
}

+(UIFont*) dinMediumOfSize:(CGFloat)fontSize
{
  return [self fontWithNameWithFallback:@"DINOT-Medium" ofSize:fontSize respectUserContentSizeSetting:YES];
}

+(UIFont*) dinBoldOfSize:(CGFloat)fontSize
{
  return [self fontWithNameWithFallback:@"DINOT-Bold" ofSize:fontSize respectUserContentSizeSetting:YES];
}

+(UIFont*) dinBlackOfSize:(CGFloat)fontSize
{
  return [self fontWithNameWithFallback:@"DINOT-Black" ofSize:fontSize respectUserContentSizeSetting:YES];
}

+(UIFont*) dinItalicOfSize:(CGFloat)fontSize
{
  return [self fontWithNameWithFallback:@"DINOffc-Ita" ofSize:fontSize respectUserContentSizeSetting:YES];
}

+(UIFont*) dinItalicLightOfSize:(CGFloat)fontSize
{
  return [self fontWithNameWithFallback:@"DINOffc-LightIta" ofSize:fontSize respectUserContentSizeSetting:YES];
}

+(UIFont*) dinItalicMediumOfSize:(CGFloat)fontSize
{
  return [self fontWithNameWithFallback:@"DINOffc-MediIta" ofSize:fontSize respectUserContentSizeSetting:YES];
}

+(UIFont*) dinItalicBoldOfSize:(CGFloat)fontSize
{
  return [self fontWithNameWithFallback:@"DINOffc-BoldIta" ofSize:fontSize respectUserContentSizeSetting:YES];
}

+(UIFont*) dinItalicBlackOfSize:(CGFloat)fontSize
{
  return [self fontWithNameWithFallback:@"DINOffc-BlackIta" ofSize:fontSize respectUserContentSizeSetting:YES];
}

@end
