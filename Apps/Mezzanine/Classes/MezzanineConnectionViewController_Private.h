//
//  MezzanineConnectionViewController_Private.h
//  Mezzanine
//
//  Created by Miguel Sánchez Valdés on 23/10/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

#import "MezzanineConnectionViewController.h"
#import "OBRecentConnectionsViewController.h"

@interface MezzanineConnectionViewController ()
{
  CGRect logoImageViewPortraitFrame;
  FPPopoverController *recentConnectionsPopover;
  OBRecentConnectionsViewController *recentConnectionsViewController;
  NSMutableArray *observationTokens;

  //Analytics
  NSString *_connectionSource;
}

@property (nonatomic, strong) UIButton *recentConnectionsButton;

- (void)showRecentConnections;
- (void)updateHistoryButton;
- (void)updateConnectButton;

- (void)setInterfaceEnabled:(BOOL)enabled;
- (NSString *)sanitizePoolname;

// TODO: Remove when teamwork toggle is deleted
// Automatic reconnection for teamworkUI settings toggle
- (void)connectTo:(NSString *)name;

@end
