//
//  PortfolioMenuViewController.h
//  Mezzanine
//
//  Created by Zai Chang on 9/25/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import "ButtonMenuViewController.h"


@interface PortfolioMoreMenuViewController : ButtonMenuViewController

@property (nonatomic, strong) MZSystemModel *systemModel;
@property (nonatomic, strong) MZPresentation *presentation;
@property (nonatomic, strong) MZExportInfo *exportInfo;

@property (nonatomic, copy) void (^requestPortfolioExport)();
@property (nonatomic, copy) void (^requestPortfolioDeleteAll)();

@end
