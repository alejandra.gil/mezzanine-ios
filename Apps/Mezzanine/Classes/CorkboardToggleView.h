//
//  CorkboardToggleView.h
//  Mezzanine
//
//  Created by miguel on 04/09/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CorkboardToggleViewDelegate;

@interface CorkboardToggleView : UIView

@property (nonatomic, weak) id <CorkboardToggleViewDelegate> delegate;
@property (nonatomic, strong) MZSystemModel *systemModel;
@property (nonatomic, assign) BOOL selected;

- (CGSize)calculateSizeForView;
- (void)toggleCorkboard:(id)sender;

@end

@protocol CorkboardToggleViewDelegate <NSObject>

- (void)didTapOnCorkboardToggle;

@end