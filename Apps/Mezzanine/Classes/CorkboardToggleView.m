//
//  CorkboardToggleView.m
//  Mezzanine
//
//  Created by miguel on 04/09/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import "CorkboardToggleView.h"

#import "NSString+VersionChecking.h"
#import "NSObject+BlockObservation.h"
#import "MezzanineStyleSheet.h"


@interface CorkboardToggleView ()
{
  NSMutableArray *_observers;
  UIButton *corkboardButton;
}

@end

@implementation CorkboardToggleView

- (void)initialize
{
  self.backgroundColor = [UIColor clearColor];
}


- (instancetype)initWithFrame:(CGRect)frame
{
  self = [super initWithFrame:frame];
  if (self)
  {
    [self initialize];
  }
  return self;
}


- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
  self = [super initWithCoder:aDecoder];
  if (self) {
    [self initialize];
  }
  return self;
}


- (void)dealloc
{
  if (_observers)
    for (id observer in _observers)
      [_systemModel removeObserverWithBlockToken:observer];

  _observers = nil;
}


#pragma mark - Properties

- (void)setSystemModel:(MZSystemModel *)systemModel
{
  if (systemModel != _systemModel)
  {
    if (_systemModel)
    {
      for (id observer in _observers)
        [_systemModel removeObserverWithBlockToken:observer];
      _observers = nil;
    }

    _systemModel = systemModel;

    if (_systemModel)
    {
      _observers = [NSMutableArray array];
      id observer;
      __weak typeof(self) __self = self;

      observer = [_systemModel addObserverForKeyPath:@"surfaces" options:(NSKeyValueObservingOptionNew) onQueue:[NSOperationQueue mainQueue] task:^(id object, NSDictionary *change) {
        [__self updateLayout];
      }];
      [_observers addObject:observer];

      [self updateLayout];
    }
  }
}

- (void)setSelected:(BOOL)selected
{
  _selected = selected;
  corkboardButton.selected = _selected;

  if ([self shouldHide])
    return;
}


#pragma mark - Layout

- (void)updateLayout
{
  if ([self shouldHide])
  {
    corkboardButton.hidden = YES;
    return;
  }

  if (!corkboardButton)
  {
    corkboardButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [corkboardButton setFrame:CGRectMake(0, 6, 18, 30)];
    [corkboardButton setShowsTouchWhenHighlighted:YES];
    [corkboardButton addTarget:self action:@selector(toggleCorkboard:) forControlEvents:UIControlEventTouchUpInside];
    [corkboardButton setImage:[[UIImage imageNamed:@"mobile-corkboards-icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    corkboardButton.imageView.tintColor = [UIColor whiteColor];
    [self addSubview:corkboardButton];
  }

  corkboardButton.hidden = NO;
}


- (CGSize)calculateSizeForView
{
  if ([self shouldHide])
    return CGSizeZero;

  CGFloat totalCorkboards = [_systemModel sideWallScreenCount] > 1 ? [self corkboardSize].width + 2.0 : [self corkboardSize].width;

  return CGSizeMake(totalCorkboards, self.frame.size.height);
}


- (BOOL)shouldHide
{
  return ([_systemModel sideWallScreenCount] == 0);
}


#pragma mark - Layout Helpers

- (CGSize)corkboardSize
{
  if ([_systemModel sideWallScreenCount] == 0)
    return CGSizeZero;

  CGFloat aspectRatio = 9.0 / 16.0;
  CGFloat corkboardHeight = ceil(self.frame.size.height);
  CGFloat corkboardWidth = ceil(corkboardHeight * aspectRatio);

  return CGSizeMake((NSInteger)corkboardWidth, (NSInteger)corkboardHeight);
}



#pragma mark - Actions

- (void)toggleCorkboard:(id)sender
{
  self.selected = !self.selected;

  SEL selector = NSSelectorFromString(@"didTapOnFeld:");
  if ([_delegate respondsToSelector:selector])
    [_delegate didTapOnCorkboardToggle];

  corkboardButton.selected = self.selected;
}

@end
