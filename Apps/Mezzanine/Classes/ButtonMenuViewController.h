//
//  ButtonMenuViewController.h
//  Mezzanine
//
//  Created by Zai Chang on 6/19/13.
//  Copyright (c) 2013 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>


/// ButtonMenuViewController


@interface ButtonMenuViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSArray *menuItems;
@property (nonatomic, strong) UITableView *tableView;

@end



// Displays a button item within the menu
@interface ButtonMenuItem : NSObject

@property (nonatomic, strong) NSString *buttonTitle;
@property (nonatomic, strong) UIColor *buttonColor;
@property (nonatomic, copy) void (^action)();
@property (nonatomic, assign) BOOL enabled;

+(ButtonMenuItem *) itemWithTitle:(NSString*)title action:(void (^)())action;

@end


@interface ButtonMenuTextItem : NSObject

@property (nonatomic, strong) NSString *buttonTitle;
@property (nonatomic, strong) NSString *buttonText;
@property (nonatomic, strong) UIFont *buttonFont;
@property (nonatomic, strong) UIColor *buttonColor;
@property (nonatomic, copy) void (^action)();

+(ButtonMenuTextItem *) itemWithText:(NSString *)text title:(NSString*)title action:(void (^)())action;

@end


@interface ButtonMenuHeaderTextItem : ButtonMenuTextItem
@end
