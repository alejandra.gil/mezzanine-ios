//
//  ParticipantsCounterViewController.swift
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 06/03/2019.
//  Copyright © 2019 Oblong Industries. All rights reserved.
//

import UIKit

class ParticipantsCounterViewController: UIViewController {

  @IBOutlet var titleLabel: UILabel!
  @IBOutlet var divider: UIView!

  override func viewDidLoad() {
    super.viewDidLoad()

    guard let styleSheet = MezzanineStyleSheet.shared() else { return }
    styleSheet.stylizeSectionHeaderLabel(titleLabel)
    divider.backgroundColor = styleSheet.grey150Color
  }

  func updateParticipantsCount(roomsCount: Int, peopleCount: Int) {
    // Rooms
    let roomsString = (roomsCount == 0 || roomsCount > 1) ? String(format: "Rooms Number Of Participants".localized, String(roomsCount)) : String(format: "Rooms Number Of Participants Singular".localized, String(roomsCount))

    // People
    let peopleString = (peopleCount == 0 || peopleCount > 1) ? String(format: "People Number Of Participants".localized, String(peopleCount)) : String(format: "People Number Of Participants Singular".localized, String(peopleCount))

    titleLabel.text = String(format: "%@, %@", roomsString, peopleString)
  }
}
