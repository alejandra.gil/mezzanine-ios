//
//  MuteButton.swift
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 11/11/2016.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

import UIKit
import AVFoundation

enum MuteButtonType: NSInteger {
  case video = 0
  case audio = 1
  case audioLarge = 2
}


class MuteButton: UIButton {
  final var showsTitle: Bool = true {
    didSet {
      self.titleLabel?.alpha = showsTitle == true ? 1.0 : 0.0
    }
  }
  
  final var isMuted: Bool = false {
    willSet(newValue) {
      updateButton(self.type)
    }
  }
  
  final var isAllowed: Bool = false
  
  final var type: MuteButtonType! {
    didSet {
      updatePermissions()
    }
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    
    // Title setup
    self.titleLabel!.font = UIFont.dinMedium(ofSize: 13)
    let offset: CGFloat = 26.0
    let imageInset = (self.frame.width - (self.imageView?.frame)!.width) / 2
    self.titleEdgeInsets = UIEdgeInsetsMake((self.imageView?.frame)!.height + offset, -((self.imageView?.frame)!.width + imageInset), 0, 0)
    self.titleLabel?.textAlignment = .center
    self.setTitle("ON", for: UIControlState())
  }
  
  
  // MARK: Layout
  
  // Sets image according to permissions
  override func setImage(_ image: UIImage?, for state: UIControlState) {
    DispatchQueue.main.async {
      if self.isAllowed == false {
        if self.type == .audio {
          super.setImage(UIImage.init(named: "mobile-mic-off"), for: UIControlState())
        } else if self.type == .audioLarge {
          super.setImage(UIImage.init(named: "mobile-mic-off-large"), for: UIControlState())
        } else {
          super.setImage(UIImage.init(named: "mobile-camera-off"), for: UIControlState())
        }
        
        self.setTitleColor(MezzanineStyleSheet.shared().grey130Color, for: UIControlState())
        self.setTitle("OFF", for: UIControlState())
      } else {
        super.setImage(image, for: state)
      }
    }
  }
  
  // Sets image according to mute state
  func updateButton(_ type: MuteButtonType) {
    DispatchQueue.main.async {
      if type == .audio {
        self.setImage(UIImage.init(named: (!self.isMuted ? "mobile-mic-on" : "mobile-mic-off")), for: UIControlState())
        self.setTitleColor(MezzanineStyleSheet.shared().grey130Color, for: UIControlState())
        self.setTitle(!self.isMuted ? "ON" : "OFF", for: UIControlState())
      } else if type == .audioLarge {
        self.setImage(UIImage.init(named: (!self.isMuted ? "mobile-mic-on-large" : "mobile-mic-off-large")), for: UIControlState())
        self.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        self.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        self.setTitle(nil, for: UIControlState())
      } else {
        self.setImage(UIImage.init(named: (!self.isMuted ? "mobile-camera-on" : "mobile-camera-off")), for: UIControlState())
        self.setTitleColor(MezzanineStyleSheet.shared().grey130Color, for: UIControlState())
        self.setTitle(!self.isMuted ? "ON" : "OFF", for: UIControlState())
      }
    }
  }
  
  
  // MARK: Logic
  func updatePermissions() {
    if type == .audio || type == .audioLarge {
      AVAudioSession.sharedInstance().requestRecordPermission { (granted) in
        self.isAllowed = granted
        self.isMuted = !granted
      }
    } else {
      let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
      switch cameraAuthorizationStatus {
      case .denied, .restricted:
        self.isAllowed = false
        self.isMuted = true
        
        break
      case .authorized:
        self.isAllowed = true
        self.isMuted = false
        
        break
      case .notDetermined:
        AVCaptureDevice.requestAccess(for: AVMediaType.video) { granted in
          self.isAllowed = granted
          self.isMuted = !granted
        }
        
        break
      }
    }
  }
  
  // Updates mute states and pass the event. If we don't have permissions we show the alert.
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    if isAllowed == true {
      isMuted = !isMuted
      self.sendActions(for: UIControlEvents.touchUpInside)
    } else {
      let alertController = buildSettingAlert()
      show(alertController, animated: true, completion: nil)
    }
  }
  
  
  // MARK: Show settings
  
  fileprivate func buildSettingAlert() -> UIAlertController {
    
    var alertTitle = "Settings Audio Alert Title".localized
    var alertMessage = "Settings Audio Alert Message".localized
    
    if type == .video {
      alertTitle = "Settings Video Alert Title".localized
      alertMessage = "Settings Video Alert Message".localized
    }
    
    let alertController = UIAlertController (title: alertTitle, message: alertMessage, preferredStyle: .alert)
    let settingsAction = UIAlertAction(title: "Generic Title Settings".localized, style: .default) { (_) -> Void in
      let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
      if let url = settingsUrl {
        UIApplication.shared.openURL(url)
      }
    }
    
    let cancelAction = UIAlertAction(title: "Generic Title OK".localized, style: .default, handler: nil)
    alertController.addAction(settingsAction)
    alertController.addAction(cancelAction)
    
    return alertController;
  }
}
