//
//  AssetView.h
//  Mezzanine
//
//  Created by Miguel Sánchez Valdés on 3/12/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MZWindshieldItem.h"
#import "OBDirectionalPanGestureRecognizer.h"

@interface AssetView : UIView <UIGestureRecognizerDelegate>
{
  @protected
  id asset;
  id __weak userInfo; // For extra metadata
  
  UIView *contentView;
  CIFilter *contentFilter;
  BOOL actionsPanelVisible;
  UIView *deleteShroud;
  UIView *actionsPanel;
  CAGradientLayer *contentShadowLayer;  // Appears underneath the content and above the action panel, to highlight the fact that the action panel is a layer below 
  CALayer *shadowLayer;
  
  CGPoint verticalPanGestureStart;
  NSMutableArray *assetObservers;
  CGFloat actionsPanelAnimationState;
}

@property (nonatomic, strong) id asset;
@property (nonatomic, weak) id userInfo;

@property (nonatomic, strong, readonly) OBDirectionalPanGestureRecognizer *panRecognizer; // Is this still needed

@property (nonatomic, strong, readonly) UIView *contentView;
@property (nonatomic, strong) UIView *actionsPanel;
@property (nonatomic, copy) void (^actionsPanelBecameVisible)(AssetView *);

@property (nonatomic, strong) UIControl *deleteButton;
@property (nonatomic, assign) BOOL actionsPanelEnabled; // Disabling will cause gesture recognizer not to activate
@property (nonatomic, assign) BOOL actionsPanelVisible;
@property (nonatomic, copy) void (^deleteButtonTapped)(AssetView *);

@property (nonatomic, assign) BOOL isBeingManipulated; // YES when user is scaling an asset
@property (nonatomic, assign) CGFloat marginForGestures; // YES when user is scaling an asset

@property (nonatomic, strong) UIView *overlayView;
@property (nonatomic, strong) UIColor *borderColor;

@property (nonatomic, assign) BOOL useAutolayout;

-(void) setActionsPanelAnimationState:(CGFloat)state;
-(void) setActionsPanelVisible:(BOOL)visible animated:(BOOL)animated;
-(void) deleteAssetRequested:(id)sender;
-(void) updateViewStatesAnimated:(BOOL)animated;

@end
