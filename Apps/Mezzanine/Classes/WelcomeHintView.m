//
//  WelcomeHintView.m
//  Mezzanine
//
//  Created by miguel on 23/07/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import "WelcomeHintView.h"
#import "MezzanineStyleSheet.h"
#import "UITraitCollection+Additions.h"
#import "Mezzanine-Swift.h"

@implementation WelcomeHintView

@synthesize titleLabel;
@synthesize logoImageView;
@synthesize zoomScale;
@synthesize alreadyLoaded;

static CGFloat maxFontSize = 150.0;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
      zoomScale = 1.0;

      MezzanineStyleSheet *styleSheet = [MezzanineStyleSheet sharedStyleSheet];

      titleLabel = [UILabel new];
      titleLabel.font = [UIFont dinOfSize: [SettingsFeatureToggles sharedInstance].teamworkUI ? 40.0 : maxFontSize];
      titleLabel.textColor = styleSheet.mediumFillColor;
      titleLabel.adjustsFontSizeToFitWidth = YES;
      titleLabel.minimumScaleFactor = 0.01;
      titleLabel.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
      titleLabel.textAlignment = self.traitCollection.isRegularWidth ? NSTextAlignmentLeft : NSTextAlignmentCenter;
      titleLabel.accessibilityIdentifier = @"WelcomeHintView.Label";
      [self addSubview:titleLabel];
      
      logoImageView = [UIImageView new];
      logoImageView.contentMode = UIViewContentModeScaleAspectFit;
      logoImageView.accessibilityIdentifier = @"WelcomeHintView.LogoImageView";
      [self addSubview:logoImageView];
    }
  
    return self;
}

- (void)setFrame:(CGRect)frame
{
  [super setFrame:frame];
  
  CGFloat viewWidth = CGRectGetWidth(frame);
  CGFloat viewHeight = CGRectGetHeight(frame);
  
  CGRect titleLabelFrame = self.traitCollection.isRegularWidth ? CGRectMake(viewWidth * 0.02, 0.0, viewWidth * 0.62, viewHeight) : CGRectInset(self.bounds, viewWidth * 0.2, viewHeight * 0.2);
  
  CGFloat scale = viewWidth && titleLabel.frame.size.width ? titleLabelFrame.size.width / titleLabel.frame.size.width : 1.0;
  
  CGPoint titleLabelCenter = self.traitCollection.isRegularWidth ? CGPointMake(titleLabelFrame.origin.x + titleLabelFrame.size.width / 2.0, titleLabelFrame.origin.y + titleLabelFrame.size.height / 2.0) :
  CGPointMake(viewWidth / 2.0, viewHeight / 2.0);
  
  CGRect logoFrame = CGRectMake(CGRectGetMaxX(titleLabelFrame) + viewWidth * 0.02, 0.0, viewWidth * 0.3, viewHeight);
  
  if (!alreadyLoaded) {
    titleLabel.frame = titleLabelFrame;
    logoImageView.frame = logoFrame;
    [self updateLogoImage];
    
    if (self.traitCollection.isRegularWidth) {
      logoImageView.alpha = 1.0;
      titleLabel.text = NSLocalizedString(@"Welcome Hint iPad Title", nil);
    } else {
      logoImageView.alpha = 0.0;
      titleLabel.text = NSLocalizedString(@"Welcome Hint iPod Title", nil);
      titleLabel.textAlignment = NSTextAlignmentCenter;
    }
  } else {
    [UIView animateWithDuration:1.0/3.0 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
      titleLabel.transform = CGAffineTransformScale(titleLabel.transform, scale, scale);
      titleLabel.center = titleLabelCenter;
      
      if (self.traitCollection.isRegularWidth)
      {
        logoImageView.alpha = 1.0;
        logoImageView.frame = logoFrame;
        titleLabel.text = NSLocalizedString(@"Welcome Hint iPad Title", nil);

        [self updateLogoImage];
      } else {
        titleLabel.text = NSLocalizedString(@"Welcome Hint iPod Title", nil);
        logoImageView.alpha = 0.0;
      }
    } completion:nil];
  }
}


- (void) updateLogoImage
{
  UIImage *image = (CGRectGetWidth(logoImageView.frame) * zoomScale <= 128) ? [UIImage imageNamed:@"oblong-logo-white-128.png"] : [UIImage imageNamed:@"oblong-logo-white-256.png"];
  [logoImageView setImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
  logoImageView.tintColor = [MezzanineStyleSheet sharedStyleSheet].mediumFillColor;
}

-(void) setZoomScale:(CGFloat)scale
{
  zoomScale = scale;
  [self updateLogoImage];
}


@end
