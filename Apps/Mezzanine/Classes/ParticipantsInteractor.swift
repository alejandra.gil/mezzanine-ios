//
//  ParticipantsInteractor.swift
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 05/03/2019.
//  Copyright © 2019 Oblong Industries. All rights reserved.
//

import Foundation

protocol ParticipantsInteractorDelegate: AnyObject {
  func participantsInteractorMezzaninesWereUpdated(mezzanines: Array<MZMezzanine>, totalRooms: Int, totalPeople: Int)
}

/// ParticipantsInteractor observes infopresence changes in the systemModel context
/// The idea behind is that the participant roster table view controller and the participant
/// counter view controller are updated (through delegation) using this source of truth and remove code duplication (mezzanines array)

/// The original idea behind this interactor was only support the participant-roster flag on, but since participant
/// counter view controller is common for both flag status, this class has the minimum information
/// to update the the counter view controller for the participant-roster flag off (nonRosterRoomCount and nonRosterPeopleCount Ints)

class ParticipantsInteractor: NSObject {
  fileprivate var kSystemModelContext = 0
  fileprivate let kInfopresenceStatusObservationKeyPath = "infopresence.status"
  fileprivate let kInfopresenceRoomsObservationKeyPath = "infopresence.rooms"

  // Only valid for non-roster
  fileprivate let kInfopresenceRemoteParticipantsObservationKeyPath = "infopresence.remoteParticipants"

  fileprivate var kMezzContext = 1
  fileprivate var kMezzParticipantsObservationKeyPath = "participants"

  // Only valid for non-roster
  var nonRosterRoomCount: Int = 0
  var nonRosterPeopleCount: Int = 0

  weak var delegate: ParticipantsInteractorDelegate?
  var mezzanines = Array<MZMezzanine>()
  var systemModel: MZSystemModel? {
    willSet(aNewModel) {
      if (self.systemModel != nil) && (self.systemModel != aNewModel) {
        endObservingSystemModel()
      }
    }
    didSet {
      if (self.systemModel != nil) {
        beginObservingSystemModel()
      }
    }
  }

  deinit {
    print("ParticipantsInteractor deinit");
    endObservingSystemModel()
    systemModel = nil
  }

  func beginObservingSystemModel() {
    guard let systemModel = systemModel else { return }
    systemModel.addObserver(self, forKeyPath: kInfopresenceStatusObservationKeyPath, options: .new, context: &kSystemModelContext)
    systemModel.addObserver(self, forKeyPath: kInfopresenceRoomsObservationKeyPath, options: [.new, .old], context: &kSystemModelContext)

    // Only valid for non-roster
    self.systemModel?.addObserver(self, forKeyPath: kInfopresenceRemoteParticipantsObservationKeyPath, options: [.new, .old], context: &kSystemModelContext)

    if let mz: MZMezzanine = systemModel.myMezzanine {
      mezzanines.append(mz)
      beginObservingMezz(systemModel.myMezzanine)
    }

    if let rooms = systemModel.infopresence.rooms {
      for case let room as MZMezzanine in rooms {
        if !(room.roomType == .cloud && room.participants.count == 0) {
          mezzanines.append(room)
        }
        beginObservingMezz(room)

        // Only valid for non-roster
        if (room.roomType != .cloud) {
          nonRosterRoomCount += 1
        }
      }
    }

    // Only valid for non-roster
    if let remoteParticipants = systemModel.infopresence.remoteParticipants {
      nonRosterPeopleCount = remoteParticipants.count
    }

    delegate?.participantsInteractorMezzaninesWereUpdated(mezzanines: mezzanines, totalRooms: totalRooms(), totalPeople: totalPeople())
  }

  func endObservingSystemModel() {
    guard let systemModel = systemModel else { return }

    endObservingMezz(systemModel.myMezzanine)

    if let rooms = systemModel.infopresence.rooms {
      for case let room as MZMezzanine in rooms {
        endObservingMezz(room)
      }
      }

    mezzanines.removeAll()

    systemModel.removeObserver(self, forKeyPath: kInfopresenceStatusObservationKeyPath)
    systemModel.removeObserver(self, forKeyPath: kInfopresenceRoomsObservationKeyPath)

    // Only valid for non-roster
    self.systemModel?.removeObserver(self, forKeyPath: kInfopresenceRemoteParticipantsObservationKeyPath)
  }

  func beginObservingMezz(_ mezz: MZMezzanine) {
    mezz.addObserver(self, forKeyPath: kMezzParticipantsObservationKeyPath, options: [.new, .old], context: &kMezzContext)
  }

  func endObservingMezz(_ mezz: MZMezzanine) {
    mezz.removeObserver(self, forKeyPath: kMezzParticipantsObservationKeyPath, context: &kMezzContext)
  }

  override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
    switch (keyPath!, context) {
    case (kInfopresenceStatusObservationKeyPath, .some(&kSystemModelContext)):
      delegate?.participantsInteractorMezzaninesWereUpdated(mezzanines: mezzanines, totalRooms: totalRooms(), totalPeople: totalPeople())

    case (kInfopresenceRoomsObservationKeyPath, .some(&kSystemModelContext)):
      if let inserted = change?[NSKeyValueChangeKey.newKey] as? NSArray {
        for insertedItem in inserted {
          guard let item = insertedItem as? MZMezzanine else { continue }
          if !(item.roomType == .cloud && item.participants.count == 0) {
            mezzanines.append(item)
          }
          beginObservingMezz(item)
        }
      }

      if let deleted = change?[NSKeyValueChangeKey.oldKey] as? NSArray {
        for deletedItem in deleted {
          if deletedItem is MZMezzanine {
            guard let item = deletedItem as? MZMezzanine else { continue }
            mezzanines = mezzanines.filter {$0.uid != item.uid}
            endObservingMezz(item)
          }
        }
      }

      delegate?.participantsInteractorMezzaninesWereUpdated(mezzanines: mezzanines, totalRooms: totalRooms(), totalPeople: totalPeople())

    case (kMezzParticipantsObservationKeyPath, .some(&kMezzContext)):
      let mz: MZMezzanine = object as! MZMezzanine
      if mz.roomType == MZMezzanineRoomType.cloud && mz.participants.count == 0 {
        mezzanines = mezzanines.filter {$0.uid != mz.uid}
      } else if !mezzanines.contains(mz) {
        mezzanines.append(mz)
      }

      delegate?.participantsInteractorMezzaninesWereUpdated(mezzanines: mezzanines, totalRooms: totalRooms(), totalPeople: totalPeople())

    // Only valid for non-roster
    case (kInfopresenceRemoteParticipantsObservationKeyPath, .some(&kSystemModelContext)):
      if let inserted = change?[NSKeyValueChangeKey.newKey] as? NSArray {
        for _ in inserted {
          nonRosterPeopleCount += 1
        }
      }

      if let deleted = change?[NSKeyValueChangeKey.oldKey] as? NSArray {
        for _ in deleted {
          nonRosterPeopleCount -= 1
        }
      }

      delegate?.participantsInteractorMezzaninesWereUpdated(mezzanines: mezzanines, totalRooms: totalRooms(), totalPeople: totalPeople())



    default:
      super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
    }
  }


  // MARK: Helpers

  /// Methods and vars to get the count summary
  /// Depending on the participantRoster flag we will count based on:
  /// mezzanines array (participantRoster on)
  /// nonRosterRoomCount & nonRosterPeopleCount (participantRoster off)

  func totalRooms() -> Int {
    guard let systemModel = systemModel else { return 0 }

    if !systemModel.featureToggles.participantRoster {
      return nonRosterRoomCount
    }
    return mezzanines.count;
  }

  func totalPeople() -> Int {
    guard let systemModel = systemModel else { return 0 }

    if !systemModel.featureToggles.participantRoster {
      return nonRosterPeopleCount
    }

    var count: Int = 0
    for case let mz in mezzanines {
      count += mz.participants.count
    }

    return count
  }
}
