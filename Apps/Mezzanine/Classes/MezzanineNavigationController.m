//
//  MezzanineNavigationController.m
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 22/03/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

#import "MezzanineNavigationController.h"
#import "MezzanineConnectionViewController.h"
#import "AssetViewController.h"
#import "PortfolioViewController.h"
#import "LiveStreamsViewController.h"
#import "PrimaryWindshieldViewController.h"
#import "ExtendedWindshieldViewController.h"
#import "PresentationViewController.h"

#import "Mezzanine-Swift.h"

/* This class provides custom animations if customTransitionContext is set and is valid. If not the system will handle the transition */

@interface MezzanineNavigationController ()

// transitionView allows to keep track if we should perform a custom transition or not
@property (nonatomic, strong) UIView *transitionView;

@end

@implementation MezzanineNavigationController

- (void)viewDidLoad
{
  [super viewDidLoad];
  
  self.delegate = self;
  self.navigationBar.barStyle = UIBarStyleBlack;
  self.interactivePopGestureRecognizer.delegate = self;
}


- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
  return self.visibleViewController.supportedInterfaceOrientations;
}


- (BOOL)shouldAutorotate
{
  return self.visibleViewController.shouldAutorotate;
}


- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation
{
  return UIStatusBarAnimationFade;
}


- (BOOL)prefersStatusBarHidden
{
  return NO;
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
  return UIStatusBarStyleLightContent;
}


#pragma mark - Pop

- (void)popViewControllerWithTransitionView:(UIView *)transitionView
{
  _transitionView = transitionView;
  
  [super popViewControllerAnimated:YES];
}


#pragma mark - Push

- (void)pushViewController:(UIViewController *)viewController transitionView:(UIView *)transitionView;
{
  _transitionView = transitionView;
  
  [super pushViewController:viewController animated:YES];
}


#pragma mark - UINavigationControllerDelegate

- (id <UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController
                                   animationControllerForOperation:(UINavigationControllerOperation)operation
                                                fromViewController:(UIViewController *)fromVC
                                                  toViewController:(UIViewController *)toVC
{
  
  // Local preview
  if (([toVC isKindOfClass:[AssetViewController class]] || [fromVC isKindOfClass:[AssetViewController class]])
      && _transitionView)
  {
    AssetAnimationController *animator = [[AssetAnimationController alloc] initWithTransitionView:_transitionView
                                                                                        operation: operation];
    _transitionView = nil;
    return animator;
  }
  
  // To Workspace. The checks are needed because Sliding panels are childViewControllers.
  if ([toVC isKindOfClass:[MezzanineRootViewController class]] &&
      ([[toVC.childViewControllers lastObject] isKindOfClass:[MezzanineSessionViewController class]] || [toVC.childViewControllers count] == 0))
  {
      FadeAnimationController *animator = [[FadeAnimationController alloc] initWithOperation: operation];
      return animator;
  }
  
  // Connection animations
  if ([toVC isKindOfClass:[RemoteParticipationJoinViewController class]] || [fromVC isKindOfClass:[RemoteParticipationJoinViewController class]])
  {
    ConnectionAnimationController *animator = [[ConnectionAnimationController alloc] initWithOperation: operation];
    return animator;
  }
  
  // To Connection
  if ([toVC isKindOfClass:[MezzanineConnectionViewController class]]) {
    FadeAnimationController *animator = [[FadeAnimationController alloc] initWithOperation: operation];
    return animator;
  }
  
  // Returning nil means the system will handle the transition
  _transitionView = nil;
  return nil;
}


#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
  // ER #15499
  // Listed below, view controllers which do not allow the interactivePopGestureRecognizer to work
  // 1. MezzanineRootViewController and MezzanineConnectionViewController are "root" of their respective navigation flow, so it can't pop
  // 2. AssetViewController is pushed and popped with a custom transition
  // 3. RemoteParticipationJoinViewController since it has a cancel button which disconnects
  
  BOOL enabledInteractivePop = NO;
  if (![self.visibleViewController isKindOfClass:[MezzanineRootViewController class]] &&
      ![self.visibleViewController isKindOfClass:[MezzanineConnectionViewController class]] &&
      ![self.visibleViewController isKindOfClass:[AssetViewController class]] &&
      ![self.visibleViewController isKindOfClass:[RemoteParticipationJoinViewController class]])
    enabledInteractivePop = YES;
  
  return enabledInteractivePop;
}

@end
