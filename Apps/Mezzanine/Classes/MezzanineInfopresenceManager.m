//
//  MezzanineInfopresenceManager.m
//  Mezzanine
//
//  Created by miguel on 10/6/15.
//  Copyright (c) 2015 Oblong Industries. All rights reserved.
//

#import "MezzanineInfopresenceManager.h"
#import "MezzanineInfopresenceManager_Private.h"
#import "InfopresenceOverlayViewController.h"
#import "NSObject+UIAlertController.h"
#import "NSObject+BlockObservation.h"
#import <objc/runtime.h>
#import "Mezzanine-Swift.h"

@implementation MezzanineInfopresenceManager

- (void)reset
{
  [self setCurrentCall:nil];
  infopresenceOverlayViewController.remoteMezz = nil;
  _infopresenceIsBeingHidden = NO;
  infopresenceOverlayViewController.view.alpha = 0.0;
  overlayWindow.hidden = YES;

  if (incomingRequestAlertController)
  {
    [incomingRequestAlertController dismissViewControllerAnimated:YES completion:nil];
    incomingRequestAlertController = nil;
  }

  if (infopresenceInterruptedAlertController)
  {
    [infopresenceInterruptedAlertController dismissViewControllerAnimated:YES completion:nil];
    infopresenceInterruptedAlertController = nil;
  }
}

#pragma mark - Infopresence Observation

- (void)setSystemModel:(MZSystemModel *)systemModel
{
  if (_systemModel != systemModel)
  {
    if (_systemModel)
    {
      if (systemModelObservers)
      {
        for (id token in systemModelObservers)
          [_systemModel removeObserverWithBlockToken:token];

        infopresenceObservers = nil;
      }

      if (infopresenceObservers)
      {
        for (id token in infopresenceObservers)
          [_systemModel.infopresence removeObserverWithBlockToken:token];

        infopresenceObservers = nil;
      }

      infopresenceOverlayViewController.systemModel = nil;
    }

    _systemModel = systemModel;

    if (_systemModel)
    {
      MZInfopresence *infopresence = _systemModel.infopresence;
      infopresenceObservers = [NSMutableArray new];
      id token;
      __weak typeof(self) __self = self;

      // The following is intended to support the case where a client connected to single-feld mezzanine
      // and is subsequently collaborating with a triple-feld system and the design had called for displaying
      // activity on the remote mezz (see bug 6732). Since this 'feature' is no longer part of 2.4, this code path is disabled

      token = [infopresence addObserverForKeyPath:@"outgoingCalls"
                                         options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld)
                                         onQueue:[NSOperationQueue mainQueue]
                                            task:^(id obj, NSDictionary *change) {
                                              [__self processInfopresenceCalls];
                                            }];
      [infopresenceObservers addObject:token];

      token = [infopresence addObserverForKeyPath:@"incomingCalls"
                                         options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld)
                                         onQueue:[NSOperationQueue mainQueue]
                                            task:^(id obj, NSDictionary *change) {
                                              [__self processInfopresenceCalls];
																						}];
      [infopresenceObservers addObject:token];

      token = [infopresence addObserverForKeyPath:@"status"
                                         options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld)
                                         onQueue:[NSOperationQueue mainQueue]
                                            task:^(id obj, NSDictionary *change) {
                                              [__self updateinfopresenceInterruptedAlertController];
                                              ;
                                            }];

      [infopresenceObservers addObject:token];

      // System Model observation
      systemModelObservers = [NSMutableArray new];

      token = [_systemModel addObserverForKeyPath:@"passphraseRequested"
                                          options:0
                                          onQueue:[NSOperationQueue mainQueue]
                                             task:^(id obj, NSDictionary *change) {
                                               // Once in collaboration, we do not need the overlay anymore
                                               if (__self.systemModel.passphraseRequested)
                                               {
                                                 [__self hideInfopresenceOverlayAnimated:YES];
                                                 [__self dismissincomingRequestAlertController];
                                                 [__self setCurrentCall:nil];
                                               }
                                               else if (__self.communicator.isConnected)
                                               {
                                                 MZInfopresence *infopresence = __self.systemModel.infopresence;

                                                 if (infopresence.outgoingCalls.count > 0 && [(MZInfopresenceCall *)infopresence.outgoingCalls.firstObject type] == MZInfopresenceCallTypeJoinRequest)
                                                 {
                                                   [__self setCurrentCall:infopresence.outgoingCalls.firstObject];
                                                   [__self showInfopresenceOverlayJoining];
                                                 }
                                                 else if (infopresence.incomingCalls.count > 0)
                                                 {
                                                   MZInfopresenceCall *firstCall = infopresence.incomingCalls[0];
                                                   if (firstCall.type == MZInfopresenceCallTypeJoinRequest)
                                                   {
                                                     [__self presentincomingRequestAlertController:firstCall];
                                                   }
                                                   else if (firstCall.type == MZInfopresenceCallTypeInvite)
                                                   {
                                                     [__self presentInviteAlert:firstCall];
                                                   }
                                                 }
                                               }
                                             }];
      [systemModelObservers addObject:token];

      infopresenceOverlayViewController.systemModel = self.systemModel;
    }
  }
}


- (void)processInfopresenceCalls
{
  // 1. No outgoings neither incomings -> set currentCall to nil, dismiss/hide any possible thing
  // 2. No outgoings but incomings -> present first incoming call
  // 3. Outgoings but no incomings -> if it's join wait with the overlay, if not present outgoing
  // 4. Outgoing and incomings -> outgoings have priority, hold the incomings until there's no outgoing
  
  if ([self currentCall])
    return;
  
  MZInfopresence *infopresence = _systemModel.infopresence;
  
  if (infopresence.outgoingCalls.count == 0)
  {
    [self hideInfopresenceOverlayAnimated:NO];
    
    if (infopresence.incomingCalls.count == 0)
    {
      [self setCurrentCall:nil];
      [self dismissincomingRequestAlertController];
    }
    else
    {
      [self presentIncomingCallIfNeeded];
    }
  }
  else
  {
    NSPredicate *joinRequestCallsPredicate = [NSPredicate predicateWithFormat:@"type == 1"]; // MZInfopresenceCallTypeJoinRequest
    NSArray *outgoingJoinRequests = [infopresence.outgoingCalls filteredArrayUsingPredicate:joinRequestCallsPredicate];
  
    [self presentOutgoingCallIfNeeded];
    
    if (infopresence.incomingCalls.count > 0 && outgoingJoinRequests.count == 0)
      [self presentIncomingCallIfNeeded];
  }
}


- (void)presentOutgoingCallIfNeeded
{
  MZInfopresenceCall *callToPresent = [_systemModel.infopresence.outgoingCalls firstObject];
 
  if (!callToPresent)
    return;
  
  if (callToPresent.type == MZInfopresenceCallTypeJoinRequest)
  {
    if (!self.systemModel.passphraseRequested)
    {
      [self setCurrentCall:callToPresent];
      [self showInfopresenceOverlayJoining];
    }
  }
}


- (void)presentIncomingCallIfNeeded
{
  MZInfopresenceCall *callToPresent = [_systemModel.infopresence.incomingCalls firstObject];
  
  if (!callToPresent)
    return;
  
  if (![self associatedAlertToInfopresenceCall:callToPresent])
    [self dismissincomingRequestAlertController];
  
  if (callToPresent.type == MZInfopresenceCallTypeJoinRequest)
  {
    [self presentincomingRequestAlertController:callToPresent];
  }
  else if (callToPresent.type == MZInfopresenceCallTypeInvite)
  {
    [self presentInviteAlert:callToPresent];
  }
}


- (void)setCurrentCall:(MZInfopresenceCall *)currentCall
{
  if (_currentCall != currentCall)
  {
    if (_currentCall && currentCallObservers)
    {
      for (id token in currentCallObservers)
        [_currentCall removeObserverWithBlockToken:token];

      [currentCallObservers removeAllObjects];
      currentCallObservers = nil;
    }

    _currentCall = currentCall;

    if (_currentCall)
    {
      currentCallObservers = [NSMutableArray new];
      id token;
      __weak typeof(self) __self = self;

      token = [_currentCall addObserverForKeyPath:@"resolution"
                                          options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld)
                                          onQueue:[NSOperationQueue mainQueue]
                                             task:^(id obj, NSDictionary *change) {

                                               MZInfopresenceCall *call = (MZInfopresenceCall *)obj;
                                               // MZInfopresenceCallResolutionUnknown is only used when the call is not yet resolved
                                               if (call.resolution != MZInfopresenceCallResolutionUnknown)
                                               {
                                                 [__self hideInfopresenceOverlayAnimated:YES];
                                                 [__self setCurrentCall:nil];
                                               }
                                             }];
      [currentCallObservers addObject:token];
    }
  }
}

- (void)setCommunicator:(MZCommunicator *)communicator
{
  if (_communicator != communicator)
  {
    infopresenceOverlayViewController.communicator = communicator;
    _communicator = communicator;
  }
}


#pragma mark - Associated Calls to Alerts

static NSString *kUIAlertControllerKey = @"UIAlertViewKey";
static NSString *kMZInfopresenceCallKey = @"MZInfopresenceCallKey";

- (UIAlertController *)associatedAlertToInfopresenceCall:(MZInfopresenceCall *)call
{
  UIAlertController *alert = (UIAlertController *) objc_getAssociatedObject(call, (__bridge const void *)(kUIAlertControllerKey));
  return alert;
}


- (void)setAssociatedAlert:(UIAlertController *)alertView toCall:(MZInfopresenceCall *)call
{
  objc_setAssociatedObject (call, (__bridge const void *)(kUIAlertControllerKey), alertView, OBJC_ASSOCIATION_ASSIGN);
}


- (MZInfopresenceCall *)associatedInfopresenceCallToAlert:(UIAlertController *)alertController
{
  MZInfopresenceCall *call = (MZInfopresenceCall *) objc_getAssociatedObject(alertController, (__bridge const void *)(kMZInfopresenceCallKey));
  return call;
}


- (void)setAssociatedCall:(MZInfopresenceCall *)call toAlert:(UIAlertController *)alertController
{
  objc_setAssociatedObject (alertController, (__bridge const void *)(kMZInfopresenceCallKey), call, OBJC_ASSOCIATION_ASSIGN);
}


#pragma mark - Alert Management

- (void)presentInviteAlert:(MZInfopresenceCall*)call
{
  if (_systemModel.passphraseRequested)
    return;

  if (!call)
    return;
  if (call.type != MZInfopresenceCallTypeInvite)
    return;
  if ([self associatedAlertToInfopresenceCall:call])
    return;

  MZRemoteMezz *mezzInviting = [_systemModel remoteMezzWithUid:call.uid];

  NSString *title = NSLocalizedString(@"Infopresence Invite Dialog Title", @"Infopresence Invite Dialog Title");
  NSString *cancelButtonTitle = NSLocalizedString(@"Infopresence Invite Dialog Cancel Button Title", @"Infopresence Invite Dialog Cancel Button Title");
  NSString *confirmButtonTitle = NSLocalizedString(@"Infopresence Invite Dialog Confirm Button Title", @"Infopresence Invite Dialog Confirm Button Title");
  NSString *messageFormatString = [NSString stringWithFormat:NSLocalizedString(@"Infopresence Invite Dialog Message", @"Infopresence Invite Dialog Message"), mezzInviting.name];
  if (!_systemModel.currentWorkspace.isSaved && _systemModel.infopresence.status == MZInfopresenceStatusActive)
  {
    messageFormatString = [NSString stringWithFormat:NSLocalizedString(@"Infopresence Invite Discard Workspace And End Current Session Dialog Message", @"Infopresence Invite Discard Workspace And End Current Session Dialog Message"), mezzInviting.name];
  }
  else if (!_systemModel.currentWorkspace.isSaved && _systemModel.infopresence.status != MZInfopresenceStatusActive)
  {
    messageFormatString = [NSString stringWithFormat:NSLocalizedString(@"Infopresence Invite Discard Workspace Dialog Message", @"Infopresence Invite Discard Workspace Dialog Message"), mezzInviting.name];
  }
  else if (_systemModel.currentWorkspace.isSaved && _systemModel.infopresence.status == MZInfopresenceStatusActive)
  {
    messageFormatString = [NSString stringWithFormat:NSLocalizedString(@"Infopresence Invite End Current Session Dialog Message", @"Infopresence Invite End Current Session Dialog Message"), mezzInviting.name];
  }

  UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:messageFormatString preferredStyle:UIAlertControllerStyleAlert];

  __weak typeof(self) __self = self;
  UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:cancelButtonTitle style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    [__self.communicator.requestor requestInfopresenceIncomingInviteResolveRequest:call.uid accept:NO];
    if (![__self.systemModel.infopresence.incomingCalls count])
      incomingRequestAlertController = nil;
  }];
  UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:confirmButtonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    [__self.communicator.requestor requestInfopresenceIncomingInviteResolveRequest:call.uid accept:YES];
    if (![__self.systemModel.infopresence.incomingCalls count])
      incomingRequestAlertController = nil;
  }];

  [alertController addAction:cancelAction];
  [alertController addAction:confirmAction];

  [self setAssociatedAlert:alertController toCall:call];
  [self setAssociatedCall:call toAlert:alertController];
  [self showAlertController:alertController animated:YES completion:nil];
  incomingRequestAlertController = alertController;
}

- (void)presentincomingRequestAlertController:(MZInfopresenceCall*)call
{
  if (_systemModel.passphraseRequested)
    return;

	if (!call)
		return;
  if (call.type != MZInfopresenceCallTypeJoinRequest)
    return;
  if ([self associatedAlertToInfopresenceCall:call])
    return;

  MZRemoteMezz *remoteMezz = [self.systemModel remoteMezzWithUid:call.uid];

	NSString *title = NSLocalizedString(@"Infopresence Request Dialog Title", @"Infopresence Request Dialog Title");
	NSString *messageFormatString = [NSString stringWithFormat:NSLocalizedString(@"Infopresence Request Dialog Message", @"Infopresence Request Dialog Message"), remoteMezz.name];
	NSString *cancelButtonTitle = NSLocalizedString(@"Infopresence Request Dialog Cancel Button Title", @"Infopresence Request Dialog Cancel Button Title");
	NSString *confirmButtonTitle = NSLocalizedString(@"Infopresence Request Dialog Confirm Button Title", @"Infopresence Request Dialog Confirm Button Title");

  UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:messageFormatString preferredStyle:UIAlertControllerStyleAlert];

  __weak typeof(self) __self = self;
  UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:cancelButtonTitle style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    [__self.communicator.requestor requestInfopresenceIncomingResponse:call.uid accept:NO];
    if (![__self.systemModel.infopresence.incomingCalls count])
      incomingRequestAlertController = nil;
  }];
  UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:confirmButtonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    [__self.communicator.requestor requestInfopresenceIncomingResponse:call.uid accept:YES];
    if (![__self.systemModel.infopresence.incomingCalls count])
      incomingRequestAlertController = nil;
  }];

  [alertController addAction:cancelAction];
  [alertController addAction:confirmAction];

  [self setAssociatedAlert:alertController toCall:call];
  [self setAssociatedCall:call toAlert:alertController];
  [self showAlertController:alertController animated:YES completion:nil];
  incomingRequestAlertController = alertController;
}


- (void)dismissincomingRequestAlertController
{
  if (incomingRequestAlertController)
    [incomingRequestAlertController dismissViewControllerAnimated:YES completion:nil];

  MZInfopresenceCall *associatedCall = [self associatedInfopresenceCallToAlert:incomingRequestAlertController];
  [self setAssociatedCall:nil toAlert:incomingRequestAlertController];
  [self setAssociatedAlert:nil toCall:associatedCall];

  if (![_systemModel.infopresence.incomingCalls count])
    incomingRequestAlertController = nil;
}


- (void)updateinfopresenceInterruptedAlertController
{
  if (_systemModel.infopresence.status == MZInfopresenceStatusInterrupted)
  {
    NSString *title = NSLocalizedString(@"Infopresence Interrupted Dialog Title", @"Infopresence Interrupted Dialog Title");
    NSString *message = NSLocalizedString(@"Infopresence Interrupted Dialog Message", @"Infopresence Interrupted Dialog Message");
    NSString *cancelButtonTitle = NSLocalizedString(@"Infopresence Interrupted Dialog Cancel Button Title", @"Infopresence Interrupted Dialog Cancel Button Title");
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];

    __weak typeof(self) __self = self;
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:cancelButtonTitle style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
      [__self.communicator.requestor requestInfopresenceLeaveRequest:YES];
    }];

    [alertController addAction:cancelAction];

    [self showAlertController:alertController animated:YES completion:nil];
    infopresenceInterruptedAlertController = alertController;
  }
  else
  {
    [infopresenceInterruptedAlertController dismissViewControllerAnimated:YES completion:nil];
    infopresenceInterruptedAlertController = nil;
  }
}

#pragma mark - Infopresence Overlay Window

- (void)prepareOverlayWindowUsingMainWindow:(UIWindow*)mainWindow
{
  if (overlayWindow)
  {
    [overlayWindow removeFromSuperview];
    overlayWindow = nil;
  }

  overlayWindow = [[HideableWindow alloc] initWithFrame:mainWindow.frame];
  overlayWindow.windowLevel = UIWindowLevelAlert;
  overlayWindow.hidden = YES;


  infopresenceOverlayViewController = [[InfopresenceOverlayViewController alloc] initWithNibName:nil bundle:nil];
  infopresenceOverlayViewController.communicator = self.communicator;
  infopresenceOverlayViewController.systemModel = self.systemModel;
  infopresenceOverlayViewController.view.alpha = 0.0;

  overlayWindow.rootViewController = infopresenceOverlayViewController;
  [infopresenceOverlayViewController.view setFrame:overlayWindow.window.bounds];
}

- (void)hideWindow:(BOOL)hidden
{
  overlayWindow.hidden = hidden;
}

#pragma mark - Infopresence Overlay Management

-(void) showInfopresenceOverlayJoining
{
  if (_currentCall)
    [self showInfopresenceOverlayJoining:[_systemModel remoteMezzWithUid:_currentCall.uid]];
}

-(void) showInfopresenceOverlayJoining:(MZRemoteMezz*)remoteMezz
{
  [self showInfopresenceOverlayJoining:remoteMezz animateFromRect:CGRectZero];
}

-(void) showInfopresenceOverlayJoining:(MZRemoteMezz*)remoteMezz animateFromRect:(CGRect)animateFromRect
{
  if (_systemModel.passphraseRequested)
    return;

  if (!overlayWindow.hidden)
    return;

  overlayWindow.hidden = NO;

  infopresenceOverlayViewController.remoteMezz = remoteMezz;

  BOOL animateUsingStartRect = !CGRectEqualToRect(animateFromRect, CGRectZero);
  if (animateUsingStartRect)
    infopresenceOverlayViewController.view.alpha = 1.0;  // Start with full alpha if animating from another location, as this cell already exists in a list and we're faking a movement from the list to the center


  void (^animationBlock)() = ^{
    infopresenceOverlayViewController.view.alpha = 1.0;
  };

  [UIView animateWithDuration:kWorkspaceOverlaysAnimationDuration animations:animationBlock completion:nil];

  _infopresenceOverlayStartTime = [NSDate date];
}


-(void) hideInfopresenceOverlayAnimated:(BOOL)animated
{
  if (overlayWindow.hidden || _infopresenceIsBeingHidden)
    return;

  CGFloat minimumAnimationDelay = 1.6;
  CGFloat animationDelay = MAX(1.0, minimumAnimationDelay - [[NSDate date] timeIntervalSinceDate:_infopresenceOverlayStartTime]);

  [self hideInfopresenceOverlayAnimated:animated withDelay:animationDelay];
}

-(void) hideInfopresenceOverlayAnimated:(BOOL)animated withDelay:(CGFloat)animationDelay
{
  if (overlayWindow.hidden || _infopresenceIsBeingHidden)
    return;

  UIView *infopresenceOverlayView = infopresenceOverlayViewController.view;
  void (^animationBlock)() = ^{
    infopresenceOverlayView.alpha = 0.0;
  };
  void (^completionBlock)(BOOL) = ^(BOOL completion){
    infopresenceOverlayViewController.remoteMezz = nil;
    _infopresenceIsBeingHidden = NO;
    overlayWindow.hidden = YES;
  };

  if (animated)
    [UIView animateWithDuration:kWorkspaceOverlaysAnimationDuration delay:animationDelay options:0 animations:animationBlock completion:completionBlock];
  else
  {
    animationBlock();
    completionBlock(false);
  }

  _infopresenceOverlayStartTime = nil;
  _infopresenceIsBeingHidden = YES;
}

@end
