//
//  ExtendedWindshieldViewController.m
//  Mezzanine
//
//  Created by miguel on 2/3/15.
//  Copyright (c) 2015 Oblong Industries. All rights reserved.
//

#import "ExtendedWindshieldViewController.h"
#import "WindshieldViewController_Private.h"
#import "ExtendedWindshieldViewController_Private.h"
#import "MezzanineMainViewController.h"
#import "MezzanineStyleSheet.h"
#import "ImageAssetView.h"
#import "VideoAssetView.h"
#import "CoreGraphicsAdditions.h"
#import "NSObject+BlockObservation.h"
#import "UIImage+Resizing.h"
#import "NSString+VersionChecking.h"
#import "Mezzanine-Swift.h"


static CGFloat kExtendedWindshieldAppearAnimationDuration = 0.33;
static CGFloat marginsToAvoidMainAreaFloatingButtons = 60.0;


@implementation ExtendedWindshieldViewController

@dynamic systemModel;

#pragma mark - View lifecycle

- (void)viewDidLoad
{
  [super viewDidLoad];

  feldBoundViewsContainer = [[UIView alloc] initWithFrame:self.view.bounds];
  feldBoundViewsContainer.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
  feldBoundViewsContainer.userInteractionEnabled = NO;
  feldBoundViewsContainer.layer.zPosition = -1.0;
  [self.view addSubview:feldBoundViewsContainer];

  separatorLineViews = [NSMutableArray new];

  [self addFeldBoundsAndLineSeparators];
  [self refreshLayout];
}


- (void) addFeldBoundsAndLineSeparators
{
  [[feldBoundViewsContainer subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];

  [separatorLineViews makeObjectsPerformSelector:@selector(removeFromSuperview)];
  [separatorLineViews removeAllObjects];

  UIColor *feldBoundsColor = [UIColor colorWithWhite:0.33 alpha:1.0];
  for (MZSurface *surface in self.systemModel.surfacesInExtendedWindshield)
  {
    SurfaceView *feldBoundView = [[SurfaceView alloc] initWithFrame:CGRectZero];
    feldBoundView.surface = surface;
    feldBoundView.backgroundColor = [MezzanineStyleSheet sharedStyleSheet].grey60Color;
    feldBoundView.layer.borderColor = feldBoundsColor.CGColor;
    feldBoundView.layer.borderWidth = [self borderWidth];
    feldBoundView.userInteractionEnabled = NO;
    [feldBoundViewsContainer addSubview:feldBoundView];
  }

  NSInteger surfacesInExtendedWindshield = self.systemModel.surfacesInExtendedWindshield.count;
  for (NSInteger i = 0 ; i < surfacesInExtendedWindshield - 1 ; i++)
  {
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectZero];
    separatorView.backgroundColor = feldBoundsColor;
    [separatorLineViews addObject:separatorView];
    [self.view addSubview:separatorView];
  }
}


#pragma mark - Layout

-(void) refreshLayout
{
  DLog(@"ExtendedWindshieldViewController refreshLayout");

  [self updateSurfacesLayout];

  NSArray *items = [self.systemModel.currentWorkspace.windshield itemsInExtendedWindshield];
  [self refreshItemsLayout:items];
  for (MZWindshieldItem *item in items)
    [self updateCropViewForItem:item];
}


-(void) updateCropViewForItem:(MZWindshieldItem *)item
{
  CGRect visibleRect = [[self surfaceViewForSurface:item.surface] visibleRect];
  AssetView *assetView = [self viewForItem:item];
  [self cropView:assetView rect:visibleRect];
}


-(void) updateSurfaceSizeAndMargins
{
  NSInteger numberOfSurfaces = self.systemModel.surfacesInExtendedWindshield.count;

  if (numberOfSurfaces == 0)
    return;

  CGFloat marginToBorders = 20.0;
  CGFloat corkPageWidth = self.view.frame.size.width / numberOfSurfaces;

  NSInteger surfaceCumXOffset = 0;
  CGRect contentViewFrame = CGRectMake(0, 0, (NSInteger) corkPageWidth - 2.0 * marginToBorders, (NSInteger)self.view.frame.size.height - 2.0 * marginsToAvoidMainAreaFloatingButtons);

  for (SurfaceView *view in feldBoundViewsContainer.subviews)
  {
    // Get surface aspect ratio
    MZSurface *surface =  view.surface;
    MZFeld *surfaceBoundingFeld = surface.boundingFeld;
    MZVect *surfacePhysicalSize = surfaceBoundingFeld.physicalSize;
    CGRect surfaceRect = CGRectMake(0, 0, surfacePhysicalSize.x.doubleValue, surfacePhysicalSize.y.doubleValue);
    CGRect surfaceViewFrame = ProportionalResizeIntoAndCenter(surfaceRect, contentViewFrame);

    view.surfaceSize = CGSizeMake((NSInteger)surfaceViewFrame.size.width, (NSInteger)surfaceViewFrame.size.height);
    view.surfaceOffset = CGPointMake((NSInteger)surfaceCumXOffset + (corkPageWidth - surfaceViewFrame.size.width) / 2.0,
                                     (NSInteger)(self.view.frame.size.height - surfaceViewFrame.size.height)/2.0);
    surfaceCumXOffset += corkPageWidth;
  }
}


-(void) updateSurfacesLayout
{
  NSInteger numberOfSurfaces = self.systemModel.surfacesInExtendedWindshield.count;

  if (numberOfSurfaces == 0)
    return;

  [self updateSurfaceSizeAndMargins];

  // Set the right frame to each surface
  for (SurfaceView *surfaceView in feldBoundViewsContainer.subviews)
  {
    surfaceView.frame = CGRectMake((NSInteger)surfaceView.surfaceOffset.x,
                                   (NSInteger)surfaceView.surfaceOffset.y,
                                   (NSInteger)surfaceView.surfaceSize.width,
                                   (NSInteger)surfaceView.surfaceSize.height);
  }

  for (NSInteger i = 0 ; i < separatorLineViews.count ; i++)
  {
    UIView *separator = separatorLineViews[i];
    CGPoint previousFeldCenter = [(UIView *)feldBoundViewsContainer.subviews[i] center];
    CGPoint nextFeldCenter = [(UIView *)feldBoundViewsContainer.subviews[i+1] center];

    CGFloat separatorVerticalMargin = 55.0;

    [separator setFrame:CGRectMake((previousFeldCenter.x + nextFeldCenter.x)/2.0,
                                   separatorVerticalMargin,
                                   [self borderWidth],
                                   self.view.frame.size.height - 2.0 * separatorVerticalMargin)];
    
  }

  for (MZSurface *surface in self.systemModel.surfacesInExtendedWindshield)
  {
    SurfaceView *surfaceView = [self surfaceViewForSurface:surface];
    surfaceView.visibleRect = [self rectAreaOfSurface:surface];
  }
}


- (void)cropView:(UIView *)itemView rect:(CGRect)rect
{
  // Create a mask layer and the frame to determine what will be visible in the view.
  CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];

  CGFloat x = CGRectGetMinX(itemView.frame) < 0 ? fabs(CGRectGetMinX(itemView.frame)) + CGRectGetMinX(rect) : MAX(0, CGRectGetMinX(rect) - CGRectGetMinX(itemView.frame));
  CGFloat y = 0;
  CGFloat w = MIN(CGRectGetMaxX(itemView.frame),CGRectGetMaxX(rect)) - MAX(MAX(CGRectGetMinX(rect), CGRectGetMinX(itemView.frame)),x);
  w += CGRectGetMinX(itemView.frame) < 0 ? fabs(CGRectGetMinX(itemView.frame)) : 0;
  CGFloat h = itemView.frame.size.height;

  CGRect maskRect = CGRectMake(x,y,w,h);

  // If no mask is needed, don't set it and remove any previous one.
  if (CGRectEqualToRect(itemView.frame, maskRect))
  {
    itemView.layer.mask = nil;
  }
  else
  {
    CGPathRef path = CGPathCreateWithRect(maskRect, NULL);
    maskLayer.path = path;
    CGPathRelease(path);
    itemView.layer.mask = maskLayer;
  }
}


-(CGRect) rectAreaOfSurface:(MZSurface *)surface
{
  NSInteger surfaceIx = [self.systemModel.surfacesInExtendedWindshield indexOfObject:surface];
  CGRect frame = CGRectZero;
  frame.origin.x = surfaceIx > 0 ? CGRectGetMaxX([separatorLineViews[surfaceIx-1] frame]) : 0;
  frame.size.width = ((surfaceIx < separatorLineViews.count) ? CGRectGetMinX([separatorLineViews[surfaceIx] frame]) : CGRectGetMaxX(self.view.frame)) - frame.origin.x;
  frame.size.height = CGRectGetHeight(self.view.frame);
  return frame;
}


- (void)setBorderHighlighted:(BOOL)highlighted forFeld:(UIView *)feldBoundView
{
  if (highlighted)
  {
    feldBoundView.layer.borderColor = [MezzanineStyleSheet sharedStyleSheet].ovumHoverHighlightColor.CGColor;
    feldBoundView.layer.borderWidth = [self borderWidth]*2.0;
  }
  else
  {
    feldBoundView.layer.borderColor = [UIColor colorWithWhite:0.5 alpha:0.5].CGColor;
    feldBoundView.layer.borderWidth = [self borderWidth];
  }
}


- (void) updateFeldBorderWithLocation:(CGPoint)location
{
  for (UIView *feldBound in feldBoundViewsContainer.subviews)
  {
    if (CGRectContainsPoint(feldBound.frame, location))
      [self setBorderHighlighted:YES forFeld:feldBound];
    else
      [self setBorderHighlighted:NO forFeld:feldBound];
  }
}


-(CGFloat) borderWidth
{
  return 1.0 / [UIScreen mainScreen].scale;  // Exactly one pixel at every zoom scale
}

- (CGFloat) zoomScaleOfWindshield
{
  return 1.0;
}

#pragma mark - Model Observation

-(void) setSystemModel:(MZSystemModel *)aModel
{
  if (_systemModel != aModel)
  {
    if (systemModelObservers && _systemModel)
    {
      for (id token in systemModelObservers)
        [_systemModel removeObserverWithBlockToken:token];

      systemModelObservers = nil;
    }

    _systemModel = aModel;

    if (_systemModel)
    {
      systemModelObservers = [[NSMutableArray alloc] init];
      id token;
      __weak typeof(self) __self = self;

      token = [_systemModel addObserverForKeyPath:@"surfaces"
                                         options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld)
                                         onQueue:[NSOperationQueue mainQueue]
                                            task:^(id obj, NSDictionary *change) {

                                              [__self addFeldBoundsAndLineSeparators];
                                              [__self refreshLayout];

                                            }];
      [systemModelObservers addObject:token];
      
    }
  }
}


-(void) loadAllWindshieldItems
{
  [self removeAllItemViews];
  [self.windshield.itemsInExtendedWindshield enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
    [self insertViewForItem:obj];
  }];
}


#pragma mark - Presentation and Dismissal

-(void) presentExtendedWindshieldWithCompletion:(void (^)())completionBlock
{
  isPresenting = YES;
  
  self.view.transform = CGAffineTransformMakeScale(1.4, 1.4);
  self.view.alpha = 0.0;
  
  [UIView animateWithDuration:kExtendedWindshieldAppearAnimationDuration
                        delay:0.0
                      options:UIViewAnimationOptionCurveEaseInOut
                   animations:^{
                     self.view.transform = CGAffineTransformMakeScale(1.0, 1.0);
                     self.view.alpha = 1.0;
                   }
                   completion:^(BOOL finished) {
                     isPresenting = NO;
                     if (completionBlock) {
                       completionBlock ();
                     }
                   }];
  
  // Analytics
  MezzanineAnalyticsManager *analyticsManager = [MezzanineAnalyticsManager sharedInstance];
  [analyticsManager tagScreen:analyticsManager.extendedWindshieldScreen];
}


-(void) hideExtendedWindshield
{
  isPresenting = YES;
  
  [UIView animateWithDuration:kExtendedWindshieldAppearAnimationDuration
                        delay:0.0
                      options:UIViewAnimationOptionCurveEaseInOut
                   animations:^{
                     self.view.transform = CGAffineTransformMakeScale(1.4, 1.4);
                     self.view.alpha = 0.0;
                   }
                   completion:^(BOOL finished) {
                     isPresenting = NO;
                     self.view.transform = CGAffineTransformMakeScale(1.0, 1.0);
                     
                     // Analytics
                     [[MezzanineAnalyticsManager sharedInstance] tagWorkspaceStatusScreen];
                   }];
}


#pragma mark - OBDropZone

-(void) ovumDropped:(OBOvum*)ovum inView:(UIView*)view atLocation:(CGPoint)location
{
  [self updateFeldBorderWithLocation:CGPointZero];
  [super ovumDropped:ovum inView:view atLocation:location];
}


-(OBDropAction) ovumEntered:(OBOvum*)ovum inView:(UIView*)view atLocation:(CGPoint)location
{
  [self updateFeldBorderWithLocation:location];
  return [super ovumEntered:ovum inView:view atLocation:location];
}


-(void) ovumExited:(OBOvum*)ovum inView:(UIView*)view atLocation:(CGPoint)location
{
  [self updateFeldBorderWithLocation:location];
  [super ovumExited:ovum inView:view atLocation:location];
}


-(OBDropAction) ovumMoved:(OBOvum*)ovum inView:(UIView*)view atLocation:(CGPoint)location
{
  [self updateFeldBorderWithLocation:location];
  return [super ovumMoved:ovum inView:view atLocation:location];
}


#pragma mark -
#pragma mark Coordinates transforms

-(SurfaceView *) surfaceViewForSurface:(MZSurface *)surface
{
  NSArray *filtered = [feldBoundViewsContainer.subviews filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"surface == %@", surface]];
  if (filtered.count)
    return [filtered firstObject];
  else
    return nil;
}

-(CGFloat) surface:(MZSurface *)surface feldHeight:(MZFeld *)feld
{
  MZVect *feldPhysicalSize = feld.physicalSize;
  MZVect *surfacePhysicalSize = surface.boundingFeld.physicalSize;
  CGFloat proportion = feldPhysicalSize.y.doubleValue / surfacePhysicalSize.y.doubleValue;
  SurfaceView *surfaceView = [self surfaceViewForSurface:surface];
  return surfaceView.surfaceSize.height * proportion;
}


-(CGFloat) surface:(MZSurface *)surface feldWidth:(MZFeld *)feld
{
  MZVect *feldPhysicalSize = feld.physicalSize;
  MZVect *surfacePhysicalSize = surface.boundingFeld.physicalSize;
  CGFloat proportion = feldPhysicalSize.x.doubleValue / surfacePhysicalSize.x.doubleValue;

  SurfaceView *surfaceView = [self surfaceViewForSurface:surface];
  return surfaceView.surfaceSize.width * proportion;
}

-(CGFloat) surface:(MZSurface *)surface feldXOffset:(MZFeld *)feld
{
  CGFloat surfacePhysicalWidth = surface.boundingFeld.physicalSize.x.doubleValue;
  CGFloat feldPhysicalWidth = feld.physicalSize.x.doubleValue;
  CGFloat boundingFeldOver = [surface.boundingFeld.center doubleVectDot:surface.boundingFeld.over];
  CGFloat feldOver = [feld.center doubleVectDot:feld.over];
  CGFloat dist = fabs((boundingFeldOver - surfacePhysicalWidth / 2.0) - (feldOver - feldPhysicalWidth / 2.0));
  SurfaceView *surfaceView = [self surfaceViewForSurface:surface];
  CGFloat pixelsPerMM = surfaceView.frame.size.width / surfacePhysicalWidth;
  return dist * pixelsPerMM;
}

-(CGFloat) surface:(MZSurface *)surface feldYOffset:(MZFeld *)feld
{ // Vertical coordinates in native are grow bottom to top, iOS is top to bottom.
  CGFloat surfacePhysicalHeight = surface.boundingFeld.physicalSize.y.doubleValue;
  CGFloat feldPhysicalHeight = feld.physicalSize.y.doubleValue;
  CGFloat boundingFeldUp = [surface.boundingFeld.center doubleVectDot:surface.boundingFeld.up];
  CGFloat feldUp = [feld.center doubleVectDot:feld.up];
  CGFloat dist = (boundingFeldUp + surfacePhysicalHeight / 2.0) - (feldUp + feldPhysicalHeight / 2.0);
  SurfaceView *surfaceView = [self surfaceViewForSurface:surface];
  CGFloat pixelsPerMM = surfaceView.frame.size.height / surfacePhysicalHeight;
  return dist * pixelsPerMM;
}


-(CGRect) convertFrameFromRelativeCoordinatesToWindshieldViewCoordinates:(CGRect)frameInRelativeCoordinates inFeld:(MZFeld *)feld inSurface:(MZSurface *)surface
{
  CGRect rect = CGRectMake([self surface:surface feldXOffset:feld] + ((frameInRelativeCoordinates.origin.x - frameInRelativeCoordinates.size.width / 2.0) + 0.5) * [self surface:surface feldWidth:feld],
                    [self surface:surface feldYOffset:feld] + ((1 - (frameInRelativeCoordinates.origin.y + 0.5)) - frameInRelativeCoordinates.size.height / 2.0) * [self surface:surface feldHeight:feld],
                           frameInRelativeCoordinates.size.width * [self surface:surface feldWidth:feld],
                           frameInRelativeCoordinates.size.height * [self surface:surface feldHeight:feld]);

  return rect;
}


- (CGRect)convertFrameFromWindshieldViewCoordinatesToRelativeCoordinates:(CGRect)frameInWindshieldViewCoordinates inFeld:(MZFeld *)feld inSurface:(MZSurface *)surface
{
  return CGRectMake(-0.5 + (frameInWindshieldViewCoordinates.origin.x - [self surface:surface feldXOffset:feld]) / [self surface:surface feldWidth:feld] + frameInWindshieldViewCoordinates.size.width / [self surface:surface feldWidth:feld] / 2.0,
                    0.5 - ((frameInWindshieldViewCoordinates.origin.y - [self surface:surface feldYOffset:feld]) / [self surface:surface feldHeight:feld]) - frameInWindshieldViewCoordinates.size.height / [self surface:surface feldHeight:feld] / 2.0,
                    frameInWindshieldViewCoordinates.size.width / [self surface:surface feldWidth:feld],
                    frameInWindshieldViewCoordinates.size.height / [self surface:surface feldHeight:feld]);
}


- (CGPoint)shiftPointFromOrigin:(CGPoint)pointInOrigin toFeld:(MZFeld *)feld inSurface:(MZSurface *)surface
{
  SurfaceView *surfaceView = [self surfaceViewForSurface:surface];
  return CGPointMake(pointInOrigin.x + surfaceView.frame.origin.x,
                     pointInOrigin.y + surfaceView.frame.origin.y);
}


- (CGPoint)shiftPointToOrigin:(CGPoint)pointInOrigin fromFeld:(MZFeld *)feld inSurface:(MZSurface *)surface
{
  SurfaceView *surfaceView = [self surfaceViewForSurface:surface];
  return CGPointMake(pointInOrigin.x - surfaceView.frame.origin.x,
                     pointInOrigin.y - surfaceView.frame.origin.y);
}


-(CGRect) rectFromNativeWindshieldCoordinatesForItem:(MZWindshieldItem *)item
{
  return [self rectFromNativeWindshieldCoordinates:item.frame inSurface:item.surface inFeld:item.feld];
}


-(CGRect) rectFromNativeWindshieldCoordinates:(CGRect)frame inSurface:(MZSurface *)surface inFeld:(MZFeld *)feld
{
  CGRect absoluteRect = [self convertFrameFromRelativeCoordinatesToWindshieldViewCoordinates:frame inFeld:(MZFeld *)feld inSurface:surface];
  absoluteRect.origin = [self shiftPointFromOrigin:absoluteRect.origin toFeld:feld inSurface:surface];
  return absoluteRect;
}


-(CGRect) rectToNativeWindshieldCoordinates:(CGRect)frame inFeld:(MZFeld *)feld
{
  MZSurface *surface = [self.systemModel surfaceWithFeld:feld];
  frame.origin = [self shiftPointToOrigin:frame.origin fromFeld:feld inSurface:surface];
  frame = [self convertFrameFromWindshieldViewCoordinatesToRelativeCoordinates:frame inFeld:feld inSurface:surface];
  return frame;
}


-(MZFeld *)closestFeldAtLocation:(CGPoint)location
{
  // Get the surface area between separators and borders for the current location
  NSInteger surfaceAreaIx = 0;
  for (UIView *separator in separatorLineViews)
  {
    if (location.x <= separator.frame.origin.x)
    {
      break;
    }
    else
    {
      surfaceAreaIx++;
    }
  }

  SurfaceView *selectedSurfaceView = feldBoundViewsContainer.subviews[surfaceAreaIx];
  MZSurface *surface = selectedSurfaceView.surface;

  CGFloat minDist = CGFLOAT_MAX;
  MZFeld *closestFeld = nil;

  for (MZFeld *feld in surface.felds)
  {
    CGRect feldRect = CGRectMake(selectedSurfaceView.surfaceOffset.x + [self surface:surface feldXOffset:feld],
                                 selectedSurfaceView.surfaceOffset.y + [self surface:surface feldYOffset:feld],
                                 [self surface:surface feldWidth:feld], [self surface:surface feldHeight:feld]);
    if (CGRectContainsPoint(feldRect, location))
      return feld;

    CGPoint cent = CGPointMake(CGRectGetMidX(feldRect), CGRectGetMidY(feldRect));
    CGFloat dist = CGPointDistance(location, cent);
    if (dist < minDist)
    {
      minDist = dist;
      closestFeld = feld;
    }
  }
  return closestFeld;
}

@end
