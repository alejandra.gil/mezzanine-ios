//
//  MezzanineAppDelegate_Private.h
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 29/09/2016.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

#import "MezzanineAppDelegate.h"

@interface MezzanineAppDelegate ()

- (BOOL)connectToPool:(NSString *)poolName;
- (BOOL)attempToConnectFromURL:(NSURL *)url;

@end
