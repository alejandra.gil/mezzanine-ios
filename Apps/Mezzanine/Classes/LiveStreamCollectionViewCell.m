//
//  LiveStreamCollectionViewCell.m
//  Mezzanine
//
//  Created by Zai Chang on 3/20/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import "LiveStreamCollectionViewCell.h"
#import "LiveStreamCollectionViewCell_Private.h"
#import "MezzanineStyleSheet.h"
#import "MZDownloadManager.h"
#import "MZLiveStream.h"
#import "Mezzanine-Swift.h"

@implementation LiveStreamCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
      
      MezzanineStyleSheet *styleSheet = [MezzanineStyleSheet sharedStyleSheet];
      if (![SettingsFeatureToggles sharedInstance].teamworkUI)
      {
        self.layer.borderColor = styleSheet.mediumFillColor.CGColor;
        self.layer.borderWidth = 1.0 / [UIScreen mainScreen].scale;
      }
      self.backgroundColor = styleSheet.darkestFillColor;

      _videoPlaceholderView = [VideoPlaceholderView new];
      _videoPlaceholderView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
      _videoPlaceholderView.backgroundColor = [MezzanineStyleSheet sharedStyleSheet].videoPlaceholderBackgroundColor;
      _videoPlaceholderView.frame = CGRectZero;
      _videoPlaceholderView.minTitleFontSize = 12.0;
      _videoPlaceholderView.minSubtitleFontSize = 12.0;
      [self.contentView addSubview:_videoPlaceholderView];

      // Add image view last so they cover over the labels
      _imageView = [[UIImageView alloc] initWithFrame:frame];
      _imageView.contentMode = UIViewContentModeScaleAspectFit;
      [self.contentView addSubview:_imageView];

      self.accessibilityIdentifier = @"LiveStreamsView.CollectionViewCell";
    }
    return self;
}


- (void)dealloc
{
  self.liveStream = nil;
}


- (void)layoutSubviews
{
  [super layoutSubviews];
  
  CGRect bounds = self.bounds;
  _imageView.frame = bounds;

  _videoPlaceholderView.frame = bounds;
}


- (void)prepareForReuse
{
  self.imageView.image = nil;
  self.liveStream = nil;
}


- (void)setLiveStream:(MZLiveStream *)liveStream
{
  if (_liveStream != liveStream)
  {
    if (_liveStream)
    {
      [_liveStream removeObserver:self forKeyPath:@"displayName"];
      [_liveStream removeObserver:self forKeyPath:@"active"];
      [_liveStream removeObserver:self forKeyPath:@"thumbURL"];

      [[MZDownloadManager sharedLoader] cancelLoadingURLForTarget:_imageView];
    }

    _liveStream = liveStream;

    if (_liveStream)
    {
      [_liveStream addObserver:self forKeyPath:@"displayName" options:(NSKeyValueObservingOptionInitial) context:nil];
      [_liveStream addObserver:self forKeyPath:@"active" options:0 context:nil];
      [_liveStream addObserver:self forKeyPath:@"thumbURL" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:nil];

      [self updateThumbnail];
    }
  }
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
  if ([keyPath isEqualToString:@"displayName"])
  {
    _videoPlaceholderView.sourceName = _liveStream.displayName;
    _videoPlaceholderView.sourceOrigin = _liveStream.remoteMezzName;
  }
  else if ([keyPath isEqualToString:@"active"])
  {
    [self updateThumbnail];
  }
  else if ([keyPath isEqualToString:@"thumbURL"] || [keyPath isEqualToString:@"active"])
  {
    if (![change[NSKeyValueChangeOldKey] isEqual:[NSNull null]])
    {
      NSString *oldURL = change[NSKeyValueChangeOldKey];
      [self unloadOldThumbnailURL:oldURL];
    }

    [self updateThumbnail];
  }
}


- (void)unloadOldThumbnailURL:(NSString *)oldURL
{
  NSURL *url = [[MezzanineAppContext currentContext].currentCommunicator urlForResource:oldURL];
  if (url == nil)
    return;

  [[MZDownloadManager sharedLoader] unloadImageFromMemoryCacheWithURL:url];
  [[MZDownloadManager sharedLoader] cancelLoadingURLForTarget:_imageView];
}

- (void)updateThumbnail
{
  if (_liveStream.active && _liveStream.thumbURL)
  {
    NSURL *url = [[MezzanineAppContext currentContext].currentCommunicator urlForResource:_liveStream.thumbURL];
    if (url == nil)
      return;

    [[MZDownloadManager sharedLoader] loadImageWithURL:url
                                              atTarget:_imageView
                                               success:^(UIImage *image) {
                                                 if (_liveStream.thumbnail != image)
                                                   _liveStream.thumbnail = image;

                                                 _imageView.image = image;
                                                 _videoPlaceholderView.hidden = YES;
                                               }
                                                 error:nil
                                              canceled:nil
                                         cachingOption:MZDownloadCacheTypeOnlyInMemory];
  }
  else
  {
    _imageView.image = nil;
    _videoPlaceholderView.hidden = NO;
  }
}


@end
