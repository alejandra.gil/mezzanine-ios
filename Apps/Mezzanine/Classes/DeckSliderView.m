//
//  DeckSliderView.m
//  Mezzanine
//
//  Created by Miguel Sánchez Valdés on 4/5/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import "DeckSliderView.h"
#import "MezzanineStyleSheet.h"
#import "NSString+VersionChecking.h"
#import "OBDirectionalPanGestureRecognizer.h"
#import "Three20Style.h"
#import "Mezzanine-Swift.h"

@interface  DeckSliderView (Private) 

- (CGFloat) normalizedPosition;

@end


@implementation DeckSliderView

@synthesize delegate;
@synthesize trackHeight;
@synthesize minThumbWidth;
@synthesize pushbacked;
@synthesize isTracking;
@synthesize showDeckStateLabels;


-(TTStyle*) smallThumbStyle
{
  TTShape *shape = [TTRoundedRectangleShape shapeWithRadius:3.0];
	return [TTInsetStyle styleWithInset:UIEdgeInsetsMake(3.0, 3.0, 3.0, 3.0) next:
          [TTShapeStyle styleWithShape:shape next:
           [TTSolidFillStyle styleWithColor:[UIColor colorWithWhite:1.0 alpha:1.0] next:
            nil]]];
}


-(TTStyle*) trackDarkStyle
{
  TTShape *shape = [TTRoundedRectangleShape shapeWithRadius:5.0];
	return [TTShapeStyle styleWithShape:shape next:
          [TTSolidBorderStyle styleWithColor:[UIColor clearColor] width:1.0 next:
           [TTSolidFillStyle styleWithColor:[UIColor clearColor] next: nil]]];
}


-(TTStyle*) thumbDarkStyle
{
  TTShape *shape = [TTRoundedRectangleShape shapeWithRadius:5.0];
	return [TTInsetStyle styleWithInset:UIEdgeInsetsMake(2.0, 2.0, 2.0, 2.0) next:
          [TTShapeStyle styleWithShape:shape next:
           [TTSolidFillStyle styleWithColor:[UIColor colorWithWhite:0.38 alpha:1.0] next:
            nil]]];
}

-(TTStyle*) thumbDottedBorderStyle
{
  TTShape *shape = [TTRoundedRectangleShape shapeWithRadius:5.0];
	return [TTInsetStyle styleWithInset:UIEdgeInsetsMake(1.0, 1.0, 1.0, 1.0) next:
          [TTShapeStyle styleWithShape:shape next:
           [TTSolidBorderStyle styleWithColor:[UIColor colorWithWhite:0.24 alpha:1.0] width:2.0 next:
            [TTSolidFillStyle styleWithColor:[UIColor colorWithWhite:0.36 alpha:1.0] next:
             nil]]]];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
  self = [super initWithCoder:aDecoder];
  if (self)
  {
    [self initialize];
  }
  return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
      [self initialize];
    }
    return self;
}

- (void)initialize
{
  MezzanineStyleSheet *styleSheet = [MezzanineStyleSheet sharedStyleSheet];

  trackHeight = 20.0;
  minThumbWidth = 32.0;

  self.backgroundColor = [UIColor clearColor];

  CGSize boundsSize = self.bounds.size;
  CGFloat trackMarginY = (NSInteger)((boundsSize.height - trackHeight) / 2);
  CGRect trackFrame = CGRectMake(0.0, trackMarginY, boundsSize.width, trackHeight);
  trackView = [[TTView alloc] initWithFrame:trackFrame];
  trackView.backgroundColor = [UIColor clearColor];
  trackView.style = [styleSheet sliderTrackStyle:UIControlStateNormal];
  trackView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
  [self addSubview:trackView];

  thumbView = [[TTView alloc] initWithFrame:CGRectMake(0, 0, minThumbWidth, trackFrame.size.height)];
  thumbView.backgroundColor = [UIColor clearColor];
  thumbView.style = [styleSheet sliderThumbStyle:UIControlStateNormal];
  [trackView addSubview:thumbView];

  slideIndexLabel = [[UILabel alloc] initWithFrame:CGRectZero];
  slideIndexLabel.font = [UIFont dinBoldOfSize:12.0];
  slideIndexLabel.backgroundColor = [UIColor clearColor];
  slideIndexLabel.textAlignment = NSTextAlignmentCenter;
  slideIndexLabel.textColor = [UIColor whiteColor];
  [thumbView addSubview:slideIndexLabel];

  firstSlideLabel = [[UILabel alloc] initWithFrame:CGRectZero];
  firstSlideLabel.font = [UIFont dinBoldOfSize:12.0];
  firstSlideLabel.backgroundColor = [UIColor clearColor];
  firstSlideLabel.textAlignment = NSTextAlignmentLeft;
  firstSlideLabel.textColor = [[MezzanineStyleSheet sharedStyleSheet] textSecondaryColor];
  firstSlideLabel.text = @"1";
  [trackView insertSubview:firstSlideLabel belowSubview:thumbView];

  lastSlideLabel = [[UILabel alloc] initWithFrame:CGRectZero];
  lastSlideLabel.font = [UIFont dinBoldOfSize:12.0];
  lastSlideLabel.backgroundColor = [UIColor clearColor];
  lastSlideLabel.textAlignment = NSTextAlignmentRight;
  lastSlideLabel.textColor = [[MezzanineStyleSheet sharedStyleSheet] textSecondaryColor];
  [trackView insertSubview:lastSlideLabel belowSubview:thumbView];

  noSlidesLabel = [[UILabel alloc] initWithFrame:CGRectZero];
  noSlidesLabel.font = [UIFont dinBoldOfSize:12.0];
  noSlidesLabel.backgroundColor = [UIColor clearColor];
  noSlidesLabel.textAlignment = NSTextAlignmentCenter;
  noSlidesLabel.textColor = [[MezzanineStyleSheet sharedStyleSheet] textSecondaryColor];
  noSlidesLabel.text = NSLocalizedString(@"No Slides Text", nil);
  noSlidesLabel.hidden = YES;
  [trackView insertSubview:noSlidesLabel belowSubview:thumbView];

  OBDirectionalPanGestureRecognizer *horizontalPanRecognizer = [[OBDirectionalPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleSliderPanGesture:)];
  horizontalPanRecognizer.direction = OBDirectionalPanHorizontal;
  horizontalPanRecognizer.minimumNumberOfTouches = 1;
  horizontalPanRecognizer.maximumNumberOfTouches = 1;
  [self addGestureRecognizer:horizontalPanRecognizer];

  showDeckStateLabels = YES;
  isTracking = NO;

  self.accessibilityIdentifier = @"DeckSliderView";
  thumbView.accessibilityIdentifier = @"DeckSliderView.ThumbView";
  slideIndexLabel.accessibilityIdentifier = @"DeckSliderView.SlideIndexLabel";
  firstSlideLabel.accessibilityIdentifier = @"DeckSliderView.FirstSlideLabel";
  lastSlideLabel.accessibilityIdentifier = @"DeckSliderView.LastIndexLabel";
}


-(void) layoutSlideIndexLabel
{
  slideIndexLabel.frame = CGRectInset(thumbView.bounds, 4.0, 0.0);
}


-(void) layoutSubviews
{
  [super layoutSubviews];

  CGSize boundsSize = self.bounds.size;
  CGFloat trackMarginY = (NSInteger)((boundsSize.height - trackHeight) / 2);
  CGRect trackFrame = CGRectMake(0.0, trackMarginY, boundsSize.width, trackHeight);
  trackView.frame = trackFrame;
  
  CGRect thumbFrame = CGRectMake(thumbView.frame.origin.x, 0.0, thumbView.frame.size.width, trackFrame.size.height);
  thumbView.frame = thumbFrame;
  
  [self layoutSlideIndexLabel];
  
  CGRect trackLabelFrame = CGRectInset(trackView.bounds, 8.0, 1.0);
  firstSlideLabel.frame = trackLabelFrame;
  lastSlideLabel.frame = trackLabelFrame;
  noSlidesLabel.frame = trackLabelFrame;
  
  if ([delegate respondsToSelector:@selector(viewDidLayout:)])
    [delegate viewDidLayout:self];
}


- (CGFloat) normalizedPosition
{
  CGFloat thumbCenterX = thumbView.center.x;
  CGFloat thumbWidth = thumbView.frame.size.width;
  CGFloat normalizedPosition = (thumbCenterX - thumbWidth / 2) / (self.frame.size.width - thumbWidth);
  normalizedPosition = (normalizedPosition < 0 ) ? 0:normalizedPosition;
  normalizedPosition = (normalizedPosition > 1 ) ? 1:normalizedPosition;
  return normalizedPosition;
}


-(void) handleSliderPanGesture:(UIPanGestureRecognizer*)recognizer
{
  if (recognizer.state == UIGestureRecognizerStateBegan)
  {
    CGPoint p = [recognizer locationInView:self];
    if ((p.x > thumbView.frame.origin.x - 20) && (p.x < thumbView.frame.origin.x + thumbView.frame.size.width + 20))
    {
      isTracking = YES;
      [delegate didStartDraggingThumbslider];
    }
  }
  else if (recognizer.state == UIGestureRecognizerStateChanged)
  {
    if (isTracking)
    {
      CGPoint p = [recognizer locationInView:self];
      CGPoint thumbCenter = thumbView.center;
      
      if ((p.x >= 0 + thumbView.frame.size.width/2.0) && (p.x <= trackView.frame.size.width - thumbView.frame.size.width/2.0))
      {
        thumbView.center = CGPointMake(p.x, thumbCenter.y);
      }
      else if (p.x < 0 + thumbView.frame.size.width/2.0)
      {
        thumbView.center = CGPointMake(thumbView.frame.size.width/2.0, thumbCenter.y);
      }
      else if (p.x > self.frame.size.width - thumbView.frame.size.width/2.0)
      {
        thumbView.center = CGPointMake(self.frame.size.width - thumbView.frame.size.width/2.0, thumbCenter.y);
      }

      [delegate didDragThumbslider:[self normalizedPosition]];
    }
  }
  else if (recognizer.state == UIGestureRecognizerStateEnded)
  {
    if (isTracking)
    {
      isTracking = NO;
      [delegate didFinishDraggingThumbsliderAtPosition:[self normalizedPosition]];
    }
    
    // Analytics
    MezzanineAnalyticsManager *analyticsManager = [MezzanineAnalyticsManager sharedInstance];
    [analyticsManager tagEvent:analyticsManager.scrollPresentationEvent attributes:@{analyticsManager.sourceKey : analyticsManager.sliderAttribute}];
  }
}

- (void) pushbackFinished:(BOOL)isPushbacked
{
  CGRect frame;
  pushbacked = isPushbacked;
  
  if (pushbacked)
  {
    CGFloat newWidth = thumbView.frame.size.width * 3.0;
    newWidth = (newWidth > self.frame.size.width) ? self.frame.size.width : newWidth;
    
    CGFloat widthDeltaHalf = (newWidth - thumbView.frame.size.width) / 2.0;
    
    // In case the thumb grows to the left more than coordinate (0,y) or right more than (view.width,y) and a shift is needed
    CGFloat leftOffset = ((thumbView.frame.origin.x - widthDeltaHalf) < 0) ? fabs(thumbView.frame.origin.x - widthDeltaHalf) : 0;
    CGFloat rightOffset = ((thumbView.frame.origin.x - widthDeltaHalf + newWidth) > trackView.frame.size.width) ? 
    fabs (thumbView.frame.origin.x - widthDeltaHalf + newWidth - trackView.frame.size.width) : 0;
    
    frame = CGRectMake(thumbView.frame.origin.x - widthDeltaHalf + leftOffset - rightOffset, thumbView.frame.origin.y, 
                           newWidth, thumbView.frame.size.height);
  }
  else
  {
    CGFloat newWidth = thumbView.frame.size.width / 3.0;
    CGFloat widthDeltaHalf = (thumbView.frame.size.width - newWidth) / 2.0;
    frame = CGRectMake(thumbView.frame.origin.x + widthDeltaHalf  , thumbView.frame.origin.y, 
                                 newWidth , thumbView.frame.size.height);
  }
  
  [UIView animateWithDuration:0.15
                        delay:0
                      options:UIViewAnimationOptionAllowUserInteraction | UIViewAnimationCurveEaseOut
                   animations:^{	
                     thumbView.frame = frame;
                   }
                   completion:NULL];
}

- (void) setSlideIndex:(NSInteger)slideIndex numberOfSlides:(NSInteger)numberOfSlides
{
  CGFloat position = (numberOfSlides <= 1) ?  0 : (CGFloat)slideIndex / ((CGFloat)numberOfSlides - 1.0);

  CGFloat loc = position * (self.frame.size.width - thumbView.frame.size.width);
  
  CGRect frame = CGRectMake(loc, thumbView.frame.origin.y, 
                            thumbView.frame.size.width, thumbView.frame.size.height);
  
  [UIView animateWithDuration:0.15
                        delay:0
                      options:UIViewAnimationOptionAllowUserInteraction | UIViewAnimationCurveEaseOut
                   animations:^{	
                     thumbView.frame = frame;
                   }
                   completion:NULL];
  
  if (showDeckStateLabels)
  {
    slideIndexLabel.text = [NSString stringWithFormat:@"%ld", (long)slideIndex+1];
    lastSlideLabel.text = [NSString stringWithFormat:@"%ld", (long)numberOfSlides];
    
    BOOL showNoSlidesLabel = (numberOfSlides == 0);
    noSlidesLabel.hidden = !showNoSlidesLabel;
    firstSlideLabel.hidden = showNoSlidesLabel;
    lastSlideLabel.hidden = showNoSlidesLabel;
  }
}


-(void) setShowDeckStateLabels:(BOOL)show
{
  showDeckStateLabels = show;
  
  firstSlideLabel.hidden = !show;
  lastSlideLabel.hidden = !show;
}


- (void) setStyle:(DeckSliderStyle)style
{
  switch (style) 
  {
    case DeckSliderNormalStyle:
    {
      trackView.style = [[MezzanineStyleSheet sharedStyleSheet] sliderTrackStyle:UIControlStateNormal];
      thumbView.style = [[MezzanineStyleSheet sharedStyleSheet] sliderThumbStyle:UIControlStateNormal];
    }
      break;
    case DeckSliderOnlyThumbStyle:
    {
      trackView.style = nil;
      thumbView.style = [self thumbDottedBorderStyle];
    }
      break;
    case DeckSliderDarkStyle:
    {
      trackView.style = [self trackDarkStyle];
      thumbView.style = [self thumbDarkStyle];
    }
    case DeckSliderSmallStyle:
    {
      trackView.style = [[MezzanineStyleSheet sharedStyleSheet] sliderTrackStyle:UIControlStateNormal];
      thumbView.style = [self smallThumbStyle];
    }
      break;
    default:
      break;
  }
}

- (void) updateThumbWidthforSlides:(NSInteger)numberOfSlides andCurrentSlide:(NSInteger)currentSlideIndex
{
  CGFloat posX = 0;
  CGFloat newWidth = 0;   // If 0 slides >> No Thumb (width = 0)
  
  if (numberOfSlides > 0)
  {    
    newWidth = trackView.frame.size.width / ((CGFloat) numberOfSlides);
    if (pushbacked)
      newWidth = trackView.frame.size.width * (3.0 / ((CGFloat) numberOfSlides + 2.0));
  
    if (newWidth < minThumbWidth) newWidth = minThumbWidth;  
  
    CGFloat widthDeltaHalf = fabs (thumbView.frame.size.width - newWidth) / 2.0;   
    
    if (thumbView.frame.size.width > newWidth) // Thumb gets smaller
    {
      if (!pushbacked)
      {
        CGFloat position = (numberOfSlides <= 1) ? 0 : (CGFloat)currentSlideIndex / ((CGFloat)numberOfSlides - 1.0);
        posX = position * (trackView.frame.size.width - newWidth);
      }
      else
      {
        posX = thumbView.frame.origin.x + widthDeltaHalf;
      }
    }
    else
    {
      CGFloat widthDeltaHalf = (newWidth - thumbView.frame.size.width) / 2.0;
      
      // In case the thumb grows to the left more than coordinate (0,y) and a shift is needed
      CGFloat leftOffset = ((thumbView.frame.origin.x - widthDeltaHalf) < 0) ? 
                          fabs(thumbView.frame.origin.x - widthDeltaHalf) : 0;
      // In case the thumb grows to the right more than coordinate (view.width,y) and a shift is needed
      CGFloat rightOffset = ((thumbView.frame.origin.x - widthDeltaHalf + newWidth) > trackView.frame.size.width) ? 
                          fabs (thumbView.frame.origin.x - widthDeltaHalf + newWidth - trackView.frame.size.width) : 0;
      
      posX = thumbView.frame.origin.x - widthDeltaHalf + leftOffset - rightOffset;
    }
  }
    
  [UIView animateWithDuration:0.15
                        delay:0
                      options:UIViewAnimationOptionAllowUserInteraction | UIViewAnimationCurveEaseOut
                   animations:^{	
                     thumbView.frame = CGRectMake(posX, thumbView.frame.origin.y, 
                                                  newWidth, thumbView.frame.size.height);
                   }
                   completion:NULL];
  
  [self layoutSlideIndexLabel];
}


@end
