//
//  PortfolioCollectionViewCell.h
//  Mezzanine
//
//  Created by Zai Chang on 4/2/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PortfolioCollectionViewCell : UICollectionViewCell

@property (nonatomic, assign) MZCommunicator *communicator;
@property (nonatomic, retain) MZPortfolioItem *slide;
@property (nonatomic, retain) UIImageView *imageView;

@end
