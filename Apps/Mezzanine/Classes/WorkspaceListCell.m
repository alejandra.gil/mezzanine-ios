//
//  WorkspaceListCell.m
//  Mezzanine
//
//  Created by miguel on 10/03/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import "WorkspaceListCell.h"
#import "WorkspaceListCell_Private.h"
#import "MezzanineStyleSheet.h"
#import "ButtonMenuViewController.h"
#import "WorkspaceEditorViewController.h"
#import "UIImageView+MZDownloadManager.h"
#import "FPPopoverController+Mezzanine.h"
#import "NSString+VersionChecking.h"
#import "UITraitCollection+Additions.h"
#import "NSObject+UIAlertController.h"

#define kWorkspaceContext @"kWorkspaceContext"
#define kSystemModelContext @"kSystemModelContext"

@interface WorkspaceListCell ()
{
  UITraitCollection *_previousTraitCollection;
}

@end

@implementation WorkspaceListCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
      MezzanineStyleSheet *styleSheet = [MezzanineStyleSheet sharedStyleSheet];

      infoView = [[UIView alloc] initWithFrame:CGRectZero];
      infoView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
      infoView.userInteractionEnabled = NO;  // Important for gesture recognizers to work
      infoView.clipsToBounds = YES;
      infoView.layer.cornerRadius = 3.0f;
      [self.contentView addSubview:infoView];
      
      UIColor *cellContentBackgroundColor = styleSheet.grey70Color;
      imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 120, 120)];  // using CGRectZero here doesn't quite work because the resultant image does not fill the frame (bug 9529)
      imageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
      imageView.contentMode = UIViewContentModeScaleAspectFit;
      imageView.clipsToBounds = YES;
      [infoView addSubview:imageView];

      titleLabel = [[UITextField alloc] initWithFrame:CGRectZero];
      titleLabel.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth| UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleLeftMargin;
      titleLabel.textAlignment = NSTextAlignmentLeft;
      titleLabel.font = [UIFont dinMediumOfSize:15.0];
      titleLabel.backgroundColor = [UIColor clearColor];
      titleLabel.textColor = [UIColor whiteColor];
      titleLabel.minimumFontSize = 12.0;
      titleLabel.enabled = NO;
      [infoView addSubview:titleLabel];

      dateLabel = [[UILabel alloc] initWithFrame:CGRectZero];
      dateLabel.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleLeftMargin;
      dateLabel.textAlignment = NSTextAlignmentLeft;
      dateLabel.font = [UIFont dinOfSize:12.0];
      dateLabel.backgroundColor = [UIColor clearColor];
      dateLabel.textColor = [UIColor whiteColor];
      [infoView addSubview:dateLabel];

      ownerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
      ownerLabel.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleLeftMargin;
      ownerLabel.textAlignment = NSTextAlignmentLeft;
      ownerLabel.font = [UIFont dinOfSize:12.0];
      ownerLabel.backgroundColor = [UIColor clearColor];
      ownerLabel.textColor = [UIColor whiteColor];
      [infoView addSubview:ownerLabel];

      actionsView = [[UIView alloc] initWithFrame:CGRectZero];
      [self.contentView insertSubview:actionsView aboveSubview:infoView];

      _optionsButton = [[UIButton alloc] initWithFrame:CGRectZero];
      [_optionsButton setImage:[UIImage imageNamed:@"WorkspaceWhiteArrow.png"] forState:UIControlStateNormal];
      [_optionsButton addTarget:self action:@selector(showWorkspaceOptions) forControlEvents:UIControlEventTouchUpInside];
      _optionsButton.accessibilityIdentifier = [NSString stringWithFormat:@"%@.OptionsButton", [self class]];
      [actionsView addSubview:_optionsButton];


      UITapGestureRecognizer *actionsAreaTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showWorkspaceOptions)];
      actionsAreaTapRecognizer.numberOfTapsRequired = 1;
      actionsAreaTapRecognizer.numberOfTouchesRequired = 1;
      actionsAreaTapRecognizer.delegate = self;
      [actionsView addGestureRecognizer:actionsAreaTapRecognizer];


      self.contentView.layer.borderWidth = 1.0f;
      self.contentView.layer.cornerRadius = 3.0f;
      self.contentView.layer.borderColor = [MezzanineStyleSheet sharedStyleSheet].defaultHoverColor.CGColor;
      self.contentView.backgroundColor = cellContentBackgroundColor;
      self.contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;

      self.layer.rasterizationScale = [[UIScreen mainScreen] scale];
      self.layer.shouldRasterize = YES;
      
      self.accessibilityIdentifier = @"WorkspaceListCell.CollectionViewCell";

    }
    return self;
}


-(void) layoutSubviews
{
  CGRect bounds = self.bounds;

  [infoView setFrame:bounds];

  CGFloat imageViewMargin = 6.0;
  CGRect imageViewFrame = CGRectMake(imageViewMargin, imageViewMargin, bounds.size.width / 3.0 - 2 * imageViewMargin, bounds.size.height - 2 * imageViewMargin);
  [imageView setFrame:imageViewFrame];

  CGFloat actionsViewWidth = 40.0;
  CGFloat yPos = bounds.size.height / 8.0;
  CGFloat margin = 10.0;
  CGFloat arrowWidth = 22.0;

  [titleLabel setFrame:CGRectMake(bounds.size.width / 3.0 + margin,
                                  yPos,
                                  2* bounds.size.width / 3.0 - (actionsViewWidth),
                                  bounds.size.height / 4.0)];
  yPos += titleLabel.frame.size.height;
  [dateLabel setFrame:CGRectMake(bounds.size.width / 3.0 + margin,
                                 yPos,
                                 2* bounds.size.width / 3.0 - (actionsViewWidth),
                                 bounds.size.height / 4.0)];
  yPos += dateLabel.frame.size.height;
  [ownerLabel setFrame:CGRectMake(bounds.size.width / 3.0 + margin,
                                 yPos,
                                 2* bounds.size.width / 3.0 - (actionsViewWidth),
                                 bounds.size.height / 4.0)];

  [actionsView setFrame:CGRectMake(bounds.size.width - actionsViewWidth, 0.0, actionsViewWidth, bounds.size.height)];
  [_optionsButton setFrame:CGRectMake((actionsViewWidth-arrowWidth)/2.0, 10.0, arrowWidth, arrowWidth)];
}


- (void)setIsOpeningViewCell:(BOOL)isOpeningViewCell
{
  _isOpeningViewCell = isOpeningViewCell;

  if (_isOpeningViewCell)
  {
    MezzanineStyleSheet *styleSheet = [MezzanineStyleSheet sharedStyleSheet];
    titleLabel.textColor = styleSheet.grey90Color;
    dateLabel.textColor = styleSheet.grey90Color;
    ownerLabel.textColor = styleSheet.grey90Color;

    CALayer *layer = self.contentView.layer;

    // Border pulsating animation
    CALayer *outerBorderLayer = [CALayer layer];
    outerBorderLayer.frame = self.frame;
    outerBorderLayer.borderWidth = 1.0f;
    outerBorderLayer.cornerRadius = 3.0f;
    outerBorderLayer.borderColor = [UIColor whiteColor].CGColor;
    [layer addSublayer:outerBorderLayer];

    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"borderWidth"];
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animation.autoreverses = YES;
    animation.duration = 1.0;
    animation.values = @[@1.0, @2.0];
    animation.repeatCount = HUGE_VALF;
    [outerBorderLayer addAnimation:animation forKey:@"BorderPulsatingAnimation"];

    CAKeyframeAnimation *animation2 = [CAKeyframeAnimation animationWithKeyPath:@"bounds.size.width"];
    animation2.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animation2.autoreverses = YES;
    animation2.duration = 1.0;
    animation2.values = @[@(layer.bounds.size.width), @(layer.bounds.size.width+2.0)];
    animation2.repeatCount = HUGE_VALF;
    [outerBorderLayer addAnimation:animation2 forKey:@"BorderPulsatingAnimation2"];

    CAKeyframeAnimation *animation3 = [CAKeyframeAnimation animationWithKeyPath:@"bounds.size.height"];
    animation3.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animation3.autoreverses = YES;
    animation3.duration = 1.0;
    animation3.values = @[@(layer.bounds.size.height), @(layer.bounds.size.height+2.0)];
    animation3.repeatCount = HUGE_VALF;
    [outerBorderLayer addAnimation:animation3 forKey:@"BorderPulsatingAnimation3"];
  }

  [self updateCellBorder];
}


- (void) updateCellBorder
{
  MezzanineStyleSheet *styleSheet = [MezzanineStyleSheet sharedStyleSheet];
  UIColor *highlightedCellColor = styleSheet.grey90Color;
  UIColor *defaultCellColor = styleSheet.grey70Color;

  CGFloat defaultBorderWidth = 0.0;

  if (_isOpeningViewCell)
  {
    // Only allow one style for opening cell
    self.contentView.layer.borderColor = [MezzanineStyleSheet sharedStyleSheet].highlightedTextColor.CGColor;
    self.contentView.layer.borderWidth = defaultBorderWidth;
    infoView.backgroundColor = [UIColor whiteColor];

    return;
  }
  
  if ([_systemModel.currentWorkspace.uid isEqualToString:[_workspace uid]])
  {
    if (_workspace.isLoading)
    {
      self.contentView.layer.borderColor = [MezzanineStyleSheet sharedStyleSheet].highlightedTextColor.CGColor;
      self.contentView.layer.borderWidth = defaultBorderWidth;
      infoView.backgroundColor = highlightedCellColor;
    }
    else
    {
      self.contentView.layer.borderColor = [MezzanineStyleSheet sharedStyleSheet].defaultHighlightColor.CGColor;
      self.contentView.layer.borderWidth = 4.0f;
      
      infoView.backgroundColor = styleSheet.grey50Color;
    }
  }
  else
  {
    if (self.highlighted)
    {
      self.contentView.layer.borderColor = [MezzanineStyleSheet sharedStyleSheet].defaultHoverColor.CGColor;
      self.contentView.layer.borderWidth = defaultBorderWidth;
      infoView.backgroundColor = highlightedCellColor;
    }
    else
    {
      self.contentView.layer.borderColor = [MezzanineStyleSheet sharedStyleSheet].defaultHoverColor.CGColor;
      self.contentView.layer.borderWidth = defaultBorderWidth;
      infoView.backgroundColor = defaultCellColor;
    }
  }

  self.layer.shouldRasterize = YES;
}


- (void)setWorkspace:(id)anObject
{
  if (anObject != _workspace)
  {
    if (_workspace)
    {
      [self endObservingWorkspace:_workspace];
      dateLabel.text = nil;
      titleLabel.text = nil;
    }

    _workspace = anObject;

    if (_workspace)
    {
      BOOL isPublic = !_workspace.owner || [_workspace.owner isEqualToString:@""];
      titleLabel.text = _workspace.name;
      dateLabel.text = _workspace.modifiedDateString;
      ownerLabel.text = isPublic ? NSLocalizedString(@"Workspace List Cell Public", nil) : _workspace.owner;
      self.accessibilityLabel = [NSString stringWithFormat:@"%@-%@", [self class], _workspace.name];
      self.optionsButton.accessibilityIdentifier = [NSString stringWithFormat:@"%@.optionsButton-%@", [self class], _workspace.name];

      NSString *placeholderImageName = @"WorkspaceThumbNoSlides";
      NSURL *url = [[MezzanineAppContext currentContext].currentCommunicator urlForResource:_workspace.dynamicThumbURL];
      [imageView setImageWithURL:url andPlaceholderImage:[UIImage imageNamed:placeholderImageName]];

      [self beginObservingWorkspace:_workspace];

      [self updateCellBorder];
    }
  }
}


- (void)prepareForReuse
{
  DLog(@"prepareForReuse cell with tag: %@", self);
  
  [self endObservingWorkspace:self.workspace];

  _systemModel = nil;
  _workspace = nil;
  dateLabel.text = nil;
  titleLabel.text = nil;
  [imageView stopLoadingImage];
  [super prepareForReuse];

  if (currentPopoverController)
  {
    // This condition happens when a user gets connected while a popover (e.g. rename) is visible. In which case we want to dismiss it to prevent a possible crash. See bug 12966
    [currentPopoverController dismissPopoverAnimated:NO];
  }

  if (currentAlertController)
  {
    [currentAlertController dismissViewControllerAnimated:YES completion:nil];
    currentAlertController = nil;
  }
}


- (void)dealloc
{
  [self endObservingWorkspace:self.workspace];

  [imageView stopLoadingImage];
}


- (void)updatePopoverForTraitCollection:(UITraitCollection *)traitCollection
{
  if (!currentPopoverController) {
    return;
  }
  
  if (_previousTraitCollection.horizontalSizeClass != traitCollection.horizontalSizeClass ||
      _previousTraitCollection.verticalSizeClass != traitCollection.verticalSizeClass)
  {
    [currentPopoverController dismissPopoverAnimated:NO];
  } else if (traitCollection.isiPadRegularHeight && traitCollection.isiPadRegularWidth) {
    [currentPopoverController presentPopoverFromRect:_optionsButton.frame
                                              inView:actionsView
                            permittedArrowDirections:FPPopoverArrowDirectionVertical
                                            animated:YES];
  } else {
    [currentPopoverController dismissPopoverAnimated:NO];
  }

  if (currentAlertController)
  {
    [currentAlertController dismissViewControllerAnimated:YES completion:nil];
    currentAlertController = nil;
  }

  _previousTraitCollection = traitCollection;
}


#pragma mark -  Model

- (void)beginObservingWorkspace:(MZWorkspace*)aWorkspace
{
  [aWorkspace addObserver:self forKeyPath:@"name" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:kWorkspaceContext];
  [aWorkspace addObserver:self forKeyPath:@"modifiedDate" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:kWorkspaceContext];
  [aWorkspace addObserver:self forKeyPath:@"modifiedDateString" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:kWorkspaceContext];
  [aWorkspace addObserver:self forKeyPath:@"isBeingEdited" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:kWorkspaceContext];
}

- (void)endObservingWorkspace:(MZWorkspace*)aWorkspace
{
  [aWorkspace removeObserver:self forKeyPath:@"name"];
  [aWorkspace removeObserver:self forKeyPath:@"modifiedDate"];
  [aWorkspace removeObserver:self forKeyPath:@"modifiedDateString"];
  [aWorkspace removeObserver:self forKeyPath:@"isBeingEdited"];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)anObject change:(NSDictionary *)change context:(void *)context
{
  if (context == kWorkspaceContext)
  {
    MZWorkspace *workspace = (MZWorkspace*)anObject;

    if ([keyPath isEqual:@"name"])
    {
      titleLabel.text = workspace.name;
      _optionsButton.accessibilityIdentifier = [NSString stringWithFormat:@"%@.optionsButton-%@", [self class], _workspace.name];
    }
    else if ([keyPath isEqual:@"modifiedDate"])
    {
      NSString *defaultPlaceholderImageName = @"WorkspaceThumbNoSlides";
      UIImage *placeholderImage = imageView.image ? imageView.image : [UIImage imageNamed:defaultPlaceholderImageName];
      NSURL *url = [[MezzanineAppContext currentContext].currentCommunicator urlForResource:workspace.dynamicThumbURL];
      [imageView setImageWithURL:url andPlaceholderImage:placeholderImage];
    }
    else if ([keyPath isEqual:@"modifiedDateString"])
    {
      dateLabel.text = workspace.modifiedDateString;
    }
    else if ([keyPath isEqual:@"isBeingEdited"])
    {
      self.alpha = (workspace.isBeingEdited) ? 0.0 : 1.0;
    }
  }
  else
  {
    [super observeValueForKeyPath:keyPath ofObject:anObject change:change context:context];
  }
}


- (void)showWorkspaceOptions
{
  ButtonMenuViewController *controller = [[ButtonMenuViewController alloc] init];

  __weak typeof (self) weakSelf = self;
  NSMutableArray *items = [[NSMutableArray alloc] init];

  [items addObject:[ButtonMenuItem itemWithTitle:NSLocalizedString(@"Workspace Menu Rename Button", nil) action:^{
    [currentPopoverController dismissPopoverAnimated:NO completion:^{
      currentPopoverController = nil;
      [weakSelf renameWorkspace];
    }];
  }]];
  [items addObject:[ButtonMenuItem itemWithTitle:NSLocalizedString(@"Workspace Menu Duplicate Button", nil) action:^{
    [currentPopoverController dismissPopoverAnimated:NO completion:^{
      currentPopoverController = nil;
      [weakSelf duplicateWorkspace];
    }];
  }]];

  ButtonMenuItem *deleteButton = [ButtonMenuItem itemWithTitle:NSLocalizedString(@"Workspace Menu Delete Button", nil) action:^{
    [weakSelf deleteWorkspace];
  }];

  deleteButton.buttonColor = [MezzanineStyleSheet sharedStyleSheet].redHighlightColor;

  [items addObject:deleteButton];

  controller.menuItems = items;

  currentPopoverController = [[FPPopoverController alloc] initWithContentViewController:controller delegate:self];
  [currentPopoverController stylizeAsActionSheet];
  [currentPopoverController presentPopoverFromRect:_optionsButton.frame inView:actionsView permittedArrowDirections:[self popoverArrowDirectionForCurrentOrientationAndDevice] animated:YES];
}


#pragma mark UIPopoverControllerDelegate

- (void)popoverControllerDidDismissPopover:(FPPopoverController *)popoverController
{
  if (currentPopoverController)
  {
    currentPopoverController = nil;
  }
}

#pragma mark Workspace actions

- (void)duplicateWorkspace
{
  WorkspaceEditorViewController *viewController = [[WorkspaceEditorViewController alloc] initWithWorkspace:self.workspace];
  viewController.preferredContentSize = CGSizeMake([self contentWidthForPopover], 188.0);
  viewController.doneButtonTitle = NSLocalizedString(@"Workspace Menu Duplicate Button", nil);
  viewController.hintText = (_systemModel.currentUsername != nil) ? [NSString stringWithFormat:NSLocalizedString(@"Worskpace Save As Private With User Message", nil), _systemModel.currentUsername] : NSLocalizedString(@"Worskpace Save As Public Message", nil);
  NSString *newName = [self.workspace.name stringByAppendingString:NSLocalizedString(@"Workspace List Cell Duplicated Workspace Name Suffix", nil)];
  viewController.workspaceNameOverride = newName;
  viewController.autoHighlightNameField = YES;
  
  __weak typeof (self) weakSelf = self;
  __weak typeof (viewController) __viewController = viewController;
  viewController.requestSave = ^{
    NSArray *ownersArray = weakSelf.systemModel.currentUsername ? @[weakSelf.systemModel.currentUsername] : @[];
    [weakSelf.communicator.requestor requestWorkspaceDuplicateRequest:weakSelf.workspace.uid owners:ownersArray name:__viewController.nameTextField.text];
    [currentPopoverController dismissPopoverAnimated:YES];
  };
  
  currentPopoverController = [[FPPopoverController alloc] initWithContentViewController:viewController delegate:self];
  [currentPopoverController stylizeAsActionSheet];

  [currentPopoverController presentPopoverFromRect:_optionsButton.frame inView:actionsView permittedArrowDirections:[self popoverArrowDirectionForCurrentOrientationAndDevice] animated:YES];
}

- (void)renameWorkspace
{
  WorkspaceEditorViewController *viewController = [[WorkspaceEditorViewController alloc] initWithWorkspace:self.workspace];
  viewController.preferredContentSize = CGSizeMake([self contentWidthForPopover], 130.0);
  viewController.doneButtonTitle = NSLocalizedString(@"Workspace Menu Rename Button", nil);
  viewController.autoHighlightNameField = YES;
  
  __weak typeof (self) weakSelf = self;
  __weak typeof (viewController) __viewController = viewController;
  viewController.requestSave = ^{
    [weakSelf.communicator.requestor requestWorkspaceRenameRequest:weakSelf.workspace.uid name:__viewController.nameTextField.text];
    [currentPopoverController dismissPopoverAnimated:YES];
  };
  
  currentPopoverController = [[FPPopoverController alloc] initWithContentViewController:viewController delegate:self];
  [currentPopoverController stylizeAsActionSheet];
  [currentPopoverController presentPopoverFromRect:_optionsButton.frame inView:actionsView permittedArrowDirections:[self popoverArrowDirectionForCurrentOrientationAndDevice] animated:YES];
}

- (void)deleteWorkspace
{
  __weak typeof (self) __weakSelf = self;
  UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Workspace Delete Dialog Title", nil)
                                                                           message:nil
                                                                    preferredStyle:UIAlertControllerStyleAlert];
  UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Workspace Delete Dialog Confirm Button Title", nil)
                                                         style:UIAlertActionStyleDestructive
                                                       handler:^(UIAlertAction * _Nonnull action) {
                                                         [__weakSelf confirmDeletingWorkspace:__weakSelf.workspace.uid];
                                                       }];
  UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Workspace Delete Dialog Cancel Button Title", nil)
                                                         style:UIAlertActionStyleCancel
                                                       handler:^(UIAlertAction * _Nonnull action) {
                                                         [__weakSelf cancelDeletingWorkspace];
                                                       }];
  [alertController addAction:deleteAction];
  [alertController addAction:cancelAction];
  [self showAlertController:alertController animated:YES completion:nil];
  currentAlertController = alertController;
}


- (void)confirmDeletingWorkspace:(NSString *)workspaceUid
{
  __weak typeof (self) __weakSelf = self;
  [currentPopoverController dismissPopoverAnimated:NO completion:^{
    [__weakSelf.communicator.requestor requestWorkspaceDeleteRequest:workspaceUid];
  }];
  currentAlertController = nil;
}


- (void)cancelDeletingWorkspace
{
  [currentPopoverController dismissPopoverAnimated:NO completion:nil];
  currentAlertController = nil;
}


#pragma mark Layout Helpers

- (CGFloat) contentWidthForPopover
{
  if ([[UIDevice currentDevice] isIPad])
    return 320.0;

  if (UIDeviceOrientationIsLandscape([[UIDevice currentDevice] orientation]))
    return 340.0;

  return 300.0;
}


- (FPPopoverArrowDirection) popoverArrowDirectionForCurrentOrientationAndDevice
{
  return ![[UIDevice currentDevice] isIPad] && UIDeviceOrientationIsLandscape([[UIDevice currentDevice] orientation]) ? FPPopoverNoArrow : FPPopoverArrowDirectionVertical;
}




@end
