//
//  UIView+Debug.h
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 03/02/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Debug)

- (void)debugWithColor:(UIColor *)color;

@end
