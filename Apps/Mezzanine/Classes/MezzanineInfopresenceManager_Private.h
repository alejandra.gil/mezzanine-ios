//
//  MezzanineInfopresenceManager_Private.h
//  Mezzanine
//
//  Created by miguel on 14/3/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

#import "MezzanineInfopresenceManager.h"
#import "InfopresenceOverlayViewController.h"

static CGFloat kWorkspaceOverlaysAnimationDuration = 0.2;

@interface MezzanineInfopresenceManager ()
{
  NSMutableArray *infopresenceObservers;
  NSMutableArray *systemModelObservers;
  NSMutableArray *currentCallObservers;
  UIAlertController *incomingRequestAlertController;
  UIAlertController *infopresenceInterruptedAlertController;

  UIWindow *overlayWindow;
  InfopresenceOverlayViewController *infopresenceOverlayViewController;
  BOOL _infopresenceIsBeingHidden;
  NSDate *_infopresenceOverlayStartTime;
}


@property (nonatomic, strong) MZInfopresenceCall *currentCall;


- (void)showInfopresenceOverlayJoining;
- (void)showInfopresenceOverlayJoining:(MZRemoteMezz*)remoteMezz;
- (void)showInfopresenceOverlayJoining:(MZRemoteMezz*)remoteMezz animateFromRect:(CGRect)animateFromRect;
- (void)hideInfopresenceOverlayAnimated:(BOOL)animated;
- (void)hideInfopresenceOverlayAnimated:(BOOL)animated withDelay:(CGFloat)animationDelay;

@end
