//
//  UIViewControllerExtension.swift
//  Mezzanine
//
//  Created by miguel on 10/11/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

extension UIViewController
{
  @objc func enableHideKeyboardOnTapOutsideTextFields()
  {
    let tap: UITapGestureRecognizer = UITapGestureRecognizer(
      target: self,
      action: #selector(UIViewController.dismissKeyboard))

    view.addGestureRecognizer(tap)
  }

  @objc func dismissKeyboard()
  {
    view.endEditing(true)
  }
}

