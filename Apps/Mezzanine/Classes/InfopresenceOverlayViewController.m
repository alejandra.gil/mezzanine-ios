//
//  InfopresenceOverlayViewController.m
//  Mezzanine
//
//  Created by Zai Chang on 4/22/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import "InfopresenceOverlayViewController.h"
#import "InfopresenceOverlayViewController_Private.h"
#import "MezzanineStyleSheet.h"
#import "NSObject+BlockObservation.h"
#import "NSString+VersionChecking.h"
#import "Mezzanine-Swift.h"

@implementation InfopresenceOverlayViewController


- (void)viewDidLoad
{
  [super viewDidLoad];
  [self setupView];
  [self initAccessibility];
}

- (void)setupView
{
  if (!_systemModel)
    return;

  MezzanineStyleSheet *styleSheet = [MezzanineStyleSheet sharedStyleSheet];
  [styleSheet stylizeButton:self.button];
  self.label.font = [UIFont dinBoldOfSize:19.0];
  self.label.textColor = [UIColor whiteColor];

  BOOL isIPad = [[UIDevice currentDevice] isIPad];

  if (isIPad)
  {
    self.cellContainerWidthConstraint.constant = 360.0;
    self.cellContainerHeightConstraint.constant = 90.0;
    [self.view setNeedsLayout];
  }

  [_remoteMezzView removeFromSuperview];

  self.view.backgroundColor = [styleSheet.selectedBlueColor colorWithAlphaComponent:0.9];

  _remoteMezzView = [[[NSBundle mainBundle] loadNibNamed:@"RemoteMezzanineTableViewCell" owner:self options:nil] firstObject];
  _remoteMezzView.frame = _cellContainerView.bounds;
  _remoteMezzView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
  _remoteMezzView.isJoiningViewCell = YES;
  [self.cellContainerView addSubview:_remoteMezzView];
  [_remoteMezzView setNeedsLayout];

  self.button.titleLabel.font = [UIFont dinMediumOfSize:13.0];
  self.button.backgroundColor = styleSheet.grey170Color;
  self.button.layer.cornerRadius = 5.0;
  [self.button setTitle:NSLocalizedString(@"Infopresence Overlay Cancel", nil) forState:UIControlStateNormal];

  self.labelContainerLeadingConstraint.constant = 0.0;
  self.buttonContainerLeadingConstraint.constant = 0.0;
  self.buttonWidthConstraint.constant = 90.0;

  [self updateView];
}


- (void)initAccessibility
{
  self.label.accessibilityIdentifier = @"InfopresenceOverlayView.State";
  self.button.accessibilityIdentifier = @"InfopresenceOverlayView.CancelButton";
}


- (void)setRemoteMezz:(MZRemoteMezz *)remoteMezz
{
  if (_remoteMezz != remoteMezz)
  {
    if (_remoteMezz)
    {
      [_remoteMezz removeObserver:self forKeyPath:@"callResolution"];
      _remoteMezzView.remoteMezz = nil;
    }

    _remoteMezz = remoteMezz;

    if (_remoteMezz)
    {
      [_remoteMezz addObserver:self forKeyPath:@"callResolution" options:(NSKeyValueObservingOptionInitial) context:nil];
      [self updateView];
    }
  }
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
  if ([keyPath isEqualToString:@"callResolution"])
  {
    [self updateView];
  }
  else if ([keyPath isEqualToString:@"mezzBeingJoined"])
  {
    [self updateView];
  }
  else if ([keyPath isEqualToString:@"myMezzanine.version"])
  {
    [self setupView];
  }
}


- (void)updateView
{
  if (!_remoteMezz)
    return;

  if (_remoteMezz.callResolution ==  MZInfopresenceCallResolutionUnknown)
  {
    NSString *buttonTitle;
    _remoteMezzView.remoteMezz = _remoteMezz;
    buttonTitle = NSLocalizedString(@"Infopresence Overlay Cancel", nil);

    self.label.text = NSLocalizedString(@"Infopresence Overlay Title Joining", nil);
    [self.label sizeToFit];
    [self.button setTitle:buttonTitle forState:UIControlStateNormal];
    self.button.hidden = NO;

    [self.button addTarget:self action:@selector(cancelCall:) forControlEvents:UIControlEventTouchUpInside];
  }
  else
  {
    NSString *title = NSLocalizedString(@"Infopresence Overlay Title Cancelled", nil);
    if (_remoteMezz.callResolution == MZInfopresenceCallResolutionAccepted)
    {
      return;
    }
    else if (_remoteMezz.callResolution == MZInfopresenceCallResolutionDeclined)
      title = NSLocalizedString(@"Infopresence Overlay Title Declined", nil);
    else if (_remoteMezz.callResolution == MZInfopresenceCallResolutionTimedOut)
      title = NSLocalizedString(@"Infopresence Overlay Title TimedOut", nil);

    self.label.text = title;
    [self.label sizeToFit];
    self.button.hidden = YES;

    [self.button removeTarget:self action:@selector(cancelCall:) forControlEvents:UIControlEventTouchUpInside];
  }
}


- (IBAction)cancelCall:(id)sender
{
  [_communicator.requestor requestInfopresenceOutgoingCancelRequest];
}

@end
