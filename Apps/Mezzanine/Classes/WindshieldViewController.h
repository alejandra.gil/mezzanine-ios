//
//  WindshieldViewController.h
//  Mezzanine
//
//  Created by Miguel Sánchez Valdés on 5/10/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AssetView.h"
#import <OBDragDropManager.h>
#import "FPPopoverController.h"

@class AssetView;
@class MezzanineMainViewController;
@class WorkspaceViewController;

@protocol WindshieldViewControllerDelegate <NSObject>

- (void)windshieldViewUpdateSpaceIndexWithLocation:(CGPoint)location;
- (void)windshieldViewPresentDragDropDeletionAreaWithDeletion:(BOOL)allowDeletion;
- (void)windshieldViewHideDragDropDeletionArea;
- (void)windshieldViewEdgeScrollingEventAtLocation:(CGPoint)location;
- (void)windshieldViewEndEdgeScrolling;

@end


@interface WindshieldViewController : UIViewController <OBDropZone, OBOvumSource, FPPopoverControllerDelegate, UIGestureRecognizerDelegate>
{
  AssetView *draggedView;  // Shielder View that is being dragged
  
  NSMutableArray *insertionIndicators;
  NSMutableArray *itemViews;
  
  CGRect frameBeforeDrag;
  BOOL hasExitedFromWindshield;
  BOOL ovumDragViewAsIndicator;

  MZSystemModel *_systemModel;
}

@property (nonatomic, weak) id <WindshieldViewControllerDelegate> delegate;

@property (nonatomic, weak) MZCommunicator *communicator;
@property (nonatomic, weak) WorkspaceViewController *workspaceViewController;
@property (nonatomic, strong) MZSystemModel *systemModel;
@property (nonatomic, strong) MZWindshield *windshield;
@property (nonatomic, strong) MZWorkspace *workspace;
@property (nonatomic, strong) AssetView *draggedView;

-(void) requestDeleteItem:(MZWindshieldItem *)item;

-(void) updateOnEdgeScrolling;

@end
