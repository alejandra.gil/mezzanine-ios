//
//  FPPopoverController+Oblong.h
//  Mezzanine
//
//  Created by Zai Chang on 6/17/13.
//  Copyright (c) 2013 Oblong Industries. All rights reserved.
//

#import "FPPopoverController.h"

@interface FPPopoverController (Mezzanine)

- (void)stylizeBorderlessWithArrowColor:(UIColor *)arrowColor;
- (void)stylizeWithNavigationBar:(BOOL)hasNavigationBar;
- (void)stylizeAsActionSheet;

@end
