//
//  PhotoCollectionViewCell.swift
//  Mezzanine
//
//  Created by miguel on 3/1/17.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import UIKit

struct PhotoCollectionViewCellConstants {
  static let checkmarkSide:CGFloat = 30.0
  static let checkmarkMargin:CGFloat = 5.0
}

class PhotoCollectionViewCell: UICollectionViewCell {

  var imageView: UIImageView
  var overlayImageView: UIImageView

  var photoAsset: PhotoAsset! {
    willSet(aPhotoAsset) {
      if self.photoAsset != aPhotoAsset && self.photoAsset != nil {
        endObservingPhotoAsset()
        if tag != 0 {
          PhotoManager.sharedInstance.cancelThumbnailRequest(tag)
        }
      }
    }

    didSet(aPhotoAsset) {
      if self.photoAsset != aPhotoAsset && self.photoAsset != nil {
        beginObservingPhotoAsset()
        tag = PhotoManager.sharedInstance.requestThumbnail(for: self.photoAsset!, resultHandler: { result in
          self.imageView.image = result
        })
        overlayImageView.alpha = photoAsset!.selected ? 1.0 : 0.0
      }
    }
  }

  override init(frame: CGRect) {
    imageView = UIImageView(frame: CGRect.zero)
    imageView.contentMode = .scaleAspectFill
    imageView.clipsToBounds = true

    overlayImageView = UIImageView(frame: CGRect.zero)
    overlayImageView.contentMode = .scaleAspectFill
    overlayImageView.clipsToBounds = true

    overlayImageView.image = UIImage(named: "OBImagePickerCheckmark.png")
    overlayImageView.alpha = 0.0

    super.init(frame: frame)

    let checkmarkSide = PhotoCollectionViewCellConstants.checkmarkSide
    let checkmarkMargin = PhotoCollectionViewCellConstants.checkmarkMargin

    imageView.frame = self.bounds
    overlayImageView.frame = CGRect(x: self.bounds.width - checkmarkSide - checkmarkMargin, y: self.bounds.height - checkmarkSide - checkmarkMargin, width: checkmarkSide, height: checkmarkSide)

    backgroundColor = UIColor.clear
    accessibilityIdentifier = "Image Picker Cell"

    contentView.addSubview(imageView)
    contentView.insertSubview(overlayImageView, aboveSubview: imageView)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  deinit {
    endObservingPhotoAsset()
    if tag != 0 {
      PhotoManager.sharedInstance.cancelThumbnailRequest(tag)
    }
  }

  override func prepareForReuse() {
    super.prepareForReuse()
    photoAsset = nil
    overlayImageView.alpha = 0.0
  }

  func endObservingPhotoAsset() {
    photoAsset?.removeObserver(self, forKeyPath: "selected")
  }

  func beginObservingPhotoAsset() {
    photoAsset?.addObserver(self, forKeyPath: "selected", options: .new, context: nil)
  }

  override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
    if keyPath == "selected" {
      guard let asset = object as? PhotoAsset else { return }
      if asset == photoAsset {
        overlayImageView.alpha = photoAsset!.selected ? 1.0 : 0.0
      }
    }
    else {
      super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
    }
  }
}
