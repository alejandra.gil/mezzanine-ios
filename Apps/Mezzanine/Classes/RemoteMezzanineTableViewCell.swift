//
//  RemoteMezzanineTableViewCell.swift
//  Mezzanine
//
//  Created by Zai Chang on 6/26/15.
//  Copyright (c) 2015 Oblong Industries. All rights reserved.
//

import UIKit

class RemoteMezzanineTableViewCell : UITableViewCell {

	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var companyLabel: UILabel!
	@IBOutlet weak var locationLabel: UILabel!
	@IBOutlet weak var statusLabel: UILabel!
	@IBOutlet weak var iconImageView: UIImageView!
  @IBOutlet weak var iconModifierImageView: UIImageView!
  @IBOutlet weak var bottomSeparator: UIView!
  @IBOutlet weak var nameTrailingSpaceToSuperviewConstraint: NSLayoutConstraint!
  @IBOutlet weak var companyTrailingSpaceToSuperviewConstraint: NSLayoutConstraint!
  @IBOutlet weak var locationTrailingSpaceToSuperviewConstraint: NSLayoutConstraint!

	override func awakeFromNib() {
		super.awakeFromNib()

		let styleSheet = MezzanineStyleSheet.shared()
		self.backgroundColor = styleSheet?.grey30Color // 3.0 styleSheet.darkestBluerFillColor
		
		selectedBackgroundView = UIView(frame: self.bounds)
		selectedBackgroundView!.backgroundColor = styleSheet?.selectedBlueColor

    iconImageView.image = UIImage(named: "mobile-mezz-icon-header.png")?.withRenderingMode(.alwaysTemplate)
    iconModifierImageView.image = nil

		// Stylize labels
		nameLabel.font = UIFont.dinBold(ofSize: 16.0)
		companyLabel.font = UIFont.din(ofSize: 16.0)
		locationLabel.font = UIFont.din(ofSize: 16.0)
		statusLabel.font = UIFont.dinLight(ofSize: 16.0)
	}

	deinit
	{
    endObservingRemoteMezz()
	}


	override func setSelected(_ selected: Bool, animated: Bool) {

    if isJoiningViewCell {
      return
    }

		super.setSelected(selected, animated: animated)

		statusLabel.isHidden = selected
		
		if selected {
      let dummyBiggerView = UIView(frame: CGRect(x: 0, y: 0, width: 36, height: 13))
      let checkView = UIImageView(image: UIImage(named: "mobile-check-icon"))
      checkView.frame = CGRect(x: 0, y: 0, width: 18, height: 13)
      dummyBiggerView.addSubview(checkView)
      self.accessoryView = dummyBiggerView
      self.accessoryView!.frame = CGRect(x: 0, y: 0, width: 36, height: 13)

      nameTrailingSpaceToSuperviewConstraint.constant = 90
      companyTrailingSpaceToSuperviewConstraint.constant = 90
      locationTrailingSpaceToSuperviewConstraint.constant = 90
		}
		else {
      self.accessoryView = nil

      nameTrailingSpaceToSuperviewConstraint.constant = 55
      companyTrailingSpaceToSuperviewConstraint.constant = 55
      locationTrailingSpaceToSuperviewConstraint.constant = 55
		}

    updateConstraintsIfNeeded()
	}

	func updateNameLabel() {
		nameLabel.text = remoteMezz?.name
    updateConstraintsIfNeeded()
	}

	func updateCompanyLabel() {
		companyLabel.text = remoteMezz?.company
    updateConstraintsIfNeeded()
	}

	func updateLocationLabel() {
		locationLabel.text = remoteMezz?.location
    updateConstraintsIfNeeded()
	}

	func updateCollaborationState() {

    if isJoiningViewCell == true
    {
      return
    }

		statusLabel.text = remoteMezz.stateString()

		let styleSheet = MezzanineStyleSheet.shared()
		var textColor = styleSheet?.textNormalColor
    var iconModifier: UIImage? = nil

		switch remoteMezz.collaborationState {
		case .notInCollaboration:
			if !remoteMezz.online {
				textColor = styleSheet?.grey90Color
        statusLabel.text = "Infopresence Collaboration State Offline".localized
        statusLabel.textColor = styleSheet?.mediumFillColor
			}
      else {
        textColor = UIColor.white
        statusLabel.text = nil
      }
		case .inCollaboration:
      self.isSelected = false
			textColor = styleSheet?.grey150Color
			statusLabel.text = "Infopresence Collaboration State InCollaboration".localized
      statusLabel.textColor = styleSheet?.greenHighlightColor
      iconModifier = UIImage(named: "mobile-infopresence-checkmark-icon.png")
		case .joining:
      self.isSelected = false
			textColor = styleSheet?.grey150Color
			statusLabel.text = "Infopresence Collaboration State Joining".localized
      statusLabel.textColor = styleSheet?.yellowHighlightColor
      iconModifier = UIImage(named: "mobile-infopresence-time-icon.png")
		case .invited:
			self.isSelected = false
			textColor = styleSheet?.grey150Color
			statusLabel.text = "Infopresence Collaboration State Invited".localized
			statusLabel.textColor = styleSheet?.yellowHighlightColor
			iconModifier = UIImage(named: "mobile-infopresence-time-icon.png")
		}

		nameLabel.textColor = textColor
		companyLabel.textColor = textColor
		locationLabel.textColor = textColor
    iconImageView.tintColor = textColor
    iconModifierImageView.image = iconModifier
    updateConstraintsIfNeeded()
	}


  @objc var isJoiningViewCell = false {
    willSet(joiningViewCell) {
    }

    didSet
    {
      if (self.isJoiningViewCell == true)
      {
        bottomSeparator.isHidden = true
        statusLabel.removeFromSuperview()

        self.backgroundColor = UIColor.white
        self.layer.cornerRadius = 5.0

        let styleSheet = MezzanineStyleSheet.shared()
        let textColor = styleSheet?.grey90Color

        nameLabel.textColor = textColor
        companyLabel.textColor = textColor
        locationLabel.textColor = textColor
        iconImageView.tintColor = textColor
        iconModifierImageView.image = nil

        nameTrailingSpaceToSuperviewConstraint.constant = 22
        companyTrailingSpaceToSuperviewConstraint.constant = 22
        locationTrailingSpaceToSuperviewConstraint.constant = 22

        updateConstraintsIfNeeded()
      }
    }
  }


	fileprivate var kRemoteMezzContext = 0
	// Ideally, constants such as key paths should be placed in the object's header file
	fileprivate var kNameObservationKeyPath = "name"
	fileprivate var kLocationObservationKeyPath = "location"
	fileprivate var kCompanyObservationKeyPath = "company"
	fileprivate var kCollaborationStateObservationKeyPath = "collaborationState"
  fileprivate var kOnlineObservationKeyPath = "online"

	@objc var remoteMezz: MZRemoteMezz! {
		willSet(aRemoteMezz) {
			if self.remoteMezz != aRemoteMezz && self.remoteMezz != nil
			{
				endObservingRemoteMezz()
			}
		}

		didSet(aRemoteMezz)
		{
			updateNameLabel()
			updateCompanyLabel()
			updateLocationLabel()
			updateCollaborationState()

      if self.remoteMezz != aRemoteMezz && self.remoteMezz != nil
      {
        beginObservingRemoteMezz()
      }
		}
	}

	func beginObservingRemoteMezz() {
		remoteMezz?.addObserver(self, forKeyPath: kNameObservationKeyPath, options: .new, context: &kRemoteMezzContext)
    remoteMezz?.addObserver(self, forKeyPath: kCompanyObservationKeyPath, options: .new, context: &kRemoteMezzContext)
		remoteMezz?.addObserver(self, forKeyPath: kLocationObservationKeyPath, options: .new, context: &kRemoteMezzContext)
		remoteMezz?.addObserver(self, forKeyPath: kCollaborationStateObservationKeyPath, options: .new, context: &kRemoteMezzContext)
    remoteMezz?.addObserver(self, forKeyPath: kOnlineObservationKeyPath, options: .new, context: &kRemoteMezzContext)
	}
	
	func endObservingRemoteMezz() {
		remoteMezz?.removeObserver(self, forKeyPath: kNameObservationKeyPath)
    remoteMezz?.removeObserver(self, forKeyPath: kCompanyObservationKeyPath)
		remoteMezz?.removeObserver(self, forKeyPath: kLocationObservationKeyPath)
		remoteMezz?.removeObserver(self, forKeyPath: kCollaborationStateObservationKeyPath)
    remoteMezz?.removeObserver(self, forKeyPath: kOnlineObservationKeyPath)
	}

	override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
		if (context == &kRemoteMezzContext) {
			switch keyPath {
				case kNameObservationKeyPath?:
					updateNameLabel()
				case kCompanyObservationKeyPath?:
					updateCompanyLabel()
				case kLocationObservationKeyPath?:
					updateLocationLabel()
				case kCollaborationStateObservationKeyPath?:
					updateCollaborationState()
				case kOnlineObservationKeyPath?:
					updateCollaborationState()
				default:
					break
			}
		}
	}

}
