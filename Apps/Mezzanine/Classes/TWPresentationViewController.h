//
//  TWPresentationViewController.h
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 16/03/2017.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

#import "PresentationViewController.h"
#import "DeckSliderView.h"

@class PlanarWorkspaceViewController;
@class WorkspaceGeometry;

@interface TWPresentationViewController : PresentationViewController

@property (nonatomic, strong) WorkspaceGeometry *workspaceGeometry;
@property (nonatomic, weak) PlanarWorkspaceViewController *planarWorkspaceViewController;
@property (nonatomic, strong) NSArray<UIGestureRecognizer *> *tapGesturesThatShouldBeRequiredToFail;

- (void)updateLayout;

@end
