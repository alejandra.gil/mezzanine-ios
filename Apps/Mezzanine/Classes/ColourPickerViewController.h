//
//  ColourPickerViewController.h
//  Mezzanine
//
//  Created by Zai Chang on 11/1/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ColourPickerViewController : UIViewController
{
  NSInteger numberOfRows;
  NSInteger numberOfColumns;
  NSMutableArray *colours;
  
  UIScrollView *scrollView;
  UIView *contentView;
}

@property (nonatomic, strong) UIColor *selectedColour;
@property (nonatomic, copy) void (^selectedColourChanged)(UIColor *newColor);

@end
