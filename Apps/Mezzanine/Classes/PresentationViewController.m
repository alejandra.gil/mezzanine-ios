//
//  PresentationViewController.m
//  Mezzanine
//
//  Created by Zai Chang on 10/20/10.
//  Copyright 2010 Oblong Industries. All rights reserved.
//

#import "PresentationViewController.h"
#import "PresentationViewController_Private.h"
#import "MezzanineMainViewController.h"
#import "MZCommunicator.h"
#import "MezzanineStyleSheet.h"
#import "AssetViewController.h"
#import "NSIndexSetEnumerator.h"
#import "MZDownloadManager.h"
#import "NSObject+BlockObservation.h"
#import "UIImage+Resizing.h"
#import "NSString+VersionChecking.h"
#import "ButtonMenuViewController.h"
#import "UIViewController+FPPopover.h"
#import "Mezzanine-Swift.h"


// KVO Observaion Contexts
static void *kDocumentObservationContext = @"kDocumentObservationContext";
static void *kSlideObservationContext = @"kSlideObservationContext";


static CGFloat kSlidesContentLayerMinimumScale = 0.33;


@implementation PresentationViewController

@synthesize communicator;
@synthesize presentation;

@synthesize workspaceViewController;

@synthesize feldIndex;

@synthesize currentSlideIndex;
@synthesize sliderIsBeingDragged;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]))
    {
      _slideLayers = [[NSMutableArray alloc] init];
      feldIndex = 1;
      _performWithAnimations = NO;
      _slidesContentLayerScale = 1.0;
    }
    return self;
}


- (void)dealloc 
{
  DLog(@"%@ dealloc", [self class]);
  
  self.presentation = nil;

  [self removeAllSlideLayers];
 
  _slideLayers = nil;
}

- (void)setWorkspaceViewController:(WorkspaceViewController *)aWorkspaceViewController
{
  workspaceViewController = aWorkspaceViewController;
  _contentScrollView = aWorkspaceViewController.contentScrollView;
  _deckSliderView = aWorkspaceViewController.deckSliderView;
}

- (void)setWorkspace:(MZWorkspace *)workspace
{
  if (_workspace != workspace)
  {
    if (_workspace)
    {
      for (id observer in _workspaceObservers)
        [_workspace removeObserverWithBlockToken:observer];
      
      _workspaceObservers = nil;
    }
    
    _workspace = workspace;
    
    [self setPresentation: _workspace.presentation];

    if (_workspace)
    {
      _workspaceObservers = [@[] mutableCopy];
      __weak typeof(self) __self = self;

      // Observing the change of workspace loading we trigger to perform animations or not to avoid
      // weird animations when the new feld configuration is different than the old one
      // Setting performWithAnimations here makes layout changes without animations
      id observer = [_workspace addObserverForKeyPath:@"isLoading"
                                              options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld)
                                              onQueue:[NSOperationQueue mainQueue]
                                                 task:^(id obj, NSDictionary *change) {
                                                   _performWithAnimations = __self.workspace.isLoading;
                                                 }];
      
      [_workspaceObservers addObject:observer];
    }
  }
}


- (void)setSystemModel:(MZSystemModel *)systemModel
{
  if (_systemModel != systemModel)
  {
    if (_systemModel)
    {
      for (id observer in _systemModelObservers)
        [_systemModel removeObserverWithBlockToken:observer];

      _systemModelObservers = nil;
    }

    _systemModel = systemModel;

    if (_systemModel)
    {
      _systemModelObservers = [NSMutableArray array];
      id observer;
      __weak typeof(self) __self = self;

      observer = [_systemModel addObserverForKeyPath:@"infopresence.status"
                                             options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld)
                                             onQueue:[NSOperationQueue mainQueue]
                                                task:^(id obj, NSDictionary *change) {
                                                  NSNumber *oldValue = [change valueForKey:NSKeyValueChangeOldKey];
                                                  NSNumber *newValue = [change valueForKey:NSKeyValueChangeNewKey];

                                                  // Update only when Infopresence status changes
                                                  if ([oldValue isEqualToNumber:newValue])
                                                    return;

                                                  // Bug 14703 Video slides keep the old livestream when calling to another workspace with video slides
                                                  [__self loadAllSlides];
                                                  [__self layoutSlideLayersAnimated:NO];
                                                  [__self goToSlideAtIndex:__self.presentation.currentSlideIndex];
                                                  [__self setPresentationModeActive:__self.presentation.active animated:NO];
                                                }];
      [_systemModelObservers addObject:observer];
      
      // Observing the change of felds to disable any animation related to the felds
      // Setting performWithAnimations here makes layout changes without animations
      observer = [_systemModel addObserverForKeyPath:@"felds"
                                             options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld)
                                             onQueue:[NSOperationQueue mainQueue]
                                                task:^(id obj, NSDictionary *change) {
                                                  _performWithAnimations = NO;
                                                }];
      [_systemModelObservers addObject:observer];
    }
  }
}


- (void)setPresentation:(MZPresentation *)newPresentation
{
  if (presentation != newPresentation)
  {
    if (presentation)
    {
      [self endObservingDocument:presentation];
      
      // This cleanup also serves to remove all observers attached to SlideLayers
      [self removeAllSlideLayers];
    }
    
    presentation = newPresentation;
    
    if (presentation)
    {
      [self beginObservingDocument:presentation];
      
      if (![SettingsFeatureToggles sharedInstance].teamworkUI)
      {
        [self loadAllSlides];
        [self layoutSlideLayersAnimated:YES];
        [self goToSlideAtIndex:presentation.currentSlideIndex];
        [self setPresentationModeActive:presentation.active animated:YES];
      }
      else
      {
        [self loadAllSlides];
        [self setPresentationModeActive:presentation.active animated:NO];

      }
    }
  }
}


- (void)viewDidLoad 
{
  DLog(@"PresentationViewController viewDidLoad");

  [super viewDidLoad];

  self.view.backgroundColor = [MezzanineStyleSheet sharedStyleSheet].grey20Color;
  
  // To prevent slides outside the bounds to be drawn during view transitions
  // see bug 2675
  self.view.clipsToBounds = YES;
  self.view.accessibilityIdentifier = @"PresentationView";
  
  _slidesContainerLayer = [CALayer layer];
  [self.view.layer addSublayer:_slidesContainerLayer];
  
  
  feldBoundViewsContainer = [[UIView alloc] initWithFrame:self.view.bounds];
  feldBoundViewsContainer.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
  feldBoundViewsContainer.userInteractionEnabled = NO;
  feldBoundViewsContainer.layer.zPosition = -1.0;
  feldBoundViewsContainer.accessibilityIdentifier = @"PresentationView.FeldContainer";
  feldBoundViewsContainer.isAccessibilityElement = NO;
  [self.view addSubview:feldBoundViewsContainer];
  
  // Finally load all the slides if data is available,
  // which is the case if memory warning caused the view to unload and
  // it is now reloading.
  if (![SettingsFeatureToggles sharedInstance].teamworkUI && presentation)
    [self loadAllSlides];
}


- (void)didReceiveMemoryWarning
{
  DLog(@"PresentationViewController didReceiveMemoryWarning");
  
  // Releases the view if it doesn't have a superview.
  [super didReceiveMemoryWarning];
  
  // Release any cached data, images, etc that aren't in use.
  // Nimbus automatically releases all the memory cached images
  // when receiving a memory warning.

  // Go through and unload slide images for slides that aren't visible
  for (SlideLayer *slideLayer in _slideLayers)
  {
    if (![self isSlideLayerVisible:slideLayer])
    {
      slideLayer.slideImage = nil;
      slideLayer.slideImageNeedsRefresh = YES;
    }
  }
}


- (void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];

  // Felds (background) are added here and not in viewDidLoad
  // to give some time to the main view to get framed correctly
  // otherwise it gets animated
  [self refreshFeldBoundViews];
}


- (void)viewDidAppear:(BOOL)animated
{
  [super viewDidAppear:animated];

  if (![SettingsFeatureToggles sharedInstance].teamworkUI)
    [self loadAllSlides];

  if ([SettingsFeatureToggles sharedInstance].presentationItemTap)
  {
    presentationTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewPortSingleTap:)];
    presentationTapGesture.numberOfTapsRequired = 1;
    presentationTapGesture.numberOfTouchesRequired = 1;
    presentationTapGesture.delegate = self;
    [self.view.superview addGestureRecognizer:presentationTapGesture];
  }

  [[NSOperationQueue mainQueue] addOperationWithBlock:^{
    _performWithAnimations = YES;
  }];
}


- (void)viewDidUnload 
{
  DLog(@"PresentationViewController viewDidUnload");
  
  // Ran into a release order problem here. Was relying on the dealloc
  // message
  // but by that time that function was called, the block was referring to 
  /*
  if ([self.view isKindOfClass:[DeckView class]])
  {
    DeckView *deckView = (DeckView*)self.view;
    deckView.frameChanged = nil;
  }
   */

  _slidesContainerLayer = nil;
  self.workspaceViewController = nil;
  
  [self removeAllSlideLayers];
  
  // Doing super's viewDidUnload last seems to work a lot smoother
  [super viewDidUnload];
}


- (void)refreshLayout
{
  if (!sliderIsBeingDragged)
    [self layoutSlideLayersAnimated:NO];

  void (^animations)() = ^{
    [self layoutFeldBoundViews];
  };
  
  // Doing this non-animatedly seems to help with avoiding an unwanted animation the first load
  if (_performWithAnimations)
  {
    [UIView animateWithDuration:1.0/3.0 animations:animations];
  }
  else
  {
    animations();
  }
  
  [self revealCurrentSlide];
}


- (void)viewDidLayoutSubviews
{
  if (![SettingsFeatureToggles sharedInstance].teamworkUI)
    [self refreshLayout];
}


#pragma mark - Coordinates transforms

- (CGRect)rectInViewCoordinates:(CGRect)rectInSlidesCoordinates
{
  return [_slidesContainerLayer convertRect:rectInSlidesCoordinates toLayer:self.view.layer];
}

- (CGRect)rectInSlidesCoordinates:(CGRect)rectInViewCoordinates
{
  return [self.view.layer convertRect:rectInViewCoordinates toLayer:_slidesContainerLayer];
}

- (CGPoint)pointInViewCoordinates:(CGPoint)pointInSlidesCoordinates
{
  return [_slidesContainerLayer convertPoint:pointInSlidesCoordinates toLayer:self.view.layer];
}

- (CGPoint)pointInSlidesCoordinates:(CGPoint)pointInViewCoordinates
{
  return [self.view.layer convertPoint:pointInViewCoordinates toLayer:_slidesContainerLayer];
}


#pragma mark - View States

- (void)setFeldIndex:(NSInteger)index
{
  feldIndex = index;

  [self refreshFullSlideImagesWithDelay:0.0];
}


#pragma mark - Felds Support

- (CGSize)getFeldSize
{
  NSInteger numberOfFelds = self.systemModel.felds.count;
  if (numberOfFelds == 0) // Strange ...
    return CGSizeZero;
  CGSize viewSize = self.view.frame.size;
  CGSize feldSize = CGSizeMake(viewSize.width / numberOfFelds, viewSize.height);
  return feldSize;
}


// Only useful for teamwork UI, the "old" UI there's no such space represented

- (CGFloat)spaceBetweenFelds
{
  return 0.0;
}


- (CGRect)frameForFeldAtIndex:(NSInteger)index
{
  CGSize feldSize = [self getFeldSize];
  CGRect nativeFeldFrame = [self.systemModel mainFeldspace];
  CGRect effectiveLayoutArea = UIEdgeInsetsInsetRect(CGRectMake(0, 0, feldSize.width, feldSize.height), self.contentInset);
  CGRect feldFrameInView = ProportionalResizeIntoAndCenter(nativeFeldFrame, effectiveLayoutArea);
  feldFrameInView.origin.x += index * feldSize.width;
  feldFrameInView.origin.x = (NSInteger) feldFrameInView.origin.x;
  feldFrameInView.origin.y = (NSInteger) feldFrameInView.origin.y;
  feldFrameInView.size.width = (NSInteger) feldFrameInView.size.width;
  feldFrameInView.size.height = (NSInteger) feldFrameInView.size.height;
  return feldFrameInView;
}


- (CGFloat)feldBorderWidth
{
  return (1.0 / self.workspaceViewController.contentScrollView.zoomScale) / [UIScreen mainScreen].scale;  // Exactly one pixel at every zoom scale
}


- (void)refreshFeldBoundViews
{
  if ([SettingsFeatureToggles sharedInstance].teamworkUI) {
    return;
  }
  
  for (UIView *subview in feldBoundViewsContainer.subviews)
    [subview removeFromSuperview];
  
  UIColor *feldBoundsColor = [UIColor colorWithWhite:0.33 alpha:1.0];

  // Add feld bound views as a hint to users that they're looking at the content of felds
  for (NSInteger i=0; i < self.systemModel.felds.count; i++)
  {
    CGRect frame = [self frameForFeldAtIndex:i];
    UIView *feldBoundView = [[UIView alloc] initWithFrame:frame];
    feldBoundView.tag = i;
    feldBoundView.backgroundColor = [MezzanineStyleSheet sharedStyleSheet].grey60Color;
    feldBoundView.layer.borderColor = feldBoundsColor.CGColor;
    feldBoundView.layer.borderWidth = [self feldBorderWidth];
    feldBoundView.userInteractionEnabled = NO;
    feldBoundView.accessibilityIdentifier = [NSString stringWithFormat:@"PresentationView.Feld"];
    feldBoundView.isAccessibilityElement = NO;

    [feldBoundViewsContainer addSubview:feldBoundView];
  }
  
  [self.view setNeedsLayout];
}


- (void)layoutFeldBoundViews
{
  NSArray *subviews = feldBoundViewsContainer.subviews;
  for (UIView *feldBoundView in subviews)
  {
    CGRect frame = [self frameForFeldAtIndex:feldBoundView.tag];
    frame = CGRectInset(frame, 1.0, 1.0);
    feldBoundView.frame = frame;
  }
  
  [self updateFeldBoundViews];
}


- (void)updateFeldBoundViews
{
  NSArray *subviews = feldBoundViewsContainer.subviews;

  // In a mixed geometry workspace some felds are not visible to all users
  BOOL mixedGeometryWorkspace = NO;
  for (MZFeld *feld in self.systemModel.felds)
  {
    if (!feld.visibleToAll)
      mixedGeometryWorkspace = YES;
  }

  for (UIView *feldBoundView in subviews)
  {
    if (feldBoundView.tag >= self.systemModel.felds.count)
      continue;
    
    MZFeld *feld = (self.systemModel.felds)[feldBoundView.tag];
    if (feld)
    {
      BOOL isSharedFeld = (self.systemModel.infopresence.status == MZInfopresenceStatusActive || mixedGeometryWorkspace) && feld.visibleToAll;
      feldBoundView.layer.borderColor = isSharedFeld ? MezzanineStyleSheet.sharedStyleSheet.feldBorderInCollaborationColor.CGColor :
                                                       MezzanineStyleSheet.sharedStyleSheet.feldMutedBorderColor.CGColor;
      
      feldBoundView.layer.borderWidth = [self feldBorderWidth];
    }
  }
}


#pragma mark - Slide Layers Management

static CGFloat kSlideMarginX = 12.0;

- (CGRect)frameForSlideAtIndex:(NSInteger)index
{
  CGSize feldSize = [self getFeldSize];
  CGFloat x = feldSize.width * index;
  CGFloat y = 0;
  CGFloat viewAspectRatio = feldSize.width / feldSize.height;
  CGRect mainFeldSpace = self.systemModel.mainFeldspace;
  CGFloat aspectRatio = mainFeldSpace.size.width / mainFeldSpace.size.height;
  CGRect slideArea = CGRectMake(x, y, feldSize.width, feldSize.height);
  CGRect pageArea = (aspectRatio > viewAspectRatio) ? 
        CGRectMake(x, y, feldSize.width, feldSize.width/aspectRatio) :
        CGRectMake(x, y, feldSize.height * aspectRatio, feldSize.height);
  
  CGSize slideMargin = CGSizeMake(kSlideMarginX, kSlideMarginX/aspectRatio);
  CGRect frame = CGRectInset(ProportionalResizeIntoAndCenter(pageArea, slideArea), slideMargin.width, slideMargin.height);
  frame = UIEdgeInsetsInsetRect(frame, self.contentInset);  // Take into account contentInset
  frame.origin.x = (NSInteger) frame.origin.x;
  frame.origin.y = (NSInteger) frame.origin.y;
  frame.size.width = (NSInteger) frame.size.width;
  frame.size.height = (NSInteger) frame.size.height;
  return frame;
}

- (CGRect)frameForSlidesArea
{
  CGSize viewSize = self.view.frame.size;
  CGFloat width = viewSize.width * presentation.numberOfSlides;
  return CGRectMake(0, 0, width, viewSize.height);
}

- (SlideLayer*)layerForSlide:(MZPortfolioItem*)slide
{
  NSUInteger count = [_slideLayers count];
  for (int i=0; i<count; i++)
  {
    SlideLayer *sublayer = _slideLayers[i];
    if (sublayer.slide == slide)
      return sublayer;
  }
  return nil;
}

- (void)layoutSlideLayersAnimated:(BOOL)animated
{
  
  // Update due to 19364.
  // Right now, there is no unwanted [CATransaction commit].
  // You can check it by not committing the transaction and check it completion block it's called (it's not).
  //
  // Even if we don't want it animated the CATransaction was started and committed or not based on the flag.
  // If we don't commit the app freezes because the transaction is started but not committed and it's in the main thread.
  // The fix is to only start the CATransaction when we want it animated.
  
  // OLD STATUS
  // The animation of th slide layout is hacky in order because for some reason
  // still to be discovered when the CATransaction gets commit while the presentation
  // is being zoomed in or out the set of slides doesn't animate smoothly
  // As explained below, some other transaction might be commiting this one at the right time
  // letting the animation work fine. But as observed in bug 14795, the app can get frozen
  // in some cases that this animation is not committed.
  // For this reason, that aanimated flag is only used to confirm and close the animation
  // when requested to do so.

  //DLog(@"PresentationViewController layoutSlideLayers. View frame is %@", NSStringFromCGRect(self.view.frame));
  
  void (^slideUpdate)() = ^{
    for (MZPortfolioItem *item in presentation.slides)
    {
      // Keeping for posterity a bit, in case presentation interaction returns. In the case of a slide that's involved in a pending transaction, it should have a custom transform so don't mess it up
      //    NSArray *pending = [model pendingTransactionsForSlide:item];
      //    if (pending.count > 0)
      //      continue;
      
      SlideLayer *slideLayer = [self layerForSlide:item];
      
      CGRect targetFrame = [self frameForSlideAtIndex:item.index];
      if (!CGRectEqualToRect(slideLayer.frame, targetFrame))  // Don't adjust frame (and trigger animation) unnecessarily
      {
        slideLayer.frame = targetFrame;
      }
      
      [slideLayer setContainerZoomScale:[_contentScrollView zoomScale]];
    }
  };
  
  
  if (!animated)
  {
    slideUpdate();
  }
  else
  {
    [CATransaction begin];
    DLog(@"Started CATransaction");

    [CATransaction setValue:@(1.0/3.0) forKey:kCATransactionAnimationDuration];
    
    // OLD STATUS
    // Check Bug 12246
    // Using a different animation timing function the slide layers transitions gets adjusted almost perfectly
    // to what the background feld and the windshield items transition is.
    [CATransaction setAnimationTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    slideUpdate();
    
    // OLD STATUS
    // if the transaction is correctly set, the animation behaves weirdly
    // if it's not set as a transaction, the animation doesn't work either
    // Strangely, leaving it without commit, works.
    // Our guess is that some external agent commits it for us
    // To check that, set a CATransaction completionBlock and see that it is called.
    
    [CATransaction setCompletionBlock:^{
      DLog(@"Finished CATransaction");
    }];
    
    [CATransaction commit];
  }
}


- (void)loadAllSlides
{
  DLog(@"loadAllSlides view frame is %@", NSStringFromCGRect(self.view.frame));
  
  [self removeAllSlideLayers];
  
  NSArray *slides = presentation.slides;
  for (NSInteger i=0; i<[slides count]; i++)
  {
    [self insertLayerForSlide:slides[i] atIndex:i];
  }
}

- (void)insertLayerForSlide:(MZPortfolioItem*)slide atIndex:(NSUInteger)index
{
  SlideLayer *slideLayer = [SlideLayer layer];
  slideLayer.frame = [self frameForSlideAtIndex:index];
  [_slidesContainerLayer insertSublayer:slideLayer atIndex:(unsigned)index];
  
  slide.index = index;
  slideLayer.slide = slide;
  slideLayer.contentScale = 1.0;

  [_slideLayers insertObject:slideLayer atIndex:index];
  // TODO - Move observation code into SlideLayer
  [slide addObserver:self forKeyPath:@"index" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:kSlideObservationContext];
  [slide addObserver:self forKeyPath:@"thumbURL" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld | NSKeyValueObservingOptionInitial) context:kSlideObservationContext];
  [slide addObserver:self forKeyPath:@"fullURL" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld | NSKeyValueObservingOptionInitial) context:kSlideObservationContext];
  [slide addObserver:self forKeyPath:@"imageAvailable" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:kSlideObservationContext];
  [slide addObserver:self forKeyPath:@"contentSource" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:kSlideObservationContext];

  [self updateSlideLayerVisibility:slideLayer animated:NO];

  //DLog(@"Slide Layer at index %d has frame %@", index, NSStringFromCGRect(slideLayer.frame));
}

- (void)removeSlideLayer:(SlideLayer*)slideLayer
{
  id slide = slideLayer.slide;
  if (slide && slide != (id)[NSNull null])
  {
    /*
    id observerToken = [slideLayer valueForKey:@"observerToken"];
    if (observerToken)
    {
      [[NSNotificationCenter defaultCenter] removeObserver:observerToken];
    }
     */
    
    [slide removeObserver:self forKeyPath:@"thumbURL"];
    [slide removeObserver:self forKeyPath:@"fullURL"];
    [slide removeObserver:self forKeyPath:@"imageAvailable"];
    [slide removeObserver:self forKeyPath:@"index"];
    [slide removeObserver:self forKeyPath:@"contentSource"];

    slideLayer.slide = nil;
    slideLayer.liveStream = nil;
  }

  [slideLayer removeFromSuperlayer];
  [_slideLayers removeObject:slideLayer];
}

- (void)removeLayerForSlide:(MZPortfolioItem*)slide
{
  SlideLayer *slideLayer = [self layerForSlide:slide];
  if (slideLayer)
  {
    [self removeSlideLayer:slideLayer];
  }
}

- (void)removeAllSlideLayers
{
  NSArray *slideLayersToRemove = [_slideLayers copy];
  for (SlideLayer *layer in slideLayersToRemove)
    [self removeSlideLayer:layer];
}

- (BOOL)isSlideLayerVisible:(CALayer*)layer
{
  CGRect viewRect = self.view.layer.bounds;
  CGRect layerRect = [self rectInViewCoordinates:layer.frame];
  return CGRectGetMinX(layerRect) > CGRectGetMinX(viewRect) && CGRectGetMaxX(layerRect) < CGRectGetMaxX(viewRect);
}

- (BOOL)isSlideLayerVisibleOrNextToVisible:(CALayer*)layer
{
  CGRect viewRect = self.view.layer.bounds;
  CGRect layerRect = [self rectInViewCoordinates:layer.frame];

  // Extend the window to search depending on the pushback state of the deck
  CGFloat extensionWidth = viewRect.size.width * 5.0;
  viewRect.origin.x -= extensionWidth;
  viewRect.size.width += extensionWidth * 2.0;

  return CGRectGetMinX(layerRect) > CGRectGetMinX(viewRect) && CGRectGetMaxX(layerRect) < CGRectGetMaxX(viewRect);
}

- (SlideLayer*)slideLayerAtViewPosition:(CGPoint)position
{
  //position = [self.view.layer convertPoint:position toLayer:slidesContainerLayer];
  CALayer *hitLayer = [_slidesContainerLayer hitTest:position];
  if (hitLayer && [hitLayer isKindOfClass:[SlideLayer class]])
    return (SlideLayer*)hitLayer;
  return nil;
}


#pragma mark - Presentation Mode

- (void)setPresentationModeActive:(BOOL)active animated:(BOOL)animated
{
  for (SlideLayer *slideLayer in _slideLayers)
  {
    [self updateSlideLayerVisibility:slideLayer animated:animated];
  }

  _slidesContentLayerScale = active ? 1.0 : kSlidesContentLayerMinimumScale;
  [self updateSlideContainerTransformAnimated:animated];
}


- (void)updateSlideLayerVisibility:(SlideLayer*)slideLayer animated:(BOOL)animated
{
  BOOL visible = presentation.active;
  slideLayer.hidden = !visible;
  
  if (![SettingsFeatureToggles sharedInstance].teamworkUI)
    [slideLayer setIndexLayerVisible:visible];
  else
    [slideLayer setIndexLayerVisible:false];
}



#pragma mark - Slide Container Transform

- (CGFloat)slideContainerOffsetForSlideIndex:(NSInteger)slideIndex
{
  NSInteger indexOfCenterFeld = self.systemModel.indexOfCenterFeld;
  CGSize feldSize = [self getFeldSize];
  CGFloat offsetX = indexOfCenterFeld * feldSize.width - (feldSize.width * slideIndex);
  return offsetX;
}

- (CATransform3D)slideContainerTransformWithScale:(CGFloat)scale
{
  CGFloat offsetX = [self slideContainerOffsetForSlideIndex:currentSlideIndex] + slideContainerTransformNudge.x;
  CGFloat offsetY = 0.0;

  if (scale != 1.0)
  {
    CGSize feldSize = [self getFeldSize];
    offsetX += feldSize.width * [self.systemModel.felds count];
    offsetY = feldSize.height;
  }

  CATransform3D t = CATransform3DIdentity;
  t = CATransform3DScale(t, scale, scale, 1.0);
  t = CATransform3DTranslate(t, offsetX, offsetY, 0.0);

  return t;
}


- (CATransform3D)slideContainerTransform
{
  return [self slideContainerTransformWithScale:_slidesContentLayerScale];
}


- (void)updateSlideContainerTransformAnimated:(BOOL)animated
{
  [CATransaction begin];

  if (animated)
  {
    [CATransaction setAnimationDuration:0.33];
    [CATransaction setAnimationTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
  }
  else
  {
    [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
  }
  
  CATransform3D transform = [self slideContainerTransform];
  _slidesContainerLayer.transform = transform;
  
  // Check if any of the slide layers need to load its image
  if (!sliderIsBeingDragged && !deckPanInProgress && ![_deckSliderView isTracking])
    [self updateSlideLayerImages];
  
  [CATransaction commit];
}


- (void)updateSlideContainerTransform
{
  [self updateSlideContainerTransformAnimated:YES];
}


// Re-determines which slides needs to load full image based on current slide index
- (void)refreshFullSlideImagesWithDelay:(CGFloat)delay
{
  NSAssert(_delegate, @"delegate can't be nil");

  __weak typeof(self) __self = self;
  
  void (^refreshFullSlideImage)(SlideLayer*) = ^(SlideLayer *slideLayer) {
    if (slideLayer && !slideLayer.isDisplayingFullImage && [__self shouldDisplayFullImageForLayer:slideLayer])
    {
      if (delay > 0)
      {
        // Delay loading of full image because it makes scrolling a bit jerky
        SEL selector = @selector(loadFullImageForSlideLayer:);
        [NSObject cancelPreviousPerformRequestsWithTarget:__self selector:selector object:slideLayer];
        [__self performSelector:selector withObject:slideLayer afterDelay:delay];
      }
      else
      {
        [__self loadFullImageForSlideLayer:slideLayer];
      }
    }
  };

  NSMutableArray *layersToUnload = [_slideLayers mutableCopy];
  NSMutableArray *layersToLoad = [[NSMutableArray alloc] init];

  NSInteger numberOfFelds = self.systemModel.felds.count;

  // Single Felds are always zoomed, so calculate it as zoomed
  if ([[UIDevice currentDevice] isIPad] && (numberOfFelds == 1 || [_delegate presentationViewControllerZoomState]))
  {
    NSInteger margin = ceil((numberOfFelds - 1.0) / 2.0);

    for (NSInteger ix = currentSlideIndex - margin ; ix <= currentSlideIndex + margin ; ix ++)
    {
      if (ix < 0 || ix >= presentation.slides.count)
        continue;

      MZPortfolioItem *slide = [presentation objectInSlidesAtIndex:ix];
      SlideLayer *slideLayer = [self layerForSlide:slide];
      if (slideLayer)  // Full slide image is only available when in presentation mode
        [layersToLoad addObject:slideLayer];
    }
  }
  else
  {
    // Currently displayed slide index depends on which feld the user is viewing
    NSInteger slideIndex = currentSlideIndex + (feldIndex - 1);

    if (presentation.slides.count == 0 || slideIndex < 0 || slideIndex >= presentation.slides.count)
      return;

    MZPortfolioItem *slide = [presentation objectInSlidesAtIndex:slideIndex];
    SlideLayer *slideLayer = [self layerForSlide:slide];
    if (slideLayer)  // Full slide image is only available when in presentation mode
      [layersToLoad addObject:slideLayer];
  }
  
  for (SlideLayer *slideLayer in layersToLoad)  // Full slide image is only available when in presentation mode
  {
    refreshFullSlideImage(slideLayer);
    [layersToUnload removeObject:slideLayer];
  }
  
  // TODO - Also load 
  
  // Unload any previous full slide images
  for (SlideLayer *layer in layersToUnload)
  {
    [self unloadFullImageForSlideLayer:layer];
  }
}


- (BOOL)revealSlideAtIndex:(NSInteger)index
{
  if (index < 0 && index >= presentation.slides.count)
    return NO;
  
  [self updateSlideContainerTransform];
  
  [self refreshFullSlideImagesWithDelay:0.15];
  return YES;
}

- (void)revealCurrentSlide
{
  [self revealSlideAtIndex:currentSlideIndex];
}



#pragma mark -
#pragma mark Slide Image Loading

- (void)loadImageForSlide:(MZItem*)slide inLayer:(SlideLayer*)slideLayer
{
  if (slide.isVideo)
  {
    // Handle image for a video slide
    MZLiveStream *liveStream = [_systemModel.currentWorkspace liveStreamWithUid:slide.contentSource];
    slideLayer.liveStream = liveStream;
    return;
  }
  
  if (slideLayer.slideImage && !slideLayer.slideImageNeedsRefresh)
    return;
  
  if (!slide.thumbURL)
    return;
  
  // This prevents to download the image if it is not available (avoid having 404 at MZDownloader)
  if (!slide.imageAvailable)
    return;
  
#if __DEBUG__
  if (self.systemModel.testModeEnabled && slide.image)
  {
    slideLayer.slideImage = slide.image;
    slideLayer.slideImageNeedsRefresh = NO;
    return;
  }
#endif
  
  NSURL *url = [communicator urlForResource:slide.thumbURL];
  if (url == nil)
    return;
  
//  DLog(@"Loading thumbnail for slide %@", slide);
  
  // Set the flag to false here to prevent additional transform updates
  // from triggering another thread
  slideLayer.slideImageNeedsRefresh = NO;

  __weak typeof (slideLayer) __slideLayer = slideLayer;

  [[MZDownloadManager sharedLoader] loadImageWithURL:url
                                             atTarget:slideLayer
                                              success:^(UIImage *image) {
                                                SlideLayer *strongSlideLayer = __slideLayer;
                                                strongSlideLayer.slideImage = image;
                                              }
                                                error:^(NSError *error) {
                                                  SlideLayer *strongSlideLayer = __slideLayer;
                                                  strongSlideLayer.slideImageNeedsRefresh = YES;
                                                }];
}


- (void)updateSlideLayerImages
{
  //DLog(@"updateSlideLayerImages");
  
  for (SlideLayer *slideLayer in _slideLayers)
  {
    if (!slideLayer.slide)
      continue;
    
    if ((slideLayer.slideImage == nil || slideLayer.slideImageNeedsRefresh) && [self isSlideLayerVisibleOrNextToVisible:slideLayer])
    {
      [self loadImageForSlide:slideLayer.slide inLayer:slideLayer];
    }
  }
}


// Determination of whether a slide layer should request its full image
- (BOOL)shouldDisplayFullImageForLayer:(SlideLayer*)slideLayer
{
  
  NSAssert(_delegate, @"delegate can't be nil");
  
  MZPortfolioItem *item = (MZPortfolioItem *)slideLayer.slide;
  if (item.isVideo)
    return NO;

  if ([[UIDevice currentDevice] isIPad])
  {
    NSInteger numberOfFelds = self.systemModel.felds.count;
    NSInteger margin = ceil((numberOfFelds - 1.0) / 2.0);

    // Single Felds are always zoomed, so calculate it as zoomed
    if (numberOfFelds == 1 || [_delegate presentationViewControllerZoomState])
    {
      // Display all the visible slides in the triptych
      if (((NSInteger)item.index < currentSlideIndex - margin) || ((NSInteger)item.index > currentSlideIndex + margin))
        return NO;
    }
    else
    {
      // Currently displayed slide index depends on which feld the user is viewing
      NSInteger slideIndex = currentSlideIndex + (feldIndex - 1);
      if (item.index != slideIndex)
        return NO;
    }
  }
  else
  {
    // Currently displayed slide index depends on which feld the user is viewing
    NSInteger slideIndex = currentSlideIndex + (feldIndex - 1);
    if (item.index != slideIndex)
      return NO;

    if ([_delegate presentationViewControllerZoomState])
      return NO;
  }

  return YES;
}

- (void)loadFullImageForSlideLayer:(SlideLayer*)slideLayer
{
  // Load the full resolution image only if user manipulations aren't happening as it will result
  // in jittery user interaction
  if (deckPanInProgress || [_deckSliderView isTracking])
    return;
  
  // Layer is already displaying the full image
  if (slideLayer.isDisplayingFullImage)
    return;
  
  // Not a layer that should display full image
  if (![self shouldDisplayFullImageForLayer:slideLayer])
    return;
  
  MZPortfolioItem *slide = (MZPortfolioItem *)slideLayer.slide;

  // The slide's image is not available on the server yet (mid-transfer)
  if (!slide.imageAvailable)
    return;

  NSURL *url = [communicator urlForResource:slide.fullURL];
  if (url == nil)
    return;

  __weak typeof (self) __self = self;
  __weak typeof (slideLayer) __slideLayer = slideLayer;

  slideLayer.isLoadingFullImage = YES;
  [[MZDownloadManager sharedLoader] loadImageWithURL:url
                                             atTarget:slideLayer
                                              success:^(UIImage *image) {
                                                PresentationViewController *strongSelf = __self;
                                                SlideLayer *strongSlideLayer = __slideLayer;
                                                if ([strongSelf shouldDisplayFullImageForLayer:strongSlideLayer])
                                                  [strongSlideLayer setFullSlideImage:image];
                                              }
                                                error:^(NSError *error) {
                                                  SlideLayer *strongSlideLayer = __slideLayer;
                                                  strongSlideLayer.slideImageNeedsRefresh = YES;
                                                }];
}


- (void)unloadFullImageForSlideLayer:(SlideLayer*)slideLayer
{
  if (slideLayer.isDisplayingFullImage || slideLayer.isLoadingFullImage)
  {
    [slideLayer setFullSlideImage:nil];
    NSURL *url = [communicator urlForResource:((MZSlide *)slideLayer.slide).fullURL];
    [[MZDownloadManager sharedLoader] unloadImageFromMemoryCacheWithURL:url];
    [[MZDownloadManager sharedLoader] cancelLoadingURLForTarget:slideLayer];
  }
}


#pragma mark - Slide Actions

- (void)nextSlide
{
  //NSLog(@"PresentationViewController viewportLeftSwiped");
  
  if ([presentation numberOfSlides] == 0)
    return;
  
  if (currentSlideIndex >= presentation.numberOfSlides-1)
    return;
  
  currentSlideIndex++;
  [communicator.requestor requestPresentationScrollRequest:_systemModel.currentWorkspace.uid currentIndex:currentSlideIndex];
  [self revealCurrentSlide];
  
  if ([_delegate respondsToSelector:@selector(presentationViewUpdateWorkspaceViewControllerDeckSlider)]) {
    [_delegate presentationViewUpdateWorkspaceViewControllerDeckSlider];
  }
  
  // Analytics
  MezzanineAnalyticsManager *analyticsManager = [MezzanineAnalyticsManager sharedInstance];
  [analyticsManager tagEvent:analyticsManager.scrollPresentationEvent attributes:@{ analyticsManager.sourceKey : analyticsManager.swipeAttribute}];
}

- (void)previousSlide
{
  //NSLog(@"PresentationViewController viewportRightSwiped");
  
  if ([presentation numberOfSlides] == 0)
    return;
  
  if (currentSlideIndex <= 0)
    return;

  currentSlideIndex--;
  [communicator.requestor requestPresentationScrollRequest:_systemModel.currentWorkspace.uid currentIndex:currentSlideIndex];
  [self revealCurrentSlide];
  
  if ([_delegate respondsToSelector:@selector(presentationViewUpdateWorkspaceViewControllerDeckSlider)]) {
    [_delegate presentationViewUpdateWorkspaceViewControllerDeckSlider];
  }
  
  // Analytics
  MezzanineAnalyticsManager *analyticsManager = [MezzanineAnalyticsManager sharedInstance];
  [analyticsManager tagEvent:analyticsManager.scrollPresentationEvent attributes:@{ analyticsManager.sourceKey : analyticsManager.swipeAttribute}];
}

- (void)goToSlideAtIndex:(NSInteger)slideIndex
{  
  if ([presentation numberOfSlides] == 0)
    return;
  
  if ((slideIndex < 0) || (slideIndex >= [presentation numberOfSlides]))
    return;
  
  currentSlideIndex = slideIndex;
  [self revealCurrentSlide];
}


- (void)openFullSlideImageForSlide:(MZItem*)slide fromLayer:(SlideLayer*)layer
{
  if (!slide || !layer)
    return;
  
  AssetViewController *controller = [[AssetViewController alloc] initWithNibName:nil bundle:nil];
  controller.appContext = [MezzanineAppContext currentContext];
  controller.workspace = _systemModel.currentWorkspace;
  controller.placeholderImage = layer.slideImage;
  controller.item = slide;
  
  UIImageView *image = [[UIImageView alloc] initWithImage:layer.slideImage];
  CGRect rect = [self.view convertRect:[self rectInViewCoordinates:layer.frame] toView:nil];
  image.frame = rect;

  [[MezzanineAppContext currentContext].mainNavController pushViewController:controller transitionView:image];
}


#pragma mark -
#pragma mark Document Observation

- (void)beginObservingDocument:(MZPresentation *)aPresentation
{
  [aPresentation addObserver:self forKeyPath:@"currentSlideIndex" options:(NSKeyValueObservingOptionInitial) context:kDocumentObservationContext];
  [aPresentation addObserver:self forKeyPath:@"slides" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:kDocumentObservationContext];
  [aPresentation addObserver:self forKeyPath:@"active" options:(NSKeyValueObservingOptionNew) context:kDocumentObservationContext];
}

- (void)endObservingDocument:(MZPresentation *)aPresentation
{
  [aPresentation removeObserver:self forKeyPath:@"currentSlideIndex"];
  [aPresentation removeObserver:self forKeyPath:@"slides"];
  [aPresentation removeObserver:self forKeyPath:@"active"];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
  if (context == kDocumentObservationContext)
  {
    if ([keyPath isEqual:@"slides"])
    {
      NSNumber *kind = change[NSKeyValueChangeKindKey];
      if ([kind integerValue] == NSKeyValueChangeInsertion)  // Rows were added
      {
        NSIndexSet *indices = change[NSKeyValueChangeIndexesKey];
        NSArray *inserted = change[NSKeyValueChangeNewKey];

        DLog(@"Slides were inserted: %@", inserted);
        
        NSIndexSetEnumerator *enumerator = [NSIndexSetEnumerator enumeratorWithIndexSet:indices];
        NSUInteger count = [inserted count];
        for (int i=0; i<count; i++)
        {
          MZPortfolioItem *slide = inserted[i];
          NSUInteger index = [enumerator indexAtIndex:i];
          
          // If a slide is pending a reorder, its corresponding layer should already
          // exist in the view and its layout simply needs to be adjusted
          if (slide.pendingReorder)
            continue;
          
          [self insertLayerForSlide:slide atIndex:index];
        }
        
        // Slides would have moved because of insertion. Refresh the frames
        [self layoutSlideLayersAnimated:YES];
        // A slide may have come into view, check for image loading
        [self updateSlideLayerImages];
        
        [_deckSliderView updateThumbWidthforSlides:[presentation numberOfSlides] andCurrentSlide:currentSlideIndex];
        
        if ([_delegate respondsToSelector:@selector(presentationViewUpdateWorkspaceViewControllerDeckSlider)]) {
          [_delegate presentationViewUpdateWorkspaceViewControllerDeckSlider];
        }
      }
      else if ([kind integerValue] == NSKeyValueChangeRemoval)  // Rows were removed
      {
        //NSIndexSet *indices = [change objectForKey:NSKeyValueChangeIndexesKey];
        NSArray *removed = change[NSKeyValueChangeOldKey];
        
        // In the case of a slide reorder, two KVO calls will be made. One of removal and one
        // of its reinsertion at the new index. In this case, we ignore the removal KVO call
        // and handle the UI update entirely when the slide is re-inserted
        if (removed.count == 1 && [removed[0] pendingReorder])
          return;

        DLog(@"Slides were deleted: %@", removed);
        
        for (MZPortfolioItem *slide in removed)
          [self removeLayerForSlide:slide];
        
        // When slides are deleted, need to reshuffle the layers
        [self layoutSlideLayersAnimated:YES];
        // A slide may have come into view, check for image loading
        [self updateSlideLayerImages];
        
        [_deckSliderView updateThumbWidthforSlides:presentation.numberOfSlides andCurrentSlide:currentSlideIndex];
        if ([_delegate respondsToSelector:@selector(presentationViewUpdateWorkspaceViewControllerDeckSlider)]) {
          [_delegate presentationViewUpdateWorkspaceViewControllerDeckSlider];
        }
      }
      else if ([kind integerValue] == NSKeyValueChangeReplacement)
      {
        DLog(@"Slides were replaced:");
      }
    }
    else if ([keyPath isEqual:@"currentSlideIndex"])
    {
      if (currentSlideIndex != presentation.currentSlideIndex)
      {
        currentSlideIndex = presentation.currentSlideIndex;
        
        [self revealCurrentSlide];
        
        if ([_delegate respondsToSelector:@selector(presentationViewUpdateWorkspaceViewControllerDeckSlider)]) {
          [_delegate presentationViewUpdateWorkspaceViewControllerDeckSlider];
        }
      }
    }
    else if ([keyPath isEqual:@"active"])
    {
      [self setPresentationModeActive:presentation.active animated:YES];
    }
  }
  
  if (context == kSlideObservationContext)
  {
    MZPortfolioItem *slide = (MZPortfolioItem*)object;
    
    //DLog(@"PresentationViewController observed change in slide %d keypath=%@", slide.index, keyPath);
    
    if ([keyPath isEqual:@"thumbURL"])
    {
      if (slide.thumbURL)
      {
        SlideLayer *slideLayer = (SlideLayer*)[self layerForSlide:slide];
        slideLayer.slideImageNeedsRefresh = YES;
        if (slideLayer && [self isSlideLayerVisibleOrNextToVisible:slideLayer])
        {
          [self loadImageForSlide:slide inLayer:slideLayer];
        }
      }
    }
    else if ([keyPath isEqual:@"fullURL"])
    {
      if (slide.fullURL)
      {
        SlideLayer *slideLayer = (SlideLayer*)[self layerForSlide:slide];
        if (slideLayer && [self shouldDisplayFullImageForLayer:slideLayer])
        {
          [self loadFullImageForSlideLayer:slideLayer];
        }
      }
    }
    else if ([keyPath isEqual:@"imageAvailable"])
    {
      SlideLayer *slideLayer = (SlideLayer*)[self layerForSlide:slide];
      if (slideLayer && [self isSlideLayerVisibleOrNextToVisible:slideLayer])
      {
        if ([self shouldDisplayFullImageForLayer:slideLayer])
        {
          [self loadFullImageForSlideLayer:slideLayer];
        }
        else
        {
          [self loadImageForSlide:slide inLayer:slideLayer];
        }
      }
    }
    else if ([keyPath isEqual:@"contentSource"])
    {
      SlideLayer *slideLayer = (SlideLayer*)[self layerForSlide:slide];
      if (slideLayer && [self isSlideLayerVisibleOrNextToVisible:slideLayer])
      {
        if ([self shouldDisplayFullImageForLayer:slideLayer])
        {
          [self loadFullImageForSlideLayer:slideLayer];
        }
        else
        {
          [self loadImageForSlide:slide inLayer:slideLayer];
        }
      }
    }
    else if ([keyPath isEqual:@"index"])
    {
      SlideLayer *slideLayer = (SlideLayer*)[self layerForSlide:slide];
      [slideLayer updateWithSlide:slide];

      CGRect targetFrame = [self frameForSlideAtIndex:slide.index];
      if (!CGRectEqualToRect(slideLayer.frame, targetFrame))  // Don't adjust frame (and trigger animation) unnecessarily
      {
        slideLayer.frame = targetFrame;
        if ([self shouldDisplayFullImageForLayer:slideLayer])
        {
          [self loadFullImageForSlideLayer:slideLayer];
        }
        else
        {
          [self loadImageForSlide:slideLayer.slide inLayer:slideLayer];
        }
      }
    }
  }
}


#pragma mark -
#pragma mark Gesture Recognizers

- (void)handleDeckPanGesture:(UIPanGestureRecognizer*)recognizer
{
  // In teamwork, we have to include the spaceBetweenFelds in the feldWidth
  CGFloat feldWidth = [self getFeldSize].width + [self spaceBetweenFelds];
  CGFloat sensitivity = feldWidth;
  CGPoint translation = [recognizer translationInView:recognizer.view];
  CGFloat state = translation.x / sensitivity;
  CGFloat maxState = 1.0;
    
  if (recognizer.state == UIGestureRecognizerStateBegan)
  {
    slideContainerTransformNudge = CGPointZero;
    
    [recognizer setTranslation:CGPointZero inView:recognizer.view];
    
    deckPanInProgress = YES;
  }
  else if (recognizer.state == UIGestureRecognizerStateChanged)
  {
    CGFloat offset = 0.0;
    if ((currentSlideIndex >= presentation.slides.count - 1 && state < 0) ||
        (currentSlideIndex <= 0 && state > 0))
    {
      state = state / fabs(state) * log10(fabs(state) + 1.0);
      offset = state * feldWidth;
    }
    else
    {
      state = MIN(MAX(state, -maxState), maxState);
      offset = state * feldWidth;
    }
    
    slideContainerTransformNudge = CGPointMake(offset, 0);
    
    [self updateSlideContainerTransformAnimated:NO];
  }
  else if (recognizer.state == UIGestureRecognizerStateEnded)
  {
    deckPanInProgress = NO;
    
    slideContainerTransformNudge = CGPointZero;
    [self updateSlideContainerTransformAnimated:YES];
    
    state = MIN(MAX(state, -maxState), maxState);
    CGPoint velocity = [recognizer velocityInView:recognizer.view];
    if (fabs(velocity.x) > 150.0)
    {
      if (velocity.x < 0.0)
        [self nextSlide];
      else
        [self previousSlide];
    }
    else if (fabs(state) > (maxState / 3.0))
    {
      if (state < 0.0)
        [self nextSlide];
      else
        [self previousSlide];
    }
    
    // Reset the translation such that the algorithm in gestureRecognizerShouldBegin would function properly
    [recognizer setTranslation:CGPointZero inView:recognizer.view];    
  }
  else if (recognizer.state == UIGestureRecognizerStateCancelled)
  {
    deckPanInProgress = NO;
    
    slideContainerTransformNudge = CGPointZero;
    [self updateSlideContainerTransformAnimated:YES];
    
    // Reset the translation such that the algorithm in gestureRecognizerShouldBegin would function properly
    [recognizer setTranslation:CGPointZero inView:recognizer.view];
    
    // Re-enable gesture in case it was disabled
    recognizer.enabled = YES;
  }
}


#pragma mark - Slide Double Tap Handling

- (void)viewportDoubleTapped:(UITapGestureRecognizer*)recognizer
{
  if (recognizer.state == UIGestureRecognizerStateRecognized)
  {
    CGPoint position = [recognizer locationInView:self.view];
    SlideLayer *slideLayer = [self slideLayerAtViewPosition:position];
    if (slideLayer)
    {
      MZPortfolioItem *slide = (MZPortfolioItem *)slideLayer.slide;
      if (slide.fullURL != nil && slideLayer.slideImage != nil)
      {
        [self openFullSlideImageForSlide:slide fromLayer:slideLayer];
      }
    }
  }
}


- (void)viewPortSingleTap:(UITapGestureRecognizer *)recognizer
{
  CGPoint position = [recognizer locationInView:self.view];
 
  // 18520. Menu should display only if the tap is within the frame of the view, not outside (view is clipped)
  CGPoint pointInSuperview =  [self.view convertPoint:position toView:self.view.superview];
  if (!CGRectContainsPoint(self.view.frame, pointInSuperview)) {
    return;
  }
  
  SlideLayer *slideLayer = [self slideLayerAtViewPosition:position];
  if (slideLayer)
  {
    ButtonMenuViewController *menuViewController = [[ButtonMenuViewController alloc] initWithNibName:nil bundle:nil];
    NSMutableArray *menuItems = [NSMutableArray array];
    
    [menuItems addObject:[ButtonMenuItem itemWithTitle:NSLocalizedString(@"Live Stream Item Menu View", nil) action:^{
      
      MZItem *slide = nil;
      if (slideLayer.liveStream)
        slide = (MZLiveStream *)slideLayer.liveStream;
      else
        slide = (MZPortfolioItem *)slideLayer.slide;
      
      [self openFullSlideImageForSlide:slide fromLayer:slideLayer];
      [self.currentPopover dismissPopoverAnimated:NO];
    }]];
    
    ButtonMenuItem *deleteItem = [ButtonMenuItem itemWithTitle:NSLocalizedString(@"Portfolio Item Menu Delete", nil) action:^{
      [self.communicator.requestor requestPortfolioItemDeleteRequest:_systemModel.currentWorkspace.uid uid:slideLayer.slide.uid];
      [self.currentPopover dismissPopoverAnimated:NO];
    }];
    
    deleteItem.buttonColor = [MezzanineStyleSheet sharedStyleSheet].redHighlightColor;
    
    [menuItems addObject:deleteItem];
    menuViewController.menuItems = menuItems;
    
    FPPopoverController *popover = [[FPPopoverController alloc] initWithContentViewController:menuViewController delegate:self];
    popover.arrowDirection = (FPPopoverArrowDirectionVertical);
    popover.context = slideLayer;
    [popover stylizeAsActionSheet];
    
    [popover presentPopoverFromPoint: [self.view convertPoint:position toView:nil]];
    self.currentPopover = popover;
  }
}


#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
  if ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]])
  {
    return self.presentation.active;
  }
  
  return YES;
}


@end
