//
//  MezzanineRoomViewController.swift
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 09/02/2017.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import UIKit

class MezzanineRoomViewController: MezzanineBaseRoomViewController, BinContainerViewControllerDelegate {

  struct Constants {
    static let workspaceOverlaysAnimationDuration = 0.2
  }

  @IBOutlet var workspaceContainerView: UIView!
  @IBOutlet var binContainerView: UIView!
  @IBOutlet var workspaceModalOverlayView: UIView!
  @IBOutlet var workspaceModalOverlayLabel: UILabel!
  @IBOutlet var videoChatViewContainer: SubviewHittableView!

  var workspaceModalOverlayStartTime: Date?
  var workspaceOpeningViewStartTime: Date?

  // Controllers
  let binContainerViewController = BinContainerViewController()
  let navigableWorkspaceViewController = NavigableWorkspaceViewController()
  let workspaceOpeningViewController = WorkspaceOpeningViewController()
  var videoChatContainerViewController: VideoChatContainerViewController! = nil
  
  // Constraints
  @IBOutlet var binContainerHeightConstraint: NSLayoutConstraint!
  
  @IBOutlet var binContainerBottomConstraint: NSLayoutConstraint!
  
  fileprivate var binIsCollapsed: Bool = false
  fileprivate var systemModelObserverArray = Array <String> ()
  fileprivate var workspaceObserverArray = Array <String> ()

  override var systemModel: MZSystemModel! {
    willSet(newSystemModel) {
      endObservingSystemModel()
    }

    didSet {
      beginObservingSystemModel()
      workspaceOpeningViewController.systemModel = systemModel
      navigableWorkspaceViewController.systemModel = systemModel
      binContainerViewController.systemModel = systemModel
    }
  }

  override var workspace: MZWorkspace! {
    willSet(newWorkspace) {
      endObservingWorkspace()
    }

    didSet {
      beginObservingWorkspace()
    }
  }
  
  init() {
    super.init(nibName: "MezzanineRoomViewController", bundle: nil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  deinit {
    print("MezzanineRoomViewController deinit")
    navigableWorkspaceViewController.systemModel = nil
    navigableWorkspaceViewController.communicator = nil
    binContainerViewController.systemModel = nil
    binContainerViewController.communicator = nil
    
    guard let videoChatViewController = videoChatViewController else { return }
    videoChatViewController.pexipManager = nil
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()

    initializeNavigableWorkspace()
    initializeBin()
    initializeWorkspaceOverlay()
    initializeVideoChat()
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  fileprivate func initializeNavigableWorkspace() {
    
    navigableWorkspaceViewController.systemModel = systemModel
    navigableWorkspaceViewController.communicator = communicator
    
    addChildViewController(navigableWorkspaceViewController)
    let aView = navigableWorkspaceViewController.view
    aView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    aView?.frame = view.bounds
    view.insertSubview(aView!, belowSubview: binContainerView)
    navigableWorkspaceViewController.didMove(toParentViewController: self)
  }
  
  fileprivate func initializeBin() {
    binContainerViewController.delegate = self
    binContainerViewController.systemModel = systemModel
    binContainerViewController.communicator = communicator
    
    addChildViewController(binContainerViewController)
    let view = binContainerViewController.view
    view?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    view?.frame = binContainerView.bounds
    binContainerView.addSubview(view!)
    binContainerViewController.didMove(toParentViewController: self)

    let bottomInset: CGFloat = UIApplication.shared.keyWindow != nil ?
      UIApplication.shared.keyWindow!.safeAreaInsets.bottom : 0.0

    if (view?.traitCollection.isiPhone())! {
      binContainerHeightConstraint.constant = 194 + bottomInset
    } else {
      binContainerHeightConstraint.constant = 227 + bottomInset
    }

    binContainerBottomConstraint.constant = 0
  }

  fileprivate func initializeWorkspaceOverlay() {

    // Closing, discarding overlay
    workspaceModalOverlayView.accessibilityLabel = "WorkspaceModalOverlay";
    workspaceModalOverlayView.backgroundColor = UIColor(white:0.0, alpha:0.5)

    workspaceModalOverlayLabel.font = UIFont.dinBold(ofSize: 24.0)
    workspaceModalOverlayLabel.textAlignment = .center
    workspaceModalOverlayLabel.textColor = UIColor.white

    // Opening overlay
    workspaceOpeningViewController.systemModel = self.systemModel

    let openingView = workspaceOpeningViewController.view
    openingView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    openingView?.frame = workspaceModalOverlayView.bounds
    workspaceModalOverlayView.addSubview(openingView!)

    workspaceOpeningViewController.view.isHidden = true
    workspaceOpeningViewController.view.isUserInteractionEnabled = false
  }
  
  fileprivate func initializeVideoChat() {
    if !communicator.isARemoteParticipationConnection {
      videoChatViewContainer.removeFromSuperview()
      return
    }
    
    videoChatContainerViewController = VideoChatContainerViewController(pexipManager: appContext.pexipManager)
    videoChatContainerViewController.delegate = self
    addChildViewController(videoChatContainerViewController)
    let videoChatContainerView = videoChatContainerViewController.view
    videoChatContainerView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    videoChatContainerView?.frame = videoChatViewContainer.bounds
    videoChatViewContainer.addSubview(videoChatContainerView!)
    videoChatContainerViewController.didMove(toParentViewController: self)
  }
  

  //MARK: Adaptivity

  override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
    super.willTransition(to: newCollection, with: coordinator)
   
    guard let videoChatContainerViewController = videoChatContainerViewController else { return }
    videoChatContainerViewController.updateLayout()
  }
  
  override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
    super.viewWillTransition(to: size, with: coordinator)
    
    guard let videoChatContainerViewController = videoChatContainerViewController else { return }
    videoChatContainerViewController.updateLayout()
  }
  
  override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
    super.traitCollectionDidChange(previousTraitCollection)
    
    guard let videoChatContainerViewController = videoChatContainerViewController else { return }
    videoChatContainerViewController.updateLayout()
  }
  
  //MARK: BinContainerViewControllerDelegate
  
  func binContainerViewControllerWillCollapse(_ willCollapse: Bool) {
    // TO DO. Any other stuff needed at some point

    // Animate container
    collapseBin(willCollapse)
    binIsCollapsed = willCollapse
  }
  
  fileprivate func collapseBin(_ shouldCollapse: Bool) {
    let binCollapseHeight = -binContainerHeightConstraint.constant + binContainerViewController.binControlsContainerView.frame.height
    binContainerBottomConstraint.constant = shouldCollapse == true ?  binCollapseHeight : 0.0
    
    UIView.animate(withDuration: 0.33, delay: 0, options: .beginFromCurrentState, animations: {
      self.view.layoutIfNeeded()
      }, completion:nil)
  }
  
  fileprivate func fadeOutBin(_ fadeOut: Bool) {
    UIView.animate(withDuration: 0.33, delay: 0, options: .beginFromCurrentState, animations: {
      self.binContainerView.alpha = fadeOut == true ? 0.0 : 1.0
      }, completion:nil)
  }
  

  // MARK: Observation 

  func beginObservingSystemModel() {

    if systemModel == nil {
      return
    }

    systemModelObserverArray.append(systemModel.addObserver(forKeyPath: "currentWorkspace", options:[.old], on: OperationQueue.main) { [unowned self] ( obj, change: [AnyHashable: Any]!) in
      let systemModel = obj as! MZSystemModel

      let oldWorkspace = change[NSKeyValueChangeKey.oldKey]
      guard let newWorkspace = systemModel.currentWorkspace else {
        self.workspace = nil
        return
      }

      if !(oldWorkspace is NSNull) && !newWorkspace.isSaved {
        // Previously workspace was closed, discarded, or switching workspaces
        let oldWorkspace = oldWorkspace as! MZWorkspace
        if oldWorkspace.wasDestroyed && !newWorkspace.isLoading {

          // If the new workspace isn't currently loading (loading overlay visible)
          let message = oldWorkspace.isSaved ? "Workspace Deleting".localized : "Workspace Discarding".localized
          self.showWorkspaceOverlay(withMessage: message, animated: true)
          self.hideWorkspaceOverlayView(animated: true)
        }
        else if oldWorkspace.isSaved && !newWorkspace.hasContent {
          // Old workspace was closed and is replaced by a new empty workspace
          self.showWorkspaceOverlay(withMessage: "Workspace Closing".localized, animated: true)
          self.hideWorkspaceOverlayView(animated: true)
        }
      }
      else if newWorkspace.isSaved && !newWorkspace.isLoading {
        // This is the case when user opens an empty workspace. In which case throw up the loading overlay
        // is not triggered because isLoading flag is not on
        self.setWorkspaceOpeningViewVisible(true)
        self.setWorkspaceOpeningViewVisible(false) // This invokes the minimum
      }
      self.workspace = systemModel.currentWorkspace
    })
  }

  func endObservingSystemModel() {
    if systemModelObserverArray.count > 0 {
      for token in systemModelObserverArray {
        systemModel.removeObserver(withBlockToken: token)
      }
    }
    systemModelObserverArray.removeAll()
    workspace = nil
  }

  func beginObservingWorkspace() {

    if workspace == nil {
      return
    }

    workspaceObserverArray.append(workspace.addObserver(forKeyPath: "isLoading", options:[.initial], on: OperationQueue.main) { [unowned self] ( obj, change: [AnyHashable: Any]!) in
      let workspace = obj as! MZWorkspace
      self.setWorkspaceOpeningViewVisible(workspace.isLoading)
    })
  }

  func endObservingWorkspace() {
    if workspaceObserverArray.count > 0 {
      for token in workspaceObserverArray {
        workspace.removeObserver(withBlockToken: token)
      }
    }
    workspaceObserverArray.removeAll()
  }


  // MARK: Workspace Overlay

  override func showWorkspaceOverlay(withMessage message: String!, animated: Bool) {

    workspaceModalOverlayLabel!.text = message;

    let animationBlock = {
      self.workspaceModalOverlayView!.alpha = 1.0
    }

    if animated {
      UIView.animate(withDuration: Constants.workspaceOverlaysAnimationDuration, animations: animationBlock)
    }
    else {
      animationBlock()
    }

    workspaceModalOverlayStartTime = Date()
  }

  override func hideWorkspaceOverlayView(animated: Bool) {
    guard let workspaceModalOverlayStartTime = workspaceModalOverlayStartTime else { return }
    let minimumAnimationDelay = 1.6
    let animationDelay = max(0.0, minimumAnimationDelay - Date().timeIntervalSince(workspaceModalOverlayStartTime))

    let animationBlock = {
      self.workspaceModalOverlayView!.alpha = 0.0
    }

    if animated {
      UIView.animate(withDuration: Constants.workspaceOverlaysAnimationDuration, delay: animationDelay, options:[], animations:animationBlock, completion: nil)
    }
    else {
      animationBlock()
    }

    self.workspaceModalOverlayStartTime = nil
  }

  func setWorkspaceOpeningViewVisible(_ visible: Bool) {
    if visible {
      workspaceOpeningViewController.view.isUserInteractionEnabled = true
      workspaceOpeningViewController.view.isHidden = false

      UIView.animate(withDuration: Constants.workspaceOverlaysAnimationDuration, animations: { 
        self.workspaceModalOverlayView.alpha = 1.0
        })

      workspaceOpeningViewStartTime = Date()
    }
    else if !workspaceOpeningViewController.view.isHidden && workspaceOpeningViewController.view.isUserInteractionEnabled {

      let minimumAnimationDelay = 5.6
      let timeSinceVisible = (workspaceOpeningViewStartTime != nil) ? Date().timeIntervalSince(workspaceOpeningViewStartTime!) : 0.0
      let animationDelay = max(0.0, minimumAnimationDelay - timeSinceVisible)

      UIView.animate(withDuration: Constants.workspaceOverlaysAnimationDuration, delay: animationDelay, options:[], animations:{
        self.workspaceModalOverlayView?.alpha = 0.0;
        }, completion: { (Bool) in
          self.workspaceOpeningViewController.view.isHidden = true
          self.workspaceOpeningViewStartTime = nil;
      })
      workspaceOpeningViewController.view.isUserInteractionEnabled = false
    }
  }
}

extension MezzanineRoomViewController: VideoChatContainerViewControllerDelegate {
  func videoChatContainerWillTransitionToState(_ state: VideoChatState) {
    UIView.animate(withDuration: 0.33, delay: 0, options: .beginFromCurrentState, animations: {
      self.navigableWorkspaceViewController.state = state == .preview ? .full : .preview
      self.fadeOutBin(state == .fullscreen ? true : false)
      if !self.binIsCollapsed {
        self.collapseBin(state == .fullscreen)
      }
    }, completion: nil)
  }
}
