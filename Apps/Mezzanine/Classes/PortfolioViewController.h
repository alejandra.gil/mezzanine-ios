//
//  PortfolioViewController.h
//  Mezzanine
//
//  Created by Zai Chang on 1/16/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FPPopoverController+Mezzanine.h"
#import "MezzanineAppContext.h"
#import "MZSystemModel.h"
#import "ProgressPieView.h"
#import "OBDragDropProtocol.h"


@class MezzanineMainViewController;


/// Controller for the portfolio respresentation of the presentation

@interface PortfolioViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, FPPopoverControllerDelegate, UIImagePickerControllerDelegate, OBOvumSource, OBDropZone, UIGestureRecognizerDelegate, UINavigationControllerDelegate>

@property (nonatomic, strong) MezzanineAppContext *appContext;
@property (nonatomic, strong) MZWorkspace *workspace;
@property (nonatomic, strong) MZSystemModel *systemModel;
@property (nonatomic, strong) MZPresentation *presentation;

@property (nonatomic, weak) MezzanineMainViewController *mezzanineViewController;

@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIButton *addButton;
@property (strong, nonatomic) IBOutlet UIButton *presentButton;
@property (strong, nonatomic) IBOutlet UIButton *moreButton;
@property (strong, nonatomic) IBOutlet UIImageView *addButtonImageView;
@property (strong, nonatomic) IBOutlet UIImageView *presentButtonImageView;
@property (strong, nonatomic) IBOutlet UIImageView *moreButtonElipsisImageView;

@property (nonatomic, assign) BOOL preparingImageBatchFromAssetLibrary;

@property (nonatomic, strong) IBOutlet ProgressPieView* progressPieView;

@property (nonatomic, strong) IBOutlet UIView *statusView;
@property (nonatomic, strong) IBOutlet UILabel *statusLabel;
@property (nonatomic, strong) IBOutlet UILabel *hintLabel;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *titleLabelLeftConstraint;

- (IBAction)add:(id)sender;
- (IBAction)present:(id)sender;
- (IBAction)more:(id)sender;

- (void)updatePresentationModeState;
- (void)updateStatus;

@end
