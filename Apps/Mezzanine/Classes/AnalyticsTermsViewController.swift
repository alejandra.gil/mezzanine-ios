//
//  AnalyticsTermsViewController.swift
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 04/03/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

import UIKit

class AnalyticsTermsViewController: UIViewController {
  
  @IBOutlet var termsTextView: UITextView!
  
  
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
  }
  
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    title = "Privacy policy"
    
    termsTextView.font = UIFont.din(ofSize: 11)
    termsTextView.textColor = MezzanineStyleSheet.shared().grey70Color
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    navigationController?.setNavigationBarHidden(false, animated: true)
  }
  
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    termsTextView.setContentOffset(CGPoint.zero, animated: false)
  }
}
