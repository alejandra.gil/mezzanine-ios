//
//  MezzanineWindow.h
//  Mezzanine
//
//  Created by Zai Chang on 1/28/13.
//  Copyright (c) 2013 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MezzanineWindow : UIWindow
{
  UIView *contextView;
  CALayer *contextViewSublayer;
  void (^contextViewDismissBlock)();
}

// sublayer is optional in case the hit test should be performed on a sublayer of that view
-(void) registerContextView:(UIView*)view sublayer:(CALayer*)sublayer withDismissBlock:(void (^)())dismissBlock;
-(void) clearContextView:(UIView*)view;

@end
