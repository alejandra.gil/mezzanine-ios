//
//  WorkspaceListCell_Private.h
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 27/10/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

#import "WorkspaceListCell.h"

@interface WorkspaceListCell ()
{
  UIImageView *imageView;
  UITextField *titleLabel;
  UILabel *dateLabel;
  UILabel *ownerLabel;
  
  UIView *infoView;
  UIView *actionsView;
  
  FPPopoverController *currentPopoverController;
  UIAlertController *currentAlertController;
}

- (void)showWorkspaceOptions;
- (void)duplicateWorkspace;
- (void)renameWorkspace;

@end
