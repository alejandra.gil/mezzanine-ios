//
//  FeldSwitcherView.m
//  Mezzanine
//
//  Created by miguel on 30/05/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import "FeldSwitcherView.h"
#import "NSObject+BlockObservation.h"
#import "MezzanineStyleSheet.h"
#import "TTButton.h"

@interface FeldSwitcherView ()
{
  NSMutableArray *_feldsObservers;
  NSMutableArray *feldButtons;
}

@end

@implementation FeldSwitcherView

- (void)initialize
{
  feldButtons = [[NSMutableArray alloc] init];
  self.backgroundColor = [UIColor clearColor];
}


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
      [self initialize];
    }
    return self;
}


- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
  self = [super initWithCoder:aDecoder];
  if (self) {
    [self initialize];
  }
  return self;
}


- (void)dealloc
{
  if (_feldsObservers)
    for (id observer in _feldsObservers)
      [_systemModel removeObserverWithBlockToken:observer];

  _feldsObservers = nil;
}


#pragma mark - Properties

- (void)setSystemModel:(MZSystemModel *)systemModel
{
  if (systemModel != _systemModel)
  {
    if (_systemModel)
    {
      for (id observer in _feldsObservers)
        [_systemModel removeObserverWithBlockToken:observer];
      _feldsObservers = nil;
    }

    _systemModel = systemModel;

    if (_systemModel)
    {
      _feldsObservers = [NSMutableArray array];
      id observer;
      __weak typeof(self) __self = self;
      observer = [_systemModel addObserverForKeyPath:@"felds" options:(NSKeyValueObservingOptionNew) onQueue:[NSOperationQueue mainQueue] task:^(id object, NSDictionary *change) {
        [__self updateLayout];
      }];
      [_feldsObservers addObject:observer];

      [self updateLayout];
    }
  }
}


- (void)setSelectedFeldIndex:(NSInteger)selectedFeldIndex
{
  _selectedFeldIndex = selectedFeldIndex;
  [self updatebuttonStates];
}


- (void)updatebuttonStates
{
  if ([self shouldHideFeldSwitcher])
    return;

  for (TTButton *button in feldButtons)
  {
    [button setSelected:NO];
  }

  if (_selectedFeldIndex < feldButtons.count)
    [feldButtons[_selectedFeldIndex] setSelected:YES];
}


#pragma mark - Layout

- (void)updateLayout
{
  for (UIView *view in feldButtons)
  {
    [view removeFromSuperview];
  }
  
  [feldButtons removeAllObjects];

  if ([self shouldHideFeldSwitcher])
    return;

  CGFloat x = 0;
  NSInteger index = 1;
  CGFloat width = (NSInteger) [self feldWidth];
  CGFloat height = (NSInteger) [self feldHeight];

  for (NSInteger i = 0 ; i < _systemModel.felds.count ; i++)
  {
    TTButton *button = [TTButton buttonWithStyle:@"feldSwitcherButton:"];
    CGFloat buttonY = (NSInteger) (self.frame.size.height - height) / 2.0;
    [button setFrame:CGRectMake(x, buttonY, width, height)];
    [button addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
    button.tag = index++;

    [feldButtons addObject:button];
    [self addSubview:button];
    x += width;
  }
}


- (CGSize)calculateSizeForView
{
  if ([self shouldHideFeldSwitcher])
    return CGSizeZero;

  CGFloat totalFelds = [self feldWidth] * _systemModel.felds.count;

  return CGSizeMake(totalFelds, self.frame.size.height);
}


- (BOOL)shouldHideFeldSwitcher
{
  return (_systemModel.felds.count <= 1);
}

#pragma mark - Layout Helpers

- (CGFloat)feldWidth
{
  return ceil(self.frame.size.height);
}


- (CGFloat)feldHeight
{
  if (!_systemModel.felds.count)
    return 0;

  CGFloat aspectRatio = [_systemModel.felds[0] aspectRatio] ? [_systemModel.felds[0] aspectRatio] : 16.0 / 9.0;
  return ceil([self feldWidth] / aspectRatio);
}


#pragma mark - Actions

- (void)buttonTapped:(id)sender
{
  TTButton *button = sender;
  [self setSelectedFeldIndex:[feldButtons indexOfObject:button]];
  if ([_delegate respondsToSelector:@selector(didTapOnFeld:)])
    [_delegate didTapOnFeld:button.tag-1];
}


@end
