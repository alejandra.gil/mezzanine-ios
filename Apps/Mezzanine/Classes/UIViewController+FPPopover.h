//
//  UIViewController+FPPopover.h
//  Mezzanine
//
//  Created by Zai Chang on 8/13/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FPPopoverController.h"


@interface UIViewController (FPPopover)

@property (nonatomic, strong) FPPopoverController *currentPopover;

- (void)dismissPopoverIfContextMatches:(NSArray*)objects;

@end
