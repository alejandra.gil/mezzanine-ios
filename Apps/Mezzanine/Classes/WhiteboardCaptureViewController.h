//
//  WhiteboardCaptureViewController.h
//  Mezzanine
//
//  Created by Zai Chang on 12/3/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WhiteboardInteractor.h"
#import "MezzKit.h"
#import "TTButton.h"


@interface WhiteboardCaptureViewDataSource : NSObject<UITableViewDataSource>

@property (nonatomic, retain) MZSystemModel *systemModel;

- (void)setupTableView:(UITableView *)tableView;

@end


@interface WhiteboardCaptureViewController : UIViewController<UITableViewDelegate>

@property (nonatomic, retain) IBOutlet UITableView *tableView;
@property (nonatomic, retain) IBOutlet TTButton *captureAllButton;

@property (nonatomic, retain) MZSystemModel *systemModel;
@property (nonatomic, retain) WhiteboardCaptureViewDataSource *dataSource;
@property (nonatomic, retain) WhiteboardInteractor *interactor;

@property (nonatomic, copy) void (^didRequestWhiteboardCapture)(NSString *);

@end
