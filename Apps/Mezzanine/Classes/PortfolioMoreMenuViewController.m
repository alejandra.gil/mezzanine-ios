//
//  PortfolioMenuViewController.m
//  Mezzanine
//
//  Created by Zai Chang on 9/25/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import "PortfolioMoreMenuViewController.h"
#import "UIViewController+FPPopover.h"
#import "NSObject+BlockObservation.h"


@interface PortfolioMoreMenuViewController ()
{
  NSMutableArray *_presentationObservers;
  NSMutableArray *_exportObservers;
}

@end

@implementation PortfolioMoreMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
    }
    return self;
}


- (void)dealloc
{
  self.systemModel = nil;
}

- (void)setSystemModel:(MZSystemModel *)systemModel
{
  if (_systemModel != systemModel)
  {
    _systemModel = systemModel;
    [self setExportInfo:systemModel.portfolioExport.info];
    [self setPresentation:systemModel.currentWorkspace.presentation];
  }
}


- (void)setPresentation:(MZPresentation *)presentation
{
  if (_presentation != presentation)
  {
    if (_presentation)
    {
      for (id observer in _presentationObservers)
        [_presentation removeObserverWithBlockToken:observer];
      _presentationObservers = nil;
    }
    
    _presentation = presentation;
    
    if (_presentation)
    {
      _presentationObservers = [NSMutableArray array];
      __weak typeof(self) __self = self;
      id observer = [_presentation addObserverForKeyPath:@"slides"
                                               options:0
                                               onQueue:[NSOperationQueue mainQueue]
                                                  task:^(id obj, NSDictionary *change) {
                                                    [__self reloadItems];
                                                  }];
      [_presentationObservers addObject:observer];
      
      [self reloadItems];
    }
  }
}


- (void)setExportInfo:(MZExportInfo *)exportInfo
{
  if (_exportInfo != exportInfo)
  {
    if (_exportInfo)
    {
      for (id observer in _exportObservers)
        [_exportInfo removeObserverWithBlockToken:observer];
      _exportInfo = nil;
    }

    _exportInfo = exportInfo;

    if (_exportInfo)
    {
      _exportObservers = [NSMutableArray array];
      __weak typeof(self) __self = self;
      id observer = [_exportInfo addObserverForKeyPath:@"state"
                                                 options:0
                                                 onQueue:[NSOperationQueue mainQueue]
                                                    task:^(id obj, NSDictionary *change) {
                                                      [__self reloadItems];
                                                    }];
      [_exportObservers addObject:observer];

      [self reloadItems];
    }
  }
}


- (void)reloadItems
{
  __weak typeof(self) __self = self;
  NSMutableArray *menuItems = [NSMutableArray array];

  if (_systemModel.featureToggles.disableDownloadsEnabled && !_systemModel.isSignedIn)
  {
    NSString *buttonTitle = NSLocalizedString(@"Portfolio Export Button Sign In Title", nil);
    ButtonMenuItem *exportItem = [ButtonMenuItem itemWithTitle:buttonTitle action:nil];
    exportItem.enabled = NO;
    [menuItems addObject:exportItem];
  }
  else
  {
    BOOL isPreparingExport = _exportInfo.state == MZExportStateRequested;
    BOOL isDownloadingExport = _exportInfo.state == MZExportStateDownloading;
    NSString *buttonTitle = isPreparingExport ? NSLocalizedString(@"Portfolio Preparing Export Button Title", nil) : NSLocalizedString(@"Portfolio Export Button Title", nil);
    buttonTitle = isDownloadingExport ? NSLocalizedString(@"Portfolio Downloading Export Button Title", nil) : buttonTitle;
    ButtonMenuItem *exportItem = [ButtonMenuItem itemWithTitle:buttonTitle action:^{
      if (__self.requestPortfolioExport)
        __self.requestPortfolioExport();
    }];
    exportItem.enabled = !isPreparingExport && !isDownloadingExport &&(_presentation.slides.count > 0);
    [menuItems addObject:exportItem];
  }


  ButtonMenuItem *deleteAllItem = [ButtonMenuItem itemWithTitle:NSLocalizedString(@"Portfolio Delete All Button Title", nil) action:^{
    if (__self.requestPortfolioDeleteAll)
      __self.requestPortfolioDeleteAll();
  }];
  deleteAllItem.enabled = (_presentation.slides.count > 0);
  [menuItems addObject:deleteAllItem];
  
  self.menuItems = menuItems;
}



@end
