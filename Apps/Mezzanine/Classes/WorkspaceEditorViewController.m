//
//  WorkspaceEditorViewController.m
//  Mezzanine
//
//  Created by Zai Chang on 3/20/11.
//  Copyright 2011 Oblong Industries. All rights reserved.
//

#import "WorkspaceEditorViewController.h"
#import "MezzanineStyleSheet.h"
#import "UIViewController+Three20.h"
#import "NSString+VersionChecking.h"

@interface WorkspaceEditorViewController ()
{
  id textFieldObserver;
}

- (void)cancel;
- (void)save;

@end

@implementation WorkspaceEditorViewController

@synthesize autoHighlightNameField;

@synthesize nameTextField;
@synthesize requestSave;
@synthesize requestCancel;

@synthesize saveButton;
@synthesize hintLabel;

@synthesize viewTitle;
@synthesize doneButtonTitle;


- (id)initWithWorkspace:(MZWorkspace*)workspace
{
  self = [super initWithNibName:nil bundle:nil];
  if (self)
  {
    self.doneButtonTitle = NSLocalizedString(@"Generic Button Title Done", nil);
    self.workspace = workspace;
  }
  return self;
}


- (void)dealloc
{
  [[NSNotificationCenter defaultCenter] removeObserver:textFieldObserver];
}


- (void)viewDidLoad 
{
  [super viewDidLoad];

  self.edgesForExtendedLayout = UIRectEdgeNone;
  
  MezzanineStyleSheet *styleSheet = [MezzanineStyleSheet sharedStyleSheet];
  self.view.backgroundColor = [UIColor whiteColor];

  UITextField* textField = [[UITextField alloc] init];
  textField.frame = CGRectZero;
  textField.font = [UIFont dinOfSize:18.0];
  textField.text = (_workspaceNameOverride != nil) ? _workspaceNameOverride : _workspace.name;
  textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;    
  textField.placeholder = NSLocalizedString(@"Workspace Name Placeholder", nil);
  textField.clearButtonMode = UITextFieldViewModeWhileEditing;
  textField.returnKeyType = UIReturnKeyDone;
  textField.borderStyle = UITextBorderStyleRoundedRect;
  textField.delegate = self;
  textField.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin);
  textField.accessibilityIdentifier = @"WorkspaceEditorView.textField";
  __weak typeof(self) weakSelf = self;
  textFieldObserver = [[NSNotificationCenter defaultCenter] addObserverForName:UITextFieldTextDidChangeNotification object:textField queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *notification) {
    weakSelf.saveButton.enabled = (weakSelf.nameTextField.text.length > 0);
  }];

  self.nameTextField = textField;
  [self.view addSubview:self.nameTextField];

  if (self.hintText)
  {
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.backgroundColor = [UIColor clearColor];
    label.numberOfLines = 0;
    label.font = [UIFont dinOfSize:14.0];
    label.minimumScaleFactor = 0.7;
    label.textAlignment = NSTextAlignmentLeft;
    label.textColor = styleSheet.grey90Color;
    label.text = self.hintText;
    label.autoresizingMask = (UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin);

    label.frame = CGRectZero;
    self.hintLabel = label;

    [self.view addSubview:self.hintLabel];
  }

  NSString *buttonStyle = @"lightTopBorderedButton:";
  TTButton *button = [TTButton buttonWithStyle:buttonStyle title:self.doneButtonTitle];
  [button addTarget:self action:@selector(save) forControlEvents:UIControlEventTouchUpInside];
  button.frame = CGRectZero;
  button.accessibilityIdentifier = [NSString stringWithFormat:@"WorkspaceEditorView.%@", self.doneButtonTitle];
  self.saveButton = button;
  [self.view addSubview:self.saveButton];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
  if ([[UIDevice currentDevice] isIPad])
    return YES;
  
  return (interfaceOrientation == UIInterfaceOrientationPortrait) || UIInterfaceOrientationIsLandscape(interfaceOrientation);
}


- (void)viewDidLayoutSubviews
{
  CGFloat marginX = 12.0;
  CGFloat contentWidth = (self.view.frame.size.width - 2 * marginX);
  CGFloat y = 20.0;
  CGFloat marginY = 12.0;
  CGFloat actionButtonHeight = 44.0;

  self.nameTextField.frame = CGRectMake(marginX, y, contentWidth, 37.0);
  y += self.nameTextField.frame.size.height + marginY;

  if (self.hintText)
  {
    CGSize size = [self.hintLabel sizeThatFits:CGSizeMake(self.view.frame.size.width, CGFLOAT_MAX)];
    self.hintLabel.frame = CGRectMake(marginX, y, size.width, size.height);
  }

  self.saveButton.frame = CGRectMake(0, self.view.frame.size.height - actionButtonHeight, self.view.bounds.size.width, actionButtonHeight);
}


- (void)cancel
{
  if (requestCancel)
    requestCancel();
}

- (void)save
{
  if (requestSave)
    requestSave();
}


- (BOOL)textFieldShouldClear:(UITextField *)textField
{
  saveButton.enabled = NO;
  return YES;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
  [self save];
  return YES;
}

- (void)textFieldCanBecomeFirstResponder
{
  // Highlight the name field after the keyboard has appeard to avoid the blue initial/end selectors to appear before the popover
  if (autoHighlightNameField)
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(highlightNameField) name:UIKeyboardDidShowNotification object:nil];

  // For iPad, form sheet seems to have issues animating. Adding a slight delay to show the keyboard fix the popover appearing without flickering
  dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.15 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    [self.nameTextField becomeFirstResponder];
  });
}

- (void)highlightNameField
{
  // Bug 14307 -> Reset the text to force cursor to go to end of line
  self.nameTextField.text = self.nameTextField.text;

  // Remove the notification because we only want to highlight the text when the popover appears for the first time
  [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
  [self.nameTextField selectAll:nil];
}

@end
