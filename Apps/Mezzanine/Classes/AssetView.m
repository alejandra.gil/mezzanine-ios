//
//  AssetView.m
//  Mezzanine
//
//  Created by Miguel Sánchez Valdés on 3/12/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import "AssetView.h"
#import "NSObject+BlockObservation.h"
#import "MezzanineStyleSheet.h"
#import "MezzanineWindow.h"
#import "TTButton.h"
#import "Mezzanine-Swift.h"

@interface AssetView (Private)

-(void) setActionsPanelAnimationState:(CGFloat)state;

@end

@implementation AssetView

@synthesize asset;
@synthesize userInfo;

@synthesize panRecognizer;

@synthesize contentView;
@synthesize actionsPanel;
@synthesize actionsPanelEnabled;
@synthesize actionsPanelVisible;
@synthesize actionsPanelBecameVisible;

@synthesize deleteButton;
@synthesize deleteButtonTapped;

@synthesize isBeingManipulated;
@synthesize marginForGestures;

@synthesize overlayView;
@synthesize borderColor;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) 
    {
      self.exclusiveTouch = YES;
      self.userInteractionEnabled = YES;
      self.clipsToBounds = YES;
      //self.backgroundColor = [UIColor colorWithWhite:0.12 alpha:1.0];
      self.marginForGestures = 0.0;

      if (![SettingsFeatureToggles sharedInstance].teamworkUI)
      {
        self.borderColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
        CALayer *layer = self.layer;
        layer.borderWidth = 1.0;
      }

      actionsPanel = [[UIView alloc] initWithFrame:CGRectZero];
      actionsPanel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
      actionsPanel.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.33];
      [self addSubview:actionsPanel];
      
      deleteButton = [TTButton buttonWithStyle:@"redTransluscentRoundedButton:" title:NSLocalizedString(@"Generic Button Title Delete", nil)];
      deleteButton.frame = CGRectZero;
      [deleteButton addTarget:self action:@selector(deleteAssetRequested:) forControlEvents:UIControlEventTouchUpInside];
      [actionsPanel addSubview:deleteButton];
      
      [self setActionsPanelVisible:NO animated:NO];
      self.actionsPanelEnabled = NO;

      contentView = [[UIView alloc] initWithFrame:CGRectZero];
      contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
      contentView.backgroundColor = [MezzanineStyleSheet sharedStyleSheet].assetBackgroundColor;
      contentView.userInteractionEnabled = NO;  // Important for gesture recognizers to work
      [self addSubview:contentView];



//      contentFilter = [CIFilter filterWithName:@"CIColorControls"];
//      [contentFilter setDefaults];
//      contentView.layer.filters = [NSArray arrayWithObjects:contentFilter, nil];
      
      
      deleteShroud = [[UIView alloc] initWithFrame:CGRectZero];
      deleteShroud.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
      deleteShroud.backgroundColor = [UIColor colorWithRed:0.5 green:0.0 blue:0.0 alpha:0.42];
      deleteShroud.userInteractionEnabled = NO;  // Important for gesture recognizers to work
      deleteShroud.layer.hidden = YES;
      [self addSubview:deleteShroud];
      
      
      contentShadowLayer = [CAGradientLayer layer];
      contentShadowLayer.colors = @[(id)[UIColor colorWithWhite:0.0 alpha:0.28].CGColor,
                                        (id)[UIColor colorWithWhite:0.0 alpha:0.0].CGColor];
      contentShadowLayer.locations = @[@0.0,
                                           @1.0];
      [contentView.layer addSublayer:contentShadowLayer];
      
      // The follow removes implicit layer animations because they cause some funky effects during orientation change
      [actionsPanel.layer removeAllAnimations];
      
      
      overlayView = [[UIView alloc] initWithFrame:CGRectZero];
      overlayView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
      [overlayView setBackgroundColor:[UIColor colorWithWhite:0.1 alpha:1.0]];
      [overlayView setAlpha:0.0];
      [self insertSubview:overlayView aboveSubview:contentView];
      
      
      panRecognizer = [[OBDirectionalPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanGesture:)];
      panRecognizer.direction = OBDirectionalPanUp;
      panRecognizer.delegate = self;
      [self addGestureRecognizer:panRecognizer];
      
      OBDirectionalPanGestureRecognizer *downwardsPanRecognizer = [[OBDirectionalPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleDownwardsPanGesture:)];
      downwardsPanRecognizer.direction = OBDirectionalPanDown;
      downwardsPanRecognizer.delegate = self;
      [self addGestureRecognizer:downwardsPanRecognizer];
      
      
      // Adding a second set of gesture recognizers
      // because a subview can consume touch events and the underlying
      // recognizer on AssetView is no longer activated
      OBDirectionalPanGestureRecognizer *actionsPanelPanRecognizer = [[OBDirectionalPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanGesture:)];
      actionsPanelPanRecognizer.direction = OBDirectionalPanUp;
      actionsPanelPanRecognizer.delegate = self;
      [actionsPanel addGestureRecognizer:actionsPanelPanRecognizer];
      
      OBDirectionalPanGestureRecognizer *actionsPanelDownwardsPanRecognizer = [[OBDirectionalPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleDownwardsPanGesture:)];
      actionsPanelDownwardsPanRecognizer.direction = OBDirectionalPanDown;
      actionsPanelDownwardsPanRecognizer.delegate = self;
      [actionsPanel addGestureRecognizer:actionsPanelDownwardsPanRecognizer];
    }
  
    return self;
}

-(void) layoutSubviews
{
  [super layoutSubviews];

  CGRect bounds = self.bounds;
  
  // Shadow path needs to be refreshed
  self.layer.shadowPath = [UIBezierPath bezierPathWithRect:bounds].CGPath;

  if (!self.useAutolayout)
  {
    [self updateContentViewLayout];
  }

  CGRect contentViewFrame = contentView.frame;
  [CATransaction begin];
  [CATransaction setDisableActions: YES];
  contentShadowLayer.frame = CGRectMake(0, CGRectGetMaxY(contentViewFrame), CGRectGetWidth(contentViewFrame), 8.0);
  [CATransaction commit];
  
  overlayView.frame = contentViewFrame;

  CGRect actionsPanelFrame = bounds;
  
  CGFloat actionPanelMaxHeight = 64.0;
  
  if (actionsPanelFrame.size.height > actionPanelMaxHeight)
  {
    actionsPanelFrame.origin.y = actionsPanelFrame.size.height - actionPanelMaxHeight;
    actionsPanelFrame.size.height = actionPanelMaxHeight;
  }
  
  [actionsPanel setFrame:actionsPanelFrame];
  
  CGSize deleteButtonSize = CGSizeMake(96, 40);
  deleteButton.frame = CGRectMake(0, 0, deleteButtonSize.width, deleteButtonSize.height);
  deleteButton.center = CGPointMake(CGRectGetMidX(actionsPanel.bounds), CGRectGetMidY(actionsPanel.bounds));
  
  [deleteShroud setFrame:bounds];
}

- (void) dealloc;
{
  if (assetObservers)
  {
    for (NSString *token in assetObservers)
      [asset removeObserverWithBlockToken:token];
    
    assetObservers = nil;
  }
}

- (void) setUseAutolayout:(BOOL)useAutolayout
{
  _useAutolayout = useAutolayout;

  if (useAutolayout)
  {
    contentView.autoresizingMask = UIViewAutoresizingNone;
    [contentView setTranslatesAutoresizingMaskIntoConstraints:NO];

    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[contentView]-0-|"
                                                                 options:0
                                                                 metrics:nil
                                                                   views:NSDictionaryOfVariableBindings(contentView)]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[contentView]-0-|"
                                                                 options:0
                                                                 metrics:nil
                                                                   views:NSDictionaryOfVariableBindings(contentView)]];
  }
  else
  {
    contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
  }
}

#pragma mark - Model

-(void) setAsset:(MZAsset *)anAsset
{
  if (asset != anAsset)
  {
    if (asset && assetObservers)
    {
      for (NSString *token in assetObservers)
        [asset removeObserverWithBlockToken:token];
      
      assetObservers = nil;
    }
    
    asset = anAsset;
    
    if (asset)
    {
      assetObservers = [[NSMutableArray alloc] init];
      __weak typeof(self) __self = self;
      id token;
      token = [asset addObserverForKeyPath:@"isPendingDeletion" task:^(id obj, NSDictionary *change) {
        [__self updateViewStatesAnimated:YES];
      }];
      [assetObservers addObject:token];
    }
    
    [self updateViewStatesAnimated:NO];
  }
}


-(void) setActionsPanelEnabled:(BOOL)enabled
{
  actionsPanelEnabled = enabled;
  
  // Also hide the panel, in case the AssetView becomes transparent
  actionsPanel.hidden = !actionsPanelEnabled;
}


#pragma mark - Visual States

-(void) setBorderColor:(UIColor *)color
{
  borderColor = color;
  
  self.layer.borderColor = borderColor.CGColor;
}


-(void) updateContentViewLayout
{
  CGRect frame = self.bounds;
  frame.origin.y = -actionsPanelAnimationState * actionsPanel.frame.size.height;
  contentView.frame = frame;
}

-(void) setActionsPanelAnimationState:(CGFloat)state
{
  actionsPanelAnimationState = state;
  
//  [contentFilter setValue:[NSNumber numberWithFloat:state] forKey:@"inputSaturation"];
  
  [self updateContentViewLayout];
}


-(void) setActionsPanelVisible:(BOOL)visible animated:(BOOL)animated
{
  if (!actionsPanelEnabled)
    return;
  
  // Don't do anything if asset is already about to be deleted
  if (asset && [asset isPendingDeletion])
    return;
  
  actionsPanelVisible = visible;
  
  void (^viewChanges)() = ^{
    CGFloat animationState = actionsPanelVisible ? 1.0 : 0.0;
    [self setActionsPanelAnimationState:animationState];
  };
  
  void (^animationCompletion)(BOOL finished) = ^(BOOL finished) {
    if (!visible)
      actionsPanel.hidden = YES;
  };
  
  if (visible)
    actionsPanel.hidden = NO;
  
  if (animated)
  {
    [UIView animateWithDuration:0.25
                     animations:viewChanges
                     completion:animationCompletion];
  }
  else
  {
    viewChanges();
    animationCompletion(YES);
  }
  
  if ([self.window isKindOfClass:[MezzanineWindow class]])
  {
    MezzanineWindow *mezzanineWindow = (MezzanineWindow *)self.window;
    if (actionsPanelVisible)
    {
      [mezzanineWindow registerContextView:self sublayer:nil withDismissBlock:^{
        [self setActionsPanelVisible:NO animated:YES];
      }];
    }
    else
    {
      [mezzanineWindow clearContextView:self];
    }
  }
  
  if (actionsPanelVisible && self.actionsPanelBecameVisible)
    self.actionsPanelBecameVisible(self);
}


-(void) updateViewStatesAnimated:(BOOL)animated
{
  
  void (^viewChanges) () = ^{
    if (asset && [asset isPendingDeletion])
    {
      deleteShroud.layer.hidden = NO;
      self.layer.borderColor = [UIColor redColor].CGColor;
    }
    else
    {
      deleteShroud.layer.hidden = YES;
      self.layer.borderColor = borderColor.CGColor;
    }
  };

  
  if (animated)
    [UIView animateWithDuration:0.12 animations:^{
      viewChanges();      
    }];
  else
    viewChanges();
}

-(IBAction) toggleActionsPanel:(id)sender
{
  [self setActionsPanelVisible:!actionsPanelVisible animated:YES];
}

-(void) deleteAssetRequested:(id)sender
{
  // Don't do anything if asset is already about to be deleted
  if (asset && [asset isPendingDeletion])
    return;
  
  if (actionsPanelVisible)
  {
    [self setActionsPanelVisible:NO animated:YES];
  
    if (deleteButtonTapped)
      deleteButtonTapped(self);
  }
}


-(void) setIsBeingManipulated:(BOOL)manipulated
{
  isBeingManipulated = manipulated;
  
//  if (isBeingManipulated)
//    self.layer.shadowPath = nil;
//  else
//    self.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.bounds].CGPath;
}



static CGFloat panGestureThreshold = 50;  // Threshold for when a user releases the touch, to complete the animation

-(void) handlePanGesture:(UIPanGestureRecognizer*)recognizer
{
  if (!actionsPanelEnabled)
    return;
  
  CGPoint location = [recognizer locationInView:recognizer.view];
  CGFloat deltaY = location.y - verticalPanGestureStart.y;
  
  if (recognizer.state == UIGestureRecognizerStateBegan)
  {
    verticalPanGestureStart = location;
    actionsPanel.hidden = NO;
  }
  else if (recognizer.state == UIGestureRecognizerStateChanged)
  {
    CGFloat panAnimationState = MIN(1.0, MAX(0.0, -deltaY / actionsPanel.frame.size.height));
    
    [self setActionsPanelAnimationState:panAnimationState];    
  }
  else if (recognizer.state == UIGestureRecognizerStateEnded)
  {
    CGPoint velocity = [recognizer velocityInView:recognizer.view];
    if (deltaY < -panGestureThreshold || velocity.y < -300)
    {
      [self setActionsPanelVisible:YES animated:YES];
    }
    else
    {
      [self setActionsPanelVisible:NO animated:YES];
    }
  }
}


-(void) handleDownwardsPanGesture:(UIPanGestureRecognizer*)recognizer
{
  CGPoint location = [recognizer locationInView:recognizer.view];
  CGFloat deltaY = location.y - verticalPanGestureStart.y;
  
  if (recognizer.state == UIGestureRecognizerStateBegan)
  {
    verticalPanGestureStart = location;
  }
  else if (recognizer.state == UIGestureRecognizerStateChanged)
  {
    CGFloat panAnimationState = MIN(1.0, MAX(0.0, 1.0 - deltaY / actionsPanel.frame.size.height));
    
    [self setActionsPanelAnimationState:panAnimationState];    
  }
  else if (recognizer.state == UIGestureRecognizerStateEnded)
  {
    CGPoint velocity = [recognizer velocityInView:recognizer.view];
    if (deltaY > panGestureThreshold || velocity.y > 300)
    {
      [self setActionsPanelVisible:NO animated:YES];
    }
    else
    {
      [UIView animateWithDuration:0.25 animations:^{
        [self setActionsPanelAnimationState:1.0];
      }];
    }
  }  
}


- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
  if ([gestureRecognizer isKindOfClass:[OBDirectionalPanGestureRecognizer class]])
  {
    if (!actionsPanelEnabled)
      return NO;
    
    OBDirectionalPanGestureRecognizer *recognizer = (OBDirectionalPanGestureRecognizer*)gestureRecognizer;
    if (recognizer.direction == OBDirectionalPanUp)
      return !actionsPanelVisible;
    else if (recognizer.direction == OBDirectionalPanDown)
      return actionsPanelVisible;
  }
  return YES;
}

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
  // Some AssetViews layers can be masked and the outer space should not respond to touch.
  CAShapeLayer *mask = (CAShapeLayer *)self.layer.mask;
  if (mask)
  {
    return CGPathContainsPoint(mask.path, nil, point, YES);
  }

  CGRect touchFrame = [self frame];
  touchFrame.origin.x -= marginForGestures;
  touchFrame.origin.y -= marginForGestures;
  touchFrame.size.width += marginForGestures*2.0;
  touchFrame.size.height += marginForGestures*2.0;
    
  point = [self convertPoint:point toView:[self superview]];
  return CGRectContainsPoint(touchFrame, point);
}


@end
