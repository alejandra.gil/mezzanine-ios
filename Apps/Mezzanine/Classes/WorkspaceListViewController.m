//
//  WorkspaceListViewController.m
//  Mezzanine
//
//  Created by miguel on 10/03/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import "WorkspaceListViewController.h"
#import "WorkspaceListViewController_Private.h"
#import "WorkspaceListCollectionViewLayout.h"
#import "MezzanineStyleSheet.h"
#import "WorkspaceEditorViewController.h"
#import "WorkspaceListCell.h"
#import "UserSignInViewController.h"
#import "ButtonMenuViewController.h"
#import "MezzanineMainViewController.h"
#import "UITraitCollection+Additions.h"
#import "Mezzanine-Swift.h"

static void *kSystemModelContext = @"kSystemModelContext";
static void *kWorkspaceContext = @"kWorkspaceContext";

@implementation WorkspaceListViewController

- (id)init
{
  if (self = [super initWithNibName:NSStringFromClass([self class]) bundle:nil])
  {
    // Custom initialization
  }
  return self;
}


- (void)viewDidLoad
{
  [super viewDidLoad];
  self.view.backgroundColor = [UIColor clearColor];
  self.view.accessibilityIdentifier = @"WorkspaceList";
  
  self.title = NSLocalizedString(@"Workspace List Saved Workspaces Title", nil);
  
  _signInBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Workspace List New Workspace Button", nil)
                                                                           style:UIBarButtonItemStylePlain
                                                                          target:self
                                                                          action:@selector(signIn)];
  
  _workspaceNewBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Workspace List New Workspace Button", nil)
                                                                           style:UIBarButtonItemStylePlain
                                                                          target:self
                                                                          action:@selector(newWorkspace)];
  
  _doneBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Generic Button Title Done", nil)
                                                                            style:UIBarButtonItemStylePlain
                                                                           target:self
                                                                           action:@selector(done)];
  
  self.navigationItem.leftBarButtonItems = @[_workspaceNewBarButtonItem, _signInBarButtonItem];
  self.navigationItem.rightBarButtonItem = _doneBarButtonItem;
  
  _collectionView.backgroundColor = [[MezzanineStyleSheet sharedStyleSheet] grey20Color];
  [_collectionView setCollectionViewLayout:[self collectionViewLayoutForSize]];
  [_collectionView registerClass:[WorkspaceListCell class] forCellWithReuseIdentifier:@"WorkspaceCell"];
  
  _emptyWorkspaceListLabel.text = NSLocalizedString(@"Workspace List Empty List Text", nil);

  [_emptyWorkspaceListLabel setFont:[UIFont dinBoldOfSize:18.0]];
  [self updateButtonStates];
}


- (void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
  [self updateSortedWorkspaces];
}


- (void)viewDidAppear:(BOOL)animated
{
  [super viewDidAppear:animated];
  [self scrollToCurrentWorkspace:YES];
  
  // Analytics
  MezzanineAnalyticsManager *analyticsManager = [MezzanineAnalyticsManager sharedInstance];
  [analyticsManager tagScreen:analyticsManager.workspaceListViewScreen];
}


- (void)viewWillDisappear:(BOOL)animated
{
  [super viewWillDisappear:animated];
  
  // Analytics
  MezzanineAnalyticsManager *analyticsManager = [MezzanineAnalyticsManager sharedInstance];
  [analyticsManager tagWorkspaceStatusScreen];
}


- (void)dealloc
{
  [self setSystemModel:nil];
}


#pragma mark - Layout

- (void)viewDidLayoutSubviews
{
  [super viewDidLayoutSubviews];
  
  // On the iPhone the cells adapt to the width of the screen
  if (self.traitCollection.isCompactWidth)
  {
    // This is to prevent duplicate layout adjustment calls, which prevents scrollToCurrentWorkspace:
    // from working on an iPhone
    if (_collectionView.frame.size.width != _lastColletionViewLayoutWidth)
    {
      [_collectionView setCollectionViewLayout:[self collectionViewLayoutForSize] animated:YES];
      _lastColletionViewLayoutWidth = _collectionView.frame.size.width;
    }
  }
}


- (void)updateButtonStates
{
  [_signInBarButtonItem setTitle:(_systemModel.isSignedIn ? _systemModel.currentUsername : NSLocalizedString(@"Workspace List Sign In Button", nil))];
}


- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
  [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
    [_collectionView setCollectionViewLayout:[self collectionViewLayoutForSize] animated:NO];
  } completion:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
    [self scrollToCurrentWorkspace:YES];
  }];
}


- (void)willTransitionToTraitCollection:(UITraitCollection *)newCollection withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
  [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
    for (WorkspaceListCell *cell in [_collectionView visibleCells]) {
      [cell updatePopoverForTraitCollection:newCollection];
    }
  } completion:nil];
}


#pragma mark - Model

- (void)beginObservingSystemModel
{
  [_systemModel addObserver:self forKeyPath:@"isSignedIn" options:0 context:kSystemModelContext];
  [_systemModel addObserver:self forKeyPath:@"workspaces" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:kSystemModelContext];
  [_systemModel addObserver:self forKeyPath:@"currentWorkspace" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:kSystemModelContext];
  [_systemModel addObserver:self forKeyPath:@"currentWorkspace.isLoading" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:kSystemModelContext];

  for (MZWorkspace *workspace in _systemModel.workspaces)
    [self beginObservingWorkspace:workspace];
}

- (void)endObservingSystemModel
{
  [_systemModel removeObserver:self forKeyPath:@"isSignedIn"];
  [_systemModel removeObserver:self forKeyPath:@"workspaces"];
  [_systemModel removeObserver:self forKeyPath:@"currentWorkspace"];
  [_systemModel removeObserver:self forKeyPath:@"currentWorkspace.isLoading"];

  for (MZWorkspace *workspace in _systemModel.workspaces)
    [self endObservingWorkspace:workspace];
}

- (void)beginObservingWorkspace:(MZWorkspace*)workspace
{
  [workspace addObserver:self forKeyPath:@"name" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:kWorkspaceContext];
}

- (void)endObservingWorkspace:(MZWorkspace*)workspace
{
  [workspace removeObserver:self forKeyPath:@"name"];
}

- (void)updateSortedWorkspaces
{
  NSArray *sortDescriptors;
  NSArray *source;

  if (_systemModel.isCurrentUserSuperuser)
  {
    sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"owner" ascending:YES selector:@selector(caseInsensitiveCompare:)], [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(caseInsensitiveCompare:)]];

    source = _systemModel.workspaces;
  }
  else
  {
    sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(caseInsensitiveCompare:)]];
    source = [_systemModel workspacesWithOwner:_systemModel.currentUsername];
  }

  _sortedWorkspaces = [source sortedArrayUsingDescriptors:sortDescriptors];

  _emptyWorkspaceListLabel.hidden = _sortedWorkspaces.count ? YES : NO;
  
  [_collectionView reloadData];
}


- (void)setSystemModel:(MZSystemModel *)aModel
{
  if (_systemModel != aModel)
  {
    if (_systemModel)
      [self endObservingSystemModel];

    _systemModel = aModel;

    if (_systemModel)
      [self beginObservingSystemModel];
  }
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
  if (context == kSystemModelContext)
  {
    if ([keyPath isEqual:@"isSignedIn"])
    {
      _selectedOwner = _systemModel.currentUsername;
      [self updateButtonStates];
      [self updateSortedWorkspaces];
    }
    else if ([keyPath isEqual:@"currentWorkspace"])
    {
      MZWorkspace *previous = change[NSKeyValueChangeOldKey];
      if (previous != (id)[NSNull null])
      {
        WorkspaceListCell *cell = (WorkspaceListCell *)[self collectionViewCellForWorkspace:previous];
        if (cell)
          [cell updateCellBorder];
      }
      
      MZWorkspace *current = change[NSKeyValueChangeNewKey];
      if (current != (id)[NSNull null])
      {
        WorkspaceListCell *cell = (WorkspaceListCell *)[self collectionViewCellForWorkspace:current];
        if (cell)
          [cell updateCellBorder];
      }
    }
    else if ([keyPath isEqual:@"currentWorkspace.isLoading"])
    {
      WorkspaceListCell *cell = (WorkspaceListCell *)[self collectionViewCellForWorkspace:_systemModel.currentWorkspace];
      if (cell)
        [cell updateCellBorder];
    }
    else if ([keyPath isEqual:@"workspaces"])
    {
      NSNumber *kind = change[NSKeyValueChangeKindKey];
      if ([kind integerValue] == NSKeyValueChangeInsertion)  // Rows were added
      {
        NSArray *inserted = change[NSKeyValueChangeNewKey];

        // Observation and Collection view refreshment need to be done separately
        for (MZWorkspace *workspace in inserted)
          [self beginObservingWorkspace:workspace];
        
        [_collectionView performBatchUpdates:^{

          // Sort workspaces before obtaining index to get the correct index
          [self updateSortedWorkspaces];

          NSMutableArray *indexPaths = [NSMutableArray array];

          for (MZWorkspace *workspace in inserted)
          {
            NSUInteger sortedIndex = [_sortedWorkspaces indexOfObject:workspace];
            
            if (sortedIndex != NSNotFound)
              [indexPaths addObject:[NSIndexPath indexPathForItem:sortedIndex inSection:0]];
          }
          
          [_collectionView insertItemsAtIndexPaths:indexPaths];
          
        } completion:nil];
        
        if (_openNewlyCreatedWorkspace)
        {
          MZWorkspace *newWorkspace = _systemModel.workspaces[_systemModel.workspaces.count - 1];
          [_communicator.requestor requestWorkspaceOpenRequest:_systemModel.currentWorkspace.uid switchToUid:newWorkspace.uid];
          
          [self done];
          _openNewlyCreatedWorkspace = NO;
        }
      }
      else if ([kind integerValue] == NSKeyValueChangeRemoval)  // Rows were removed
      {
        NSArray *deleted = change[NSKeyValueChangeOldKey];

        // Observation and Collection view refreshment need to be done separately
        for (MZWorkspace *workspace in deleted)
          [self endObservingWorkspace:workspace];

        [_collectionView performBatchUpdates:^{

          NSMutableArray *indexPaths = [NSMutableArray array];

          for (MZWorkspace *workspace in deleted)
          {
            NSUInteger sortedIndex = [_sortedWorkspaces indexOfObject:workspace];
            NSUInteger sectionIndex = 0;

            if (sortedIndex != NSNotFound)
              [indexPaths addObject:[NSIndexPath indexPathForItem:sortedIndex inSection:sectionIndex]];
          }

          // Obtain deleted index path before sorting workspaces as it'll be gone from the array
          [self updateSortedWorkspaces];

          [_collectionView deleteItemsAtIndexPaths:indexPaths];
        } completion:nil];
      }
    }
  }
  else if (context == kWorkspaceContext)
  {
    MZWorkspace *workspace = (MZWorkspace*)object;

    if ([keyPath isEqual:@"name"])
    {
      NSUInteger oldIndex = [_sortedWorkspaces indexOfObject:workspace];
      NSUInteger sectionIndex = 0;

      [self updateSortedWorkspaces];

      NSUInteger newIndex = [_sortedWorkspaces indexOfObject:workspace];

      if (oldIndex != NSNotFound && newIndex != NSNotFound)
      {
        [_collectionView performBatchUpdates:^{
          [_collectionView moveItemAtIndexPath:[NSIndexPath indexPathForItem:oldIndex inSection:sectionIndex] toIndexPath:[NSIndexPath indexPathForItem:newIndex inSection:sectionIndex]];
        } completion:nil];
      }
      else
      {
        [_collectionView reloadData];
      }
    }
  }
  else
  {
    [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
  }
}


- (MZWorkspace *)workspaceAtIndexPath:(NSIndexPath *)indexPath
{
  MZWorkspace *workspace = _sortedWorkspaces[indexPath.row];
  return workspace;
}


#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section;
{
  return [_sortedWorkspaces count];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
  return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
  WorkspaceListCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"WorkspaceCell" forIndexPath:indexPath];
  cell.systemModel = _systemModel;
  cell.communicator = [self communicator];

  MZWorkspace *workspace = [self workspaceAtIndexPath:indexPath];
  if (workspace)
    cell.workspace = workspace;

  return cell;
}


- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
  return YES;
}


- (void)collectionView:(UICollectionView *)aCollectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
  WorkspaceListCell *cell = (WorkspaceListCell *) [aCollectionView cellForItemAtIndexPath:indexPath];
  [cell updateCellBorder];
}


- (void)collectionView:(UICollectionView *)aCollectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
  WorkspaceListCell *cell = (WorkspaceListCell *) [aCollectionView cellForItemAtIndexPath:indexPath];
  [cell updateCellBorder];
}


#pragma mark - UICollectionViewDelegate

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
  NSInteger itemsPerRow = self.view.window.traitCollection.isiPadRegularWidth ? 2 : 1;
  UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout *)collectionViewLayout;
  CGFloat availableWidth = CGRectGetWidth(self.collectionView.frame) - (layout.sectionInset.left + layout.sectionInset.right + layout.minimumInteritemSpacing);
  
  return CGSizeMake(availableWidth/itemsPerRow, 80.0);
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
  MZWorkspace *tappedWorkspace = [self workspaceAtIndexPath:indexPath];
  
  MZWorkspace *currentWorkspace = _systemModel.currentWorkspace;
  BOOL currentWorkspaceIsUnsavedAndHasContent = (!currentWorkspace.isSaved && currentWorkspace.hasContent);
  if (currentWorkspaceIsUnsavedAndHasContent)
  {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Workspace Open Dialog Title", nil)
                                                                             message:NSLocalizedString(@"Workspace Open Dialog Message", nil)
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Workspace Open Dialog Cancel Button Title", nil)
                                                           style:UIAlertActionStyleCancel
                                                         handler:nil];
    UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Workspace Open Dialog Confirm Button Title", nil)
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * _Nonnull action) {
                                                           [self.communicator.requestor requestWorkspaceOpenRequest:currentWorkspace.uid switchToUid:tappedWorkspace.uid];
                                                           [self done];
                                                         }];
    [alertController addAction:cancelAction];
    [alertController addAction:confirmAction];
    [self presentViewController:alertController animated:YES completion:nil];
  }
  else if (tappedWorkspace == currentWorkspace)
  {
    [self done];  // Just dismiss the view
  }
  else
  {
    [self.communicator.requestor requestWorkspaceOpenRequest:currentWorkspace.uid switchToUid:tappedWorkspace.uid];
    [self done];
  }
}


#pragma mark - UICollectionView

- (UICollectionViewFlowLayout *) collectionViewLayoutForSize
{
  return [WorkspaceListCollectionViewLayout new];
}


- (UICollectionViewCell*)collectionViewCellForWorkspace:(MZWorkspace *)workspace
{
  NSInteger index = [_sortedWorkspaces indexOfObject:workspace];
  if (index != NSNotFound)
    return [_collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0]];
  return nil;
}


- (void)scrollToWorkspace:(MZWorkspace *)workspace animated:(BOOL)animated
{
  NSInteger index = [_sortedWorkspaces indexOfObject:workspace];
  if (index != NSNotFound)
  {
    UICollectionViewScrollPosition scrollPosition = UICollectionViewScrollPositionCenteredVertically;
    [_collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0]
                                atScrollPosition:scrollPosition
                                        animated:animated];
  }
}


- (void)scrollToCurrentWorkspace:(BOOL)animated
{
  [self scrollToWorkspace:_systemModel.currentWorkspace animated:animated];
}


#pragma mark - Actions

- (void)newWorkspace
{
  CGFloat popoverWidth = self.traitCollection.isRegularWidth ? 320 : 300;
  MZWorkspace *tempWorkspace = [[MZWorkspace alloc] init];
  tempWorkspace.name =  [MZWorkspace createDefaultWorkspaceName];
  
  WorkspaceEditorViewController *controller = [[WorkspaceEditorViewController alloc] initWithWorkspace:tempWorkspace];
  controller.preferredContentSize = CGSizeMake(popoverWidth, 178);
  controller.doneButtonTitle = NSLocalizedString(@"Workspace List Create Button", nil);
  controller.hintText = (_systemModel.currentUsername != nil) ? NSLocalizedString(@"Worskpace List Save As Private Hint", nil) : NSLocalizedString(@"Worskpace List Save As Public Hint", nil);
  controller.autoHighlightNameField = YES;
  
  FPPopoverController *popoverController = [[FPPopoverController alloc] initWithContentViewController:controller delegate:self];
  [popoverController stylizeAsActionSheet];

  __weak typeof(self) __self = self;
  __weak WorkspaceEditorViewController *__controller = controller;
  __weak FPPopoverController *__popoverController = popoverController;
  
  controller.requestSave = ^{
    MZWorkspace *currentWorkspace = _systemModel.currentWorkspace;
    BOOL currentWorkspaceIsUnsavedAndHasContent = (!currentWorkspace.isSaved && currentWorkspace.hasContent);
    if (currentWorkspaceIsUnsavedAndHasContent)
    {
      UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Workspace New Dialog Title", nil)
                                                                               message:NSLocalizedString(@"Workspace New Dialog Message", nil)
                                                                        preferredStyle:UIAlertControllerStyleAlert];
      UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Workspace New Dialog Cancel Button Title", nil)
                                                             style:UIAlertActionStyleCancel
                                                           handler:nil];
      UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Workspace New Dialog Confirm Button Title", nil)
                                                              style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * _Nonnull action) {
                                                              [__self performNewWorkspace:__controller.nameTextField.text];
                                                              [__popoverController dismissPopoverAnimated:YES];
                                                            }];
      [alertController addAction:cancelAction];
      [alertController addAction:confirmAction];
      [self presentViewController:alertController animated:YES completion:nil];
    }
    else
    {
      [__self performNewWorkspace:__controller.nameTextField.text];
      [__popoverController dismissPopoverAnimated:YES];
    }
  };
  controller.requestCancel = ^{
    [__popoverController dismissPopoverAnimated:YES];
  };
  
  UIView *view = [_workspaceNewBarButtonItem valueForKey:@"view"];
 [popoverController presentPopoverFromRect:view.frame inView:view.superview permittedArrowDirections:FPPopoverArrowDirectionUp animated:YES];
  currentPopoverController = popoverController;
}


- (void)performNewWorkspace:(NSString*)workspaceName
{
  NSArray *ownersArray = _systemModel.currentUsername ? @[_systemModel.currentUsername] : @[];
  MZTransaction *transaction = [_communicator.requestor requestWorkspaceCreateRequest:workspaceName owners:ownersArray];
  
  transaction.successBlock = ^{
    // Note that we can't directly open the new workspace here before
    // successBlock is called *before* the protein handler, which is probably
    // a architecture choice that needs to be revisited.
    _openNewlyCreatedWorkspace = YES;
  };
}


- (void)signIn
{
  UIViewController *controller = nil;
  __weak typeof(self) __self = self;

  if (!_systemModel.currentUsername || [_systemModel.currentUsername isEqualToString:@""])
  {
    controller = [[UserSignInViewController alloc] initWithNibName:@"UserSignInViewController" bundle:nil];
    controller.preferredContentSize = CGSizeMake(245.0, 220.0);
    
    ((UserSignInViewController *)controller).signInRequested = ^(NSString *user, NSString *password){
      
       MZTransaction *transaction = [__self.communicator.requestor requestClientSignInWithUsername:user password:password];
      
      transaction.successBlock = ^{
        [currentPopoverController dismissPopoverAnimated:YES];
      };
      
      transaction.failedBlock = ^(NSError *error) {
        NSString *summary = (error.userInfo)[@"summary"];
        NSString *description = (error.userInfo)[@"description"];
        NSString *title = summary ? summary : NSLocalizedString(@"Workspace List Sign In Error Dialog Title", nil);
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:description preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Generic Button Title Okay", nil)
                                                               style:UIAlertActionStyleCancel
                                                             handler:nil];
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:nil];
      };
      
    };
  }
  else
  {
    controller = [[ButtonMenuViewController alloc] init];
    NSMutableArray *items = [[NSMutableArray alloc] init];
    [items addObject:[ButtonMenuItem itemWithTitle:NSLocalizedString(@"Workspace List Sign Out Button", nil) action:^{
      [__self.communicator.requestor requestClientSignOutRequest];
      [currentPopoverController dismissPopoverAnimated:YES];
    }]];

    ((ButtonMenuViewController *)controller).menuItems = items;
  }

  if (_systemModel.currentUsername || self.traitCollection.isiPad)
  {
    currentPopoverController = [[FPPopoverController alloc] initWithContentViewController:controller delegate:self];
    [currentPopoverController stylizeAsActionSheet];
    
    
    UIView *view = [_signInBarButtonItem valueForKey:@"view"];
    [currentPopoverController presentPopoverFromRect:view.frame inView:view.superview permittedArrowDirections:FPPopoverArrowDirectionUp animated:YES];
  }
  else
  {
    ((UserSignInViewController *)controller).signInRequested = ^(NSString *user, NSString *password) {
      MZTransaction *transaction = [__self.communicator.requestor requestClientSignInWithUsername:user password:password];
      
      transaction.successBlock = ^{
        [__self.navigationController popViewControllerAnimated:YES];
      };
      
      transaction.failedBlock = ^(NSError *error) {
        NSString *summary = (error.userInfo)[@"summary"];
        NSString *description = (error.userInfo)[@"description"];
        NSString *title = summary ? summary : NSLocalizedString(@"Workspace List Sign In Error Dialog Title", nil);
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:description preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Generic Button Title Okay", nil)
                                                               style:UIAlertActionStyleCancel
                                                             handler:nil];
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:nil];
      };
    };
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Mezzanine", nil) style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.navigationController pushViewController:controller animated:YES];
  }
}


- (void)done
{
  if ([self.delegate respondsToSelector:@selector(dismissWorkspaceListViewController:)])
    [self.delegate dismissWorkspaceListViewController:self];
  else
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark UIPopoverControllerDelegate

- (void)popoverControllerDidDismissPopover:(FPPopoverController *)popoverController
{
  if (currentPopoverController)
  {
    currentPopoverController = nil;
  }
}

@end
