//
//  MezzanineInfopresenceManager.h
//  Mezzanine
//
//  Created by miguel on 10/6/15.
//  Copyright (c) 2015 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MezzanineInfopresenceManager : NSObject

@property (nonatomic, strong) MZSystemModel *systemModel;
@property (nonatomic, strong) MZCommunicator *communicator;


- (void)prepareOverlayWindowUsingMainWindow:(UIWindow*)mainWindow;
- (void)hideWindow:(BOOL)hidden;
- (void)reset;

@end
