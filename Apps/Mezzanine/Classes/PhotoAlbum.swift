//
//  PhotoAlbum.swift
//  Mezzanine
//
//  Created by miguel on 3/1/17.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import UIKit
import Photos

class PhotoAlbum: NSObject {
  
  var assetCollection: PHAssetCollection!
  var title: String!
  var coverPhoto: UIImage?
  var count: Int = NSNotFound
  var smartAlbum: Bool = false
  var photoAssets: Array<PhotoAsset>?
  var amountSelected: Int {
    get {
      if let photoAssets = photoAssets {
        return photoAssets.filter({ $0.selected == true} ).count
      }
      return 0
    }
  }
  
  init(assetCollection: PHAssetCollection, title: String, smart: Bool) {
    self.assetCollection = assetCollection
    self.title = title
    self.smartAlbum = smart
  }
}
