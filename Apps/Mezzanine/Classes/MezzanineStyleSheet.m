//
//  MezzanineStyleSheet.m
//  Mezzanine
//
//  Created by Zai Chang on 5/25/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import "MezzanineStyleSheet.h"
#import "Three20Style.h"
#import "TTView.h"
#import "UIColorAdditions.h"
#import "NSString+VersionChecking.h"
#import "Mezzanine-Swift.h"

// Consolidation of Mezzanine-specific colors and styles

@interface MezzanineStyleSheet ()

@property (nonatomic, strong) UIImage *navigationBarBackgroundImage;
@property (nonatomic, strong) UIImage *buttonBackgroundImage;
@property (nonatomic, strong) UIImage *buttonHighlightedBackgroundImage;
@property (nonatomic, strong) UIImage *segmentedControlSeparatorImage;
@property (nonatomic, strong) UIImage *backButtonBackgroundImage;
@property (nonatomic, strong) UIImage *backButtonHighlightedBackgroundImage;

@end

@implementation MezzanineStyleSheet

+(MezzanineStyleSheet*) sharedStyleSheet
{
  TTStyleSheet *globalStyleSheet = [TTStyleSheet globalStyleSheet];
  if ([globalStyleSheet isKindOfClass:[MezzanineStyleSheet class]])
    return (MezzanineStyleSheet*) globalStyleSheet;
  return nil;
}


-(id) init
{
  self = [super init];
  if (self)
  {

    BOOL teamworkUI = [SettingsFeatureToggles sharedInstance].teamworkUI;
    
    self.defaultSystemBlue = RGBCOLOR(0.0, 122.0, 255.0);
    self.defaultHoverColor = RGBCOLOR(112, 129, 145);
    self.defaultHighlightColor = RGBCOLOR(184, 213, 239); // Equivalent to HSV (208, 23, 94)
		
    self.mediumFillColor = RGBCOLOR(89, 89, 99);
    self.darkFillColor = RGBCOLOR(49, 49, 59);  // aka primary band color
    self.darkerFillColor = RGBCOLOR(39, 39, 49);  // aka another dark fill
    self.darkestFillColor = RGBCOLOR(29, 29, 39); // aka dark fill
    
    self.textNormalColor = [UIColor colorWithWhite:0.9 alpha:1.0];
    self.textHighlightedColor = self.mediumFillColor;
    self.textSecondaryColor = RGBCOLOR(113, 113, 119);
    
    self.buttonBorderColor = RGBCOLOR(89, 89, 89);
    self.buttonHighlightBackgroundColor = RGBCOLOR(69, 69, 79);
    
    self.feldMutedBorderColor = [UIColor colorWithWhite:0.5 alpha:1.0];
    self.feldBorderInCollaborationColor = self.defaultHighlightColor;
    
    self.slideBackgroundColor = teamworkUI ? [UIColor clearColor] : RGBCOLOR(59, 58, 72);
    self.slideBorderColor = self.mediumFillColor;
    self.assetBackgroundColor = [UIColor clearColor];
    self.shielderBorderColor = self.mediumFillColor;
    self.shielderBackgroundColor = [UIColor clearColor]; // RGBACOLOR(0, 0, 0, 0.2);

    self.ovumBackgroundColor = [UIColor colorWithWhite:0.0 alpha:0.2];
    self.ovumHoverHighlightColor = [self.defaultHighlightColor colorWithAlphaComponent:0.66];
    
    self.videoPlaceholderBackgroundColor = [UIColor colorWithRed:29.0/255.0 green:29.0/255.0 blue:39.0/255.0 alpha:225.0/255.0];
    self.videoPlaceholderPrimaryColor = [UIColor colorWithWhite:0.8 alpha:1.0];
    self.videoPlaceholderSecondaryColor = [UIColor colorWithWhite:0.5 alpha:1.0];
		
		self.superHighlightColor = RGBCOLOR(255, 120, 90);
		self.superHighlightColorActive = RGBCOLOR(251, 142, 116);

    self.greenHighlightColor = RGBCOLOR(81, 188, 139);
    self.yellowHighlightColor = RGBCOLOR(255, 182, 78);
    self.redHighlightColor = RGBCOLOR(237, 52, 83);
    self.redHighlightColorPress = RGBCOLOR(198, 50, 77);

    self.selectedBlueColor = RGBCOLOR(65, 70, 112);
    self.horizonBlueColor = RGBCOLOR(132, 152, 182);
    self.horizonBlueColorActive = RGBCOLOR(178, 215, 255);
    self.darkHorizonBlueColor = RGBCOLOR(105, 116, 147);
    self.darkHorizonBlueColorDisabled = RGBCOLOR(80, 80, 98);
    self.lightBlueRoomColor = RGBCOLOR(176, 212, 250);

    self.declineActionColorNormal = self.redHighlightColor;
    self.declineActionColorPress = self.redHighlightColorPress;
    
    self.greenIndicatorColor = RGBCOLOR(73,206,139);

    self.grey10Color = RGBCOLOR(10, 10, 28);
    self.grey20Color = RGBCOLOR(20, 20, 38);
    self.grey30Color = RGBCOLOR(30, 30, 48);
    self.grey40Color = RGBCOLOR(40, 40, 58);
    self.grey50Color = RGBCOLOR(50, 50, 68);
    self.grey60Color = RGBCOLOR(60, 60, 78);
    self.grey70Color = RGBCOLOR(70, 70, 88);
    self.grey80Color = RGBCOLOR(80, 80, 98);
    self.grey90Color = RGBCOLOR(90, 90, 108);
    self.grey110Color = RGBCOLOR(110, 110, 122);
    self.grey130Color = RGBCOLOR(130, 130, 140);
    self.grey150Color = RGBCOLOR(150, 150, 160);
    self.grey170Color = RGBCOLOR(170, 170, 175);
    self.grey190Color = RGBCOLOR(190, 190, 195);

		[self initializeAppearance];
  }
  
  return self;
}


-(void) initializeAppearance
{
  self.darkestFillColor = self.grey10Color;

  UIFont *textAttributesFont = UIDevice.currentDevice.isIPad ? [UIFont dinMediumOfSize:16.0] : [UIFont dinBoldOfSize:14.0];
  NSDictionary *textAttributes = @{NSFontAttributeName: textAttributesFont,
                                  NSForegroundColorAttributeName: self.textNormalColor,
                                   };
  NSDictionary *highlightedTextAttributes = @{NSFontAttributeName: textAttributesFont,
                                             NSForegroundColorAttributeName: self.textHighlightedColor,
                                              };
  
  // UINavigationBar
  [[UINavigationBar appearance] setBackgroundImage:self.navigationBarBackgroundImage forBarMetrics:UIBarMetricsDefault];
  [UINavigationBar appearance].tintColor = self.defaultHighlightColor;
  
  CGFloat navigationBarTitleFontSize = UIDevice.currentDevice.isIPad ? 21.0 : 18.0;
  [[UINavigationBar appearance] setTitleTextAttributes:@{NSFontAttributeName: [UIFont dinLightOfSize:navigationBarTitleFontSize],
                                                         NSForegroundColorAttributeName: self.textNormalColor,
                                                         }];
  


  // UIBarButtonItem
  id barButtonItemAppearance = [UIBarButtonItem appearance];
  NSDictionary *barButtonTextAttributes = @{NSFontAttributeName:[self buttonFont],
                                            NSForegroundColorAttributeName:[UIColor whiteColor] };
  [barButtonItemAppearance setTitleTextAttributes:barButtonTextAttributes forState:UIControlStateNormal];
  
  NSDictionary *disabledBarButtonTextAttributes = @{NSFontAttributeName: [self buttonFont],
                                                    NSForegroundColorAttributeName: self.mediumFillColor,
                                                    };
  [barButtonItemAppearance setTitleTextAttributes:disabledBarButtonTextAttributes forState:UIControlStateDisabled];

  NSDictionary *highlightedBarButtonTextAttributes = @{NSFontAttributeName: [self buttonFont],
                                            NSForegroundColorAttributeName: self.textHighlightedColor,
                                                       };
  [barButtonItemAppearance setTitleTextAttributes:highlightedBarButtonTextAttributes forState:UIControlStateHighlighted];


  // UIToolbar
  [UIToolbar appearance].tintColor = self.defaultHighlightColor;
  [[UIToolbar appearance] setBackgroundImage:self.navigationBarBackgroundImage forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];


  // UISegmentedControl
  [[UISegmentedControl appearance] setBackgroundImage:self.buttonBackgroundImage forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
  [[UISegmentedControl appearance] setBackgroundImage:self.buttonHighlightedBackgroundImage forState:UIControlStateHighlighted barMetrics:UIBarMetricsDefault];
  [[UISegmentedControl appearance] setBackgroundImage:self.buttonSelectedBackgroundImage forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
  [[UISegmentedControl appearance] setDividerImage:self.segmentedControlSeparatorImage forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
  [[UISegmentedControl appearance] setTitleTextAttributes:textAttributes forState:UIControlStateNormal];
  [[UISegmentedControl appearance] setTitleTextAttributes:highlightedTextAttributes forState:UIControlStateHighlighted];
  [[UISegmentedControl appearance] setTitleTextAttributes:highlightedTextAttributes forState:UIControlStateSelected];

  // UISlider
  TTStyle *minimumTrackStyleNormal = [self sliderMinimumTrackStyle:UIControlStateNormal];
  UIImage *minimumTrackImageNormal = [[self imageWithTTStyle:minimumTrackStyleNormal size:CGSizeMake(24, 24)] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
  [[UISlider appearance] setMinimumTrackImage:minimumTrackImageNormal forState:UIControlStateNormal];
  TTStyle *maximumTrackStyleNormal = [self sliderMaximumTrackStyle:UIControlStateNormal];
  UIImage *maximumTrackImageNormal = [[self imageWithTTStyle:maximumTrackStyleNormal size:CGSizeMake(24, 24)] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
  [[UISlider appearance] setMaximumTrackImage:maximumTrackImageNormal forState:UIControlStateNormal];
  
  // Extra padding is added to the thumb because DeckViewSlider uses the same style, but expects a different inset
  TTStyle *thumbStyleNormal = [TTInsetStyle styleWithInset:UIEdgeInsetsMake(1, 0, 1, 0) next:
                               [self sliderThumbStyle:UIControlStateNormal]];
  UIImage *thumbStyleNormalImage = [[self imageWithTTStyle:thumbStyleNormal size:CGSizeMake(24, 24)] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
  [[UISlider appearance] setThumbImage:thumbStyleNormalImage forState:UIControlStateNormal];
  
  // UIButton
  UIButton *buttonAppearance = [UIButton appearance];
  [buttonAppearance.titleLabel setFont:[self buttonFont]];
  [buttonAppearance setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
  [buttonAppearance setTitleColor:self.mediumFillColor forState:UIControlStateDisabled];
  [buttonAppearance setTitleColor:self.textHighlightedColor forState:UIControlStateHighlighted];
  [buttonAppearance setTintColor:[UIColor whiteColor]];

  // UISwitch
  UISwitch *switchAppearance = [UISwitch appearance];
  switchAppearance.onTintColor = self.greenHighlightColor;
}


-(CGFloat) buttonFontSize
{
  return 16.0;
}

-(CGFloat) buttonSmallFontSize
{
  return 12.0;
}

-(CGFloat) toolbarButtonFontSize
{
  return 16.0;
}

- (UIFont*)buttonFont
{
  UIFont *font = [UIFont dinOfSize:[self buttonFontSize]];
  return font;
}

- (UIFont*)buttonSmallFont
{
  UIFont *font = [UIFont dinOfSize:[self buttonSmallFontSize]];
  return font;
}

- (UIFont*)toolbarButtonFont
{
  UIFont *font = [UIFont dinOfSize:[self toolbarButtonFontSize]];
  return font;
}


#pragma mark - Styles that override OBStyleSheet

-(TTStyle*) darkRoundedButton:(UIControlState)state
{
	return [self matteButton:state
                     shape:[TTRoundedRectangleShape shapeWithRadius:5.0]
                 tintColor:self.feldBackingColor
               borderColor:self.buttonBorderColor
                 textColor:RGBCOLOR(255, 255, 255)];
}


#pragma mark - Styles

-(TTStyle*) navigationBarButton:(UIControlState)state
{
  TTStyle *style = [self matteButton:state
                               shape:[TTRoundedRectangleShape shapeWithRadius:5.0]
                           tintColor:RGBCOLOR(0, 0, 0)
                         borderColor:self.buttonBorderColor
                           textColor:RGBCOLOR(255, 255, 255)];
  TTTextStyle *textStyle = [style firstStyleOfClass:[TTTextStyle class]];
  textStyle.font = [UIFont dinBoldOfSize:13.0];
  
  if (!(state & UIControlStateDisabled) && (state & UIControlStateSelected))
  {
    TTSolidFillStyle *fillStyle = [style firstStyleOfClass:[TTSolidFillStyle class]];
    
    textStyle.color = self.textHighlightedColor;
    fillStyle.color = RGBCOLOR(128, 128, 128);
  }
  
  return style;
}


-(TTStyle*) highlightedRoundedButton:(UIControlState)state
{
	return [self matteButton:state
                     shape:[TTRoundedRectangleShape shapeWithRadius:5.0]
                 tintColor:self.buttonHighlightBackgroundColor
               borderColor:self.buttonBorderColor
                 textColor:RGBCOLOR(255, 255, 255)];
}


-(TTStyle*) selectedRoundedButton:(UIControlState)state
{
	return [self matteButton:state
                     shape:[TTRoundedRectangleShape shapeWithRadius:5.0]
                 tintColor:self.buttonHighlightBackgroundColor
               borderColor:self.buttonBorderColor
                 textColor:RGBCOLOR(255, 255, 255)];
}


-(TTStyle*) disabledRoundedButton:(UIControlState)state
{
	return [self matteButton:state
                     shape:[TTRoundedRectangleShape shapeWithRadius:5.0]
                 tintColor:[self.buttonHighlightBackgroundColor colorWithAlphaComponent:0.5]
               borderColor:self.buttonBorderColor
                 textColor:self.buttonBorderColor];
}


-(TTStyle*) tableViewCellAccessoryButton:(UIControlState)state
{
	TTStyle *baseStyle = [self matteButton:state
                                   shape:[TTRoundedRectangleShape shapeWithRadius:3.0]
                               tintColor:[UIColor clearColor]
                             borderColor:self.buttonBorderColor
                               textColor:[UIColor colorWithRed:0.66 green:0.66 blue:0.66 alpha:1.0]];
  TTStyle *style = [TTInsetStyle styleWithInset:UIEdgeInsetsMake(3, 3, 3, 3) next:baseStyle];
  TTBoxStyle *boxStyle = [style firstStyleOfClass:[TTBoxStyle class]];
  if (boxStyle)
  {
    UIImage *image = (state == UIControlStateDisabled) ? [UIImage imageNamed:@"Options-Disabled.png"] : [UIImage imageNamed:@"Options.png"];
    // Replace the default text style with an image
    //boxStyle.margin = UIEdgeInsetsMake(2, 2, 2, 2);
    boxStyle.next = [TTImageStyle styleWithImage:image next:nil];
  }
  return style;
}


-(TTStyle*) sliderMinimumTrackStyle:(UIControlState)state
{
  // TODO - add styling for various control state
  TTShape *shape = [TTRoundedRectangleShape shapeWithRadius:5.0];
	return [TTInsetStyle styleWithInset:UIEdgeInsetsMake(2, 0, 2, 0) next:
          [TTShapeStyle styleWithShape:shape next:
           [TTSolidBorderStyle styleWithColor:self.buttonBorderColor width:1.0 next:
            [TTSolidFillStyle styleWithColor:RGBCOLOR(89, 89, 99) next:
             nil]]]];
}


-(TTStyle*) sliderMaximumTrackStyle:(UIControlState)state
{
  // TODO - add styling for various control state
  TTShape *shape = [TTRoundedRectangleShape shapeWithRadius:5.0];
	return [TTInsetStyle styleWithInset:UIEdgeInsetsMake(2, 0, 2, 0) next:
          [TTShapeStyle styleWithShape:shape next:
           [TTSolidBorderStyle styleWithColor:self.buttonBorderColor width:1.0 next:
            [TTSolidFillStyle styleWithColor:self.feldBackingColor next:
             nil]]]];
}


// Used for DeckViewSlider not in UISlider
-(TTStyle*) sliderTrackStyle:(UIControlState)state
{
  UIColor *borderColor = self.grey50Color;
  UIColor *fillColor = self.grey50Color;
  CGFloat radius = 3.0;
  CGFloat border = 0.0;

  // TODO - add styling for various control state
  TTShape *shape = [TTRoundedRectangleShape shapeWithRadius:radius];
	return [TTShapeStyle styleWithShape:shape next:
          [TTSolidBorderStyle styleWithColor:borderColor width:border next:
           [TTSolidFillStyle styleWithColor:fillColor next:
            nil]]];
}

// Used for DeckViewSlider not in UISlider
-(TTStyle*) sliderThumbStyle:(UIControlState)state
{
  UIColor *fillColor;

  if (state == UIControlStateHighlighted)
    fillColor = self.darkFillColor;
  else
    fillColor = self.mediumFillColor;

  CGFloat radius = 3.6;

  // TODO - add styling for various control state
  TTShape *shape = [TTRoundedRectangleShape shapeWithRadius:radius];
	return [TTInsetStyle styleWithInset:UIEdgeInsetsMake(2.0, 2.0, 2.0, 2.0) next:
          [TTShapeStyle styleWithShape:shape next:
           [TTSolidFillStyle styleWithColor:fillColor next:
            nil]]];
}


-(TTStyle*) darkTopBorderedButton:(UIControlState)state
{
  UIColor *backgroundColor = self.darkestFillColor;
  UIColor *fontColor = self.defaultHighlightColor;
  if (state == UIControlStateHighlighted)
  {
    backgroundColor = self.darkFillColor;
  }
  else if (state == UIControlStateDisabled)
  {
    fontColor = self.mediumFillColor;
  }

	return [TTShapeStyle styleWithShape:[TTRectangleShape shape] next:
          [TTFourBorderStyle styleWithTop:self.buttonBorderColor width:1.0 next:
           [TTSolidFillStyle styleWithColor:backgroundColor next:
            [TTBoxStyle styleWithPadding:UIEdgeInsetsMake(3, 6, 3, 6) next:
             [TTTextStyle styleWithFont:[self buttonFont] color:fontColor next:
              nil]]]]];
}


-(TTStyle*) lightTopBorderedButton:(UIControlState)state
{
  UIColor *backgroundColor = [UIColor whiteColor];
  UIColor *fontColor = _grey90Color;
  if (state == UIControlStateHighlighted)
  {
    backgroundColor = _grey190Color;
  }
  else if (state == UIControlStateDisabled)
  {
    fontColor = _grey190Color;
  }

  return [TTShapeStyle styleWithShape:[TTRectangleShape shape] next:
          [TTFourBorderStyle styleWithTop:_grey190Color width:(1.0 / [UIScreen mainScreen].scale) next:
           [TTSolidFillStyle styleWithColor:backgroundColor next:
            [TTBoxStyle styleWithPadding:UIEdgeInsetsMake(3, 6, 3, 6) next:
             [TTTextStyle styleWithFont:[UIFont dinMediumOfSize:16.0] color:fontColor next:
              nil]]]]];

}


-(TTStyle*) darkToggleButton:(UIControlState)state
{
  UIColor *backgroundColor = [UIColor clearColor];
  UIColor *fontColor = self.defaultHighlightColor;
  UIColor *borderColor = [UIColor clearColor];

  if (state == UIControlStateSelected)
  {
    backgroundColor = self.defaultHoverColor;
    fontColor = self.darkestFillColor;
  }
  else if (state == UIControlStateDisabled)
  {
    fontColor = self.mediumFillColor;
  }
  
	return [TTShapeStyle styleWithShape:[TTRoundedRectangleShape shapeWithRadius:5.0] next:
          [TTSolidBorderStyle styleWithColor:borderColor width:1.0 next:
          [TTSolidFillStyle styleWithColor:backgroundColor next:
           [TTBoxStyle styleWithPadding:UIEdgeInsetsMake(1, 6, 1, 6) next:
            [TTTextStyle styleWithFont:[self buttonFont] color:fontColor next:
             nil]]]]];
}


#pragma mark - Label Styles

-(void) stylizeHeaderTitleLabel:(UILabel*)label
{
	if (label == nil)
		return;
	
	UIFont *font = [UIFont dinOfSize:18.0];
	label.font = font;
	label.textColor = _textNormalColor;
}


-(void) stylizeSectionHeaderLabel:(UILabel*)label
{
	if (label == nil)
		return;
	
  UIFont *font = [UIFont dinOfSize:16.0];
  label.font = font;
  label.textColor = self.lightBlueRoomColor;
}


-(void) stylizeParticipantSectionHeaderLabel:(UILabel*)label
{
  if (label == nil)
    return;

  UIFont *font = [UIFont dinMediumOfSize:16.0];
  label.font = font;
  label.textColor = [self grey150Color];
}


-(void) stylizeInformativeTitleLabelInWhitePopover:(UILabel*)label
{
  if (label == nil)
    return;
  label.font = [UIFont dinBoldOfSize:[self buttonFontSize]];
  label.textColor = _grey90Color;
}

-(void) stylizeInformativeDescriptionLabelInWhitePopover:(UILabel*)label
{
  if (label == nil)
    return;
  label.font = [UIFont dinMediumOfSize:[self buttonFontSize]];
  label.textColor = _grey90Color;
}

-(void) stylizeInformativeTitleLabel:(UILabel*)label
{
	if (label == nil)
		return;
	label.font = [UIFont dinBoldOfSize:[self buttonFontSize]];
	label.textColor = _grey190Color;
}

-(void) stylizeInformativeDescriptionLabel:(UILabel*)label
{
	if (label == nil)
		return;
	label.font = [UIFont dinOfSize:[self buttonFontSize]];
	label.textColor = _grey190Color;
}

-(void) stylizeInformativeLightTitleLabel:(UILabel*)label
{
  if (label == nil)
    return;
  label.font = [UIFont dinMediumOfSize:[self buttonFontSize]];
  label.textColor = _grey110Color;
}

-(void) stylizeInformativeLightDescriptionLabel:(UILabel*)label
{
  if (label == nil)
    return;
  label.font = [UIFont dinMediumOfSize:[self buttonFontSize]];
  label.textColor = _grey150Color;
}

-(void) stylizeDimInformativeTitleLabel:(UILabel*)label
{
	if (label == nil)
		return;
	label.font = [UIFont dinBoldOfSize:13.0];
	label.textColor = _grey150Color;
}

-(void) stylizeDimInformativeDescriptionLabel:(UILabel*)label
{
	if (label == nil)
		return;
	label.font = [UIFont dinMediumOfSize:13.0];
	label.textColor = _grey150Color;
}


-(void) stylizeLabel:(UILabel*)label withPanelTitle:(NSString*)title
{
  if (label == nil)
    return;
  
  UIFont *font = [UIFont dinMediumOfSize:12.0];
  
  CGFloat kerning = 1.3;
  
  NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:title];
  [attributedText addAttribute:NSKernAttributeName
                         value:[NSNumber numberWithFloat:kerning]
                         range:NSMakeRange(0, [title length])];
  label.attributedText = attributedText;
  label.font = font;
  label.textColor = [UIColor whiteColor];
}


-(void) stylizeLabel:(UILabel*)label withTitle:(NSString*)title
{
	if (label == nil)
		return;
	
  UIFont *font = [UIFont dinOfSize:15.0];
  CGFloat kerning = 1.5;

  NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:title];
  [attributedText addAttribute:NSKernAttributeName
                         value:[NSNumber numberWithFloat:kerning]
                         range:NSMakeRange(0, [title length])];
  label.attributedText = attributedText;
  label.textColor = [UIColor whiteColor];
  label.font = font;
}


#pragma mark - Text Button Styles

-(void) stylizeButton:(UIButton*)button
{
  UIFont *font = [self buttonFont];
  button.titleLabel.font = font;
  button.titleEdgeInsets = UIEdgeInsetsMake(2, 0, 2, 0);
  button.backgroundColor = [UIColor clearColor];
}

-(void) stylizeButtonForWhitePopover:(UIButton*)button
{
	[self stylizeButton:button];
	button.tintColor = _grey90Color;
	[button setTitleColor:_grey90Color forState:UIControlStateNormal];
	[button setTitleColor:_grey90Color forState:UIControlStateSelected];
	[button setTitleColor:_grey170Color forState:UIControlStateDisabled];
	[button setBackgroundImage:[self imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
	[button setBackgroundImage:[self imageWithColor:_grey190Color] forState:UIControlStateSelected];
}

-(void) stylizeSmallFontButton:(UIButton*)button
{
  UIFont *font = [self buttonSmallFont];
  button.titleLabel.font = font;
  button.titleEdgeInsets = UIEdgeInsetsMake(2, 0, 2, 0);
  button.backgroundColor = [UIColor clearColor];
}

-(void) stylizeActionBarButton:(UIButton*)button
{
  UIFont *font = [UIFont dinMediumOfSize:[self buttonSmallFontSize]];
  button.titleLabel.font = font;
  button.backgroundColor = self.grey50Color;
}

-(void) stylizeCallToActionButton:(UIButton*)button
{
	UIFont *font = [UIFont dinMediumOfSize:14.0];
	button.titleLabel.font = font;
	button.titleEdgeInsets = UIEdgeInsetsMake(2, 0, 2, 0);

	[button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[button setTitleColor:self.textSecondaryColor forState:UIControlStateDisabled];

	[button setBackgroundImage:[self imageWithColor:_superHighlightColor] forState:UIControlStateNormal];
	[button setBackgroundImage:[self imageWithColor:_superHighlightColorActive] forState:UIControlStateSelected];
	[button setBackgroundImage:[self imageWithColor:_grey60Color] forState:UIControlStateDisabled];
}

-(void) stylizeCallToActionGrayButton:(UIButton*)button
{
  UIFont *font = [UIFont dinMediumOfSize:14.0];
  button.titleLabel.font = font;
  button.titleEdgeInsets = UIEdgeInsetsMake(2, 0, 2, 0);

  [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
  [button setTitleColor:self.textSecondaryColor forState:UIControlStateDisabled];

  [button setBackgroundImage:[self imageWithColor:_grey90Color] forState:UIControlStateNormal];
  [button setBackgroundImage:[self imageWithColor:_grey50Color] forState:UIControlStateSelected];
  [button setBackgroundImage:[self imageWithColor:_grey50Color] forState:UIControlStateHighlighted];
}

- (void)stylizeDeclineActionButton:(UIButton*)button
{
  UIFont *font = [UIFont dinMediumOfSize:14.0];
  button.titleLabel.font = font;
  button.titleEdgeInsets = UIEdgeInsetsMake(2, 0, 2, 0);

  [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
  [button setTitleColor:self.textSecondaryColor forState:UIControlStateDisabled];

  [button setBackgroundImage:[self imageWithColor:_declineActionColorNormal] forState:UIControlStateNormal];
  [button setBackgroundImage:[self imageWithColor:_declineActionColorPress] forState:UIControlStateSelected];
  [button setBackgroundImage:[self imageWithColor:_grey60Color] forState:UIControlStateDisabled];

  button.layer.cornerRadius = 4.0;
  button.clipsToBounds = YES;
}

- (void)stylizeCancelActionButton:(UIButton*)button
{
  UIFont *font = [UIFont dinMediumOfSize:14.0];
  button.titleLabel.font = font;
  button.titleEdgeInsets = UIEdgeInsetsMake(2, 0, 2, 0);

  [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
  [button setTitleColor:self.textSecondaryColor forState:UIControlStateDisabled];

  [button setBackgroundImage:[self imageWithColor:_grey50Color] forState:UIControlStateNormal];
  [button setBackgroundImage:[self imageWithColor:_grey30Color] forState:UIControlStateSelected];
  [button setBackgroundImage:[self imageWithColor:_grey10Color] forState:UIControlStateDisabled];

  button.layer.cornerRadius = 4.0;
  button.clipsToBounds = YES;
}

-(void) stylizeToolbarButton:(UIButton*)button
{
  UIFont *font = [self toolbarButtonFont];
  button.titleLabel.font = font;
  button.titleEdgeInsets = UIEdgeInsetsMake(2, 0, 2, 0);
  button.backgroundColor = [UIColor clearColor];

  [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
  [button setTitleColor:self.grey70Color forState:UIControlStateDisabled];
  [button setTitleColor:self.grey170Color forState:UIControlStateHighlighted];
  [button setTintColor:[UIColor whiteColor]];
}

-(void) stylizeWithTopAndBottomBorders:(UIView *)aView;
{
  CALayer *topBorder = [CALayer layer];
  topBorder.borderColor = self.grey70Color.CGColor;
  topBorder.borderWidth = 1;
  topBorder.frame = CGRectMake(0, 0, aView.frame.size.width, 1);

  CALayer* bottomBorder = [CALayer layer];
  bottomBorder.borderColor = self.grey70Color.CGColor;
  bottomBorder.borderWidth = 1;
  bottomBorder.frame = CGRectMake(0, aView.frame.size.height-1, aView.frame.size.width, 1);

  [aView.layer addSublayer:topBorder];
  [aView.layer addSublayer:bottomBorder];
}


- (void)stylizeCancelButtonPopover:(UIButton *)button
{
  [button setTitleColor:self.horizonBlueColorActive forState:UIControlStateNormal];
  button.titleLabel.font = [UIFont dinBoldOfSize:[self buttonSmallFontSize]];
}


-(void) stylizeButtonSectionView:(UIButton *)button
{
  // 1. Title colors
  [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
  [button setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
  [button setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];

  // 2. Highlighted and selected background
  UIView *selectedView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, button.frame.size.width, 1)];
  selectedView.backgroundColor = _selectedBlueColor;

  UIGraphicsBeginImageContextWithOptions(selectedView.bounds.size, selectedView.opaque, 0.0);
  [selectedView.layer renderInContext:UIGraphicsGetCurrentContext()];
  UIImage *imgSelection = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();

  [button setBackgroundImage:imgSelection forState:UIControlStateSelected];
  [button setBackgroundImage:imgSelection forState:UIControlStateHighlighted];
  [button setBackgroundImage:imgSelection forState:UIControlStateSelected|UIControlStateHighlighted];

  // Standard background
  UIView *standardView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
  selectedView.backgroundColor = _grey40Color;

  UIGraphicsBeginImageContextWithOptions(standardView.bounds.size, standardView.opaque, 0.0);
  [standardView.layer renderInContext:UIGraphicsGetCurrentContext()];
  UIImage *imgStandard = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();

  [button setBackgroundImage:imgStandard forState:UIControlStateNormal];

  [self stylizeWithTopAndBottomBorders:button];
}


#pragma mark - Generated Images

// Utility function to generate background image for a button given a colour
- (UIImage*) imageWithColor:(UIColor*)color
{
	CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
	UIGraphicsBeginImageContext(rect.size);
	CGContextRef context = UIGraphicsGetCurrentContext();
	CGContextSetFillColorWithColor(context, [color CGColor]);
	CGContextFillRect(context, rect);
	
	UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	return image;
}


-(UIImage*) navigationBarBackgroundImage
{
  if (_navigationBarBackgroundImage == nil)
  {
    // Getting same color than outside the modal view controller means mixing the background
    // with a gray on top.
    
    UIView *colorView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
    UIView *colorView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
    
    [colorView setBackgroundColor: self.grey60Color];
    [colorView2 setBackgroundColor: self.grey50Color];
    
    [colorView addSubview:colorView2];
    
    UIGraphicsBeginImageContext(colorView.bounds.size);
    [colorView.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    _navigationBarBackgroundImage = viewImage;
  }
  return _navigationBarBackgroundImage;
}


-(UIImage*) imageWithTTStyle:(TTStyle*)style size:(CGSize)size
{
  TTView *view = [[TTView alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
  view.backgroundColor = [UIColor clearColor];
  view.style = style;
  
  UIGraphicsBeginImageContextWithOptions(view.bounds.size, NO, 2.0);
  [view.layer renderInContext:UIGraphicsGetCurrentContext()];
  UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  
  return image;
}


// Note on resizableImageWithCapInsets: here. Previously the insets used below were (5, 5, 5, 5)
// But this caused an inexplicable crash that was only manifesting itself in release builds.
// This turns out to be a combination of the optimization flag being set to Fastest, Smalest
// and an obscure bug whose crash log looks like something this:
// 0   libobjc.A.dylib               	0x320505b0 objc_msgSend + 16
// 1   Foundation                    	0x36f36ef4 probeGC + 60
// 2   Foundation                    	0x36f3cefa -[NSConcreteMapTable removeObjectForKey:] + 30
// 3   UIKit                         	0x363502e0 -[_UIImageViewPretiledImageWrapper dealloc] + 76
//
// The culprit appears to be a problem with [UIImage resizableImageWithCapInsets:] having issues
// with certain inset values. Bloody ridiculous. Here are some references.
// http://openradar.appspot.com/11411000
// http://twitter.com/kode80/statuses/236487393643986944
//
// Changing the insets to (6, 6, 6, 6) seem to have fixed the problem.
//
// Related to bug 6782

-(UIImage*) buttonBackgroundImage
{
  if (_buttonBackgroundImage == nil)
  {
    TTStyle *style = [self darkRoundedButton:UIControlStateNormal];
    UIImage *image = [self imageWithTTStyle:style size:CGSizeMake(14, 14)];
    image = [image resizableImageWithCapInsets:UIEdgeInsetsMake(6, 6, 6, 6)];
    _buttonBackgroundImage = image;
  }
  return _buttonBackgroundImage;
}


-(UIImage*) buttonHighlightedBackgroundImage
{
  if (_buttonHighlightedBackgroundImage == nil)
  {
    TTStyle *style = [self highlightedRoundedButton:UIControlStateNormal];
    UIImage *image = [self imageWithTTStyle:style size:CGSizeMake(14, 14)];
    image = [image resizableImageWithCapInsets:UIEdgeInsetsMake(6, 6, 6, 6)];
    _buttonHighlightedBackgroundImage = image;
  }
  return _buttonHighlightedBackgroundImage;
}


-(UIImage*) buttonSelectedBackgroundImage
{
  if (_buttonSelectedBackgroundImage == nil)
  {
    TTStyle *style = [self selectedRoundedButton:UIControlStateNormal];
    UIImage *image = [self imageWithTTStyle:style size:CGSizeMake(14, 14)];
    image = [image resizableImageWithCapInsets:UIEdgeInsetsMake(6, 6, 6, 6)];
    _buttonSelectedBackgroundImage = image;
  }
  return _buttonSelectedBackgroundImage;
}


-(UIImage*) buttonDisabledBackgroundImage
{
  if (_buttonDisabledBackgroundImage == nil)
  {
    TTStyle *style = [self disabledRoundedButton:UIControlStateNormal];
    UIImage *image = [self imageWithTTStyle:style size:CGSizeMake(14, 14)];
    image = [image resizableImageWithCapInsets:UIEdgeInsetsMake(6, 6, 6, 6)];
    _buttonDisabledBackgroundImage = image;
  }
  return _buttonDisabledBackgroundImage;
}


-(UIImage*) backButtonBackgroundImage
{
  if (_backButtonBackgroundImage == nil)
  {
    CGFloat width = 32.0;
    CGFloat height = 32.0;
    TTStyle *style = [self matteButton:UIControlStateNormal
                                 shape:[TTRoundedLeftArrowShape shapeWithRadius:5.0]
                             tintColor:self.feldBackingColor
                           borderColor:self.buttonBorderColor
                             textColor:RGBCOLOR(255, 255, 255)];
    UIImage *image = [self imageWithTTStyle:style size:CGSizeMake(width, height)];
    image = [image resizableImageWithCapInsets:UIEdgeInsetsMake(height/2 - 1, width/2 - 1, height/2 - 1, width/2 - 1)];
    _backButtonBackgroundImage = image;
  }
  return _backButtonBackgroundImage;
}


-(UIImage*) backButtonHighlightedBackgroundImage
{
  if (_backButtonHighlightedBackgroundImage == nil)
  {
    CGFloat width = 32.0;
    CGFloat height = 32.0;
    TTStyle *style = [self matteButton:UIControlStateNormal
                                 shape:[TTRoundedLeftArrowShape shapeWithRadius:5.0]
                             tintColor:self.buttonHighlightBackgroundColor
                           borderColor:self.buttonBorderColor
                             textColor:RGBCOLOR(255, 255, 255)];
    UIImage *image = [self imageWithTTStyle:style size:CGSizeMake(width, height)];
    image = [image resizableImageWithCapInsets:UIEdgeInsetsMake(height/2 - 1, width/2 - 1, height/2 - 1, width/2 - 1)];
    _backButtonHighlightedBackgroundImage = image;
  }
  return _backButtonHighlightedBackgroundImage;
}


-(UIImage*) segmentedControlSeparatorImage
{
  if (_segmentedControlSeparatorImage == nil)
  {
    TTStyle *style = [TTSolidFillStyle styleWithColor:self.buttonBorderColor next:nil];
    UIImage *image = [self imageWithTTStyle:style size:CGSizeMake(1, 14)];
    image = [image resizableImageWithCapInsets:UIEdgeInsetsMake(6, 0, 6, 0)];
    _segmentedControlSeparatorImage = image;
  }
  return _segmentedControlSeparatorImage;
}

-(TTStyle*) blueHightlightedToolbarButton:(UIControlState)state
{
  UIColor *(^adjustColor)(UIColor *color, UIControlState state) = ^(UIColor *color, UIControlState state) {
    if (state == UIControlStateHighlighted)
      return [color multiplyHue:1.0 saturation:1.0 value:1.3];
    else if (state == UIControlStateDisabled)
      return [color multiplyHue:1.0 saturation:0.42 value:0.42];
    return color;
  };
  
	UIColor *adjustedTintColor = adjustColor([self feldBackingColor], state);
  
  
  UIFont *font = [UIFont dinMediumOfSize:16.0];
	
	return [TTShapeStyle styleWithShape:[TTRoundedRectangleShape shapeWithRadius:5.0] next:
          [TTSolidBorderStyle styleWithColor:self.feldBorderInCollaborationColor width:1.0 next:
           [TTSolidFillStyle styleWithColor:adjustedTintColor next:
            [TTBoxStyle styleWithPadding:UIEdgeInsetsMake(3, 6, 3, 6) next:
             [TTTextStyle styleWithFont:font color:self.feldBorderInCollaborationColor next:
              nil]]]]];
}


#pragma mark - Feld Switcher Styles

-(TTStyle*) feldSwitcherButton:(UIControlState)state
{
  UIColor *(^adjustColor)(UIColor *normalColor, UIColor *highlightedColor, UIControlState state) = ^(UIColor *normalColor, UIColor *highlightedColor, UIControlState state) {
    if (state == UIControlStateHighlighted)
      return [highlightedColor colorWithAlphaComponent:0.8];
    else if (state == UIControlStateSelected)
      return highlightedColor;
    else if (state == UIControlStateDisabled)
      return [normalColor multiplyHue:1.0 saturation:0.42 value:0.42];
    return normalColor;
  };

  UIColor *adjustedFeldColor = adjustColor(self.darkFillColor, self.horizonBlueColor, state);
	UIColor *adjustedBorderColor = adjustColor(self.grey130Color, self.horizonBlueColorActive, state);

  CGFloat borderWidth = 1.0 / [[UIScreen mainScreen] scale];

	return [TTShapeStyle styleWithShape:[TTRectangleShape shape] next:
          [TTSolidBorderStyle styleWithColor:adjustedBorderColor width:borderWidth next:
           [TTSolidFillStyle styleWithColor:adjustedFeldColor next:
            [TTBoxStyle styleWithPadding:UIEdgeInsetsMake(3, 6, 3, 6) next:
             nil]]]];
}

#pragma mark - Version Helpers

-(void) setVersion:(NSString *)version
{
  _version = version;

  // We need to reset the image, since the mezzanine version can be different.
  _navigationBarBackgroundImage = nil;
  [self initializeAppearance];
}

@end
