//
//  NSString+Additions.m
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 20/09/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

#import "NSString+Additions.h"

@implementation NSString (Additions)

- (NSString *)sanitizeStringAsServerName
{
  NSString *name = self;
  name = [name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
  NSURL *canditateURL = [NSURL URLWithString:name];
  if (canditateURL && [canditateURL scheme] && [canditateURL host])
  {
    NSURLComponents *components = [NSURLComponents componentsWithURL:canditateURL resolvingAgainstBaseURL:YES];
    NSString *host = components.host ? components.host : @"";
    NSString *path = components.path ? components.path : @"";
    NSString *query = components.query ? components.query : @"";
    NSString *fragment = components.fragment ? [NSString stringWithFormat:@"#%@", components.fragment] : @"";
    
    name = [NSString stringWithFormat:@"%@%@%@%@", host, path, query, fragment];
  }
  
  return name;
}

@end
