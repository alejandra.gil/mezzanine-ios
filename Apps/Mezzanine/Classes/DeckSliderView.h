//
//  DeckSliderView.h
//  Mezzanine
//
//  Created by Miguel Sánchez Valdés on 4/5/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTView.h"

enum 
{
  DeckSliderNormalStyle,
  DeckSliderOnlyThumbStyle,
  DeckSliderDarkStyle,
  DeckSliderSmallStyle
};
typedef NSInteger DeckSliderStyle;


@protocol DeckSliderViewDelegate;

@interface DeckSliderView : UIView <UIGestureRecognizerDelegate>
{
  TTView *trackView;
  TTView *thumbView;
  UILabel *slideIndexLabel;
  BOOL isTracking;
  
  UILabel *firstSlideLabel;
  UILabel *lastSlideLabel;
  UILabel *noSlidesLabel;
  
  __weak id delegate;
}

@property (nonatomic, assign) CGFloat trackHeight;
@property (nonatomic, assign) CGFloat minThumbWidth;
@property (nonatomic, assign) BOOL pushbacked;
@property (nonatomic, assign) BOOL isTracking;
@property (nonatomic, assign) BOOL showDeckStateLabels;
@property (nonatomic, weak) id <DeckSliderViewDelegate> delegate;

- (void) pushbackFinished:(BOOL)isPushbacked;
- (void) setSlideIndex:(NSInteger)slideIndex numberOfSlides:(NSInteger)numberOfSlides;
- (void) updateThumbWidthforSlides:(NSInteger)numberOfSlides andCurrentSlide:(NSInteger)currentSlideIndex;
- (void) setStyle:(DeckSliderStyle)style;

@end


@protocol DeckSliderViewDelegate <NSObject>

@optional
- (void) viewDidLayout:(DeckSliderView*)view;
- (void) didStartDraggingThumbslider;
- (void) didDragThumbslider:(CGFloat)position;
- (void) didFinishDraggingThumbsliderAtPosition:(CGFloat)position;

@end
