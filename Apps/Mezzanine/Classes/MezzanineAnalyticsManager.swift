//
//  MezzanineAnalyticsManager.swift
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 21/01/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

import Foundation

@objc class MezzanineAnalyticsManager: NSObject {
  
  @objc static let sharedInstance = MezzanineAnalyticsManager()
  
  // Constant to show dialog 
  fileprivate let kShowDialogAfterOpened = 2
  
  // Constant identifiers
  fileprivate let kAnalyticsSendUsage = "analytics.sendUsageToGAandLocalytics"
  fileprivate let kAnalyticsOptInShown = "analytics.optInShownAfterGAandLocalitics"
  fileprivate let kAnalyticsAmountTimesAppStarted = "analytics.counterAppStarted"
  
  // Localytics keys
  fileprivate let kLOCALYTICS_DEBUG = "74fd2bf52c7743020353b25-9285508a-b55e-11e5-8c9b-00736b041834"
  fileprivate let kLOCALYTICS_RELEASE = "9c4c1badde45db48f8f6647-3d784a80-c035-11e5-6094-002dea3c3994"

  // Google Analytics IDs
  fileprivate let kGAI_DEBUG = "UA-93806121-2"
  fileprivate let kGAI_RELEASE = "UA-93806121-1"

  // Google Analytics vars
  fileprivate var sessionStarted: NSDate?

  // Prevents to send duplicated tagScreen: events in case the view lifecycle calls it multiple times
  fileprivate var currentScreen = String()
  
  // Expose constants to ObjC
  
  // Events
  @objc let connectionEvent = Constants.Events.connection
  @objc let connectionErrorEvent = Constants.Events.connectionError
  @objc let disconnectionEvent = Constants.Events.disconnection
  @objc let mezzanineDetailsEvent = Constants.Events.mezzanineDetails
  @objc let startPresentationEvent = Constants.Events.startPresentation
  @objc let stopPresentationEvent = Constants.Events.stopPresentation
  @objc let shareDataUsageEvent = Constants.Events.shareDataUsage
  @objc let signInWorkspacesEvent = Constants.Events.signInWorkspaces
  @objc let addPortfolioItemEvent = Constants.Events.addPortfolioItem
  @objc let uploadAnnotationsEvent = Constants.Events.uploadAnnotations
  @objc let downloadPresentionEvent = Constants.Events.downloadPresentation
  @objc let startWorkspaceZoomEvent = Constants.Events.startWorkspaceZoom
  @objc let scrollPresentationEvent = Constants.Events.scrollPresentation
  @objc let createWindshieldItemEvent = Constants.Events.createWindshieldItem
  @objc let transformWindshieldItemEvent = Constants.Events.transformWindshieldItem
  @objc let deleteWindshieldItemEvent = Constants.Events.deleteWindshieldItem
  @objc let mezzInToggleEvent = Constants.Events.mezzInToggle
  @objc let mezzInShareLinkEvent = Constants.Events.mezzInShareLink
  @objc let vtcMuteEvent = Constants.Events.vtcMute
  @objc let vtcFullscreenEvent = Constants.Events.vtcFullscreen
  @objc let audioOnlyEvent = Constants.Events.audioOnly
  @objc let handoffInitEvent = Constants.Events.handoffInit
  @objc let handoffEndedEvent = Constants.Events.handoffEnded
  @objc let leaveEvent = Constants.Events.leave
  @objc let photoAlbumVisitedEvent = Constants.Events.photoAlbumVisited

  // Keys
  @objc let sourceKey = Constants.Attributes.Keys.source
  @objc let contextKey = Constants.Attributes.Keys.context
  @objc let destinationKey = Constants.Attributes.Keys.destination
  @objc let mezzVersionKey = Constants.Attributes.Keys.mezzanineVersion
  @objc let responseKey = Constants.Attributes.Keys.response
  @objc let presentationActiveKey = Constants.Attributes.Keys.presentationActive
  @objc let connectionTypeKey = Constants.Attributes.Keys.connectionType
  @objc let typeKey = Constants.Attributes.Keys.type
  @objc let passkeyKey = Constants.Attributes.Keys.passkey
  @objc let actionKey = Constants.Attributes.Keys.action
  @objc let mediaKey = Constants.Attributes.Keys.media
  @objc let joinedKey = Constants.Attributes.Keys.joined
  @objc let stateKey = Constants.Attributes.Keys.state
  @objc let errorKey = Constants.Attributes.Keys.error
  @objc let apiVersionKey = Constants.Attributes.Keys.apiVersion
  @objc let contentTypeKey = Constants.Attributes.Keys.contentType
  @objc let mezzInEnabledKey = Constants.Attributes.Keys.mezzInEnabled
  @objc let remoteMezzCountKey = Constants.Attributes.Keys.remoteMezzCount
  @objc let workspaceCountKey = Constants.Attributes.Keys.workspaceCount
  @objc let nameKey = Constants.Attributes.Keys.name

  // Attributes
  @objc let URLAttribute = Constants.Attributes.Values.URL
  @objc let touchAttribute = Constants.Attributes.Values.touch
  @objc let connectionViewAttribute = Constants.Attributes.Values.connectionView
  @objc let connectionHistoryAttribute = Constants.Attributes.Values.connectionHistory
  @objc let yesAttribute = Constants.Attributes.Values.yes
  @objc let noAttribute = Constants.Attributes.Values.no
  @objc let onAttribute = Constants.Attributes.Values.on
  @objc let offAttribute = Constants.Attributes.Values.off
  @objc let textAttribute = Constants.Attributes.Values.text
  @objc let cameraAttribute = Constants.Attributes.Values.camera
  @objc let libraryAttribute = Constants.Attributes.Values.library
  @objc let whiteboardAttribute = Constants.Attributes.Values.whiteboard
  @objc let workspaceAttribute = Constants.Attributes.Values.workspace
  @objc let windshieldAttribute = Constants.Attributes.Values.windshield
  @objc let extendedWindshieldAttribute = Constants.Attributes.Values.extendedWindshield
  @objc let presentButtonAttribute = Constants.Attributes.Values.presentButton
  @objc let portfolioItemAttribute = Constants.Attributes.Values.portfolioItem
  @objc let windshieldItemAttribute = Constants.Attributes.Values.windshieldItem
  @objc let sliderButtonAttribute = Constants.Attributes.Values.sliderButton
  @objc let localAttribute = Constants.Attributes.Values.local
  @objc let cloudAttribute = Constants.Attributes.Values.cloud
  @objc let swipeAttribute = Constants.Attributes.Values.swipe
  @objc let jumpToSlideAttribute = Constants.Attributes.Values.jumpSlide
  @objc let sliderAttribute = Constants.Attributes.Values.slider
  @objc let startAttribute = Constants.Attributes.Values.start
  @objc let stopAttribute = Constants.Attributes.Values.stop
  @objc let copyAttribute = Constants.Attributes.Values.copy
  @objc let emailAttribute = Constants.Attributes.Values.email
  @objc let audioAttribute = Constants.Attributes.Values.audio
  @objc let videoAttribute = Constants.Attributes.Values.video
  @objc let bothAttribute = Constants.Attributes.Values.both
  @objc let neitherAttribute = Constants.Attributes.Values.neither
  @objc let fullscreenAttribute = Constants.Attributes.Values.fullscreen
  @objc let bubbleAttribute = Constants.Attributes.Values.bubble
  @objc let hostNotFoundAttribute = Constants.Attributes.Values.hostNotFound
  @objc let timeoutAttribute = Constants.Attributes.Values.timeout
  @objc let incorrectPassphraseAttribute = Constants.Attributes.Values.incorrectPassphrase
  @objc let certificateErrorAttribute = Constants.Attributes.Values.certError
 
  @objc let mezzanineMenuAttribute = Constants.Attributes.Values.mezzanineMenu
  @objc let backgroundTimeoutAttribute = Constants.Attributes.Values.backgroundTimeout
  @objc let heartbeatTimeoutAttribute = Constants.Attributes.Values.heartbeatTimeout
  @objc let websocketClosedAttribute = Constants.Attributes.Values.websocketClosed
  @objc let clientEvictedAttribute = Constants.Attributes.Values.clientEvicted
  @objc let plasmaSessionEndedAttribute = Constants.Attributes.Values.plasmaSessionEnded
  @objc let ziggySessionEndedAttribute = Constants.Attributes.Values.ziggySessionEnded
  @objc let serverRestartAttribute = Constants.Attributes.Values.serverRestart
  @objc let audioOnlyLeaveAttribute = Constants.Attributes.Values.audioOnlyLeave
  @objc let newMezzanineURLAttribute = Constants.Attributes.Values.newMezzanineURL
  @objc let cancelAttribute = Constants.Attributes.Values.cancel
  @objc let disconnectAttribute = Constants.Attributes.Values.disconnect
  @objc let addButtonAttribute = Constants.Attributes.Values.addButton
  @objc let itemPlaceholderAttribute = Constants.Attributes.Values.itemPlaceholder
  @objc let leaveJustMeAttribute = Constants.Attributes.Values.leaveJustMe
  @objc let leaveEveryoneAttribute = Constants.Attributes.Values.leaveEveryone

  
  //Screens
  @objc let connectionViewScreen = Constants.Screens.connection
  @objc let workspaceViewScreen = Constants.Screens.workspace
  @objc let workspaceZoomScreen = Constants.Screens.workspaceZoom
  @objc let extendedWindshieldScreen = Constants.Screens.extendedWindshield
  @objc let workspaceListViewScreen = Constants.Screens.workspaceList
  @objc let localPreviewScreen = Constants.Screens.localPreview
  @objc let infopresenceViewScreen = Constants.Screens.infopresence
  @objc let infopresenceJoinScreen = Constants.Screens.infopresenceJoin
  @objc let infopresenceInviteMezzaninesScreen = Constants.Screens.infopresenceInviteMezzanines
  @objc let infopresenceInviteParticipantsScreen = Constants.Screens.infopresenceInviteParticipants
  @objc let passkeyViewScreen = Constants.Screens.passkey
  @objc let mezzInJoinViewScreen = Constants.Screens.mezzInJoin
  @objc let vtcFullScreen = Constants.Screens.vtcFullscreen
  @objc let audioOnlyViewScreen = Constants.Screens.audioOnly

  // Attribute Enums

  @objc public enum WindshieldItemContentType: Int, RawRepresentable {
    case image
    case video
    case web
    case unknown

    public typealias RawValue = String

    public var rawValue: RawValue {
      switch self {
      case .image:
        return Constants.Attributes.Values.image
      case .video:
        return Constants.Attributes.Values.video
      case .web:
        return Constants.Attributes.Values.web
      case .unknown:
        return Constants.Attributes.Values.unknown
      }
    }

    public init?(rawValue: RawValue) {
      switch rawValue {
      case Constants.Attributes.Values.image:
        self = .image
      case Constants.Attributes.Values.video:
        self = .video
      case Constants.Attributes.Values.web:
        self = .web
      case Constants.Attributes.Values.unknown:
        self = .unknown
      default:
        self = .unknown
      }
    }
  }

  @objc public enum LeaveSessionType: Int, RawRepresentable {
    case directly
    case justMe
    case everyone
    case unknown

    public typealias RawValue = String

    public var rawValue: RawValue {
      switch self {
      case .directly:
        return Constants.Attributes.Values.leaveDirectly
      case .justMe:
        return Constants.Attributes.Values.leaveJustMe
      case .everyone:
        return Constants.Attributes.Values.leaveEveryone
      case .unknown:
        return Constants.Attributes.Values.unknown
      }
    }

    public init?(rawValue: RawValue) {
      switch rawValue {
      case Constants.Attributes.Values.leaveJustMe:
        self = .justMe
      case Constants.Attributes.Values.leaveEveryone:
        self = .everyone
      case Constants.Attributes.Values.leaveDirectly:
        self = .directly
      default:
        self = .unknown
      }
    }
  }

  @objc public enum PhotoAlbumType: Int, RawRepresentable {
    case smart
    case userCreated
    case unknown

    public typealias RawValue = String

    public var rawValue: RawValue {
      switch self {
      case .smart:
        return Constants.Attributes.Values.smartAlbum
      case .userCreated:
        return Constants.Attributes.Values.userCreatedAlbum
      case .unknown:
        return Constants.Attributes.Values.unknown
      }
    }

    public init?(rawValue: RawValue) {
      switch rawValue {
      case Constants.Attributes.Values.smartAlbum:
        self = .smart
      case Constants.Attributes.Values.userCreatedAlbum:
        self = .userCreated
      default:
        self = .unknown
      }
    }
  }


  // MARK: Analytics Management On User Defaults

  // Property to keep track if opt in dialog can be shown
  @objc var amountTimesAppStarted: Int {
    get {
      return UserDefaults.standard.integer(forKey: kAnalyticsAmountTimesAppStarted)
    }
    
    set(newValue) {
      UserDefaults.standard.set(newValue, forKey: kAnalyticsAmountTimesAppStarted)
    }
  }
  
  // Property to know if opt in dialog was already shown
  @objc var optInDialogShown: Bool {
    get {
      return UserDefaults.standard.bool(forKey: kAnalyticsOptInShown)
    }
    
    set(newValue) {
      UserDefaults.standard.set(newValue, forKey: kAnalyticsOptInShown)
    }
  }
  
  // Property to know if user allowed share data usage
  @objc var userAllowsShareDataUsage: Bool {
    get {
      // By default, the switch is OFF.
      // The first time the value will be nil so we need to register the default value.
      // We know that nil is equivalent to false but let's do it the right and safe way
      if (UserDefaults.standard.value(forKey: kAnalyticsSendUsage) == nil) {
        UserDefaults.standard.register(defaults: [kAnalyticsSendUsage : false])
        UserDefaults.standard.synchronize()
      }
      
      return UserDefaults.standard.bool(forKey: kAnalyticsSendUsage)
    }
    
    set(newValue) {
      UserDefaults.standard.set(newValue, forKey: kAnalyticsSendUsage)
    }
  }


  // MARK: UI Helpers

  @objc func canShowAnalyticsOptInDialog() -> Bool {
    return (optInDialogShown == false && amountTimesAppStarted >= kShowDialogAfterOpened)
  }


  // MARK: Events Tagging

  @objc func trackShareDataUsageEvent(accepted: Bool, isRemote:Bool) {
    // We will always send the response of sharing data usage
    userAllowsShareDataUsage = true

    // To Localytics
    tagEvent(shareDataUsageEvent, attributes: [responseKey : accepted ? yesAttribute : noAttribute])

    // To Google Analytics: Remotes connections do not require all the information
    // for room connections enqueue the event if we have enough information about the system/company/room
    let action = accepted ? GAConstants.Action.analyticsEnabled : GAConstants.Action.analyticsDisabled
    if isRemote || canDispatchEnqueuedEvents() {
      dispatchEvent(category: GAConstants.Category.preferences, action: action, label: nil, value: nil)
    }
    else {
      enqueueEvent(category: GAConstants.Category.preferences, action: action, label: nil, value: nil)
    }

    // And store the real option for future events
    userAllowsShareDataUsage = accepted
  }

  @objc func trackConnectionEvent(source: String, mezzVersion: String, isRemote: Bool, passkeyState: String, apiVersion: String) {

    // To Localytics
    let connectionType = isRemote ? cloudAttribute : localAttribute
    tagEvent(connectionEvent, attributes: [sourceKey: source, mezzVersionKey: mezzVersion, typeKey: connectionType, passkeyKey: passkeyState, apiVersionKey: apiVersion])

    // To Google Analytics: Remotes connections do not require all the information
    // for room connections enqueue the event if we have enough information about the system/company/room
    let label = isRemote ? GAConstants.Label.remote : GAConstants.Label.room
    if isRemote || canDispatchEnqueuedEvents() {
      dispatchEvent(category: GAConstants.Category.session, action: GAConstants.Action.sessionConnect, label: label, value: nil)
    }
    else {
      enqueueEvent(category: GAConstants.Category.session, action: GAConstants.Action.sessionConnect, label: label, value: nil)
    }

    sessionStarted = NSDate()
  }

  @objc func trackDisconnectionEvent(disconnectionReason: String, isRemote: Bool) {

    // Don't send any disconnection event if a session hasn't been started.
    guard let sessionStarted = sessionStarted else { return }

    // Some strings used in Mezzkit needs conversion to the real string
    var message = disconnectionReason
    switch disconnectionReason {
    case MZCommunicatorDidDisconnectServerRestart:
      message = serverRestartAttribute
    case MZCommunicatorDidDisconnectHeartbeatTimeout:
      message = heartbeatTimeoutAttribute
    case MZCommunicatorDidDisconnectClientEvicted:
      message = clientEvictedAttribute
    case MZCommunicatorDidDisconnectPlasmaSessionEnded:
      message = plasmaSessionEndedAttribute
    case MZCommunicatorDidDisconnectZiggySessionEnded:
      message = ziggySessionEndedAttribute
    default:
      message = disconnectionReason
    }

    // To Localytics
    let connectionType = isRemote ? cloudAttribute : localAttribute
    tagEvent(disconnectionEvent, attributes: [sourceKey: message, connectionTypeKey: connectionType])

    // To Google Analytics: Remotes connections do not require all the information
    // for room connections enqueue the event if we have enough information about the system/company/room

    let sessionEnd = NSDate()
    let interval = sessionEnd.timeIntervalSince(sessionStarted as Date)
    let duration = String(Int(interval))

    let label = disconnectionReason == mezzanineMenuAttribute ? GAConstants.Label.close : GAConstants.Label.kill
    dispatchEvent(category: GAConstants.Category.session, action: GAConstants.Action.sessionDisconnect, label: label, value: nil, duration: duration)
  }

  @objc func trackMezzanineDetailsEvent(mezzVersion: String, isRemote: Bool, apiVersion: String, mezzInEnabled: Bool, remoteMezzanineCount: NSNumber, workspaceCount: NSNumber) {

    // To Localytics
    let connectionType = isRemote ? cloudAttribute : localAttribute
    let mezzInEnabledAttr = mezzInEnabled ? yesAttribute : noAttribute

    tagEvent(mezzanineDetailsEvent, attributes: [mezzVersionKey: mezzVersion,
                                                 typeKey: connectionType,
                                                 apiVersionKey: apiVersion,
                                                 mezzInEnabledKey: mezzInEnabledAttr,
                                                 remoteMezzCountKey: remoteMezzanineCount.stringValue,
                                                 workspaceCountKey: workspaceCount.stringValue])
  }

  @objc func trackWindshieldItemCreateEvent(_ contentType: WindshieldItemContentType) {
    tagEvent(createWindshieldItemEvent, attributes: [sourceKey: windshieldItemAttribute, contentTypeKey: contentType.rawValue])
  }
  @objc func trackWindshieldItemTransformEvent(_ contentType: WindshieldItemContentType) {
    tagEvent(transformWindshieldItemEvent, attributes: [sourceKey: windshieldItemAttribute, contentTypeKey: contentType.rawValue])
  }
  @objc func trackWindshieldItemDeleteEvent(_ contentType: WindshieldItemContentType) {
    tagEvent(deleteWindshieldItemEvent, attributes: [sourceKey: windshieldItemAttribute, contentTypeKey: contentType.rawValue])
  }

  @objc func trackLeaveSessionEvent(_ leaveType: LeaveSessionType) {
    tagEvent(leaveEvent, attributes: [typeKey: leaveType.rawValue])
  }

  @objc func trackPhotoAlbumNavigationEvent(_ albumType: PhotoAlbumType, albumName: String) {
    tagEvent(photoAlbumVisitedEvent, attributes: [typeKey: albumType.rawValue, nameKey: albumName])
  }


  // MARK: Custom Dimensions

  @objc func setMezzanineModel(mezzanineModel: String!) {
    // To Google Analytics:
    let tracker = GAI.sharedInstance().tracker(withName: "iOS", trackingId: googleAnalyticsID)
    tracker?.set(GAIFields.customDimension(for: 3), value: mezzanineModel)

    // To Localytics
  // Localytics.setValue(mezzanineModel, forCustomDimension: 0)
  }

  @objc func setCustomerID(customerID: String!) {
    let tracker = GAI.sharedInstance().tracker(withName: "iOS", trackingId: googleAnalyticsID)
    tracker?.set(GAIFields.customDimension(for: 4), value: customerID)

    // To Localytics
    //Localytics.setValue(customerID, forCustomDimension: 1)
  }

  @objc func setRoomID(roomID: String!) {
    // To Google Analytics:
    let tracker = GAI.sharedInstance().tracker(withName: "iOS", trackingId: googleAnalyticsID)
    tracker?.set(GAIFields.customDimension(for: 5), value: roomID)

    // To Localytics
    //Localytics.setValue(roomID, forCustomDimension: 2)
  }


  // MARK: Localytics
  
  @objc func autoIntegrate(_ launchOptions : Dictionary<String, AnyObject>) {
    var localyticsKey: String!
    
    #if LOCALYTICS_DEBUG
      localyticsKey = kLOCALYTICS_DEBUG
     // Localytics.setLoggingEnabled(true)
    #else
      localyticsKey = kLOCALYTICS_RELEASE
    #endif

  //  Localytics.autoIntegrate(localyticsKey, launchOptions: launchOptions)
  //  Localytics.setOptions(["collect_adid" : false as NSObject])

    #if LOCALYTICS_LOG
      NSLog("\n\n-BEGIN-Analytics- Localytics integration using key: %@ -END-\n", localyticsKey);
    #endif
  }


  @objc func tagEvent(_ eventName: String, attributes: [String: String]) {
    
    if !userAllowsShareDataUsage {
      return
    }

    var mutableAttributes = attributes
    guard let communicator = MezzanineAppContext.current().currentCommunicator else { return }
    
    // Those events don't have "connectionType" attribute
    if eventName != Constants.Events.connection &&
      eventName != Constants.Events.mezzInToggle &&
      eventName != Constants.Events.mezzInShareLink &&
      eventName != Constants.Events.vtcMute &&
      eventName != Constants.Events.vtcFullscreen &&
      eventName != Constants.Events.signInWorkspaces &&
      eventName != Constants.Events.audioOnly {
        mutableAttributes[Constants.Attributes.Keys.connectionType] = communicator.isARemoteParticipationConnection ? "Cloud" : "Local"
    }

    #if LOCALYTICS_LOG
      print("\n\n-BEGIN-Analytics- Tagging event: \"\(eventName)\" with attributes: \(String(describing: mutableAttributes)) -END-\n")
    #endif
    
  //  Localytics.tagEvent(eventName, attributes: mutableAttributes)
  }

  
  @objc func tagEvent(_ eventName: String) {

    #if LOCALYTICS_LOG
      print("\n\n-BEGIN-Analytics- Tagging event: \"\(eventName)\" -END-\n")
    #endif
    
    if (userAllowsShareDataUsage == true) {
   //   Localytics.tagEvent(eventName)
    }
  }

  
  @objc func tagScreen(_ screenName: String!) {
    guard let screenName = screenName else { return }

    if screenName != currentScreen {
      #if LOCALYTICS_LOG
        print("\n\n-BEGIN-Analytics- Tagging screen: \"\(screenName)\" -END-\n")
      #endif
      
     //  Localytics.tagScreen(screenName)
    }
    
    currentScreen = screenName
  }
  
  // TODO: Refactor this to simplify since we always keep the workspace status
  @objc func tagWorkspaceStatusScreen() {
    
    // FIXME: Removed when feature toggle is gone
    if SettingsFeatureToggles.sharedInstance.teamworkUI == true {
      return
    }
    
    let navController = MezzanineAppContext.current().mainNavController
    
    for controller in (navController?.viewControllers)! {
      if controller is MezzanineRootViewController {
        let workspaceViewController = (controller as! MezzanineRootViewController).mezzanineMainViewController.workspaceViewController
        let videoChatViewController = (controller as! MezzanineRootViewController).mezzanineMainViewController.videoChatViewController

        // In this case, we omit tagging the workspace because we go full screen
        if videoChatViewController != nil && videoChatViewController!.state == VideoChatState.fullscreen {
          tagScreen(Constants.Screens.vtcFullscreen)
          return
        }
        
        // Zoom in, extended windshield, default view
        if (workspaceViewController?.isZoomedIn())! {
          tagScreen(Constants.Screens.workspaceZoom)
        } else if workspaceViewController?.extendedWindshieldContainerView.isHidden == false {
          tagScreen(Constants.Screens.extendedWindshield)
        } else {
          tagScreen(Constants.Screens.workspace)
        }
      }
    }
  }


  // MARK: Google Analytics

  var googleAnalyticsID: String! {
    #if LOCALYTICS_DEBUG
      return kGAI_DEBUG
    #else
      return kGAI_RELEASE
    #endif
  }

  @objc func initializeGoogleAnalyticsTracking(appVersion: String) {

    // Google tip: If your app runs for long periods of time in the foreground,
    // you might consider turning on periodic dispatching.
    GAI.sharedInstance().dispatchInterval = 15

    GAI.sharedInstance().trackUncaughtExceptions = true
    let tracker = GAI.sharedInstance().tracker(withName: "iOS", trackingId: googleAnalyticsID)
    tracker?.set(kGAIAppName, value: "iOS")
    tracker?.set(kGAIAppVersion, value: appVersion)

    let uniqueIdentifier = UIDevice.current.identifierForVendor?.uuidString
    tracker?.set(kGAIUserId, value: uniqueIdentifier)
    tracker?.set(GAIFields.customDimension(for: 7), value: uniqueIdentifier)

    #if LOCALYTICS_LOG
      print("\n\n-BEGIN-Analytics- Google Analytics integration using key: %@ -END-\n", googleAnalyticsID)
    #endif
  }

  @objc func dispatchEvent (category: String!, action: String!, label: String?, value: NSNumber?) {
    dispatchEvent(category: category, action: action, label: label, value: value, duration: nil)
  }

  @objc func dispatchEvent (category: String!, action: String!, label: String?, value: NSNumber?, duration: String?) {

    if !userAllowsShareDataUsage {
      return
    }

    let tracker = GAI.sharedInstance().defaultTracker
    let event = GAIDictionaryBuilder.createEvent(withCategory: category, action: action, label: label, value: value)
    if let duration = duration {
      event?.set(duration, forKey: GAIFields.customMetric(for: 1))
    }
    let eventDictionary = event?.build()
    tracker?.send(eventDictionary! as [NSObject : AnyObject])
    GAI.sharedInstance().dispatch()
  }

  var eventQueue = Array<NSMutableDictionary> ()
  func enqueueEvent (category: String!, action: String!, label: String?, value: NSNumber?) {
    let event = GAIDictionaryBuilder.createEvent(withCategory: category, action: action, label: label, value: value).build()
    eventQueue.append(event!)
  }

  func canDispatchEnqueuedEvents () -> Bool
  {
    let tracker = GAI.sharedInstance().tracker(withName: "iOS", trackingId: googleAnalyticsID)
    let dim3 = tracker?.get(GAIFields.customDimension(for: 3)) as String?
    let dim4 = tracker?.get(GAIFields.customDimension(for: 4)) as String?
    let dim5 = tracker?.get(GAIFields.customDimension(for: 5)) as String?

    return dim3 != nil && dim4 != nil && dim5 != nil
  }

  @objc func dispatchEnqueuedEvents () {

    if canDispatchEnqueuedEvents () == false {
      return
    }

    for event in eventQueue {
      GAI.sharedInstance().defaultTracker.send(event as [NSObject : AnyObject])
    }
    eventQueue.removeAll()
    
    GAI.sharedInstance().dispatch()
  }
}

struct GAConstants {
  struct Category {
    static let session = "session"
    static let preferences = "preferences"
  }

  struct Action {
    static let analyticsEnabled = "analytics-enabled"
    static let analyticsDisabled = "analytics-disabled"
    static let sessionConnect = "session-connect"
    static let sessionDisconnect = "session-disconnect"
  }

  struct Label {
    static let close = "close"
    static let kill = "kill"
    static let room = "room"
    static let remote = "remote"
  }
}


struct Constants {
  struct Events {
    static let connection = "Connection"
    static let connectionError = "Connection error"
    static let disconnection = "Disconnection"
    static let startPresentation = "Start presentation"
    static let stopPresentation = "Stop presentation"
    static let shareDataUsage = "Share Data Usage"
    static let signInWorkspaces = "Sign In workspaces"
    static let addPortfolioItem = "Add portfolio item"
    static let uploadAnnotations = "Upload annotations"
    static let downloadPresentation = "Download presentation"
    static let startWorkspaceZoom = "Start workspace zoom"
    static let scrollPresentation = "Scroll presentation"
    static let createWindshieldItem = "Create windshield item"
    static let transformWindshieldItem = "Transform windshield item"
    static let deleteWindshieldItem = "Delete windshield item"
    static let mezzInToggle = "Mezz-In toggle"
    static let mezzInShareLink = "Mezz-In share link"
    static let mezzanineDetails = "Mezzanine details"
    static let vtcMute = "VTC mute"
    static let vtcFullscreen = "VTC fullscreen"
    static let audioOnly = "Audio-only mode"
    static let handoffInit = "Handoff Initialized"
    static let handoffEnded = "Handoff Ended"
    static let leave = "Leave"
    static let photoAlbumVisited = "Photo Album Visited"
  }
  
  struct Attributes {
    struct Keys {
      static let source = "source"
      static let context = "context"
      static let destination = "destination"
      static let mezzanineVersion = "mezzanine version"
      static let response = "response"
      static let presentationActive = "presentation is active"
      static let connectionType = "connection type"
      static let type = "type"
      static let passkey = "passkey"
      static let action = "action"
      static let media = "media"
      static let joined = "joined"
      static let state = "state"
      static let error = "error"
      static let apiVersion = "api version"
      static let contentType = "content type"
      static let mezzInEnabled = "Mezz-In enabled"
      static let remoteMezzCount = "count of remote mezzanines"
      static let workspaceCount = "count of workspaces"
      static let name = "name"
    }
    
    struct Values {
      static let yes = "Yes"
      static let no = "No"
      static let on = "On"
      static let off = "Off"
      static let URL = "URL"
      static let touch = "3D Touch"
      static let connectionView = "Connection view"
      static let connectionHistory = "Connection history"
      static let text = "Text"
      static let camera = "Camera"
      static let library = "Library"
      static let whiteboard = "Whiteboard"
      static let workspace = "Workspace"
      static let windshield = "Windshield"
      static let extendedWindshield = "Extended windshield"
      static let presentButton = "Present button"
      static let portfolioItem = "Portfolio item"
      static let windshieldItem = "Windshield item"
      static let sliderButton = "Slider button"
      static let local = "Local"
      static let cloud = "Cloud"
      static let swipe = "Swipe"
      static let jumpSlide = "Jump to slide"
      static let slider = "Slider"
      static let start = "Start"
      static let stop = "Stop"
      static let copy = "Copy"
      static let email = "Email"
      static let audio = "Audio"
      static let video = "Video"
      static let image = "Image"
      static let web = "Web"
      static let unknown = "Unknown"
      static let both = "Both"
      static let neither = "Neither"
      static let fullscreen = "Fullscreen"
      static let bubble = "Bubble"
      static let cancel = "Cancel"
      static let disconnect = "Disconnect"
      static let addButton = "Add button"
      static let itemPlaceholder = "Item placeholder"
      static let leaveJustMe = "Just Me"
      static let leaveEveryone = "Everyone"
      static let leaveDirectly = "Directly"
      static let smartAlbum = "Smart Album"
      static let userCreatedAlbum = "User Created Album"

      // Connection error
      static let hostNotFound = "Host not found"
      static let timeout = "Timeout"
      static let incorrectPassphrase = "Incorrect passphrase"
      static let certError = "Certificate error"
      
      // Disconnection. Some of them are hardcoded in Mezzkit since the propagation of the error starts there
      static let mezzanineMenu = "Mezzanine menu"
      static let backgroundTimeout = "Background timeout"
      static let audioOnlyLeave = "Audio-only leave"
      static let newMezzanineURL = "New mezzanine URL"

      // Hardcoded in Mezzkit
      static let heartbeatTimeout = "Heartbeat timeout"
      static let plasmaSessionEnded = "Plasma Session ended"
      static let ziggySessionEnded = "Ziggy Session ended"
      static let clientEvicted = "Client evicted"
      static let serverRestart = "Server restart"
      
      // Not used. We reconnect in this case and disconnected because of heartbeatTimeout
      static let websocketClosed = "Websocket closed"
    }
  }
  
  struct Screens {
    static let connection = "Connection View"
    static let workspace = "Workspace View"
    static let workspaceZoom = "Workspace Zoom"
    static let extendedWindshield = "Extended Windshield"
    static let workspaceList = "Workspace List View"
    static let localPreview = "Local preview"
    static let infopresence = "Infopresence View"
    static let infopresenceJoin = "Infopresence Join"
    static let infopresenceInviteMezzanines = "Infopresence Invite Mezzanines"
    static let infopresenceInviteParticipants = "Infopresence Invite Participants"
    static let passkey = "Passkey View"
    static let mezzInJoin = "Mezz-In Join View"
    static let vtcFullscreen = "VTC Fullscreen View"
    static let audioOnly = "Audio-only view"
  }
}
