//
//  WorkspaceOpeningViewController.m
//  Mezzanine
//
//  Created by Zai Chang on 3/20/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import "WorkspaceOpeningViewController.h"
#import "WorkspaceListCell.h"
#import "MezzanineStyleSheet.h"
#import "NSObject+BlockObservation.h"
#import "NSString+VersionChecking.h"

@interface WorkspaceOpeningViewController ()
{
  NSMutableArray *systemModelObservers;
  WorkspaceListCell *workspaceCell;
}

@end

@implementation WorkspaceOpeningViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
  [super viewDidLoad];
  
  self.accessibilityLabel = @"WorkspaceOpening";

  self.label.font = [UIFont dinBoldOfSize:19.0];
  self.label.text = NSLocalizedString(@"Workspace Opening", nil);
  self.label.textColor = [UIColor whiteColor];

  self.cellContainerView.backgroundColor = [UIColor clearColor];
  self.view.backgroundColor = [[MezzanineStyleSheet sharedStyleSheet].selectedBlueColor colorWithAlphaComponent:0.9];

  BOOL isIPad = [[UIDevice currentDevice] isIPad];
  if (isIPad)
  {
    self.cellContainerWidthConstraint.constant = 360.0;
    self.cellContainerHeightConstraint.constant = 100.0;
    [self.view layoutIfNeeded];
  }

  workspaceCell = [[WorkspaceListCell alloc] initWithFrame:self.cellContainerView.bounds];
  workspaceCell.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
  workspaceCell.systemModel = self.systemModel;
  workspaceCell.optionsButton.hidden = YES;
  workspaceCell.isOpeningViewCell = YES;
  [self.cellContainerView addSubview:workspaceCell];
  [workspaceCell setNeedsLayout];


  if (self.systemModel)
    [self updateView];
}


- (void)setSystemModel:(MZSystemModel *)systemModel
{
  if (systemModel != _systemModel)
  {
    if (_systemModel)
    {
      [_systemModel removeObserver:self forKeyPath:@"currentWorkspace.isLoading"];
    }
    
    _systemModel = systemModel;
    
    if (_systemModel)
    {
      [_systemModel addObserver:self forKeyPath:@"currentWorkspace.isLoading" options:(NSKeyValueObservingOptionInitial) context:nil];
    }
  }
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
  if ([keyPath isEqualToString:@"currentWorkspace.isLoading"])
  {
    [self updateView];
  }
}


- (void)updateView
{
  MZWorkspace *currentWorkspace = self.systemModel.currentWorkspace;
  
  if (workspaceCell.workspace != currentWorkspace)
    workspaceCell.workspace = currentWorkspace;
}

@end
