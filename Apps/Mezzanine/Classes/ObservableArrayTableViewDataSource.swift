//
//  ObservableArrayTableViewDataSource.swift
//  Mezzanine
//
//  Created by Zai Chang on 7/10/15.
//  Copyright (c) 2015 Oblong Industries. All rights reserved.
//

import UIKit


/// A general purpose UITableViewDataSource which binds to a model object that contains
/// a mutable array conforming to KVO, and propagates change events to the table view
///
// As much an attempt to re-use more code as it is to try generics in Swift, but ran into issues
// which required overriding generic functions in subclasses which is inelegant and ultimately
// confusing
//
// Would've done something like the following ... 
//
// public class ObservableArrayTableViewDataSource<ModelType: AnyObject, CellType: ModelTableViewCell<ModelType>>: NSObject, UITableViewDataSource
//
// but according to http://stackoverflow.com/questions/29585371/non-generic-swift-class-can-implement-uipickerviewdatasource-but-generic-version
// "Generic classes cannot be delegates for Foundation methods"
//
// Other articles http://stackoverflow.com/questions/31110748/generic-uitableviewdatasource-using-swift
//                http://stackoverflow.com/questions/26097581/generic-nsoperation-subclass-loses-nsoperation-functionality
//


// Not in use, and probably not in the near future, but keeping it around for reference and
// a nod to the use of generics in the future
//

/*
protocol ModelTableViewCell {
	typealias ModelType
	var model: ModelType { get set }
}

public class ObservableArrayTableViewDataSource: NSObject, UITableViewDataSource
{
	var tableView: UITableView
	var keyPath: String
	
	public init(model: NSObject, keyPath: String, tableView: UITableView) {
		self.model = model
		self.keyPath = keyPath
		self.tableView = tableView
		super.init()

		tableView.dataSource = self
		
		// Because the above assignment doesn't seem to invoke the didSet handler
		beginObservingModel()
	}
	
	deinit {
		endObservingModel()
	}
	
	/// Model
	///
	var model: NSObject! {
		willSet(aNewModel) {
			if self.model != aNewModel && self.model != nil
			{
				endObservingModel()
			}
		}
		didSet
		{
			beginObservingModel()
		}
	}
	
	func beginObservingModel()
	{
		model?.addObserver(self, forKeyPath: keyPath, options: .New | .Old, context: &kModelContext)
	}
	
	func endObservingModel()
	{
		model?.removeObserver(self, forKeyPath: keyPath, context: &kModelContext)
	}
	
	func observedArray() -> NSArray {
		return model.valueForKeyPath(keyPath) as! NSArray
	}
	
	public override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [NSObject : AnyObject]?, context: UnsafeMutablePointer<Void>)
	{
		switch (keyPath!, context) {
		case (keyPath!, &kModelContext):
			if let changeMap = change as? [NSString: AnyObject] {
//				let kind = changeMap[NSKeyValueChangeKindKey]
				
				if let inserted = changeMap[NSKeyValueChangeNewKey] as? NSArray {
					var indexPaths = Array<AnyObject>();
					for insertedItem in inserted {
						var indexPath = NSIndexPath(forRow: observedArray().indexOfObject(insertedItem), inSection: 0)
						indexPaths.append(indexPath)
					}
					
					if indexPaths.count > 0 {
						tableView.insertRowsAtIndexPaths(indexPaths, withRowAnimation: UITableViewRowAnimation.Automatic)
					}
				}
				
				if let deleted = changeMap[NSKeyValueChangeOldKey] as? NSArray,
					indexes = changeMap[NSKeyValueChangeIndexesKey] as? NSIndexSet {
						var indexPaths = Array<NSIndexPath>();
						indexes.enumerateIndexesUsingBlock({(index: Int, pointer: UnsafeMutablePointer<ObjCBool>) -> Void in
							let indexPath = NSIndexPath(forRow: index, inSection: 0)
							indexPaths.append(indexPath)
						});
						
						if indexPaths.count > 0 {
							tableView.deleteRowsAtIndexPaths(indexPaths, withRowAnimation: UITableViewRowAnimation.Automatic)
						}
				}
			}
		default:
			super.observeValueForKeyPath(keyPath!, ofObject: object!, change: change!, context: context)
		}
	}
	
	
	/// Items and Selection
	///
	public func itemAtIndexPath_internal(indexPath: NSIndexPath) -> AnyObject {
		return observedArray()[indexPath.row] as AnyObject;
	}
	
	public func selectedItems_internal() -> Array<AnyObject>
	{
		var results: Array<AnyObject> = []
		if let indexPaths = tableView.indexPathsForSelectedRows() {
			for indexPath in indexPaths {
				let item = itemAtIndexPath_internal(indexPath as! NSIndexPath)
				results.append(item)
			}
		}
		return results
	}
	
	
	/// UITableViewDelegate
	///
	public func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
	{
		return observedArray().count;
	}
	
	public func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
	{
//		let cell: ModelTableViewCell = tableView.dequeueReusableCellWithIdentifier(kCellIdentifier, forIndexPath: indexPath) as! ModelTableViewCell
//		
//		let item = itemAtIndexPath(indexPath)
//		cell.model = item;
//		
//		return cell
		return UITableViewCell()
	}
	
	
	private var kCellIdentifier = "cellIdentifier";
	private var kModelContext = 0
}
*/
