//
//  PassphraseViewController.h
//  Mezzanine
//
//  Created by Miguel Sánchez Valdés on 3/26/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PassphraseViewController;

@protocol PassphraseViewControllerDelegate <NSObject>

- (void)passkeyCharacterEntered;
- (void)passkeyWasIntroduced: (NSString *)passkey;
- (void)passkeyCancelled;

@end


@interface PassphraseViewController : UIViewController <UITextFieldDelegate>

-(instancetype) initWithKeyLength:(NSInteger) length;

@property (nonatomic, weak) id<PassphraseViewControllerDelegate> delegate;

@property (nonatomic, strong) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) IBOutlet UILabel *instructionsLabel;

@property (nonatomic, strong) IBOutlet UIView *textFieldsContainer;
@property (nonatomic, assign) IBOutlet NSLayoutConstraint *fieldContainerWidthConstraint;
@property (nonatomic, assign) IBOutlet NSLayoutConstraint *fieldContainerHeightConstraint;


@property (nonatomic, strong) IBOutlet id cancelButton;

- (IBAction)dismiss:(id)sender;
- (void)presentIncorrectPasskeyAlert;

@end
