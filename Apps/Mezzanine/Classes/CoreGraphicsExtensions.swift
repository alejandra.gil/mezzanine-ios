//
//  CoreGraphicsExtensions.swift
//  Mezzanine
//
//  Created by miguel on 28/2/17.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

extension CGFloat {

  func roundNearest(_ nearest: CGFloat) -> CGFloat {
    let n = 1/nearest
    return Darwin.round(self * n) / n
  }

  func roundNearestPixel() -> CGFloat {
    let n = UIScreen.main.scale
    return Darwin.round(self * n) / n
  }
}

extension CGRect {
  func roundedFrame() -> CGRect {
    return CGRect(x: minX.roundNearestPixel(), y: minY.roundNearestPixel(), width: width.roundNearestPixel(), height: height.roundNearestPixel())
  }
}

extension CGPoint {
  func roundedPoint() -> CGPoint {
    return CGPoint(x: x.roundNearestPixel(), y: y.roundNearestPixel())
  }
}
