//
//  OBRestrictedPageViewController.swift
//  Mezzanine
//
//  Created by miguel on 12/10/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

import UIKit

protocol OBRestrictedPageViewControllerDelegate: class {
  func restrictedPageViewControllerDidScroll(_ scrollView: UIScrollView)
}

class OBRestrictedPageViewController: UIPageViewController, UIGestureRecognizerDelegate, UIScrollViewDelegate {
  weak var restrictedPageViewControllerDelegate: OBRestrictedPageViewControllerDelegate?

  fileprivate var scrollViewPanGestureRecognizer = UIPanGestureRecognizer()
  var swipeAreaView = UIView()
  var swipeAreaHeight: CGFloat = 0

  override func viewDidLoad() {
    super.viewDidLoad()

    self.view.clipsToBounds = false
    // http://stackoverflow.com/questions/30222153/how-to-get-uipageviewcontroller-to-not-cliptobounds
    // Disable clipsToBounds on _UIQueuingScrollView
    if self.view.subviews.count > 0 {
      self.view.subviews[0].clipsToBounds = false
    }

    self.view.backgroundColor = UIColor.clear

    let scrollView = self.view.subviews.filter { $0 is UIScrollView }.first as! UIScrollView
    scrollView.delegate = self
    scrollViewPanGestureRecognizer.delegate = self;
    scrollView.addGestureRecognizer(scrollViewPanGestureRecognizer)
  }

  // MARK: UIGestureRecognizerDelegate

  func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
    return false
  }

  func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
    if gestureRecognizer == scrollViewPanGestureRecognizer {
      let locationInView = gestureRecognizer.location(in: self.view)
      if locationInView.y > swipeAreaHeight {
        return true
      }
    }
    return false
  }
  
  
  // MARK: UIScrollViewDelegate
  
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    if restrictedPageViewControllerDelegate != nil {
      restrictedPageViewControllerDelegate?.restrictedPageViewControllerDidScroll(scrollView)
    }
  }
}
