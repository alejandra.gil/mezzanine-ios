//
//  MezzanineDocumentInteractionController.m
//  Mezzanine
//
//  Created by Zai Chang on 9/18/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import "MezzanineDocumentInteractionController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "MezzanineStyleSheet.h"
#import "NSString+VersionChecking.h"
#import "NSObject+UIAlertController.h"

@implementation MezzanineDocumentInteractionController

@synthesize maximumFileSizeInBytes;
@synthesize isUploadingToParamus;
@synthesize isUploadingToDeck;


-(id) init
{
  self = [super init];
  if (self)
  {
    pdfRenderingQueue = [[NSOperationQueue alloc] init];
    pdfRenderingQueue.maxConcurrentOperationCount = 1;
    
    if ([[UIDevice currentDevice] isIPad3OrNewer] ||
        [[UIDevice currentDevice] isIPhone5OrNewer])
      // These devices have 1GB RAM
      maximumFileSizeInBytes = 150*1024*1024;
    else if ([[UIDevice currentDevice] isIPad2OrNewer] ||
        [[UIDevice currentDevice] isIPhone4OrNewer] ||
        [[UIDevice currentDevice] isIPod5thGenOrNewer])
      // These devices have 512GB RAM
      maximumFileSizeInBytes = 100*1024*1024;
    else
      // Lower limit for older devices
      maximumFileSizeInBytes = 60*1024*1024;
  }
  return self;
}


-(NSArray*) createSlideImagesWithText:(NSString *)textContent
{
  CGRect bounds = CGRectMake(0.0, 0.0, 1920, 1080);
  CGSize size = bounds.size;
  
  NSMutableArray *images = [[NSMutableArray alloc] init];
  
  UIColor *slideBackgroundColor = [MezzanineStyleSheet sharedStyleSheet].grey60Color;
  while (![textContent isEqualToString:@""])
  {
    UIGraphicsBeginImageContext(size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, slideBackgroundColor.CGColor);
    CGContextFillRect(context, CGRectMake(0, 0, size.width, size.height));
    
    CGFloat separatorLineYPosition = size.height - 100.0;
    
    // Draw a separation line
    CGContextSetStrokeColorWithColor(context, [UIColor whiteColor].CGColor);
    CGContextSetLineWidth(context, 1.0);
    CGContextMoveToPoint(context, 0.0, separatorLineYPosition);
    CGContextAddLineToPoint(context, size.width, separatorLineYPosition);
    CGContextStrokePath(context);
    
    
    CGFloat margin = 60.0;
    CGFloat fontSize = 32.0;
    CGFloat y = bounds.size.height-margin-fontSize/2.0;
    UIFont *font = [UIFont dinOfSize:fontSize];
    CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
    
    // Draw left-aligned workspace name

    NSString *workspaceName = [MezzanineAppContext currentContext].applicationModel.systemModel.currentWorkspace.name;

    CGFloat workspaceNameMaxWidth = 1000.0;
    CGRect workspaceNameRect = CGRectMake(margin, y, workspaceNameMaxWidth, fontSize * 1.3);

    NSMutableParagraphStyle *workspaceNameStyle = [[NSMutableParagraphStyle alloc] init];
    workspaceNameStyle.lineBreakMode = NSLineBreakByTruncatingTail;
    workspaceNameStyle.alignment = NSTextAlignmentLeft;

    [workspaceName drawInRect:workspaceNameRect
                        withAttributes:@{NSFontAttributeName:font,
                                         NSParagraphStyleAttributeName:workspaceNameStyle,
                                         NSForegroundColorAttributeName:[UIColor whiteColor]}];

    // Draw right-aligned date
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEEE, MMMM d y HH:mm"];
    NSString *dateString = [dateFormatter stringFromDate:[NSDate date]];
    CGRect dateStringRect = CGRectMake(CGRectGetMaxX(workspaceNameRect) + margin, y, bounds.size.width - CGRectGetMaxX(workspaceNameRect) - 2 * margin, fontSize * 1.3);

    NSMutableParagraphStyle *dateStringStyle = [[NSMutableParagraphStyle alloc] init];
    dateStringStyle.lineBreakMode = NSLineBreakByTruncatingTail;
    dateStringStyle.alignment = NSTextAlignmentRight;

    [dateString drawInRect:dateStringRect
             withAttributes:@{NSFontAttributeName:font,
                              NSParagraphStyleAttributeName:dateStringStyle,
                              NSForegroundColorAttributeName:[UIColor whiteColor]}];

    // Draw some textContent that fits in a slide
    NSString *text = textContent;
    if ([text length] > 0)
    {
      CGFloat border = 60.0;
      CGFloat fontSize = 60.0;
      UIFont *font = [UIFont dinOfSize:fontSize];
      
      CGRect textBounds = CGRectMake (bounds.origin.x+border,
                                      bounds.origin.y+border,
                                      bounds.size.width-2*border,
                                      bounds.size.height-2*border-100);
      
      NSArray *listItems = [text componentsSeparatedByString:@"\n"];
      
      NSString *stringThatFits = [NSString new];
      for (NSString *item in listItems)
      {
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;

        CGSize totalSize = [stringThatFits boundingRectWithSize:CGSizeMake(textBounds.size.width, MAXFLOAT)
                                                        options:NSStringDrawingUsesLineFragmentOrigin
                                                     attributes:@{NSFontAttributeName:font,
                                                                  NSParagraphStyleAttributeName:paragraphStyle}
                                                        context:nil].size;

        if (totalSize.height > textBounds.size.height)
          break;
        
        stringThatFits = [stringThatFits stringByAppendingFormat:@"%@\n",item];
      }
      
      NSInteger stfl = [stringThatFits length];
      NSInteger tcl = [textContent length];
      if (tcl-stfl<0)
        textContent = @"";
      else
        textContent = [text substringWithRange:NSMakeRange(stfl, MAX(0,tcl-stfl))];
      
      CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);

      NSMutableParagraphStyle *stringThatFitsStyle = [[NSMutableParagraphStyle alloc] init];
      stringThatFitsStyle.lineBreakMode = NSLineBreakByWordWrapping;
      stringThatFitsStyle.alignment = NSTextAlignmentLeft;

      [stringThatFits drawInRect:textBounds
              withAttributes:@{NSFontAttributeName:font,
                               NSParagraphStyleAttributeName:stringThatFitsStyle,
                               NSForegroundColorAttributeName:[UIColor whiteColor]}];
    }
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    [images addObject:image];
  }
  
  return images;
}

-(UIImage*) imageOfPDF:(CGPDFDocumentRef)pdfDocument atPage:(NSInteger)pageNumber
{
  CGPDFPageRef page = CGPDFDocumentGetPage(pdfDocument, pageNumber);
  CGRect cropBox = CGPDFPageGetBoxRect(page, kCGPDFCropBox);
  UIGraphicsBeginImageContextWithOptions(cropBox.size, NO, 1.0);
  
  CGContextRef context = UIGraphicsGetCurrentContext();
  CGContextTranslateCTM(context, 0.0, cropBox.size.height);
  CGContextScaleCTM(context, 1.0, -1.0);
  CGContextDrawPDFPage(context, page);
  
  UIImage *pageImage = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  return pageImage;
}


-(BOOL) canUploadDocumentAtURL:(NSURL*)url error:(NSError**)error
{
  NSString *filename = url.lastPathComponent;
  
  // Does file exist?
  if (![NSFileManager.defaultManager fileExistsAtPath:url.path])
  {
    if (error)
      *error = [NSError errorWithDomain:MezzanineDocumentInteractionControllerDomain
                                   code:MezzanineFileUploadError
                               userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:NSLocalizedString(@"File Does Not Exist Error Message", nil), filename]}];
    return NO;
  }
  
  if (self.isBusy)
  {
    if (error)
      *error = [NSError errorWithDomain:MezzanineDocumentInteractionControllerDomain
                                   code:MezzanineFileUploadError
                               userInfo:@{NSLocalizedDescriptionKey:NSLocalizedString(@"Another Upload In Progress Error Message", nil)}];
    return NO;
  }

  CFStringRef fileExtension = (__bridge CFStringRef) [url pathExtension];
  CFStringRef fileUTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, fileExtension, NULL);

  BOOL isImageOrPDF = NO;
  MZSystemModel *systemModel = MezzanineAppContext.currentContext.currentCommunicator.systemModel;
  
  if (UTTypeConformsTo(fileUTI, kUTTypePDF))
  {
    CGPDFDocumentRef pdfDocument = CGPDFDocumentCreateWithURL((__bridge CFURLRef)url);
    BOOL result = (pdfDocument != NULL);
    CGPDFDocumentRelease(pdfDocument);

    if (!result)
    {
      CFRelease(fileUTI);
      return NO;
    }

    isImageOrPDF = YES;
  }
  else if (UTTypeConformsTo(fileUTI, kUTTypeText) ||
           UTTypeConformsTo(fileUTI, kUTTypePlainText) ||
           UTTypeConformsTo(fileUTI, kUTTypeUTF8PlainText) ||
           UTTypeConformsTo(fileUTI, kUTTypeUTF16ExternalPlainText) ||
           UTTypeConformsTo(fileUTI, kUTTypeUTF16PlainText))
  {
    CFRelease(fileUTI);
    return YES;
  }
  else if (UTTypeConformsTo(fileUTI, kUTTypeRTF))
  {
    CFRelease(fileUTI);
    return YES;
  }
  else // Check if its some sort of image
  {
    UIImage *image = [UIImage imageWithContentsOfFile:url.path];
    if (image)
    {
      NSInteger maxImageFileSizeInBytes = [systemModel.uploadLimits.maxImageSizeInMB integerValue]*1024*1024;
      
      if (systemModel.featureToggles.largeImagesEnabled || systemModel.featureToggles.immenseImagesEnabled)
      {
        NSInteger maxImageSizeInPixels = [systemModel.uploadLimits.maxImageSizeInMP integerValue]*1024*1024;
        NSInteger imagePixelSize = CGImageGetWidth(image.CGImage) * CGImageGetHeight(image.CGImage);
        if (imagePixelSize > maxImageSizeInPixels)
        {
          if (error)
            *error = [NSError errorWithDomain:MezzanineDocumentInteractionControllerDomain
                                         code:MezzanineFileUploadError
                                     userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:NSLocalizedString(@"Image File Upload Exceeded Megapixels Error Dialog Message", nil), filename, systemModel.uploadLimits.maxImageSizeInMP]}];
          CFRelease(fileUTI);
          return NO;
        }
      }
      else
      {
        NSInteger maxImageWidth = [systemModel.uploadLimits.maxImageWidth integerValue];
        NSInteger maxImageHeight = [systemModel.uploadLimits.maxImageHeight integerValue];
        CGSize imageSize = image.size;
        if (imageSize.width > maxImageWidth || imageSize.height > maxImageHeight)
        {
          if (error)
            *error = [NSError errorWithDomain:MezzanineDocumentInteractionControllerDomain
                                         code:MezzanineFileUploadError
                                     userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:NSLocalizedString(@"Image File Upload Pixel Dimension Error Dialog Message", nil), filename, @(maxImageWidth), @(maxImageHeight)]}];
          CFRelease(fileUTI);
          return NO;
        }
        
      }

      NSData *imageData = nil;
      if (UTTypeConformsTo(fileUTI, kUTTypePNG))
      {
        imageData = UIImagePNGRepresentation(image);
      }
      else if (UTTypeConformsTo(fileUTI, kUTTypeJPEG) || UTTypeConformsTo(fileUTI, kUTTypeJPEG2000))
      {
        imageData = UIImageJPEGRepresentation(image, 0.9);
      }

      if (imageData && imageData.length > maxImageFileSizeInBytes)
      {
        if (error)
          *error = [NSError errorWithDomain:MezzanineDocumentInteractionControllerDomain
                                       code:MezzanineFileUploadError
                                   userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:NSLocalizedString(@"Image File Upload File Size Error Dialog Message", nil), filename, @(maxImageFileSizeInBytes/1024/1024)]}];
        CFRelease(fileUTI);
        return NO;
      }
      
    }
    isImageOrPDF = YES;
  }

  if (! isImageOrPDF)
  {
    if (error)
      *error = [NSError errorWithDomain:MezzanineDocumentInteractionControllerDomain
                                   code:MezzanineFileFormatNotSupportedError
                               userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:NSLocalizedString(@"File Format Error Message", nil), filename]}];
    CFRelease(fileUTI);
    return NO;
  }
  
  NSDictionary *dictionary = [NSFileManager.defaultManager attributesOfItemAtPath:url.path error:nil];
  if (dictionary)
  {
    NSNumber *fileSize = dictionary[NSFileSize];
    
    NSInteger maxFileSizeInBytes = maximumFileSizeInBytes;
    if (UTTypeConformsTo(fileUTI, kUTTypePDF))
    {
      NSInteger maxPDFSizeInMB = systemModel.uploadLimits.maxPDFSizeInMB.integerValue;
      if (maxPDFSizeInMB > 0)
        maxFileSizeInBytes = maxPDFSizeInMB*1024*1024;
    }
    else
    {
      NSInteger maxImageSizeInMB = systemModel.uploadLimits.maxImageSizeInMB.integerValue;
      if (maxImageSizeInMB > 0)
        maxFileSizeInBytes = maxImageSizeInMB*1024*1024;
    }
    
    // Enforce the local limit for file size even if server has max sizes
    // because large files would cause the iOS app to crash (see bug 9275)
    maxFileSizeInBytes = MIN(maximumFileSizeInBytes, maxFileSizeInBytes);
    
    if (fileSize.integerValue > maxFileSizeInBytes)
    {
      if (error)
      {
        NSString *fileSizeString = [@(maxFileSizeInBytes / 1024 / 1024) stringValue];
        *error = [NSError errorWithDomain:MezzanineDocumentInteractionControllerDomain
                                     code:MezzanineFileUploadError
                                 userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:NSLocalizedString(@"File Upload File Size Error Dialog Message", nil), filename, fileSizeString]}];
      }
      CFRelease(fileUTI);
      return NO;
    }
  }

  CFRelease(fileUTI);
  return YES;
}


-(NSInteger) numberOfPagesForDocumentAtURL:(NSURL*)url
{
  NSString *extension = url.pathExtension;
  if ([extension isEqual:@"pdf"])
  {
    CGPDFDocumentRef pdfDocument = CGPDFDocumentCreateWithURL((__bridge CFURLRef)url);
    if (pdfDocument)
    {
      NSInteger numberOfPages = CGPDFDocumentGetNumberOfPages(pdfDocument);
      CGPDFDocumentRelease(pdfDocument);
      return numberOfPages;
    }
  }
  
  return 0;
}


-(void) updateTransactionStatusHack:(MZSystemModel*)systemModel
{
  // Somewhat hacky way to force a refresh of deck view status. Otherwise status shows blank until
  // the second PDF page
  MZTransaction *transaction = [MZTransaction transactionWithProtein:nil];
  NSInteger index = systemModel.pendingTransactions.count;
  [systemModel insertObject:transaction inPendingTransactionsAtIndex:index];
  [systemModel removeObjectFromPendingTransactionsAtIndex:index];
}


-(void) uploadDocumentAtURL:(NSURL*)url usingCommunicator:(MZCommunicator*)communicator
{
  NSError *error = nil;
  if (![self canUploadDocumentAtURL:url error:&error])
  {
    NSString *errorDescription = [error localizedDescription];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Upload FileTypeNotSupported Dialog Title", nil)
                                                                             message:(errorDescription ? errorDescription : NSLocalizedString(@"Upload FileTypeNotSupported Dialog Message", nil))
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Generic Button Title Okay", nil) style:UIAlertActionStyleCancel handler:nil];
    [alertController addAction:cancelAction];
    [self showAlertController:alertController animated:YES completion:nil];
    return;
  }
  
  if (MezzanineAppContext.currentContext.currentCommunicator.systemModel.passphraseRequested)
  {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Upload SessionLockedError Dialog Title", nil)
                                                                             message:NSLocalizedString(@"Upload SessionLockedError Dialog Message", nil)
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Generic Button Title Okay", nil) style:UIAlertActionStyleCancel handler:nil];
    [alertController addAction:cancelAction];
    [self showAlertController:alertController animated:YES completion:nil];
    return;
  }
  
  CFStringRef fileExtension = (__bridge CFStringRef) [url pathExtension];
  CFStringRef fileUTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, fileExtension, NULL);
  
  if (UTTypeConformsTo(fileUTI, kUTTypePDF))
  {
    // PDF
    CGPDFDocumentRef pdfDocument = CGPDFDocumentCreateWithURL((__bridge CFURLRef)url);
    if (pdfDocument)
    {
      MZFileUploadInfo *uploadInfo = [[MZFileUploadInfo alloc] init];
      uploadInfo.format = @"pdf";
      uploadInfo.fileName = [[[url absoluteString] lastPathComponent] stringByRemovingPercentEncoding];
      uploadInfo.fileURL = url;
      [communicator.uploader requestFilesUpload:@[uploadInfo] workspaceUid:[MezzanineAppContext currentContext].applicationModel.systemModel.currentWorkspace.uid limitReached:nil singleUploadCompleted:nil errorHandler:nil];
    }
    
    return;
  }
  
  if (UTTypeConformsTo(fileUTI, kUTTypeRTF))
  {
    NSError *error = nil;
    NSAttributedString* attributedFileContents = [[NSAttributedString alloc] initWithURL:url options:@{} documentAttributes:nil error:&error];
    NSString *plainTextString = [attributedFileContents string];
    
    if (!error)
    {
      [self uploadSlidesWithText:plainTextString];
      //Future: [self uploadSlidesWithAttributedText:attributedFileContents];
    }

    return;
  }

  if (UTTypeConformsTo(fileUTI, kUTTypeText) ||
      UTTypeConformsTo(fileUTI, kUTTypePlainText) ||
      UTTypeConformsTo(fileUTI, kUTTypeUTF8PlainText) ||
      UTTypeConformsTo(fileUTI, kUTTypeUTF16ExternalPlainText) ||
      UTTypeConformsTo(fileUTI, kUTTypeUTF16PlainText))
  {
    NSString* fileContents = [[NSString alloc] initWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
    DLog(@"%@", fileContents);
    [self uploadSlidesWithText:fileContents];
    
    return;
  }
  
  // Check if its some sort of image
  UIImage *image = [UIImage imageWithContentsOfFile:url.path];
  if (image)
  {
    MZFileUploadInfo *uploadInfo = [[MZFileUploadInfo alloc] init];
    uploadInfo.format = (__bridge NSString*)fileUTI;
    uploadInfo.fileName = [[[url absoluteString] lastPathComponent] stringByRemovingPercentEncoding];
    uploadInfo.imageBlock = ^{ return image; };
    [communicator.uploader requestFilesUpload:@[uploadInfo] workspaceUid:[MezzanineAppContext currentContext].applicationModel.systemModel.currentWorkspace.uid limitReached:nil singleUploadCompleted:nil errorHandler:nil];
  }
}

- (void)uploadSlidesWithText:(NSString *)text
{
  if (!text || [text isEqualToString:@""])
  {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Upload FileTypeNotSupported Dialog Title", nil)
                                                                             message:nil
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Generic Button Title Okay", nil) style:UIAlertActionStyleCancel handler:nil];
    [alertController addAction:cancelAction];
    [self showAlertController:alertController animated:YES completion:nil];
    return;
  }
  
  NSArray *images = [self createSlideImagesWithText:text];
  MZCommunicator *communicator = [MezzanineAppContext currentContext].currentCommunicator;
  
  NSInteger numberOfPages = images.count;
  NSInteger i=1;
  NSMutableArray *uploadInfos = [NSMutableArray array];
  for (MZImage *image in images)
  {
    MZFileUploadInfo *uploadInfo = [[MZFileUploadInfo alloc] init];
    uploadInfo.fileName = [NSString stringWithFormat:NSLocalizedString(@"Text Slide Page Number Text", nil), (long)i++, (long)numberOfPages];
    uploadInfo.format = @"jpg";
    uploadInfo.imageBlock = ^{ return image; };
    [uploadInfos addObject:uploadInfo];
  }

  MZUploader *uploader = communicator.uploader;
  [uploader requestFilesUpload:uploadInfos
                  workspaceUid:communicator.systemModel.currentWorkspace.uid
                  limitReached:^(MZFileBatchUploadRequestTransaction *requestTransaction,
                                 NSArray *uids,
                                 MZFileBatchUploadRequestContinuationBlock continueBlock) {
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Upload Continuation Dialog Title", nil)
                                                                                             message:[NSString stringWithFormat:NSLocalizedString(@"Upload Continuation Dialog Message", nil), (long)uids.count, (long)numberOfPages]
                                                                                      preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Upload Continuation Dialog Cancel Button", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                      continueBlock(requestTransaction, uids, false);
                    }];
                    UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Upload Continuation Dialog Confirm Button", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                      continueBlock(requestTransaction, uids, true);
                    }];
                    [alertController addAction:cancelAction];
                    [alertController addAction:confirmAction];
                    [self showAlertController:alertController animated:YES completion:nil];
                  }
         singleUploadCompleted:nil
                  errorHandler:nil];
}


-(BOOL) isBusy
{
  return (pdfRenderingQueue.operationCount > 0);
}


-(void) cancel
{
  // Don't cancel the last operation since its the cleanup operation
  for (NSInteger i=0; i < pdfRenderingQueue.operations.count-1; i++)
  {
    NSOperation *operation = (pdfRenderingQueue.operations)[i];
    [operation cancel];
  }
}

@end
