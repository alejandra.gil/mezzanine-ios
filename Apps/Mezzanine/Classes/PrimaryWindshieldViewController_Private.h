//
//  PrimaryWindshieldViewController_Private.h
//  Mezzanine
//
//  Created by Miguel Sánchez Valdés on 09/10/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

#import "PrimaryWindshieldViewController.h"

@interface PrimaryWindshieldViewController ()

- (CGPoint)shiftPointFromWindshieldViewToShielderContainer:(CGPoint)pointInWindshieldViewCoordinates;
- (CGPoint)shiftPointFromShielderContainerToWindshieldView:(CGPoint)pointInShielderViewCoordinates;
- (CGPoint)convertPointFromWindshieldToScrollViewCoordinates:(CGPoint)pointInWindshieldCoordinates;

@end
