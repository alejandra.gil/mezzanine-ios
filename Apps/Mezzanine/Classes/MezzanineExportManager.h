//
//  MezzanineExportManager.h
//  Mezzanine
//
//  Created by Miguel Sánchez on 16/01/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuickLook/QuickLook.h>
#import "MZExportInfo.h"

@interface MezzanineExportManager : NSObject <QLPreviewControllerDataSource, QLPreviewControllerDelegate, UIDocumentInteractionControllerDelegate>

@property (nonatomic, assign) NSUInteger maximumFileSizeInBytes;
@property (nonatomic, strong) UIView *openInPopoverAnchorView;
@property (nonatomic, strong) MZExportInfo *portfolioExportInfo;

- (void)checkPendingCompletedExports;

@end
