//
//  MezzanineUserActivities.swift
//  Mezzanine
//
//  Created by miguel on 30/11/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

import Foundation

// The activities should be equal to the NSUserActivityTypes in all Info.plist files
struct MezzanineUserActivityTypes {
  static let connected = "com.oblong.mezzanine.connected"
}

class MezzanineUserActivities: NSObject {
  @objc class func connected() -> String { return MezzanineUserActivityTypes.connected }
}
