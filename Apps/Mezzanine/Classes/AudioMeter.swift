//
//  AudioMeter.swift
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 04/11/2016.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

import UIKit
import AVFoundation

protocol AudioMeterDelegate: class {
  func audioMeterDidUpdateValue(_ value: CGFloat)
}

class AudioMeter: NSObject, AVAudioRecorderDelegate {
  weak var delegate: AudioMeterDelegate?
  var audioSession: AVAudioSession!
  fileprivate var audioRecorder: AVAudioRecorder!
  fileprivate var timer: Timer!
  fileprivate let fileName = String("recording.m4a")
  
  fileprivate var currentAvgPw: Array <Float> = Array()
  fileprivate var normalizedDb: CGFloat = 0.0
  fileprivate var resetTimer: Timer = Timer()
  
  required init(delegate: AudioMeterDelegate) {
    super.init()
    self.delegate = delegate
    
    startRecording()
  }
  
  deinit {
    finishRecording()
  }
  
  
  func startRecording() {
    if timer != nil {
      timer.invalidate()
    }
    
    timer = Timer.scheduledTimer(timeInterval: 1/15, target: self, selector: #selector(self.updateMeter), userInfo: nil, repeats: true)
    
    let audioFilename = getTempDirectoryURL()?.appendingPathComponent(fileName)
    
    let settings = [
      AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
      AVSampleRateKey: 48000,
      AVNumberOfChannelsKey: 1,
      AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
    ]
    
    do {
      audioSession = AVAudioSession.sharedInstance()
      try audioSession.setCategory(AVAudioSessionCategoryPlayAndRecord, with:AVAudioSessionCategoryOptions.mixWithOthers)
      try audioSession.setActive(true)
      
      audioRecorder = try AVAudioRecorder(url: audioFilename!, settings: settings)
      audioRecorder.delegate = self
      audioRecorder.isMeteringEnabled = true
      audioRecorder.prepareToRecord()
      audioRecorder.record()
      audioRecorder.isMeteringEnabled = true
      
    } catch {
      finishRecording()
    }
  }
  
  
  func finishRecording() {
    if delegate != nil {
      delegate?.audioMeterDidUpdateValue(1.0)
    }
    
    timer.invalidate()
    timer = nil
    
    audioRecorder.stop()
    audioRecorder = nil
    
    clearTempFolder()
  }
  
  
  @objc func updateMeter() {
    audioRecorder.updateMeters()
    let averagePower = audioRecorder.averagePower(forChannel: 0)
    
    // Calculate average of total session sound values
    currentAvgPw.append(averagePower)
    let totalAvg = currentAvgPw.reduce(0.0) {
      return $0 + $1/Float(currentAvgPw.count)
    }
    
    if averagePower > -35.0 || (totalAvg - averagePower) < -10  {
      let min: Float = -35.0
      let max: Float = 0
      let normalizedMin: Float = 1.0
      let normalizedMax: Float = 2.0
      
      //Normalization formula between a given range: f(x) = ((b-a)*(x-min) / (max-min)) + a
      let a: Float = (normalizedMax - normalizedMin) * (averagePower - min)
      normalizedDb = CGFloat(( a / (max - min)) + normalizedMax)
      resetTimer.invalidate()
      
      if delegate != nil {
        delegate?.audioMeterDidUpdateValue(normalizedDb)
      }
      
    } else {
      if resetTimer.isValid == false {
        resetTimer =  Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(resetValue), userInfo: nil, repeats: false)
      }
    }
  }
  
  @objc func resetValue() {
    if delegate != nil {
      delegate?.audioMeterDidUpdateValue(1.0)
    }
  }
  
  // MARK: I/O
  func getTempDirectoryURL() -> URL? {
    return URL.init(fileURLWithPath: NSTemporaryDirectory())
  }
  
  
  func clearTempFolder() {
    let fileManager = FileManager.default
    do {
      try fileManager.removeItem(atPath: NSTemporaryDirectory() + fileName)
    } catch {
      print("Could not clear temp folder: \(error)")
    }
  }
  
  // MARK: AVAudioRecorderDelegate
  func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
    if !flag {
      self.finishRecording()
    }
  }
}


