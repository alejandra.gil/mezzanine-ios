//
//  PanelView.swift
//  Mezzanine
//
//  Created by Miguel Sanchez on 30/6/15.
//  Copyright (c) 2015 Oblong Industries. All rights reserved.
//

import UIKit

class PanelView: UIView
{
  var panelSize: CGSize = CGSize.zero

  var widthPanelConstraint = NSLayoutConstraint()
  var spaceToPreviousPanelConstraint = NSLayoutConstraint()
  let lateralShadowLayer = CAGradientLayer()
  var panelSideType: SlidingPanelOrigin

  init(frame: CGRect, sideType: SlidingPanelOrigin) {

    self.panelSideType = sideType

    super.init(frame: frame)

    lateralShadowLayer.colors = [UIColor(white: 0.0, alpha: 0.28).cgColor, UIColor(white: 0.0, alpha: 0.0).cgColor]
    switch panelSideType{
    case .left:
      lateralShadowLayer.startPoint = CGPoint.zero
      lateralShadowLayer.endPoint = CGPoint(x: 1, y: 0)
    case .right:
      lateralShadowLayer.startPoint = CGPoint(x: 1, y: 0)
      lateralShadowLayer.endPoint = CGPoint.zero
    }
    self.layer.insertSublayer(lateralShadowLayer, at:0)
  }

  required init(coder aDecoder: NSCoder) {

      fatalError("init(coder:) has not been implemented")
  }

  var controller: UIViewController? {

    willSet(aNewController) {

      self.controller?.view.removeFromSuperview()
    }
    didSet {

      let theController = self.controller!
      self.addSubview(theController.view)
      theController.view.translatesAutoresizingMaskIntoConstraints = false
      self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[contentView]-0-|", options:NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["contentView":theController.view]))
      self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[contentView]-0-|", options:NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["contentView":theController.view]))
    }
  }

  override func layoutSubviews() {

    super.layoutSubviews()
    switch panelSideType{
    case .left:
      lateralShadowLayer.frame = CGRect(x: frame.width, y: frame.minY, width: 8.0, height: frame.height)
    case .right:
      lateralShadowLayer.frame = CGRect(x: -8.0, y: frame.minY, width: 8.0, height: frame.height)
    }
  }
}
