//
//  MezzanineExportManager_Private.h
//  Mezzanine
//
//  Created by miguel on 26/11/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

#import "MezzanineExportManager.h"

@interface MezzanineExportManager ()

@property (nonatomic, strong) NSMutableArray *filesToPreview;
@property (nonatomic, strong) UIDocumentInteractionController *openInDocInteractionController;
@property (nonatomic, strong) NSMutableArray *exportCompletedInfo;
@property (nonatomic, strong) QLPreviewController *preview;
@property (nonatomic, strong) UIAlertController *exportCompleteAlertController;
@property BOOL willUpdatePreview;

- (void)addNewExportedDocument:(MZExportInfo *)exportInfo;
- (void)presentAlertForExportCompleted;
- (void)loadOpenInMenuForFile:(NSString *)filepath;
- (void)presentAlertForNoPDFReaderAppAvailable;
- (void)loadQuickLookPreview;

@end
