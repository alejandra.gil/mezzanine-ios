//
//  MezzanineWindow.m
//  Mezzanine
//
//  Created by Zai Chang on 1/28/13.
//  Copyright (c) 2013 Oblong Industries. All rights reserved.
//

#import "MezzanineWindow.h"


@interface UIView (Utilities)

-(BOOL) containsView:(UIView*)view;

@end



@implementation UIView (Utilities)

-(BOOL) containsView:(UIView*)view
{
  for (UIView *subview in self.subviews)
  {
    if (subview == view)
      return YES;
    
    if ([subview containsView:view])
      return YES;
  }
  
  return NO;
}

@end



@implementation MezzanineWindow

-(void) registerContextView:(UIView*)view sublayer:(CALayer*)sublayer withDismissBlock:(void (^)())dismissBlock
{
  if (contextView != view || contextViewSublayer != sublayer)
  {
    contextView = view;
    contextViewSublayer = sublayer;
    contextViewDismissBlock = [dismissBlock copy];
  }
}


-(void) clearContextView:(UIView*)view
{
  if (contextView == view)
  {
    contextView = nil;
    contextViewSublayer = nil;
    contextViewDismissBlock = nil;
  }
}


-(void) sendEvent:(UIEvent *)event
{
  if (contextView)
  {
    NSSet *touches = [event allTouches];
    // Intercept touches
    if([touches count] == 1)
    {
      UITouch *touch = [touches anyObject];
      if(touch.phase == UITouchPhaseBegan)
      {
        BOOL touchOutside = NO;
        CGPoint location = [touch locationInView:contextView];
        UIView *hitView = [self hitTest:[touch locationInView:self] withEvent:event];
        if (hitView != contextView && ![contextView containsView:hitView])
        {
          touchOutside = YES;
        }
        else
        {
          // If a sublayer is present, check that the touch is outside of that particular sublayer
          if (contextViewSublayer)
          {
            CGPoint layerPoint = [contextView.layer convertPoint:location toLayer:contextViewSublayer];
            if (![contextViewSublayer containsPoint:layerPoint])
              touchOutside = YES;
          }
        }
        
        if (touchOutside && contextViewDismissBlock)
        {
          contextViewDismissBlock();
        }
      }
    }
  }
  
  [super sendEvent:event];
}

@end
