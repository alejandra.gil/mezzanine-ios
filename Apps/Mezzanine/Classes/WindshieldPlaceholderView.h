//
//  WindshieldPlaceholderView.h
//  Mezzanine
//
//  Created by miguel on 14/04/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OBDragDropManager.h"

@interface WindshieldPlaceholderView : UIView

@property (nonatomic, weak) OBOvum *ovum;

@end
