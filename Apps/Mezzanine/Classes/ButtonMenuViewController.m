//
//  ButtonMenuViewController.m
//  Mezzanine
//
//  Created by Zai Chang on 6/19/13.
//  Copyright (c) 2013 Oblong Industries. All rights reserved.
//

#import "ButtonMenuViewController.h"
#import "MezzanineStyleSheet.h"
#import "NSString+Accesibility.h"

@interface ButtonMenuViewController ()

@end



@implementation ButtonMenuViewController

static CGFloat marginX = 0.0;
static CGFloat marginY = 12.0;
static CGFloat buttonHeight = 50.0;
static CGFloat interitemSpacing = 12.0;
static CGFloat rowWidth = 200.0;
static NSString *cellIdentifier = @"ButtonMenuCell";

@synthesize tableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
  [super viewDidLoad];

  MezzanineStyleSheet *styleSheet = [MezzanineStyleSheet sharedStyleSheet];
  self.view.backgroundColor = styleSheet.darkestFillColor;

  tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
  tableView.delegate = self;
  tableView.dataSource = self;
  tableView.scrollEnabled = NO;
  tableView.backgroundColor = [UIColor whiteColor];
  tableView.separatorColor = styleSheet.grey190Color;
  [tableView setSeparatorInset:UIEdgeInsetsZero];
  [tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:cellIdentifier];
  [self.view addSubview:tableView];

  [self updateTableView];
}


// The real width is given by the superview after the popover has been layout.
// We take preferredContentSize as a fallback...
- (CGFloat)contentViewWidth
{
  if (tableView.superview)
  {
    return CGRectGetWidth(tableView.superview.frame);
  } else {
    return [self calculatePreferredContentSize].width;
  }
}


- (CGSize)preferredContentSize
{
  return [self calculatePreferredContentSize];
}


-(CGSize) calculatePreferredContentSize
{
  CGFloat height = 0.0;
  for (id item in self.menuItems)
    height += [self calculateRowSizeForItem:item].height;

  // Height: 20 pixels to be OK with FPPopover layout
  //         -1 pixel to hide the bottom separator of the last row
  return CGSizeMake(rowWidth, height + 20.0 - 1.0);
}


-(CGSize)calculateRowSizeForItem:(id)item
{
  CGFloat height = 0.0;
  if ([item isKindOfClass:[ButtonMenuItem class]])
  {
    height = buttonHeight;
  }
  else if ([item isKindOfClass:[ButtonMenuTextItem class]])
  {
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;

    CGSize size = [[(ButtonMenuTextItem *)item buttonText] boundingRectWithSize:CGSizeMake(rowWidth - 2.0 * marginX, CGFLOAT_MAX)
                                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                                               attributes:@{NSFontAttributeName:[UIFont dinOfSize:14.0],
                                                                            NSParagraphStyleAttributeName:paragraphStyle}
                                                                  context:nil].size;


    height = marginY + ceilf(size.height) + interitemSpacing;
    if ([(ButtonMenuTextItem*)item buttonTitle].length > 0)
      height += buttonHeight;
  }
  return CGSizeMake(rowWidth, height);
}


-(void)setMenuItems:(NSArray *)menuItems
{
  _menuItems = menuItems;
  
  self.preferredContentSize = [self calculatePreferredContentSize];

  [self updateTableView];
}

-(void) updateTableView
{
  CGSize size = [self calculatePreferredContentSize];
  [tableView setFrame:CGRectMake(0, 0, rowWidth, size.height)];

  [self.tableView reloadData];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
  return 1;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  return self.menuItems.count;
}


- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  UITableViewCell *cell = [aTableView dequeueReusableCellWithIdentifier:cellIdentifier];
  MezzanineStyleSheet *styleSheet = [MezzanineStyleSheet sharedStyleSheet];

  cell.selectionStyle = UITableViewCellSelectionStyleNone;

  for (UIView *view in [cell.contentView subviews])
    [view removeFromSuperview];

  id item = self.menuItems[indexPath.row];
  if ([item isKindOfClass:[ButtonMenuItem class]])
  {
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.tag = [self.menuItems indexOfObject:item];

    BOOL enabled = [(ButtonMenuItem *)item enabled];
    UIFont *font = [UIFont dinMediumOfSize:16.0];
    label.font = font;
    label.backgroundColor = [UIColor clearColor];
    label.text = [(ButtonMenuItem *)item buttonTitle];
    label.textAlignment = NSTextAlignmentCenter;
		UIColor *customColor = [(ButtonMenuItem *)item buttonColor];
		label.textColor = enabled ? (customColor ? customColor : styleSheet.grey90Color) : styleSheet.grey190Color;
    label.frame = CGRectMake(marginX, 0, [self contentViewWidth] - 2.0 * marginX, buttonHeight);
    
    cell.userInteractionEnabled = enabled;
    [cell.contentView addSubview:label];

    cell.accessibilityLabel = [self stringForAccesibilityLabelWithString:[(ButtonMenuItem *)item buttonTitle]];
  }
  else if ([item isKindOfClass:[ButtonMenuTextItem class]])
  {
    CGFloat itemMarginX = 20.0;
    ButtonMenuTextItem *theItem = (ButtonMenuTextItem *)item;
    BOOL enabled = (theItem.action != nil);
    
    UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    CGFloat y = marginY;

    textLabel.font = theItem.buttonFont;
    textLabel.text = theItem.buttonText;
		textLabel.textColor = theItem.buttonColor ? theItem.buttonColor : styleSheet.grey90Color;
    textLabel.textAlignment = NSTextAlignmentCenter;
    textLabel.backgroundColor = [UIColor clearColor];
    CGSize size = [textLabel sizeThatFits:CGSizeMake([self contentViewWidth] - 2.0 * itemMarginX, CGFLOAT_MAX)];
    textLabel.frame = CGRectMake(itemMarginX, y, [self contentViewWidth] - 2.0 * itemMarginX, size.height);
    [cell.contentView addSubview:textLabel];

    cell.accessibilityLabel = theItem.buttonTitle;
    cell.accessibilityIdentifier = [self stringForAccesibilityLabelWithString:[theItem buttonTitle]];

    if (theItem.buttonTitle.length > 0)
    {
      y += size.height + interitemSpacing;
      
      UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
      label.tag = [self.menuItems indexOfObject:item];
      label.font = [UIFont dinOfSize:16.0];
      label.backgroundColor = [UIColor clearColor];
      label.text = theItem.buttonTitle;
      label.textAlignment = NSTextAlignmentCenter;
      label.textColor = [styleSheet grey90Color];
      label.frame = CGRectMake(marginX, y, [self contentViewWidth] - 2.0 * marginX, buttonHeight);
      [cell.contentView addSubview:label];
    }

    cell.userInteractionEnabled = enabled;
  }
  
  cell.layoutMargins = UIEdgeInsetsZero;
  cell.preservesSuperviewLayoutMargins = NO;

  return cell;
}


#pragma mark - UITableViewDelegate

-(CGFloat)tableView:(UITableView *)aTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
  return [self calculateRowSizeForItem:self.menuItems[indexPath.row]].height;
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
  cell.backgroundColor = [UIColor clearColor];
}


- (void)tableView:(UITableView *)aTableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
  MezzanineStyleSheet *styleSheet = [MezzanineStyleSheet sharedStyleSheet];
  UITableViewCell *cell = [aTableView cellForRowAtIndexPath:indexPath];
  cell.contentView.backgroundColor = styleSheet.grey190Color;
}


- (void)tableView:(UITableView *)aTableView didUnhighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
  UITableViewCell *cell = [aTableView cellForRowAtIndexPath:indexPath];
  cell.contentView.backgroundColor = [UIColor clearColor];
}


-(void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  ButtonMenuItem *item = (self.menuItems)[indexPath.row];
  if (item.action)
    item.action();
}


#pragma mark - Accesibility

- (NSString *)stringForAccesibilityLabelWithString:(NSString *)string
{
  return [NSString stringWithFormat:@"ButtonMenuItem-%@",[string stringByRemovingSymbols]];
}

@end



@implementation ButtonMenuItem

-(id) initWithTitle:(NSString*)title action:(void (^)())action
{
  self = [super init];
  if (self)
  {
    _buttonTitle = title;
    _action = action;
    _enabled = YES;
  }
  return self;
}

+(ButtonMenuItem *) itemWithTitle:(NSString*)title action:(void (^)())action
{
  return [[ButtonMenuItem alloc] initWithTitle:title action:action];
}

@end


@implementation ButtonMenuTextItem

-(id) initWithText:(NSString *)text title:(NSString*)title action:(void (^)())action
{
  self = [super init];
  if (self)
  {
    self.buttonText = text;
    self.buttonTitle = title;
    self.action = action;
    self.buttonFont = [UIFont dinOfSize:14.0];
		self.buttonColor = nil;
  }
  return self;
}

+(ButtonMenuTextItem *) itemWithText:(NSString *)text title:(NSString*)title action:(void (^)())action
{
  return [[ButtonMenuTextItem alloc] initWithText:text title:title action:action];
}

@end


@implementation ButtonMenuHeaderTextItem

-(id) initWithText:(NSString *)text title:(NSString*)title action:(void (^)())action
{
  self = [super initWithText:text title:title action:action];
  if (self)
  {
    self.buttonFont = [UIFont dinBoldOfSize:16.0];
  }
  return self;
}

+(ButtonMenuHeaderTextItem *) itemWithText:(NSString *)text title:(NSString*)title action:(void (^)())action
{
  return [[ButtonMenuHeaderTextItem alloc] initWithText:text title:title action:action];
}

@end
