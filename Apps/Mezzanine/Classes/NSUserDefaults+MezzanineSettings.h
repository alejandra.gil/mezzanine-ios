//
//  NSUserDefaults+MezzanineSettings.h
//  Mezzanine
//
//  Created by Zai Chang on 6/17/11.
//  Copyright 2011 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSUserDefaults (MezzanineSettings)

@property (nonatomic, strong) UIColor *annotationPrimaryColor;
@property (nonatomic, strong) NSString *lastConnectedMezz;

@property (nonatomic, strong) NSString *displayName;


@property (nonatomic, assign) BOOL windshieldParallaxEnabled;
@property (nonatomic, assign) BOOL windshieldContinuousDrag;

+(void) registerMezzanineSettings;

@end
