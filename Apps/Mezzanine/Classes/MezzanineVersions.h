//
//  MezzanineVersions.h
//  Mezzanine
//
//  Created by Zai Chang on 6/14/11.
//  Copyright 2011 Oblong Industries. All rights reserved.
//

// Version key:
// these are defined using 'Other C Flags' of the target settings
// 1 - Oblong
// 2 - P&G


#if MEZZANINE_VERSION == 2

#define CONNECTION_VIEW_LOGO_IMAGE @"P&G iPad Connection.png"

#else

#define CONNECTION_VIEW_LOGO_IMAGE @"ConnectionViewLogo-iPad.png"

#endif
