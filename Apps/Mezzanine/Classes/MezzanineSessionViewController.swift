//
//  MezzanineSessionViewController.swift
//  Mezzanine
//
//  Created by miguel on 20/10/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

import UIKit

class MezzanineSessionViewController: UIViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate, OBRestrictedPageViewControllerDelegate {

  private lazy var __once: () = {
      self.joinPexipConferenceForCurrentPage()
    }()

  struct Constants {
    fileprivate static let kSwipeBarHeight: CGFloat = 50.0
  }

  var dispatchOncePexipInitialJoinToken: Int = 0

  var pexipManager: PexipManager! {
    willSet(newPexipManager) {
      if (pexipManager != nil) {
        endObservingPexipManager()
        pexipManager.leave()
      }
    }

    didSet {
      if (pexipManager != nil) {
        beginObservingPexipManager()
      }
    }
  }

  var communicator: MZCommunicator! {
    willSet {
      if (communicator != nil) {
        endObservingCommunicator()
      }
    }
    didSet {
      if (communicator != nil) {
        beginObservingCommunicator()
      }
    }
  }

  var restrictedPageViewController: OBRestrictedPageViewController
  var fullContentSessionPageViewController: SessionPageViewController
  var audioOnlySessionPageViewController: SessionPageViewController?
  var currentPage: SessionPageViewController?
  var initAudioOnlySession = false

  fileprivate var pexipManagerObservers = Array <String> ()
  fileprivate var communicatorObservers = Array <String> ()

  // TODO: `mezzanineMainViewController: UIViewController` was MezzanineMainViewController

  init(pexipManager: PexipManager, communicator: MZCommunicator, mezzanineMainViewController: UIViewController, audioOnlyViewController: AudioOnlyViewController?, initAudioOnlySession: Bool) {

    self.pexipManager = pexipManager
    self.communicator = communicator

    self.restrictedPageViewController = OBRestrictedPageViewController.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)

    self.fullContentSessionPageViewController = SessionPageViewController(controller: mezzanineMainViewController, swipeAreaHeight: Constants.kSwipeBarHeight, swipeDirection: .left)
    self.fullContentSessionPageViewController.swipeAreaTitle = "Session Swipe Bar Full Content Title".localized
    self.fullContentSessionPageViewController.swipeAreaSubtitle = "Session Swipe Bar Full Content Subtitle".localized

    if audioOnlyViewController != nil {
      self.initAudioOnlySession = initAudioOnlySession
      self.audioOnlySessionPageViewController = SessionPageViewController(controller: audioOnlyViewController!, swipeAreaHeight: Constants.kSwipeBarHeight, swipeDirection: .right)
      self.audioOnlySessionPageViewController!.swipeAreaTitle = "Session Swipe Bar Audio Only Title".localized
      self.audioOnlySessionPageViewController!.swipeAreaSubtitle = "Session Swipe Bar Audio Only Subtitle".localized
    }

    super.init(nibName: nil, bundle: nil)
    beginObservingPexipManager()
    beginObservingCommunicator()
    self.restrictedPageViewController.restrictedPageViewControllerDelegate = self
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  deinit {
    print ("MezzanineSessionViewController is being deinitialized")
  }


  override func viewDidLoad() {
    super.viewDidLoad()

    let styleSheet = MezzanineStyleSheet.shared()
    self.view.backgroundColor = styleSheet?.grey20Color

    restrictedPageViewController.view.backgroundColor = styleSheet?.grey20Color
    restrictedPageViewController.swipeAreaHeight = Constants.kSwipeBarHeight

    addChildViewController(restrictedPageViewController)
    restrictedPageViewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    restrictedPageViewController.view.frame = self.view.bounds
    self.view.addSubview(restrictedPageViewController.view)
    restrictedPageViewController.didMove(toParentViewController: self)

    let initialViewController = initAudioOnlySession ? audioOnlySessionPageViewController! : fullContentSessionPageViewController
    restrictedPageViewController.setViewControllers([initialViewController], direction: .forward, animated: false, completion: nil)
    currentPage = initialViewController

    updatePagingLayout(animated: false)
    enableSessionSwipeBar(false)
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    _ = self.__once
  }

  func shouldShowMultipleSessionPages() -> Bool {
    return communicator.networkConnectionType == .cellular && communicator.isARemoteParticipationConnection
  }

  func updatePagingLayout(animated: Bool) {
    if shouldShowMultipleSessionPages() {
      fullContentSessionPageViewController.setSwipeable(true, animated: animated)
      audioOnlySessionPageViewController?.setSwipeable(true, animated: animated)
      restrictedPageViewController.dataSource = self
      restrictedPageViewController.delegate = self
    }
    else {
      fullContentSessionPageViewController.setSwipeable(false, animated: animated)
      audioOnlySessionPageViewController?.setSwipeable(false, animated: animated)
      restrictedPageViewController.dataSource = nil
      restrictedPageViewController.delegate = nil
      currentPage = fullContentSessionPageViewController
    }
  }


  // MARK: Pexip Connection Management

  func joinPexipConferenceForCurrentPage() {
    if !communicator.isARemoteParticipationConnection
      || pexipManager.infopresence?.pexipNode == nil || pexipManager.infopresence?.pexipConference == nil {
      return
    }

    let pexipConnectionType: PexipConferenceMode = (currentPage == fullContentSessionPageViewController) ? .audioVideo : .audioOnly
    pexipManager.join(pexipConnectionType, displayName: communicator.displayName) { (result) in
      print("Connection to Pexip: \(result)")
    }
  }


  // MARK: Observation

  func beginObservingPexipManager() {
    pexipManagerObservers.append(pexipManager.addObserver(forKeyPath: "conferenceState", options: [.new, .old], on: OperationQueue.main) { ( obj, change: [AnyHashable: Any]!) in

      let newStateRawValue = Int(change[NSKeyValueChangeKey.newKey] as! NSNumber)
      let isConnected = PexipConferenceState(rawValue:newStateRawValue)! == .connected
      self.enableSessionSwipeBar(isConnected)
    })

    pexipManagerObservers.append(pexipManager.addObserver(forKeyPath: "infopresence.pexipNode", options: [.new, .old], on: OperationQueue.main) { ( obj, change: [AnyHashable: Any]!) in
      self.joinPexipConferenceForCurrentPage()
    })

    pexipManagerObservers.append(pexipManager.addObserver(forKeyPath: "infopresence.pexipConference", options: [.new, .old], on: OperationQueue.main) { ( obj, change: [AnyHashable: Any]!) in
      self.joinPexipConferenceForCurrentPage()
    })
  }

  func endObservingPexipManager() {
    for token in pexipManagerObservers {
      pexipManager.removeObserver(withBlockToken: token)
    }
    pexipManagerObservers.removeAll()
  }

  func beginObservingCommunicator() {
    communicatorObservers.append(communicator.addObserver(forKeyPath: "networkConnectionType", options: [.new, .old], on: OperationQueue.main) { ( obj, change: [AnyHashable: Any]!) in
      self.updatePagingLayout(animated: true)
      if !self.shouldShowMultipleSessionPages() {
        self.fullContentSessionPageViewController.controllerContainerView.alpha = 1.0
        self.restrictedPageViewController.setViewControllers([self.fullContentSessionPageViewController], direction: .reverse, animated: false, completion: nil)
      }
      self.joinPexipConferenceForCurrentPage()
      })
  }

  func endObservingCommunicator() {
    for token in communicatorObservers {
      communicator.removeObserver(withBlockToken: token)
    }
    communicatorObservers.removeAll()
  }


  // MARK: UIPageViewControllerDataSource

  func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
    return viewController == fullContentSessionPageViewController ? audioOnlySessionPageViewController : nil
  }

  func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
    return viewController == audioOnlySessionPageViewController ? fullContentSessionPageViewController : nil
  }


  // MARK: UIPageViewControllerDelegate

  func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
    let sessionPage = previousViewControllers[0] as! SessionPageViewController
    if completed && finished && sessionPage == currentPage {
      currentPage = sessionPage == fullContentSessionPageViewController ? audioOnlySessionPageViewController : fullContentSessionPageViewController
      joinPexipConferenceForCurrentPage()
      
      // Analytics
      let analyticsManager = MezzanineAnalyticsManager.sharedInstance
      analyticsManager.tagEvent(analyticsManager.audioOnlyEvent, attributes: [analyticsManager.stateKey : pexipManager.conferenceMode == .audioOnly ? analyticsManager.onAttribute : analyticsManager.offAttribute, analyticsManager.joinedKey : analyticsManager.yesAttribute])
    }
  }
  

  // MARK: Page Control

  func enableSessionSwipeBar(_ enabled: Bool) {
    restrictedPageViewController.swipeAreaHeight = enabled ? Constants.kSwipeBarHeight : 0
    // TODO: Set as a forced unwrapped with ! when removing the audioOnylEnabled feature toggle
    fullContentSessionPageViewController.setSwipeBarEnabled(enabled)
    audioOnlySessionPageViewController?.setSwipeBarEnabled(enabled)
  }
  
  
  // MARK: OBRestrictedPageViewController
  
  // Fade out the current one and fade in the upcoming one
  func restrictedPageViewControllerDidScroll(_ scrollView: UIScrollView) {
    let viewWidth = self.view.frame.width
    let alpha = 1 - abs(abs(scrollView.contentOffset.x) - viewWidth) / viewWidth
    
    let otherPage = (currentPage! == fullContentSessionPageViewController ? audioOnlySessionPageViewController : fullContentSessionPageViewController) as SessionPageViewController?

    currentPage!.controllerContainerView?.alpha = alpha
    otherPage?.controllerContainerView.alpha = 1 - alpha
  }
}
