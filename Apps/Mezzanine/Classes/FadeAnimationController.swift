//
//  FadeAnimationController.swift
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 18/11/2016.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

import UIKit

class FadeAnimationController: NSObject, UIViewControllerAnimatedTransitioning {
  
  let transitionDuration = 0.25
  var transitionContext: UIViewControllerContextTransitioning!
  var navigationControllerOperation: UINavigationControllerOperation!
  
  @objc convenience required init( operation: UINavigationControllerOperation) {
    self.init()
    navigationControllerOperation = operation
  }
  
  func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
    return transitionDuration;
  }
  
  func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
    self.transitionContext = transitionContext
    transitionViewControllers()
  }
  
  
  // MARK: Animations
  
  func transitionViewControllers() {
    let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!
    let fromViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!
    let containerView = transitionContext.containerView
    
    // Setup
    containerView.backgroundColor = MezzanineStyleSheet.shared().grey50Color
    toViewController.view.frame = transitionContext.finalFrame(for: toViewController)
    containerView.addSubview(toViewController.view)
    toViewController.view.alpha = 0.0
    
    // Frame calculations depending on navigationControllerOperation

    let originalFromVCFrame = fromViewController.view.frame
    let originalToVCFrame = toViewController.view.frame
    
    let xOffsetEndFrame = navigationControllerOperation == .push  ? -fromViewController.view.frame.width : fromViewController.view.frame.width
    let endFromVCFrame = originalFromVCFrame.offsetBy(dx: xOffsetEndFrame, dy: 0)
    
    let xOffsetStartFrame = navigationControllerOperation == .push  ? toViewController.view.frame.width : -toViewController.view.frame.width
    let startToVCFrame = originalToVCFrame.offsetBy(dx: xOffsetStartFrame, dy: 0)
    toViewController.view.frame = startToVCFrame
    
    UIView.animate(withDuration: transitionDuration, delay: 0, options: UIViewAnimationOptions(), animations: { () -> Void in
      fromViewController.view.alpha = 0.0
      fromViewController.view.frame = endFromVCFrame
      toViewController.view.alpha = 1.0
      toViewController.view.frame = originalToVCFrame
    }) { (Bool) -> Void in
      fromViewController.view.alpha = 1.0
      fromViewController.view.frame = originalFromVCFrame
      containerView.backgroundColor = UIColor.clear
      self.transitionContext.completeTransition(true)
    }
  }
}
