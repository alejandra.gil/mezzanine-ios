//
//  SurfaceControlBarViewController.swift
//  Mezzanine
//
//  Created by miguel on 13/3/17.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import UIKit

protocol SurfaceControlBarViewControllerDelegate: NSObjectProtocol {

  func didTapArrangeOnSurface(_ surface: MZSurface)
  func didTapClearOnSurface(_ surface: MZSurface)

}

class SurfaceControlBarViewController: UIViewController {

  @IBOutlet var arrangeButton: UIButton!
  @IBOutlet var clearButton: UIButton!

  weak var delegate: SurfaceControlBarViewControllerDelegate?

  var surface: MZSurface?

  @IBAction func arrangeTap() {
    guard let surface = surface else { return }
    delegate?.didTapArrangeOnSurface(surface)
  }

  @IBAction func clearTap() {
    guard let surface = surface else { return }
    delegate?.didTapClearOnSurface(surface)
  }

  func enableControls(_ enabled: Bool) {
    arrangeButton.isEnabled = enabled
    clearButton.isEnabled = enabled
  }
}
