//
//  NSObject+UIAlertController.m
//  Mezzanine
//
//  Created by miguel on 20/12/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

#import "NSObject+UIAlertController.h"

@interface AlertViewController : UIViewController

@end

@implementation AlertViewController

- (UIStatusBarStyle)preferredStatusBarStyle
{
  return UIStatusBarStyleLightContent;
}


- (BOOL)prefersStatusBarHidden
{
  return NO;
}


- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation
{
  return UIStatusBarAnimationFade;
}

@end


@implementation NSObject (UIAlertController)

- (void)showAlertController:(UIAlertController *)alertController animated:(BOOL)animated completion:(void (^)(void))completion
{
  UIWindow *alertWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
  alertWindow.rootViewController = [AlertViewController new];
  alertWindow.windowLevel = UIWindowLevelAlert;
  [alertWindow makeKeyAndVisible];
  [alertWindow.rootViewController presentViewController:alertController animated:animated completion:completion];
}

@end
