//
//  LiveStreamsViewController.m
//  Mezzanine
//
//  Created by Zai Chang on 1/24/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import "LiveStreamsViewController.h"
#import "LiveStreamsViewController_Private.h"
#import "LiveStreamCollectionViewCell.h"
#import <collections/src/NimbusCollections.h>
#import "MezzanineStyleSheet.h"
#import "NSObject+BlockObservation.h"
#import "AssetViewController.h"
#import "ButtonMenuViewController.h"
#import "MezzanineMainViewController.h"
#import "UIViewController+FPPopover.h"
#import "NSString+VersionChecking.h"
#import "OBDragDropManager.h"
#import "Mezzanine-Swift.h"


@implementation LiveStreamsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}


- (void)initAccessibility
{
  self.view.accessibilityIdentifier = @"LiveStreamsView";
  _collectionView.accessibilityIdentifier = @"LiveStreamsView.CollectionView";
}


- (void)viewDidLoad
{
  [super viewDidLoad];
  
  [self initAccessibility];
  
  MezzanineStyleSheet *styleSheet = [MezzanineStyleSheet sharedStyleSheet];
  
  self.view.backgroundColor = styleSheet.grey40Color;

  [styleSheet stylizeLabel:self.titleLabel withPanelTitle:NSLocalizedString(@"Live Streams Title", nil)];

  BOOL isIPad = [[UIDevice currentDevice] isIPad];
  [_hintLabel setFont:[UIFont dinOfSize:isIPad ? 14.0 : 11.0]];
  [_hintLabel setTextColor:styleSheet.textSecondaryColor];
  [_hintLabel setText:NSLocalizedString(@"Live Stream Hint Label Text", nil)];

  CGFloat leadingSpace = isIPad ? 15.0 : 11.0;
  _titleLabelLeftConstraint.constant = leadingSpace;
  _hintLabelLeftConstraint.constant = leadingSpace;

  [self updateCollectionViewLayout];
  [_collectionView registerClass:[LiveStreamCollectionViewCell class] forCellWithReuseIdentifier:@"LiveStreamCollectionViewCell"];
  _collectionView.alwaysBounceHorizontal = YES;
  _collectionView.delegate = self;
  _collectionView.scrollsToTop = NO;
  
  [self resetCollectionViewDataSource];
}


- (void)viewDidLayoutSubviews
{
  CGFloat collectionViewHeight = self.collectionView.frame.size.height;
  if (previousLayoutHeight != collectionViewHeight)
  {
    [self updateCollectionViewLayout];
    previousLayoutHeight = collectionViewHeight;
  }
}


- (void)viewWillDisappear:(BOOL)animated
{
  [super viewWillDisappear:animated];
  
  FPPopoverController *currentPopover = self.currentPopover;
  if ([currentPopover isPopoverVisible])
    [currentPopover dismissPopoverAnimated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)dealloc
{
  DLog(@"%@ dealloc", [self class]);

  self.collectionViewModel = nil;

  for (LiveStreamCollectionViewCell *cell in [self.collectionView visibleCells])
  {
    cell.liveStream = nil;
  }
}


- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
  if (self.currentPopover.popoverPresentingBlock)
    self.currentPopover.popoverPresentingBlock();
  else
    [self.currentPopover dismissPopoverAnimated:NO];
}


#pragma mark - Model

- (void)setWorkspace:(MZWorkspace *)workspace
{
  if (_workspace == workspace)
    return;
  
  if (_workspace)
  {
    for (MZLiveStream *liveStream in _workspace.liveStreams)
      [self endObservingLiveStream:liveStream];

    if (workspaceObservers)
    {
      for (id token in workspaceObservers)
        [_workspace removeObserverWithBlockToken:token];
      
      workspaceObservers = nil;
    }
  }
  
  _workspace = workspace;
  
  if (_workspace)
  {
    __weak typeof(self) __self = self;
    workspaceObservers = [[NSMutableArray alloc] init];
    id token;
    token = [_workspace addObserverForKeyPath:@"liveStreams"
                                      options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld)
                                      onQueue:[NSOperationQueue mainQueue]
                                         task:^(id obj, NSDictionary *change) {
      
      NSNumber *kind = change[NSKeyValueChangeKindKey];
//      NSIndexSet *indexSet = change[NSKeyValueChangeIndexesKey];

      if ([kind integerValue] == NSKeyValueChangeInsertion)  // Rows were added
      {
        // TODO - Make the following animate
        NSArray *inserted = change[NSKeyValueChangeNewKey];
        for (MZLiveStream *liveStream in inserted)
        {
          if ([__self isValidLiveStream:liveStream])
          {
            NIMutableCollectionViewModel *collectionViewModel = __self.collectionViewModel;
            [collectionViewModel addObject:liveStream];
          }
          [__self beginObservingLiveStream:liveStream];
        }
        
        [__self.collectionView reloadData];

        [__self updateHintLabel:[__self validStreamsFromModel]];
      }
      else if ([kind integerValue] == NSKeyValueChangeRemoval)  // Rows were removed
      {
        NSArray *deleted = change[NSKeyValueChangeOldKey];
        // TODO - For when we update nimbus
        /*
        __block NSInteger i=0;
        __block NSMutableArray *deletedIndexPaths = [NSMutableArray array];
        [indexSet enumerateIndexesUsingBlock:^(NSUInteger index, BOOL *stop) {
          MZLiveStream *liveStream = deleted[i];
          if ([__self isValidLiveStream:liveStream])
          {
            // TODO - For when we update nimbus
            NSIndexPath *indexPath = [__self.collectionViewModel indexPathForObject:liveStream];
            [__self.collectionViewModel removeObjectAtIndexPath:indexPath];
            [deletedIndexPaths addObject:indexPath];
          }
        }];

        [__self.collectionView deleteItemsAtIndexPaths:deletedIndexPaths];
         */
        for (MZLiveStream *liveStream in deleted)
          [__self endObservingLiveStream:liveStream];

        [__self resetCollectionViewDataSource];

        [__self dismissPopoverIfContextMatches:deleted];
      }
    }];
    [workspaceObservers addObject:token];

    for (MZLiveStream *liveStream in _workspace.liveStreams)
      [self beginObservingLiveStream:liveStream];
    
    [self resetCollectionViewDataSource];
  }
}


- (BOOL)isValidLiveStream:(MZLiveStream*)liveStream
{
  if (liveStream.active && liveStream.local)
    return YES;
  return NO;
}


- (NSArray*)validStreamsFromModel
{
  NSArray *validStreams = [self.workspace.liveStreams filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^(id evaluatedObject, NSDictionary *bindings) {
    return [self isValidLiveStream:evaluatedObject];
  }]];
  return validStreams;
}


- (void)beginObservingLiveStream:(MZLiveStream*)liveStream
{
  [liveStream addObserver:self forKeyPath:@"active" options:0 context:nil];
}


- (void)endObservingLiveStream:(MZLiveStream*)liveStream
{
  [liveStream removeObserver:self forKeyPath:@"active"];
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
  if ([object isKindOfClass:[MZLiveStream class]])
  {
    MZLiveStream *liveStream = (MZLiveStream*)object;
    if ([keyPath isEqual:@"active"])
    {
      // A rather brute force approach for now
      // TODO - Add correct animated addition!
      NSArray *validStreams = [self resetCollectionViewDataSource];

      if (![validStreams containsObject:liveStream])
      {
        [self dismissPopoverIfContextMatches:@[liveStream]];
      }
    }
  }
}


- (NSArray *)resetCollectionViewDataSource
{
  NSArray *validStreams = [self validStreamsFromModel];
  self.collectionViewModel = [[NIMutableCollectionViewModel alloc] initWithListArray:[validStreams mutableCopy] delegate:self];
  DLog(@"LiveStreamsViewController created NIMutableCollectionViewModel with %ld sections", (long)[self.collectionViewModel numberOfSectionsInCollectionView:self.collectionView]);
  DLog(@"LiveStreamsViewController created NIMutableCollectionViewModel with %ld items", (long)[self.collectionViewModel collectionView:self.collectionView numberOfItemsInSection:0]);

  self.collectionView.dataSource = self.collectionViewModel;
  [self.collectionView reloadData];

  [self updateHintLabel:validStreams];
  return validStreams;
}


- (void)updateHintLabel:(NSArray*)validStreams
{
  [UIView animateWithDuration:0.15 animations:^{
    _hintLabel.alpha = [validStreams count] == 0 ? 1.0 : 0.0;
  }];
}


- (UICollectionViewCell *)collectionViewModel:(NICollectionViewModel *)collectionViewModel
                        cellForCollectionView:(UICollectionView *)collectionView
                                  atIndexPath:(NSIndexPath *)indexPath
                                   withObject:(id)object
{
  if ([object isKindOfClass:[MZLiveStream class]])
  {
    MZLiveStream *liveStream = (MZLiveStream*)object;
    
    LiveStreamCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LiveStreamCollectionViewCell" forIndexPath:indexPath];
    cell.liveStream = liveStream;

    BOOL cellHasDragDropGestureRecognizer = NO;
    for (UIGestureRecognizer *recognizer in cell.gestureRecognizers)
    {
      if ([recognizer isKindOfClass:[UILongPressGestureRecognizer class]])
        cellHasDragDropGestureRecognizer = YES;
    }
    
    if (!cellHasDragDropGestureRecognizer)
    {
      OBDragDropManager *dragDropManager = [OBDragDropManager sharedManager];
      UILongPressGestureRecognizer *dragDropRecognizer = [dragDropManager createLongPressDragDropGestureRecognizerWithSource:self];
      dragDropRecognizer.delegate = self;
      dragDropRecognizer.minimumPressDuration = 0.12;
      [cell addGestureRecognizer:dragDropRecognizer];
    }
  
#if DOUBLE_TAP
    UITapGestureRecognizer *doubleTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTapGesture:)];
    doubleTapRecognizer.numberOfTouchesRequired = 2;
    [cell addGestureRecognizer:doubleTapRecognizer];
#endif
    return cell;
  }
  
  return nil;
}


#pragma mark - Collection View

- (void)updateCollectionViewLayout
{
  BOOL isIPad = [[UIDevice currentDevice] isIPad];
  CGFloat margin = isIPad ? 16.0 : 6.0;
  CGFloat sectionMargin = isIPad ? 16.0 : 12.0;
  CGFloat itemHeight = (NSInteger) (self.collectionView.frame.size.height - 2*margin);
  CGFloat itemWidth = (NSInteger) (itemHeight * 16.0 / 9.0);
  
  UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
  layout.itemSize = CGSizeMake(itemWidth, itemHeight);
  layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
  layout.sectionInset = UIEdgeInsetsMake(margin, sectionMargin, margin, sectionMargin);
  layout.minimumLineSpacing = margin;
  layout.minimumInteritemSpacing = margin;
  [self.collectionView setCollectionViewLayout:layout];
}


#pragma mark - Collection View Delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
  MZLiveStream *liveStream = [self.collectionViewModel objectAtIndexPath:indexPath];
  UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
  
  ButtonMenuViewController *menuViewController = [[ButtonMenuViewController alloc] initWithNibName:nil bundle:nil];
  
  NSMutableArray *menuItems = [NSMutableArray array];
  [menuItems addObject:[ButtonMenuHeaderTextItem itemWithText:liveStream.displayName title:nil action:nil]];
  [menuItems addObject:[ButtonMenuItem itemWithTitle:NSLocalizedString(@"Live Stream Item Menu Place", nil) action:^{
    [self placeItemOnScreen:liveStream];
    
    [self.currentPopover dismissPopoverAnimated:NO];
  }]];
  [menuItems addObject:[ButtonMenuItem itemWithTitle:NSLocalizedString(@"Live Stream Item Menu View", nil) action:^{
    [self openAssetViewerFromCell:(LiveStreamCollectionViewCell *)cell];
    
    [self.currentPopover dismissPopoverAnimated:NO];
  }]];
  menuViewController.menuItems = menuItems;
  
  // N.B. menuViewController needs its menu items before having a content height
  FPPopoverController *popover = [[FPPopoverController alloc] initWithContentViewController:menuViewController delegate:self];
  popover.context = liveStream;
  [popover stylizeAsActionSheet];
  [popover presentPopoverFromRect:[cell frame] inView:[cell superview] permittedArrowDirections:FPPopoverArrowDirectionDown animated:YES];
  self.currentPopover = popover;
}


#if DOUBLE_TAP
- (void)doubleTapGesture:(UIGestureRecognizer*)recognizer
{
  if (recognizer.state == UIGestureRecognizerStateRecognized)
  {
    if ([recognizer.view isKindOfClass:[LiveStreamCollectionViewCell class]])
    {
      LiveStreamCollectionViewCell *cell = (LiveStreamCollectionViewCell*)recognizer.view;
      [self viewStream:cell.liveStream fromCell:cell];
    }
  }
}
#endif


- (void)placeItemOnScreen:(MZLiveStream*)liveStream
{
  MZCommunicator *communicator = [MezzanineAppContext currentContext].currentCommunicator;
  [communicator.requestor requestWindshieldItemCreateRequest:_workspace.uid contentSource:liveStream.uid surfaceName:@"main" feldRelativeCoords:[FeldRelativeCoords new] feldRelativeSize:[FeldRelativeSize new]];
}


- (void)openAssetViewerFromCell:(LiveStreamCollectionViewCell *)cell;
{
  if (!cell)
    return;
  
  AssetViewController *controller = [[AssetViewController alloc] initWithNibName:nil bundle:nil];
  controller.appContext = [MezzanineAppContext currentContext];
  controller.workspace = self.workspace;
  controller.placeholderImage = cell.liveStream.thumbnail;
  controller.item = cell.liveStream;
  
  [[MezzanineAppContext currentContext].mainNavController pushViewController:controller transitionView:cell];
}


#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
  if ([gestureRecognizer isKindOfClass:[OBLongPressDragDropGestureRecognizer class]] && _mezzanineViewController.currentDragDropRecognizer != nil)
    return NO;
  return YES;
}


#pragma mark - OBOvumSource

- (OBOvum *)createOvumFromView:(UIView*)sourceView
{
  if ([sourceView isKindOfClass:[LiveStreamCollectionViewCell class]])
  {
    LiveStreamCollectionViewCell *cell = (LiveStreamCollectionViewCell *)sourceView;
    OBOvum *ovum = [[OBOvum alloc] init];
    ovum.dataObject = cell.liveStream;
    return ovum;
  }

  return nil;
}


- (UIView *)createDragRepresentationOfSourceView:(UIView *)sourceView inWindow:(UIWindow*)window
{
  if ([sourceView isKindOfClass:[LiveStreamCollectionViewCell class]])
  {
    LiveStreamCollectionViewCell *cell = (LiveStreamCollectionViewCell *)sourceView;
    
    CGRect frameInWindow = [cell convertRect:cell.bounds toView:cell.window];
    frameInWindow = [window convertRect:frameInWindow fromWindow:cell.window];
    
    LiveStreamCollectionViewCell *dragCell = [[LiveStreamCollectionViewCell alloc] initWithFrame:frameInWindow];
    dragCell.liveStream = cell.liveStream;
    dragCell.imageView.image = [[(LiveStreamCollectionViewCell *)sourceView imageView] image];
    return dragCell;
  }
  else
    return nil;
}


- (void)dragViewWillAppear:(UIView *)dragView inWindow:(UIWindow*)window atLocation:(CGPoint)location
{
  dragView.alpha = 1.0;
  
  [UIView animateWithDuration:0.25 animations:^{
    dragView.alpha = 0.75;
  }];
  
  BOOL isIPhone = ![[UIDevice currentDevice] isIPad];
  POPSpringAnimation *animation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
  animation.toValue = [NSValue valueWithCGSize:CGSizeMake(1.25, 1.25)];
  animation.velocity = isIPhone ? [NSValue valueWithCGSize:CGSizeMake(0.12, 0.12)] : [NSValue valueWithCGSize:CGSizeMake(0.06, 0.06)];
  animation.springBounciness = 16;
  animation.springSpeed = isIPhone ? 12 : 16;
  [dragView pop_addAnimation:animation forKey:@"scale"];
  
  animation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewCenter];
  animation.toValue = [NSValue valueWithCGPoint:location];
  animation.velocity = [NSValue valueWithCGSize:CGSizeMake(0.0, 0.0)];
  animation.springBounciness = 6;
  animation.springSpeed = 15;
  [dragView pop_addAnimation:animation forKey:@"center"];
}


- (void)ovumDragWillBegin:(OBOvum*)ovum
{
  MZCommunicator *communicator = [MezzanineAppContext currentContext].currentCommunicator;
  if ([ovum.dataObject isKindOfClass:[MZLiveStream class]])
  {
    MZLiveStream *liveStream = (MZLiveStream*)ovum.dataObject;
    [communicator.requestor requestLiveStreamGrabRequest:self.workspace.uid uid:liveStream.uid];
  }
}


- (void)ovumDragEnded:(OBOvum*)ovum
{
  MZCommunicator *communicator = [MezzanineAppContext currentContext].currentCommunicator;
  if ([ovum.dataObject isKindOfClass:[MZLiveStream class]])
  {
    MZLiveStream *liveStream = (MZLiveStream*)ovum.dataObject;
    [communicator.requestor requestLiveStreamRelinquishRequest:self.workspace.uid uid:liveStream.uid];
  }

  [ovum.dragView pop_removeAllAnimations];
}


@end
