//
//  MZAnnotation+Interactions.h
//  Mezzanine
//
//  Created by Zai Chang on 10/26/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import "MZAnnotation.h"


@interface MZAnnotation (Interactions)

-(void) beginCreationModeInView:(UIView*)view
                     completion:(void (^)())completion
                      cancelled:(void (^)())cancelled
                   gestureBegan:(void (^)(UIGestureRecognizer *))gestureBegan
                   gestureEnded:(void (^)(UIGestureRecognizer *))gestureEnded;
-(void) endCreationMode;

@end
