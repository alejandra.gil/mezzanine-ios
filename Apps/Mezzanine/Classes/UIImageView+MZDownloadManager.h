//
//  UIImageView+MZDownloadManager.h
//  Mezzanine
//
//  Created by miguel on 09/12/13.
//  Copyright (c) 2013 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MZDownloadManager.h"

@interface UIImageView (MZDownloadManager)

-(void) setImageWithURL:(NSURL *)url andPlaceholderImage:(UIImage *)placeholderImage;
-(void) stopLoadingImage;

@end
