//
//  SlideLayer.m
//  Mezzanine
//
//  Created by Zai Chang on 10/22/10.
//  Copyright 2010 Oblong Industries. All rights reserved.
//

#import "SlideLayer.h"
#import "NSObject+BlockObservation.h"
#import "MezzanineStyleSheet.h"
#import "MezzanineWindow.h"
#import "MZDownloadManager.h"
#import "NSString+VersionChecking.h"
#import "Mezzanine-Swift.h"

static BOOL kActionPanelDesaturatesContent = NO;

@implementation SlideLayer
{
  CATextLayer *slideIndexLayer;
  NSMutableArray *_liveStreamObservers;
  CGFloat _borderWidth;
}

@synthesize slide;

@synthesize slideImageNeedsRefresh;
@synthesize slideImage;
@synthesize isDisplayingFullImage;
@synthesize isLoadingFullImage;

@synthesize contentScale;
@synthesize actionsPanelVisible;
@synthesize actionsPanelLayer;

@synthesize userInteractionEnabled;

@synthesize actionPanelWillAppear;
@synthesize actionPanelWillDisappear;
@synthesize deleteRequested;

@synthesize placeholderLayer;


- (void)layoutForTeamwork {
  _borderWidth = 0.0;
  
  self.borderColor = [UIColor clearColor].CGColor;
  [contentShadowLayer removeFromSuperlayer];
}


-(id) init
{
  if (self = [super init])
  {
    _borderWidth = 1.0;
    self.borderColor = [MezzanineStyleSheet sharedStyleSheet].slideBorderColor.CGColor;
    self.edgeAntialiasingMask = kCALayerLeftEdge | kCALayerRightEdge | kCALayerBottomEdge | kCALayerTopEdge;
    
    self.userInteractionEnabled = YES;
    self.contentScale = 1.0;
    self.slideImageNeedsRefresh = YES;
    
    // For retina displays support
    CGFloat screenContentScale = [[UIScreen mainScreen] scale];
    
    backgroundLayer = [CAGradientLayer layer];
    backgroundLayer.backgroundColor = [MezzanineStyleSheet sharedStyleSheet].slideBackgroundColor.CGColor;
    backgroundLayer.frame = self.bounds;
    // Disable actions such that sublayer layouts happen immediately
    backgroundLayer.actions = @{@"frame": [NSNull null]};
    [self addSublayer:backgroundLayer];

    contentLayer = [CALayer layer];
    contentLayer.contentsGravity = kCAGravityResizeAspect;
    [self addSublayer:contentLayer];
    
    if (kActionPanelDesaturatesContent)
    {
      contentFilter = [CIFilter filterWithName:@"CIColorControls"];
      [contentFilter setDefaults];
      contentLayer.filters = @[contentFilter];
    }
    
    // Actions panel is a strip intended as a context menu for the slide
    // Users can activate or deactivate it by swiping up or down on the slide
    actionsPanelLayer = [CALayer layer];
    
    contentShadowLayer = [CAGradientLayer layer];
    contentShadowLayer.colors = @[(id)[UIColor colorWithWhite:0.0 alpha:0.28].CGColor,
                                (id)[UIColor colorWithWhite:0.0 alpha:0.0].CGColor];
    contentShadowLayer.locations = @[@0.0,
                                   @1.0];
    
    [self setActionsPanelVisible:NO animated:NO];
    [self insertSublayer:actionsPanelLayer above:backgroundLayer];
    [self insertSublayer:contentShadowLayer above:actionsPanelLayer];
    
    deleteButtonLayer = [CALayer layer];
    deleteButtonLayer.borderWidth = 1.0;
    deleteButtonLayer.cornerRadius = 5.0;
    [actionsPanelLayer addSublayer:deleteButtonLayer];
    
    CGFontRef font = CGFontCreateWithFontName((CFStringRef)@"DINOT-Medium");
    deleteButtonTextLayer = [CATextLayer layer];
    deleteButtonTextLayer.string = NSLocalizedString(@"Generic Button Title Delete", nil);
    deleteButtonTextLayer.font = font;
    deleteButtonTextLayer.alignmentMode = kCAAlignmentCenter;
    deleteButtonTextLayer.contentsScale = screenContentScale;
    [deleteButtonLayer addSublayer:deleteButtonTextLayer];
    CGFontRelease(font);

    // White on red
    deleteButtonLayer.borderColor = [UIColor redColor].CGColor;
    deleteButtonLayer.backgroundColor = [UIColor colorWithRed:0.24 green:0.0 blue:0.0 alpha:1.0].CGColor;
    deleteButtonTextLayer.foregroundColor = [UIColor colorWithWhite:1.0 alpha:1.0].CGColor;
    // Red on white
//    deleteButtonLayer.borderColor = [UIColor whiteColor].CGColor;
//    deleteButtonLayer.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.24].CGColor;
//    deleteButtonTextLayer.foregroundColor = [UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:1.0].CGColor;

    font = CGFontCreateWithFontName((CFStringRef)@"DINOT");
    slideIndexLayer = [CATextLayer layer];
    slideIndexLayer.font = font;
    [slideIndexLayer setAlignmentMode:kCAAlignmentCenter];
    slideIndexLayer.contentsScale = screenContentScale;
    [slideIndexLayer setForegroundColor:[UIColor colorWithWhite:1.0 alpha:0.66].CGColor];
    [slideIndexLayer setRasterizationScale:[[UIScreen mainScreen] scale]];
    [slideIndexLayer setShouldRasterize:YES];
    [self addSublayer:slideIndexLayer];

    CGFontRelease(font);
    
    if ([SettingsFeatureToggles sharedInstance].teamworkUI) {
      [self layoutForTeamwork];
    }
  }
  
  return self;
}

-(void) dealloc
{
  self.slide = nil;
  self.liveStream = nil;
  slideImage = nil;
}


#pragma mark - Layout

-(void) layoutTextLayer
{
  if (!centeredTextLayer)
    return;
  
  // Relative font sizing
  BOOL isIPad = [[UIDevice currentDevice] isIPad];
  CGFloat fontSize = isIPad ? 32.0 : 16.0;
  CGFloat maxFontSize = fontSize * 3.0;
  //CGFloat scaledFontSize = fontSize / contentScale;
  CGFloat scaledFontSize = fontSize / 0.5;
  
  centeredTextLayer.fontSize = (NSInteger)scaledFontSize;

  CGRect newFrame = CGRectMake(24.0,
                               CGRectGetMidY(self.bounds) - centeredTextLayer.fontSize / 2.0,
                               self.bounds.size.width - 2 * 24.0,
                               maxFontSize);
  centeredTextLayer.frame = newFrame;
}


-(CGFloat) actionPanelHeight
{
  CGFloat panelHeight = MIN(self.bounds.size.height, 64.0 / contentScale);
  return panelHeight;
}


-(void) layoutContentLayer
{
  CGFloat panelHeight = [self actionPanelHeight];  
  CGFloat translationY = - actionsPanelAnimationState * panelHeight;
  CGRect frame = self.bounds;
  frame.origin.y += translationY;
  contentLayer.frame = frame;
  backgroundLayer.frame = frame;
  
  placeholderLayer.frame = contentLayer.bounds;
  //[placeholderLayer setNeedsDisplay]; // Causes serious slowdown when animating
  
  CGFloat shadowLayerHeight = 8.0 / contentScale;
  contentShadowLayer.frame = CGRectMake(0, CGRectGetMaxY(frame), CGRectGetWidth(frame), shadowLayerHeight);
}


-(void) layoutActionsPanelLayer
{
  CGFloat panelWidth = self.bounds.size.width;
  CGFloat panelHeight = [self actionPanelHeight];
  
  CGFloat y = self.bounds.size.height - panelHeight;  
  actionsPanelLayer.frame = CGRectMake(0.0, y, panelWidth, panelHeight);
  
  [self layoutDeleteButtonLayer];
  actionsPanelNeedsLayout = NO;
}


-(void) layoutDeleteButtonLayer
{
  CGSize actionsPanelSize = actionsPanelLayer.frame.size;
  CGFloat idealButtonWidth = 160.0 / contentScale;
  CGFloat maxButtonWidth = actionsPanelSize.width * 0.9;
  CGFloat maxButtonHeight = MIN(44, actionsPanelSize.height - 12.0) / contentScale;
  
  CGFloat buttonWidth = MIN(idealButtonWidth, maxButtonWidth);
  CGFloat buttonHeight = MIN(actionsPanelSize.height - 12.0, maxButtonHeight);
  CGRect frame = CGRectMake((actionsPanelSize.width - buttonWidth) / 2.0,
                            (actionsPanelSize.height - buttonHeight) / 2.0,
                            buttonWidth,
                            buttonHeight);
  deleteButtonLayer.frame = frame;
  deleteButtonLayer.borderWidth = 1.0 / contentScale;
  deleteButtonLayer.cornerRadius = 5.0 / contentScale;
  
  static CGFloat kMarginX = 8.0;
  deleteButtonTextLayer.fontSize = 16.0 / contentScale;
  CGRect textFrame = CGRectMake(kMarginX,
                                0.8 * (frame.size.height - deleteButtonTextLayer.fontSize) / 2.0,
                                frame.size.width - 2 * kMarginX,
                                deleteButtonTextLayer.fontSize * 1.2);
  deleteButtonTextLayer.frame = textFrame;
}


-(void) layoutSlideIndexLayer
{
  if (!slideIndexLayer)
    return;

  // Relative font sizing
  BOOL isIPad = [[UIDevice currentDevice] isIPad];

  CGFloat fontSize = isIPad ? 18.0 : 12.0;
  CGFloat maxFontSize = fontSize * 3.0;
  CGFloat scaledFontSize = fontSize / _containerZoomScale;

  slideIndexLayer.fontSize = (NSInteger)scaledFontSize;

  CGFloat scale = _containerZoomScale == 1.0 ? 1.0 : _containerZoomScale * 2.0;
  [slideIndexLayer setRasterizationScale:[[UIScreen mainScreen] scale] * scale];

  CGFloat topMargin = isIPad ? 10.0 : 15.0;
  CGRect frame = CGRectMake(0, CGRectGetMaxY(self.bounds) + topMargin, CGRectGetWidth(self.bounds), maxFontSize + topMargin);
  slideIndexLayer.frame = frame;
}


-(void) layoutSublayers
{
  [super layoutSublayers];
  
  [self layoutContentLayer];
  [self layoutTextLayer];
  if (actionsPanelVisible)
    [self layoutActionsPanelLayer];
  else
    actionsPanelNeedsLayout = YES;
  [self layoutSlideIndexLayer];
}


#pragma mark - Slide

-(void) setVideoPlaceholderVisible:(BOOL)visible
{
  if (visible)
  {
    if (placeholderLayer == nil)  // Lazy loading...
    {
      placeholderLayer = [VideoPlaceholderLayer layer];
      placeholderLayer.backgroundColor = [MezzanineStyleSheet sharedStyleSheet].slideBackgroundColor.CGColor;
      placeholderLayer.contentsScale = UIScreen.mainScreen.scale;
      placeholderLayer.frame = contentLayer.bounds;
      if (_liveStream)
      {
        placeholderLayer.sourceName = _liveStream.displayName;
        placeholderLayer.sourceOrigin = _liveStream.remoteMezzName ? [NSString stringWithFormat:NSLocalizedString(@"Video Placeholder Origin Text", nil), _liveStream.remoteMezzName] : nil;
      }
      else
      {
        placeholderLayer.sourceName = [slide valueForKeyPath:@"displayInfo.display-name"];
        placeholderLayer.sourceOrigin = [slide valueForKeyPath:@"displayInfo.mezz-info.name"];
      }
      [contentLayer addSublayer:placeholderLayer];
    }
    placeholderLayer.hidden = NO;
  }
  else
  {
    placeholderLayer.hidden = YES;
  }
}


-(void) updateWithSlide:(MZItem *)aSlide
{
  centeredTextLayer.string = [NSString stringWithFormat:@"%lu", (long)(aSlide.index+1)];
  slideIndexLayer.string = [NSString stringWithFormat:@"%lu", (long)(aSlide.index+1)];

//  if (aSlide.image)
//    [self setSlideImage:aSlide.image];
}


-(void) setSlide:(MZItem *)aSlide
{
  if (slide != aSlide)
  {
    if (slideObservers)
    {
      for (id token in slideObservers)
        [slide removeObserverWithBlockToken:token];
      
      slideObservers = nil;
    }
    
    slide = aSlide;
    
    [self updateWithSlide:slide];
    
    if (slide)
    {
      slideObservers = [[NSMutableArray alloc] init];
      __weak SlideLayer *__self = self;
      id token;
      token = [slide addObserverForKeyPath:@"isVideo" task:^(id obj, NSDictionary *change) {
        [__self setVideoPlaceholderVisible:[obj isVideo]];
      }];
      [slideObservers addObject:token];
      
      token = [slide addObserverForKeyPath:@"displayInfo.display-name" task:^(id obj, NSDictionary *change) {
        [__self placeholderLayer ].sourceName = [[__self slide] valueForKeyPath:@"displayInfo.display-name"];
      }];
      [slideObservers addObject:token];
      
      token = [slide addObserverForKeyPath:@"displayInfo.mezz-info" task:^(id obj, NSDictionary *change) {
        [__self placeholderLayer].sourceOrigin = [[__self slide] valueForKeyPath:@"displayInfo.mezz-info.name"];
      }];
      [slideObservers addObject:token];
    }
  }
}


-(void) setSlideImage:(UIImage*)image
{
  if (slideImage != image)
  {
    slideImage = image;
        
    if (slideImage)
    {
      centeredTextLayer.hidden = YES;
      
      if (isDisplayingFullImage)
      {
        slideImageNeedsRefresh = YES;
      }
      else
      {
        contentLayer.contents = (id) slideImage.CGImage;
        slideImageNeedsRefresh = NO;
      }
      
      if (slide.isVideo)
        [self setVideoPlaceholderVisible:NO];
    }
    else
    {
      slideImageNeedsRefresh = YES;
      centeredTextLayer.hidden = NO;
      contentLayer.contents = nil;
      
      if (slide.isVideo)
        [self setVideoPlaceholderVisible:YES];
    }
  }
  else if (!image)
  {
    slideImageNeedsRefresh = YES;
    centeredTextLayer.hidden = NO;
    contentLayer.contents = nil;
    
    if (slide.isVideo)
      [self setVideoPlaceholderVisible:YES];
  }
}


-(void) setFullSlideImage:(UIImage*)image
{
  if (image)
  {
    contentLayer.contents = (id) image.CGImage;
    isDisplayingFullImage = YES;
    isLoadingFullImage = NO;
  }
  else
  {
    // Unload full slide iamge
    contentLayer.contents = (id) slideImage.CGImage;
    isDisplayingFullImage = NO;
    isLoadingFullImage = NO;
  }
}


-(void) setContentScale:(CGFloat)newContentScale
{
  contentScale = newContentScale;
  
  CGFloat borderWidth = _borderWidth / contentScale;
  self.borderWidth = borderWidth;
  
  CGFloat adjustedContentsScale = [[UIScreen mainScreen] scale] * contentScale;
  self.contentsScale = adjustedContentsScale;
  deleteButtonTextLayer.contentsScale = adjustedContentsScale;
  placeholderLayer.contentsScale = adjustedContentsScale;
  
  actionsPanelNeedsLayout = YES;
}

// Override such that hitting any sublayer would return the slide
-(CALayer*) hitTest:(CGPoint)p
{
  CALayer *layer = [super hitTest:p];
  if (layer)
    return self;
  return nil;
}


-(BOOL) actionStripContainsPoint:(CGPoint)location
{
  if (actionsPanelVisible)
    return [actionsPanelLayer hitTest:location] != nil;
  
  return NO;
}


-(void) setContainerZoomScale:(CGFloat)containerZoomScale
{
  _containerZoomScale = containerZoomScale;
  self.placeholderLayer.containerZoomScale = _containerZoomScale;
  [self layoutSlideIndexLayer];
}


-(void) setIndexLayerVisible:(BOOL)indexLayerVisible
{
  slideIndexLayer.hidden = !indexLayerVisible;
  _indexLayerVisible = indexLayerVisible;
}


#pragma mark - Live Stream support

- (void)setLiveStream:(MZLiveStream *)liveStream
{
  if (_liveStream)
  {
    for (id observer in _liveStreamObservers)
      [_liveStream removeObserverWithBlockToken:observer];
    _liveStreamObservers = nil;

    [[MZDownloadManager sharedLoader] cancelLoadingURLForTarget:self];
  }
  
  _liveStream = liveStream;

  if (_liveStream)
  {
    __weak typeof(self) __self = self;
    _liveStreamObservers = [NSMutableArray new];
    id observer = [_liveStream addObserverForKeyPath:@"active" options:NSKeyValueObservingOptionInitial onQueue:[NSOperationQueue mainQueue] task:^(id object, NSDictionary *change) {
      [__self updateSlideImageWithUrl];
    }];
    [_liveStreamObservers addObject:observer];

    observer = [_liveStream addObserverForKeyPath:@"fullURL" options:(NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew)  onQueue:[NSOperationQueue mainQueue] task:^(id object, NSDictionary *change) {

      if (![change[NSKeyValueChangeOldKey] isEqual:[NSNull null]])
      {
        NSString *oldURL = change[NSKeyValueChangeOldKey];
        [__self unloadOldThumbnailURL:oldURL];
      }

      [__self updateSlideImageWithUrl];
    }];
    [_liveStreamObservers addObject:observer];

    if ([slide isVideo] && placeholderLayer)
    {
      placeholderLayer.sourceName = _liveStream.displayName;
      placeholderLayer.sourceOrigin = _liveStream.remoteMezzName ? [NSString stringWithFormat:NSLocalizedString(@"Video Placeholder Origin Text", nil), _liveStream.remoteMezzName] : nil;
    }
  }
}


- (void) unloadOldThumbnailURL:(NSString *)oldURL
{
  NSURL *url = [[MezzanineAppContext currentContext].currentCommunicator urlForResource:oldURL];
  if (url == nil)
    return;

  [[MZDownloadManager sharedLoader] unloadImageFromMemoryCacheWithURL:url];
  [[MZDownloadManager sharedLoader] cancelLoadingURLForTarget:self];
}


- (void)updateSlideImageWithUrl
{
  // Show placeholder (by hiding slide image) if:
  //   live stream isn't active (DVI not connected)
  //   live stream is not local (remote streams
  //   live stream does not have thumb or full URL specified
  if (!_liveStream.active || (_liveStream.thumbURL == nil))
  {
    self.slideImage = nil;
    return;
  }

  NSURL *fullUrl = [[MezzanineAppContext currentContext].currentCommunicator urlForResource:_liveStream.fullURL];
  if (fullUrl == nil)
    return;

  if (!self.slideImage && _liveStream.thumbnail)
    self.slideImage = _liveStream.thumbnail;

  [[MZDownloadManager sharedLoader] loadImageWithURL:fullUrl
                                            atTarget:self
                                             success:^(UIImage *image) {
                                               self.slideImage = image;
                                             }
                                               error:^(NSError *error) {
                                                 NSURL *thumbUrl = [[MezzanineAppContext currentContext].currentCommunicator urlForResource:_liveStream.thumbURL];
                                                 if (thumbUrl == nil)
                                                   return;

                                                 [[MZDownloadManager sharedLoader] loadImageWithURL:thumbUrl
                                                                                           atTarget:self
                                                                                            success:^(UIImage *image) {self.slideImage = image;}
                                                                                              error:nil
                                                                                           canceled:nil
                                                                                      cachingOption:MZDownloadCacheTypeOnlyInMemory];
                                               }
                                            canceled:nil
                                       cachingOption:MZDownloadCacheTypeOnlyInMemory];

}


#pragma mark - Actions Panel

-(void) setActionsPanelAnimationState:(CGFloat)state animated:(BOOL)animated
{
  if (!animated)
  {
    [CATransaction begin];
    [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
  }
  
  actionsPanelAnimationState = MIN(MAX(state, 0.0), 1.0);
  actionsPanelLayer.hidden = (actionsPanelAnimationState == 0.0);
  [self layoutContentLayer];
  if (actionsPanelNeedsLayout)
    [self layoutActionsPanelLayer];
  
  if (kActionPanelDesaturatesContent)
  {
    NSNumber *saturation = [NSNumber numberWithFloat:1.0 - state];
    [contentLayer setValue:saturation forKey:@"filters.CIColorControls.inputSaturation"];
    [contentLayer setNeedsDisplay];
  }
  
  if (!animated)
  {
    [CATransaction commit];
  }
}

-(void) setActionsPanelVisible:(BOOL)visible animated:(BOOL)animated
{
  actionsPanelVisible = visible;
  
  if (visible && actionPanelWillAppear)
    actionPanelWillAppear(self);
  else if (!visible && actionPanelWillDisappear)
    actionPanelWillDisappear(self);
  
  CGFloat animationState = actionsPanelVisible ? 1.0 : 0.0;
  [self setActionsPanelAnimationState:animationState animated:animated];
}


#pragma mark - Actions

-(void) deleteSlide:(id)sender
{
  if (deleteRequested)
    deleteRequested(self);
}


#pragma mark - Delete Pending

-(void) setDeletePendingState:(BOOL)deletePending animated:(BOOL)animated
{
  if (!animated)
  {
    [CATransaction begin];
    [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
  }
  
  [self setActionsPanelVisible:NO animated:NO]; // animated is always NO, since we're already wrapping this call with the CATransaction
  
  if (deletePending)
  {
    self.opacity = 0.33;
    self.borderColor = [UIColor redColor].CGColor;
  }
  else
  {
    self.opacity = 1.0;
    self.borderColor = [MezzanineStyleSheet sharedStyleSheet].slideBorderColor.CGColor;
  }
  
  if (!animated)
  {
    [CATransaction commit];
  }
}

@end
