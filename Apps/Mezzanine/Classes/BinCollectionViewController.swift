//
//  BinCollectionViewController.swift
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 10/02/2017.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import UIKit

let liveStreamCellIdentifier = "liveStreamCellIdentifier"
let itemCellIdentifier = "itemCellIdentifier"
let placeholderCellIdentifier = "placeholderCellIdentifier"

var collectionViewGeneralPadding: CGFloat = 12.0

class PlaceholderCollectionViewCell: UICollectionViewCell {
  
  var imageView: UIImageView!
  var interactor: PortfolioActionsInteractor?

  override init(frame: CGRect) {
    super.init(frame: frame)
    
    imageView = UIImageView(frame: CGRect.zero)
    imageView.image = UIImage(named: "add-icon")
    imageView.contentMode = .center
    
    contentView.backgroundColor = MezzanineStyleSheet.shared().grey50Color
    contentView.addSubview(imageView)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func draw(_ rect: CGRect) {
    imageView.frame = rect
  }

}

enum ScrollDirection {
  case left
  case right
  case none
}

protocol BincollectionViewControllerDelegate: NSObjectProtocol {
  func binCollectionViewCellDidUpdateOrigin(_ collectionView: UICollectionView, scrollDirection: ScrollDirection)
  func binCollectionViewDidUpdateTransferStatusLabel(_ visible: Bool, message: String, collectionView: UICollectionView, scrollDirection: ScrollDirection)
}

class BinCollectionViewController: UIViewController {
  
  weak var delegate: BincollectionViewControllerDelegate?
  
  @IBOutlet var headerContainerView: UIView!
  @IBOutlet var collectionView: UICollectionView!
  
  var interactor: BinCollectionViewInteractor!
  let headerViewController = BinHeaderViewController()
  var currentPopover: FPPopoverController?
  var currentAlertController: UIAlertController?

  // CollectionView
  fileprivate var collectionViewDataSource: BinCollectionViewDataSource!
  fileprivate var lastContentOffset: CGFloat! = -1.0
  fileprivate var nextTraitCollection: UITraitCollection!

  // Current slide frame
  var currentSlideFrameView: UIView!
  var currentSlideViewIndex: Int = -1
  var currentSlideViewActive: Bool = false
  
  // Marker & edge scrolling
  fileprivate var insertionMarker: UIView!
  fileprivate var edgeScrollingTimer: Timer!
  fileprivate var edgeScrollingSpeed: CGFloat!
  
  convenience init(dataSource: BinCollectionViewDataSource) {
    self.init()
    collectionViewDataSource = dataSource
  }

  deinit {
    print ("BinCollectionViewController deinit")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.view.backgroundColor = MezzanineStyleSheet.shared().grey30Color
    
    initializeCollectionView()
    initializeHeaderView()
    initializeCurrentSlideFrameView()
    initializeInsertionMarker()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    updateCurrentSlideFrameView(interactor.currentPresentationIndex(), animated: false, active: interactor.presentationIsActive())
  }
  
  
  // MARK: Init
  
  func initializeCollectionView() {
    collectionView.delegate = self
    collectionView.dataSource = collectionViewDataSource
    collectionView.register(LiveStreamCollectionViewCell.self, forCellWithReuseIdentifier: liveStreamCellIdentifier)
    collectionView.register(PortfolioCollectionViewCell.self, forCellWithReuseIdentifier: itemCellIdentifier)
    collectionView.register(PlaceholderCollectionViewCell.self, forCellWithReuseIdentifier: placeholderCellIdentifier)
    
    collectionView.dropZoneHandler = self
  }
  
  func initializeHeaderView() {
    headerViewController.delegate = self
    headerViewController.collectionViewHasLiveStreams = interactor.hasLiveStreamsSection()
    
    self.delegate = headerViewController
    addChildViewController(headerViewController)
    let view = headerViewController.view
    view?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    view?.frame = headerContainerView.bounds
    headerContainerView.addSubview(view!)
    headerViewController.didMove(toParentViewController: self)
  }
  
  func initializeCurrentSlideFrameView() {
    currentSlideFrameView = UIView(frame: CGRect.zero)
    currentSlideFrameView.alpha = 0.0
    currentSlideFrameView.layer.borderColor = UIColor.white.cgColor
    currentSlideFrameView.layer.borderWidth = traitCollection.isRegularWidth() ? 4.0 : 2.0
    currentSlideFrameView.layer.cornerRadius = 3.0
    currentSlideFrameView.isUserInteractionEnabled = false
    
    collectionView.addSubview(currentSlideFrameView)
  }
  
  func initializeInsertionMarker() {
    let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
    insertionMarker = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 4.0, height: layout.itemSize.height))
    insertionMarker.backgroundColor = MezzanineStyleSheet.shared().superHighlightColor
    insertionMarker.layer.cornerRadius = 2.0
    insertionMarker.alpha = 0.0
    
    collectionView.addSubview(insertionMarker)
  }
  
  
  // MARK: Adaptivity
  
  func updateCollectionViewLayout() {
    let collectionViewLayout = BinCollectionViewFlowLayout()

    var itemHeight: CGFloat = 0.0
    var itemWidth: CGFloat = 0.0
    var sectionInsets = UIEdgeInsets.zero
    
    if view.traitCollection.isiPhone() {
      collectionViewLayout.minimumLineSpacing = collectionViewGeneralPadding
      collectionViewLayout.minimumInteritemSpacing = collectionViewGeneralPadding
    } else {
      collectionViewLayout.minimumLineSpacing = 2 * collectionViewGeneralPadding
      collectionViewLayout.minimumInteritemSpacing = 2 * collectionViewGeneralPadding
    }

    sectionInsets = UIEdgeInsetsMake(0, 2 * collectionViewGeneralPadding, 0, 2 * collectionViewGeneralPadding)
    itemHeight = self.collectionView.frame.height * 0.9
    itemWidth = itemHeight * 16.0 / 9.0

    collectionViewLayout.itemSize = CGSize(width: itemWidth, height: itemHeight)
    collectionViewLayout.scrollDirection = .horizontal
    collectionViewLayout.sectionInset = sectionInsets
    collectionView.collectionViewLayout = collectionViewLayout
    reloadCollectionView()
  }
  
  override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
    if nextTraitCollection != nil {
      return
    }
    
    updateCollectionViewLayout()
    delegate?.binCollectionViewCellDidUpdateOrigin(collectionView, scrollDirection: .none)
  }
  
  override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
    nextTraitCollection = newCollection
  }
  
  override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
    nextTraitCollection = nil
    
    updateCollectionViewLayout()
    delegate?.binCollectionViewCellDidUpdateOrigin(collectionView, scrollDirection: .none)
  }
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    
    self.updateCollectionViewLayout()
    self.updateCurrentSlideFrameView(self.interactor.currentPresentationIndex(), animated: false, active: self.interactor.presentationIsActive())
    self.delegate?.binCollectionViewCellDidUpdateOrigin(self.collectionView, scrollDirection: .none)
  }
  
  
  // MARK: Current Slide
  
  func updateCurrentSlideFrameView(_ index: Int, animated: Bool, active: Bool) {
    
    // Bug 11970. To avoid setting the current slide view before inserting the items in the collectionView
    currentSlideViewIndex = index
    currentSlideViewActive = active
    
    let sectionIndex = interactor.hasLiveStreamsSection() ? 1 : 0
    if (currentSlideFrameView == nil || index > collectionView.numberOfItems(inSection: sectionIndex)) && active {
      return
    }
    
    let indexPath = IndexPath(item: index, section: sectionIndex)
    let layout = collectionView.collectionViewLayout
    if layout.layoutAttributesForItem(at: indexPath)!.frame == CGRect.zero {
      return
    }
    
    let attributes = layout.layoutAttributesForItem(at: indexPath)
    let targetFrame = attributes!.frame.insetBy(dx: -2, dy: -2)
    
    let animationBlock = {
      self.currentSlideFrameView.frame = targetFrame
      self.currentSlideFrameView.alpha = active == true ? 1.0 : 0.0
    }
    
    if (animated) {
      UIView.animate(withDuration: 0.3, animations: animationBlock)
    }
    else {
      animationBlock()
    }
  }
  
  
  // MARK: Insertion marker
  
  func showInsertionMarker(_ index: Int, offset: CGFloat) {
    let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
    let itemsWidth = collectionViewItemSizeWithMargin() * max(CGFloat(index), 0)
    let amountOfSectionSpaces = collectionView.numberOfSections == 1 ? 1 : 3
    let additionalMargin: CGFloat = traitCollection.isiPhoneCompactWidth() ? CGFloat(amountOfSectionSpaces) * collectionViewGeneralPadding : 0
    let x = offset + itemsWidth + (layout.minimumLineSpacing / 2) - (insertionMarker.frame.width / 2) + additionalMargin
   
    insertionMarker.frame.origin = CGPoint(x: x, y: collectionViewGeneralPadding)
    insertionMarker.frame.size.height = layout.itemSize.height
    self.insertionMarker.alpha = 1.0
  }
  
  func hideInsertionIndicator() {
    self.insertionMarker.alpha = 0.0
  }
  
  func insertionIndexForLocation(_ location: CGPoint, item: MZItem) -> Int {
    let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout

    var insertionIndex: Int = NSNotFound
    let offset = location.x - liveStreamDragOffset()
    if offset < 0 {
      return Int(insertionIndex)
    }
    
    insertionIndex = Int(round((offset - layout.minimumLineSpacing / 2.0) / collectionViewItemSizeWithMargin()))
    if interactor.numberOfPresentationSlides() != NSNotFound {
      insertionIndex = Int(min(CGFloat(interactor.numberOfPresentationSlides()), CGFloat(insertionIndex - 1)))
    }
    
    return Int(insertionIndex)
  }
  
  
  // MARK: CollectionView updates
  
  func updateCollectionViewItems(_ updates: Array<[String: AnyObject]>) {
    if collectionView != nil {
      // Animation is needed to animate the headers moving
      // i.e. if a livestream is inserted the other section is pushed forward
      // so we need to adjust and animate headers accordingly
      
      // Inserting items will scroll the collection view to the new last item
      var isItemsInsertion = false
      UIView.animate(withDuration: 0.3, animations: {
        self.collectionView.performBatchUpdates({
          for cellInfo in updates {
            let kind = NSKeyValueChange(rawValue: cellInfo["kind"] as! UInt)!
            let indexPath = cellInfo["indexPath"] as! IndexPath
            switch kind {
            case .insertion:
              isItemsInsertion = (self.interactor.hasLiveStreamsSection() && indexPath.section == 1) || (!self.interactor.hasLiveStreamsSection() && indexPath.section == 0) ? true : false
              
              self.collectionView.insertItems(at: [indexPath])
            case .removal:
              self.collectionView.deleteItems(at: [indexPath])
              
            case .replacement:
              self.collectionView.reloadItems(at: [indexPath])
              
            default: break
            }
          }
          }, completion: nil)
        
        self.collectionView.collectionViewLayout.invalidateLayout()
        self.delegate?.binCollectionViewCellDidUpdateOrigin(self.collectionView, scrollDirection: .none)
      }, completion: { (finished) in
        let offset = self.collectionView.contentSize.width - self.collectionView.frame.width
        if isItemsInsertion && offset > 0 {
          self.collectionView.setContentOffset(CGPoint(x: offset, y: 0), animated: true)
        }
      }) 
    }
  }
  
  
  func updateCollectionViewSections(_ updates: Array<[String: AnyObject]>) {
    if collectionView != nil {
      for collectionViewInfo in updates {
        let kind = NSKeyValueChange(rawValue: collectionViewInfo["kind"] as! UInt)!
        let indexPath = collectionViewInfo["indexPath"] as! IndexPath
        switch kind {
        case .insertion:
          self.collectionView.insertSections(IndexSet(integer: indexPath.row))
          
        case .removal:
          self.collectionView.deleteSections(IndexSet(integer: indexPath.row))
          
        default: break
        }
      }
      
      headerViewController.collectionViewHasLiveStreams = interactor.hasLiveStreamsSection()
      self.collectionView.collectionViewLayout.invalidateLayout()
      self.delegate?.binCollectionViewCellDidUpdateOrigin(self.collectionView, scrollDirection: .none)
    }
  }
  
  func reloadCollectionView() {
    if collectionView != nil {
      collectionView.reloadData()
    }
  }
  
  
  // MARK: Upload and download status
  
  func updateTransferStatusLabel(_ visible: Bool, message: String) {
    self.delegate?.binCollectionViewDidUpdateTransferStatusLabel(visible, message: message, collectionView: collectionView, scrollDirection: .none)
  }
  
  
  // MARK: Edge scrolling
  
  func beginEdgeScrolling() {
    if edgeScrollingTimer == nil {
      edgeScrollingSpeed = 0.0
      edgeScrollingTimer = Timer.scheduledTimer(timeInterval: 0.03, target: self, selector: #selector(updateEdgeScrollingOffset), userInfo: nil, repeats: true)
    }
  }
  
  func endEdgeScrolling() {
    if edgeScrollingTimer != nil {
      edgeScrollingTimer.invalidate()
      edgeScrollingTimer = nil
    }
  }
  
  @objc func updateEdgeScrollingOffset(_ timer: Timer) {
    let maxOffsetX = collectionView.contentSize.width - collectionView.bounds.width - collectionView.contentInset.right
    
    var offsetX = collectionView.contentOffset.x + edgeScrollingSpeed
    offsetX = min(maxOffsetX, offsetX)
    offsetX = max(0, offsetX)
    
    collectionView.contentOffset.x = offsetX
  }
  
  
  func updateEdgeScrollingStatus(_ location: CGPoint) {
    let scrollTrigger: CGFloat = self.view.traitCollection.isRegularWidth() ? 120.0 : 64.0
    let maxScrollSpeed: CGFloat = self.view.traitCollection.isRegularWidth() ? 24.0 : 12.0
    
    var loc = location
    loc.x -= collectionView.contentOffset.x;
    if loc.x < scrollTrigger {
      edgeScrollingSpeed = -(scrollTrigger - loc.x) / scrollTrigger * maxScrollSpeed
      beginEdgeScrolling()
    } else if loc.x > collectionView.frame.width - scrollTrigger {
      edgeScrollingSpeed = (loc.x - (collectionView.frame.width - scrollTrigger)) / scrollTrigger * maxScrollSpeed
      beginEdgeScrolling()
    } else {
      edgeScrollingSpeed = 0.0
      endEdgeScrolling()
    }
  }
  
  
  // MARK: Private helpers
  
  fileprivate func liveStreamDragOffset() -> CGFloat {
    var offset: CGFloat = 0.0
    let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout

    if interactor.hasLiveStreamsSection() {
      offset = collectionViewItemSizeWithMargin() * CGFloat(collectionView.numberOfItems(inSection: 0)) + layout.minimumLineSpacing
    }
    
    return offset
  }
  
  fileprivate func collectionViewItemSizeWithMargin() -> CGFloat {
    let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
    return layout.itemSize.width + layout.minimumLineSpacing
  }
}


// MARK: UIScrollViewDelegate

extension BinCollectionViewController : UIScrollViewDelegate {
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    var direction: ScrollDirection = .none
    if lastContentOffset > scrollView.contentOffset.x {
      direction = .right;
    } else if lastContentOffset < scrollView.contentOffset.x {
      direction = .left;
    }
    
    lastContentOffset = scrollView.contentOffset.x;
    delegate?.binCollectionViewCellDidUpdateOrigin(collectionView, scrollDirection: direction)
  }
}


// MARK: UIGestureRecognizerDelegate

extension BinCollectionViewController : UIGestureRecognizerDelegate {
  
  func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
    if gestureRecognizer is OBLongPressDragDropGestureRecognizer || gestureRecognizer is OBDirectionalPanGestureRecognizer {
      return true
    }
    return false
  }
}


// MARK: UICollectionViewDelegate

extension BinCollectionViewController : UICollectionViewDelegate {
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    if collectionView.cellForItem(at: indexPath) is PlaceholderCollectionViewCell {
      let cell = collectionView.cellForItem(at: indexPath)!
      
      let addMenuInteractor = PortfolioAddMenuInteractor(systemModel: interactor.systemModel, communicator: interactor.communicator)
      addMenuInteractor.presenter = self

      let addMenuViewController = PortfolioAddMenuViewController(interactor: addMenuInteractor)
      addMenuViewController?.fromView = cell.contentView

      let popover = FPPopoverController(contentViewController: addMenuViewController, delegate: nil)
      popover?.stylizeAsActionSheet()
      
      let popoverPresentingBlock = { popover?.presentPopover(from: cell.contentView.frame, in: cell.contentView.superview, permittedArrowDirections: FPPopoverArrowDirectionDown, animated: true) }
      
      if self.traitCollection.isRegularWidth() {
        popover?.popoverPresentingBlock = popoverPresentingBlock as? () -> Void
      }
      
      popoverPresentingBlock()
      
      addMenuViewController?.popover = popover
      currentPopover = popover
    } else {
      
      var menuItems = Array<AnyObject>()
      let cell = collectionView.cellForItem(at: indexPath)!
      
      if indexPath.section == 0 && interactor.hasLiveStreamsSection() {
        let item: MZLiveStream = interactor.getLiveStreamItem(indexPath.row)
        
        // 1. LiveStream header title
        if item.displayName.characters.count > 0 {
          menuItems.append(addLiveStreamHeaderTitle(item.displayName))
        }
        
        // 2. Place item on screen
        menuItems.append(addPlaceItemOnScreen(item))
        
        // 3. Local preview
        menuItems.append(addLocalPreview(cell, item: item))
        
        // Show Popover
        addItemsAndShowPopover(menuItems, item: item, cell: cell)
      } else {
        let item: MZPortfolioItem = interactor.getPresentationItem(indexPath.row)
        
        // 1. LiveStream header title
        if cell is LiveStreamCollectionViewCell {
          if var displayName = interactor.getLiveStreamFromContentSource(item.contentSource)!.displayName {
            if displayName.characters.count > 0 {
              menuItems.append(addLiveStreamHeaderTitle(displayName))
            }
          }
        }
        
        // 2. Place item on screen
        menuItems.append(addPlaceItemOnScreen(item))
        
        // 3. Jump to slide
        let presentItemString = interactor.presentationIsActive() ? "Portfolio Item Jump To Slide".localized : "Portfolio Item Present From Slide".localized
        menuItems.append(addPresentOrJumpToSlide(presentItemString, index: indexPath.row))
        
        // 4. Local preview
        menuItems.append(addLocalPreview(cell, item: item))
        
        // 5. Delete slide
        menuItems.append(addDeleteItem(item))
        
        // Show Popover
        addItemsAndShowPopover(menuItems, item: item, cell: cell)
      }
    }
  }
  
  func addLiveStreamHeaderTitle(_ title: String) -> ButtonMenuTextItem {
    return ButtonMenuTextItem.init(text: title, title: nil, action: nil)
  }
  
  func addPlaceItemOnScreen(_ item: MZItem) -> ButtonMenuItem {
    return ButtonMenuItem(title: "Place on Screen".localized, action: {
      self.interactor.placeItemOnScreen(item)
      self.currentPopover?.dismissPopover(animated: false)
    })
  }
  
  func addPresentOrJumpToSlide(_ title: String, index: Int) -> ButtonMenuItem {
    return ButtonMenuItem(title: title, action: {
      // Analytics
      let analyticsManager = MezzanineAnalyticsManager.sharedInstance
      if self.interactor.presentationIsActive() == false {
        analyticsManager.tagEvent(analyticsManager.startPresentationEvent,
          attributes: [analyticsManager.sourceKey : analyticsManager.portfolioItemAttribute] )
      } else {
        analyticsManager.tagEvent(analyticsManager.scrollPresentationEvent,
          attributes: [analyticsManager.sourceKey : analyticsManager.jumpToSlideAttribute] )
      }
      
      self.interactor.presentOrJumpToItemAtIndex(index)
      self.currentPopover?.dismissPopover(animated: false)
    })
  }
  
  func addLocalPreview(_ cell: UICollectionViewCell, item: MZItem) -> ButtonMenuItem {
    return ButtonMenuItem.init(title: "Local Preview".localized, action: {
      let controller = AssetViewController()
      controller.appContext = MezzanineAppContext.current()
      controller.workspace = self.interactor.currentWorkspace()
      if cell is PortfolioCollectionViewCell {
        controller.item = item
        controller.placeholderImage = (cell as? PortfolioCollectionViewCell)!.imageView.image
      } else {
        controller.item = (cell as? LiveStreamCollectionViewCell)!.liveStream
        controller.placeholderImage = (cell as? LiveStreamCollectionViewCell)!.imageView.image
      }
      
      let navigationController = self.navigationController as? MezzanineNavigationController
      navigationController?.pushViewController(controller, transitionView: cell)
      self.currentPopover?.dismissPopover(animated: false)
    })
  }
  
  func addDeleteItem(_ item: MZItem) -> ButtonMenuItem {
    let deleteItem: ButtonMenuItem = ButtonMenuItem(title: "Portfolio Item Menu Delete".localized, action: {
      self.interactor.deleteItem(item)
      self.currentPopover?.dismissPopover(animated: false)
    })
    
    deleteItem.buttonColor = MezzanineStyleSheet.shared().redHighlightColor
    
    return deleteItem
  }
  
  func addItemsAndShowPopover(_ menuItems: Array<AnyObject>, item: MZItem, cell: UICollectionViewCell) -> FPPopoverController {
    let menuViewController = ButtonMenuViewController()
    menuViewController.menuItems = menuItems
    
    let popoverController = FPPopoverController(contentViewController: menuViewController, delegate: nil)
    popoverController?.context = item
    popoverController?.stylizeAsActionSheet()
    popoverController?.presentPopover(from: cell.frame, in: cell.superview, permittedArrowDirections: FPPopoverArrowDirectionDown, animated: true)
    currentPopover = popoverController
    
    return popoverController!
  }
}


// MARK: BinHeaderViewControllerDelegate

extension BinCollectionViewController : BinHeaderViewControllerDelegate {
  
  // Better set contentOffset rather than scrollToItemAtIndexPath
  // because we have insets and scroll to the edge of the cell
  func binHeaderViewDidTapLivestreams() {
    if collectionView != nil {
      collectionView.setContentOffset(CGPoint.zero, animated: true)
    }
  }
  
  func binHeaderViewDidTapItems() {
    if collectionView != nil {
      let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
      
      let sectionIndex = interactor.hasLiveStreamsSection() ? 1 : 0
      let indexPath = IndexPath(item: 0 , section: sectionIndex)
      let attributes = collectionView.layoutAttributesForItem(at: indexPath)
      let xOffset = (attributes?.frame.origin.x)! - layout.minimumLineSpacing + 1 // 1 is the separator width to match both separators
      
      // It may happen that we scroll out of the content size... In that case we scroll to the first item
      if abs((attributes?.frame)!.maxX - collectionView.contentSize.width) - 1 < layout.minimumLineSpacing {
        collectionView.scrollToItem(at: indexPath, at: .left, animated: true)
      } else {
        collectionView.setContentOffset(CGPoint(x: xOffset, y: 0), animated: true)
      }
    }
  }
}


// MARK: OBOvumSource

extension BinCollectionViewController: OBOvumSource {
  
  func createOvum(from sourceView: UIView!) -> OBOvum! {
    if sourceView is UICollectionViewCell == false {
      return nil
    }
    
    let indexPath = collectionView.indexPath(for: sourceView as! UICollectionViewCell)
    var slide = MZItem()
    if interactor.hasLiveStreamsSection() {
      if indexPath?.section == 0 {
        slide = interactor.getLiveStreamItem(indexPath!.row)
      } else if indexPath?.section == 1 {
        slide = interactor.getPresentationItem(indexPath!.row)
      }
    } else {
      slide = interactor.getPresentationItem(indexPath!.row)
    }
    
    let ovum = OBOvum()
    ovum.dataObject = slide
    return ovum
  }
  
  func createDragRepresentation(ofSourceView sourceView: UIView!, in window: UIWindow!) -> UIView! {
    if sourceView is PortfolioCollectionViewCell {
      let cell = sourceView as! PortfolioCollectionViewCell
      let cellFrame = cell.convert(cell.bounds, to: cell.window)
      let draggableCell = PortfolioCollectionViewCell(frame: cellFrame)
      draggableCell.slide = cell.slide
      draggableCell.imageView.image = cell.imageView.image
      
      return draggableCell
      
    } else if sourceView is LiveStreamCollectionViewCell {
      let cell = sourceView as! LiveStreamCollectionViewCell
      let cellFrame = cell.convert(cell.bounds, to: cell.window)
      let draggableCell = LiveStreamCollectionViewCell(frame: cellFrame)
      draggableCell.liveStream = cell.liveStream
      draggableCell.imageView.image = cell.imageView.image
      
      return draggableCell
    }
    
    return nil
  }
  
  func dragViewWillAppear(_ dragView: UIView!, in window: UIWindow!, atLocation location: CGPoint) {
    UIView.animate(withDuration: 0.15, animations: {
      dragView.alpha = 0.6
      dragView.center = location
      dragView.transform = CGAffineTransform.identity.scaledBy(x: 1.05, y: 1.05)
    }, completion: { (finished) in
      UIView.animate(withDuration: 0.15, delay: 0.0, options: .beginFromCurrentState, animations: {
        dragView.transform = CGAffineTransform.identity.scaledBy(x: 1.0, y: 1.0)
        
        }, completion: nil)
    }) 
  }
  
  func ovumDragWillBegin(_ ovum: OBOvum!) {
    if ovum.dataObject is MZItem {
      interactor.grabItem((ovum.dataObject as! MZItem))
    }
  }
  
  func ovumDragEnded(_ ovum: OBOvum!) {
    if ovum.dataObject is MZItem {
      interactor.relinquishItem((ovum.dataObject as! MZItem))
    }
    
    if ovum.dragView != nil {
      ovum.dragView.layer.removeAllAnimations()
    }
  }
}


// MARK: OBDropZone

extension BinCollectionViewController: OBDropZone {
  
  func ovumDropped(_ ovum: OBOvum!, in view: UIView!, atLocation location: CGPoint) {
    let insertionIndex = insertionIndexForLocation(location, item: (ovum.dataObject as! MZItem))
    
    if insertionIndex != NSNotFound {
      let dataObject = ovum.dataObject
      if dataObject is MZLiveStream {
        let item = dataObject as! MZLiveStream
        interactor.insertItem(item, index: max(insertionIndex + 1, 0))
      } else if dataObject is MZPortfolioItem {
        let item = dataObject as! MZPortfolioItem
        if insertionIndex < Int(item.index) - 1 {
          interactor.reorderItem(item, index: max(insertionIndex + 1, 0))
        } else if insertionIndex > Int(item.index) {
          interactor.reorderItem(item, index: max(insertionIndex, 0))
        }
      }
      
      endEdgeScrolling()
      hideInsertionIndicator()
    }
  }
  
  func ovumEntered(_ ovum: OBOvum!, in view: UIView!, atLocation location: CGPoint) -> OBDropAction {
    let dataObject = ovum.dataObject
    if dataObject is MZLiveStream {
      return .copy
    } else if dataObject is MZPortfolioItem {
      return .move
    }
    
    return .none
  }
  
  func ovumExited(_ ovum: OBOvum!, in view: UIView!, atLocation location: CGPoint) {
    endEdgeScrolling()
    hideInsertionIndicator()
  }
  
  func ovumMoved(_ ovum: OBOvum!, in view: UIView!, atLocation location: CGPoint) -> OBDropAction {
    let dataObject = ovum.dataObject
    let insertionIndex = insertionIndexForLocation(location, item: (ovum.dataObject as! MZItem))
    
    if dataObject is MZPortfolioItem || dataObject is MZLiveStream {
      updateEdgeScrollingStatus(location)
    }
    
    if dataObject is MZPortfolioItem {
      let item = dataObject as! MZPortfolioItem
      if insertionIndex == NSNotFound {
        hideInsertionIndicator()
      } else if insertionIndex < Int(item.index) - 1 {
        showInsertionMarker(insertionIndex + 1, offset: liveStreamDragOffset())
      } else if insertionIndex > Int(item.index) {
        showInsertionMarker(insertionIndex + 1, offset: liveStreamDragOffset())
      } else {
        hideInsertionIndicator()
      }
    } else if dataObject is MZLiveStream {
      // Livestreams has no index but there's only two cases to check
      // - 1 is the left side of the first item
      // 0 is the right side of the first item
      if insertionIndex == NSNotFound {
        hideInsertionIndicator()
      } else if insertionIndex == -1 {
        showInsertionMarker(insertionIndex, offset: liveStreamDragOffset())
      } else if insertionIndex >= 0 {
        showInsertionMarker(insertionIndex + 1, offset: liveStreamDragOffset())
      } else {
        hideInsertionIndicator()
      }
    }
    
    return ovum.dropAction
  }
}

// Presenter methods

extension BinCollectionViewController {
  
  func dimissImagePicker() {
    
    if (presentedViewController != nil) {
      dismiss(animated: true, completion: nil)
    }
  }
  
  func showUploadLimitReachedAlert(_ numberOfItems: Int, requestTransaction: MZFileBatchUploadRequestTransaction, uids: Array<String>, continueBlock: @escaping MZFileBatchUploadRequestContinuationBlock) {
    
    let alertController = UIAlertController(title: "Upload Continuation Dialog Title".localized, message: String(format: "Upload Continuation Dialog Message".localized, uids.count, numberOfItems), preferredStyle:.alert)
    
    let cancelAction = UIAlertAction(title: "Upload Continuation Dialog Cancel Button".localized, style: .cancel) { (_) in
      continueBlock(requestTransaction, uids, false)
    }
    
    let confirmAction = UIAlertAction(title: "Upload Continuation Dialog Confirm Button".localized, style: .default) { (_) in
      continueBlock(requestTransaction, uids, true)
    }
    
    alertController.addAction(cancelAction)
    alertController.addAction(confirmAction)
    
    self.present(alertController, animated: true, completion: nil)
    
    self.currentAlertController = alertController
  }
  
  func showUploadErrordAlert(_ error: NSError) {
    
    let alertController = UIAlertController(title: "Upload FileTypeNotSupported Dialog Title".localized, message: error.localizedDescription, preferredStyle:.alert)
    
    let okAction = UIAlertAction(title: "Generic Button Title Okay".localized, style: .default, handler: nil)
    
    alertController.addAction(okAction)
    
    self.present(alertController, animated: true, completion: nil)
    
    self.currentAlertController = alertController
  }
  
  func showPartialUploadAlertAndContinue(_ errorMessage: String, continueClosure: @escaping () -> ()) {
    
    let alertController = UIAlertController(title: "Image Upload Error Dialog Title".localized, message: errorMessage, preferredStyle:.alert)
    
    let okAction = UIAlertAction(title: "Generic Button Title Okay".localized, style: .default) { (_) in
      continueClosure()
    }
    
    alertController.addAction(okAction)
    
    self.present(alertController, animated: true, completion: nil)
    
    self.currentAlertController = alertController
  }
}
