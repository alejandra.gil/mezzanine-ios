//
//  WorkspaceMenuViewController.m
//  Mezzanine
//
//  Created by Zai Chang on 8/27/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import "WorkspaceMenuViewController.h"
#import "MezzanineMainViewController.h"
#import "WorkspaceToolbarViewController_Private.h"
#import "UIViewController+FPPopover.h"
#import "NSObject+BlockObservation.h"


@interface WorkspaceMenuViewController ()
{
  NSMutableArray *_workspaceObservers;
}

@end


@implementation WorkspaceMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self)
  {
  }
  return self;
}


- (void)dealloc
{
  self.workspace = nil;
}


- (void)setWorkspace:(MZWorkspace *)workspace
{
  if (_workspace != workspace)
  {
    if (_workspace)
    {
      for (id observer in _workspaceObservers)
        [_workspace removeObserverWithBlockToken:observer];
      _workspaceObservers = nil;
    }
    
    _workspace = workspace;
    
    if (_workspace)
    {
      _workspaceObservers = [NSMutableArray array];
      __weak typeof(self) __self = self;
      id observer = [_workspace addObserverForKeyPath:@"hasContent"
                                               options:0
                                               onQueue:[NSOperationQueue mainQueue]
                                                  task:^(id obj, NSDictionary *change) {
                                                    [__self reloadItems];
                                                  }];
      [_workspaceObservers addObject:observer];
      
      [self reloadItems];
    }
  }
}


- (void)reloadItems
{
  BOOL isRPConnection = [self.workspaceToolbarViewController.appContext.currentCommunicator isARemoteParticipationConnection];
  
  __weak typeof(self) __self = self;
  __weak WorkspaceToolbarViewController *__workspaceToolbarViewController = __self.workspaceToolbarViewController;

  NSMutableArray *items = [[NSMutableArray alloc] init];
  if (_workspace.isSaved)
  {
    BOOL isPublic = !__self.workspace.owner || [__self.workspace.owner isEqualToString:@""];
    BOOL isUserSignedIn = _systemModel.currentUsername != nil;

    if (isPublic || (isUserSignedIn && [__self.workspace.owner isEqualToString:_systemModel.currentUsername]))
    {
      [items addObject:[ButtonMenuItem itemWithTitle:NSLocalizedString(@"Workspace Rename Button Title", nil) action:^{
        [__workspaceToolbarViewController.currentPopover dismissPopoverAnimated:NO];
        [__self.workspaceToolbarViewController workspaceRename];
      }]];
    }
    [items addObject:[ButtonMenuItem itemWithTitle:NSLocalizedString(@"Workspace Close Button Title", nil) action:^{
      [__workspaceToolbarViewController.currentPopover dismissPopoverAnimated:NO];
      [__self.workspaceToolbarViewController workspaceClose];
    }]];
    
    if (!isRPConnection) {
      [items addObject:[ButtonMenuItem itemWithTitle:NSLocalizedString(@"Workspace Open Button Title", nil) action:^{
        [__workspaceToolbarViewController.currentPopover dismissPopoverAnimated:NO];
        [__self.workspaceToolbarViewController workspaceOpen];
      }]];
    }
  }
  else
  {
    [items addObject:[ButtonMenuItem itemWithTitle:NSLocalizedString(@"Workspace Save Button Title", nil) action:^{
      [__workspaceToolbarViewController.currentPopover dismissPopoverAnimated:NO];
      [__self.workspaceToolbarViewController workspaceSave];
    }]];
    
    ButtonMenuItem *discardItem = [ButtonMenuItem itemWithTitle:NSLocalizedString(@"Workspace Discard Button Title", nil) action:^{
      [__workspaceToolbarViewController.currentPopover dismissPopoverAnimated:NO];
      [__self.workspaceToolbarViewController workspaceDiscard];
    }];
    discardItem.enabled = _workspace.hasContent;
    [items addObject:discardItem];
    
    if (!isRPConnection) {
      [items addObject:[ButtonMenuItem itemWithTitle:NSLocalizedString(@"Workspace Open Button Title", nil) action:^{
        [__workspaceToolbarViewController.currentPopover dismissPopoverAnimated:NO];
        [__self.workspaceToolbarViewController workspaceOpen];
      }]];
    }
  }
  
  self.menuItems = items;
}

@end
