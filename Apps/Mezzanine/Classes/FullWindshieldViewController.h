//
//  FullWindshieldViewController.h
//  Mezzanine
//
//  Created by miguel on 6/3/17.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

#import "WindshieldViewController.h"

@protocol FullWindshieldViewControllerDelegate <NSObject>

- (CGFloat)windshieldContainerZoomScale;
- (UIScrollView *)windshieldContainerScrollView;

@end

@class WorkspaceGeometry;

@interface FullWindshieldViewController : WindshieldViewController

@property (nonatomic, weak) id<FullWindshieldViewControllerDelegate> fullWindshieldDelegate;
@property (nonatomic, strong) WorkspaceGeometry *workspaceGeometry;

@property (nonatomic, strong) NSArray<UIGestureRecognizer *> *tapGesturesThatShouldBeRequiredToFail;
@property (nonatomic, strong) UIPanGestureRecognizer *panGestureThatShouldBeRequiredToFailByItemPanningGestures;

@end
