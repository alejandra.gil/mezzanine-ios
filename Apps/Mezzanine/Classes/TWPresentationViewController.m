//
//  TWPresentationViewController.m
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 16/03/2017.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

#import "TWPresentationViewController.h"
#import "PresentationViewController_Private.h"
#import "Mezzanine-Swift.h"


@interface TWPresentationViewController () <PresentationSliderViewControllerDelegate>


@property (nonatomic, assign) CGRect surfaceRect;
@property (nonatomic, assign) BOOL isLoading;

@end

@implementation TWPresentationViewController


- (void)viewDidLoad
{
  [super viewDidLoad];
  
  self.view.backgroundColor = [UIColor clearColor];
}


- (void)setSystemModel:(MZSystemModel *)systemModel
{
  [super setSystemModel:systemModel];
  
  self.workspace = systemModel.currentWorkspace;
}


- (void)setPlanarWorkspaceViewController:(PlanarWorkspaceViewController *)aWorkspaceViewController
{
  _planarWorkspaceViewController = aWorkspaceViewController;
  self.contentScrollView = aWorkspaceViewController.containerScrollView;
  self.deckSliderView = nil;
}


- (void)updateLayout
{
  _surfaceRect = [_workspaceGeometry roundedFrameForSurface:self.systemModel.surfaces.firstObject];
  self.view.frame = _surfaceRect;
  
  [UIView performWithoutAnimation:^{
    if ([[self.slidesContainerLayer sublayers] count] > 0)
    {
      [self layoutSlideLayersAnimated:NO];
      [self revealCurrentSlide];
    }
    else
    {
      dispatch_async(dispatch_get_main_queue(), ^{
        [self loadAllSlides];
        
        // Needed to layout correctly the first time, once everything is loaded
        // 0.33 is a contant in parent class...
        self.slidesContentLayerScale = self.presentation.active ? 1.0 : 0.33;
        [self updateSlideContainerTransformAnimated:NO];
      });
    }
  }];
}


-(CGFloat)borderWidth
{
  return 1.0 / [UIScreen mainScreen].scale / self.contentScrollView.zoomScale;  // Exactly one pixel at every zoom scale
}


#pragma mark - Adaptivity

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
  [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
  
  [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
    [self updateLayout];
  } completion:nil];
}


#pragma mark - Overridden

- (void)loadAllSlides
{
  _isLoading = YES;
  [super loadAllSlides];
  _isLoading = NO;
  
  // Once is loaded, we can animate properly the slide status
  for (SlideLayer *slideLayer in self.slideLayers)
  {
    [self updateSlideLayerVisibility:slideLayer animated:NO];
  }
}


- (CGSize)getFeldSize
{
  MZSurface *mainSurface = [self.systemModel surfaceWithName:@"main"];
  if (!mainSurface)
    return CGSizeZero;

  MZFeld *aFeld = mainSurface.felds.firstObject;
  CGRect rect = [_workspaceGeometry roundedFrameForFeld:aFeld];

  return rect.size;
}

- (NSInteger)numberOfFelds
{
  MZSurface *mainSurface = [self.systemModel surfaceWithName:@"main"];
  if (!mainSurface)
    return 0;

  return mainSurface.felds.count;
}


- (CGRect)frameForSlideAtIndex:(NSInteger)index
{
  NSInteger numberOfFelds = [self numberOfFelds];
  CGFloat spaceBetweenFelds = [self spaceBetweenFelds];
  CGSize feldSize = [self getFeldSize];

  CGFloat firstSlideOffset = numberOfFelds == 1 ? 0 : feldSize.width;
  CGFloat x = firstSlideOffset + (feldSize.width + spaceBetweenFelds) * index;
  CGRect frame = CGRectMake(x, 0, feldSize.width, feldSize.height);
  return frame;
}


#pragma mark - Slides udpates

- (void)updateSlideLayerVisibility:(SlideLayer*)slideLayer animated:(BOOL)animated
{
  if (animated && !_isLoading)
  {
    slideLayer.hidden = NO;
    
    // TODO: Move it deck slider implementation
    [slideLayer setIndexLayerVisible:false];
    
    // Play with opacity to animate it, instead of hiding the layer...
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    animation.duration = 0.25;
    animation.fromValue = [NSNumber numberWithFloat:self.presentation.active && slideLayer.hidden ? 0.0 : 1.0];
    animation.toValue = [NSNumber numberWithFloat:self.presentation.active ? 1.0 : 0.0];
    animation.removedOnCompletion = NO;
    animation.fillMode = kCAFillModeBoth;
    animation.additive = NO;
    [slideLayer addAnimation:animation forKey:self.presentation.active ? @"opacityIn" : @"opacityOut"];
  }
  else
  {
    slideLayer.hidden = !self.presentation.active;
  }
}


- (CGFloat)slideContainerOffsetForSlideIndex:(NSInteger)slideIndex
{
  CGSize feldSize = [self getFeldSize];
  CGFloat spaceBetweenFelds = [self spaceBetweenFelds];

  return spaceBetweenFelds - ((feldSize.width + spaceBetweenFelds) * slideIndex);
}


- (CGFloat)spaceBetweenFelds
{
  MZSurface *mainSurface = [self.systemModel surfaceWithName:@"main"];
  if (!mainSurface)
    return 0;

  return [_workspaceGeometry feldMarginInSurface:mainSurface];
}


#pragma mark - UIPanGestureRecognizer

- (void)handleDeckPanGesture:(UIPanGestureRecognizer *)recognizer
{
  // Bug 18546, 18578. Pan gesture should only possible in the area of the presentation view (from top to the bottom screen)
  CGPoint pointInSuperview =  [self.view convertPoint:[recognizer locationInView:self.view] toView:self.view.superview];
 
  CGRect rect = CGRectMake(MAX(0, CGRectGetMinX(_planarWorkspaceViewController.view.frame)),
                           CGRectGetMinY(_planarWorkspaceViewController.view.frame),
                           CGRectGetMaxX(self.view.frame),
                           CGRectGetMaxY(_planarWorkspaceViewController.view.frame));
  
  // By disabling the recognizer causes to call the end of gesture and snap the presentation accordingly
  if (!CGRectContainsPoint(rect, pointInSuperview)) {
    recognizer.enabled = NO;
  }
  
  if (self.presentation.active)
    [super handleDeckPanGesture:recognizer];
}


#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRequireFailureOfGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
  if (gestureRecognizer == presentationTapGesture &&
      [_tapGesturesThatShouldBeRequiredToFail containsObject:otherGestureRecognizer])
    return YES;

  return NO;
}


#pragma mark - PresentationSliderViewControllerDelegate

- (void)presentationSliderMovedToIndex:(NSInteger)index
{
  [self goToSlideAtIndex:index];
}

@end
