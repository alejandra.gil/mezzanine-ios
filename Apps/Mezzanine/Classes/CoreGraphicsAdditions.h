//
//  CoreGraphicsAdditions.h
//  Mezzanine
//
//  Created by Zai Chang on 1/16/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//


CGFloat CGPointDistance(CGPoint pointA, CGPoint pointB);
