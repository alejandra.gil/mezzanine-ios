//
//  MezzanineBaseRoomViewController.m
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 09/02/2017.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

#import "MezzanineBaseRoomViewController.h"

@interface MezzanineBaseRoomViewController ()

@end

@implementation MezzanineBaseRoomViewController

- (void)showWorkspaceOverlayWithMessage:(NSString *)message animated:(BOOL)animated { }
- (void)hideWorkspaceOverlayViewAnimated:(BOOL)animated { }

@end
