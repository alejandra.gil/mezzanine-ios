//
//  NSObject+UIAlertController.h
//  Mezzanine
//
//  Created by miguel on 20/12/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (UIAlertController)

- (void)showAlertController:(UIAlertController *)alertController animated:(BOOL)animated completion:(void (^)(void))completion;

@end
