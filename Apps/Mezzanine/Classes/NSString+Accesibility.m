//
//  NSString+Accesibility.m
//  Mezzanine
//
//  Created by miguel on 13/1/15.
//  Copyright (c) 2015 Oblong Industries. All rights reserved.
//

#import "NSString+Accesibility.h"

@implementation NSString (Accesibility)

-(NSString *) stringByRemovingSymbols
{
  NSString *string = [self stringByReplacingOccurrencesOfString:@"…" withString:@""];
  return string;
}

@end
