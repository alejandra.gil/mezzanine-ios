//
//  MZSlideCreationViewController.m
//  Mezzanine
//
//  Created by Zai Chang on 1/25/11.
//  Copyright 2011 Oblong Industries. All rights reserved.
//

#import "SlideCreationViewController.h"
#import "MZCommunicator.h"
#import "MezzanineStyleSheet.h"
#import "UIImage+Resizing.h"
#import "UIViewController+Three20.h"
#import "UITraitCollection+Additions.h"


@implementation SlideCreationViewController

- (void)viewDidLoad
{
  [super viewDidLoad];

  self.edgesForExtendedLayout = UIRectEdgeNone;

  MezzanineStyleSheet *styleSheet = [MezzanineStyleSheet sharedStyleSheet];
  self.view.backgroundColor = styleSheet.darkFillColor;
  
  self.textView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
  self.textView.backgroundColor = styleSheet.darkFillColor;
  self.textView.font = [UIFont dinOfSize:24.0];
  self.textView.delegate = self;
  self.textView.accessibilityIdentifier = @"SlideCreateViewController.textView";
  
  self.promptLabel.font = [UIFont dinOfSize:24.0];
  self.promptLabel.textColor = styleSheet.mediumFillColor;
  self.promptLabel.numberOfLines = 0;
  self.promptLabel.text = NSLocalizedString(@"Prompt Label text", nil);
  
  self.navigationItem.title = NSLocalizedString(@"Text Slide Title", nil);
  self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel)];
  self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Create Button", nil) style:UIBarButtonItemStylePlain target:self action:@selector(done)];
  self.navigationItem.rightBarButtonItem.enabled = NO;
}


- (void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];

  [_textView becomeFirstResponder];
  
  

  [[NSNotificationCenter defaultCenter] addObserver: self
                                           selector: @selector(updateTextViewFrame:)
                                               name: UIKeyboardDidChangeFrameNotification
                                             object: nil];
}


- (void)viewWillDisappear:(BOOL)animated
{
  [super viewWillDisappear:animated];

  [[NSNotificationCenter defaultCenter] removeObserver: self
                                                  name: UIKeyboardDidChangeFrameNotification
                                                object: nil];
}


#pragma mark - Notifications

- (void)updateTextViewFrame:(NSNotification *)notification
{
  float yOffset = 20; // that's a safe margin at the bottom, between keyboard and textView.
  CGRect keyboardBounds = [[notification.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
  
  // Convert keyboard frame into our view coordinates and take the intersection frame.
  // This frame height is what we need to substract to the textView to make it fit.
  CGRect convertedKeyboardFrame = [self.view.window convertRect:keyboardBounds toView:self.view];
  CGRect intersectionTextViewKeyboardRect = CGRectIntersection(convertedKeyboardFrame, CGRectOffset(self.view.frame, 0, yOffset));
  
  // Update constraint animated
  _bottomSpaceConstraint.constant = CGRectGetHeight(intersectionTextViewKeyboardRect);
  [UIView animateWithDuration:0.2 animations:^{
    [self.view layoutIfNeeded];
  }];
}


#pragma mark - Orientation

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
  return UIInterfaceOrientationMaskAll;
}


#pragma mark - UITextViewDelegate
- (void)textViewDidChange:(UITextView *)textView
{
  BOOL textViewHasText = (self.textView.text.length > 0);
  self.navigationItem.rightBarButtonItem.enabled = textViewHasText;
  self.promptLabel.hidden = textViewHasText;
}


- (void)cancel
{
  [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)done
{
  MezzanineAppContext *context = [MezzanineAppContext currentContext];
  [context.documentInteractionController uploadSlidesWithText:_textView.text];
  
  [self dismissViewControllerAnimated:YES completion:nil];
}

@end
