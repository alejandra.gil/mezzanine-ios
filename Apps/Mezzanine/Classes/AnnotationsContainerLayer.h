//
//  AnnotationsLayer.h
//  Mezzanine
//
//  Created by Zai Chang on 10/22/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "MZAnnotation+Drawing.h"

@interface AnnotationsContainerLayer : CALayer
{
  NSMutableArray *slideObservers;
}

@property (nonatomic, strong) MZItem *item;

-(void) addLayerForAnnotation:(MZAnnotation*)annotation;
-(CALayer*) layerForAnnotation:(MZAnnotation*)annotation;
-(void) removeLayerForAnnotation:(MZAnnotation*)annotation;

@end
