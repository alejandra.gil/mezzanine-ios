//
//  PortfolioViewController_Private.h
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 09/10/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

#import "PortfolioViewController.h"

@interface PortfolioViewController ()
{
  NSMutableArray *_workspaceObservers;
  NSMutableArray *_systemModelObservers;
  NSMutableArray *_presentationObservers;
  CGFloat _previousLayoutHeight;
  
  UIView *_currentSlideView;
  
  UIView *_insertionMarker;
  CGFloat _edgeScrollingSpeed;
  NSTimer *_edgeScrollingTimer;
  
  UIAlertController *_currentAlertController;
  
  UIImagePickerController *_imagePicker;
  
  NSOperationQueue *_imageUploadQueue;
  
  dispatch_once_t onceTokenCollectionViewContentOffset;
}


@property (nonatomic, assign) NSInteger animationCount;
@property (nonatomic, assign) BOOL viewIsVisible;

- (void)clearPortfolio;
- (void)exportPortfolio;

- (void)beginEdgeScrolling;
- (void)endEdgeScrolling;
- (void)handleEdgeScrollingTimer:(NSTimer*)timer;
- (void)handleEdgeScrollingEventAtLocation:(CGPoint)location;


- (void)showInsertionMarkerAtIndex:(NSInteger)index;
- (void)hideInsertionIndicator;
- (NSInteger)insertionIndexForLocation:(CGPoint)location;

- (void)presentFromSlideAtIndex:(NSInteger)slideIndex;
- (void)titleLabelLongPressed:(UIGestureRecognizer*)recognizer;
- (void)showImagePicker:(UIImagePickerControllerSourceType)source;
- (void)presentImagePickerForCamera;
- (void)showSlideCreationView;
- (void)showWhiteboardCaptureView;
- (void)captureWhiteboard:(NSDictionary *)whiteboard;

- (void)openAssetViewerFromCell:(UICollectionViewCell*)cell;

@end
