//
//  PortfolioActionsInteractor.swift
//  Mezzanine
//
//  Created by miguel on 13/2/17.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import UIKit
import Photos

class PortfolioActionsInteractor: NSObject {

  var presenter: PortfolioActionsViewController?
  var communicator: MZCommunicator!
  let imageUploadQueue = OperationQueue()

  fileprivate var systemModelObserverArray = Array <String> ()
  fileprivate var presentationObserverArray = Array <String> ()

  var systemModel: MZSystemModel! {
    willSet(newSystemModel) {
      if self.systemModel != newSystemModel {
        endObservingSystemModel()
      }
    }

    didSet {
      if systemModel != nil {
        presentation = systemModel.currentWorkspace.presentation
        systemModelObserverArray.removeAll()
        beginObservingSystemModel()
      }
      else {
        presentation = nil
      }
    }
  }

  var presentation: MZPresentation! {
    willSet(newPresentation) {
      if self.presentation != newPresentation {
        endObservingPresentation()
      }
    }

    didSet {
      if presentation != nil {
        presentationObserverArray.removeAll()
        beginObservingPresentation()
      }
    }
  }


  init(systemModel: MZSystemModel, communicator: MZCommunicator) {

    self.systemModel = systemModel
    self.communicator = communicator
    self.presentation = systemModel.currentWorkspace.presentation

    imageUploadQueue.name = "imageUploadQueue"
    imageUploadQueue.maxConcurrentOperationCount = 1

    super.init()

    beginObservingSystemModel()
    beginObservingPresentation()
  }

  required init?(coder aDecoder: NSCoder) {

    fatalError("init(coder:) has not been implemented")
  }

  func startOrPausePresentation() {

    let presentation = systemModel.currentWorkspace.presentation

    if (presentation?.active)! {
      communicator.requestor.requestPresentationStopRequest(systemModel.currentWorkspace.uid)
      // Pre-emptive reaction in local UI?
      presentation?.active = false
    }
    else
    {
      communicator.requestor.requestPresentationStartRequest(systemModel.currentWorkspace.uid, currentIndex: (presentation?.currentSlideIndex)!)
      // Pre-emptive reaction in local UI?
      presentation?.active = true;
    }
  }

  func exportPortfolio() {

    systemModel.portfolioExport.info.state = MZExportState.requested
    communicator.requestor.requestPortfolioDownloadRequest(systemModel.currentWorkspace.uid)
  }

  func clearPortfolio() {

    communicator.requestor.requestPortfolioClearRequest(systemModel.currentWorkspace.uid)
  }


  // MARK: Observation

  func beginObservingSystemModel() {

    // currentWorkspace
    systemModelObserverArray.append(systemModel.addObserver(forKeyPath: "currentWorkspace", options:[.new, .old], on: OperationQueue.main) { ( obj, change: [AnyHashable: Any]!) in
      let systemModel = obj as! MZSystemModel
      if let workspace = systemModel.currentWorkspace {
        self.presentation = workspace.presentation
        self.defineCurrentState()
      }
      else {
        self.presentation = nil
      }
    })
  }

  func endObservingSystemModel() {
    for token in systemModelObserverArray {
      systemModel.removeObserver(withBlockToken: token)
    }
  }

  func beginObservingPresentation() {

    presentationObserverArray.append(presentation.addObserver(forKeyPath: "active", options:[.new, .old], on: OperationQueue.main) { ( obj, change: [AnyHashable: Any]!) in
      self.defineCurrentState()
    })

    presentationObserverArray.append(presentation.addObserver(forKeyPath: "slides", options:[.new, .old], on: OperationQueue.main) { ( obj, change: [AnyHashable: Any]!) in
      self.defineCurrentState()
    })
  }

  func endObservingPresentation() {
    for token in presentationObserverArray {
      presentation.removeObserver(withBlockToken: token)
    }
  }


  // MARK: Helper methods for layout

  func defineCurrentState() {
    if self.presentation.slides.count == 0 {
      self.presenter?.state = .emptyPortfolio
    }
    else if self.presentation.active {
      self.presenter?.state = .active
    }
    else {
      self.presenter?.state = .paused
    }
  }
}
