//
//  BinCollectionViewDataSource.swift
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 20/02/2017.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import UIKit


class BinCollectionViewDataSource: NSObject, UICollectionViewDataSource {
  
  var hasLiveStreamsSection = false
  var dragDropView: BinCollectionViewController?
  var communicator: MZCommunicator!

  deinit {
    print ("BinCollectionViewDataSource deinit")
  }

  var workspace: MZWorkspace! {
    didSet {
      hasLiveStreamsSection = existLiveStreamItems()
    }
  }
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return hasLiveStreamsSection ? 2 : 1;
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    if section == 0 {
      if hasLiveStreamsSection && existLiveStreamItems() {
        return validLiveStreams().count
      } else {
        return max(1, self.workspace.presentation.numberOfSlides)
      }
    } else {
      return max(1, self.workspace.presentation.numberOfSlides)
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    var cell: UICollectionViewCell!
    
    if indexPath.section == 0 {
      if hasLiveStreamsSection {
        cell = populateLiveStreamCell(collectionView, indexPath: indexPath)
      } else {
        cell = populateItemCell(collectionView, indexPath: indexPath)
      }
    } else {
      cell = populateItemCell(collectionView, indexPath: indexPath)
    }
    
    if dragDropView == nil || cell is PlaceholderCollectionViewCell {
      return cell
    } else {
      let addGesture = {
        let dragDropManager = OBDragDropManager.shared()
        let dragDropGestureRecognizer = dragDropManager?.createLongPressDragDropGestureRecognizer(with: self.dragDropView)
        dragDropGestureRecognizer?.delegate = self.dragDropView
        dragDropGestureRecognizer?.minimumPressDuration = 0.15;
        cell.addGestureRecognizer(dragDropGestureRecognizer!)
        
        let panGestureRecognizer = dragDropManager?.createDragDropGestureRecognizer(with: OBDirectionalPanGestureRecognizer.self, source: self.dragDropView)
        (panGestureRecognizer as! OBDirectionalPanGestureRecognizer).direction = OBDirectionalPanUp
        panGestureRecognizer?.delegate = self.dragDropView
        cell.addGestureRecognizer(panGestureRecognizer!)
      }
      
      if cell.gestureRecognizers != nil{
        for gestureRecognizer in cell.gestureRecognizers! {
          if !(gestureRecognizer is UILongPressGestureRecognizer) {
            addGesture()
            break
          }
        }
      } else {
        addGesture()
      }
      
      return cell
    }
  }
}

extension BinCollectionViewDataSource {
  
  func populateLiveStreamCell(_ collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
    let liveStreamCell = collectionView.dequeueReusableCell(withReuseIdentifier: liveStreamCellIdentifier, for: indexPath) as! LiveStreamCollectionViewCell
    liveStreamCell.liveStream = validLiveStreams()[indexPath.row]
    
    return liveStreamCell
  }
  
  func populateItemCell(_ collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
    if existItems() == false {
        return collectionView.dequeueReusableCell(withReuseIdentifier: placeholderCellIdentifier, for: indexPath)
    } else {
      let slide = workspace.presentation.slides[indexPath.row]
      if (slide as AnyObject).isVideo == true {
        let liveStreamCell = collectionView.dequeueReusableCell(withReuseIdentifier: liveStreamCellIdentifier, for: indexPath) as! LiveStreamCollectionViewCell
        liveStreamCell.liveStream = workspace.liveStream(withUid: (slide as AnyObject).contentSource)
        
        return liveStreamCell
      } else {
        let itemCell = collectionView.dequeueReusableCell(withReuseIdentifier: itemCellIdentifier, for: indexPath) as! PortfolioCollectionViewCell
        itemCell.communicator = communicator
        itemCell.slide = slide as! MZPortfolioItem
        
        return itemCell
      }
    }
  }
  
  func existLiveStreamItems() -> Bool {
    return validLiveStreams().count > 0 ? true : false
  }
  
  func existItems() -> Bool {
    if workspace != nil {
      return self.workspace.presentation.numberOfSlides > 0 ? true : false
    }
    
    return false
  }
  
  func isValidStream(_ livestream: MZLiveStream) -> Bool {
    return livestream.active == true && livestream.local == true ? true : false
  }
  
  func validLiveStreams() -> Array<MZLiveStream> {
    if workspace != nil {
      let validStreams = workspace.liveStreams.filtered(using: NSPredicate(block: { (obj, dic) -> Bool in
        return self.isValidStream(obj as! MZLiveStream)
      })) as! Array<MZLiveStream>
      
      return validStreams
    }
    
    return Array<MZLiveStream>()
  }
  
  func numberOfLiveStreamsItemsInCollectionView() -> Int {
    return validLiveStreams().count
  }
}
