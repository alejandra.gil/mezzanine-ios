//
//  MezzanineConnectionViewController.h
//  Mezzanine
//
//  Created by Zai Chang on 7/11/13.
//  Copyright (c) 2013 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FPPopoverController.h>

@interface MezzanineConnectionViewController : UIViewController<FPPopoverControllerDelegate, UITextFieldDelegate>

@property (nonatomic, weak) id delegate;

@property (nonatomic, strong) IBOutlet UIImageView *mezzanineLogoImageView;
@property (nonatomic, strong) IBOutlet UITextField *poolNameField;
@property (nonatomic, strong) IBOutlet UIButton *connectButton;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) IBOutlet UILabel *appVersionLabel;

@property (nonatomic, strong) IBOutlet UIImageView *oblongLogoImageView;
@property (nonatomic, strong) IBOutlet UIView *contentView;

@property (nonatomic, strong) IBOutlet UILabel *displayNameLabel;
@property (nonatomic, strong) IBOutlet UIButton *editDisplayNameButton;


@property (nonatomic, strong) UIAlertController *connectionAlertController;


// Network call for deep links and 3DTouch
- (void)connectToServerAtURL:(NSURL*)url from:(NSString *)source;

- (IBAction)connect;
- (IBAction)cancel;
- (IBAction)editDisplayName:(id)sender;

- (void)connectSucceeded:(NSURL *)url;
- (void)connectFailed:(NSError *)error;

@end


@protocol MezzanineConnectionViewControllerDelegate

@optional

// The delegate will try to attempt to do the connection itself.
// Note – This selector is called on a thread other than the main thread

-(void) connectionViewController:(MezzanineConnectionViewController*)controller
requestsAsyncConnectionAttemptTo:(NSURL*)endPointURL
                         success:(void (^)())successBlock
                           error:(void (^)(NSError *))errorBlock;

// The delegate should cancel the on-going connection process while it is still connecting
-(void) connectionViewControllerWasCancelled:(MezzanineConnectionViewController*)controller;

@end



