//
//  CornerBorderView.m
//  Mezzanine
//
//  Created by miguel on 18/07/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import "CornerBorderView.h"
#import "MezzanineStyleSheet.h"

#define MIN_SIZE 6

@implementation CornerBorderView

@synthesize cornerType;

- (id)initWithFrame:(CGRect)frame
{
  self = [super initWithFrame:frame];
  if (self)
  {
    self.backgroundColor = [UIColor clearColor];
  }

  return self;
}

- (void)drawRect:(CGRect)rect
{
  // There's no point of drawing corners that are too small
  // This also covers the case when it would be so small that the resulting
  // bracketsRect could be of size(0,0) which ends up crashing.
  if (self.bounds.size.width < MIN_SIZE || self.bounds.size.width < MIN_SIZE)
    return;

  MezzanineStyleSheet *styleSheet = [MezzanineStyleSheet sharedStyleSheet];
  CGContextRef context = UIGraphicsGetCurrentContext();

  UIGraphicsPushContext(context);

  CGContextSetAllowsAntialiasing(context, YES);
  CGContextSetAllowsFontSmoothing(context, YES);
  CGContextSetShouldAntialias(context, YES);
  CGContextSetShouldSmoothFonts(context, YES);
  CGContextSetInterpolationQuality(context, kCGInterpolationHigh);

  CGContextClearRect(context, rect);

  // Corner brackets
  CGFloat minimumDimension = MIN(self.bounds.size.width, self.bounds.size.height);
  CGFloat length = self.bounds.size.width;

  CGFloat fontSize = MAX(MIN(minimumDimension / 6.0, 40), 20);
  CGFloat lineWidth = MAX(1.0, fontSize / 14.0);
  CGContextSetLineWidth(context, lineWidth);

  CGRect bracketsRect = CGRectInset(self.bounds, lineWidth, lineWidth);

  [styleSheet.videoPlaceholderPrimaryColor setStroke];
  [styleSheet.videoPlaceholderPrimaryColor setFill];

  CGMutablePathRef path = CGPathCreateMutable();

  switch (cornerType) {
    case CornerBorderViewTypeTopLeft:
      // Top left corner
      CGPathMoveToPoint(path, NULL, CGRectGetMinX(bracketsRect) + length, CGRectGetMinY(bracketsRect));
      CGPathAddLineToPoint(path, NULL, CGRectGetMinX(bracketsRect), CGRectGetMinY(bracketsRect));
      CGPathAddLineToPoint(path, NULL, CGRectGetMinX(bracketsRect), CGRectGetMinY(bracketsRect) + length);
      break;
    case CornerBorderViewTypeTopRight:
      // Top right corner
      CGPathMoveToPoint(path, NULL, CGRectGetMaxX(bracketsRect) - length, CGRectGetMinY(bracketsRect));
      CGPathAddLineToPoint(path, NULL, CGRectGetMaxX(bracketsRect), CGRectGetMinY(bracketsRect));
      CGPathAddLineToPoint(path, NULL, CGRectGetMaxX(bracketsRect), CGRectGetMinY(bracketsRect) + length);
      break;
    case CornerBorderViewTypeBottomLeft:
      // Bottom left corner
      CGPathMoveToPoint(path, NULL, CGRectGetMinX(bracketsRect) + length, CGRectGetMaxY(bracketsRect));
      CGPathAddLineToPoint(path, NULL, CGRectGetMinX(bracketsRect), CGRectGetMaxY(bracketsRect));
      CGPathAddLineToPoint(path, NULL, CGRectGetMinX(bracketsRect), CGRectGetMaxY(bracketsRect) - length);
      break;
    case CornerBorderViewTypeBottomRight:
      // Bottom right corner
      CGPathMoveToPoint(path, NULL, CGRectGetMaxX(bracketsRect) - length, CGRectGetMaxY(bracketsRect));
      CGPathAddLineToPoint(path, NULL, CGRectGetMaxX(bracketsRect), CGRectGetMaxY(bracketsRect));
      CGPathAddLineToPoint(path, NULL, CGRectGetMaxX(bracketsRect), CGRectGetMaxY(bracketsRect) - length);
    default:
      break;
  }

  CGContextAddPath(context, path);
  CGContextDrawPath(context, kCGPathStroke);
  CGPathRelease(path);

  UIGraphicsPopContext();
}

@end