//
//  WorkspaceListCell.h
//  Mezzanine
//
//  Created by miguel on 10/03/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FPPopoverController.h"

@interface WorkspaceListCell : UICollectionViewCell <FPPopoverControllerDelegate, UIGestureRecognizerDelegate>

@property (nonatomic, strong) MZWorkspace *workspace;
@property (nonatomic, strong) MZSystemModel *systemModel;
@property (nonatomic, strong) MZCommunicator *communicator;

@property (nonatomic, strong) UIButton *optionsButton;

@property (nonatomic, assign) BOOL isOpeningViewCell; // Special case for the cell embedded inside WorkspaceOpeningViewController, in which case it does not listen to load events because this causes a visual state change when loading is complete which is undesired

- (void)updateCellBorder;
- (void)updatePopoverForTraitCollection:(UITraitCollection *)traitCollection;

@end
