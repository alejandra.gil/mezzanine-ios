//
//  PhotoManager.swift
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 02/01/2017.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import UIKit
import Photos


struct PhotoManagerConstants {
  static let thumbnailWidth:CGFloat = 100.0
  static let thumbnailHeight:CGFloat = 100.0
}

class PhotoManager: NSObject {
  
  @objc static let sharedInstance = PhotoManager()

  let imageManager = PHImageManager.default()
  
  // Authorization
  @objc func authorizationStatus(_ completion: @escaping (PHAuthorizationStatus, Bool) ->()) {
    let status = PHPhotoLibrary.authorizationStatus()
    
    if status == .notDetermined {
      PHPhotoLibrary.requestAuthorization({ (PHAuthorizationStatus) in
        completion(PHAuthorizationStatus, true)
      })
    } else {
      completion(status, false);
    }
  }

  
  // Save images to library
  @objc func saveImageToLibrary(_ image: UIImage,  completion: ((Bool, NSError?) -> Void)?) {
    PHPhotoLibrary.shared().performChanges({
      PHAssetChangeRequest.creationRequestForAsset(from: image)
      }, completionHandler: completion as? (Bool, Error?) -> Void)
  }

  func smartAlbumOfTypeToArrayOfAlbums(subtype: PHAssetCollectionSubtype) -> [PhotoAlbum] {
    let fetchOptions = PHFetchOptions()
    let smartAlbums = PHAssetCollection.fetchAssetCollections(with: .smartAlbum, subtype: subtype, options: fetchOptions)

    var newAlbumsArray = [PhotoAlbum]()
    smartAlbums.enumerateObjects { (assetCollection, ix, pointer) in
      let title = assetCollection.localizedTitle ?? ""
      newAlbumsArray.append(PhotoAlbum(assetCollection: assetCollection, title: title, smart: true))
    }
    return newAlbumsArray
  }

  func albumsToArrayOfAlbums() -> [PhotoAlbum] {
    // Fetch all regular albums (imported, created on the device or other devices using the same iCloud account)
    let albums = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: nil)
    var newAlbumsArray = [PhotoAlbum]()
    albums.enumerateObjects { (assetCollection, ix, pointer) in
      let title = assetCollection.localizedTitle ?? ""
      if assetCollection.estimatedAssetCount != NSNotFound && assetCollection.estimatedAssetCount > 0 {
        newAlbumsArray.append(PhotoAlbum(assetCollection: assetCollection, title: title, smart: false))
      }
    }

    // Sort them
    newAlbumsArray.sort(by: {
      let firstTitle = $0.assetCollection.localizedTitle
      let secondTitle = $1.assetCollection.localizedTitle

      if (firstTitle != nil && secondTitle == nil) {
        return false
      }
      else if (firstTitle == nil && secondTitle != nil) {
        return true
      }
      else {
        return firstTitle! < secondTitle!
      }
    })

    return newAlbumsArray
  }
  
  // Get an array of all photo albums (smart and regular)
  @objc func getPhotoAlbums() -> Array<PhotoAlbum> {

    var albumsArray = [PhotoAlbum]()
    let subtypes: [PHAssetCollectionSubtype] = [PHAssetCollectionSubtype.smartAlbumRecentlyAdded,
                                                PHAssetCollectionSubtype.smartAlbumUserLibrary,
                                                PHAssetCollectionSubtype.smartAlbumFavorites,
                                                PHAssetCollectionSubtype.smartAlbumScreenshots,
                                                PHAssetCollectionSubtype.smartAlbumSelfPortraits,
                                                PHAssetCollectionSubtype.smartAlbumGeneric,
                                                PHAssetCollectionSubtype.smartAlbumDepthEffect,
                                                PHAssetCollectionSubtype.smartAlbumPanoramas]

    // Append all smart albums in the same order that the subtypes array
    for subtype in subtypes {
      let fetchedAlbums = smartAlbumOfTypeToArrayOfAlbums(subtype: subtype)
      albumsArray.append(contentsOf: fetchedAlbums)
    }

    // After them append the rest of albums
    albumsArray.append(contentsOf: albumsToArrayOfAlbums())

    return albumsArray
  }
  
  @objc func getPhotoCountPerAlbum(_ albums: Array<PhotoAlbum>, completionHandler: @escaping (Int, PhotoAlbum) -> ()) {
    let qualityOfServiceClass = DispatchQoS.QoSClass.userInitiated
    let backgroundQueue = DispatchQueue.global(qos: qualityOfServiceClass)
    backgroundQueue.async(execute: {
      var index = 0
      for album in albums {
        if album.assetCollection.estimatedAssetCount != NSNotFound {
          album.count = album.assetCollection.estimatedAssetCount
        }
        else {
          let fetchOptions = PHFetchOptions()
          fetchOptions.predicate = NSPredicate(format: "mediaType == %d", PHAssetMediaType.image.rawValue)
          let result = PHAsset.fetchAssets(in: album.assetCollection, options: fetchOptions)
          album.count = result.count
        }
        DispatchQueue.main.async {
          completionHandler(index, album)
          index += 1
        }
      }
    })
  }

  @objc func getCoverFromAlbum(_ album: PhotoAlbum, size: CGSize, completion: @escaping (UIImage?, PhotoAlbum) -> Void) {
    let qualityOfServiceClass = DispatchQoS.QoSClass.background
    let backgroundQueue = DispatchQueue.global(qos: qualityOfServiceClass)
    backgroundQueue.async(execute: {
      let fetchOptions = PHFetchOptions()
      fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: true)]
      
      let assetsInCollection = PHAsset.fetchAssets(in: album.assetCollection, options: fetchOptions)
      guard let coverAsset = assetsInCollection.lastObject else {
        DispatchQueue.main.async {
          completion(nil, album)
        }
        return
      }
      
      let imageRequestOptions = PHImageRequestOptions()
      imageRequestOptions.resizeMode = .exact
      imageRequestOptions.deliveryMode = .fastFormat
      imageRequestOptions.isNetworkAccessAllowed = true
      imageRequestOptions.isSynchronous = false
      
      let size = CGSize(width: UIScreen.main.scale * size.width, height: UIScreen.main.scale * size.height)
      DispatchQueue.main.async {
        self.imageManager.requestImage(for: coverAsset, targetSize: size, contentMode: .aspectFill, options: imageRequestOptions) { (image: UIImage?, _) in
          completion(image, album)
        }
      }
    })
  }

  func getPhotoAssetsFromAlbum(_ album: PhotoAlbum, completion:@escaping (Array<PhotoAsset>) -> Void) {

    if album.photoAssets != nil {
      completion(album.photoAssets!)
      return
    }

    let qualityOfServiceClass = DispatchQoS.QoSClass.userInitiated
    let backgroundQueue = DispatchQueue.global(qos: qualityOfServiceClass)
    backgroundQueue.async(execute: {

      let allPhotosOptions = PHFetchOptions()
      allPhotosOptions.sortDescriptors = [NSSortDescriptor.init(key: "creationDate", ascending: true)]
      let allPhotos = PHAsset.fetchAssets(in: album.assetCollection, options: allPhotosOptions)

      var assetsArray = Array<PhotoAsset>()

      for i in 0...allPhotos.count-1 {
        let photoAsset = PhotoAsset(photoAsset: allPhotos[i] )
        if photoAsset.photoAsset.mediaType == PHAssetMediaType.image {
          assetsArray.append(photoAsset)
        }
      }

      album.photoAssets = assetsArray

      DispatchQueue.main.async {
        completion(assetsArray)
      }
    })
  }

  // Get Photos
  @objc func requestThumbnail(for asset: PhotoAsset, resultHandler: @escaping (UIImage?) -> Void) -> Int {
    let imageTargetSize = CGSize(width: PhotoManagerConstants.thumbnailWidth, height: PhotoManagerConstants.thumbnailHeight)
    return Int(imageManager.requestImage(for: asset.photoAsset,
                                         targetSize: imageTargetSize,
                                         contentMode: .aspectFill,
                                         options: nil) { (result, _) in
                                            resultHandler(result)
                                         })
  }

  @objc func cancelThumbnailRequest(_ requestID: Int) {
    imageManager.cancelImageRequest(PHImageRequestID(requestID))
  }

  @objc func fetchPhotoAssetData(_ photoAsset: PhotoAsset) -> Data {
    let imageRequestOptions = PHImageRequestOptions()
    imageRequestOptions.resizeMode = .none
    imageRequestOptions.deliveryMode = .highQualityFormat
    imageRequestOptions.isNetworkAccessAllowed = true
    imageRequestOptions.isSynchronous = true

    var returnData: Data? = nil
    imageManager.requestImageData(for: photoAsset.photoAsset,
                                  options: imageRequestOptions,
                                  resultHandler: { (imageData, dataUTI, orientation, info) in
                                    var value = (dataUTI?.components(separatedBy:".").last)!
                                    if value == "jpeg" {
                                      value = "jpg"
                                    }

                                    // Bug 19778. Convert HEIC to JPG if needed
                                    guard let imageData = imageData else { return }
                                    if value == "heic" || value == "heif" {
                                      guard let ciImage = CIImage(data: imageData) else { return }
                                      guard let data = CIContext().jpegRepresentation(of: ciImage, colorSpace: CGColorSpaceCreateDeviceRGB()) else { return }
                                      returnData = data
                                      value = "jpg"
                                    } else {
                                      returnData = imageData
                                    }

                                    photoAsset.imageType = value
                                    photoAsset.filesize = imageData.count
                                  })
    return returnData!
  }
}


extension Array {
  mutating func rearrange(_ from: Int, to: Int) {
    insert(remove(at: from), at: to)
  }
}
