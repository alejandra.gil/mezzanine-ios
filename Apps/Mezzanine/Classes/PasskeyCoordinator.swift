//
//  PasskeyCoordinator.swift
//  Mezzanine
//
//  Created by Miguel Sánchez Valdés on 6/2/19.
//  Copyright © 2019 Oblong Industries. All rights reserved.
//

import UIKit

typealias SwiftPasskeyZiggyCompleteBlock = (@convention(block) (NSString)->())

public extension Notification.Name {
  public static let passkeyViewPresented = Notification.Name("PassphraseViewPresented")
}

@objc public extension NSNotification {
  public static let passkeyViewPresented = Notification.Name.passkeyViewPresented
}

@objc class PasskeyCoordinator: NSObject {

  private var notificationObservers = [NSObjectProtocol]()

  @objc var passkeyViewController: PassphraseViewController?
  @objc var navigationController: UINavigationController?
  @objc var communicator: MZCommunicator? {
    willSet {
      removeAllNotificationObservers()
    }
    didSet {
      observeIncorrectPasskeyNotification()
      observeZiggyPasskeyRequestNotification()
      observeZiggyPasskeyHideNotification()
    }
  }

  private var ziggyCompletionBlock:SwiftPasskeyZiggyCompleteBlock? = nil
  private var numberOfFields: Int = 0

  // MARK: Passkey Presence

  @objc var passkeyIsVisible: Bool {
    get {
      return passkeyViewController != nil
    }
  }

  @objc var passkeyIsInTransition: Bool {
    get {
      guard let pvc = passkeyViewController else { return false }
      return pvc.isBeingDismissed || pvc.isBeingPresented
    }
  }

  @objc func presentPasskeyView(_ animated: Bool) {

    guard let navigationController = navigationController,
          let communicator = communicator else { return }

    // Set number of fields based on numberOfFields flag if available (Ziggy systems)
    // or roomKey as a fallback
    var fields = communicator.systemModel.featureToggles.roomKey ? 6 : 3;
    if numberOfFields > 0 {
      fields = numberOfFields;
    }

    guard let vc = PassphraseViewController(keyLength: fields) else { return }
    vc.delegate = self
    vc.modalTransitionStyle = .coverVertical

    /*
     Make transparent the background. In case we show analytics opt in dialog
     (it's modal) we will set alpha to 0 to make a nicer dismissal
     See confirmAction() at AnalyticsViewController
     */

    vc.modalPresentationStyle = .overCurrentContext
    navigationController.present(vc, animated: animated) {
      self.checkPassphraseViewControllerState()
    }

    passkeyViewController = vc
  }

  @objc func hidePasskeyView() {

    guard let _ = passkeyViewController,
          let navController = navigationController else { return }

    navController.dismiss(animated: true, completion: nil)

    navController.transitionCoordinator?.animate(alongsideTransition: nil,
                                                 completion: { (_) in
      self.passkeyViewController = nil
      self.checkPassphraseViewControllerState()
    })
  }

  @objc func cleanPasskey() {
    passkeyViewController = nil
  }


  private func checkPassphraseViewControllerState() {

    if ziggyCompletionBlock != nil { return }

    guard let communicator = communicator else { return }

    if passkeyIsInTransition {
      return
    }

    if communicator.systemModel.passphraseRequested {
      if (!passkeyIsVisible) {
        self.presentPasskeyView(true)
      }
      else {
        NotificationCenter.default.post(name: .passkeyViewPresented,
                                        object: self,
                                        userInfo: nil)
      }
    }
    else {
      hidePasskeyView()
    }
  }

  // MARK: Observers

  private func removeAllNotificationObservers() {
    for observer in notificationObservers {
      NotificationCenter.default.removeObserver(observer)
    }
    notificationObservers.removeAll()
  }

  private func observeZiggyPasskeyRequestNotification() {
    guard let communicator = communicator else { return }
    let name = NSNotification.Name(rawValue: MZSessionManagerShowPasskeyViewNotification)
    let observer = NotificationCenter.default.addObserver(forName: name,
                                                          object: communicator.sessionManager,
                                                          queue: OperationQueue.main) { [unowned self] notification in
                                                            guard let userInfo = notification.userInfo else { return }

                                                            self.numberOfFields = userInfo["numberOfFields"] as! Int

                                                            // We need to cast an ObjC block into its equivalent Swift closure
                                                            // The following operation casts the objC bits to AnyObject (untyped object)
                                                            let objcBlock = userInfo["completed"] as AnyObject

                                                            // And finally cast as the type of closure we want
                                                            let block = unsafeBitCast(objcBlock, to: SwiftPasskeyZiggyCompleteBlock.self)

                                                            self.ziggyCompletionBlock = block
                                                            self.presentPasskeyView(true)
                                                          }
    notificationObservers.append(observer)
  }

  private func observeZiggyPasskeyHideNotification() {
    guard let communicator = communicator else { return }
    let name = NSNotification.Name(rawValue: MZSessionManagerHidePasskeyViewNotification)
    let observer = NotificationCenter.default.addObserver(forName: name,
                                                          object: communicator.sessionManager,
                                                          queue: OperationQueue.main) { [unowned self] (_) in
                                                            self.hidePasskeyView()
                                                            communicator.cancelPassphraseJoin()
                                                            self.ziggyCompletionBlock = nil
    }

    notificationObservers.append(observer)
  }

  private func observeIncorrectPasskeyNotification() {
    guard let communicator = communicator else { return }
    let name = NSNotification.Name(rawValue: MZCommunicatorIncorrectPassphraseNotification)
    let observer = NotificationCenter.default.addObserver(forName: name,
                                                          object: communicator,
                                                          queue: OperationQueue.main) {  [unowned self] (_) in
                                                            self.incorrectPasskey()
    }

    notificationObservers.append(observer)
  }

  func incorrectPasskey() {
    guard let vc = passkeyViewController else { return }
    vc.presentIncorrectPasskeyAlert()
  }
}

extension PasskeyCoordinator : PassphraseViewControllerDelegate {

  public func passkeyCharacterEntered() {
    let name = Notification.Name(rawValue: MZSessionManagerUpdatedPasskeyViewNotification)
    NotificationCenter.default.post(Notification.init(name: name))
  }

  public func passkeyWasIntroduced(_ passkey: String!) {

    // Connecting to a Ziggy-enabled Mezzzanine
    if let zcb = ziggyCompletionBlock {
      zcb(passkey as NSString)
      return
    }

    // Connecting to a plasma (plasma-web-proxy) Mezzanine
    guard let communicator = communicator else { return }
    communicator.requestJoin(withPassphrase: passkey)
  }

  public func passkeyCancelled() {
    guard let communicator = communicator else { return }
    communicator.cancelPassphraseJoin()
  }
}
