//
//  UITraitCollection+Additions.m
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 25/01/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

#import "UITraitCollection+Additions.h"

@implementation UITraitCollection (Additions)


#pragma mark - Interface idioms

- (BOOL)isiPhone
{
  return self.userInterfaceIdiom == UIUserInterfaceIdiomPhone;
}

- (BOOL)isiPad
{
  return self.userInterfaceIdiom == UIUserInterfaceIdiomPad;
}


#pragma mark - Size Classes

- (BOOL)isCompactWidth
{
  return self.horizontalSizeClass == UIUserInterfaceSizeClassCompact;
}

- (BOOL)isRegularWidth
{
  return self.horizontalSizeClass == UIUserInterfaceSizeClassRegular;
}

- (BOOL)isCompactHeight
{
  return self.verticalSizeClass == UIUserInterfaceSizeClassCompact;
}

- (BOOL)isRegularHeight
{
  return self.verticalSizeClass == UIUserInterfaceSizeClassRegular;
}


#pragma mark - iPhone

- (BOOL)isiPhoneCompactHeight
{
  return self.userInterfaceIdiom == UIUserInterfaceIdiomPhone && self.verticalSizeClass == UIUserInterfaceSizeClassCompact;
}

- (BOOL)isiPhoneRegularHeight
{
  return self.userInterfaceIdiom == UIUserInterfaceIdiomPhone && self.verticalSizeClass == UIUserInterfaceSizeClassRegular;
}

- (BOOL)isiPhoneCompactWidth
{
  return self.userInterfaceIdiom == UIUserInterfaceIdiomPhone && self.horizontalSizeClass == UIUserInterfaceSizeClassCompact;
}

- (BOOL)isiPhoneRegularWidth
{
  return self.userInterfaceIdiom == UIUserInterfaceIdiomPhone && self.horizontalSizeClass == UIUserInterfaceSizeClassRegular;
}


#pragma mark - iPad

- (BOOL)isiPadCompactHeight
{
  return self.userInterfaceIdiom == UIUserInterfaceIdiomPad && self.verticalSizeClass == UIUserInterfaceSizeClassCompact;
}

- (BOOL)isiPadRegularHeight
{
  return self.userInterfaceIdiom == UIUserInterfaceIdiomPad && self.verticalSizeClass == UIUserInterfaceSizeClassRegular;
}

- (BOOL)isiPadCompactWidth
{
  return self.userInterfaceIdiom == UIUserInterfaceIdiomPad && self.horizontalSizeClass == UIUserInterfaceSizeClassCompact;
}

- (BOOL)isiPadRegularWidth
{
  return self.userInterfaceIdiom == UIUserInterfaceIdiomPad && self.horizontalSizeClass == UIUserInterfaceSizeClassRegular;
}


@end
