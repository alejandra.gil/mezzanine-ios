//
//  WindshieldPlaceholderView.m
//  Mezzanine
//
//  Created by miguel on 14/04/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import "WindshieldPlaceholderView.h"

@implementation WindshieldPlaceholderView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
      self.opaque = NO;
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
  CGContextRef context = UIGraphicsGetCurrentContext();

  CGContextClearRect(context, rect);

  UIGraphicsPushContext(context);

  CGContextSetAllowsAntialiasing(context, YES);
  CGContextSetShouldAntialias(context, YES);
  CGContextSetInterpolationQuality(context, kCGInterpolationHigh);

  // Corner brackets
  CGFloat length = 15.0;
  CGFloat width = ceilf(length / 4.0);
  CGFloat lineWidth = 1.0;

  CGRect bracketsRect = CGRectInset(self.bounds, lineWidth, lineWidth);
  CGContextSetLineWidth(context, lineWidth);

  CGContextSetFillColorWithColor(context, [[[UIColor whiteColor] colorWithAlphaComponent:0.75] CGColor]);
  CGContextSetStrokeColorWithColor(context, [[UIColor blackColor] CGColor]);

  CGMutablePathRef path = CGPathCreateMutable();
  // Top left corner
  CGPathMoveToPoint(path, NULL, CGRectGetMinX(bracketsRect) + length, CGRectGetMinY(bracketsRect));
  CGPathAddLineToPoint(path, NULL, CGRectGetMinX(bracketsRect), CGRectGetMinY(bracketsRect));
  CGPathAddLineToPoint(path, NULL, CGRectGetMinX(bracketsRect), CGRectGetMinY(bracketsRect) + length);
  CGPathAddLineToPoint(path, NULL, CGRectGetMinX(bracketsRect) + width, CGRectGetMinY(bracketsRect) + length);
  CGPathAddLineToPoint(path, NULL, CGRectGetMinX(bracketsRect) + width, CGRectGetMinY(bracketsRect) + width);
  CGPathAddLineToPoint(path, NULL, CGRectGetMinX(bracketsRect) + length, CGRectGetMinY(bracketsRect) + width);
  CGPathAddLineToPoint(path, NULL, CGRectGetMinX(bracketsRect) + length, CGRectGetMinY(bracketsRect));
  CGPathCloseSubpath(path);

  // Top right corner
  CGPathMoveToPoint(path, NULL, CGRectGetMaxX(bracketsRect) - length, CGRectGetMinY(bracketsRect));
  CGPathAddLineToPoint(path, NULL, CGRectGetMaxX(bracketsRect), CGRectGetMinY(bracketsRect));
  CGPathAddLineToPoint(path, NULL, CGRectGetMaxX(bracketsRect), CGRectGetMinY(bracketsRect) + length);
  CGPathAddLineToPoint(path, NULL, CGRectGetMaxX(bracketsRect) - width, CGRectGetMinY(bracketsRect) + length);
  CGPathAddLineToPoint(path, NULL, CGRectGetMaxX(bracketsRect) - width, CGRectGetMinY(bracketsRect) + width);
  CGPathAddLineToPoint(path, NULL, CGRectGetMaxX(bracketsRect) - length, CGRectGetMinY(bracketsRect) + width);
  CGPathAddLineToPoint(path, NULL, CGRectGetMaxX(bracketsRect) - length, CGRectGetMinY(bracketsRect));
  CGPathCloseSubpath(path);

  // Bottom right corner
  CGPathMoveToPoint(path, NULL, CGRectGetMaxX(bracketsRect) - length, CGRectGetMaxY(bracketsRect));
  CGPathAddLineToPoint(path, NULL, CGRectGetMaxX(bracketsRect), CGRectGetMaxY(bracketsRect));
  CGPathAddLineToPoint(path, NULL, CGRectGetMaxX(bracketsRect), CGRectGetMaxY(bracketsRect) - length);
  CGPathAddLineToPoint(path, NULL, CGRectGetMaxX(bracketsRect) - width, CGRectGetMaxY(bracketsRect) - length);
  CGPathAddLineToPoint(path, NULL, CGRectGetMaxX(bracketsRect) - width, CGRectGetMaxY(bracketsRect) - width);
  CGPathAddLineToPoint(path, NULL, CGRectGetMaxX(bracketsRect) - length, CGRectGetMaxY(bracketsRect) - width);
  CGPathAddLineToPoint(path, NULL, CGRectGetMaxX(bracketsRect) - length, CGRectGetMaxY(bracketsRect));
  CGPathCloseSubpath(path);

  // Bottom left corner
  CGPathMoveToPoint(path, NULL, CGRectGetMinX(bracketsRect) + length, CGRectGetMaxY(bracketsRect));
  CGPathAddLineToPoint(path, NULL, CGRectGetMinX(bracketsRect), CGRectGetMaxY(bracketsRect));
  CGPathAddLineToPoint(path, NULL, CGRectGetMinX(bracketsRect), CGRectGetMaxY(bracketsRect) - length);
  CGPathAddLineToPoint(path, NULL, CGRectGetMinX(bracketsRect) + width, CGRectGetMaxY(bracketsRect) - length);
  CGPathAddLineToPoint(path, NULL, CGRectGetMinX(bracketsRect) + width, CGRectGetMaxY(bracketsRect) - width);
  CGPathAddLineToPoint(path, NULL, CGRectGetMinX(bracketsRect) + length, CGRectGetMaxY(bracketsRect) - width);
  CGPathAddLineToPoint(path, NULL, CGRectGetMinX(bracketsRect) + length, CGRectGetMaxY(bracketsRect));
  CGPathCloseSubpath(path);

  // Central cross
  CGPathMoveToPoint(path, NULL, CGRectGetMidX(bracketsRect) - width / 2.0, CGRectGetMidY(bracketsRect) - length / 2.0);
  CGPathAddLineToPoint(path, NULL, CGRectGetMidX(bracketsRect) - width / 2.0, CGRectGetMidY(bracketsRect) - width / 2.0);
  CGPathAddLineToPoint(path, NULL, CGRectGetMidX(bracketsRect) - length / 2.0, CGRectGetMidY(bracketsRect) - width / 2.0);
  CGPathAddLineToPoint(path, NULL, CGRectGetMidX(bracketsRect) - length / 2.0, CGRectGetMidY(bracketsRect) + width / 2.0);
  CGPathAddLineToPoint(path, NULL, CGRectGetMidX(bracketsRect) - width / 2.0, CGRectGetMidY(bracketsRect) + width / 2.0);
  CGPathAddLineToPoint(path, NULL, CGRectGetMidX(bracketsRect) - width / 2.0, CGRectGetMidY(bracketsRect) + length / 2.0);
  CGPathAddLineToPoint(path, NULL, CGRectGetMidX(bracketsRect) + width / 2.0, CGRectGetMidY(bracketsRect) + length / 2.0);
  CGPathAddLineToPoint(path, NULL, CGRectGetMidX(bracketsRect) + width / 2.0, CGRectGetMidY(bracketsRect) + width / 2.0);
  CGPathAddLineToPoint(path, NULL, CGRectGetMidX(bracketsRect) + length / 2.0, CGRectGetMidY(bracketsRect) + width / 2.0);
  CGPathAddLineToPoint(path, NULL, CGRectGetMidX(bracketsRect) + length / 2.0, CGRectGetMidY(bracketsRect) - width / 2.0);
  CGPathAddLineToPoint(path, NULL, CGRectGetMidX(bracketsRect) + width / 2.0, CGRectGetMidY(bracketsRect) - width / 2.0);
  CGPathAddLineToPoint(path, NULL, CGRectGetMidX(bracketsRect) + width / 2.0, CGRectGetMidY(bracketsRect) - length / 2.0);
  CGPathCloseSubpath(path);

  CGContextAddPath(context, path);
  CGContextDrawPath(context, kCGPathFillStroke);
  CGContextFillPath(context);
  CGPathRelease(path);

  UIGraphicsPopContext();
}

@end
