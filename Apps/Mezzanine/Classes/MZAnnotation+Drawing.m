//
//  MZAnnotation+Drawing.h
//  Mezzanine
//
//  Created by Zai Chang on 10/22/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import "MZAnnotation+Drawing.h"

@implementation MZAnnotation (Drawing)

-(Class) CALayerClass
{
  return [CALayer class];
}


-(void) updateLayer:(CALayer*)layer
{
  // No op
}


-(void) drawLayer:(CALayer *)layer inContext:(CGContextRef)ctx
{
  [NSException raise:@"NotImplementedException" format:@"MZAnnotation subclass %@ should override drawLayer:inContext:", [self class]];
}

@end



@implementation MZFreehandAnnotation (Drawing)

-(Class) CALayerClass
{
  return [CAShapeLayer class];
}


-(CGPoint) convertPointInLayer:(CALayer*)layer fromNormalizedPoint:(CGPoint)normalizedPoint
{
  CGPoint point = CGPointMake(normalizedPoint.x * layer.bounds.size.width, normalizedPoint.y * layer.bounds.size.height);
  return point;
}


-(void) updateLayer:(CALayer*)layer
{
  CAShapeLayer *shapeLayer = (CAShapeLayer*) layer;
  shapeLayer.strokeColor = self.strokeColour.CGColor;
  shapeLayer.lineWidth = self.strokeWidth;
  shapeLayer.fillRule = kCAFillRuleEvenOdd;
  shapeLayer.fillColor = UIColor.clearColor.CGColor;
  
  UIBezierPath *bezierPath = [[UIBezierPath alloc] init];
	
  if (self.points.count > 0)
  {
    CGPoint firstPoint = [(self.points)[0] CGPointValue];
    [bezierPath moveToPoint:[self convertPointInLayer:layer fromNormalizedPoint:firstPoint]];
    
    if (self.points.count > 1)
    {
      CGPoint secondPoint = [(self.points)[1] CGPointValue];
      CGPoint firstMidPoint = CGPointMake((firstPoint.x + secondPoint.x) / 2.0,
                                          (firstPoint.y + secondPoint.y) / 2.0);
      [bezierPath addLineToPoint:[self convertPointInLayer:layer fromNormalizedPoint:firstMidPoint]];
      
      for (NSUInteger i=0; i < self.points.count-1; i++)
      {
        CGPoint currentPoint = [(self.points)[i] CGPointValue];
        CGPoint nextPoint = [(self.points)[i+1] CGPointValue];
        CGPoint midPoint = CGPointMake((currentPoint.x + nextPoint.x) / 2.0,
                                       (currentPoint.y + nextPoint.y) / 2.0);
        [bezierPath addQuadCurveToPoint:[self convertPointInLayer:layer fromNormalizedPoint:midPoint]
                           controlPoint:[self convertPointInLayer:layer fromNormalizedPoint:currentPoint]];
      }
      
      CGPoint lastPoint = [[self.points lastObject] CGPointValue];
      [bezierPath addLineToPoint:[self convertPointInLayer:layer fromNormalizedPoint:lastPoint]];
    }
  }
  
  shapeLayer.path = bezierPath.CGPath;
}

@end



@implementation MZTextAnnotation (Drawing)

-(void) drawLayer:(CALayer *)layer inContext:(CGContextRef)ctx
{
}

@end



@implementation MZArrowAnnotation (Drawing)

-(void) drawLayer:(CALayer *)layer inContext:(CGContextRef)ctx
{
}

@end
