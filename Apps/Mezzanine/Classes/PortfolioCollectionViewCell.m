//
//  PortfolioCollectionViewCell.m
//  Mezzanine
//
//  Created by Zai Chang on 4/2/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import "PortfolioCollectionViewCell.h"
#import "NSObject+BlockObservation.h"
#import "MezzanineStyleSheet.h"
#import "UIImageView+MZDownloadManager.h"
#import "Mezzanine-Swift.h"

@interface PortfolioCollectionViewCell ()
{
  NSMutableArray *_slideObservers;
}

@end


@implementation PortfolioCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
      MezzanineStyleSheet *styleSheet = [MezzanineStyleSheet sharedStyleSheet];
      if (![SettingsFeatureToggles sharedInstance].teamworkUI)
      {
        self.layer.borderColor = styleSheet.mediumFillColor.CGColor;
        self.layer.borderWidth = 1.0 / [UIScreen mainScreen].scale;
      }
      self.backgroundColor = styleSheet.darkestFillColor;
      
      _imageView = [[UIImageView alloc] initWithFrame:frame];
      _imageView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
      _imageView.contentMode = UIViewContentModeScaleAspectFit;
      _imageView.clipsToBounds = YES;
      _imageView.userInteractionEnabled = NO;  // Important for gesture recognizers
      [self.contentView addSubview:_imageView];

      self.accessibilityIdentifier = @"PortfolioView.CollectionViewCell";
    }
    return self;
}


- (void)layoutSubviews
{
  [super layoutSubviews];

  CGRect contentViewBounds = self.contentView.bounds;
  if (![SettingsFeatureToggles sharedInstance].teamworkUI)
  {
    _imageView.frame = CGRectInset(contentViewBounds, 1, 1);
  }
  else
  {
    _imageView.frame = contentViewBounds;
  }
}


- (void)dealloc
{
  for (id observer in _slideObservers)
    [_slide removeObserverWithBlockToken:observer];
  _slideObservers = nil;
}


- (void)prepareForReuse
{
  self.slide = nil;

  [_imageView stopLoadingImage];
}


- (void)setSlide:(MZSlide *)slide
{
  if (_slide != slide)
  {
    if (_slide)
    {
      for (id observer in _slideObservers)
        [_slide removeObserverWithBlockToken:observer];
      _slideObservers = nil;
    }
    
    _slide = slide;
    
    if (_slide)
    {
      __weak typeof(self) __self = self;
      _slideObservers = [NSMutableArray array];
      id observer;
      observer = [_slide addObserverForKeyPath:@"imageAvailable" task:^(id object, NSDictionary *change) {
        [__self updateThumbnail];
      }];
      [_slideObservers addObject:observer];

      observer = [_slide addObserverForKeyPath:@"thumbURL" task:^(id object, NSDictionary *change) {
        [__self updateThumbnail];
      }];
      [_slideObservers addObject:observer];

      [self updateThumbnail];
    }
  }
}


- (void)updateThumbnail
{
  if (_slide.image)
  {
    _imageView.image = _slide.image;
    return;
  }
  
  if (_slide.imageAvailable && _slide.thumbURL)
  {
    NSURL *url = [_communicator urlForResource:_slide.thumbURL];
    [_imageView setImageWithURL:url andPlaceholderImage:nil];
  }
  else
  {
    _imageView.image = nil;
  }
}


@end
