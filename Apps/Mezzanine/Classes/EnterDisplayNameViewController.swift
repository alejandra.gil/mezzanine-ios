//
//  EnterDisplayNameViewController.swift
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 17/04/2019.
//  Copyright © 2019 Oblong Industries. All rights reserved.
//

import UIKit

class EnterDisplayNameViewController: UIViewController, UITextFieldDelegate {

  @IBOutlet var welcomeLabel: UILabel!
  @IBOutlet var descriptionLabel: UILabel!
  @IBOutlet var displayNameTextField: UITextField!
  @IBOutlet var continueButton: UIButton!
  @IBOutlet var cancelButton: UIButton!

  var observationTokens = Array<AnyObject>()

  override func viewDidLoad() {
    super.viewDidLoad()

    self.navigationController?.delegate = self

    self.modalPresentationCapturesStatusBarAppearance = true

    let sharedStyle = MezzanineStyleSheet.shared()

    self.view.backgroundColor = sharedStyle?.grey50Color

    welcomeLabel.font = UIFont.dinBold(ofSize: 24)
    welcomeLabel.textColor = sharedStyle?.textNormalColor
    welcomeLabel.text = "Enter Display Name Welcome Screen Title".localized

    descriptionLabel.font = UIFont.dinLight(ofSize: 16)
    descriptionLabel.textColor = sharedStyle?.textNormalColor
    descriptionLabel.text = "Enter Display Name Welcome Screen Description".localized

    displayNameTextField.font = UIFont.din(ofSize: 16.0)
    displayNameTextField.delegate = self
    displayNameTextField.enablesReturnKeyAutomatically = true
    displayNameTextField.clearButtonMode = .whileEditing
    displayNameTextField.text = UserDefaults.standard.displayName
    displayNameTextField.placeholder = "Enter Display Name Welcome Screen Name Textfield Hint".localized

    sharedStyle?.stylizeCall(toActionButton: continueButton)
    continueButton.layer.cornerRadius = 4.0;
    continueButton.clipsToBounds = true;
    continueButton.setTitle("Enter Display Name Welcome Screen Continue Button".localized, for: UIControlState())
    updateContinueButton()

    cancelButton.titleLabel?.font = UIFont.dinMedium(ofSize: 14)
    cancelButton.setTitle("Enter Display Name Welcome Screen Cancel Button".localized, for: UIControlState())
    updateCancelButton()
  }

  override func viewWillAppear(_ animated: Bool) {
    self.navigationController?.setNavigationBarHidden(true, animated: false)

    observationTokens.append(NotificationCenter.default.addObserver(forName: NSNotification.Name.UITextFieldTextDidChange, object: displayNameTextField, queue: OperationQueue.main) { (_) in
      self.updateContinueButton()
    })

    super.viewWillAppear(animated)
  }

  override func viewWillDisappear(_ animated: Bool) {
    for observer in observationTokens {
      NotificationCenter.default.removeObserver(observer)
    }
    observationTokens.removeAll()

    super.viewWillDisappear(animated)
  }

  func updateContinueButton() {
    continueButton.isEnabled = (displayNameTextField.text?.count ?? 0 > 0);
  }

  func updateCancelButton() {
    guard let _ = UserDefaults.standard.displayName else {
      cancelButton.isHidden = true
      return
    }
  }

  @IBAction func continueAction(_ sender: UIButton) {
    UserDefaults.standard.displayName = displayNameTextField.text
    dismiss(animated: true, completion: nil)
  }

  @IBAction func cancelAction(_ sender: UIButton) {
    dismiss(animated: true, completion: nil)
  }

  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)

    if !text.isEmpty {
      continueButton.isUserInteractionEnabled = true
    } else {
      continueButton.isUserInteractionEnabled = false
    }
    return true
  }

  override var preferredStatusBarStyle : UIStatusBarStyle {
    return .lightContent
  }

  // Mark - Adaptative layout

  func shouldAutorotate() -> Bool {
    return self.traitCollection.isiPad() ? true : false
  }

  func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
    return self.traitCollection.isiPad() ? .all : .portrait
  }
}

extension EnterDisplayNameViewController: UINavigationControllerDelegate {
  public func navigationControllerSupportedInterfaceOrientations(_ navigationController: UINavigationController) -> UIInterfaceOrientationMask {
    return supportedInterfaceOrientations()
  }
}
