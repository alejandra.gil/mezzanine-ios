//
//  SlideLayer.h
//  Mezzanine
//
//  Created by Zai Chang on 10/22/10.
//  Copyright 2010 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import "MZLiveStream.h"
#import "VideoPlaceholderLayer.h"


/// Visual representation of a slide

@interface SlideLayer : CALayer 
{
  MZItem *slide;
  NSMutableArray *slideObservers;
  
  CAGradientLayer *backgroundLayer;
  CALayer *contentLayer;
  CATextLayer *centeredTextLayer;
  VideoPlaceholderLayer *placeholderLayer;
  
  BOOL slideImageNeedsRefresh;
  UIImage *slideImage;
  
  CGFloat contentScale;
  CIFilter *contentFilter;
  
  // Actions Panel
  CALayer *actionsPanelLayer;
  CAGradientLayer *contentShadowLayer;
  CALayer *deleteButtonLayer;
  CATextLayer * deleteButtonTextLayer;
  BOOL actionsPanelVisible;
  BOOL actionsPanelNeedsLayout;
  CGFloat actionsPanelAnimationState;
}
@property (nonatomic, strong) MZItem *slide;

@property (nonatomic, assign) BOOL slideImageNeedsRefresh;
@property (nonatomic, strong) UIImage *slideImage;
@property (nonatomic, assign) BOOL isDisplayingFullImage;
@property (nonatomic, assign) BOOL isLoadingFullImage;

@property (nonatomic, strong) MZLiveStream *liveStream;

@property (nonatomic, assign) CGFloat contentScale;
@property (nonatomic, assign) BOOL actionsPanelVisible;
@property (nonatomic, strong, readonly) CALayer *actionsPanelLayer;

@property (nonatomic, assign) CGFloat containerZoomScale;
@property (nonatomic, assign) BOOL indexLayerVisible;

@property (nonatomic, assign) BOOL userInteractionEnabled;

@property (nonatomic, copy) void (^actionPanelWillAppear)(SlideLayer *aSlideLayer);
@property (nonatomic, copy) void (^actionPanelWillDisappear)(SlideLayer *aSlideLayer);
@property (nonatomic, copy) void (^deleteRequested)(SlideLayer *aSlideLayer);

@property (nonatomic, strong) VideoPlaceholderLayer *placeholderLayer;

-(void) updateWithSlide:(MZItem *)slide;
-(void) setFullSlideImage:(UIImage*)image;

-(void) setActionsPanelAnimationState:(CGFloat)state animated:(BOOL)animated;
-(void) setActionsPanelVisible:(BOOL)visible animated:(BOOL)animated;

-(void) setDeletePendingState:(BOOL)deletePending animated:(BOOL)animated;

-(void) deleteSlide:(id)sender;

-(BOOL) actionStripContainsPoint:(CGPoint)location;

@end
