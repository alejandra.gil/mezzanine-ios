//
//  UIImageView+MZDownloadManager.m
//  Mezzanine
//
//  Created by miguel on 09/12/13.
//  Copyright (c) 2013 Oblong Industries. All rights reserved.
//

#import "UIImageView+MZDownloadManager.h"
#import "MZDownloadManager.h"
#import "objc/runtime.h"

@implementation UIImageView (MZDownloadManager)


static char URLKey;

-(void) setImageWithURL:(NSURL *)url andPlaceholderImage:(UIImage *)placeholderImage
{
  self.image = placeholderImage;

  __weak typeof (self) weakSelf = self;

  NSURL *associatedURL = [self getURLAssociatedWithTarget];
  if ([url.absoluteString isEqualToString:associatedURL.absoluteString])
    return;

  [self associateTargetWithURL:url];

  [[MZDownloadManager sharedLoader] loadImageWithURL:url
                                            atTarget:self
                                             success:^(UIImage *image) {
                                               UIImageView *strongSelf = weakSelf;
                                               NSURL *associatedURL = [self getURLAssociatedWithTarget];
                                               BOOL sameURL = ([associatedURL.absoluteString isEqualToString:url.absoluteString]);
                                               if (sameURL)
                                               {
                                                 [strongSelf setImage:image];
                                                 [strongSelf setNeedsDisplay];
                                                 [strongSelf removeURLAssociatedWithTarget];
                                               }
                                             }
                                               error:^(NSError *error) {
                                                 UIImageView *strongSelf = weakSelf;
                                                 if (error.code == kMZDownloadManagerErrorCode_IncorrectDownload)
                                                   [strongSelf setImageWithURL:url andPlaceholderImage:placeholderImage];
                                                 else
                                                   [strongSelf setImage:placeholderImage];
                                                 [strongSelf removeURLAssociatedWithTarget];
                                               }
                                              canceled:^{
                                                UIImageView *strongSelf = weakSelf;
                                                [strongSelf removeURLAssociatedWithTarget];
                                              }];
}

-(void) stopLoadingImage
{
  self.image = nil;
  [self removeURLAssociatedWithTarget];
  [[MZDownloadManager sharedLoader] cancelLoadingURLForTarget:self];
}


-(void)associateTargetWithURL:(NSURL *)url
{
  objc_setAssociatedObject(self, &URLKey, url, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(void)removeURLAssociatedWithTarget
{
  objc_setAssociatedObject(self, &URLKey, nil, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(NSURL *)getURLAssociatedWithTarget
{
  return objc_getAssociatedObject(self, &URLKey);
}

@end
