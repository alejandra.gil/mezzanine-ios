//
//  InfopresenceMenuViewController.swift
//  Mezzanine
//
//  Created by Miguel Sanchez on 29/6/15.
//  Copyright (c) 2015 Oblong Industries. All rights reserved.
//

import UIKit

@objc class InfopresenceMenuViewController: UIViewController
{
  @IBOutlet var navigationBar: UINavigationBar!
  @IBOutlet var participantsSection: UIView!

  let styleSheet = MezzanineStyleSheet.shared()
  weak var menuController: InfopresenceMenuController?
  let participantsInteractor = ParticipantsInteractor()

  let participantsCounterViewController = ParticipantsCounterViewController()
  var participantsTableViewController: ParticipantsBaseTableViewController?

  var communicator: MZCommunicator!
  var systemModel: MZSystemModel!

  init(systemModel: MZSystemModel, communicator: MZCommunicator, menuController aController: InfopresenceMenuController) {
    super.init(nibName: "InfopresenceMenuViewController", bundle: nil)
    self.communicator = communicator
    self.systemModel = systemModel
    menuController = aController
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  deinit {
    participantsInteractor.systemModel = nil

    guard let participantsTableViewController = participantsTableViewController else { return }
    participantsTableViewController.systemModel = nil
    participantsTableViewController.communicator = nil
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = styleSheet?.grey40Color

    let isRegular = menuController?.containerViewController?.traitCollection.isRegularWidth() == true && menuController?.containerViewController?.traitCollection.isRegularHeight() == true
    preferredContentSize = CGSize(width: isRegular ? 320.0 : 300.0, height: CGFloat.greatestFiniteMagnitude);

    // Navigation bar
    navigationBar.backgroundColor = UIColor.clear
    navigationBar.setBackgroundImage(UIImage(), for: .default)
    navigationBar.addSubview(participantsCounterViewController.view)
    participantsCounterViewController.view.frame = navigationBar.bounds


    // Table view
    if systemModel.featureToggles.participantRoster {
      participantsTableViewController = ParticipantsRosterTableViewController()
    } else {
      participantsTableViewController = ParticipantsTableViewController.init(systemModel: systemModel, communicator: communicator)
    }

    participantsSection.addSubview(participantsTableViewController!.tableView)
    participantsTableViewController!.tableView.frame = participantsSection.bounds

    participantsInteractor.delegate = self
    participantsInteractor.systemModel = self.systemModel
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)

    // Analytics
    let analyticsManager = MezzanineAnalyticsManager.sharedInstance
    analyticsManager.tagScreen(analyticsManager.infopresenceViewScreen)
  }
}


extension InfopresenceMenuViewController: ParticipantsInteractorDelegate {
  func participantsInteractorMezzaninesWereUpdated(mezzanines: Array<MZMezzanine>, totalRooms: Int, totalPeople: Int) {
    // Update participants list
    guard let participantsTableViewController = participantsTableViewController else { return }
    participantsTableViewController.updateTableView(mezzanines: mezzanines)

    // Update participants and room count
    participantsCounterViewController.updateParticipantsCount(roomsCount: totalRooms, peopleCount: totalPeople)
  }
}
