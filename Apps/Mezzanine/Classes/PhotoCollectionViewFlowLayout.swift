//
//  PhotoCollectionViewFlowLayout.swift
//  Mezzanine
//
//  Created by miguel on 3/1/17.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import UIKit

struct PhotoCollectionViewFlowLayoutConstants {
  static let itemSize:CGFloat = 78.0
  static let itemSpacing:CGFloat = 2.0
}

class PhotoCollectionViewFlowLayout: UICollectionViewFlowLayout {

  override init() {
    super.init()
    itemSize = CGSize(width: PhotoCollectionViewFlowLayoutConstants.itemSize,height: PhotoCollectionViewFlowLayoutConstants.itemSize)
    sectionInset = UIEdgeInsetsMake(PhotoCollectionViewFlowLayoutConstants.itemSpacing, PhotoCollectionViewFlowLayoutConstants.itemSpacing, PhotoCollectionViewFlowLayoutConstants.itemSpacing, PhotoCollectionViewFlowLayoutConstants.itemSpacing)
    scrollDirection = .vertical
    minimumLineSpacing = PhotoCollectionViewFlowLayoutConstants.itemSpacing
    minimumInteritemSpacing = PhotoCollectionViewFlowLayoutConstants.itemSpacing
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
