//
//  PortfolioAddMenuViewController.m
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 13/04/2017.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

#import "PortfolioAddMenuViewController.h"
#import "Mezzanine-Swift.h"

@interface PortfolioAddMenuViewController()

@property (nonatomic, strong) PortfolioAddMenuInteractor *interactor;

@end


@implementation PortfolioAddMenuViewController

- (instancetype)initWithInteractor:(PortfolioAddMenuInteractor *)interactor
{
  if (self = [super init]) {
    NSMutableArray *items = [NSMutableArray new];
    _interactor = interactor;
    
    MezzanineAnalyticsManager *analytics =  [MezzanineAnalyticsManager sharedInstance];
    
    [items addObject:[ButtonMenuItem itemWithTitle:NSLocalizedString(@"Upload Text Button Title", nil) action:^{
      [_popover dismissPopoverAnimated:NO];
      [_interactor showSlideCreationView];
      
      NSString *analyticsContext = [_fromView isKindOfClass:[UIButton class]] ? analytics.addButtonAttribute : analytics.itemPlaceholderAttribute;
      [analytics tagEvent:analytics.addPortfolioItemEvent attributes:@{analytics.sourceKey : analytics.textAttribute,
                                                                       analytics.contextKey : analyticsContext}];
    }]];
    
    [items addObject:[ButtonMenuItem itemWithTitle:NSLocalizedString(@"Upload Photo From Library Button Title", nil) action:^{
      [_popover dismissPopoverAnimated:NO];
      [_interactor showPhotoLibrary];
      
      NSString *analyticsContext = [_fromView isKindOfClass:[UIButton class]] ? analytics.addButtonAttribute : analytics.itemPlaceholderAttribute;
      [analytics tagEvent:analytics.addPortfolioItemEvent attributes:@{analytics.sourceKey : analytics.libraryAttribute,
                                                                       analytics.contextKey : analyticsContext}];
    }]];
    
    
    BOOL cameraIsAvailable = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
    if (cameraIsAvailable)
      [items addObject:[ButtonMenuItem itemWithTitle:NSLocalizedString(@"Upload New Photo Button Title", nil) action:^{
        [_popover dismissPopoverAnimated:NO];
        [_interactor showCamera];
        
        NSString *analyticsContext = [_fromView isKindOfClass:[UIButton class]] ? analytics.addButtonAttribute : analytics.itemPlaceholderAttribute;
        [analytics tagEvent:analytics.addPortfolioItemEvent attributes:@{analytics.sourceKey : analytics.cameraAttribute,
                                                                         analytics.contextKey : analyticsContext}];
      }]];
    
    if ([_interactor numberOfWhiteboards] > 0)
    {
      BOOL moreThanOneWhiteboard = [_interactor numberOfWhiteboards] > 1 ? YES : NO;
      
      NSString *title = moreThanOneWhiteboard ? NSLocalizedString(@"Whiteboard Capture Multiple Button Title", @"Whiteboard Capture") :
      NSLocalizedString(@"Whiteboard Capture Button Title", @"Whiteboard Capture");
      
      [items addObject:[ButtonMenuItem itemWithTitle:title action:^{
        
        if (moreThanOneWhiteboard)
        {
          [_popover dismissPopoverAnimated:NO];
          [self showWhiteboardCaptureView];
        }
        else
        {
          [_popover dismissPopoverAnimated:YES];
          [_interactor captureWhiteboardAtIndex:0];
        }
        
        NSString *analyticsContext = [_fromView isKindOfClass:[UIButton class]] ? analytics.addButtonAttribute : analytics.itemPlaceholderAttribute;
        [analytics tagEvent:analytics.addPortfolioItemEvent attributes:@{analytics.sourceKey : analytics.whiteboardAttribute,
                                                                         analytics.contextKey : analyticsContext}];
      }]];
    }
    
    self.menuItems = items;
  }
  
  return self;
}


- (void)showWhiteboardCaptureView
{
  WhiteboardCaptureViewController *controller = [[WhiteboardCaptureViewController alloc] initWithNibName:nil bundle:nil];
  controller.systemModel = _interactor.systemModel;
  controller.interactor = [_interactor createWhiteboardInteractor];
  controller.preferredContentSize = CGSizeMake(240.0, 308.0);
  controller.didRequestWhiteboardCapture = ^(NSString *whiteboardUid) {
    [_popover dismissPopoverAnimated:YES];
  };
  
  MezzanineNavigationController *navController = [[MezzanineNavigationController alloc] initWithRootViewController:controller];
  
  FPPopoverController *popover = [[FPPopoverController alloc] initWithContentViewController:navController delegate:nil];
  [popover stylizeAsActionSheet];
  __weak typeof (popover) weakPopover = popover;
  void (^popoverPresentingBlock)() = ^{
    [weakPopover presentPopoverFromRect:_fromView.frame inView:_fromView.superview permittedArrowDirections:FPPopoverArrowDirectionDown animated:YES];
  };
  
  if ([self.traitCollection isRegularWidth])
    popover.popoverPresentingBlock = popoverPresentingBlock;
  
  popoverPresentingBlock();
  
  _popover = popover;
}

@end
