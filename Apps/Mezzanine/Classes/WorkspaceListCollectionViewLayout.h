//
//  WorkspaceListCollectionViewLayout.h
//  Mezzanine
//
//  Created by miguel on 10/03/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WorkspaceListCollectionViewLayout : UICollectionViewFlowLayout
{
  NSMutableArray *insertedIndexPaths;
  NSMutableArray *deletedIndexPaths;
}

@end
