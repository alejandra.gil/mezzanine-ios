//
//  MezzanineConnectionViewController.m
//  Mezzanine
//
//  Created by Zai Chang on 7/11/13.
//  Copyright (c) 2013 Oblong Industries. All rights reserved.
//

#import "MezzanineConnectionViewController.h"
#import "MezzanineConnectionViewController_Private.h"
#import "MezzanineAppContext.h"
#import "AcknowledgementsViewController.h"
#import "MezzanineStyleSheet.h"
#import "OBAppData.h"
#import "UITraitCollection+Additions.h"
#import "NSURL+Additions.h"
#import "NSString+Additions.h"
#import "NSUserDefaults+MezzanineSettings.h"

#import "Mezzanine-Swift.h"

#define ALLOW_OFFLINE_TESTING 1
#define SHOW_ACKNOWLEDGMENTS_IN_APP 0


@interface MezzanineConnectionViewController ()
{
  UITraitCollection *_nextTraitCollection;
  BOOL _recentConnectionShouldRelayout;
}

@end


@implementation MezzanineConnectionViewController

#pragma mark - View lifecycle

- (void)viewDidLoad
{
  [super viewDidLoad];
  [self enableHideKeyboardOnTapOutsideTextFields];
  
  self.view.accessibilityIdentifier = @"ConnectionView";
  self.view.backgroundColor = [MezzanineStyleSheet sharedStyleSheet].grey50Color;
  
  CGRect frame = _poolNameField.frame;
  CGFloat heightDelta = _connectButton.frame.size.height - frame.size.height;
  frame.origin.y -= heightDelta / 2.0;
  frame.size.height += heightDelta;
  _poolNameField.frame = frame;

  _poolNameField.font = [UIFont dinOfSize:16.0];

  [[MezzanineStyleSheet sharedStyleSheet] stylizeCallToActionButton:_connectButton];

  _connectButton.layer.cornerRadius = 4.0;
  _connectButton.clipsToBounds = YES;

  _displayNameLabel.font = [UIFont dinLightOfSize:12.0];
  [self updateDisplayName];

  _oblongLogoImageView.image = [[UIImage imageNamed:@"oblong-logo-white-128.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
  _oblongLogoImageView.tintColor = [MezzanineStyleSheet sharedStyleSheet].grey90Color;
  logoImageViewPortraitFrame = _mezzanineLogoImageView.frame;
  
  _appVersionLabel.textColor = [MezzanineStyleSheet sharedStyleSheet].grey110Color;
  _appVersionLabel.font = [UIFont dinOfSize:12.0];

  _poolNameField.clearButtonMode = UITextFieldViewModeWhileEditing;
  _poolNameField.placeholder = NSLocalizedString(@"Connection Text Placeholder", nil);

  _poolNameField.enablesReturnKeyAutomatically = YES;
  _poolNameField.text = [[NSUserDefaults standardUserDefaults] lastConnectedMezz];
  
  _recentConnectionsButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40.0, 40.0)];
  _recentConnectionsButton.accessibilityLabel = @"ConnectionViewRecentConnectionsButton";
  [_recentConnectionsButton addTarget:self action:@selector(showRecentConnections) forControlEvents:UIControlEventTouchUpInside];
  _recentConnectionsButton.backgroundColor = [UIColor clearColor];
  [_recentConnectionsButton setImage:[UIImage imageNamed:@"mobile-clock-icon"] forState:UIControlStateNormal];
  
  CAShapeLayer *maskLayer = [CAShapeLayer new ];
  maskLayer.path = [UIBezierPath bezierPathWithRoundedRect:_recentConnectionsButton.bounds byRoundingCorners: (UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(5.0, 5.0)].CGPath;
  _recentConnectionsButton.layer.mask = maskLayer;
  
  _poolNameField.leftView = _recentConnectionsButton;
  _poolNameField.leftView.backgroundColor = [[MezzanineStyleSheet sharedStyleSheet] grey40Color];

  if (_appVersionLabel)
    _appVersionLabel.text = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleVersionKey];

#if SHOW_ACKNOWLEDGMENTS_IN_APP
  UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showAcknowledgements:)];
  self.appVersionLabel.userInteractionEnabled = YES;
  [self.appVersionLabel addGestureRecognizer:tapRecognizer];
#endif


#if __DEBUG__ && ALLOW_OFFLINE_TESTING
  UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(offlineTesting:)];
  _appVersionLabel.userInteractionEnabled = YES;
  [_appVersionLabel addGestureRecognizer:tapRecognizer];
#endif

  
  _poolNameField.delegate = self;


#define SHOW_TESTING_BUTTON 0
#if __DEBUG__ && SHOW_TESTING_BUTTON
  CGRect buttonFrame = self.appVersionLabel.frame;
  buttonFrame.origin = CGPointMake(buttonFrame.origin.x, buttonFrame.origin.y - 
24.0);
  UIButton *testButton = [UIButton buttonWithType:UIButtonTypeCustom];
  [testButton setTitle:@"Test" forState:UIControlStateNormal];
  [testButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
  testButton.titleLabel.font = [UIFont systemFontOfSize:13.0];
  testButton.frame = buttonFrame;
  testButton.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin);
  [testButton addTarget:self action:@selector(offlineTesting:) forControlEvents:UIControlEventTouchUpInside];
  [self.view addSubview:testButton];
#endif
}


- (void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];

  self.navigationController.navigationBarHidden = YES;

  if (!observationTokens)
    observationTokens = [NSMutableArray new];

  __weak typeof (self) weakSelf = self;
  [observationTokens addObject:[[NSNotificationCenter defaultCenter] addObserverForName:UIKeyboardWillShowNotification
                                                                                 object:nil
                                                                                  queue:[NSOperationQueue mainQueue]
                                                                             usingBlock:^(NSNotification *notification) {
                                                                               [weakSelf updateViewWithKeyboardVisibility:YES];
                                                                             }]];

  [observationTokens addObject:[[NSNotificationCenter defaultCenter] addObserverForName:UIKeyboardWillHideNotification
                                                                                 object:nil
                                                                                  queue:[NSOperationQueue mainQueue]
                                                                             usingBlock:^(NSNotification *notification) {
                                                                               [weakSelf updateViewWithKeyboardVisibility:NO];
                                                                             }]];

  [observationTokens addObject:[[NSNotificationCenter defaultCenter] addObserverForName:UITextFieldTextDidChangeNotification
                                                                                 object:_poolNameField
                                                                                  queue:[NSOperationQueue mainQueue]
                                                                             usingBlock:^(NSNotification *notification) {
                                                                               [weakSelf updateConnectButton];
                                                                             }]];
 
  [observationTokens addObject:[[NSNotificationCenter defaultCenter] addObserverForName:UIDeviceOrientationDidChangeNotification
                                                                                 object:nil
                                                                                  queue:[NSOperationQueue mainQueue]
                                                                             usingBlock:^(NSNotification *notification) {
                                                                               if ([[UIDevice currentDevice] isIOS10OrAbove])
                                                                                 [UIApplication sharedApplication].statusBarOrientation = UIInterfaceOrientationPortrait;
                                                                             }]];
  
  
  // The purpose of this observer is to notify when the user enters a wrong passkey to send the event
  [observationTokens addObject:[[NSNotificationCenter defaultCenter] addObserverForName:MZCommunicatorIncorrectPassphraseNotification
                                                                                 object:nil
                                                                                  queue:[NSOperationQueue mainQueue]
                                                                             usingBlock:^(NSNotification *notification) {
                                                                               if (notification.userInfo[@"error"])
                                                                                 [weakSelf connectFailed:notification.userInfo[@"error"]];
                                                                             }]];

  [self setInterfaceEnabled:YES];
  
  [self updateHistoryButton];
  [self updateConnectButton];

  if (![[NSUserDefaults standardUserDefaults] displayName])
    [self showEnterDisplayName:NO];
  else
    [self updateDisplayName];
}


- (void)viewDidAppear:(BOOL)animated
{
  [super viewDidAppear:animated];
  
  // Analytics
  MezzanineAnalyticsManager *analyticsManager = [MezzanineAnalyticsManager sharedInstance];
  [analyticsManager tagScreen:analyticsManager.connectionViewScreen];
}


- (void)viewWillDisappear:(BOOL)animated
{
  [super viewWillDisappear:animated];

  for (id observer in observationTokens)
    [[NSNotificationCenter defaultCenter] removeObserver:observer];
  [observationTokens removeAllObjects];

  if ([recentConnectionsPopover isPopoverVisible])
  {
    [recentConnectionsPopover dismissPopoverAnimated:NO];
    [self popoverControllerDidDismissPopover:recentConnectionsPopover];
  }
}


- (BOOL)prefersStatusBarHidden
{
  return NO;
}


- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation
{
  return UIStatusBarAnimationFade;
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
  return  UIStatusBarStyleLightContent;
}

#pragma mark - Enter Display Name

- (void)showEnterDisplayName:(BOOL)animated
{
  EnterDisplayNameViewController *enterDisplayNameVC = [EnterDisplayNameViewController new];
  UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:enterDisplayNameVC];
  [self presentViewController:navController animated:animated completion:nil];
}

- (void)updateDisplayName
{
  NSString *displayName = [[NSUserDefaults standardUserDefaults] displayName];
  NSString *displayNameIsYou = [NSString stringWithFormat:NSLocalizedString(@"Connection Screen Welcome User", nil), displayName ? displayName : @""];
  _displayNameLabel.text = displayNameIsYou;
  NSDictionary *attributes = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle),
                              NSFontAttributeName: [UIFont dinLightOfSize:12.0],
                              NSForegroundColorAttributeName: [UIColor whiteColor]};
  NSAttributedString *editNameText = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Connection Screen Switch User", nil) attributes:attributes];
  [_editDisplayNameButton setAttributedTitle:editNameText forState:UIControlStateNormal];
}


#pragma mark - Adaptative layout

- (BOOL)shouldAutorotate
{
  return self.traitCollection.isiPad ? YES : NO;
}


- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
  return self.traitCollection.isiPad ? UIInterfaceOrientationMaskAll : UIInterfaceOrientationMaskPortrait;
}


- (void)willTransitionToTraitCollection:(UITraitCollection *)newCollection withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
  [super willTransitionToTraitCollection:newCollection withTransitionCoordinator:coordinator];
  
  _nextTraitCollection = newCollection;
  
  if (self.presentedViewController || recentConnectionsPopover) {
    if (self.presentedViewController == recentConnectionsViewController) {
      _recentConnectionShouldRelayout = YES;
      [self.presentedViewController dismissViewControllerAnimated:NO completion:nil];
  } else if (recentConnectionsPopover) {
      _recentConnectionShouldRelayout = YES;
      [recentConnectionsPopover dismissPopoverAnimated:NO];
    }
  }
}


- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
  if (self.navigationController.visibleViewController != self) {
    return;
  }
  
  [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
  
  if (_nextTraitCollection) {
    return;
  }
  
  if (self.presentedViewController || recentConnectionsPopover) {
    
    if (self.presentedViewController)
      [self.presentedViewController dismissViewControllerAnimated:NO completion:nil];
    
    if (recentConnectionsPopover)
      [recentConnectionsPopover dismissPopoverAnimated:NO];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
      _recentConnectionShouldRelayout = NO;
      [self showRecentConnectionsAnimated:YES];
    });
  }
}


- (void)traitCollectionDidChange:(UITraitCollection *)previousTraitCollection
{
  if (self.navigationController.visibleViewController != self) {
    return;
  }
  
  [super traitCollectionDidChange:previousTraitCollection];
  
  _nextTraitCollection = nil;
  
  dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    if (_recentConnectionShouldRelayout) {
      _recentConnectionShouldRelayout = NO;
      [self showRecentConnectionsAnimated:YES];
    }
  });
}


#pragma mark - URL-based connection

- (void)connectToServerAtURL:(NSURL*)url from:(NSString *)source
{
  if (![_delegate respondsToSelector:@selector(connectionViewController:requestsAsyncConnectionAttemptTo:success:error:)])
    return;
  
  _connectionSource = source;
  
  // Show alertView and update UI
  [self configureAndShowConnectionAlertViewWithURL:url];
  [self setInterfaceEnabled:NO];
  
  __weak typeof(self) __self = self;
  __weak MezzanineAppContext *__weakAppContext = [MezzanineAppContext currentContext];

  [_delegate connectionViewController:self
     requestsAsyncConnectionAttemptTo:[url lowCaseURL]
                              success:^{
                                
                                NSURL *sanitizedURL = [NSURL URLWithString:[url.absoluteString sanitizeStringAsServerName]];
                                
                                if (_connectionAlertController)
                                {
                                  [_connectionAlertController dismissViewControllerAnimated:YES completion:nil];
                                  _connectionAlertController = nil;
                                }
                                
                                if ([[NSThread currentThread] isMainThread])
                                  [self connectSucceeded:sanitizedURL];
                                else
                                  [self performSelectorOnMainThread:@selector(connectSucceeded:) withObject:sanitizedURL waitUntilDone:NO];

                                if (![__weakAppContext.currentCommunicator isARemoteParticipationConnection])
                                  [[OBAppData sharedData] noteRecentPool:url.host];
                                
                              }
                                error:^(NSError *error) {
                                  [__self setInterfaceEnabled:YES];
                                }];
}


- (void)configureAndShowConnectionAlertViewWithURL:(NSURL *)url
{
  __weak MezzanineConnectionViewController *__self = self;


  UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Connection In Process Dialog Title", nil), url.host]
                                                                           message:nil
                                                                    preferredStyle:UIAlertControllerStyleAlert];
  UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Connection In Process Dialog Cancel Button Title", nil)
                                                     style:UIAlertActionStyleCancel
                                                   handler:^(UIAlertAction * _Nonnull action) {
                                                     if ([__self.delegate respondsToSelector:@selector(connectionViewControllerWasCancelled:)])
                                                       [__self.delegate connectionViewControllerWasCancelled:__self];
                                                     
                                                     [__self setInterfaceEnabled:YES];
                                                     __self.connectionAlertController = nil;
                                                   }];
  [alertController addAction:cancelAction];
  [self presentViewController:alertController animated:YES completion:nil];
  _connectionAlertController = alertController;
}


#pragma mark - Connection Process

- (void)beginConnect:(NSString *)poolName
{
  if ([_delegate respondsToSelector:@selector(connectionViewController:requestsAsyncConnectionAttemptTo:success:error:)])
  {
    id errorHandler = ^(NSError *error)
    {
      if ([[NSThread currentThread] isMainThread])
        [self connectFailed:error];
      else
        [self performSelectorOnMainThread:@selector(connectFailed:) withObject:error waitUntilDone:NO];
    };
    id successHandler = ^{
      if ([[NSThread currentThread] isMainThread])
        [self connectSucceeded:[NSURL URLWithString:poolName]];
      else
        [self performSelectorOnMainThread:@selector(connectSucceeded:) withObject:[NSURL URLWithString:poolName] waitUntilDone:NO];
    };
    
    [_delegate connectionViewController:self requestsAsyncConnectionAttemptTo:[NSURL httpsSchemeWithHostname:poolName] success:successHandler error:errorHandler];
  }
}


- (void)connectSucceeded:(NSURL *)url
{
  MezzanineAppContext *appContext = [MezzanineAppContext currentContext];
  [appContext cleanupOrLeaveCacheForURL:url];

  // Analytics  
  NSString *mezzVersion = appContext.applicationModel.systemModel.myMezzanine.version ? appContext.applicationModel.systemModel.myMezzanine.version : @"";
  NSString *apiVersion = appContext.applicationModel.systemModel.apiVersion ? appContext.applicationModel.systemModel.apiVersion : @"";
  BOOL isRemote = [appContext.currentCommunicator isARemoteParticipationConnection];
  NSString *passPhraseValue = appContext.attemptingToConnectWithPassphrase ? @"Enabled" : @"Disabled";
  NSString *source = _connectionSource ? _connectionSource : @"";

  [[MezzanineAnalyticsManager sharedInstance] trackConnectionEventWithSource:source mezzVersion:mezzVersion isRemote:isRemote passkeyState:passPhraseValue apiVersion:apiVersion];

  if (![appContext.currentCommunicator isARemoteParticipationConnection])
    [[OBAppData sharedData] noteRecentPool:_poolNameField.text];

  _poolNameField.text = url.absoluteString;
  
  [self setInterfaceEnabled:YES];
  
  [_poolNameField resignFirstResponder];
  
  [self updateApplicationShortcutItems];
}


- (void)connectFailed:(NSError *)error
{
  [self setInterfaceEnabled:YES];
  
  // Analytics
  if (!error || !error.code || !error.domain)
    return;
  
  MezzanineAnalyticsManager *analytics = [MezzanineAnalyticsManager sharedInstance];
  NSString *errorString = @"";
  switch (error.code) {
    case NSURLErrorCannotFindHost:
      errorString = analytics.hostNotFoundAttribute;
      break;

      // Mezz-In host error
    case 2132:
      errorString = analytics.hostNotFoundAttribute;
      break;

    case kMZCommunicatorErrorCode_ConnectionTimeout:
      errorString = analytics.timeoutAttribute;
      break;

    case kMZCommunicatorErrorCode_LoginTimeout:
      errorString = analytics.timeoutAttribute;
      break;

    case kMZCommunicatorErrorCode_InvalidServerCertificate:
      errorString = analytics.certificateErrorAttribute;
      break;

    case kMZCommunicatorErrorCode_JoinDenied:
      errorString = analytics.incorrectPassphraseAttribute;

    default:
      break;
  }

  [analytics tagEvent:analytics.connectionErrorEvent attributes:@{analytics.sourceKey : _connectionSource ? : @"",
                                                             analytics.errorKey : errorString}];
}


#pragma mark - 3D Touch

- (void)updateApplicationShortcutItems
{
  if(![UIApplicationShortcutItem class]){
    return;
  }
  
  NSMutableArray <UIApplicationShortcutItem *> *recentPoolConnectionsArray = [NSMutableArray new];
  UIApplicationShortcutIcon *iconImage = [UIApplicationShortcutIcon iconWithTemplateImageName:@"mobile-this-mezz-icon-header"];
  for (NSString *poolName in [[OBAppData sharedData] recentPools]) {
    UIMutableApplicationShortcutItem *item = [[UIMutableApplicationShortcutItem alloc] initWithType:@"connection"
                                                                                     localizedTitle:poolName
                                                                                  localizedSubtitle:@""
                                                                                               icon:iconImage
                                                                                           userInfo:@{}];
    [recentPoolConnectionsArray addObject:item];
  }
  
  [[UIApplication sharedApplication] setShortcutItems: recentPoolConnectionsArray];
}


#pragma mark - Actions

- (IBAction)connect
{
  // Analytics
  // If connectionSource is set means we selected it from recent servers
  if (!_connectionSource)
    _connectionSource = [MezzanineAnalyticsManager sharedInstance].connectionViewAttribute;

  _poolNameField.text = [self sanitizePoolname];

  if ([recentConnectionsPopover isPopoverVisible])
  {
    [recentConnectionsPopover dismissPopoverAnimated:NO];
    [self popoverControllerDidDismissPopover:recentConnectionsPopover];
  }

  [self setInterfaceEnabled:NO];

  [self performSelectorInBackground:@selector(beginConnect:) withObject:_poolNameField.text];
}


- (IBAction)cancel
{
  if ([_delegate respondsToSelector:@selector(connectionViewControllerWasCancelled:)])
    [_delegate connectionViewControllerWasCancelled:self];
}


- (void)connectTo:(NSString *)name
{
  [self setInterfaceEnabled:NO];
  [self performSelectorInBackground:@selector(beginConnect:) withObject:name];
}


- (void)showRecentConnections
{
  [self showRecentConnectionsAnimated:YES];
}


- (IBAction)showAcknowledgements:(id)sender
{
  AcknowledgementsViewController *controller = [[AcknowledgementsViewController alloc] initWithNibName:nil bundle:nil];
  MezzanineNavigationController *navController = [[MezzanineNavigationController alloc] initWithRootViewController:controller];
  navController.modalPresentationStyle = UIModalPresentationFormSheet;
  [self presentViewController:navController animated:YES completion:nil];
}


- (IBAction)offlineTesting:(id)sender
{
  if ([sender isKindOfClass:[UIGestureRecognizer class]])
       sender = [sender view];
  
  UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Choose Test", nil) message:nil preferredStyle:UIAlertControllerStyleActionSheet];

  UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Generic Button Title Cancel", nil) style:UIAlertActionStyleCancel handler:nil];

  UIAlertAction *passphraseAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Passphrase", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    [[MezzanineAppContext currentContext] loadPassphraseView];
  }];
  UIAlertAction *singleAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Workspace Single", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    [[MezzanineAppContext currentContext] loadTestWorkspaceSingleFeld];
  }];
  UIAlertAction *doubleAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Workspace Diptych", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    [[MezzanineAppContext currentContext] loadTestWorkspaceDiptych];
  }];
  UIAlertAction *tripleAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Workspace Triptych", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    [[MezzanineAppContext currentContext] loadTestWorkspaceTriptych];
  }];
  UIAlertAction *threeByTwoAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Workspace Three By Two", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    [[MezzanineAppContext currentContext] loadTestWorkspaceThreeByTwo];
  }];

  [actionSheet addAction:cancelAction];
  [actionSheet addAction:passphraseAction];
  [actionSheet addAction:singleAction];
  [actionSheet addAction:doubleAction];
  [actionSheet addAction:tripleAction];
  [actionSheet addAction:threeByTwoAction];

  UIPopoverPresentationController *popPresenter = [actionSheet popoverPresentationController];
  popPresenter.sourceView = [sender superview];
  popPresenter.sourceRect = [sender frame];

  [self presentViewController:actionSheet animated:YES completion:nil];
}

- (IBAction)editDisplayName:(id)sender
{
  [self showEnterDisplayName:YES];
}


#pragma mark - Private

- (NSString *)sanitizePoolname
{
  // In case the hostname already includes a protocol
  // BUG 16767
  return [_poolNameField.text sanitizeStringAsServerName];
}


- (void)setInterfaceEnabled:(BOOL)enabled
{
  if (enabled)
  {
    [_connectButton setTitle:NSLocalizedString(@"Connect Button Not Connected", nil) forState:UIControlStateNormal];
    [_connectButton setTitle:NSLocalizedString(@"Connect Button Not Connected", nil) forState:UIControlStateDisabled];
    [_activityIndicator stopAnimating];
  }
  else
  {
    [_connectButton setTitle:NSLocalizedString(@"Connect Button Connecting", nil) forState:UIControlStateNormal];
    [_connectButton setTitle:NSLocalizedString(@"Connect Button Connecting", nil) forState:UIControlStateDisabled];
    [_activityIndicator startAnimating];
  }
  
  // To avoid showing the active (and red) button while the view is pushed after connecting.
  BOOL enabledAndDisconnected = enabled && [MezzanineAppContext currentContext].applicationModel.systemModel.state == MZSystemStateNotConnected;
  _poolNameField.enabled = enabledAndDisconnected;
  _recentConnectionsButton.enabled = enabledAndDisconnected;
  _connectButton.enabled = enabledAndDisconnected;
  _appVersionLabel.userInteractionEnabled = enabledAndDisconnected;
}


- (void)showRecentConnectionsAnimated:(BOOL)animated
{
  recentConnectionsViewController = [[OBRecentConnectionsViewController alloc] initWithNibName:nil bundle:nil];
  recentConnectionsViewController.recentConnections = [[OBAppData sharedData] recentPools];
  recentConnectionsViewController.delegate = self;
  
  if (self.traitCollection.isRegularHeight && self.traitCollection.isRegularWidth)
  {
    FPPopoverController *fpPopover = [[FPPopoverController alloc] initWithContentViewController:recentConnectionsViewController delegate:self];
    [fpPopover stylizeBorderlessWithArrowColor:[[MezzanineStyleSheet sharedStyleSheet] grey20Color]];
    [fpPopover presentPopoverFromRect:_recentConnectionsButton.frame inView:[_recentConnectionsButton superview] permittedArrowDirections:FPPopoverArrowDirectionUp animated:animated];
    
    // We can do this because FPPopoverController has identical methods
    recentConnectionsPopover = fpPopover;
    
    // The keyboard takes up too much space ...
    if (self.poolNameField.isFirstResponder)
      [self.poolNameField resignFirstResponder];
    
  }
  else
  {
    PortraitNavigationController *navController = [[PortraitNavigationController alloc] initWithRootViewController:recentConnectionsViewController];
    [self presentViewController:navController animated:animated completion:nil];
  }
}


- (void)updateViewWithKeyboardVisibility:(BOOL)visible
{
  CGFloat frameOriginY = 0.0;
  
  if (self.traitCollection.isRegularHeight && self.traitCollection.isRegularWidth)
  {
    if (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation]))
      frameOriginY = visible ? -128.0 : 0.0;
  }
  else
  {
    if (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation]))
      frameOriginY = visible ? -60.0 : 0.0;
    else
      frameOriginY = visible ? -24.0 : 0.0;
  }
  
  [UIView beginAnimations:nil context:nil];
  
  CGRect viewFrame = self.view.frame;
  viewFrame.origin.y = frameOriginY;
  self.view.frame = viewFrame;
  
  [UIView commitAnimations];
}


- (void)updateHistoryButton
{
  OBAppData *appData = [OBAppData sharedData];
  
  [UIView animateWithDuration:0.2 animations:^{
    _recentConnectionsButton.alpha = [appData.recentPools count] == 0 ? 0.0 : 1.0;
   } completion:^(BOOL finished) {
     _recentConnectionsButton.hidden = [appData.recentPools count] == 0;
     _poolNameField.leftViewMode =  [appData.recentPools count] == 0 ? UITextFieldViewModeNever : UITextFieldViewModeAlways;
   }];

}


- (void)updateConnectButton
{
  self.connectButton.enabled = (self.poolNameField.text.length > 0);
}


#pragma mark - RecentConnections

- (void)recentConnectionSelected:(NSString*)pool
{
  // Analytics
  _connectionSource = [MezzanineAnalyticsManager sharedInstance].connectionHistoryAttribute;

  _poolNameField.text = pool;

  if (recentConnectionsPopover)
  {
    [recentConnectionsPopover dismissPopoverAnimated:NO];
    [self popoverControllerDidDismissPopover:recentConnectionsPopover];
    // Automatically begin connection when a recent pool is selected
    [self connect];
  }
  else
  {
    [self dismissViewControllerAnimated:YES completion:^{
      // Automatically begin connection when a recent pool is selected
      [self connect];
    }];
  }
}


- (void)recentConnectionsCancelled
{
  if (recentConnectionsPopover)
  {
    [recentConnectionsPopover dismissPopoverAnimated:YES];
  }
  else
  {
    [self dismissViewControllerAnimated:YES completion:nil];
  }
}


- (void)recentConnectionsCleared
{
  [self recentConnectionsCancelled];
  [self updateHistoryButton];
  [self updateApplicationShortcutItems];
}



#pragma mark - FPPopoverControllder Delegate

- (void)popoverControllerDidDismissPopover:(FPPopoverController *)popoverController
{
  if (popoverController == recentConnectionsPopover)
  {
    recentConnectionsPopover = nil;
  }
}


#pragma mark - UITextFieldDelegate

// called when 'return' key pressed. return NO to ignore
- (BOOL)textFieldShouldReturn:(UITextField *)textField;
{
  [self connect];
  return YES;
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
  // In certain circumstances it seem the main window loses focus (bug 10893)
  // And in such a case even though the cursor is visible in the text field it won't respond to keyboard input
  // Solution was taken from an alternative answer at  http://stackoverflow.com/questions/12447875/keyboard-and-cursor-show-but-i-cant-type-inside-uitextfields-and-uitextviews
  if (!textField.window.isKeyWindow)
    [textField.window makeKeyAndVisible];
  return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
  if ([string isEqualToString:[UIPasteboard generalPasteboard].string])
  {
    textField.text = [string sanitizeStringAsServerName];
    [self updateConnectButton];
    return NO;
  }

  return YES;
}

@end
