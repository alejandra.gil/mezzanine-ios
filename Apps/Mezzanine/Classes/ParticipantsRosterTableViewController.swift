//
//  ParticipantRosterTableViewController.swift
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 10/2/18.
//  Copyright (c) 2018 Oblong Industries. All rights reserved.
//

import UIKit

class ParticipantsRosterTableViewController: ParticipantsBaseTableViewController
{
  var mezzanines = Array<MZMezzanine>()
  let styleSheet = MezzanineStyleSheet.shared()
  
  deinit {
    mezzanines.removeAll()
  }
  
  // MARK: - View Cycle
  
  fileprivate let participantCellIdentifier = "ParticipantsRosterTableViewCell"
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    tableView.backgroundColor = UIColor.clear
    tableView.separatorStyle = .none
    tableView.rowHeight = 35.0
    tableView.register(UINib(nibName: participantCellIdentifier, bundle:nil), forCellReuseIdentifier: participantCellIdentifier)
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    tableView.reloadData()
  }

  func sortRoomsAlphabetically() {
    mezzanines = mezzanines.sorted(by:  {
      if $0.roomType.rawValue != $1.roomType.rawValue {
        return $0.roomType.rawValue < $1.roomType.rawValue
      } else {
        return $0.name < $1.name
      }
    })
  }

  override func updateTableView(mezzanines: Array<MZMezzanine>) {
    self.mezzanines = mezzanines

    sortRoomsAlphabetically()
    tableView.reloadData()
  }
  
  
  // MARK: - Table view data source
  override func numberOfSections(in tableView: UITableView) -> Int {
    return mezzanines.count
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return mezzanines[section].participants.count
  }
  
  override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 40.0
  }
  
  override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let headerView = UIView(frame: CGRect(x: CGFloat(0.0), y: CGFloat(0.0), width: self.tableView.frame.width, height: 40.0))
    let titleLabel = UILabel(frame: CGRect(x: 24.0, y: 16.0, width: 200, height: 20.0))

    headerView.backgroundColor = styleSheet?.grey40Color
    let room = mezzanines[section]
    titleLabel.text = room.name;
    
    if room.roomType == MZMezzanineRoomType.cloud {
      titleLabel.text = "Participants Remote Header".localized;
    }
    
    styleSheet?.stylizeParticipantSectionHeaderLabel(titleLabel)
    headerView.addSubview(titleLabel)
    
    return headerView
  }
  
  override func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
    return false
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: participantCellIdentifier, for: indexPath) as! ParticipantsRosterTableViewCell
    
    let mezzanine = mezzanines[indexPath.section]
    cell.roomType = mezzanine.roomType
    cell.participant = mezzanine.participants.sorted(by: { ($0 as! MZParticipant).displayName < ($1 as! MZParticipant).displayName })[indexPath.row] as? MZParticipant
    return cell
  }

  
  // MARK: - Adaptative layout
  override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
    for (_, viewController) in (navigationController?.viewControllers.enumerated())! {
      if viewController == self {
        navigationController?.popViewController(animated: false)
        break
      }
    }
  }
}
