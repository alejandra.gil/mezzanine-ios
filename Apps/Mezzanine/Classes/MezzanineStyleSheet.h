//
//  MezzanineStyleSheet.h
//  Mezzanine
//
//  Created by Zai Chang on 5/25/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import "OBStyleSheet.h"


@interface MezzanineStyleSheet : OBStyleSheet

// Mezzanine version
@property (nonatomic, strong) NSString *version;

// Color properties
@property (nonatomic, strong) UIColor *defaultSystemBlue;
@property (nonatomic, strong) UIColor *defaultHoverColor;
@property (nonatomic, strong) UIColor *defaultHighlightColor;

@property (nonatomic, strong) UIColor *mediumFillColor;
@property (nonatomic, strong) UIColor *darkFillColor;
@property (nonatomic, strong) UIColor *darkerFillColor;
@property (nonatomic, strong) UIColor *darkestFillColor;

@property (nonatomic, strong) UIColor *textNormalColor;
@property (nonatomic, strong) UIColor *textHighlightedColor;
@property (nonatomic, strong) UIColor *textSecondaryColor;

@property (nonatomic, strong) UIColor *buttonBorderColor;
@property (nonatomic, strong) UIColor *buttonHighlightBackgroundColor;
@property (nonatomic, strong) UIImage *buttonSelectedBackgroundImage;
@property (nonatomic, strong) UIImage *buttonDisabledBackgroundImage;

@property (nonatomic, strong) UIColor *feldMutedBorderColor;
@property (nonatomic, strong) UIColor *feldBorderInCollaborationColor;

@property (nonatomic, strong) UIColor *slideBackgroundColor;
@property (nonatomic, strong) UIColor *slideBorderColor;
@property (nonatomic, strong) UIColor *assetBackgroundColor;
@property (nonatomic, strong) UIColor *shielderBorderColor;
@property (nonatomic, strong) UIColor *shielderBackgroundColor;

@property (nonatomic, strong) UIColor *ovumBackgroundColor;
@property (nonatomic, strong) UIColor *ovumHoverHighlightColor; // Used during drag and drop when an ovum hovers over a valid drop target

@property (nonatomic, strong) UIColor *videoPlaceholderBackgroundColor;
@property (nonatomic, strong) UIColor *videoPlaceholderPrimaryColor;
@property (nonatomic, strong) UIColor *videoPlaceholderSecondaryColor;

@property (nonatomic, strong) UIColor *superHighlightColor;
@property (nonatomic, strong) UIColor *superHighlightColorActive; // For the active / pressed state

@property (nonatomic, strong) UIColor *greenHighlightColor;
@property (nonatomic, strong) UIColor *yellowHighlightColor;
@property (nonatomic, strong) UIColor *redHighlightColor;
@property (nonatomic, strong) UIColor *redHighlightColorPress;

@property (nonatomic, strong) UIColor *selectedBlueColor;
@property (nonatomic, strong) UIColor *horizonBlueColor;
@property (nonatomic, strong) UIColor *horizonBlueColorActive;
@property (nonatomic, strong) UIColor *darkHorizonBlueColor;
@property (nonatomic, strong) UIColor *darkHorizonBlueColorDisabled;
@property (nonatomic, strong) UIColor *lightBlueRoomColor;

@property (nonatomic, strong) UIColor *declineActionColorNormal;
@property (nonatomic, strong) UIColor *declineActionColorPress;

@property (nonatomic, strong) UIColor *greenIndicatorColor;

@property (nonatomic, strong) UIColor *grey10Color;
@property (nonatomic, strong) UIColor *grey20Color;
@property (nonatomic, strong) UIColor *grey30Color;
@property (nonatomic, strong) UIColor *grey40Color;
@property (nonatomic, strong) UIColor *grey50Color;
@property (nonatomic, strong) UIColor *grey60Color;
@property (nonatomic, strong) UIColor *grey70Color;
@property (nonatomic, strong) UIColor *grey80Color;
@property (nonatomic, strong) UIColor *grey90Color;
@property (nonatomic, strong) UIColor *grey110Color;
@property (nonatomic, strong) UIColor *grey130Color;
@property (nonatomic, strong) UIColor *grey150Color;
@property (nonatomic, strong) UIColor *grey170Color;
@property (nonatomic, strong) UIColor *grey190Color;

// Generated images for interface styling
@property (nonatomic, strong, readonly) UIImage *navigationBarBackgroundImage;
@property (nonatomic, strong, readonly) UIImage *buttonBackgroundImage;
@property (nonatomic, strong, readonly) UIImage *buttonHighlightedBackgroundImage;
@property (nonatomic, strong, readonly) UIImage *segmentedControlSeparatorImage;
@property (nonatomic, strong, readonly) UIImage *backButtonBackgroundImage;
@property (nonatomic, strong, readonly) UIImage *backButtonHighlightedBackgroundImage;

+(MezzanineStyleSheet*) sharedStyleSheet;

- (CGFloat)buttonFontSize;
- (CGFloat) buttonSmallFontSize;
- (UIImage*)imageWithTTStyle:(TTStyle*)style size:(CGSize)size;

// Styles
- (TTStyle*)navigationBarButton:(UIControlState)state;
- (TTStyle*)highlightedRoundedButton:(UIControlState)state;
- (TTStyle*)tableViewCellAccessoryButton:(UIControlState)state;
- (TTStyle*)sliderTrackStyle:(UIControlState)state;
- (TTStyle*)sliderThumbStyle:(UIControlState)state;
- (TTStyle*)darkTopBorderedButton:(UIControlState)state;
- (TTStyle*)lightTopBorderedButton:(UIControlState)state;

// Mezzanine Blue Highlighting buttons
- (TTStyle*)blueHightlightedToolbarButton:(UIControlState)state;

// Feld Switcher
- (TTStyle*)feldSwitcherButton:(UIControlState)state;

// Font styling methods
- (void)stylizeHeaderTitleLabel:(UILabel*)label;
- (void)stylizeSectionHeaderLabel:(UILabel*)label;
- (void)stylizeParticipantSectionHeaderLabel:(UILabel*)label;
- (void)stylizeLabel:(UILabel*)label withPanelTitle:(NSString*)title; // For headers of panels such as 'THIS MEZZANINE'
- (void)stylizeLabel:(UILabel*)label withTitle:(NSString*)title; // For headers of panels such as 'SAVED WORKSPACES'

// Informative styles are for text views that appears in menu panels
- (void)stylizeInformativeTitleLabel:(UILabel*)label;
- (void)stylizeInformativeDescriptionLabel:(UILabel*)label;

// Informative styles that appears in white popovers
- (void)stylizeInformativeTitleLabelInWhitePopover:(UILabel*)label;
- (void)stylizeInformativeDescriptionLabelInWhitePopover:(UILabel*)label;

// Informative lighter modified styles that appears in white popovers
- (void)stylizeInformativeLightTitleLabel:(UILabel*)label;
- (void)stylizeInformativeLightDescriptionLabel:(UILabel*)label;

// Dim informative styles are for views such as "No Mezzanines Available"
- (void)stylizeDimInformativeTitleLabel:(UILabel*)label;
- (void)stylizeDimInformativeDescriptionLabel:(UILabel*)label;
- (void)stylizeButton:(UIButton*)button;
- (void)stylizeButtonForWhitePopover:(UIButton*)button;
- (void)stylizeSmallFontButton:(UIButton*)button;
- (void)stylizeToolbarButton:(UIButton*)button;
- (void)stylizeCallToActionButton:(UIButton*)button;	// Call to action buttons are super-highlighted button
- (void)stylizeCallToActionGrayButton:(UIButton*)button;	// Call to action buttons with gray button
- (void)stylizeDeclineActionButton:(UIButton *)button; // Decline or Disconnect action buttons
- (void)stylizeCancelActionButton:(UIButton*)button;
- (void)stylizeButtonSectionView:(UIButton *)button; // Used in side-to-side label button with changing background in menu panels
- (void)stylizeWithTopAndBottomBorders:(UIView *)aView;
- (void)stylizeCancelButtonPopover:(UIButton *)button; // Recent connections popover, for iPad
- (void)stylizeActionBarButton:(UIButton*)button;

@end
