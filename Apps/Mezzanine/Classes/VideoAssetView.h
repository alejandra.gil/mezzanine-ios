//
//  VideoAssetView.h
//  Mezzanine
//
//  Created by Miguel Sánchez Valdés on 3/12/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import "AssetView.h"
#import "VideoPlaceholderView.h"

@interface VideoAssetView : AssetView

@property (nonatomic, strong) MZLiveStream *liveStream;

@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong, readonly) VideoPlaceholderView *placeholderView;

@property (nonatomic, assign) CGFloat maxTitleFontSize;
@property (nonatomic, assign) CGFloat minTitleFontSize;
@property (nonatomic, assign) CGFloat minSubtitleFontSize;

@end
