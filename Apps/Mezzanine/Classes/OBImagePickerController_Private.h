//
//  OBImagePickerController_Private.h
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 24/02/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

#import "OBImagePickerController.h"
#import "OBImagePickerErrorAccessView.h"


@interface OBImagePickerController ()

+ (ALAssetsLibrary *)defaultAssetsLibrary;
@property (nonatomic, copy) id listGroupBlock;
@property (nonatomic, copy) ALAssetsLibraryAccessFailureBlock failureBlock;
@property (nonatomic, strong) OBImagePickerErrorAccessView *errorView;

@end
