//
//  PhotoAlbumTableViewCell.swift
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 09/01/2017.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import UIKit

class PhotoAlbumTableViewCell: UITableViewCell {
  var coverSize: CGSize = CGSize.zero
  
  var album: PhotoAlbum! {
    didSet {
      if album == nil {
        self.imageView!.image = UIImage(named: "PhotoPickerAlbumPlaceholder")
        self.textLabel!.text = ""
        self.detailTextLabel!.text = ""
      } else {
        if album.coverPhoto == nil {
          self.imageView!.image = UIImage(named: "PhotoPickerAlbumPlaceholder")
          PhotoManager.sharedInstance.getCoverFromAlbum(album, size: coverSize) { (image, album) in
            if album != self.album {
              return
            }
            self.album.coverPhoto = image ?? nil
            self.imageView!.image = self.album.coverPhoto
            self.setNeedsLayout()
          }
        } else {
          self.imageView!.image = album.coverPhoto
        }
        self.textLabel!.text = album.title ?? ""
        self.detailTextLabel!.text = buildSubtitle()
      }
    }
  }
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
    
    self.textLabel!.font = UIFont.dinMedium(ofSize: 15.0)
    self.detailTextLabel!.font = UIFont.dinMedium(ofSize: 12.0)
    self.layoutMargins = UIEdgeInsetsMake(10.0, 0, 0, 0);
    self.accessoryType = .disclosureIndicator
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func prepareForReuse() {
    super.prepareForReuse()
    album = nil
  }
  
  func buildSubtitle() -> String {
    if album.amountSelected > 0 {
      return String(format: "PhotoPicker Number Of Images Of Total".localized, album.amountSelected, album.count)
    } else {
      return album.count != NSNotFound ? String(album.count) : "...".localized
    }
  }
}
