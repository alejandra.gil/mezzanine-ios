//
//  VideoChatViewController.swift
//  Mezzanine
//
//  Created by miguel on 22/4/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

import UIKit
import AVFoundation

@objc enum VideoChatState: NSInteger {

  case preview = 0
  case fullscreen = 1
}

@objc class VideoChatViewController: UIViewController {

  @IBOutlet var contentView: UIView!
  @IBOutlet var videoChatView: PexVideoView!
  @IBOutlet var selfVideoChatView: PexVideoView!
  @IBOutlet var permissionsErrorButton: UIButton!

  @IBOutlet var fullscreenControlButtonsContainerView: UIView!
  @IBOutlet var audioMuteButton: MuteButton!
  @IBOutlet var videoMuteButton: MuteButton!
  @IBOutlet var flipCameraButton: UIButton!
  @IBOutlet var switchTypeButton: UIButton!

  @IBOutlet var previewControlButtonsContainerView: UIView!
  @IBOutlet var audioMutePreviewButton: MuteButton!
  @IBOutlet var videoMutePreviewButton: MuteButton!

  @IBOutlet var previewVideoControlsWidthConstraint: NSLayoutConstraint!
  @IBOutlet var fullscreenVideoControlsHeightConstraint: NSLayoutConstraint!
  @IBOutlet var selfVideoViewAspectConstraint: NSLayoutConstraint!

  fileprivate var pexipManagerObservers = Array <String> ()

  @objc weak var delegate: VideoChatViewControllerDelegate?

  @objc var state: VideoChatState = .preview
  fileprivate var isUsingFrontFacingCamera: Bool = true

  @objc var pexipManager: PexipManager! {
    willSet(newManager) {
      if (pexipManager != nil) {
        endObservingPexipManager()
      }
    }

    didSet {
      if (pexipManager != nil) {
        beginObservingPexipManager()
      }
    }
  }


  @objc init(pexipManager: PexipManager) {
    self.pexipManager = pexipManager
    super.init(nibName: "VideoChatViewController", bundle: nil)
    beginObservingPexipManager()
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  deinit {
    print("VideoChatViewController deinit")
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    let tapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(switchVideoChatState))
    videoChatView.addGestureRecognizer(tapGestureRecognizer)

    // TODO: It needs some logic to set the right aspect ratio depending on the device, orientation and camera.
    flipCameraButton.isHidden = false

    updateViewBorder(self.contentView, visible: true)
    updateViewBorder(self.videoChatView, visible: true)
    videoChatView.clipsToBounds = true

    contentView.layer.shadowColor = UIColor.black.cgColor
    contentView.layer.shadowOpacity = 0.5
    contentView.layer.shadowOffset = CGSize(width: 7.0, height: 7.0)
    contentView.layer.shadowRadius = 5.0

    previewVideoControlsWidthConstraint.constant = self.traitCollection.isiPad() ? 60 : 45
    fullscreenVideoControlsHeightConstraint.constant = 60

    pexipManager.setupVideoViews(videoChatView, selfVideoView: selfVideoChatView)

    permissionsErrorButton.layer.cornerRadius = 10.0
    permissionsErrorButton.titleLabel!.font = UIFont.dinBold(ofSize: 16.0)
    permissionsErrorButton.titleLabel?.numberOfLines = 0;
    permissionsErrorButton.setTitleColor(MezzanineStyleSheet.shared().grey110Color, for: UIControlState())
    permissionsErrorButton.backgroundColor = MezzanineStyleSheet.shared().grey10Color
    permissionsErrorButton.setTitle("Settings Video Alert VTC".localized.uppercased(), for: UIControlState())
    permissionsErrorButton.isHidden = true
    
    videoMuteButton.type = .video
    videoMutePreviewButton.type = .video
    videoMutePreviewButton.showsTitle = false
    
    audioMuteButton.type = .audio
    audioMutePreviewButton.type = .audio
    audioMutePreviewButton.showsTitle = false
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    updateInterface()
  }

  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    updateSelfViewAspectRatio(false)
  }

  @objc func updateInterface() {
    updateAudioMuteButton()
    updateVideoMuteButton()
    
    if videoMuteButton.isAllowed == false {
      permissionsErrorButton.isHidden = false
    }

    if state == .preview {
      self.fullscreenControlButtonsContainerView.alpha = 0.0
      UIView.animate(withDuration: 0.33, animations: {
        self.flipCameraButton.alpha = 0.0
        self.selfVideoChatView.alpha = 0.0
        self.previewControlButtonsContainerView.alpha = 1.0
        self.view.layoutIfNeeded()
        }, completion: { (Bool) in
          UIView.animate(withDuration: 0.05, animations: {
              self.updateViewBorder(self.videoChatView, visible: true)
              self.updateViewBorder(self.contentView, visible: true)
              self.contentView.layer.shadowOpacity = 0.5
          })
      })
    } else {
      self.previewControlButtonsContainerView.alpha = 0.0
      UIView.animate(withDuration: 0.33, animations: {
        self.flipCameraButton.alpha = 1.0
        self.selfVideoChatView.alpha = !self.pexipManager.isVideoMuted() ? 1.0 : 0.0
        self.updateViewBorder(self.videoChatView, visible: false)
        self.updateViewBorder(self.contentView, visible: false)
        self.contentView.layer.shadowOpacity = 0.0
        self.fullscreenControlButtonsContainerView.alpha = 1.0
        self.view.layoutIfNeeded()
      })
    }
  }

  func updateViewBorder(_ view: UIView, visible: Bool) {
    view.layer.cornerRadius = visible ? 10.0 : 0.0
    view.layer.borderColor = visible ? MezzanineStyleSheet.shared().grey110Color.cgColor : nil
    view.layer.borderWidth = visible ? 1.0 : 0.0;
  }

  func updateAudioMuteButton() {
    audioMuteButton.isMuted = pexipManager.isAudioMuted()
    audioMutePreviewButton.isMuted = pexipManager.isAudioMuted()
  }

  func updateVideoMuteButton() {
    videoMuteButton.isMuted = pexipManager.isVideoMuted()
    videoMutePreviewButton.isMuted = pexipManager.isVideoMuted()
  }

  @objc func switchVideoChatState(_ tapGestureRecognizer: UITapGestureRecognizer) {
    if state == .fullscreen {
      updateState(.preview)
    }
    else {
      updateState(.fullscreen)
    }
    
    let analyticsManager = MezzanineAnalyticsManager.sharedInstance
    analyticsManager.tagEvent(analyticsManager.vtcFullscreenEvent, attributes: [analyticsManager.stateKey : state == .fullscreen ? analyticsManager.fullscreenAttribute : analyticsManager.bubbleAttribute])
  }

  func updateState(_ newState: VideoChatState) {
    if state == newState {
      return
    }
    state = newState
    delegate?.videoChatViewControllerWillTransitionToState!(self, state: state) { () in
      self.updateInterface()
    }
  }


  // MARK: UI Actions
  
  @IBAction func showPermissionsAlert(_ sender: UIButton) {
    
    let alertTitle = "Settings Video Alert Title".localized
    let alertMessage = "Settings Video Alert Message".localized
    
    let alertController = UIAlertController (title: alertTitle, message: alertMessage, preferredStyle: .alert)
    let settingsAction = UIAlertAction(title: "Generic Title Settings".localized, style: .default) { (_) -> Void in
      let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
      if let url = settingsUrl {
        UIApplication.shared.openURL(url)
      }
    }
    
    let cancelAction = UIAlertAction(title: "Generic Title OK".localized, style: .default, handler: nil)
    alertController.addAction(settingsAction)
    alertController.addAction(cancelAction)
    
    self.present(alertController, animated: true, completion: nil)
  }

  @IBAction func close(_ sender: UIButton) {
    updateState(.preview)
  }

  @IBAction func audioMute(_ sender: UIButton) {
    pexipManager.audioToggle()
    updateAudioMuteButton()
    sendAnalyticsMuteEvent()
  }

  @IBAction func videoMute(_ sender: UIButton) {
    pexipManager.videoToggle()
    updateVideoMuteButton()
    selfVideoChatView.alpha = !pexipManager.isVideoMuted() && state == .fullscreen ? 1.0 : 0.0
    flipCameraButton.isHidden = pexipManager.isVideoMuted()
    sendAnalyticsMuteEvent()
  }

  @IBAction func cameraFlip(_ sender: UIButton) {
    pexipManager.cameraFlip()
    isUsingFrontFacingCamera = !isUsingFrontFacingCamera
    updateSelfViewAspectRatio(true)
  }

  @objc func canBeDragged() -> Bool {
    return (state == .preview)
  }


  // MARK: VideoChat Preview styling

  func updateSelfViewAspectRatio(_ animated: Bool) {
    guard let camera = getCurrentCamera() else { return }

    // The camera dimensions are wrong until it is turned on and its PexVideoView is loaded (tested on an iPad Air 2, iOS 9)
    let size = CMVideoFormatDescriptionGetPresentationDimensions(camera.activeFormat.formatDescription, true, true);
    let localVideoAspectRatio = UIDeviceOrientationIsLandscape(UIDevice.current.orientation) ? size.width / size.height : size.height / size.width

    if (self.selfVideoViewAspectConstraint.multiplier != localVideoAspectRatio) {
      selfVideoChatView.removeConstraint(selfVideoViewAspectConstraint)
      selfVideoViewAspectConstraint = NSLayoutConstraint(item: selfVideoChatView, attribute: .width, relatedBy: .equal, toItem: selfVideoChatView, attribute: .height, multiplier: localVideoAspectRatio, constant: 0.0)
      selfVideoChatView.addConstraint(selfVideoViewAspectConstraint)

      if animated {
        UIView.animate(withDuration: 0.33, animations: {
          self.view.updateConstraintsIfNeeded()
          self.view.layoutIfNeeded()
        })
      }
      else {
        self.view.updateConstraintsIfNeeded()
        self.view.layoutIfNeeded()
      }
    }
  }

  func getCurrentCamera() -> AVCaptureDevice? {
    let availableCameraDevices = AVCaptureDevice.devices(for: AVMediaType.video)
    for device in availableCameraDevices {
      if isUsingFrontFacingCamera {
        if device.position == AVCaptureDevice.Position.front {
          return device
        }
      }
      else {
        if device.position == AVCaptureDevice.Position.back {
          return device
        }
      }
    }
    return nil
  }
  

  // MARK: Analytics
  
  func sendAnalyticsMuteEvent() {
    let analyticsManager = MezzanineAnalyticsManager.sharedInstance
    var muteString = ""
    
    if pexipManager.isAudioMuted() {
      if pexipManager.isVideoMuted() {
        muteString = analyticsManager.neitherAttribute
      } else {
        muteString = analyticsManager.videoAttribute
      }
    } else {
      if pexipManager.isVideoMuted() {
        muteString = analyticsManager.audioAttribute
      } else {
        muteString = analyticsManager.bothAttribute
      }
    }
    
    analyticsManager.tagEvent(analyticsManager.vtcMuteEvent, attributes: [analyticsManager.mediaKey : muteString])
  }


  // MARK: PexipManager Observation

  func beginObservingPexipManager() {
    pexipManagerObservers.append(pexipManager.addObserver(forKeyPath: "conferenceState", options: [.new, .old], on: OperationQueue.main) { ( obj, change: [AnyHashable: Any]!) in

      let newStateRawrawValue = Int(change[NSKeyValueChangeKey.newKey] as! NSNumber)

      switch PexipConferenceState(rawValue:newStateRawrawValue)! {
      case .disconnected:
        // TODO: Show a Disconnected overlay view or similar
        break
      case .connected:
        DispatchQueue.main.async {
          if self.pexipManager.conferenceMode == .audioVideo {
//            self.updateSelfViewAspectRatio(false)
            self.updateInterface()
          }
        }
        break
      default:
        break
      }
      })
  }

  func endObservingPexipManager() {
    for token in pexipManagerObservers {
      pexipManager.removeObserver(withBlockToken: token)
    }
    pexipManagerObservers.removeAll()
  }
}

@objc protocol VideoChatViewControllerDelegate : NSObjectProtocol {
  @objc optional func videoChatViewControllerWillTransitionToState(_ videoChatViewController: VideoChatViewController!, state:VideoChatState, animations: @escaping () -> Void)
}


