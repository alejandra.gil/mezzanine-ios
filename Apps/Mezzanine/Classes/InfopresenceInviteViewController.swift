//
//  InfopresenceInviteViewController.swift
//  Mezzanine
//
//  Created by Zai Chang on 6/26/15.
//  Copyright (c) 2015 Oblong Industries. All rights reserved.
//

import UIKit

@objc class InfopresenceInviteViewController : UIViewController, UITableViewDelegate, UINavigationBarDelegate
{
  @IBOutlet weak var navigationBar: UINavigationBar!
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var inviteButton: UIButton!

	@IBOutlet weak var maximumInvitationsErrorView: UIView!
	@IBOutlet weak var maximumInvitationsErrorLabel: UILabel!

	var noMezzaninesView: UIView?
	@IBOutlet weak var noMezzaninesTitleLabel: UILabel!
	@IBOutlet weak var noMezzaninesDescriptionLabel: UILabel!

	let inviteLimit = 4
  var maxCurrentInvitesAvailable: Int {
    get {
      return inviteLimit - systemModel.infopresence.rooms.count - systemModel.infopresence.pendingInvitesCount
    }
  }

	
	fileprivate var kSystemModelContext = 0
	var systemModel: MZSystemModel!
	
	func beginObservingSystemModel() {
		self.systemModel?.addObserver(self, forKeyPath: "remoteMezzes", options: .new, context: &kSystemModelContext)
    self.systemModel?.addObserver(self, forKeyPath: "infopresence.rooms", options: .new, context: &kSystemModelContext)
	}

	func endObservingSystemModel() {
		self.systemModel?.removeObserver(self, forKeyPath: "remoteMezzes")
    self.systemModel?.removeObserver(self, forKeyPath: "infopresence.rooms")
	}

	var communicator: MZCommunicator
  unowned var infopresenceMenuController: InfopresenceMenuController
	var dataSource: RemoteMezzaninesTableViewDataSource!

  init(systemModel: MZSystemModel, communicator: MZCommunicator, infopresenceMenuController: InfopresenceMenuController) {
		self.systemModel = systemModel
		self.communicator = communicator
    self.infopresenceMenuController = infopresenceMenuController
		super.init(nibName: "InfopresenceInviteViewController", bundle: nil)
		beginObservingSystemModel()
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	deinit {
		endObservingSystemModel()
	}

	override func viewDidLoad() {
    super.viewDidLoad()
    
    let isRegular = infopresenceMenuController.containerViewController?.traitCollection.isRegularWidth() == true && infopresenceMenuController.containerViewController?.traitCollection.isRegularHeight() == true
    preferredContentSize = CGSize(width: isRegular ? 320.0 : 300.0, height: CGFloat.greatestFiniteMagnitude);
    
		dataSource = RemoteMezzaninesTableViewDataSource(systemModel: systemModel, tableView: tableView)

    // This is to avoid the UITableView to be shifted 20 px up on iPhone because of the status bar being present
    self.edgesForExtendedLayout = UIRectEdge()

		tableView.dataSource = dataSource
		tableView.delegate = self
		tableView.allowsMultipleSelection = true
    tableView.backgroundColor = UIColor.clear

		maximumInvitationsErrorView.isHidden = true

		let styleSheet = MezzanineStyleSheet.shared()

		let noMezzaninesViewNib = UINib(nibName: "NoMezzaninesAvailableView", bundle: nil)
		noMezzaninesView = noMezzaninesViewNib.instantiate(withOwner: self, options: nil)[0] as? UIView
		noMezzaninesView!.autoresizingMask = [.flexibleWidth, .flexibleHeight]
		noMezzaninesView!.frame = tableView.frame
		noMezzaninesTitleLabel.text = "Infopresence No Mezzanines Label".localized
		noMezzaninesDescriptionLabel.text = "Infopresence No Remote Mezzanine Label".localized
		styleSheet?.stylizeDimInformativeTitleLabel(noMezzaninesTitleLabel)
		styleSheet?.stylizeDimInformativeDescriptionLabel(noMezzaninesDescriptionLabel)
		self.view.addSubview(noMezzaninesView!)
		updateNoMezzaninesView()

    maximumInvitationsErrorLabel.font = UIFont.dinMedium(ofSize: maximumInvitationsErrorLabel.font.pointSize)

    self.view.backgroundColor = styleSheet?.grey30Color
    styleSheet?.stylizeCall(toActionButton: inviteButton)

    navigationBar.delegate = self
    navigationBar.backgroundColor = styleSheet?.grey50Color

    let navBarLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 32))
    navBarLabel.text = "Infopresence Invite Mezzanines Panel Header".localized
    navBarLabel.textAlignment = .center
    styleSheet?.stylizeHeaderTitleLabel(navBarLabel)

    // The navigation item has been added at the nib file
    let item = navigationBar.items!.first!
    item.titleView = navBarLabel

    if !isRegular
    {
      let backButton = UIButton(type: .custom)
      backButton.frame = CGRect(x: 0, y: 0, width: 32, height: 22)
      backButton.setImage(UIImage(named: "Arrow"), for: .normal)
      backButton.imageEdgeInsets = UIEdgeInsetsMake(8, 0, 5, 20)
      backButton.addTarget(self, action: #selector(goBack), for: .touchUpInside)
      let barButtonItem = UIBarButtonItem(customView: backButton)
      item.leftBarButtonItem = barButtonItem
    }
	}

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    updateLimitExceededMessage()
    updateInviteButton()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    
    // Analytics
    let analyticsManager = MezzanineAnalyticsManager.sharedInstance
    analyticsManager.tagScreen(analyticsManager.infopresenceInviteMezzaninesScreen);
  }
  
  override var prefersStatusBarHidden : Bool {
    return false
  }
  
  override var preferredStatusBarUpdateAnimation : UIStatusBarAnimation {
    return .fade
  }
  
  override var preferredStatusBarStyle : UIStatusBarStyle {
    return .lightContent
  }

  @IBAction func goBack() {
    infopresenceMenuController.popViewControllerUsingNavigationController()
  }
	
	func updateNoMezzaninesView() {
		if (systemModel.remoteMezzes.count == 0) {
			noMezzaninesView?.isHidden = false
			tableView.isHidden = true
		}
		else {
			noMezzaninesView?.isHidden = true
			tableView.isHidden = false
		}
	}

  func updateLimitExceededMessage() {
    if maxCurrentInvitesAvailable == 0 {
      setInviteLimitExceededMessageVisible(true)
    }
    else if let selectedRows = tableView.indexPathsForSelectedRows {
      if (selectedRows.count) <= maxCurrentInvitesAvailable {
        setInviteLimitExceededMessageVisible(false)
      }
    }
    else {
      setInviteLimitExceededMessageVisible(false)
    }
  }

	func updateInviteButton() {
		var numberOfSelectedRows = 0
		if let indexPaths = tableView.indexPathsForSelectedRows {
			numberOfSelectedRows = indexPaths.count
		}

    if (numberOfSelectedRows) > maxCurrentInvitesAvailable {
      return
    }

		var title: String
		switch numberOfSelectedRows {
		case 0:
			title = "Infopresence Invite Mezzanines Button Title None".localized
		case 1:
			title = "Infopresence Invite Mezzanines Button Title Single".localized
		default:
			title = String(format: "Infopresence Invite Mezzanines Button Title Mutiple".localized, numberOfSelectedRows)
		}
		
		inviteButton.setTitle(title.uppercased(), for: UIControlState())
		inviteButton.isEnabled = (numberOfSelectedRows > 0)
	}
	
	internal override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?)
	{
		if context == &kSystemModelContext
		{
			if keyPath == "remoteMezzes" {
				updateNoMezzaninesView()
			}
      else if keyPath == "infopresence.rooms" {
        updateLimitExceededMessage()
        updateInviteButton()
      }
		}
		else
		{
			super.observeValue(forKeyPath: keyPath!, of: object!, change: change, context: context)
		}
	}

	func setInviteLimitExceededMessageVisible(_ visible: Bool) {
		maximumInvitationsErrorView.isHidden = !visible
		
		if visible {
      maximumInvitationsErrorLabel.text = maxCurrentInvitesAvailable == 0 ? "Infopresence Invite No More Mezzanines Reached Error Message".localized.uppercased() : String(format: "Infopresence Invite Maximum Mezzanines Reached Error Message".localized, arguments: [maxCurrentInvitesAvailable]).uppercased()
		}
	}
	
	@IBAction func inviteSelectedMezzanines() {
		let selectedMezzanine = dataSource.selectedItems().map{$0.uid}
      maximumInvitationsErrorLabel.text = maxCurrentInvitesAvailable == 0 ? "Infopresence Invite No More Mezzanines Reached Error Message".localized.uppercased() : String(format: "Infopresence Invite Maximum Mezzanines Reached Error Message".localized, arguments: [maxCurrentInvitesAvailable]).uppercased()
    communicator.requestor.requestInfopresenceOutgoingInviteRequest(selectedMezzanine.first as! String)

    if let indexPaths = tableView.indexPathsForSelectedRows {
      for indexPath in indexPaths
      {
        tableView.deselectRow(at: indexPath, animated: true)
      }
      updateInviteButton()
    }
	}

  func indexPathIsSelectable(_ indexPath: IndexPath) -> Bool {
    let remoteMezz = dataSource.itemAtIndexPath(indexPath)
    return remoteMezz.online && (remoteMezz.collaborationState != .inCollaboration && remoteMezz.collaborationState != .joining)
  }

  func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
    return indexPathIsSelectable(indexPath)
  }
	
	func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
    if indexPathIsSelectable(indexPath) == false {
      return nil
    }

		if let selectedRows = tableView.indexPathsForSelectedRows {
			if (selectedRows.count + 1) > maxCurrentInvitesAvailable {
				setInviteLimitExceededMessageVisible(true)
			}
		}

    if (maxCurrentInvitesAvailable == 0) {
      setInviteLimitExceededMessageVisible(true)
    }

		return indexPath
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let remoteMezz = dataSource.itemAtIndexPath(indexPath)
    if remoteMezz.collaborationState == MZRemoteMezzCollaborationState.invited {
      communicator.requestor.requestInfopresenceOutgoingInviteCancelRequest(remoteMezz.uid)
      
      tableView.deselectRow(at: indexPath, animated: true)
      setInviteLimitExceededMessageVisible(false)
    }
    updateInviteButton()
	}
	
	func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
    updateLimitExceededMessage()
    updateInviteButton()
	}
  
  
  // MARK: Adaptative layout

  override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
    for (_, viewController) in (navigationController?.viewControllers.enumerated())! {
      if viewController == self {
        navigationController?.popViewController(animated: false)
        break
      }
    }
  }

  
  // MARK: Navigation bar delegate

  func position(for bar: UIBarPositioning) -> UIBarPosition {
    return .topAttached
  }
}
