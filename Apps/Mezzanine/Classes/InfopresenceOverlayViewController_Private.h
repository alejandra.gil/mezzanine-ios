//
//  InfopresenceOverlayViewController_Private.h
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 16/12/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

#import "InfopresenceOverlayViewController.h"

@class RemoteMezzanineTableViewCell;

@interface InfopresenceOverlayViewController ()
{
  RemoteMezzanineTableViewCell *_remoteMezzView;
}

@end
