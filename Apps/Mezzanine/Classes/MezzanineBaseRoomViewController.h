//
//  MezzanineBaseRoomViewController.h
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 09/02/2017.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MezzanineAppContext.h"

@class WorkspaceViewController;
@class VideoChatViewController;

@protocol MezzanineMainViewControllerDelegate;

@interface MezzanineBaseRoomViewController : UIViewController
@property (nonatomic, weak) id <MezzanineMainViewControllerDelegate> delegate;

// Model
@property (nonatomic, strong) MezzanineAppContext *appContext;
@property (nonatomic, strong) MZCommunicator *communicator;
@property (nonatomic, strong) MZSystemModel *systemModel;
@property (nonatomic, strong) MZWorkspace *workspace;


// TO DO
// Deprecated
@property (nonatomic, strong) UIGestureRecognizer *currentDragDropRecognizer; // Implement in childViewControllers and private here
@property (nonatomic, strong) FPPopoverController *currentPopover; // Implement in childViewControllers and private here


// TODO: To be set as private again
@property (nonatomic, strong) WorkspaceViewController *workspaceViewController;
@property (nonatomic, strong) VideoChatViewController *videoChatViewController;

- (void)showWorkspaceOverlayWithMessage:(NSString*)message animated:(BOOL)animated;
- (void)hideWorkspaceOverlayViewAnimated:(BOOL)animated;

@end
