//
//  WorkspaceEditorViewController.h
//  Mezzanine
//
//  Created by Zai Chang on 3/20/11.
//  Copyright 2011 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MZWorkspace.h"


/// Controller for the editing a workspace

@interface WorkspaceEditorViewController : UIViewController <UITextFieldDelegate>

@property (nonatomic,strong) MZWorkspace *workspace;
@property (nonatomic,copy) NSString *workspaceNameOverride; // If set, the workspace name displayed to the user will be overridden.
@property (nonatomic,assign) BOOL autoHighlightNameField;

@property (nonatomic,strong) UITextField *nameTextField;
@property (nonatomic,copy) void (^requestCancel)();
@property (nonatomic,copy) void (^requestSave)();

@property (nonatomic,copy) NSString* viewTitle;
@property (nonatomic,copy) NSString* doneButtonTitle;
@property (nonatomic,strong) NSString *hintText;  // If not null, this text will appear above the action/done button to provide context to the action below

@property (nonatomic, strong) UIControl *saveButton;
@property (nonatomic, strong) UILabel *hintLabel;

- (id)initWithWorkspace:(MZWorkspace*)workspace;

@end
