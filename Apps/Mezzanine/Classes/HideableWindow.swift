//
//  HideableWindow.swift
//  Mezzanine
//
//  Created by miguel on 6/12/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

import UIKit

class HideableWindow: UIWindow {

  fileprivate var hiddenRootViewController: UIViewController?

  override var isHidden: Bool {
    get {
      return super.isHidden
    }
    set(newHidden) {
      super.isHidden = newHidden

      if newHidden == true {
        if self.rootViewController != nil {
          self.hiddenRootViewController = self.rootViewController
        }
        super.rootViewController = nil
      }
      else if self.hiddenRootViewController != nil {
        super.rootViewController = self.hiddenRootViewController
        self.hiddenRootViewController = nil
      }
    }
  }

  override var rootViewController: UIViewController? {
    get {
      return super.rootViewController
    }

    set(newRootViewController) {
      if super.isHidden {
        super.rootViewController = nil
        self.hiddenRootViewController = newRootViewController
      }
      else {
        super.rootViewController = newRootViewController
        self.hiddenRootViewController = nil
      }
    }
  }
}
