//
//  AssetOvumSourceViewController.m
//  Mezzanine
//
//  Created by Zai Chang on 6/25/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import "AssetOvumSourceViewController.h"
#import "ImageAssetView.h"
#import "VideoAssetView.h"

@interface AssetOvumSourceViewController ()

@end


@implementation AssetOvumSourceViewController

@synthesize dragViewScale;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) 
  {
    dragViewScale = 1.25;
  }
  return self;
}


#pragma mark - OBOvumSource

-(OBOvum *) createOvumFromView:(UIView*)sourceView
{
  if ([sourceView isKindOfClass:[ImageAssetView class]])
  {
    ImageAssetView *assetView = (ImageAssetView *)sourceView;
    OBOvum *ovum = [[OBOvum alloc] init];
    ovum.dataObject = assetView.asset;
    return ovum;
  }
  else if ([sourceView isKindOfClass:[VideoAssetView class]])
  {
//    VideoAssetView *assetView = (VideoAssetView *)sourceView;
    OBOvum *ovum = [[OBOvum alloc] init];
//    ovum.dataObject = assetView.viddle;
    return ovum;
  }
  else
    return nil;
}


-(UIView *) createDragRepresentationOfSourceView:(UIView *)sourceView inWindow:(UIWindow*)window
{
  if ([sourceView isKindOfClass:[ImageAssetView class]])
  {    
    ImageAssetView *assetView = (ImageAssetView *)sourceView;
    
    if (assetView.actionsPanelVisible)
      [assetView setActionsPanelVisible:NO animated:YES];
    
    CGRect frameInWindow = [assetView convertRect:assetView.imageView.frame toView:assetView.window];
    frameInWindow = [window convertRect:frameInWindow fromWindow:assetView.window];
    
    ImageAssetView *dragImage = [[ImageAssetView alloc] initWithFrame:frameInWindow];
    dragImage.backgroundColor = assetView.imageView.backgroundColor;
    dragImage.asset = assetView.asset;
    dragImage.contentMode = UIViewContentModeScaleAspectFit;
    dragImage.layer.masksToBounds = YES;
    return dragImage;
  }
  else if ([sourceView isKindOfClass:[VideoAssetView class]])
  {    
    VideoAssetView *assetView = (VideoAssetView *)sourceView;
    
    if (assetView.actionsPanelVisible)
      [assetView setActionsPanelVisible:NO animated:YES];
    
    CGRect frameInWindow = [assetView convertRect:assetView.imageView.frame toView:assetView.window];
    frameInWindow = [window convertRect:frameInWindow fromWindow:assetView.window];
    
    VideoAssetView *dragImage = [[VideoAssetView alloc] initWithFrame:frameInWindow];
//    dragImage.viddle = assetView.viddle;
    dragImage.backgroundColor = assetView.imageView.backgroundColor;
    dragImage.imageView.image = assetView.imageView.image;
    dragImage.contentMode = UIViewContentModeScaleAspectFit;
    dragImage.layer.masksToBounds = YES;
    return dragImage;
  }
  else
    return nil;
}


-(void) dragViewWillAppear:(UIView *)dragView inWindow:(UIWindow*)window atLocation:(CGPoint)location
{
  dragView.alpha = 0.0;
  
  [UIView animateWithDuration:0.25 animations:^{
    dragView.center = location;
    dragView.transform = CGAffineTransformMakeScale(dragViewScale, dragViewScale);
    dragView.alpha = 0.75;
  }];
}

@end
