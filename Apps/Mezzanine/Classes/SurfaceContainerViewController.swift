//
//  SurfaceContainerViewController.swift
//  Mezzanine
//
//  Created by miguel on 21/2/17.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import UIKit

class SurfaceContainerViewController: UIViewController {

  var containerScrollView: UIScrollView?

  var workspaceGeometry: WorkspaceGeometry?

  var surfaceViews = [SurfaceView]()

  var surfaces: NSArray? {
    didSet {
      insertSurfaces()
      updateSurfacesLayout()
    }
  }

  deinit {
    print("SurfaceContainerViewController deinit")
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    view.backgroundColor = UIColor.clear
    insertSurfaces()
  }

  override func viewDidLayoutSubviews() {
    updateSurfacesLayout()
  }

  func insertSurfaces() {

    surfaceViews.forEach{$0.removeFromSuperview()}
    surfaceViews.removeAll()

    guard let surfaces = surfaces else { return }

    for surface in surfaces {
      let surface = surface as! MZSurface
      let surfaceBoundView = SurfaceView(frame: CGRect.zero)
      surfaceBoundView.surface = surface
      surfaceBoundView.drawFelds = true
      surfaceBoundView.backgroundColor = UIColor.clear
      view.addSubview(surfaceBoundView)
      surfaceViews.append(surfaceBoundView)
    }
  }

  func updateSurfacesLayout() {

    guard let workspaceGeometry = workspaceGeometry else { return }
    guard let containerScrollView = containerScrollView else { return }
    let scale = containerScrollView.zoomScale

    for surfaceView in surfaceViews {
      guard let surface = surfaceView.surface else { break }
      let surfaceRect = workspaceGeometry.roundedFrameForSurface(surface)
      surfaceView.containerScale = scale
      surfaceView.frame = surfaceRect
      surfaceView.setNeedsDisplay()
      surfaceView.setNeedsLayout()
    }
  }
}
