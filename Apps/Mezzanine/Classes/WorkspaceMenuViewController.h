//
//  WorkspaceMenuViewController.h
//  Mezzanine
//
//  Created by Zai Chang on 8/27/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import "ButtonMenuViewController.h"
#import "WorkspaceToolbarViewController.h"


@interface WorkspaceMenuViewController : ButtonMenuViewController

@property (nonatomic, strong) MZWorkspace *workspace;
@property (nonatomic, strong) MZSystemModel *systemModel;
@property (nonatomic, weak) WorkspaceToolbarViewController *workspaceToolbarViewController;

@end
