//
//  MezzanineAppContext.m
//  Mezzanine
//
//  Created by Zai Chang on 1/26/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import "MezzanineAppContext.h"
#import "MZDownloadManager.h"
#import "MezzanineStyleSheet.h"
#import "OBAppData.h"
#import "NSURL+Additions.h"
#import "NSObject+UIAlertController.h"
#import "UIProgressViewController.h"

// For testing
#import "MZOfflineCommunicator.h"
#import "MZSystemModel+InterfaceTesting.h"

#import "Mezzanine-Swift.h"

#define kSystemModelContext @"kSystemModelContext"
#define kMezzanineAppContextErrorCode_RPIncompatibleOSVersion -8001

// For Analytics
#define kRemoteMezzCount @"remoteMezzCount"
#define kWorkspaceCount @"workspaceCount"


@interface MezzanineAppContext () <RemoteParticipationJoinViewControllerDelegate, SystemUseNotificationViewControllerDelegate>
{
  NSMutableArray* _pexipObservers;
  BOOL initAudioOnlySession;
  NSMutableDictionary *mezzanineStateAnalyticsData;
}

@property (nonatomic, strong) DirectoryWatcher *docWatcher;

//-(void) communicatorJoinedSuccessfully:(MZCommunicator*)communicator;
-(void) communicatorWasDisconnected:(NSNotification*)notification;
-(void) communicatorDidDisconnect:(NSNotification*)notification;

@end



@implementation MezzanineAppContext

@synthesize window;
@synthesize progressWindow;
@synthesize popupInfoWindow;
@synthesize popupInfoView;

@synthesize enteredBackgroundDate;
@synthesize attemptingToReconnect;
@synthesize attemptingToConnectWithPassphrase;

@synthesize applicationModel;
@synthesize assetCache;
@synthesize documentInteractionController;

@synthesize mainNavController;
@synthesize connectionViewController;
@synthesize progressViewController;
@synthesize remoteParticipationJoinViewController;

#pragma mark - Current Context

static MezzanineAppContext *_currentContext;

+(MezzanineAppContext*) currentContext
{
  return _currentContext;
}

+(void) setCurrentContext:(MezzanineAppContext*)context
{
  _currentContext = context;
}

-(id) init
{
  self = [super init];
  if (self)
  {
    documentInteractionController = [[MezzanineDocumentInteractionController alloc] init];
    _exportManager = [[MezzanineExportManager alloc] init];
    _exportManager.maximumFileSizeInBytes = documentInteractionController.maximumFileSizeInBytes;
    _infopresenceManager = [MezzanineInfopresenceManager new];
    _pexipManager = [PexipManager new];
    _pexipObservers = [NSMutableArray new];
    _mailComposer = [MezzanineMailComposerController new];
    _passkeyCoordinator = [PasskeyCoordinator new];

    __weak typeof(self) __self = self;
    id observerToken = [_pexipManager addObserverForKeyPath:@"conferenceMode"
                                                    options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld | NSKeyValueObservingOptionInitial)
                                                    onQueue:[NSOperationQueue mainQueue]
                                                       task:^(id obj, NSDictionary *change) {

                                                         NSNumber *number = change[NSKeyValueChangeOldKey];
                                                         PexipConferenceMode oldMode = number.unsignedIntegerValue;
                                                         PexipConferenceMode newMode = __self.pexipManager.conferenceMode;

                                                         if (oldMode == PexipConferenceModeAudioOnly && newMode == PexipConferenceModeAudioVideo) {
                                                           [__self setAudioOnlyMode:NO];
                                                         }
                                                         else if (oldMode == PexipConferenceModeAudioVideo && newMode == PexipConferenceModeAudioOnly) {
                                                           [__self setAudioOnlyMode:YES];
                                                         }
                                                       }];

    [_pexipObservers addObject:observerToken];

    [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidReceiveMemoryWarningNotification
                                                      object:[UIApplication sharedApplication]
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification *notification) {
                                                    [__self handleMemoryWarning:[notification object]];
                                                  }];

    if ([[UIDevice currentDevice] isIOS8OrAbove] && ![[UIDevice currentDevice] isIOS81OrAbove])
    {
      // Bug 12558 - iOS 8.0.x does not clean up the image data cache folder controlled by iOS at:
      //  <App_Home>/Library/Caches/<Bunddle Identifier>/fsCachedData

      // We start observing changes in that folder to do some checks
      self.docWatcher = [DirectoryWatcher watchFolderWithPath:[self applicationCacheDataFolder] delegate:self];
      [self directoryDidChange:self.docWatcher];
    }
  }
  return self;
}


-(void) dealloc
{
  [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidReceiveMemoryWarningNotification object:[UIApplication sharedApplication]];

  for (id observer in _pexipObservers)
    [_pexipManager removeObserverWithBlockToken:observer];

  [_pexipObservers removeAllObjects];
}


#pragma mark - Context Setup for UI

-(void) setupDragDropManager
{
  OBDragDropManager *manager = [OBDragDropManager sharedManager];
  [manager prepareOverlayWindowUsingMainWindow:self.window];
}


-(void) setupInfopresenceOverlay
{
  [_infopresenceManager setSystemModel:self.applicationModel.systemModel];
  [_infopresenceManager prepareOverlayWindowUsingMainWindow:self.window];
}


#pragma mark - NavigationController 

-(void) setMainNavController:(MezzanineNavigationController *)aNavController
{
  mainNavController = aNavController;
  _mailComposer.presenterViewController = aNavController;
  _passkeyCoordinator.navigationController = aNavController;
}


#pragma mark - Application Model

-(void) setApplicationModel:(MezzanineAppModel *)aModel
{
  if (applicationModel != aModel)
  {
    if (applicationModel)
    {
      MZSystemModel *systemModel = applicationModel.systemModel;
      [systemModel removeObserver:self forKeyPath:@"state"];
      [systemModel removeObserver:self forKeyPath:@"passphraseRequested"];
      [systemModel removeObserver:self forKeyPath:@"popupMessage"];
      systemModel.showError = nil;
      _infopresenceManager.systemModel = nil;
      _exportManager.portfolioExportInfo = nil;
      _pexipManager.infopresence = nil;

      [self endObservingMezzanine:systemModel.myMezzanine];
    }

    applicationModel = aModel;

    if (applicationModel)
    {
      MZSystemModel *systemModel = applicationModel.systemModel;

      [systemModel addObserver:self forKeyPath:@"state" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:kSystemModelContext];
      [systemModel addObserver:self forKeyPath:@"passphraseRequested" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:kSystemModelContext];
      [systemModel addObserver:self forKeyPath:@"popupMessage" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:kSystemModelContext];

      __weak typeof(self) __weakSelf = self;
      systemModel.showError = ^(NSString* summary, NSString *description){
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:summary message:description preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Generic Button Title Okay", nil) style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:okAction];
        [__weakSelf showAlertController:alertController animated:YES completion:nil];
      };

      _infopresenceManager.systemModel = systemModel;
      _exportManager.portfolioExportInfo = systemModel.portfolioExport.info;
      _pexipManager.infopresence = systemModel.infopresence;

      [self addMezzanineObservationTokens:systemModel.myMezzanine];
    }
  }
}


#pragma mark - MZCommunicator

-(void) setCurrentCommunicator:(MZCommunicator *)currentCommunicator
{
	_currentCommunicator = currentCommunicator;
	_infopresenceManager.communicator = _currentCommunicator;
  _passkeyCoordinator.communicator = _currentCommunicator;
}


#pragma mark - View Hierarchy Manageent

-(MezzanineRootViewController *) mezzanineViewController
{
  for (UIViewController *viewController in mainNavController.viewControllers)
  {
    if ([viewController isKindOfClass:[MezzanineRootViewController class]])
      return (MezzanineRootViewController *)viewController;
  }
  return nil;
}

-(void) showWorkspaceView:(BOOL)animated
{
  MezzanineRootViewController *mezzanineViewController = [self mezzanineViewController];
  if (mezzanineViewController)
  {
    if (mainNavController.topViewController != mezzanineViewController)
      [mainNavController popToViewController:mezzanineViewController animated:YES];
  }
  else
  {
    MezzanineRootViewController *mezzanineViewController = [[MezzanineRootViewController alloc] initWithNibName:@"MezzanineRootViewController" bundle:nil];
    mezzanineViewController.appContext = self;
    mezzanineViewController.communicator = _currentCommunicator;
    mezzanineViewController.systemModel = applicationModel.systemModel;
    mezzanineViewController.initAudioOnlySession = initAudioOnlySession;
    [mainNavController pushViewController:mezzanineViewController animated:YES];
  }
}

- (void)checkInitialStateAndSendAnalyticsEvent {

  if (!mezzanineStateAnalyticsData)
    mezzanineStateAnalyticsData = [NSMutableDictionary new];

  if (![[mezzanineStateAnalyticsData allKeys] containsObject:kRemoteMezzCount] ||
      ![[mezzanineStateAnalyticsData allKeys] containsObject:kWorkspaceCount])
    return;

  // Analytics
  MZSystemModel *systemModel = _currentCommunicator.systemModel;
  NSString *mezzVersion = systemModel.myMezzanine.version ? systemModel.myMezzanine.version : @"";
  NSString *apiVersion = systemModel.apiVersion ? systemModel.apiVersion : @"";
  BOOL isRemote = [_currentCommunicator isARemoteParticipationConnection];
  bool mezzInEnabled = _currentCommunicator.systemModel.infopresence.remoteParticipantsEnabled;
  NSNumber *remoteMezzCount = [mezzanineStateAnalyticsData objectForKey:kRemoteMezzCount];
  NSNumber *workspaceCount = [mezzanineStateAnalyticsData objectForKey:kWorkspaceCount];

  [[MezzanineAnalyticsManager sharedInstance] trackMezzanineDetailsEventWithMezzVersion:mezzVersion isRemote:isRemote apiVersion:apiVersion mezzInEnabled:mezzInEnabled remoteMezzanineCount:remoteMezzCount workspaceCount:workspaceCount];
}

-(void) showSystemUseNotificationView
{
  _systemUseNotificationViewController = [[SystemUseNotificationViewController alloc] initWithNibName:@"SystemUseNotificationViewController" bundle:nil];
  _systemUseNotificationViewController.delegate = self;
  _systemUseNotificationViewController.systemUseNotificationString = _currentCommunicator.systemUseNotificationText;
  [mainNavController presentViewController:_systemUseNotificationViewController animated:YES completion:nil];
}

#pragma mark - Analytics

- (void)showAnalyticsOptIn
{
    if (_analyticsViewController) {
      return;
    }
    
    _analyticsViewController = [[AnalyticsViewController alloc] initWithNibName:@"AnalyticsViewController" bundle:nil];
    MezzanineNavigationController *navC = [[MezzanineNavigationController alloc] initWithRootViewController:_analyticsViewController];
    navC.modalPresentationStyle = [UIDevice currentDevice].isIPad ? UIModalPresentationFormSheet : UIModalPresentationFullScreen;

    // In case Passphrase view is shown
  // TODO: Fix this by separating the visibility of those Analytics and Passkey VCs
    if (_passkeyCoordinator.passkeyIsVisible) {
      [_passkeyCoordinator.passkeyViewController presentViewController:navC animated:YES completion:^{
        [MezzanineAnalyticsManager sharedInstance].optInDialogShown = YES;
      }];
    }
    // Straight from Connection view
    else {
      [mainNavController.visibleViewController presentViewController:navC animated:YES completion:^{
        [MezzanineAnalyticsManager sharedInstance].optInDialogShown = YES;
      }];
    }
}


#pragma mark - PopupInfo View

-(void) initPopupInfoView
{
  if (popupInfoView)
    return;

  popupInfoWindow = [[HideableWindow alloc] initWithFrame:self.window.frame];
  popupInfoWindow.userInteractionEnabled = NO;
  popupInfoWindow.windowLevel = UIWindowLevelAlert;

  UIViewController *viewController = [[UIViewController alloc] initWithNibName:nil bundle:nil];
  popupInfoView = [[PopupInfoView alloc] initWithFrame:viewController.view.bounds];
  popupInfoView.overlayWindow = popupInfoWindow;
  popupInfoView.alpha = 0.0;
  popupInfoView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
  [viewController.view addSubview:popupInfoView];

  popupInfoWindow.hidden = YES;
  popupInfoWindow.rootViewController = viewController;
}



#pragma mark - Progress View

-(void) initProgressView
{
  if (progressWindow)
    return;

  progressWindow = [[HideableWindow alloc] initWithFrame:self.window.frame];
  progressWindow.userInteractionEnabled = NO;
  progressWindow.windowLevel = UIWindowLevelAlert;

  progressViewController = [[UIProgressViewController alloc] initWithNibName:@"UIProgressViewController" bundle:nil];
  progressViewController.view.frame = self.window.bounds;
  [progressWindow addSubview:progressViewController.view];

  progressWindow.hidden = YES;
  progressViewController.view.hidden = YES;
  progressWindow.rootViewController = progressViewController;
}


-(void) showProgressViewWithTitle:(NSString*)title indeterminate:(BOOL)indeterminate
{
  [progressWindow makeKeyAndVisible];
  [UIView beginAnimations:nil context:nil];
  [UIView setAnimationDuration:0.33];
  progressViewController.view.hidden = NO;
  progressWindow.hidden = NO;
  [UIView commitAnimations];
  
  progressViewController.titleLabel.text = title;
  progressViewController.isIndeterminate = indeterminate;
}


-(void) hideProgressView
{
  [UIView beginAnimations:nil context:nil];
  progressViewController.view.hidden = YES;
  progressWindow.hidden = YES;
  [UIView commitAnimations];
  
  [self.window makeKeyAndVisible];
}


#pragma mark -
#pragma mark SystemModel observation

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
  if (context == kSystemModelContext)
  {
    DLog(@"MezzanineAppDelegate observed change in MZSystemModel. keyPath %@", keyPath);
    
    if ([keyPath isEqual:@"state"])
    {
      BOOL animateIncomingView = YES;
      
      MZSystemState oldState = [change[NSKeyValueChangeOldKey] integerValue];
      MZSystemState newState = applicationModel.systemModel.state;

      if (oldState == MZSystemStateWorkspace)
      {
        // Stop loading images from slides or assets
        [[MZDownloadManager sharedLoader] cancelAllOperations];
        
        // User could be uploading a pdf while connection was lost
        if (documentInteractionController.isBusy)
          [documentInteractionController cancel];
        
        // Make sure we close modal controllers within mezzanineviewcontroller
        // eg. UIImagePicker
        UIViewController *controller = [self mezzanineViewController];
        if (controller.presentedViewController && controller.presentedViewController != _passkeyCoordinator.passkeyViewController)
          [controller dismissViewControllerAnimated:NO completion:nil];
      }
      else if (_passkeyCoordinator.passkeyIsVisible && (oldState == MZSystemStateNotConnected) && !_passkeyCoordinator.passkeyIsInTransition)
      {
        if  ([[MezzanineAnalyticsManager sharedInstance] canShowAnalyticsOptInDialog])
        {
          [self showAnalyticsOptIn];
        }
        else
        {
          [_passkeyCoordinator hidePasskeyView];
          animateIncomingView = NO;
        }
      }
      else if ((remoteParticipationJoinViewController != nil) && (oldState == MZSystemStateNotConnected))
      {
        [mainNavController dismissViewControllerAnimated:YES completion:nil];
        [mainNavController.transitionCoordinator animateAlongsideTransition:nil completion:^(id<UIViewControllerTransitionCoordinatorContext> context) {
          remoteParticipationJoinViewController.communicator = nil;
          remoteParticipationJoinViewController = nil;
        }];
        animateIncomingView = NO;
      }

      if (newState == MZSystemStateNotConnected)
      {
        [mainNavController dismissViewControllerAnimated:YES completion:nil];
        [mainNavController popToRootViewControllerAnimated:animateIncomingView];
        
        [_passkeyCoordinator cleanPasskey];
        
        // Bug 13918 - iOS: Reconnecting sometimes fails, and spinner never times out
        if (!progressWindow.hidden)
          [self hideProgressView];
      }
      else if (newState == MZSystemStateWorkspace)
      {
        if  ([[MezzanineAnalyticsManager sharedInstance] canShowAnalyticsOptInDialog])
          [self showAnalyticsOptIn];

        if (_currentCommunicator.systemUseNotification)
          [self showSystemUseNotificationView];
        else
          [self showWorkspaceView:animateIncomingView];
      }
    }
    else if ([keyPath isEqual:@"passphraseRequested"])
    {
      if (_passkeyCoordinator.passkeyIsInTransition)
        return;
      
      if (applicationModel.systemModel.passphraseRequested)
      {
        if (!_passkeyCoordinator.passkeyIsVisible)
        {
          if (connectionViewController.connectionAlertController)
          {
            [connectionViewController.connectionAlertController dismissViewControllerAnimated:YES completion:nil];
            connectionViewController.connectionAlertController = nil;
          }

          // In case any menu is shown it needs to be dismissed first
          UIViewController *modalViewController = [mainNavController presentedViewController];
          if (modalViewController && ![modalViewController isKindOfClass:[PassphraseViewController class]])
            [mainNavController dismissViewControllerAnimated:NO completion:nil];

          // Don't animated when attempting to reconnect since user would catch a brief
          // glimpse of the content underneath
          BOOL animated = !attemptingToReconnect;

          // TODO: WHY???
          [[MezzanineStyleSheet sharedStyleSheet] setVersion:self.applicationModel.systemModel.myMezzanine.version];

          [_passkeyCoordinator presentPasskeyView:animated];
          
          attemptingToConnectWithPassphrase = YES;
        }
        
        if (attemptingToReconnect)
        {
          // When reconnecting (ie, returning from the background), dismiss the reconnection progress
          // if screen so that user can enter the passphrase
          [self hideProgressView];
          attemptingToReconnect = NO;
        }
      }
      else
      {
        // The PassphraseViewController will be dismiss after the MezzanineViewController is loaded.
        // This is to avoid a condition where the the connection view being shown briefly before transitioning (see bug 6876)
        if ((applicationModel.systemModel.state == MZSystemStateNotConnected) && _currentCommunicator.isConnected)
          return;
        
        [_passkeyCoordinator hidePasskeyView];
      }
    }
    else if ([keyPath isEqual:@"popupMessage"])
    {
      NSInteger words = [[applicationModel.systemModel.popupMessage componentsSeparatedByString:@" "] count];
      
      CGFloat time = words / 2.0;
      time = (time < 3.0)? 3.0 : time;
      time = (time > 8.0)? 8.0 : time;
      
      [popupInfoView displayMessage:applicationModel.systemModel.popupMessage withTimeout:time];
    }
  }
  else
  {
    [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
  }
}


#pragma mark - Cache Management

-(void) cleanupOrLeaveCacheForURL:(NSURL *)url
{
  NSString *lastMezz = [[NSUserDefaults standardUserDefaults] lastConnectedMezz];

  if (lastMezz && [lastMezz isEqualToString:url.absoluteString])
    return;

  [[NSUserDefaults standardUserDefaults] setLastConnectedMezz:url.absoluteString];
  [self beginCacheCleanupOperation];
}

-(void) beginCacheCleanupOperation
{
  // Go through and clean up unnecessary files
  NSOperationQueue *queue = [[NSOperationQueue alloc] init];
  
  [queue addOperationWithBlock:^{
    [self cleanupCache];
  }];
}

-(void) cleanupCache
{
  // For now, remove all but at a later date we would want more intelligent caching, especially if
  // offline workspaces are available
  [assetCache removeAll:YES];
}


-(void) cleanupInbox
{
  NSFileManager *fileManager = [NSFileManager defaultManager];
  NSError *error = nil;
  NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
  if (paths.count > 0)
  {
    NSString *documentsPath = paths[0];
    NSString *inboxPath = [documentsPath stringByAppendingPathComponent:@"Inbox"];
    NSArray *contents = [fileManager contentsOfDirectoryAtPath:inboxPath error:&error];
    if (error)
    {
      DLog(@"Error during cleanupInbox: %@", error);
      return;
    }
    
    for (NSString *file in contents)
    {
      NSString *path = [inboxPath stringByAppendingPathComponent:file];
      [fileManager removeItemAtPath:path error:&error];
      
      if (error)
      {
        DLog(@"Error during cleanupInbox: %@", error);
        return;
      }
      DLog(@"Removed inbox item: %@", path);
    }
  }
}


-(void) cleanupExportedDocuments
{
  [applicationModel.systemModel.portfolioExport eraseDownloadFolder];
}


#pragma mark - Memory management

-(void) handleMemoryWarning:(UIApplication *)application
{
  DLog(@"MezzanineAppContext received memory warning");

  // An attempt for fixing bug 9275
  // But it seems canceling doesn't work as the memory is already in plasma
  // the following code is here for reference on how to cancel pending transactions
  /*
  BOOL removedPendingPDFUploads = NO;
  NSArray *pendingTransactions = systemModel.pendingTransactions;
  for (MZTransaction *transaction in pendingTransactions)
  {
    if ([transaction.transactionName isEqual:MZPDFReadyTransactionName])
    {
      [_currentCommunicator cancelTransaction:transaction];
      removedPendingPDFUploads = YES;
    }
  }
  
  if (removedPendingPDFUploads)
  {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error with PDF upload" message:@"The PDF" delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil];
    [alertView show];
  }
   */
}


#pragma mark - OfflineTesting

-(void) loadTestWorkspaceSingleFeld
{
  MZSystemModel *systemModel = self.applicationModel.systemModel;
  MZOfflineCommunicator *testCommunicator = [[MZOfflineCommunicator alloc] init];
  testCommunicator.systemModel = systemModel;
  testCommunicator.serverUrl = [NSURL URLWithString:@"test.local"];
  self.currentCommunicator = testCommunicator;

  testCommunicator.isConnected = YES;
  [systemModel loadTestFeldSingle];
  // Add Surfaces for extended windshield
  [systemModel loadWhiteboards:1];
  [systemModel loadTestWorkspace];
}


-(void) loadTestWorkspaceDiptych
{
  MZSystemModel *systemModel = self.applicationModel.systemModel;
  MZOfflineCommunicator *testCommunicator = [[MZOfflineCommunicator alloc] init];
  testCommunicator.systemModel = systemModel;
  testCommunicator.serverUrl = [NSURL URLWithString:@"test.local"];
  self.currentCommunicator = testCommunicator;

  testCommunicator.isConnected = YES;
  [systemModel loadTestFeldsDouble];
  // Add Surfaces for extended windshield
  [systemModel loadWhiteboards:1];
  [systemModel loadTestWorkspace];
}


-(void) loadTestWorkspaceTriptych
{
  MZSystemModel *systemModel = self.applicationModel.systemModel;
  MZOfflineCommunicator *testCommunicator = [[MZOfflineCommunicator alloc] init];
  testCommunicator.systemModel = systemModel;
  testCommunicator.serverUrl = [NSURL URLWithString:@"test.local"];
  self.currentCommunicator = testCommunicator;

  testCommunicator.isConnected = YES;
  [systemModel loadTestFeldsTriple];
  // Add Surfaces for extended windshield
  [systemModel loadWhiteboards:4];
  [systemModel loadTestWorkspace];
}


-(void) loadTestWorkspaceThreeByTwo
{
  MZSystemModel *systemModel = self.applicationModel.systemModel;
  MZOfflineCommunicator *testCommunicator = [[MZOfflineCommunicator alloc] init];
  testCommunicator.systemModel = systemModel;
  testCommunicator.serverUrl = [NSURL URLWithString:@"test.local"];
  self.currentCommunicator = testCommunicator;

  testCommunicator.isConnected = YES;
  [systemModel loadTestFeldsThreeByTwo];
  // Add Surfaces for extended windshield
  [systemModel loadWhiteboards:4];
  [systemModel loadTestWorkspace];
}


-(void) loadPassphraseView
{
  MZSystemModel *systemModel = self.applicationModel.systemModel;
  MZOfflineCommunicator *testCommunicator = [[MZOfflineCommunicator alloc] init];
  testCommunicator.systemModel = systemModel;
  testCommunicator.serverUrl = [NSURL URLWithString:@"http://test.local"];
  self.currentCommunicator = testCommunicator;
  
  systemModel.passphraseRequested = YES;
}

#pragma mark - iOS 8 Cache Bug - DirectoryWatcherDelegate

// The following is a hacky workaround to clean up the fsCachedData folder in iOS 8.0 ONLY
- (void)directoryDidChange:(DirectoryWatcher *)folderWatcher
{
  NSFileManager *fileManager = [NSFileManager defaultManager];

  NSString *cacheDB = [self applicationCacheDBFfile];

  NSNumber *filesize = @(0);
  if ([fileManager fileExistsAtPath:cacheDB])
  {
    NSDictionary *attributes = [fileManager attributesOfItemAtPath:cacheDB error:nil];
    filesize = attributes[NSFileSize];
  }

  // In iOS 7.x.y and iOS 8.1.y when the <cacheDB> filesize is around 4 MB (3.95) that cache is cleanup
  // So it is a good indicator to know when we should delete its contents, besides
  // this check is quicker than summing all filesizes in cacheData folder.
  if ([filesize longLongValue] > 4100000)
  {
    NSError *error = nil;
    for (NSString *file in [fileManager contentsOfDirectoryAtPath:[self applicationCacheDataFolder] error:&error]) {
      BOOL success = [fileManager removeItemAtPath:[[self applicationCacheDataFolder] stringByAppendingPathComponent:file] error:&error];
      if (!success || error)
      {
        DLog(@"%@: some file in the cleanup could not be deleted", [self applicationCacheDataFolder]);
      }
    }
  }
}


- (NSString *)applicationCacheFolder
{
  NSString *cachesPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
  NSString *bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
  return [cachesPath stringByAppendingPathComponent:bundleIdentifier];
}


- (NSString *)applicationCacheDBFfile
{
  return [[self applicationCacheFolder] stringByAppendingPathComponent:@"Cache.db-wal"];
}


- (NSString *)applicationCacheDataFolder
{
  return [[self applicationCacheFolder] stringByAppendingPathComponent:@"fsCachedData"];
}


#pragma mark Connection Delegate

-(void) connectionViewController:(MezzanineConnectionViewController*)controller
requestsAsyncConnectionAttemptTo:(NSURL*)endPointURL
                         success:(void (^)(void))successBlock
                           error:(void (^)(NSError *))errorBlock
{
  NSString *displayName = [[NSUserDefaults standardUserDefaults] displayName];

  MZCommunicator *communicator = [[MZCommunicator alloc] initWithServerUrl:endPointURL displayName:displayName];
  communicator.machineName = [[UIDevice currentDevice] machine];
  communicator.systemModel = self.applicationModel.systemModel;
  [self setCurrentCommunicator:communicator];
  
  [self addCommunicatorObservationTokensFor:controller success:successBlock error:errorBlock];

  [_currentCommunicator startConnection];
}


-(void) connectionViewControllerWasCancelled:(MezzanineConnectionViewController*)controller
{
  DLog(@"MezzanineAppContext connectionViewControllerWasCancelled");
  [_currentCommunicator disconnect];
}

// Connection failed to connect to mezzanine
//
-(void) failedToConnect:(NSError*)error
{
  NSString *errorDescription = [error localizedDescription];
  DLog(@"MezzanineAppContext failed to connect with reason: %@", errorDescription);
  
  if (attemptingToReconnect)
    return;
  
  if (attemptingToConnectWithPassphrase || self.passkeyCoordinator.passkeyIsVisible)
  {
    attemptingToConnectWithPassphrase = NO;
    return;
  }
  
  // This is a bug in iOS 8.0, 8.1 and 8.2.
  // Workaround: Replace the wrong errorDescription with the standard one.
  // Should be checked against whatever string it has in all localizations supported.
  if ([errorDescription isEqualToString:@"The operation couldn’t be completed. (NSURLErrorDomain error -1009.)"])
    errorDescription = NSLocalizedString(@"Connection Error Internet Not Available", nil);
  
  NSString *genericErrorMessage = NSLocalizedString(@"Connection Generic Error Message", nil);
  
  NSString *title = [error userInfo][@"title"];
  if (!title)
    title = NSLocalizedString(@"Connection Error Dialog Title", nil);
  NSString *message;
  
  NSString *errorDomain = error.domain;
  NSInteger errorCode = [error code];
  
  if (errorCode == kMZCommunicatorErrorCode_InvalidServerCertificate)
    title = NSLocalizedString(@"Connection Error Dialog Title Invalid Certificate", nil);
  else if (errorCode == kMezzanineAppContextErrorCode_RPIncompatibleOSVersion)
    title = NSLocalizedString(@"Connection Error RP Connection Not Compatible Title", nil);
  
  if (errorCode == POOL_SERVER_UNREACH ||
      errorCode == POOL_NO_SUCH_POOL ||
      errorCode == POOL_SEND_BADTH)
    message = [genericErrorMessage stringByAppendingString:NSLocalizedString(@"Connection Error Generic", nil)];
  else if (errorCode == kOBPoolConnectorErrorCode_HostOrNetworkUnreachable)
    message = NSLocalizedString(@"Connection Error Host Or Network Unreachable", nil);
  else if (errorCode == kMZCommunicatorErrorCode_LoginTimeout ||
           errorCode == kMZCommunicatorErrorCode_ConnectionTimeout)
    message = NSLocalizedString(@"Connection Error Timeout", nil);
  else if (errorCode == kMZCommunicatorErrorCode_InvalidServerCertificate)
    message = NSLocalizedString(@"Connection Error Invalid Server Certificate", nil);
  else if (errorCode == kMezzanineAppContextErrorCode_RPIncompatibleOSVersion)
    message = NSLocalizedString(@"Connection Error RP Connection Not Compatible Message", nil);
  else if (errorDescription.length > 0)
    message = errorDescription;
  else
    message = [genericErrorMessage stringByAppendingString:@"."];
  
  UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
  UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Generic Button Title Okay", nil) style:UIAlertActionStyleDefault handler:nil];
  [alertController addAction:okAction];
  [self showAlertController:alertController animated:YES completion:nil];
}


// Connection from mezzanine was severed not because of user disconnection
//
-(void) communicatorWasDisconnected:(NSNotification*)notification
{
  // If the app is in background there's no need to show this because we'll reconnect
  if (!_inBackground) {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Connection Lost Dialog Title", nil)
                                                                             message:NSLocalizedString(@"Connection Lost Dialog Message", nil)
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Generic Button Title Okay", nil) style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:okAction];
    [self showAlertController:alertController animated:YES completion:nil];
  }
}

// This is called in all cases where the app was disconnected from mezzanine
// Either because of connectivity issues, or because of a user-disconnect.
//
-(void) communicatorDidDisconnect:(NSNotification*)notification
{
  // Analytics
  if (notification.userInfo) {
    MezzanineAnalyticsManager *analytics = [MezzanineAnalyticsManager sharedInstance];
    NSString *disconnectionReason = notification.userInfo[@"message"];
    [analytics trackDisconnectionEventWithDisconnectionReason:disconnectionReason isRemote:_currentCommunicator.isARemoteParticipationConnection];
  }

#if DISCONNECT_IN_BACKGROUND
  if (_inBackground)
    return;
#endif

  MZCommunicator *communicator = [notification object];

  // Remove possible objects that may have a reference to the communicator
  [self endObservingCommunicator:communicator];
  communicator.systemModel = nil;
  [self setCurrentCommunicator:nil];

  [self unhookViewControllersFromModel];
  
  self.applicationModel.systemModel.state = MZSystemStateNotConnected;
  [self resetModelAndCaches];
  [self.infopresenceManager reset];

  self.attemptingToConnectWithPassphrase = NO;
  initAudioOnlySession = NO;

  mezzanineStateAnalyticsData = nil;
  
  [UIApplication sharedApplication].idleTimerDisabled = NO;
}

-(void) communicatorRequestRemoteParticipationSettings:(NSNotification*)notification
{
  remoteParticipationJoinViewController = [[RemoteParticipationJoinViewController alloc] initWithNibName:@"RemoteParticipationJoinViewController" bundle:nil];
  remoteParticipationJoinViewController.delegate = self;
  remoteParticipationJoinViewController.communicator = _currentCommunicator;
  [mainNavController pushViewController:remoteParticipationJoinViewController animated:YES];
}

#pragma mark ConnectWarningViewControllerDelegate

- (void)systemUseNotificationViewControllerDidAgree:(BOOL)agreement
{
  if (!agreement)
    [_currentCommunicator disconnect];
  else
    [self showWorkspaceView:true];
}


#pragma mark RemoteParticipationJoinViewController

- (void)remoteParticipationJoinViewControllerShouldConnect:(RemoteParticipationJoinViewController *)rpVC name:(NSString *)name audioMuted:(BOOL)audioMuted videoMuted:(BOOL)videoMuted audioOnly:(BOOL)audioOnly
{
  [NSUserDefaults standardUserDefaults].displayName = name;

  [_pexipManager setVideoMuted:videoMuted];
  [_pexipManager setAudioMuted:audioMuted];
  initAudioOnlySession = audioOnly;

  // TODO - Include passphrase in RP Join View and then in the communicator request
  [self.currentCommunicator requestJoinWithRemoteParticipantName:name passphrase:nil];
}

- (void)remoteParticipationJoinViewControllerShouldCancelConnection:(RemoteParticipationJoinViewController *)rpVC
{  
  // 18371. If we cancel we should reset as well the server url otherwise next attempt won't happen because we avoid to connect to the same URL.
  _currentCommunicator.serverUrl = nil;
  
  [mainNavController popViewControllerAnimated:YES];
  [mainNavController.transitionCoordinator animateAlongsideTransition:nil completion:^(id<UIViewControllerTransitionCoordinatorContext> context) {
    [self endObservingCommunicator:_currentCommunicator];
    remoteParticipationJoinViewController.communicator = nil;
    remoteParticipationJoinViewController = nil;
  }];
}


#pragma mark Private

- (void)unhookViewControllersFromModel
{
  MezzanineRootViewController *viewController = [self mezzanineViewController];
  if (viewController)
  {
    viewController.communicator = nil;
    viewController.systemModel = nil; // This helps de-observation of properties of all child view controllers, cause of bug 12220 12249
    viewController.appContext = nil;
  }

  // TODO: Probably not needed
  _passkeyCoordinator.communicator = nil;
}


-(void) resetModelAndCaches
{
  [self.applicationModel reset];
  
  // Remove in-memory image caches
  NIMemoryCache *memoryCache = [Nimbus imageMemoryCache];
  [memoryCache removeAllObjects];
  
  [[MZDownloadManager sharedLoader] cancelAllOperations];
}

#pragma mark 

- (void)addMezzanineObservationTokens:(MZMezzanine *)thisMezzanine
{
  if (!mezzanineObservationTokens)
    mezzanineObservationTokens = [NSMutableArray new];

  __weak typeof(self) __weakSelf = self;

  id token;

  token = [thisMezzanine addObserverForKeyPath:@"name" options:(NSKeyValueObservingOptionNew) onQueue:[NSOperationQueue mainQueue] task:^(id object, NSDictionary *change) {
    [[MezzanineAnalyticsManager sharedInstance] setRoomIDWithRoomID:__weakSelf.applicationModel.systemModel.myMezzanine.name];
    [[MezzanineAnalyticsManager sharedInstance] dispatchEnqueuedEvents];
  }];

  [mezzanineObservationTokens addObject:token];

  token = [thisMezzanine addObserverForKeyPath:@"company" options:(NSKeyValueObservingOptionNew) onQueue:[NSOperationQueue mainQueue] task:^(id object, NSDictionary *change) {
    [[MezzanineAnalyticsManager sharedInstance] setCustomerIDWithCustomerID:__weakSelf.applicationModel.systemModel.myMezzanine.company];
    [[MezzanineAnalyticsManager sharedInstance] dispatchEnqueuedEvents];
  }];

  [mezzanineObservationTokens addObject:token];

  token = [thisMezzanine addObserverForKeyPath:@"model" options:(NSKeyValueObservingOptionNew) onQueue:[NSOperationQueue mainQueue] task:^(id object, NSDictionary *change) {
    [[MezzanineAnalyticsManager sharedInstance] setMezzanineModelWithMezzanineModel:__weakSelf.applicationModel.systemModel.myMezzanine.model];
    [[MezzanineAnalyticsManager sharedInstance] dispatchEnqueuedEvents];
  }];

  [mezzanineObservationTokens addObject:token];
}

- (void)endObservingMezzanine:(MZMezzanine *)thisMezzanine
{
  for (id token in mezzanineObservationTokens)
    [thisMezzanine removeObserverWithBlockToken:token];

  [mezzanineObservationTokens removeAllObjects];
}


#pragma mark Communicator observation

- (void)addCommunicatorObservationTokensFor:(MezzanineConnectionViewController *)controller success:(void (^)(void))successBlock error:(void (^)(NSError *))errorBlock
{
  if (!communicatorObservationTokens)
    communicatorObservationTokens = [[NSMutableArray alloc] init];
  
  __weak typeof(self) __weakSelf = self;
  __weak typeof(_currentCommunicator) __weakCommunicator = _currentCommunicator;
  
  id token;
  token =
  [[NSNotificationCenter defaultCenter] addObserverForName:MZCommunicatorBeganJoinNotification
                                                    object:_currentCommunicator
                                                     queue:[NSOperationQueue mainQueue]
                                                usingBlock:^(NSNotification *notification) {
                                                  if (controller.connectionAlertController)
                                                  {
                                                    [controller.connectionAlertController dismissViewControllerAnimated:YES completion:nil];
                                                    controller.connectionAlertController = nil;
                                                  }

                                                  [controller.connectButton setTitle:NSLocalizedString(@"Connect Button Joining Session", nil) forState:UIControlStateNormal];
                                                  [controller.connectButton setTitle:NSLocalizedString(@"Connect Button Joining Session", nil) forState:UIControlStateDisabled];
                                                }];
  [communicatorObservationTokens addObject:token];
  
  // Join success case
  token =
  [[NSNotificationCenter defaultCenter] addObserverForName:MZCommunicatorJoinSuccesNotification
                                                    object:_currentCommunicator
                                                     queue:[NSOperationQueue mainQueue]
                                                usingBlock:^(NSNotification *notification) {
                                                }];
  [communicatorObservationTokens addObject:token];
  
  // Join failure case
  
  token =
  [[NSNotificationCenter defaultCenter] addObserverForName:MZCommunicatorJoinFailedNotification
                                                    object:_currentCommunicator
                                                     queue:[NSOperationQueue mainQueue]
                                                usingBlock:^(NSNotification *notification) {
                                                  NSError *error = [notification userInfo][@"error"];
                                                  
                                                  if (attemptingToReconnect)
                                                  {
                                                    [__weakSelf hideProgressView];
                                                    attemptingToReconnect = NO;
                                                  }

                                                  if (__weakSelf.connectionViewController.connectionAlertController)
                                                  {
                                                    [__weakSelf.connectionViewController.connectionAlertController dismissViewControllerAnimated:YES completion:nil];
                                                    __weakSelf.connectionViewController.connectionAlertController = nil;
                                                  }

                                                  [__weakSelf failedToConnect:error];
                                                  
                                                  if (errorBlock)
                                                    errorBlock(error);
                                                  
                                                  
                                                  // In the case of rejoining after backgrounding
                                                  if (__weakSelf.applicationModel.systemModel.state != MZSystemStateNotConnected)
                                                    __weakSelf.applicationModel.systemModel.state = MZSystemStateNotConnected;
                                                  
                                                  // In the case of join failed because user doesn't have
                                                  // the correct password
                                                  if (__weakSelf.applicationModel.systemModel.passphraseRequested || __weakSelf.passkeyCoordinator.passkeyViewController)
                                                    __weakSelf.applicationModel.systemModel.passphraseRequested = NO;
                                                  
                                                  // Ending communicator observation seem to release this block while its
                                                  // being executed, and a fix is to dispatch the following calls in a
                                                  // separate block
                                                  [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                                    [__weakSelf endObservingCommunicator:__weakCommunicator];
                                                    [__weakSelf setCurrentCommunicator:nil];
                                                    [__weakSelf resetModelAndCaches];
                                                  }];
                                                }];
  [communicatorObservationTokens addObject:token];
  
  
  // After join, communicator will begin requesting state
  token =
  [[NSNotificationCenter defaultCenter] addObserverForName:MZCommunicatorBeganRequestingStateNotification
                                                    object:_currentCommunicator
                                                     queue:[NSOperationQueue mainQueue]
                                                usingBlock:^(NSNotification *notification) {
                                                  [controller.connectButton setTitle:NSLocalizedString(@"Connect Button Receiving State", nil) forState:UIControlStateNormal];
                                                  [controller.connectButton setTitle:NSLocalizedString(@"Connect Button Receiving State", nil) forState:UIControlStateDisabled];
                                                  
                                                  // Note to self - perhaps this is best handled when state is received?
                                                  if (attemptingToReconnect)
                                                    [__weakSelf hideProgressView];
                                                }];
  [communicatorObservationTokens addObject:token];
  
  
  // Finally, once the mez-state protein is received we dismiss the connection view
  // and push in the mezzanine view
  token =
  [[NSNotificationCenter defaultCenter] addObserverForName:MZCommunicatorReceviedInitialStateNotification
                                                    object:_currentCommunicator
                                                     queue:[NSOperationQueue mainQueue]
                                                usingBlock:^(NSNotification *notification) {
                                                  
                                                  if (attemptingToReconnect)
                                                    attemptingToReconnect = NO;
                                                  else
                                                    [[MezzanineStyleSheet sharedStyleSheet] setVersion:__weakSelf.applicationModel.systemModel.myMezzanine.version];
                                                  
                                                  // All the successBlock contains its connectSucceded call
                                                  if (successBlock)
                                                    successBlock();

                                                  if (__weakCommunicator.isARemoteParticipationConnection)
                                                    [UIApplication sharedApplication].idleTimerDisabled = YES;

                                                  [__weakSelf checkInitialStateAndSendAnalyticsEvent];
                                                }];
  [communicatorObservationTokens addObject:token];

  // Fill up mezzanine initial state dictionary to track analytics
  token =
  [[NSNotificationCenter defaultCenter] addObserverForName:MZCommunicatorReceviedInitialWorkspaceListStateNotification
                                                    object:_currentCommunicator
                                                     queue:[NSOperationQueue mainQueue]
                                                usingBlock:^(NSNotification *notification) {
                                                  MezzanineAppContext *strongSelf = __weakSelf;
                                                  MZSystemModel *systemModel = __weakCommunicator.systemModel;
                                                  NSNumber *workspaceCount = @(systemModel.countOfWorkspaces);
                                                  [strongSelf->mezzanineStateAnalyticsData setObject:workspaceCount forKey:kWorkspaceCount];
                                                  [__weakSelf checkInitialStateAndSendAnalyticsEvent];
                                                }];
  [communicatorObservationTokens addObject:token];

  token =
  [[NSNotificationCenter defaultCenter] addObserverForName:MZCommunicatorReceviedInitialInfopresenceStateNotification
                                                    object:_currentCommunicator
                                                     queue:[NSOperationQueue mainQueue]
                                                usingBlock:^(NSNotification *notification) {
                                                  MezzanineAppContext *strongSelf = __weakSelf;
                                                  MZSystemModel *systemModel = __weakCommunicator.systemModel;
                                                  NSNumber *remoteMezzCount = @(systemModel.remoteMezzes.count);
                                                  [strongSelf->mezzanineStateAnalyticsData setObject:remoteMezzCount forKey:kRemoteMezzCount];
                                                  [__weakSelf checkInitialStateAndSendAnalyticsEvent];
                                                }];
  [communicatorObservationTokens addObject:token];
  
  
  token =
  [[NSNotificationCenter defaultCenter] addObserverForName:OBCommunicatorWasDisconnectedNotification
                                                    object:_currentCommunicator
                                                     queue:[NSOperationQueue mainQueue]
                                                usingBlock:^(NSNotification *notification) {
                                                  [__weakSelf communicatorWasDisconnected:notification];
                                                }];
  [communicatorObservationTokens addObject:token];
  
  
  token =
  [[NSNotificationCenter defaultCenter] addObserverForName:MZCommunicatorDidDisconnectNotification
                                                    object:_currentCommunicator
                                                     queue:[NSOperationQueue mainQueue]
                                                usingBlock:^(NSNotification *notification) {
                                                  [__weakSelf communicatorDidDisconnect:notification];
                                                }];
  [communicatorObservationTokens addObject:token];

  token = [[NSNotificationCenter defaultCenter] addObserverForName:MZCommunicatorRequestRemoteParticipationSettingsNotification
                                                    object:_currentCommunicator
                                                     queue:[NSOperationQueue mainQueue]
                                                usingBlock:^(NSNotification *notification) {
                                                  
                                                  // If we come from an external URL, remove the connecting alert
                                                  if (__weakSelf.connectionViewController.connectionAlertController)
                                                  {
                                                    [__weakSelf.connectionViewController.connectionAlertController dismissViewControllerAnimated:YES completion:nil];
                                                    __weakSelf.connectionViewController.connectionAlertController = nil;
                                                  }
                                                  
                                                  if (![[UIDevice currentDevice] isIOS9OrAbove])
                                                  {
                                                    [__weakCommunicator disconnect];
                                                    
                                                    NSError *error = [[NSError alloc] initWithDomain:@"MezzanineAppContext" code:kMezzanineAppContextErrorCode_RPIncompatibleOSVersion userInfo:nil];
                                                    __weak typeof(self) __self = self;
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                      [__self failedToConnect:error];
                                                      if (errorBlock) {
                                                        errorBlock(error);
                                                      }
                                                    });
                                                    return;
                                                  }
                                                  
                                                  [__weakSelf communicatorRequestRemoteParticipationSettings:notification];
                                                }];
  [communicatorObservationTokens addObject:token];
}


-(void) endObservingCommunicator:(MZCommunicator*)communicator
{
  for (id token in communicatorObservationTokens)
    [[NSNotificationCenter defaultCenter] removeObserver:token];
  [communicatorObservationTokens removeAllObjects];
}


#pragma mark - Pexip Conference Mode Management

-(void)setAudioOnlyMode:(BOOL)active
{
  if (active) {
    MZDownloadManager.sharedLoader.pauseDownloads = YES;
  }
  else {
    MZDownloadManager.sharedLoader.pauseDownloads = NO;
    [self resetImageAvailability];
  }
}

- (void)resetImageAvailability
{
  MZWorkspace *currentWorkspace = _currentCommunicator.systemModel.currentWorkspace;
  for (MZSlide *slide in currentWorkspace.presentation.slides)
  {
    if (slide.imageAvailable){
      slide.imageAvailable = YES;
    }
  }

  for (MZItem *item in currentWorkspace.windshield.items)
  {
    if (item.imageAvailable) {
      item.imageAvailable = YES;
    }
  }
}


#pragma mark - Background / Foreground app states

-(void)setInBackground:(BOOL)inBackground
{
  _inBackground = inBackground;

  BOOL hideableWindowSetHidden = _inBackground ? NO : YES;
  [OBDragDropManager sharedManager].overlayWindow.hidden = hideableWindowSetHidden;
  popupInfoWindow.hidden = hideableWindowSetHidden;
  progressWindow.hidden = hideableWindowSetHidden;
  [_infopresenceManager hideWindow:hideableWindowSetHidden];
}


@end
