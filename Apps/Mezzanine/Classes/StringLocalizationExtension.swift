//
//  StringLocalizationExtension.swift
//  Mezzanine
//
//  Created by miguel on 24/6/15.
//  Copyright (c) 2015 Oblong Industries. All rights reserved.
//

import Foundation

extension String {
  var localized: String {
    return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
  }
}
