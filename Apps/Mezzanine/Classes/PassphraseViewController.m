//
//  PassphraseViewController.m
//  Mezzanine
//
//  Created by Miguel Sánchez Valdés on 3/26/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import "PassphraseViewController.h"
#import "PassphraseViewController_Private.h"
#import "UIViewController+Three20.h"
#import "MezzanineStyleSheet.h"
#import "MezzanineConnectionViewController.h"
#import "Mezzanine-Swift.h"

#define FIELD_WIDTH_3 50.0
#define FIELD_WIDTH_6 32.0
#define FIELD_HEIGHT_3 44.0
#define FIELD_HEIGHT_6 32.0
#define FIELD_TEXT_SIZE_3 40.0
#define FIELD_TEXT_SIZE_6 30.0
#define UNDERLINE_HEIGHT 2.0
#define FIELD_GAP 6.0
#define GROUP_GAP 8.0

#define TITLE_TEXT_SIZE 32.0
#define INSTRUCTIONS_TEXT_SIZE 14.0


@implementation PassphraseViewController

-(instancetype) initWithKeyLength:(NSInteger)length
{
  self = [super init];
  if (self)
  {
    self.numOfFields = length;
  }
  return self;
}

- (void)dealloc
{
  DLog(@"%@ dealloc", [self class]);
}


#pragma mark - View lifecycle

- (void)viewDidLoad
{
  [super viewDidLoad];

  _textFieldArray = [NSMutableArray array];
  _underlinesArray = [NSMutableArray array];
  
  MezzanineStyleSheet *styleSheet = [MezzanineStyleSheet sharedStyleSheet];
  
  self.view.backgroundColor = styleSheet.grey30Color;
  
  UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(textFieldsTapped:)];
  [_textFieldsContainer addGestureRecognizer:tapGestureRecognizer];
  
  _titleLabel.textColor = styleSheet.grey150Color;
  _titleLabel.font = [UIFont dinLightOfSize:TITLE_TEXT_SIZE];
  _titleLabel.text = NSLocalizedString(@"Passkey View Title", nil);
  
  _instructionsLabel.textColor = styleSheet.grey190Color;
  _instructionsLabel.font = [UIFont dinOfSize:INSTRUCTIONS_TEXT_SIZE];
  _instructionsLabel.text = [NSString stringWithFormat: NSLocalizedString(@"Passkey View Hint", nil), _numOfFields];
  
  
  [_cancelButton setTitle:NSLocalizedString(@"Generic Button Title Cancel", nil) forState:UIControlStateNormal];
  [styleSheet stylizeButton:_cancelButton];
  
  [self buildFields];
  [self callFirstResponder];
  [self updateLineViews];
  
  self.navigationController.navigationBarHidden = NO;
}


- (void)viewDidAppear:(BOOL)animated
{
  [super viewDidAppear:animated];
  
  // Analytics
  MezzanineAnalyticsManager *analyticsManager = [MezzanineAnalyticsManager sharedInstance];
  [analyticsManager tagScreen:analyticsManager.passkeyViewScreen];
  
}


- (void)viewWillDisappear:(BOOL)animated
{
  [super viewWillDisappear:animated];
  
  // Analytics
  UIViewController *lastViewController = [((MezzanineNavigationController *)self.presentingViewController).viewControllers lastObject];
  MezzanineAnalyticsManager *analyticsManager = [MezzanineAnalyticsManager sharedInstance];
  if ([lastViewController isKindOfClass:[MezzanineConnectionViewController class]]) {
    [analyticsManager tagScreen:analyticsManager.connectionViewScreen];
  } else {
    [analyticsManager tagWorkspaceStatusScreen];
  }
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
  return UIInterfaceOrientationMaskAll;
}


- (void)callFirstResponder
{
  [((UITextField *)[_textFieldArray firstObject]) becomeFirstResponder];
}


- (void)buildFields
{
  CGFloat width = _numOfFields == 3 ? FIELD_WIDTH_3 : FIELD_WIDTH_6;
  CGFloat textSize = _numOfFields == 3 ? FIELD_TEXT_SIZE_3 : FIELD_TEXT_SIZE_6;
  CGFloat tfHeight = _numOfFields == 3 ? FIELD_HEIGHT_3 : FIELD_HEIGHT_6;
  CGFloat lHeight = UNDERLINE_HEIGHT;
  
  CGFloat marginX = FIELD_GAP;
  CGFloat groupMarginX = _numOfFields == 3 ? 0.0 : GROUP_GAP;
  CGFloat offsetX = 0.0;
  
  CGFloat totalWidth = (_numOfFields * width) + ((_numOfFields - 1) * marginX) + groupMarginX;
  _fieldContainerWidthConstraint.constant = totalWidth;
  _fieldContainerHeightConstraint.constant = tfHeight + FIELD_GAP;
  
  [self.view layoutIfNeeded];
  
  for (NSInteger count = 0; count < _numOfFields; count++) {
    UITextField *tf = [[UITextField alloc] initWithFrame:CGRectMake(offsetX, 0.0, width, tfHeight)];
    tf.tag = count;
    tf.delegate = self;
    tf.textColor = [UIColor whiteColor];
    tf.textAlignment = NSTextAlignmentCenter;
    tf.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    tf.autocorrectionType = UITextAutocorrectionTypeNo;
    tf.keyboardType = _numOfFields == 3 ? UIKeyboardTypeDefault : UIKeyboardTypeNumberPad;
    tf.font = [UIFont dinLightOfSize:textSize];
    tf.accessibilityLabel = [NSString stringWithFormat:@"PasskeyField%li", (long)count];
    [_textFieldArray addObject:tf];
    
    UIView *l = [[UIView alloc] initWithFrame:CGRectMake(offsetX, _fieldContainerHeightConstraint.constant - lHeight, width, lHeight)];
    l.tag = count;
    l.backgroundColor = [UIColor whiteColor];
    [_underlinesArray addObject:l];
    
    
    [_textFieldsContainer addSubview:tf];
    [_textFieldsContainer addSubview:l];
    
    offsetX += width + marginX;
    if (count == 2) {
      offsetX += groupMarginX;
    }
  }
}

- (void)textFieldsTapped:(UITapGestureRecognizer*)recognizer
{
  [self startOver];
}

- (void)startOver
{
  for (UITextField *tf in _textFieldArray) {
    tf.text = @"";
  }
  
  [self callFirstResponder];
  [self updateLineViews];
}


// This is a hack to accept only one character in every UITextField and jump to the next/prev one
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
  if (_inTextFieldChangeCallback)
    return YES;
  
  _inTextFieldChangeCallback = YES;
  
  BOOL autoJoin = NO;
  
  
  NSCharacterSet *numbersOnly = [NSCharacterSet decimalDigitCharacterSet];
  NSCharacterSet *characterSetFromString = [NSCharacterSet characterSetWithCharactersInString:string];
  
  if (![numbersOnly isSupersetOfSet:characterSetFromString] && _numOfFields == 6) {
    _inTextFieldChangeCallback = NO;
    return false;
  }
  
  
  if (! [string isEqualToString:@""]) {
    for (UITextField *tf in _textFieldArray) {
      NSInteger idx = [tf tag];
      if (textField == tf && idx < (_numOfFields - 1)) {
        [((UITextField *)[_textFieldArray objectAtIndex:[tf tag] + 1]) becomeFirstResponder];
        [self updateLineViews];
        break;
      } else if (idx == (_numOfFields - 1)) {
        autoJoin = true;
        [tf resignFirstResponder];
      }
    }
  } else {
    for (UITextField *tf in _textFieldArray) {
      NSInteger idx = [tf tag];
      if (textField == tf && idx > 0) {
        [((UITextField *)[_textFieldArray objectAtIndex:[tf tag] - 1]) becomeFirstResponder];
        [self updateLineViews];
        break;
      }
    }
  }
  
  [textField setText:string];
  
  if (autoJoin)
    [self join];
  
  _inTextFieldChangeCallback = NO;
  
  return NO;
}


// To prevent users from hitting Next and advancing to the next textfield without
// having entered a letter
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
  return NO;
}


// Needed for empty UITextField to accept the backwards key
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
  textField.text = @"\u200B";
}


- (void)updateLineViews
{
  // Notify coordinator there's been a change
  if ([_delegate respondsToSelector:@selector(passkeyCharacterEntered)]) {
    [_delegate passkeyCharacterEntered];
  }

  for (UITextField *tf in _textFieldArray) {
    if ([tf isFirstResponder]) {
      [UIView animateWithDuration:0.3 animations:^{
        for (UIView *l in self.underlinesArray) {
          l.backgroundColor = [UIColor whiteColor];
        }
        
        UIView *l = [self.underlinesArray objectAtIndex:[tf tag]];
        l.backgroundColor = [MezzanineStyleSheet sharedStyleSheet].superHighlightColor;
      }];
      
      return;
    }
  }
  
  // By default, first field is highlighted otherwise
  ((UIView *)[_underlinesArray firstObject]).backgroundColor = [MezzanineStyleSheet sharedStyleSheet].superHighlightColor;
}


- (IBAction)dismiss:(id)sender
{
  if ([_delegate respondsToSelector:@selector(passkeyCancelled)]) {
    [_delegate passkeyCancelled];
  }
}

- (void)join
{
  NSMutableString *passphrase = [NSMutableString stringWithString:@""];
  
  for (UITextField *tf in _textFieldArray) {
    [passphrase appendString:tf.text];
  }

  if ([_delegate respondsToSelector:@selector(passkeyWasIntroduced:)]) {
    [_delegate passkeyWasIntroduced:passphrase];
  }
}

- (void)presentIncorrectPasskeyAlert
{
  __weak typeof(self) __self = self;
  UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Passkey Incorrect Dialog Title", nil)
                                                                           message:NSLocalizedString(@"Passkey Incorrect Dialog Message", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
  UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Generic Button Title Okay", nil)
                                                         style:UIAlertActionStyleCancel
                                                       handler:^(UIAlertAction * _Nonnull action) {
                                                         [__self startOver];
                                                       }];
  [alertController addAction:cancelAction];
  [self presentViewController:alertController animated:YES completion:nil];
}


@end
