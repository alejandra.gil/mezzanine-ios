//
//  MezzanineDocumentInteractionController.h
//  Mezzanine
//
//  Created by Zai Chang on 9/18/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>


#define MezzanineDocumentInteractionControllerDomain @"MezzanineDocumentInteractionController"

#define MezzanineFileUploadError -2200
#define MezzanineFileFormatNotSupportedError -2201
#define MezzanineUnsupportedImageError -2202


@interface MezzanineDocumentInteractionController : NSObject
{
  NSOperationQueue *pdfRenderingQueue;
  NSInteger numberOfUploadsInProgress;
}
@property (nonatomic, assign) NSInteger maximumFileSizeInBytes;

// TODO - Put these flags in the upload task in order to support concurrent or queued PDF uploads
@property (nonatomic, assign) BOOL isUploadingToParamus;
@property (nonatomic, assign) BOOL isUploadingToDeck;


-(BOOL) canUploadDocumentAtURL:(NSURL*)url error:(NSError**)error;
-(NSInteger) numberOfPagesForDocumentAtURL:(NSURL*)url;
-(void) uploadDocumentAtURL:(NSURL*)url usingCommunicator:(MZCommunicator*)communicator;
-(void) uploadSlidesWithText:(NSString *)text;

-(BOOL) isBusy;
-(void) cancel;

@end
