//
//  SystemUseNotificationViewController.swift
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 06/07/2017.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import UIKit

@objc protocol SystemUseNotificationViewControllerDelegate: NSObjectProtocol {
  func systemUseNotificationViewControllerDidAgree(_ agreement: Bool)
}

class SystemUseNotificationViewController: UIViewController {
  
  @IBOutlet var titleLabel: UILabel!
  @IBOutlet var messageTextView: UITextView!
  @IBOutlet var okButton: UIButton!
  @IBOutlet var cancelButton: UIButton!
  @IBOutlet var messageTextViewHeightConstraint: NSLayoutConstraint!
  
  @objc weak var delegate: SystemUseNotificationViewControllerDelegate?
  @objc var systemUseNotificationString: String!
  var nextTraitCollection: UITraitCollection!
  
  override var preferredStatusBarStyle : UIStatusBarStyle {
    return .lightContent
  }
  
  override var shouldAutorotate : Bool {
    return traitCollection.isCompactWidth() && traitCollection.isCompactHeight()
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    view.backgroundColor = MezzanineStyleSheet.shared().grey30Color
    
    // titleLabel
    titleLabel.font = UIFont.dinMedium(ofSize: 26)
    titleLabel.text = "System Use Notification".localized

    // messageLabel
    messageTextView.text = systemUseNotificationString
    messageTextView.textColor = UIColor.white
    
    // ok button
    MezzanineStyleSheet.shared().stylizeCall(toActionButton: okButton)
    okButton.layer.cornerRadius = 3.0;
    okButton.clipsToBounds = true;
    okButton.setTitle("Ok".localized.uppercased(), for: UIControlState())
    
    // cancel
    cancelButton.setTitle("Cancel".localized.uppercased(), for: UIControlState())
    cancelButton.titleLabel?.font = UIFont.dinMedium(ofSize: 14.0)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    messageTextView.font = self.traitCollection.isiPad() ? UIFont.dinMedium(ofSize: 16) : UIFont.dinMedium(ofSize: 14)
  }
  
  override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
    nextTraitCollection = newCollection
  }
  
  override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
    if nextTraitCollection != nil {
      return
    }
    
    updateMessageHeight(size)
  }
  
  override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
    nextTraitCollection = nil
    updateMessageHeight(view.frame.size)
  }
  
  func updateMessageHeight(_ size: CGSize) {
    // Let's assume height will be maximum half of the view size
    messageTextViewHeightConstraint.constant = size.height * 0.5
  }
  
  @IBAction func okButtonAction(_ sender: UIButton) {
    self.delegate?.systemUseNotificationViewControllerDidAgree(true)
    dismiss(animated: true, completion: nil)
  }

  @IBAction func cancelButtonAction(_ sender: UIButton) {
    self.delegate?.systemUseNotificationViewControllerDidAgree(false)
    dismiss(animated: true, completion: nil)
  }
}
