//
//  WelcomeHintView.h
//  Mezzanine
//
//  Created by miguel on 23/07/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WelcomeHintView : UIView

@property (nonatomic, strong) UIImageView *logoImageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, assign) CGFloat zoomScale;
@property (nonatomic, assign) BOOL alreadyLoaded;

@end
