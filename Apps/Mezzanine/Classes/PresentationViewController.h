//
//  PresentationViewController.h
//  Mezzanine
//
//  Created by Zai Chang on 10/20/10.
//  Copyright 2010 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "MZWorkspace.h"
#import "SlideLayer.h"
#import "OBUmbilicalLayer.h"
#import "FPPopoverController.h"

@class WorkspaceViewController;

@protocol PresentationViewControllerDelegate <NSObject>

- (CGFloat)presentationViewControllerZoomState;
- (void)presentationViewUpdateWorkspaceViewControllerDeckSlider;

@end

/// Controller for the presentation display in the main workspace area

@interface PresentationViewController : UIViewController <UIGestureRecognizerDelegate, FPPopoverControllerDelegate>
{  
  // Gesture recognition
//  UIPanGestureRecognizer *panRecognizer;
  BOOL deckPanInProgress;
  CGPoint initialPanTranslation;
  CGPoint lastPanTranslation;

  // Slides
  CGPoint slideContainerTransformNudge; // To display in-progress pan gesture
  
  // Felds support
  NSInteger feldIndex;
  UIView *feldBoundViewsContainer;
  
  // Local view states
  NSInteger currentSlideIndex;

  UITapGestureRecognizer *presentationTapGesture;
}

@property (nonatomic, weak) id <PresentationViewControllerDelegate> delegate;

@property (nonatomic, assign) MZCommunicator *communicator;
@property (nonatomic, strong) MZSystemModel *systemModel;
@property (nonatomic, strong) MZWorkspace *workspace;
@property (nonatomic, strong) MZPresentation *presentation;

@property (nonatomic, weak) WorkspaceViewController *workspaceViewController;
@property (nonatomic, readonly) NSInteger currentSlideIndex;
@property (nonatomic, assign) BOOL sliderIsBeingDragged; 

@property (nonatomic, assign) UIEdgeInsets contentInset;  // Padding for the layout of felds and slides

// Left: 0   Center: 1   Right: 2 for a triptych
@property (nonatomic, assign) NSInteger feldIndex;

-(CGSize) getFeldSize; // Size of the area displayed by a feld in device coordinates
- (CGFloat)spaceBetweenFelds; // Needed for teamwork UI

-(SlideLayer*) layerForSlide:(MZPortfolioItem*)slide;
-(BOOL) revealSlideAtIndex:(NSInteger)index;
-(void) revealCurrentSlide;
-(void) refreshLayout;


// Coordinates transformation
-(CGRect) rectInViewCoordinates:(CGRect)rectInSlidesCoordinates;
-(CGRect) rectInSlidesCoordinates:(CGRect)rectInViewCoordinates;
-(CGPoint) pointInViewCoordinates:(CGPoint)pointInSlidesCoordinates;
-(CGPoint) pointInSlidesCoordinates:(CGPoint)pointInViewCoordinates;


-(void) refreshFeldBoundViews;
-(void) updateFeldBoundViews;


-(void) goToSlideAtIndex:(NSInteger)slideIndex;

- (void)handleDeckPanGesture:(UIPanGestureRecognizer*)recognizer;
- (void)viewPortSingleTap:(UITapGestureRecognizer *)recognizer;

@end
