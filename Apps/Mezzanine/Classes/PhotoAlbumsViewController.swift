//
//  PhotoAlbumsViewController.swift
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 03/01/2017.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import UIKit

class PhotoAlbumsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, PhotoSelectionViewControllerDelegate {
  
  fileprivate var tableView: UITableView!
  fileprivate var albums: Array<PhotoAlbum>

  fileprivate weak var containerController: PhotoPickerController!
  weak var delegate: PhotoPickerDelegate?
  
  fileprivate let RowHeight: CGFloat = 70.0
  fileprivate let TableViewPadding: CGFloat = 20.0
  
  init(containerController: PhotoPickerController, albums: Array<PhotoAlbum>) {
    self.albums = albums
    self.containerController = containerController
    
    super.init(nibName: nil, bundle: nil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    title = "OBImagePicker Photo Library Title".localized
    navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.cancel))
    self.view.accessibilityIdentifier = "OBAlbumsController"
    
    // Table configuration
    let frame = CGRect(x: 0.0, y: -TableViewPadding, width: self.view.bounds.size.width, height: self.view.bounds.size.height + TableViewPadding)
    tableView = UITableView(frame: frame)
    tableView.rowHeight = RowHeight
    tableView.separatorStyle = .none
    tableView.contentInset = UIEdgeInsetsMake(TableViewPadding, 0, TableViewPadding, 0)
    tableView.delegate = self
    tableView.dataSource = self
    tableView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    tableView.register(PhotoAlbumTableViewCell.self, forCellReuseIdentifier: "AlbumCellIdentifier")

    view.addSubview(tableView)
    

    // Fetch in background the accurate number of photos per album and reload table when it's finished
    PhotoManager.sharedInstance.getPhotoCountPerAlbum(albums) { (index, album) in
      if self.tableView == nil {
        return
      }

      self.tableView.beginUpdates()
      if (index >= self.tableView.numberOfRows(inSection: 0)) {
        self.tableView.insertRows(at: [IndexPath(row: index, section: 0)], with: .none)
        self.albums.insert(album, at: index)
      } else {
        self.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
        self.albums[index] = album
      }
      self.tableView.endUpdates()
    }

    self.tableView.reloadData()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    if (containerController.selectedPhotos.count > 0) {
      let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.cancel))
      let uploadButton = UIBarButtonItem(title: "OBImagePicker Upload Action Button".localized, style: .plain, target: self, action: #selector(self.finish))
      self.navigationItem.rightBarButtonItems = [cancelButton, uploadButton ]
    }
    
    tableView.reloadData()
  }

  
  // MARK: UITableViewDataSource
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return albums.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = self.tableView.dequeueReusableCell(withIdentifier: "AlbumCellIdentifier") as? PhotoAlbumTableViewCell
    cell!.coverSize = CGSize(width: RowHeight, height: RowHeight)
    cell!.album = albums[indexPath.row]

    return cell!
  }
  
  // MARK: UITableViewDelegate
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    
    let album: PhotoAlbum = albums[indexPath.row]

    // Safe check
    if album.count > 0 {
      let analyticsManager = MezzanineAnalyticsManager.sharedInstance
      analyticsManager.trackPhotoAlbumNavigationEvent(album.smartAlbum ? .smart : .userCreated,
                                                      albumName: album.smartAlbum ? album.title : "User Private Album")
      let vc = PhotoSelectionViewController(selectedPhotos: containerController.selectedPhotos, album: album)
      vc.delegate = self
      self.navigationController?.pushViewController(vc, animated: true)
    }
  }
  
  // MARK: UIBarButtonItem actions
  
  @objc func cancel() {
    containerController.cancel()
  }
  
  @objc func finish() {
    containerController.didFinishPickingAssets()
  }
  
  
  //MARK: PhotoCollectionViewControllerDelegate

  func photoSelectionViewControllerDidFinishPickingAssets() {
    finish()
  }
}
