//
//  NSString+Additions.h
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 20/09/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Additions)

- (NSString *)sanitizeStringAsServerName;

@end
