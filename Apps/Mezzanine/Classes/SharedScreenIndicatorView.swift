//
//  SharedScreenIndicator.swift
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 19/06/2017.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import UIKit

class SharedScreenIndicatorView: SubviewHittableView {
  
  var workspaceGeometry: WorkspaceGeometry!
  var surface: MZSurface!
  var containerScrollView: UIScrollView!
  var zoomLevel: ZoomLevel = .workspace {
    didSet {
      updateLayout()
    }
  }
  
  // Components
  fileprivate var titleLabel: UILabel!
  fileprivate var visibleBorderView: UIView!
  
  // Constants
  fileprivate let borderWidth: CGFloat = 2.0
  fileprivate let innerMargin: CGFloat = 8.0
  fileprivate let frameSize: CGSize = CGSize(width: 80.0, height: 24.0)
  fileprivate let fontSize: CGFloat = 12.0
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    let styleSheet = MezzanineStyleSheet.shared()
    
    visibleBorderView = SubviewHittableView(frame: CGRect.zero)
    visibleBorderView.backgroundColor = UIColor.clear
    visibleBorderView.layer.borderColor = styleSheet?.greenIndicatorColor.cgColor
    
    titleLabel = UILabel(frame: CGRect.zero)
    titleLabel.adjustsFontSizeToFitWidth = true
    titleLabel.minimumScaleFactor = 0.01
    titleLabel.backgroundColor = styleSheet?.greenIndicatorColor
    titleLabel.textColor = UIColor.white
    titleLabel.textAlignment = .center

    addSubview(visibleBorderView)
    addSubview(titleLabel)
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  func updateLayout() {
    guard let workspaceGeometry = workspaceGeometry else { return }
    guard let containerScrollView = containerScrollView else { return }
    let zoomScale = containerScrollView.zoomScale
    
    let surfaceRect = workspaceGeometry.roundedFrameForSurface(surface)
    let visibleToAllRect = workspaceGeometry.rectForVisibleToAllFelds(surface: surface)
    let convertedRect = self.superview!.convert(visibleToAllRect, to: self)
    
    
    // Outter view
    frame = surfaceRect
    
    // Green border
    visibleBorderView.frame = convertedRect
    visibleBorderView.layer.borderWidth = borderWidth / zoomScale / UIScreen.main.scale
    
    // Title
    // Apply kern to space the letters
    let attributes: NSDictionary = [
      NSAttributedStringKey.font:UIFont.dinBold(ofSize: fontSize / zoomScale),
      NSAttributedStringKey.kern:CGFloat(0.5 / zoomScale)
    ]
    
    let title = NSAttributedString(string: "Shared".localizedUppercase,
                                   attributes:attributes as? [NSAttributedStringKey : AnyObject])
    
    titleLabel.attributedText = title
    
    titleLabel.frame.size = CGSize(width: frameSize.width / zoomScale, height: frameSize.height / zoomScale)
    
    var sharedScreenTitleLabelOrigin = CGPoint.zero
    let maskLayer =  CAShapeLayer()
    if self.zoomLevel != .feld {
      let path = UIBezierPath(roundedRect:titleLabel.bounds, byRoundingCorners:[.topRight, .topLeft], cornerRadii: CGSize(width: borderWidth / zoomScale, height: borderWidth / zoomScale))
      maskLayer.path = path.cgPath
      
      sharedScreenTitleLabelOrigin = CGPoint(x: convertedRect.minX, y: convertedRect.minY - titleLabel.frame.height + 1.0 / zoomScale)
    } else {
      let path = UIBezierPath(roundedRect:titleLabel.bounds, byRoundingCorners:[.topRight, .topLeft, .bottomLeft, .bottomRight], cornerRadii: CGSize(width: borderWidth / zoomScale, height: borderWidth / zoomScale))
      maskLayer.path = path.cgPath
      
      sharedScreenTitleLabelOrigin = CGPoint(x: convertedRect.minX + innerMargin / zoomScale, y: convertedRect.minY + innerMargin / zoomScale)
    }
    
    titleLabel.frame.origin = sharedScreenTitleLabelOrigin
    titleLabel.layer.mask = maskLayer
    
    titleLabel.layer.contentsScale = UIScreen.main.scale * zoomScale
    layer.layoutIfNeeded()
  }
}
