//
//  WorkspaceOpeningViewController.h
//  Mezzanine
//
//  Created by Zai Chang on 3/20/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WorkspaceOpeningViewController : UIViewController

@property (strong, nonatomic) MZSystemModel *systemModel;

@property (strong, nonatomic) IBOutlet UIView *cellContainerView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cellContainerWidthConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cellContainerHeightConstraint;
@property (strong, nonatomic) IBOutlet UILabel *label;

@end
