//
//  PortfolioAddMenuViewController.h
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 13/04/2017.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

#import "ButtonMenuViewController.h"

@class PortfolioAddMenuInteractor;

@interface PortfolioAddMenuViewController : ButtonMenuViewController

@property (nonatomic, strong) FPPopoverController *popover;
@property (nonatomic, strong) UIView *fromView;

- (instancetype)initWithInteractor:(PortfolioAddMenuInteractor *)interactor;
@end
