//
//  WorkspaceListViewController.h
//  Mezzanine
//
//  Created by miguel on 10/03/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FPPopoverController.h"

@class MezzanineAppContext;
@class WorkspaceViewController;
@class WorkspaceListViewController;

@protocol WorkspaceListViewControllerDelegate <NSObject>

- (void)dismissWorkspaceListViewController:(WorkspaceListViewController*)controller;

@end


/// Controller for the management of workspaces, displayed as a collection

@interface WorkspaceListViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, FPPopoverControllerDelegate>

@property (nonatomic,weak) id delegate;

@property (nonatomic, strong) MezzanineAppContext *appContext;
@property (nonatomic, strong) MZCommunicator *communicator;
@property (nonatomic, strong) MZSystemModel *systemModel;

@end
