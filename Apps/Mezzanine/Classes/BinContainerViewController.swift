//
//  BinContainerViewController.swift
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 10/02/2017.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import UIKit

protocol BinContainerViewControllerDelegate: NSObjectProtocol {
  func binContainerViewControllerWillCollapse(_ willCollapse: Bool)
}

class BinContainerViewController: UIViewController {
  
  weak var delegate: BinContainerViewControllerDelegate?

  // System
  var communicator: MZCommunicator!
  var systemModel: MZSystemModel!
  
  // Components
  @IBOutlet var portfolioControls: UIView!
  @IBOutlet var collapseBinButton: UIButton!
  @IBOutlet var separatorView: UIView!
  @IBOutlet var gapView: UIView!

  // Containers
  @IBOutlet var binControlsContainerView: UIView!
  @IBOutlet var collectionViewContainer: UIView!
  
  // Controllers
  var binCollectionViewController: BinCollectionViewController!
  var portfolioActionsViewController: PortfolioActionsViewController!

  // Interactor
  var binCollectionViewInteractor: BinCollectionViewInteractor!
  var portfolioActionsInteractor: PortfolioActionsInteractor!

  // Data Sources
  let binCollectionDataSource = BinCollectionViewDataSource()
  
  // Constraints
  @IBOutlet var separatorViewConstraintHeight: NSLayoutConstraint!
  @IBOutlet var leftSpaceBinControlsContainer: NSLayoutConstraint!

  @IBOutlet var topSpaceBinControlsContainer: NSLayoutConstraint!
  var initialTopSpaceBinControlsContainer: CGFloat!

  @IBOutlet var separatorBinContainerVerticalConstraint: NSLayoutConstraint!
  var initialSeparatorBinContainerVertical: CGFloat!

  fileprivate var binIsCollapsed: Bool!

  deinit {
    print("BinContainerViewController deinit")

    binCollectionDataSource.dragDropView = nil

    binCollectionViewInteractor.presenter = nil
    binCollectionViewInteractor.systemModel = nil
    binCollectionViewInteractor.communicator = nil
    binCollectionViewController.interactor = nil

    portfolioActionsInteractor.presenter = nil
    portfolioActionsInteractor.systemModel = nil
    portfolioActionsInteractor.communicator = nil
    portfolioActionsViewController.interactor = nil
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    binIsCollapsed = false
    separatorView.backgroundColor = MezzanineStyleSheet.shared().grey50Color
    separatorViewConstraintHeight.constant = 1.0 / UIScreen.main.scale

    // collectionViewContainer init setup
    initializeBinCollectionView()
    
    // collapseBinButton init setup
    collapseBinButton.layer.cornerRadius = collapseBinButton.frame.height/2
    // tmp
    collapseBinButton.contentEdgeInsets = UIEdgeInsetsMake(2.0, 0, 0, 0)
    
    // binControlsContainerView init setup
    initialTopSpaceBinControlsContainer = topSpaceBinControlsContainer.constant
    initialSeparatorBinContainerVertical = separatorBinContainerVerticalConstraint.constant

    portfolioControls.backgroundColor = UIColor.clear

    gapView.backgroundColor = MezzanineStyleSheet.shared().grey30Color
    initializePortfolioActionsViewController()
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  func initializeBinCollectionView() {

    binCollectionViewInteractor = BinCollectionViewInteractor(systemModel: systemModel, communicator: communicator, entity: binCollectionDataSource)
    binCollectionViewController = BinCollectionViewController(dataSource: binCollectionDataSource)
    
    binCollectionDataSource.dragDropView = binCollectionViewController
    binCollectionViewInteractor.presenter = binCollectionViewController
    binCollectionViewController.interactor = binCollectionViewInteractor
    
    addChildViewController(binCollectionViewController)
    let view = binCollectionViewController.view
    view?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    view?.frame = collectionViewContainer.bounds
    collectionViewContainer.addSubview(view!)
    binCollectionViewController.didMove(toParentViewController: self)
    
    leftSpaceBinControlsContainer.constant = traitCollection.isRegularWidth() ? 38 : 14
  }

  func initializePortfolioActionsViewController() {
    portfolioActionsInteractor = PortfolioActionsInteractor(systemModel: systemModel, communicator: communicator)
    portfolioActionsViewController = PortfolioActionsViewController()

    portfolioActionsInteractor.presenter = portfolioActionsViewController
    portfolioActionsViewController.interactor = portfolioActionsInteractor

    addChildViewController(portfolioActionsViewController)
    let view = portfolioActionsViewController.view
    view?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    view?.frame = portfolioControls.bounds
    portfolioControls.addSubview(view!)
    portfolioActionsViewController.didMove(toParentViewController: self)
  }
  
  @IBAction func collapseBin(_ sender: UIButton) {
    binIsCollapsed = !binIsCollapsed

    // In case we have safeArea bottom, leave 10px padding for bottom edge
    let bottomPadding: CGFloat = view.safeAreaInsets.bottom > 0 ? 10.0 : 0.0

    separatorBinContainerVerticalConstraint.constant = binIsCollapsed == false ? initialSeparatorBinContainerVertical : -initialSeparatorBinContainerVertical * 2.0
    topSpaceBinControlsContainer.constant = binIsCollapsed == false ? initialTopSpaceBinControlsContainer : -bottomPadding
    let delay = binIsCollapsed == false ? 0 : 0.2

    UIView.animate(withDuration: 0.33, delay: delay, options: UIViewAnimationOptions(), animations: {
      self.collapseBinButton.transform = CGAffineTransform(rotationAngle: CGFloat(self.binIsCollapsed == false ? 0 : Double.pi))
      self.view.layoutIfNeeded()
      }, completion: nil)

    delegate?.binContainerViewControllerWillCollapse(binIsCollapsed)
  }
}
