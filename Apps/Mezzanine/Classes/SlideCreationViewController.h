//
//  MZSlideCreationViewController.h
//  Mezzanine
//
//  Created by Zai Chang on 1/25/11.
//  Copyright 2011 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface SlideCreationViewController : UIViewController <UITextViewDelegate>

@property (nonatomic, weak) IBOutlet UITextView *textView;
@property (nonatomic, weak) IBOutlet UILabel *promptLabel;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *bottomSpaceConstraint;

@end
