//
//  SurfaceView.swift
//  Mezzanine
//
//  Created by miguel on 28/2/17.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import UIKit

class SurfaceView: UIView {

  @objc var surfaceSize = CGSize.zero
  @objc var surfaceOffset = CGPoint.zero
  @objc var visibleRect = CGRect.zero
  @objc var drawFelds = false
  @objc var containerScale:CGFloat = 1.0

  @objc var feldGeometry = FeldGeometry()

  @objc var surface: MZSurface? {
    didSet {
      guard let surface = self.surface else { return }

      let idName = surface.name == "main" ? "MainSurfaceView" : "ExtendedSurfaceView"
      self.accessibilityIdentifier = String(idName)
      self.accessibilityLabel = String(format:"Surface-\(surface.name)")

      feldGeometry.surface = surface;
      feldGeometry.containerRect = self.bounds

      let feldBoundsColor = UIColor.init(white: 0.33, alpha: 1.0)

      for feld in surface.felds {
        let feld = feld as! MZFeld
        let feldView = UIView(frame:feldGeometry.roundedFrameForFeld(feld))
        feldView.backgroundColor = SettingsFeatureToggles.sharedInstance.teamworkUI ? MezzanineStyleSheet.shared().grey30Color : MezzanineStyleSheet.shared().grey60Color
        feldView.layer.borderColor = feldBoundsColor.cgColor;
        feldView.layer.borderWidth = borderWidth()
        feldView.isUserInteractionEnabled = false
        feldView.accessibilityIdentifier = String("FeldView")
        feldView.accessibilityLabel = String(format:"Feld-\(feld.name)")
        self.addSubview(feldView)
      }
    }
  }

  override func layoutSubviews() {

    if drawFelds == false {
      return
    }

    guard let surface = surface else { return }

    feldGeometry.containerRect = self.bounds

    for i in 0...subviews.count-1 {
      let feldView = subviews[i]
      let feld = surface.felds[i]
      feldView.layer.borderWidth = borderWidth()
      let frame = feldGeometry.roundedFrameForFeld(feld as! MZFeld)
      feldView.frame = frame.insetBy(dx: -borderWidth(), dy: -borderWidth());
      feldView.setNeedsDisplay()
      feldView.setNeedsLayout()
    }
  }

  func borderWidth() -> CGFloat {
    if SettingsFeatureToggles.sharedInstance.teamworkUI == false {
      return (1 / UIScreen.main.scale) / containerScale
    }
    else {
      return 0
    }
  }
}
