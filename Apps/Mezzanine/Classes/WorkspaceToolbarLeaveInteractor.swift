//
//  WorkspaceToolbarLeaveInteractor.swift
//  Mezzanine
//
//  Created by miguel on 21/02/2019.
//  Copyright © 2019 Oblong Industries. All rights reserved.
//

import UIKit

class WorkspaceToolbarLeaveInteractor: NSObject {

  @objc var communicator: MZCommunicator?

  private weak var viewController: UIViewController?

  fileprivate var disconnectAlertController: UIAlertController?

  @objc init(viewController: UIViewController) {
    self.viewController = viewController
    super.init()
  }

  deinit {
    print("WorkspaceToolbarLeaveInteractor deinit")
    disconnectAlertController?.dismiss(animated: true, completion: nil)
    disconnectAlertController = nil
  }

  @objc func leave(fromButton: UIButton) {

    guard let viewController = viewController else { return }

    let analytics = MezzanineAnalyticsManager.sharedInstance

    // A shortcut to go back to connection screen when "Everyone in the room" is a noop.
    if shouldJustLeave() {
      analytics.trackLeaveSessionEvent(.directly)
      justMe()
      return
    }

    let actionSheet = UIAlertController (title: "Leave Action Sheet Title".localized, message: nil, preferredStyle: .actionSheet)
    let cancelAction = UIAlertAction(title: "Generic Button Title Cancel".localized, style: .cancel, handler: nil)
    let justMeAction = UIAlertAction(title: "Leave Just Me Button".localized, style: .default) { [unowned self](_) -> Void in
      analytics.trackLeaveSessionEvent(.justMe)
      self.justMe()
    }
    let everyoneAction = UIAlertAction(title: "Leave Everyone Button".localized, style: .destructive) { [unowned self](_) -> Void in
      analytics.trackLeaveSessionEvent(.everyone)
      self.everyone()
    }

    actionSheet.addAction(cancelAction)
    actionSheet.addAction(justMeAction)
    actionSheet.addAction(everyoneAction)

    if let popPresenter = actionSheet.popoverPresentationController {
      popPresenter.sourceView = fromButton.superview
      popPresenter.sourceRect = fromButton.frame;
    }

    viewController.present(actionSheet, animated: true, completion: nil)
  }

  func everyone() {
    disconnectAlertController = nil
    guard let communicator = communicator else { return }
    communicator.endSession()
  }

  func justMe() {

    guard let communicator = communicator,
          let viewController = viewController else { return }

    if communicator.systemModel.portfolioExport.isExporting() {

      let alertTitle = "Disconnect While Exporting Dialog Title".localized
      let alertMessage = "Disconnect While Exporting Dialog Message".localized
      let alertController = UIAlertController (title: alertTitle, message: alertMessage, preferredStyle: .alert)

      let confirmAction = UIAlertAction(title: "Connection Lost Dialog Disconnect Action".localized, style: .destructive) { [unowned self](_) -> Void in
        self.performDisconnect()
      }
      let cancelAction = UIAlertAction(title: "Generic Button Title Cancel".localized, style: .cancel) { [unowned self](_) -> Void in
        self.disconnectAlertController = nil
      }
      alertController.addAction(confirmAction)
      alertController.addAction(cancelAction)

      viewController.present(alertController, animated: true, completion: nil)
      disconnectAlertController = alertController
    }
    else {
      performDisconnect();
    }
  }

  func shouldJustLeave() -> Bool {
    guard let communicator = communicator, let systemModel = communicator.systemModel
    else {
        return false
    }

    // When connected to a Mezz-In Session
    if communicator.isARemoteParticipationConnection {
      return true
    }

    // When connected to a room if there's no mezz-in/m2m session and workspace is clean
    if systemModel.infopresence.status == .inactive && !systemModel.currentWorkspace.hasContent {

      // And the room is a pre-Ziggy system
      if communicator.sessionManager.sessionType == .plasmaWebProxy {
        return true
      }
      // Or it is a Ziggy system and this is the only connected client
      else if communicator.sessionManager.sessionType == .ziggy && systemModel.myMezzanine.participants.count == 1 {
        return true
      }
    }
    return false
  }

  func performDisconnect() {
    disconnectAlertController = nil

    guard let communicator = communicator else { return }

    if communicator.systemModel!.isSignedIn {
      communicator.requestor.requestClientSignOutRequest()
    }

    // Disconnect pools/hoses
    if communicator.isConnected == true {
      communicator.disconnect(withMessage: MezzanineAnalyticsManager.sharedInstance.mezzanineMenuAttribute)
    }
    else { // A catch for certain rare cases when communicator has disconnected yet somehow the app has not returned to the connection screen
      // This is necessary for cleanup
      communicator.postDidDisconnectNotification(MezzanineAnalyticsManager.sharedInstance.mezzanineMenuAttribute)
    }
  }
}
