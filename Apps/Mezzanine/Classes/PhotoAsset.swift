//
//  PhotoAsset.swift
//  Mezzanine
//
//  Created by miguel on 3/1/17.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import UIKit
import Photos

class PhotoAsset: NSObject {

  @objc var photoAsset: PHAsset
  @objc dynamic var selected = false

  init(photoAsset: PHAsset) {
    self.photoAsset = photoAsset
  }

  @objc var pixelWidth: Int {
    get { return photoAsset.pixelWidth }
  }

  @objc var pixelHeight: Int {
    get { return photoAsset.pixelHeight }
  }

  @objc var imageDataBlock: () -> Data {
    get {
      return { () in
        return PhotoManager.sharedInstance.fetchPhotoAssetData(self)
      }
    }
  }

  @objc var filename: String {
    get { return photoAsset.localIdentifier }
  }

  @objc var imageType: String = "not defined yet"

  @objc var filesize: Int = NSNotFound
}
