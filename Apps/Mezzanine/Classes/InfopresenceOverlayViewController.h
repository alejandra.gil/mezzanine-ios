//
//  InfopresenceOverlayViewController.h
//  Mezzanine
//
//  Created by Zai Chang on 4/22/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InfopresenceOverlayViewController : UIViewController

@property (strong, nonatomic) MZCommunicator *communicator;
@property (strong, nonatomic) MZSystemModel *systemModel;
@property (strong, nonatomic) MZRemoteMezz *remoteMezz;

@property (strong, nonatomic) IBOutlet UIView *cellContainerView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cellContainerWidthConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cellContainerHeightConstraint;
@property (strong, nonatomic) IBOutlet UILabel *label;
@property (strong, nonatomic) IBOutlet UIButton *button;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *labelContainerLeadingConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *buttonContainerLeadingConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *buttonWidthConstraint;

@end
