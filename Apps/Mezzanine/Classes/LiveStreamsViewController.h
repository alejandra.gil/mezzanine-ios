//
//  LiveStreamsViewController.h
//  Mezzanine
//
//  Created by Zai Chang on 1/24/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OBDragDropProtocol.h"
#import <collections/src/NICollectionViewModel.h>

@class MezzanineMainViewController;

@interface LiveStreamsViewController : UIViewController <OBOvumSource, UIGestureRecognizerDelegate, UICollectionViewDelegate, NICollectionViewModelDelegate>

@property (nonatomic, strong) MZWorkspace *workspace;

@property (nonatomic, weak) MezzanineMainViewController *mezzanineViewController;

@property (nonatomic, assign) BOOL showOnlyActiveStreams; // Non-active, unplugged DVI cable sources will be omitted

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *hintLabel;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *titleLabelLeftConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *hintLabelLeftConstraint;

@end
