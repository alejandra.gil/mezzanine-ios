//
//  ConnectionAnimationController.swift
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 17/11/2016.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

import UIKit


class ConnectionAnimationController: NSObject, UIViewControllerAnimatedTransitioning {
  
  fileprivate var transitionDuration = 0.35
  fileprivate var transitionContext: UIViewControllerContextTransitioning!
  fileprivate var navigationControllerOperation: UINavigationControllerOperation!
  
  fileprivate var originalToVCAnimatableFrame: CGRect!
  fileprivate var originalFromVCAnimatableFrame: CGRect!
  fileprivate var endFromVCAnimatableFrame: CGRect!
  fileprivate var startToVCAnimatableFrame: CGRect!
  
  fileprivate var toViewControllerKey: UIViewController!
  fileprivate var fromViewControllerKey: UIViewController!
  
  
  @objc convenience required init(operation: UINavigationControllerOperation) {
    self.init()
    navigationControllerOperation = operation
  }
  
  func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
    return transitionDuration;
  }
  
  func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
    self.transitionContext = transitionContext
    transitionViewControllers(navigationControllerOperation)
  }
  
  
  // MARK: Animations
  
  func transitionViewControllers(_ type: UINavigationControllerOperation) {
    let containerView = transitionContext.containerView
    toViewControllerKey = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!
    fromViewControllerKey = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!
    
    // Setup
    let finalToVCFrame = transitionContext.finalFrame(for: toViewControllerKey)
    toViewControllerKey.view.frame = finalToVCFrame
    containerView.addSubview(toViewControllerKey.view)
    toViewControllerKey.view.alpha = 0.0
    
    
    if navigationControllerOperation == .push {
      let toViewController = toViewControllerKey as! RemoteParticipationJoinViewController
      let fromViewController = fromViewControllerKey as! MezzanineConnectionViewController
      
      originalFromVCAnimatableFrame = fromViewController.contentView.frame
      let xOffsetEndFrame = -fromViewController.contentView.frame.width
      endFromVCAnimatableFrame = originalFromVCAnimatableFrame.offsetBy(dx: xOffsetEndFrame, dy: 0)
      
      originalToVCAnimatableFrame = toViewController.contentView.frame
      let xOffsetStartFrame = finalToVCFrame.width
      startToVCAnimatableFrame = originalToVCAnimatableFrame.offsetBy(dx: xOffsetStartFrame, dy: 0)
      toViewController.contentView.frame = startToVCAnimatableFrame
      
      animate(fromViewController.contentView, toContentView: toViewController.contentView)
    } else {
      let toViewController = toViewControllerKey as! MezzanineConnectionViewController
      let fromViewController = fromViewControllerKey as! RemoteParticipationJoinViewController
      
      originalFromVCAnimatableFrame = fromViewController.contentView.frame
      let xOffsetEndFrame = fromViewController.contentView.frame.width
      endFromVCAnimatableFrame = originalFromVCAnimatableFrame.offsetBy(dx: xOffsetEndFrame, dy: 0)
      
      originalToVCAnimatableFrame = toViewController.contentView.frame
      let xOffsetStartFrame = -finalToVCFrame.width
      startToVCAnimatableFrame = originalToVCAnimatableFrame.offsetBy(dx: xOffsetStartFrame, dy: 0)
      toViewController.contentView.frame = startToVCAnimatableFrame
      
      animate(fromViewController.contentView, toContentView: toViewController.contentView)
    }
  }
  
  func animate(_ fromContentView: UIView, toContentView: UIView) {
    UIView.animate(withDuration: transitionDuration, delay: 0, options: .curveEaseOut, animations: { () -> Void in
      fromContentView.alpha = 0.0
      fromContentView.frame = self.endFromVCAnimatableFrame
      self.toViewControllerKey.view.alpha = 1.0
      toContentView.frame = self.originalToVCAnimatableFrame
    }) { (Bool) -> Void in
      fromContentView.alpha = 1.0
      fromContentView.frame = self.originalFromVCAnimatableFrame
      self.transitionContext.completeTransition(true)
    }
  }
}
