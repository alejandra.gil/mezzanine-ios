//
//  MezzanineMailComposerController.swift
//  Mezzanine
//
//  Created by miguel on 9/12/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

import UIKit
import MessageUI

class MezzanineMFMailComposeViewController: MFMailComposeViewController {
  
  override var preferredStatusBarStyle : UIStatusBarStyle {
    return .lightContent;
  }
  
  override var prefersStatusBarHidden : Bool {
    return true
  }
  
  override var childViewControllerForStatusBarStyle : UIViewController? {
    return nil
  }
}


class MezzanineMailComposerController: NSObject, MFMailComposeViewControllerDelegate {

  @objc var presenterViewController: MezzanineNavigationController?

  func presentMailComposer(_ subject: String, message: String, isHTML: Bool) {
    let mailComposer = MezzanineMFMailComposeViewController()
    mailComposer.mailComposeDelegate = self
    mailComposer.setSubject(subject)
    mailComposer.setMessageBody(message, isHTML: isHTML)
    presenterViewController?.present(mailComposer, animated: true, completion: nil)
  }

  func canSendMail() -> Bool {
    return MFMailComposeViewController.canSendMail()
  }


  // MARK: MFMailComposeViewControllerDelegate

  func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
    if (result == .cancelled) || (result == .failed)
    {
      if (error != nil)
      {
        let sendMailErrorAlert = UIAlertController (title: "Mail Composer Error Dialog Title".localized, message: error?.localizedDescription, preferredStyle: .alert)

        let cancelAction = UIAlertAction(title: "Generic Title OK".localized, style: .default, handler: nil)
        sendMailErrorAlert.addAction(cancelAction)
        presentAlert(sendMailErrorAlert)
      }
    }
    controller.dismiss(animated: true, completion: nil)
  }

  fileprivate func presentAlert(_ viewController: UIViewController) {
    let alertWindow = UIWindow(frame: UIScreen.main.bounds)
    alertWindow.rootViewController = UIViewController()
    alertWindow.windowLevel = UIWindowLevelAlert
    alertWindow.makeKeyAndVisible()
    alertWindow.rootViewController?.present(viewController, animated: true, completion: nil)
  }
}
