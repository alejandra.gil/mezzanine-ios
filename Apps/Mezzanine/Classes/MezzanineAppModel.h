//
//  MezzanineAppModel.h
//  Mezzanine
//
//  Created by miguel on 15/10/13.
//  Copyright (c) 2013 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MZSystemModel.h"

@interface MezzanineAppModel : NSObject

// Model
@property (nonatomic, strong) MZSystemModel *systemModel;

-(void)reset;

@end
