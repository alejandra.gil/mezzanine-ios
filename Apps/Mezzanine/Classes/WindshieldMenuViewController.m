//
//  WindshieldMenuViewController.m
//  Mezzanine
//
//  Created by Zai Chang on 8/27/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import "WindshieldMenuViewController.h"
#import "UIViewController+FPPopover.h"
#import "NSObject+BlockObservation.h"
#import "NSObject+UIAlertController.h"
#import "Mezzanine-Swift.h"

@interface WindshieldMenuViewController ()
{
  NSMutableArray *_windshieldObservers;
  UIAlertController *_currentAlertController;
}

@end


@implementation WindshieldMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
    }
    return self;
}


- (void)dealloc
{
  if (_currentAlertController)
  {
    [_currentAlertController dismissViewControllerAnimated:YES completion:nil];
    _currentAlertController = nil;
  }
  self.windshield = nil;
}


- (void)setWindshield:(MZWindshield *)windshield
{
  if (_windshield != windshield)
  {
    if (_windshield)
    {
      for (id observer in _windshieldObservers)
        [_windshield removeObserverWithBlockToken:observer];
      _windshieldObservers = nil;
    }

    _windshield = windshield;

    if (_windshield)
    {
      _windshieldObservers = [NSMutableArray array];
      __weak typeof(self) __self = self;
      id observer = [_windshield addObserverForKeyPath:@"items"
                                               options:0
                                               onQueue:[NSOperationQueue mainQueue]
                                                  task:^(id obj, NSDictionary *change) {
                                                    [__self reloadItems];
                                                  }];
      [_windshieldObservers addObject:observer];

      [self reloadItems];
    }
  }
}


- (void)reloadItems
{
  __weak typeof (self) __self = self;

  // Primary Windshield doesn't need to use its windshield name for now.
  // To be reviewed for 3.2
  NSArray *surfaceNames = nil;
  NSArray *itemsInCurrentWindshield;
  if (_windshieldType == WindshieldExtended)
  {
    surfaceNames = [_communicator.systemModel surfaceNamesInExtendedWindshield];
    itemsInCurrentWindshield = [_windshield itemsInExtendedWindshield];
  }
  else
  {
    surfaceNames = [_communicator.systemModel surfaceNamesInPrimaryWindshield];
    itemsInCurrentWindshield = [_windshield itemsInPrimaryWindshield];
  }

  BOOL windshieldHasItems = (itemsInCurrentWindshield.count > 0);

  NSMutableArray *menuItems = [NSMutableArray array];
  ButtonMenuItem *arrangeItem = [ButtonMenuItem itemWithTitle:NSLocalizedString(@"Windshield Arrange Button Title", nil) action:^{
    [__self.communicator.requestor requestWindshieldArrangeRequest:__self.workspaceViewController.workspace.uid surfaces:surfaceNames];
    [__self.workspaceViewController.currentPopover dismissPopoverAnimated:NO];
  }];
  arrangeItem.enabled = windshieldHasItems;
  [menuItems addObject:arrangeItem];

  ButtonMenuItem *clearItem = [ButtonMenuItem itemWithTitle:NSLocalizedString(@"Windshield Clear Button Title", nil) action:^{
    [__self clearWindshieldWithSurfaceNames:surfaceNames];
  }];
  clearItem.enabled = windshieldHasItems;
  [menuItems addObject:clearItem];

  self.menuItems = menuItems;
}


-(void) clearWindshieldWithSurfaceNames:(NSArray *)surfaceNames
{
  NSString *type = (_windshieldType == WindshieldPrimary ? @"Windshield Clear Dialog" : @"Corkboard Clear Dialog");
  NSString *title = [NSString stringWithFormat:@"%@ Title", type];
  NSString *message = [NSString stringWithFormat:@"%@ Message", type];
  NSString *cancelButtonTitle = [NSString stringWithFormat:@"%@ Cancel Button Title", type];
  NSString *confirmButtonTitle = [NSString stringWithFormat:@"%@ Confirm Button Title", type];

  UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(title, nil)
                                                                           message:NSLocalizedString(message, nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];

  __weak typeof (self) weakSelf = self;
  UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(cancelButtonTitle, nil)
                                                         style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                                                           [weakSelf.workspaceViewController.currentPopover dismissPopoverAnimated:NO];
                                                         }];

  UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:NSLocalizedString(confirmButtonTitle, nil)
                                                         style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                           [weakSelf.communicator.requestor requestWindshieldClearRequest:weakSelf.workspaceViewController.workspace.uid surfaces:surfaceNames];
                                                           [weakSelf.workspaceViewController.currentPopover dismissPopoverAnimated:NO];
                                                         }];
  [alertController addAction:cancelAction];
  [alertController addAction:confirmAction];
  [self showAlertController:alertController animated:YES completion:nil];
  _currentAlertController = alertController;
}


@end
