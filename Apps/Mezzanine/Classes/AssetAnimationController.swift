//
//  AssetAnimationController.swift
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 21/03/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

import UIKit

struct TransitionContextConstants {
  static let portfolio: String = NSStringFromClass(PortfolioCollectionViewCell.classForCoder())
  static let livestream: String = NSStringFromClass(LiveStreamCollectionViewCell.classForCoder())
  static let windshieldImage: String = NSStringFromClass(ImageAssetView.classForCoder())
  static let windshieldVideo: String = NSStringFromClass(VideoAssetView.classForCoder())
  static let presentation: String = NSStringFromClass(UIImageView.classForCoder())
}


class AssetAnimationController: NSObject, UIViewControllerAnimatedTransitioning {
  
  fileprivate var transitionDuration = 0.3
  fileprivate var transitionContext: UIViewControllerContextTransitioning!
  fileprivate var navigationControllerOperation: UINavigationControllerOperation!
  fileprivate var transitionView: UIView!
  
  
  @objc convenience required init(transitionView: UIView, operation: UINavigationControllerOperation) {
    self.init()
    self.transitionView = transitionView
    navigationControllerOperation = operation
  }
  
  
  func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
    return transitionDuration;
  }
  
  
  func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
    self.transitionContext = transitionContext
    
    if navigationControllerOperation == UINavigationControllerOperation.push {
      workspaceToAssetTransition()
    } else {
      defaultFadeOutTransition()
    }
  }
  
  
  // MARK: Animations
  
  /* Animation by zooming the selected asset to the asset view controller image's frame */
  func workspaceToAssetTransition() {
    
    let viewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!
    let stringMirror = Mirror(reflecting: viewController)

    // This is actually a safe check it came out from unit tests, since we push directly from workspace child view controllers
    // instead of having the whole workspace set up
    if (stringMirror.subjectType != MezzanineRootViewController.self) {
      defaultFadeOutTransition()
      return
    }
    
    let mezzanineViewController = viewController as! MezzanineRootViewController
    let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)! as! AssetViewController
    let containerView = transitionContext.containerView
    
    // If there's no placeholder we can't set the animation properly so let's fade in and wait to load the asset in destination controller
    if toViewController.placeholderImage == nil {
      defaultFadeOutTransition()
      return
    }
    
    
    // Setup the initial view and subview states
    toViewController.view.frame = transitionContext.finalFrame(for: toViewController)
    toViewController.view.alpha = 0;
    let convertedView = adaptTransitionViewToContainer()
    transitionView.isHidden = true
    
    containerView.addSubview(toViewController.view)
    containerView.addSubview(convertedView)
    
    
    //We have to take into account for calculations and animations that the scrollview AssetViewController size is 20px less due to status bar
    let navigationBarHeight = 20 as CGFloat
    var scrollViewSize = CGSize(width: toViewController.view.frame.width, height: toViewController.view.frame.height)
    scrollViewSize.height -= navigationBarHeight
    
    
    // Calculate aspect ratio to get the final frame for the animation
    let aspectRatio = toViewController.placeholderImage.size.height/toViewController.placeholderImage.size.width as CGFloat
    var width = scrollViewSize.width
    var height = width * aspectRatio
    
    if height > scrollViewSize.height {
      height = scrollViewSize.height
      width = height / aspectRatio
    }
    
    // Animate frame and set final states. Remove animated subviews
    UIView.animate(withDuration: transitionDuration, delay: 0, options: .curveEaseOut, animations: { () -> Void in
      convertedView.frame = CGRect(x: toViewController.view.frame.midX - width/2, y: toViewController.view.frame.midY - (height - navigationBarHeight)/2, width: width, height: height)
      mezzanineViewController.view.alpha = 0.0
      }) { (Bool) -> Void in
        toViewController.view.alpha = 1.0
        self.transitionView.isHidden = false
        mezzanineViewController.view.alpha = 1.0
        convertedView.removeFromSuperview()
        self.transitionContext.completeTransition(true)
    }
  }
  
  /* Animation by fading out origin's view */
  func defaultFadeOutTransition() {
    let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!
    let fromViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!
    let containerView = transitionContext.containerView
    
    containerView.addSubview(toViewController.view)
    toViewController.view.frame = transitionContext.finalFrame(for: toViewController)
    toViewController.view.alpha = 0.0
    
    UIView.animate(withDuration: transitionDuration, delay: 0, options: .curveEaseIn, animations: { () -> Void in
      fromViewController.view.alpha = 0.0
      toViewController.view.alpha = 1.0
      }) { (Bool) -> Void in
        self.transitionContext.completeTransition(true)
    }
  }
  
  
  // MARK: Private
  
  func adaptTransitionViewToContainer() -> UIView {
    let transitionViewClassType = NSStringFromClass(transitionView.classForCoder)
    
    var imageView: UIImageView
    
    if  transitionViewClassType ==  TransitionContextConstants.portfolio
    {
      let portfolioCell = transitionView as! PortfolioCollectionViewCell
      imageView = UIImageView(image: portfolioCell.imageView.image)
      imageView.contentMode = .scaleAspectFit

      // Assign the proper imageView to the transitionView to hide it when animation starts
      transitionView = portfolioCell.imageView
      
      // Get converted frame to start transition from
      let convertedCellRect = transitionContext.containerView.convert(portfolioCell.imageView.frame, from: portfolioCell.imageView)
      imageView.frame = convertedCellRect
      
    }
    else if transitionViewClassType ==  TransitionContextConstants.livestream
    {
      let livestreamCell = transitionView as! LiveStreamCollectionViewCell
      imageView = UIImageView(image: livestreamCell.liveStream.thumbnail)
      imageView.contentMode = .scaleAspectFit
      
      // Assign the proper imageView to the transitionView to hide it when animation starts
      transitionView = livestreamCell.imageView
      
      // Get converted frame to start transition from
      let convertedCellRect = transitionContext.containerView.convert(livestreamCell.imageView.frame, from: livestreamCell.imageView)
      imageView.frame = convertedCellRect
      
    }
    else if transitionViewClassType == TransitionContextConstants.windshieldImage
    {
      let imageAssetView = transitionView as! ImageAssetView
      imageView = UIImageView(image: imageAssetView.imageView.image)
      imageView.contentMode = .scaleAspectFit
      
      // Assign the proper imageView to the transitionView to hide it when animation starts
      transitionView = imageAssetView.imageView
      
      // Get converted frame to start transition from
      let convertedCellRect = transitionContext.containerView.convert(imageAssetView.imageView.frame, from: imageAssetView.imageView)
      imageView.frame = convertedCellRect
      
    }
    else if transitionViewClassType == TransitionContextConstants.windshieldVideo
    {
      let videoAssetView = transitionView as! VideoAssetView
      imageView = UIImageView(image: videoAssetView.liveStream.thumbnail)
      imageView.contentMode = .scaleAspectFit
      
      // Assign the proper imageView to the transitionView to hide it when animation starts
      transitionView = videoAssetView.imageView
      
      // Get converted frame to start transition from
      let convertedCellRect = transitionContext.containerView.convert(videoAssetView.imageView.frame, from: videoAssetView.imageView)
      imageView.frame = convertedCellRect
      
    }
    else if transitionViewClassType == TransitionContextConstants.presentation
    {
      let imageAssetView = transitionView as! UIImageView
      imageView = UIImageView(image: imageAssetView.image)
      imageView.contentMode = .scaleAspectFit
      
      // Assign the proper imageView to the transitionView to hide it when animation starts
      transitionView = imageAssetView
      imageView.frame = transitionView.frame
      
    }
    else
    {
      imageView = UIImageView()
    }
    
    return imageView
  }
}
