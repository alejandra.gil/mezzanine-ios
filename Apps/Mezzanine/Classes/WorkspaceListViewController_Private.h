//
//  WorkspaceListViewController_Private.h
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 19/10/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

#import "WorkspaceListViewController.h"

@class FPPopoverController;

@interface WorkspaceListViewController ()
{
  CGFloat _lastColletionViewLayoutWidth;
  BOOL _openNewlyCreatedWorkspace;
  NSArray *_visibleCellsBeforeRotation;
  NSArray *_sortedWorkspaces;
  NSArray *_sortedOwners;
  NSString *_selectedOwner;
  FPPopoverController *currentPopoverController;
  


}

@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) IBOutlet UILabel *emptyWorkspaceListLabel;
@property (strong, nonatomic) UIBarButtonItem *workspaceNewBarButtonItem;
@property (strong, nonatomic) UIBarButtonItem *signInBarButtonItem;
@property (strong, nonatomic) UIBarButtonItem *doneBarButtonItem;

- (void)signIn;
- (void)newWorkspace;
- (void)done;

- (UICollectionViewCell*)collectionViewCellForWorkspace:(MZWorkspace *)workspace;

@end