//
//  PhotoSelectionViewController.swift
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 03/01/2017.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import UIKit

protocol PhotoSelectionViewControllerDelegate: NSObjectProtocol {
  func photoSelectionViewControllerDidFinishPickingAssets()
}

class PhotoSelectionViewController: UIViewController {

  weak var delegate: PhotoSelectionViewControllerDelegate?

  fileprivate var album: PhotoAlbum
  fileprivate var selectedPhotos: NSMutableArray

  fileprivate var photoCollectionViewController: PhotoCollectionViewController?

  init(selectedPhotos: NSMutableArray, album: PhotoAlbum) {
    self.selectedPhotos = selectedPhotos
    self.album = album
    super.init(nibName: nil, bundle: nil)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    view.backgroundColor = UIColor.white

    let activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    activityIndicatorView.translatesAutoresizingMaskIntoConstraints = false
    self.view.addSubview(activityIndicatorView)

    activityIndicatorView.addConstraint(NSLayoutConstraint(item: activityIndicatorView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 20.0))
    activityIndicatorView.addConstraint(NSLayoutConstraint(item: activityIndicatorView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 20.0))
    self.view.addConstraint(NSLayoutConstraint(item: activityIndicatorView, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1.0, constant: 0.0))
    self.view.addConstraint(NSLayoutConstraint(item: activityIndicatorView, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1.0, constant: 30.0))

    activityIndicatorView.startAnimating()

    PhotoManager.sharedInstance.getPhotoAssetsFromAlbum(album, completion: { (assets) in
      let aPhotoCollectionViewController = PhotoCollectionViewController(assets: assets)
      aPhotoCollectionViewController.delegate = self
      aPhotoCollectionViewController.willMove(toParentViewController: self)
      self.addChildViewController(aPhotoCollectionViewController)
      aPhotoCollectionViewController.collectionView!.frame = self.view.bounds
      self.view.addSubview(aPhotoCollectionViewController.collectionView!)
      aPhotoCollectionViewController.didMove(toParentViewController: self)
      self.photoCollectionViewController = aPhotoCollectionViewController
      self.updateBarButtonItems()

      activityIndicatorView.stopAnimating()

      aPhotoCollectionViewController.scrollToBottom()
      DispatchQueue.main.async() {
        aPhotoCollectionViewController.scrollToBottom()
      }
    })
  }

  func updateBarButtonItems() {
    let showNoneButton = photoCollectionViewController!.assets.filter( { return $0.selected == true } ).count > 0

    let barAllOrDoneButtonItem = !showNoneButton ? UIBarButtonItem(title: "OBImagePicker All Action Button".localized, style: .plain, target: self, action: #selector(PhotoSelectionViewController.selectAllItems)) : UIBarButtonItem(title:"OBImagePicker None Action Button".localized, style: .plain, target: self, action: #selector(PhotoSelectionViewController.deselectAllItems))

    let title = selectedPhotos.count > 0 ? "OBImagePicker Upload Action Button".localized : "Generic Button Title Cancel".localized
    let barDoneButtonItem = UIBarButtonItem(title: title, style: .plain, target: self, action: #selector(PhotoSelectionViewController.finishSelection))

    navigationItem.rightBarButtonItems = [barDoneButtonItem, barAllOrDoneButtonItem]
  }


  @objc func selectAllItems() {
    let selectAsset = { (asset:PhotoAsset) -> () in
      if asset.selected == false {
        asset.selected = true
        self.selectedPhotos.add(asset)
      }
    }
    _ = photoCollectionViewController!.assets.map(selectAsset)
    photoCollectionViewController?.collectionView!.reloadData()
    updateBarButtonItems()
  }

  @objc func deselectAllItems() {
    let deselectAsset = { (asset:PhotoAsset) -> () in
      if asset.selected == true && self.selectedPhotos.contains(asset) {
        asset.selected = false
        self.selectedPhotos.remove(asset)
      }
    }
    _ = photoCollectionViewController!.assets.map(deselectAsset)
    photoCollectionViewController?.collectionView!.reloadData()
    updateBarButtonItems()
  }

  @objc func finishSelection() {
    delegate?.photoSelectionViewControllerDidFinishPickingAssets()
  }
}

extension PhotoSelectionViewController: PhotoCollectionViewControllerDelegate {
  func photoCollectionViewControllerDidSelectPhotoAsset(_ asset: PhotoAsset) {
    selectedPhotos.add(asset)
    updateBarButtonItems()
  }

  func photoCollectionViewControllerDidDeselectPhotoAsset(_ asset: PhotoAsset) {
    selectedPhotos.remove(asset)
    updateBarButtonItems()
  }
}
