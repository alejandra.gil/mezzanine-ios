//
//  ImageAssetView.m
//  Mezzanine
//
//  Created by Zai Chang on 2/15/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import "ImageAssetView.h"
#import "MezzanineStyleSheet.h"
#import "UIImageView+MZDownloadManager.h"
#import "Mezzanine-Swift.h"

static void *kAssetObservationContext = @"kAssetObservationContext";

@interface ImageAssetView (Private)

-(void) setActionsPanelAnimationState:(CGFloat)state;

@end


@implementation ImageAssetView

@synthesize imageView;
@synthesize imageSourceType;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) 
    {
      imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
      imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
      imageView.backgroundColor = [MezzanineStyleSheet sharedStyleSheet].assetBackgroundColor;
      imageView.contentMode = UIViewContentModeScaleAspectFit;
      imageView.userInteractionEnabled = NO;  // Important for gesture recognizers
      [contentView addSubview:imageView];

      // TODO: imageSourceType should depend on view size
      imageSourceType = ImageAssetViewStandardThumb;
    }

    return self;
}

-(void) dealloc
{
  self.asset = nil;
}

-(void) layoutSubviews
{
  [super layoutSubviews];

  if (!self.useAutolayout)
  {
    CGRect bounds = contentView.bounds;
    if (![SettingsFeatureToggles sharedInstance].teamworkUI)
    {
      imageView.frame = CGRectInset(bounds, 2.0, 2.0);
    }
    else
    {
      imageView.frame = bounds;
    }
  }
}

- (void) setUseAutolayout:(BOOL)useAutolayout
{
  [super setUseAutolayout:useAutolayout];

  if (useAutolayout)
  {
    imageView.autoresizingMask = UIViewAutoresizingNone;
    [imageView setTranslatesAutoresizingMaskIntoConstraints:NO];

    NSDictionary *metrics = @{@"border":([SettingsFeatureToggles sharedInstance].teamworkUI ? @0.0 : @2.0)};

    [contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-border-[imageView]-border-|"
                                                                        options:0
                                                                        metrics:metrics
                                                                          views:NSDictionaryOfVariableBindings(imageView)]];
    [contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-border-[imageView]-border-|"
                                                                        options:0
                                                                        metrics:metrics
                                                                          views:NSDictionaryOfVariableBindings(imageView)]];
  }
  else
  {
    imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
  }
}


-(void) setImageSourceType:(ImageAssetViewSourceType)type
{
  if (asset)
    [asset removeObserver:self forKeyPath:[self keyStringFromSourceType]];

  imageSourceType = type;

  if (asset)
    [asset addObserver:self forKeyPath:[self keyStringFromSourceType] options:(NSKeyValueObservingOptionNew) context:&kAssetObservationContext];
}


#pragma mark - Model

-(void) loadAsset
{
  if (![asset isKindOfClass:[MZAsset class]])
    return;
  if (![(MZAsset*)asset imageAvailable])
    return;

  NSURL *url = [[MezzanineAppContext currentContext].currentCommunicator urlForResource:[self imageSourceTypeUrlFromAsset:self.asset]];

  UIImage *thumbImage = nil;
  BOOL targetURLIsLargerThanThumb = (self.imageSourceType != ImageAssetViewStandardThumb);
  BOOL targetImageCached = ([[MZDownloadManager sharedLoader] cachedImageForURL:url] != nil);
  if (targetURLIsLargerThanThumb && !targetImageCached)
  {
    // If the thumbnail image is cached
    NSURL *thumbURL = [NSURL URLWithString:[(MZAsset*)asset thumbURL]];
    thumbImage = [[MZDownloadManager sharedLoader] cachedImageForURL:thumbURL];
  }
 
  [imageView setImageWithURL:url andPlaceholderImage:thumbImage];
  
//
//  // Double check of thumb. In case the non-boxed thumb fails it loads the one
//  // from the slide. If we trust native thumbs this is not necessary.
//  if (imageSourceType != ImageAssetViewStandardThumb)
//  {
//    imageSourceType = ImageAssetViewStandardThumb;
//    [__self loadAsset];
//  }
}


-(void) setAsset:(id)anAsset
{
  if (asset != anAsset)
  {
    if ([asset isKindOfClass:[MZAsset class]])
    {
      [asset removeObserver:self forKeyPath:[self keyStringFromSourceType]];
      [asset removeObserver:self forKeyPath:@"imageAvailable"];
    }

    [super setAsset:anAsset];

    if (asset && [asset isKindOfClass:[MZAsset class]])
    {
      [self loadAsset];
      
      [asset addObserver:self forKeyPath:[self keyStringFromSourceType] options:(NSKeyValueObservingOptionNew) context:&kAssetObservationContext];
      [asset addObserver:self forKeyPath:@"imageAvailable" options:(NSKeyValueObservingOptionNew) context:&kAssetObservationContext];
    }
  }
}


-(void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
  if (context == &kAssetObservationContext)
  {
    MZAsset *item = (MZAsset*)object;
    
    if ([keyPath isEqual:[self keyStringFromSourceType]])
    {
      if ([self imageSourceTypeUrlFromAsset:item])
      {
        [self loadAsset];
      }
    }
    else if ([keyPath isEqual:@"imageAvailable"])
    {
      if (item.imageAvailable)
        [self loadAsset];
    }
  }
  else
  {
    [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
  }
}


#pragma mark - Type of image sources convinient methods

-(NSString *) imageSourceTypeUrlFromAsset:(MZAsset *)anAsset
{
  NSString *keyPath = [self keyStringFromSourceType];
  NSString *urlString = [anAsset valueForKeyPath:keyPath];
  if (urlString)
    return urlString;
  
  return anAsset.thumbURL;
}


-(NSString *) keyStringFromSourceType
{
  switch (self.imageSourceType) {
    case ImageAssetViewFullResolution:
      return @"fullURL";
      break;
    case ImageAssetViewLargeThumb:
      return @"largeURL";
      break;
    default:
      return @"thumbURL";
  }
}


#pragma mark - Image Additions

- (CGSize)imageSizeAfterScale
{
  CGFloat sx = self.imageView.frame.size.width / self.imageView.image.size.width;
  CGFloat sy = self.imageView.frame.size.height / self.imageView.image.size.height;
  CGFloat s = 1.0;
  
  CGFloat imWidth = self.imageView.image.size.width;
  CGFloat imHeight = self.imageView.image.size.height;
  
  CGFloat fsx;
  CGFloat fsy;

  switch (self.imageView.contentMode) {
    case UIViewContentModeScaleAspectFit:
    {
      s = fminf(sx, sy);
      fsx = s;
      fsy = s;
    }
      break;
    case UIViewContentModeScaleAspectFill:
    {
      s = fmaxf(sx, sy);
      fsx = s;
      fsy = s;
    }
      break;
    case UIViewContentModeScaleToFill:
    {
      fsx = sx;
      fsy = sy;
    }
      break;
    default:
    {
      fsx = s;
      fsy = s;
    }
  }
  
  return CGSizeMake(imWidth*fsx, imHeight*fsy);
}

@end
