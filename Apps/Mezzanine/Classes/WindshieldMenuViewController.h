//
//  WindshieldMenuViewController.h
//  Mezzanine
//
//  Created by Zai Chang on 8/27/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import "ButtonMenuViewController.h"
#import "MezzanineMainViewController.h"

@class WorkspaceViewController;

typedef NS_ENUM(NSInteger, WindshieldType) {
  WindshieldPrimary,
  WindshieldExtended
};


@interface WindshieldMenuViewController : ButtonMenuViewController

@property (nonatomic, assign) WindshieldType windshieldType;
@property (nonatomic, strong) MZWindshield *windshield;
@property (nonatomic, strong) MZCommunicator *communicator;
@property (nonatomic, weak) WorkspaceViewController *workspaceViewController;

@end
