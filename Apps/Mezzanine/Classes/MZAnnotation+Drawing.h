//
//  MZAnnotation+Drawing.h
//  Mezzanine
//
//  Created by Zai Chang on 10/22/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import "MZAnnotation.h"

@interface MZAnnotation (Drawing)

-(Class) CALayerClass;
-(void) updateLayer:(CALayer*)layer;

-(void) drawLayer:(CALayer *)layer inContext:(CGContextRef)ctx;

@end
