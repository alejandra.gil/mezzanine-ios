//
//  MezzanineMainViewController.m
//  F.K.A. MezzanineViewController.m
//  Mezzanine
//
//  Created by Zai Chang on 9/20/10.
//  Copyright 2010 Oblong Industries. All rights reserved.
//

#import "MezzanineMainViewController.h"
#import "MezzanineMainViewController_Private.h"
#import "MezzanineStyleSheet.h"

// Additions
#import "NSObject+BlockObservation.h"
#import "NSString+VersionChecking.h"
#import "UITraitCollection+Additions.h"
#import "UIView+Debug.h"

#import "Mezzanine-Swift.h"

static CGFloat kWorkspaceBinsAnimationDuration = 1.0/3.0;
static CGFloat kWorkspaceOverlaysAnimationDuration = 0.2;
static CGFloat kVideoChatResizingViewAnimationDuration = 0.33;

@interface MezzanineMainViewController () <UIPopoverControllerDelegate, UIImagePickerControllerDelegate, UIGestureRecognizerDelegate, VideoChatViewControllerDelegate, WorkspaceViewControllerDelegate>
{
  NSMutableArray *_systemModelObservers;
  NSMutableArray *_workspaceObservers;
  NSMutableArray *_communicatorObservers;

  UIView *_workspaceModalOverlayView;
  UILabel *_workspaceModalOverlayLabel;
  
  WorkspaceOpeningViewController *_workspaceOpeningViewController;
  NSDate *_workspaceModalOverlayStartTime;
  NSDate *_workspaceOpeningViewStartTime;

  BOOL _binsVisible;
  
  UITraitCollection *_nextTraitCollection;
  
  CGPoint videoChatOffset;
  CGFloat lastRelativeTrailingOffset;
  CGFloat lastRelativeBottomOffset;
  NSMutableArray *videoChatInSuperviewConstraints;
  UIPanGestureRecognizer *_videoChatPanGestureRecognizer;
}

@end


#define DEBUG_VIEW_FRAMES 0
#define AUTOHIDE_PARAMUS_IN_IPHONE_LANDSCAPE 1
#define OVUM_AUTO_ZOOM_IN 0

@implementation MezzanineMainViewController

@synthesize delegate = _delegate;
@synthesize appContext = _appContext;
@synthesize communicator = _communicator;
@synthesize systemModel = _systemModel;
@synthesize workspace = _workspace;
@synthesize currentDragDropRecognizer = _currentDragDropRecognizer;
@synthesize currentPopover = _currentPopover;
@synthesize workspaceViewController = _workspaceViewController;
@synthesize videoChatViewController = _videoChatViewController;

#pragma mark - View Lifecycle

- (void)viewDidLoad
{
  [super viewDidLoad];
  
  self.view.accessibilityIdentifier = @"WorkspaceView";

  MezzanineStyleSheet *styleSheet = [MezzanineStyleSheet sharedStyleSheet];
  self.view.backgroundColor = styleSheet.grey20Color;

  // Workspace
  [self initializeWorkspaceViewController];

  // Portfolio view
  [self initializePortfolioViewController];


  if (_communicator.isARemoteParticipationConnection)
  {
    _videoChatViewController = [[VideoChatViewController alloc] initWithPexipManager:_appContext.pexipManager];
    
    _videoChatViewController.delegate = self;
    [self addChildViewController:_videoChatViewController];
    _videoChatViewController.view.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    _videoChatViewController.view.frame = _vtcBubbleContainerView.bounds;
    [_vtcBubbleContainerView addSubview:_videoChatViewController.view];
    [_videoChatViewController didMoveToParentViewController:self];
    videoChatInSuperviewConstraints = @[_vtcBubbleContainerTopConstraint, _vtcBubbleContainerTrailingConstraint].mutableCopy;

    _videoChatPanGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleVideoChatPreviewRelocation:)];
    _videoChatPanGestureRecognizer.maximumNumberOfTouches = 1;
    [_vtcBubbleContainerView addGestureRecognizer:_videoChatPanGestureRecognizer];
    
    [_liveStreamsContainerView removeFromSuperview];
  }
  else
  {
    // Livestream
    _liveStreamsViewController = [[LiveStreamsViewController alloc] initWithNibName:nil bundle:nil];
    _liveStreamsViewController.mezzanineViewController = self;
    [self addChildViewController:_liveStreamsViewController];
    
    UIView *liveStreamsView = _liveStreamsViewController.view;
    liveStreamsView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    liveStreamsView.frame = _liveStreamsContainerView.bounds;
    [_liveStreamsContainerView addSubview:liveStreamsView];
    [_liveStreamsViewController didMoveToParentViewController:self];

    [_vtcBubbleContainerView removeFromSuperview];
  }
  
  // Separator
  _binsSeparatorView.backgroundColor = RGBCOLOR(55, 55, 73);
  
  // Data model injection
  //
  if (self.workspace != _systemModel.currentWorkspace)
  {
    self.workspace = _systemModel.currentWorkspace;
  }
  else
  {
    _workspaceViewController.workspace = _workspace;
    _portfolioViewController.workspace = _workspace;
    _liveStreamsViewController.workspace = _workspace;
  }
  
#if DEBUG_VIEW_FRAMES
  if (systemModel.testModeEnabled)
  {
    _liveStreamsViewController.view.backgroundColor = [[UIColor purpleColor] colorWithAlphaComponent:0.1];
    _portfolioContainerView.backgroundColor = [[UIColor blueColor] colorWithAlphaComponent:0.1];
    _binsContainerView.backgroundColor = [[UIColor blueColor] colorWithAlphaComponent:0.1];
  }
#endif
  
  [self updateBinsLayoutConstraints];
}


-(void) viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
  DLog(@"mezzanineViewController viewWillAppear");

  [self updateAdaptativeLayout:self.traitCollection];
}


-(void) viewDidAppear:(BOOL)animated
{
  [super viewDidAppear:animated];
  
  // Analytics
  UIViewController *topViewControllerInNavigationController = self.navigationController.topViewController;
  if ([topViewControllerInNavigationController isKindOfClass:[MezzanineRootViewController class]] &&
      !((MezzanineRootViewController *)topViewControllerInNavigationController).infopresenceMenuController) {
    MezzanineAnalyticsManager *analyticsManager = [MezzanineAnalyticsManager sharedInstance];
    [analyticsManager tagWorkspaceStatusScreen];
  }
}


-(void) viewWillDisappear:(BOOL)animated
{
  [super viewWillDisappear:animated];
  DLog(@"mezzanineViewController viewWillDisappear");

  if ([_currentPopover isPopoverVisible])
    [_currentPopover dismissPopoverAnimated:YES];
}

#pragma mark - Initalizers

- (void)initializeWorkspaceViewController
{
  _workspaceViewController = [[WorkspaceViewController alloc] initWithSystemModel:_systemModel
                                                                        communicator:_communicator
                                                                            delegate:self];

  UIView *workspaceView = _workspaceViewController.view;
  workspaceView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
  workspaceView.frame = _workspaceContainerView.bounds;
  
  [self addChildViewController:_workspaceViewController];
  [_workspaceContainerView addSubview:_workspaceViewController.view];
  [_workspaceViewController didMoveToParentViewController:self];
}


- (void)initializePortfolioViewController
{
  _portfolioViewController = [[PortfolioViewController alloc] initWithNibName:nil bundle:nil];
  _portfolioViewController.appContext = _appContext;
  _portfolioViewController.mezzanineViewController = self;
  
  [self addChildViewController:_portfolioViewController];
  
  UIView *portfolioView = _portfolioViewController.view;
  portfolioView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
  portfolioView.layer.anchorPoint = CGPointMake(0.5, 0.0);
  portfolioView.frame = _portfolioContainerView.bounds;
  [_portfolioContainerView addSubview:portfolioView];
  [_portfolioViewController didMoveToParentViewController:self];
}


#pragma mark - Model

-(void) setSystemModel:(MZSystemModel *)aModel
{
  if (_systemModel != aModel)
  {
    if (_systemModelObservers && _systemModel)
    {
      for (id token in _systemModelObservers)
        [_systemModel removeObserverWithBlockToken:token];
      
      _systemModelObservers = nil;
      
      _workspaceViewController.systemModel = nil;
      _workspaceOpeningViewController.systemModel = nil;
    }
    
    _systemModel = aModel;
    
    if (_systemModel)
    {
      _systemModelObservers = [[NSMutableArray alloc] init];
      id token;
      __weak typeof(self) __self = self;
      
      // The following is intended to support the case where a client connected to single-feld mezzanine
      // and is subsequently collaborating with a triple-feld system and the design had called for displaying
      // activity on the remote mezz (see bug 6732). Since this 'feature' is no longer part of 2.4, this code path is disabled
      
      token = [_systemModel addObserverForKeyPath:@"currentWorkspace"
                                          options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld)
                                          onQueue:[NSOperationQueue mainQueue]
                                             task:^(id obj, NSDictionary *change) {
                                               
                                               MZWorkspace *oldWorkspace = change[NSKeyValueChangeOldKey];
                                               MZWorkspace *newWorkspace = __self.systemModel.currentWorkspace;
                                               if (oldWorkspace != (id)[NSNull null] && !newWorkspace.isSaved)
                                               {
                                                 // Previously workspace was closed, discarded, or switching workspaces
                                                 if (oldWorkspace.wasDestroyed && !newWorkspace.isLoading)
                                                 {
                                                   // If the new workspace isn't currently loading (loading overlay visible)
                                                   NSString *message = oldWorkspace.isSaved ? NSLocalizedString(@"Workspace Deleting", nil) : NSLocalizedString(@"Workspace Discarding", nil);
                                                   [__self showWorkspaceOverlayWithMessage:message animated:YES];
                                                   [__self hideWorkspaceOverlayViewAnimated:YES];
                                                 }
                                                 else if (oldWorkspace.isSaved && !newWorkspace.hasContent)
                                                 {
                                                   // Old workspace was closed and is replaced by a new empty workspace
                                                   [__self showWorkspaceOverlayWithMessage:NSLocalizedString(@"Workspace Closing", nil) animated:YES];
                                                   [__self hideWorkspaceOverlayViewAnimated:YES];
                                                 }
                                               }
                                               else if (newWorkspace.isSaved && !newWorkspace.isLoading)
                                               {
                                                 // This is the case when user opens an empty workspace. In which case throw up the loading overlay
                                                 // is not triggered because isLoading flag is not on
                                                 [__self setWorkspaceOpeningViewVisible:YES];
                                                 [__self setWorkspaceOpeningViewVisible:NO]; // This invokes the minimum
                                               }
                                               
                                               __self.workspace = __self.systemModel.currentWorkspace;
                                             }];
      [_systemModelObservers addObject:token];
    }
  }
}


-(void) setCommunicator:(MZCommunicator *)aCommunicator
{
  if (_communicator != aCommunicator)
  {
    if (_communicatorObservers && _communicator)
    {
      for (id token in _communicatorObservers)
        [_communicator removeObserverWithBlockToken:token];
      
      _communicatorObservers = nil;
    }
    
    _communicator = aCommunicator;
    
    if (_communicator)
    {
      _communicatorObservers = [[NSMutableArray alloc] init];
      __weak typeof(self) __self = self;
      id token = [_communicator addObserverForKeyPath:@"pendingTransactions"
                                              options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld)
                                              onQueue:[NSOperationQueue mainQueue]
                                                 task:^(id obj, NSDictionary *change) {
                                                   [__self.portfolioViewController updateStatus];
                                                 }];
      [_communicatorObservers addObject:token];
    }
  }
}


-(void) setAppContext:(MezzanineAppContext *)appContext
{
  _appContext = appContext;
  _portfolioViewController.appContext = appContext;
}


-(void) setWorkspace:(MZWorkspace*)newWorkspace
{
  if (_workspace != newWorkspace)
  {
    if (_workspace && _workspaceObservers)
    {
      for (id token in _workspaceObservers)
        [_workspace removeObserverWithBlockToken:token];
      
      _workspaceObservers = nil;
    }
    
    _workspace = newWorkspace;
    
    _workspaceViewController.workspace = _workspace;
    _portfolioViewController.workspace = _workspace;
    _liveStreamsViewController.workspace = _workspace;
    
    if (_workspace)
    {
      __weak typeof(self) __self = self;
      _workspaceObservers = [[NSMutableArray alloc] init];
      id observer;
      observer = [_workspace addObserverForKeyPath:@"isLoading" options:(NSKeyValueObservingOptionInitial) onQueue:[NSOperationQueue mainQueue] task:^(id obj, NSDictionary *change) {
        BOOL isLoading = __self.workspace.isLoading;
        DLog(@"Workspace isLoading changed to %d", isLoading);
        
        [__self setWorkspaceOpeningViewVisible:isLoading];
      }];
      [_workspaceObservers addObject:observer];
      
      observer = [_workspace addObserverForKeyPath:@"presentation.active" task:^(id obj, NSDictionary *change) {
        [__self.portfolioViewController updatePresentationModeState];
      }];
      [_workspaceObservers addObject:observer];
      
      [_portfolioViewController updatePresentationModeState];
    }
  }
}


#pragma mark - Adaptative Layout

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
  return self.traitCollection.isiPad ? UIInterfaceOrientationMaskAll : UIInterfaceOrientationMaskAllButUpsideDown;
}


- (void)willTransitionToTraitCollection:(UITraitCollection *)newCollection withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
  [super willTransitionToTraitCollection:newCollection withTransitionCoordinator:coordinator];
  
  _nextTraitCollection = newCollection;
  
  [self updateAdaptativeLayout:_nextTraitCollection];
}


- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
  [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
  
  if (_nextTraitCollection) {
    return;
  }
  
  [self updateAdaptativeLayout:self.traitCollection];
}


- (void)traitCollectionDidChange:(UITraitCollection *)previousTraitCollection
{
  [super traitCollectionDidChange:previousTraitCollection];
  _nextTraitCollection = nil;

  if((_videoChatViewController.state == VideoChatStateFullscreen) && self.traitCollection.isiPhone)
  {
    [self removeVideoChatViewControllerConstraints];
    [self setCurrentVideoChatFrameAsAutolayoutConstraints];
  }
}


-(void) updateAdaptativeLayout:(UITraitCollection *)traitCollection
{
  [self layoutBinsContainerView];
  
  if ([_currentPopover isPopoverVisible])
    [_currentPopover dismissPopoverAnimated:YES];
}


#pragma mark - Workspace Opening / Closing / Discarding

- (void)setWorkspaceOpeningViewVisible:(BOOL)visible
{
  if (visible)
  {
    if (_workspaceOpeningViewController == nil)
    {
      _workspaceOpeningViewController = [[WorkspaceOpeningViewController alloc] initWithNibName:nil bundle:nil];
      _workspaceOpeningViewController.systemModel = self.systemModel;
      
      UIView *openingView = _workspaceOpeningViewController.view;
      openingView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
      openingView.frame = self.workspaceOpeningViewContainer.bounds;
      [self.workspaceOpeningViewContainer addSubview:openingView];
    }
    
    _workspaceOpeningViewContainer.userInteractionEnabled = YES;
    
    _workspaceOpeningViewContainer.hidden = NO;
    
    [UIView animateWithDuration:kWorkspaceOverlaysAnimationDuration animations:^{
      _workspaceOpeningViewContainer.alpha = 1.0;
    }];
    
    _workspaceOpeningViewStartTime = [NSDate date];
  }
  else if (!_workspaceOpeningViewContainer.hidden && _workspaceOpeningViewContainer.userInteractionEnabled)
  {
    CGFloat minimumAnimationDelay = 1.6;
    NSTimeInterval timeSinceVisible = _workspaceOpeningViewStartTime ? [[NSDate date] timeIntervalSinceDate:_workspaceOpeningViewStartTime] : 0.0;
    CGFloat animationDelay = MAX(0.0, minimumAnimationDelay - timeSinceVisible);
    
    [UIView animateWithDuration:kWorkspaceOverlaysAnimationDuration delay:animationDelay options:0 animations:^{
      _workspaceOpeningViewContainer.alpha = 0.0;
    } completion:^(BOOL completion) {
      _workspaceOpeningViewContainer.hidden = YES;
      
      _workspaceOpeningViewStartTime = nil;
    }];
    _workspaceOpeningViewContainer.userInteractionEnabled = NO;
  }
}


-(void) showWorkspaceOverlayWithMessage:(NSString*)message animated:(BOOL)animated
{
  if (_workspaceModalOverlayView == nil)
  {
    CGRect frame = self.workspaceOpeningViewContainer.frame;
    _workspaceModalOverlayView = [[UIView alloc] initWithFrame:frame];
    _workspaceModalOverlayView.accessibilityLabel = @"WorkspaceModalOverlay";
    _workspaceModalOverlayView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    _workspaceModalOverlayView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    [self.workspaceOpeningViewContainer.superview addSubview:_workspaceModalOverlayView];
    
    CGRect bounds = _workspaceModalOverlayView.bounds;
    _workspaceModalOverlayLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, bounds.size.width, 30.0)];
    _workspaceModalOverlayLabel.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin);
    _workspaceModalOverlayLabel.font = [UIFont dinBoldOfSize:24.0];
    _workspaceModalOverlayLabel.textAlignment = NSTextAlignmentCenter;
    _workspaceModalOverlayLabel.textColor = [UIColor whiteColor];
    _workspaceModalOverlayLabel.center = CGPointMake(CGRectGetMidX(bounds), CGRectGetMidY(bounds));
    [_workspaceModalOverlayView addSubview:_workspaceModalOverlayLabel];
  }
  
  _workspaceModalOverlayLabel.text = message;
  
  void (^animationBlock)() = ^{
    _workspaceModalOverlayView.alpha = 1.0;
  };
  
  if (animated)
  {
    [UIView animateWithDuration:kWorkspaceOverlaysAnimationDuration animations:animationBlock];
  }
  else
    animationBlock();
  
  _workspaceModalOverlayStartTime = [NSDate date];
}


-(void) hideWorkspaceOverlayViewAnimated:(BOOL)animated
{
  CGFloat minimumAnimationDelay = 1.6;
  CGFloat animationDelay = MAX(0.0, minimumAnimationDelay - [[NSDate date] timeIntervalSinceDate:_workspaceModalOverlayStartTime]);
  
  void (^animationBlock)() = ^{
    _workspaceModalOverlayView.alpha = 0.0;
  };
  
  if (animated)
    [UIView animateWithDuration:kWorkspaceOverlaysAnimationDuration delay:animationDelay options:0 animations:animationBlock completion:nil];
  else
    animationBlock();
  
  _workspaceModalOverlayStartTime = nil;
}


#pragma mark - Memory Management

- (void)dealloc
{
  DLog(@"%@ dealloc", [self class]);
  
  // End workspace and model observation
  self.systemModel = nil;
  self.workspace = nil;
  self.communicator = nil;
  
  _workspaceOpeningViewController.systemModel = nil;
  _workspaceOpeningViewController = nil;
  
  self.currentPopover = nil;

  _videoChatViewController.pexipManager = nil;
}


#pragma mark - Layout components

- (void)updateBinsLayoutConstraints
{
  NSInteger heightBinsContainerConstant;
  NSInteger topBinSeparatorConstant;
  
  if (_communicator.isARemoteParticipationConnection)
  {
    heightBinsContainerConstant = self.traitCollection.isCompactWidth||self.traitCollection.isiPhone ? 115 : 145;
    topBinSeparatorConstant = 0.0;
  }
  else
  {
    heightBinsContainerConstant = self.traitCollection.isCompactWidth||self.traitCollection.isiPhone ? 195 : 290;
    topBinSeparatorConstant = self.traitCollection.isCompactWidth||self.traitCollection.isiPhone  ? 80 : 144;
  }
  
  
  [_binsContainerView removeConstraint:_binsContainerHeightConstraint];
  [_binsContainerView removeConstraint:_binsSeparatorViewTopConstraint];
  
  _binsSeparatorViewTopConstraint = [NSLayoutConstraint constraintWithItem:_binsSeparatorView
                                                                 attribute:NSLayoutAttributeTop
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:_binsContainerView
                                                                 attribute:NSLayoutAttributeTop
                                                                multiplier:1.0
                                                                  constant:topBinSeparatorConstant];
  
  _binsContainerHeightConstraint = [NSLayoutConstraint constraintWithItem:_binsContainerView
                                                                attribute:NSLayoutAttributeHeight
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:nil
                                                                attribute:NSLayoutAttributeNotAnAttribute
                                                               multiplier:1.0
                                                                 constant:heightBinsContainerConstant];
  
  [_binsContainerView addConstraints:@[_binsContainerHeightConstraint, _binsSeparatorViewTopConstraint]];
  
  [_liveStreamsContainerView removeConstraint:_liveStreamsBinHeightConstraint];
  _liveStreamsBinHeightConstraint = [NSLayoutConstraint constraintWithItem:_liveStreamsContainerView
                                                                 attribute:NSLayoutAttributeHeight
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:nil
                                                                 attribute:NSLayoutAttributeNotAnAttribute
                                                                multiplier:1.0
                                                                  constant:topBinSeparatorConstant];
  
  [_liveStreamsContainerView addConstraints:@[_liveStreamsBinHeightConstraint]];
  
  [self.view setNeedsUpdateConstraints];
}


-(void) layoutBinsContainerView
{
#if AUTOHIDE_PARAMUS_IN_IPHONE_LANDSCAPE
  if (_nextTraitCollection.isCompactHeight || (self.traitCollection.isCompactHeight && !_nextTraitCollection)) {
    _binsVisible = NO;
  }
#endif
  
  CGFloat binVisibleHeight = (_binsVisible * _binsContainerHeightConstraint.constant);
  
  // If the constraint is not adjusted, the bins can reappear on rotation because of autolayout even if its set to be off.
  self.binsBottomConstraint.constant = binVisibleHeight - _binsContainerHeightConstraint.constant;
  
  //  [self.view setNeedsUpdateConstraints];
}


-(void) set_binsVisible:(BOOL)visible animated:(BOOL)animated
{
  _binsVisible = (_videoChatViewController.state == VideoChatStateFullscreen) ? NO : visible;
  
  void (^animationBlock)() = ^{
    [self layoutBinsContainerView];
    [self.view layoutIfNeeded];
  };
  
  if (animated)
    [UIView animateWithDuration:kWorkspaceBinsAnimationDuration
                     animations:animationBlock
                     completion:nil];
  else
    animationBlock();
}


#pragma mark - UIGestureRecognizerDelegate

- (void)handleThreeFingerDetection:(UIGestureRecognizer*)recognizer
{
  //  if (recognizer.state == UIGestureRecognizerStateBegan)
  //  {
  //    // Cancel any drag and drop that's currently running
  //    if (_currentDragDropRecognizer)
  //    {
  //      // Need to capture this gesture recognizer because the .enabled = NO call will lead to
  //      // the nilifying of the _currentDragDropRecognizer property, thus making the .enabled = YES call useless
  //      UIGestureRecognizer *dragDropRecognizerToCancel = _currentDragDropRecognizer;
  //
  //      dragDropRecognizerToCancel.enabled = NO;
  //      dragDropRecognizerToCancel.enabled = YES;
  //    }
  //  }
}


#pragma mark - VideoChatViewControllerDelegate

- (CGRect)videoChatViewControllerFrameForState:(VideoChatState)state
{
  CGFloat x;
  CGFloat y;
  CGFloat width;
  CGFloat height;
  
  if (state == VideoChatStatePreview)
  {
    width = self.traitCollection.isiPad ? 320.0 : 240.0;
    height = self.traitCollection.isiPad ? 180.0 : 135.0;
    x = lastRelativeTrailingOffset ? CGRectGetWidth(self.view.frame) * lastRelativeTrailingOffset : CGRectGetWidth(self.view.frame) - width - 40.0;
    y = lastRelativeBottomOffset ? CGRectGetHeight(self.view.frame) * lastRelativeBottomOffset : CGRectGetHeight(self.view.frame) - height - 200.0;
  }
  else
  {
    // TODO: Fix VideoChat bubble to be able to stay above workspace toolbar
//    BOOL isiPhoneInCompactHeight = self.traitCollection.isiPhone && self.traitCollection.isCompactHeight;
//    CGFloat toolbarHeight = isiPhoneInCompactHeight ? 0.0 : CGRectGetHeight(self.workspaceToolbarViewController.view.frame);
    CGFloat toolbarHeight = 0.0;

    width = self.view.frame.size.width;
    height = self.view.frame.size.height - toolbarHeight;
    x = 0;
    y = toolbarHeight;
  }
  
  return CGRectMake(x, y, width, height);
}


- (void)videoChatViewControllerWillTransitionToState:(VideoChatViewController *)videoChatViewController state:(enum VideoChatState)state animations:(void (^)(void))animations
{
  if (state == VideoChatStateFullscreen)
  {
    lastRelativeBottomOffset = CGRectGetMinY(_vtcBubbleContainerView.frame)/CGRectGetHeight(self.view.frame);
    lastRelativeTrailingOffset = CGRectGetMinX(_vtcBubbleContainerView.frame)/CGRectGetWidth(self.view.frame);
  }

  [self removeVideoChatViewControllerConstraints];
  CGRect finalFrame = [self videoChatViewControllerFrameForState:state];
  
  [UIView animateWithDuration:kVideoChatResizingViewAnimationDuration animations:^{
    [_vtcBubbleContainerView setFrame:finalFrame];
    animations();

    [_workspaceViewController setState:(state == VideoChatStatePreview? WorkspaceViewStateFull : WorkspaceViewStatePreview)];
    [self set_binsVisible:(state == VideoChatStatePreview && !_workspaceViewController.isZoomedIn) animated:YES];
    
    // TO DO:
    [_workspaceViewController.windshieldViewController performSelector:@selector(refreshLayout)];
    
    [_vtcBubbleContainerView layoutIfNeeded];
  } completion:^(BOOL finished) {
    [self setCurrentVideoChatFrameAsAutolayoutConstraints];
  }];
  
  // Analytics
  MezzanineAnalyticsManager *analyticsManager = [MezzanineAnalyticsManager sharedInstance];
  if (state == VideoChatStateFullscreen) {
    [analyticsManager tagScreen:analyticsManager.vtcFullScreen];
  } else {
    [analyticsManager tagWorkspaceStatusScreen];
  }
}


#pragma mark - VideoChatViewController layout and interaction methods

- (void)reallocateVideoChatBubble
{
  float margin = self.traitCollection.isiPad ? 15.0 : 5.0;
  CGRect originalRect = _vtcBubbleContainerView.frame;
  CGRect cmpRect = CGRectInset(_vtcBubbleContainerView.frame, -margin, -margin);
  float maxX = CGRectGetWidth(self.view.frame) - cmpRect.size.width;
  float maxY = CGRectGetHeight(self.view.frame) - cmpRect.size.height;
  
  if (cmpRect.origin.x < 0.0)
    originalRect.origin.x = margin;
  else if (cmpRect.origin.x > maxX)
    originalRect.origin.x = maxX + margin;
  
  if (cmpRect.origin.y < 0.0)
    originalRect.origin.y = margin;
  else if (cmpRect.origin.y > maxY)
    originalRect.origin.y = maxY + margin;
  
  _videoChatPanGestureRecognizer.enabled = NO;
  [UIView animateWithDuration:0.15 delay:0.0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
    _vtcBubbleContainerView.frame = originalRect;
  } completion:^(BOOL finished) {
    videoChatOffset = CGPointZero;
    [self setCurrentVideoChatFrameAsAutolayoutConstraints];
    _videoChatPanGestureRecognizer.enabled = YES;
  }];
}


- (void)handleVideoChatPreviewRelocation:(UIPanGestureRecognizer *)recognizer
{
  if (![_videoChatViewController canBeDragged])
    return;
  
  CGPoint locationInView = [recognizer locationInView:self.view];
  
  if (recognizer.state == UIGestureRecognizerStateBegan)
  {
    videoChatOffset.x = locationInView.x - _vtcBubbleContainerView.center.x;
    videoChatOffset.y = locationInView.y - _vtcBubbleContainerView.center.y;
    [self removeVideoChatViewControllerConstraints];
  }
  else if (recognizer.state == UIGestureRecognizerStateChanged)
  {
    CGPoint newCenter = CGPointZero;
    newCenter.x = locationInView.x - videoChatOffset.x;
    newCenter.y = locationInView.y - videoChatOffset.y;
    
    _vtcBubbleContainerView.center = newCenter;
  }
  else if (recognizer.state == UIGestureRecognizerStateEnded)
  {
    [self reallocateVideoChatBubble];
  }
}


- (void)removeVideoChatViewControllerConstraints
{
  [self.view removeConstraints:videoChatInSuperviewConstraints];
  [videoChatInSuperviewConstraints removeAllObjects];

  _vtcBubbleContainerView.translatesAutoresizingMaskIntoConstraints = YES;
}


- (void)setCurrentVideoChatFrameAsAutolayoutConstraints
{
  if (!videoChatInSuperviewConstraints)
    videoChatInSuperviewConstraints = @[].mutableCopy;
  
  _vtcBubbleContainerView.translatesAutoresizingMaskIntoConstraints = NO;
  
  if (_videoChatViewController.state == VideoChatStatePreview)
  {
    [videoChatInSuperviewConstraints addObject:[NSLayoutConstraint constraintWithItem:_vtcBubbleContainerView
                                                                            attribute:NSLayoutAttributeTrailing
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:self.view
                                                                            attribute:NSLayoutAttributeTrailing
                                                                           multiplier:CGRectGetMaxX(_vtcBubbleContainerView.frame)/CGRectGetWidth(self.view.frame)
                                                                             constant:0.0]];
    
    [videoChatInSuperviewConstraints addObject:[NSLayoutConstraint constraintWithItem:_vtcBubbleContainerView
                                                                            attribute:NSLayoutAttributeBottom
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:self.view
                                                                            attribute:NSLayoutAttributeBottom
                                                                           multiplier:CGRectGetMaxY(_vtcBubbleContainerView.frame)/CGRectGetHeight(self.view.frame)
                                                                             constant:0.0]];
  }
  else
  {
    [videoChatInSuperviewConstraints addObject:[NSLayoutConstraint constraintWithItem:_vtcBubbleContainerView
                                                                            attribute:NSLayoutAttributeCenterX
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:self.view
                                                                            attribute:NSLayoutAttributeCenterX
                                                                           multiplier:1.0
                                                                             constant:0.0]];
    
    [videoChatInSuperviewConstraints addObject:[NSLayoutConstraint constraintWithItem:_vtcBubbleContainerView
                                                                            attribute:NSLayoutAttributeCenterY
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:self.view
                                                                            attribute:NSLayoutAttributeCenterY
                                                                           multiplier:1.0
                                                                             constant:0.0]];

    [videoChatInSuperviewConstraints addObject:[NSLayoutConstraint constraintWithItem:_vtcBubbleContainerView
                                                                            attribute:NSLayoutAttributeWidth
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:self.view
                                                                            attribute:NSLayoutAttributeWidth
                                                                           multiplier:1.0
                                                                             constant:0.0]];

    [videoChatInSuperviewConstraints addObject:[NSLayoutConstraint constraintWithItem:_vtcBubbleContainerView
                                                                            attribute:NSLayoutAttributeHeight
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:self.view
                                                                            attribute:NSLayoutAttributeHeight
                                                                           multiplier:1.0
                                                                             constant:0.0]];
  }

  [self.view addConstraints:videoChatInSuperviewConstraints];
}


#pragma mark - WorkspaceViewControllerDelegate

- (void)workspaceViewControllerDidStopPresentation:(WorkspaceViewController *)workspaceVC
{
  [_portfolioViewController present:nil];
}


- (void)workspaceViewControllerDidToggleZoom:(WorkspaceViewController *)workspaceVC mode:(BOOL)mode animated:(BOOL)animated
{
  [self set_binsVisible:!mode animated:animated];
}


- (void)workspaceViewControllerDidDraggingWindshieldItem:(WorkspaceViewController *)workspaceVC
{
  if ([_delegate respondsToSelector:@selector(mezzanineMainViewControllerDidStartDraggingWindshieldItem:)])
    [_delegate mezzanineMainViewControllerDidStartDraggingWindshieldItem:self];
}


- (void)workspaceViewControllerDidStopDraggingWindshieldItem:(WorkspaceViewController *)workspaceVC
{
  if ([_delegate respondsToSelector:@selector(mezzanineMainViewControllerDidStopDraggingWindshieldItem:)])
    [_delegate mezzanineMainViewControllerDidStopDraggingWindshieldItem:self];
}


@end
