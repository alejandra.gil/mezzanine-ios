//
//  UserSignInViewController.m
//  Mezzanine
//
//  Created by miguel on 12/03/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import "UserSignInViewController.h"
#import "MezzanineStyleSheet.h"
#import "UIViewController+Three20.h"
#import "NSString+VersionChecking.h"
#import "UITraitCollection+Additions.h"
#import "Mezzanine-Swift.h"

#define kKeyboardHeightIPhonePortrait 216
#define kKeyboardHeightIPhoneLandscape 162


@implementation UserSignInViewController

@synthesize usernameField;
@synthesize passwordField;
@synthesize signInButton;
@synthesize signInLabel;
@synthesize signInRequested;

- (void)viewDidLoad
{
  [super viewDidLoad];
  
  [self.navigationController setNavigationBarHidden:NO animated:YES];

  MezzanineStyleSheet *styleSheet = [MezzanineStyleSheet sharedStyleSheet];

  self.edgesForExtendedLayout = UIRectEdgeNone;

  [signInLabel setFont:[UIFont dinOfSize:15.0]];
  [signInLabel setText:NSLocalizedString(@"Sign In Hint Label Text", nil)];
  [signInLabel setTextColor:[UIColor whiteColor]];
  self.view.backgroundColor = styleSheet.darkestFillColor;
  [styleSheet stylizeButton:self.signInButton];
  [signInButton setTitle:NSLocalizedString(@"Sign In Button Title", nil) forState:UIControlStateNormal];

  void (^styleTextField)() = ^(UITextField *textField){

    CGRect frame = textField.frame;
    CGFloat heightDelta = signInButton.frame.size.height - frame.size.height;
    frame.origin.x = (self.view.frame.size.width - frame.size.width) / 2.0;
    frame.origin.y -= heightDelta / 2.0;
    frame.size.height += heightDelta;
    textField.frame = frame;
    textField.font = [UIFont dinOfSize:16.0];
  };

  usernameField.placeholder = NSLocalizedString(@"Sign In User Name Placeholder Text", nil);
  passwordField.placeholder = NSLocalizedString(@"Sign In Password Placeholder Text", nil);

  styleTextField (usernameField);
  styleTextField (passwordField);
  
  usernameField.accessibilityLabel = [NSString stringWithFormat:@"%@%@", NSStringFromClass([self class]), @"UsernameTextField"];
  passwordField.accessibilityLabel = [NSString stringWithFormat:@"%@%@", NSStringFromClass([self class]), @"PasswordTextField"];
  signInButton.accessibilityLabel = [NSString stringWithFormat:@"%@%@", NSStringFromClass([self class]), @"SignInButton"];
  signInLabel.accessibilityLabel = [NSString stringWithFormat:@"%@%@", NSStringFromClass([self class]), @"SignInDescription"];
}


- (void)viewDidAppear:(BOOL)animated
{
  [super viewDidAppear:animated];
  
  if (self.traitCollection.isRegularWidth && self.traitCollection.isRegularHeight)
  {
    MezzanineStyleSheet *styleSheet = [MezzanineStyleSheet sharedStyleSheet];
    
    NSString *buttonStyle = @"lightTopBorderedButton:";
    TTButton *button = [TTButton buttonWithStyle:buttonStyle title:self.signInButton.titleLabel.text];
    button.accessibilityLabel = [NSString stringWithFormat:@"%@%@", NSStringFromClass([self class]), @"SignInButton"];

    CGRect frame = self.signInButton.frame;
    frame.origin.y = CGRectGetHeight(self.view.frame) - CGRectGetHeight(frame);
    button.frame = frame;
    [button addTarget:self action:@selector(signIn:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    
    [self.signInButton removeFromSuperview];
    
    [signInLabel setTextColor:styleSheet.grey90Color];
    self.view.backgroundColor = [UIColor whiteColor];
  }
  else
  {
    [self updateConstraintsForTraitCollection:self.traitCollection];
    [self textFieldCanBecomeFirstResponder];
  }
}


- (void)textFieldCanBecomeFirstResponder
{
  dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.15 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    if (self.usernameField.text.length > 0)
      [self.passwordField becomeFirstResponder];
    else
      [self.usernameField becomeFirstResponder];
  });
}


- (void)willTransitionToTraitCollection:(UITraitCollection *)newCollection withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
  [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
    [self updateConstraintsForTraitCollection:newCollection];
  } completion:nil];
}


#pragma mark - Constraints

- (void)updateConstraintsForTraitCollection:(UITraitCollection *)traitCollection
{
  if (self.traitCollection.isRegularWidth && self.traitCollection.isRegularHeight) {
    return;
  }
  
  BOOL isLandscape = CGRectGetWidth(self.view.frame) > CGRectGetHeight(self.view.frame);
  
  _topConstraint.constant = isLandscape ? 20 : 50;
  _scrollViewBottomConstraint.constant = isLandscape ? kKeyboardHeightIPhoneLandscape : 0 ;
}


#pragma mark - Actions

- (IBAction)signIn:(id)sender
{
  NSString *username = usernameField.text;
  NSString *password = passwordField.text;

  NSString *errorMessage = nil;
  if (username.length == 0)
    errorMessage = NSLocalizedString(@"Sign In Empty Username Error", nil);
  else if (password.length == 0)
    errorMessage = NSLocalizedString(@"Sign In Empty Password Error", nil);
  
  if (errorMessage)
  {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil
                                                                             message:errorMessage
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okayAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Generic Button Title Okay", nil)
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * _Nonnull action) {
                                                         [alertController dismissViewControllerAnimated:YES completion:nil];
                                                       }];
    [alertController addAction:okayAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
    return;
  }
  
  if (signInRequested)
  {
    [[MezzanineAnalyticsManager sharedInstance] tagEvent:[MezzanineAnalyticsManager sharedInstance].signInWorkspacesEvent];
    signInRequested (usernameField.text, passwordField.text);
  }
}


#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
  if (textField == usernameField)
  {
    [passwordField becomeFirstResponder];
  }
  else if (textField == passwordField)
  {
    [self signIn:nil];
  }
  return YES;
}


@end
