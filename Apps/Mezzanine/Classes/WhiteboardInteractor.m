//
//  WhiteboardInteractor.m
//  Mezzanine
//
//  Created by Zai Chang on 12/4/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import "WhiteboardInteractor.h"

@implementation WhiteboardInteractor

-(MZTransaction*) requestWhiteboardCapture:(NSString*)uid
{
  if (uid.length == 0)
    NSAssert(NO, @"Uid cannot be nil or empty for requestWhiteboardCapture");
  
  NSDictionary *whiteboard = [_systemModel whiteboardWithUid:uid];
  if (whiteboard == nil)
  {
    DLog(@"requestWhiteboardCapture error: whiteboard with uid %@ does not exist", uid);
    return nil;
  }
  
  return [_communicator.requestor requestWhiteboardCaptureRequest:uid workspaceUid:_systemModel.currentWorkspace.uid]; //requestWhiteboardCapture:uid workspaceUid:_systemModel.currentWorkspace.uid];
}


-(void) requestWhiteboardCaptureAll
{
  // Loop through whiteboards to make single requests
  NSArray *whiteboardUids = [_systemModel.whiteboards valueForKeyPath:@"uid"];
  for (NSString *uid in whiteboardUids)
  {
    [self requestWhiteboardCapture:uid];
  }
}

@end
