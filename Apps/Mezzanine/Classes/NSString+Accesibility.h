//
//  NSString+Accesibility.h
//  Mezzanine
//
//  Created by miguel on 13/1/15.
//  Copyright (c) 2015 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Accesibility)

-(NSString *) stringByRemovingSymbols;

@end
