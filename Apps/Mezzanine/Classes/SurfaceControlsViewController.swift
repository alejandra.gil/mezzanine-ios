//
//  SurfaceControlsViewController.swift
//  Mezzanine
//
//  Created by miguel on 10/3/17.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import UIKit

struct SurfaceControlsBarConstants {
  static let width: CGFloat = 92
  static let height: CGFloat = 72
  static let margin: CGFloat = 12
}

struct HintConstants {
  static let width: CGFloat = 200
  static let height: CGFloat = 24
  static let bottomMargin: CGFloat = 12
}


protocol SurfaceControlsViewControllerDelegate: NSObjectProtocol {

  func currentFocusArea() -> Any?
}


class SurfaceControlsViewController: UIViewController {

  var systemModelObserverArray = Array <String> ()
  var systemModel: MZSystemModel? {
    willSet(newSystemModel) {
      if self.systemModel != newSystemModel {
        endObservingSystemModel()
      }
    }

    didSet {
      if systemModel != nil {
        beginObservingSystemModel()
        self.windshield = systemModel?.currentWorkspace.windshield
      }
      else {
        self.windshield = nil
      }
    }
  }

  var windshieldObserverArray = Array <String> ()
  var windshield: MZWindshield? {
    willSet(newWindshield) {
      if self.windshield != newWindshield {
        endObservingWindshield()
      }
    }

    didSet {
      if windshield != nil {
        beginObservingWindshield()
        updateEnabledControls()
      }
    }
  }

  var communicator: MZCommunicator?

  weak var delegate: SurfaceControlsViewControllerDelegate?

  var contentContainerView: UIView?

  var workspaceGeometry: WorkspaceGeometry?

  var surfaceControlsViews = [SurfaceControlBarViewController]()
  
  var hintsViewController: HintsViewController!
  
  var presentationSliderViewController: PresentationSliderViewController!
  var presentationSliderInteractor: PresentationSliderInteractor!
  
  var surfaces: NSArray? {
    didSet {
      insertSurfaceControlsViews()
      updateControlsLayout()
      updateEnabledControls()
      insertDoubleTapHint()
    }
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = UIColor.clear
    
    initializePresentationSlider()
  }

  override func viewDidLayoutSubviews() {
    updateControlsLayout()
  }
  
  deinit {
    print("SurfaceControlsViewController deinit")

    presentationSliderViewController.interactor = nil
    presentationSliderViewController.delegate = nil
  }
  
  override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
    super.viewWillTransition(to: size, with: coordinator)
    
    presentationSliderViewController.view.isHidden = size.width > size.height ? true : false
  }
  
  override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
    super.traitCollectionDidChange(previousTraitCollection)
    
    let view = transitionCoordinator?.containerView ?? self.view
    presentationSliderViewController.view.isHidden = view!.frame.width > view!.frame.height ? true : false
  }
  
  func updateLayout() {
    updateControlsLayout()
    updatePresentationSlider()
    updateDoubleTapHintLayout()
  }

  // MARK: Presentation slider
  
  func initializePresentationSlider() {
    presentationSliderViewController = PresentationSliderViewController(sliderStyle: .portrait)
    presentationSliderViewController.interactor = presentationSliderInteractor
    
    self.addChildViewController(presentationSliderViewController)
    
    
    let presentationSliderView = presentationSliderViewController.view
    presentationSliderView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    presentationSliderView?.frame = view.bounds
    view.addSubview(presentationSliderView!)
    
    presentationSliderViewController.didMove(toParentViewController: self)
  }
  
  func updatePresentationSlider() {
      presentationSliderViewController.view.frame = rectForSlider()
      presentationSliderViewController.updateLayout(animate: true)
  }
  
  func rectForSlider() -> CGRect {
    var mainSurface = MZSurface()
    for surface in surfaces! {
      let surface = surface as! MZSurface
      if surface.name == "main" {
        mainSurface = surface
      }
    }
    
    guard let workspaceGeometry = workspaceGeometry else { return CGRect.zero }
    guard let contentContainerView = contentContainerView else { return CGRect.zero }
    guard let felds = mainSurface.felds else {return CGRect.zero}
    
    let currentFocus = delegate?.currentFocusArea()
    var rect = workspaceGeometry.rectForSurface(mainSurface)
    
    if currentFocus != nil && felds.contains(currentFocus!) {
      rect = workspaceGeometry.rectForFeld(currentFocus as! MZFeld)
    }
    
    rect = contentContainerView.convert(rect, to: self.view)
    
    let maxWidth = min(rect.width, self.view.frame.width)
    let margin: CGFloat = self.traitCollection.isRegularWidth() ? 24.0 : 12.0
    
    return CGRect(x:rect.minX + margin,
                  y:rect.maxY,
                  width: maxWidth - 2 * margin,
                  height: presentationSliderViewController.constants.height)
  }
  
  // MARK: Surface controls
  
  func insertSurfaceControlsViews() {

    surfaceControlsViews.forEach{$0.view.removeFromSuperview()}
    surfaceControlsViews.removeAll()

    guard let surfaces = surfaces else { return }

    for surface in surfaces {
      let surface = surface as! MZSurface

      let surfaceControlBar = SurfaceControlBarViewController(nibName: "SurfaceControlBarViewController", bundle: nil)
      surfaceControlBar.delegate = self
      surfaceControlBar.surface = surface
      surfaceControlBar.view.backgroundColor = UIColor.clear
      view.addSubview(surfaceControlBar.view)
      surfaceControlBar.view.isUserInteractionEnabled = true
      surfaceControlsViews.append(surfaceControlBar)
    }
  }

  func updateControlsLayout() {

    let currentSurface = currentFocusSurface()

    for surfaceControlsView in surfaceControlsViews {
      guard let surface = surfaceControlsView.surface else { continue }
      surfaceControlsView.view.alpha = currentSurface == nil || currentSurface === surface ? 1.0 : 0.0
      surfaceControlsView.view.frame = rectForControls(surface)
      surfaceControlsView.view.setNeedsDisplay()
      surfaceControlsView.view.setNeedsLayout()
    }
  }

  func updateEnabledControls() {

    guard let windshield = windshield else { return }

    for surfaceControlsView in self.surfaceControlsViews {
      guard let surface = surfaceControlsView.surface else { continue }
      guard let items = windshield.items(in: surface) else { continue }
      surfaceControlsView.enableControls(items.count != 0)
    }
  }

  func currentFocusSurface() -> MZSurface? {
    guard let workspaceGeometry = workspaceGeometry else { return nil }

    let currentFocus = delegate?.currentFocusArea()

    if currentFocus is MZFeld {
      let feldRect = workspaceGeometry.rectForFeld(currentFocus as! MZFeld)
      let center = CGPoint(x: feldRect.midX, y: feldRect.midY)
      return workspaceGeometry.closestSurfaceForLocation(center)
    }
    else if currentFocus is MZSurface {
      return currentFocus as? MZSurface
    }

    return nil
  }
  
  func rectForControls(_ surface: MZSurface) -> CGRect {
    guard let workspaceGeometry = workspaceGeometry else { return CGRect.zero }
    guard let contentContainerView = contentContainerView else { return CGRect.zero }
    
    var surfaceRect = workspaceGeometry.roundedFrameForSurface(surface)
    surfaceRect = contentContainerView.convert(surfaceRect, to: self.view)
    
    return CGRect(x:(surfaceRect.maxX - view.frame.width > 0 ? view.frame.width - SurfaceControlsBarConstants.margin : surfaceRect.maxX) - SurfaceControlsBarConstants.width + SurfaceControlsBarConstants.margin,
                  y:(surfaceRect.minY - SurfaceControlsBarConstants.height),
                  width: SurfaceControlsBarConstants.width,
                  height: SurfaceControlsBarConstants.height)
  }

  // MARK: Hints
  
  func insertDoubleTapHint() {
    hintsViewController = HintsViewController()
    hintsViewController.currentFocus = delegate?.currentFocusArea() as AnyObject
    hintsViewController.view.frame = CGRect.zero
    view.addSubview(hintsViewController.view)

    // TODO: Confirm complete removal of hint
    hintsViewController.view.isHidden = true
  }
  
  func updateDoubleTapHintLayout() {
    hintsViewController.currentFocus = delegate?.currentFocusArea() as AnyObject
    let currentSurface = currentFocusSurface()
    
    if currentSurface == nil {
      let rect = CGRect(x: self.view.frame.midX - HintConstants.width/2, y: originYForMostTopControl() - HintConstants.bottomMargin - HintConstants.height, width: HintConstants.width, height: HintConstants.height)
      hintsViewController.view.frame = rect
    } else {
      hintsViewController.view.frame = rectForHint(currentSurface!)
    }
  }
  
  func rectForHint(_ surface: MZSurface) -> CGRect {
    let controlsRect = rectForControls(surface)
    let rect = CGRect(x: self.view.frame.midX - HintConstants.width/2, y: controlsRect.origin.y - HintConstants.bottomMargin - HintConstants.height, width: HintConstants.width, height: HintConstants.height)
    return rect;
  }
  
  func originYForMostTopControl() -> CGFloat {
    var y = CGFloat.greatestFiniteMagnitude
    
    for surfaceControlsView in surfaceControlsViews {
      y = min(y, surfaceControlsView.view.frame.origin.y)
    }
    
    return y
  }

  // MARK: Observation

  func beginObservingSystemModel() {
    systemModelObserverArray.append(systemModel!.addObserver(forKeyPath: "currentWorkspace", options:[.new, .old], on: OperationQueue.main) { ( obj, change: [AnyHashable: Any]!) in
      let systemModel = obj as! MZSystemModel
      self.windshield = systemModel.currentWorkspace != nil ? systemModel.currentWorkspace.windshield : nil
    })
  }

  func endObservingSystemModel() {
    if systemModelObserverArray.count > 0 {
      for token in systemModelObserverArray {
        systemModel!.removeObserver(withBlockToken: token)
      }
    }
    systemModelObserverArray.removeAll()
  }

  func beginObservingWindshield() {
    windshieldObserverArray.append(windshield!.addObserver(forKeyPath: "items", options: [.new, .old], on: OperationQueue.main, task: { ( obj, change: [AnyHashable: Any]!) in

      self.updateEnabledControls()

      let kind = NSKeyValueChange(rawValue: change[NSKeyValueChangeKey.kindKey] as! UInt)
      if kind == NSKeyValueChange.insertion {
        let inserted = change[NSKeyValueChangeKey.newKey]

        (inserted as AnyObject).enumerateObjects({ (object, index, stop) in
          self.beginObservingWindshieldItem(object as! MZWindshieldItem)
        })
      } else if kind == NSKeyValueChange.removal {
        let deleted = change[NSKeyValueChangeKey.oldKey]

        (deleted as AnyObject).enumerateObjects({ (object, index, stop) in
          self.endObservingWindshieldItem(object as! MZWindshieldItem)
        })
      }
    }))

    for item in windshield!.items {
      beginObservingWindshieldItem(item as! MZWindshieldItem)
    }
  }

  func endObservingWindshield() {

    guard let windshield = windshield else {
      if windshieldObserverArray.count > 0 {
        print ("Error: SurfaceControlsViewController is leaking observers")
      }
      return
    }

    if windshieldObserverArray.count > 0 {
      for token in windshieldObserverArray {
        windshield.removeObserver(withBlockToken: token)
      }
    }
    windshieldObserverArray.removeAll()

    for item in windshield.items {
      endObservingWindshieldItem(item as! MZWindshieldItem)
    }
  }

  func beginObservingWindshieldItem(_ item: MZWindshieldItem) {
    item.addObserver(self, forKeyPath: "surface", options: [.new], context: nil)
  }

  func endObservingWindshieldItem(_ item: MZWindshieldItem) {
    item.removeObserver(self, forKeyPath: "surface")
  }

  override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {

    if keyPath == "surface" {
      updateEnabledControls()
    }
    else {
      super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
    }
  }
}

extension SurfaceControlsViewController: SurfaceControlBarViewControllerDelegate {

  func didTapClearOnSurface(_ surface: MZSurface) {
    guard let communicator = communicator else { return }
    guard let systemModel = systemModel else { return }
    
    let title = surface.felds.count > 1 ? "Surface Clear Dialog Title Plural" : "Surface Clear Dialog Title Singular"
    let message = surface.felds.count > 1 ? "Surface Clear Dialog Message Plural" : "Surface Clear Dialog Message Singular"
    
    let alertController = UIAlertController(title: title.localized, message: message.localized, preferredStyle: .alert)
    let cancelAction = UIAlertAction(title: "Generic Button Title Cancel".localized, style: .cancel, handler: nil)
    let confirmAction = UIAlertAction(title: "Generic Button Title Clear".localized, style: .default) { (_) in
      communicator.requestor.requestWindshieldClearRequest(systemModel.currentWorkspace.uid, surfaces: [surface.name])
    }

    alertController.addAction(cancelAction)
    alertController.addAction(confirmAction)
    self.present(alertController, animated: true, completion: nil)
  }

  func didTapArrangeOnSurface(_ surface: MZSurface) {
    guard let communicator = communicator else { return }
    guard let systemModel = systemModel else { return }
    communicator.requestor.requestWindshieldArrangeRequest(systemModel.currentWorkspace.uid, surfaces: [surface.name])
  }

}
