//
//  AssetViewController.m
//  Mezzanine
//
//  Created by Zai Chang on 3/7/11.
//  Copyright 2011 Oblong Industries. All rights reserved.
//

#import "AssetViewController.h"
#import "PresentationViewController.h"
#import "MezzanineMainViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "ColourPickerViewController.h"
#import "MezzanineStyleSheet.h"
#import "MZAnnotation+Interactions.h"
#import "NSObject+BlockObservation.h"
#import "ButtonMenuViewController.h"
#import "MZDownloadManager.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "UIImage+Resizing.h"
#import "Three20Style.h"
#import "NSString+VersionChecking.h"
#import "UITraitCollection+Additions.h"
#import "Mezzanine-Swift.h"

#define LOAD_FULL_ORIGINAL_IMAGE 0


@interface AssetViewController ()
{
  NSMutableArray *_undoManagerObservers;
  NSMutableArray *_itemObservers;
  
  UIScrollView *_scrollView;
  UIImageView *_contentImageView;
  UIView *_optionsItemPlaceholderView;
  UIView *_colorPickerPlaceholderView;
  FPPopoverController *_currentPopoverController;
  
  AnnotationsContainerLayer *_annotationsContainerLayer;
  MZAnnotation *_temporaryAnnotation;  // An annotation that is being created
  UIColor *_currentColor;

  UIPanGestureRecognizer *_threeFingerDetectionRecognizer;
  UIGestureRecognizer *_currentDrawingRecognizer;
}

@end


static CGFloat kToolbarAlpha = 1.0;


@implementation AssetViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
      UIColor *annotationPrimaryColor = [NSUserDefaults standardUserDefaults].annotationPrimaryColor;
      if (annotationPrimaryColor)
        self.currentColor = annotationPrimaryColor;
      else
        self.currentColor = [UIColor redColor];
    }
    return self;
}


-(void) dealloc
{
  DLog(@"%@ dealloc", [self class]);
  
  _annotationsContainerLayer.item = nil;  // Stops slide observation
  self.item = nil;
  
  self.placeholderImage = nil;
}


-(void) viewDidLoad 
{
  [super viewDidLoad];

  [self.view setBackgroundColor:[UIColor blackColor]];

  self.view.accessibilityIdentifier = @"AssetAnnotationView";
  
  if (_placeholderImage)
    _placeholderImageView.image = _placeholderImage;

  _videoPlaceholderView.backgroundColor = [MezzanineStyleSheet sharedStyleSheet].videoPlaceholderBackgroundColor;
  _videoPlaceholderView.minTitleFontSize = 12.0;
  _videoPlaceholderView.minSubtitleFontSize = 12.0;

  // At first the image is not loaded so any manipulation UI should be disabled
  [self setManipulationInterfaceEnabled:NO];

  self.titleItem.title = self.title;

  _drawOrNavigationModeItem.title = NSLocalizedString(@"Asset Viewer Draw Button", nil);
  _closeItem.title = NSLocalizedString(@"Asset Viewer Close Button", nil);
  _undoItem.title = NSLocalizedString(@"Asset Viewer Undo Button", nil);
  _redoItem.title = NSLocalizedString(@"Asset Viewer Redo Button", nil);
  _clearItem.title = NSLocalizedString(@"Asset Viewer Clear Button", nil);
  _loadingLabel.text = NSLocalizedString(@"Asset Viewer Loading Label", nil);


  TTStyle *style = [TTSolidFillStyle styleWithColor:[UIColor colorWithWhite:0.0 alpha:0.66] next:nil];
  UIImage *image = [[MezzanineStyleSheet sharedStyleSheet] imageWithTTStyle:style size:CGSizeMake(2, 2)];
  [_toolbar setBackgroundImage:image forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
  [_bottomToolbar setBackgroundImage:image forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];

  // No idea why the close button doesn't inherit from UIAppearance
  _closeItem.tintColor = [MezzanineStyleSheet sharedStyleSheet].defaultHighlightColor;

  _toolbar.alpha = 0.0;
  _bottomToolbar.alpha = 0.0;
  
  // Empty view only for FPPopovers to attach themselves to
  _optionsItemPlaceholderView = [[UIView alloc] initWithFrame:CGRectMake(_toolbar.frame.size.width - 44.0, 0.0, 44.0, 44.0)];
  _optionsItemPlaceholderView.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin);
  _optionsItemPlaceholderView.userInteractionEnabled = NO;
  [_toolbar addSubview:_optionsItemPlaceholderView];
  
  _colorPickerPlaceholderView = [[UIView alloc] initWithFrame:CGRectMake(74, 12, 44, 44)];
  _colorPickerPlaceholderView.userInteractionEnabled = NO;
  [_bottomToolbar addSubview:_colorPickerPlaceholderView];
  
  _progressView.layer.cornerRadius = 6.0;
  
  if (_isLoadingContent)
    _progressView.hidden = NO;
  
  [self updateUndoRedoButtons];
  
  _colourPickerButton.layer.cornerRadius = 5.0;
  _colourPickerButton.layer.borderColor = [MezzanineStyleSheet sharedStyleSheet].buttonBorderColor.CGColor;
  _colourPickerButton.layer.borderWidth = 1.0;
  _colourPickerButton.backgroundColor = self.currentColor;
  _colourPickerButton.alpha = 0.0;
  [_colourPickerButton addTarget:self action:@selector(showColourPicker:) forControlEvents:UIControlEventTouchUpInside];

  _colourPickerButton.alpha = 0.0;
}

-(void) centerScrollViewContents
{
  if (_scrollView == nil || _contentImageView == nil)
    return;
  
  // Center the image if its smaller than the scroll view ... otherwise it automatically
  // pegs the image to the top left
  // From http://stackoverflow.com/questions/1316451/center-content-of-uiscrollview-when-smaller
  // 
  CGFloat offsetX = (_scrollView.bounds.size.width > _scrollView.contentSize.width) ?
                    (_scrollView.bounds.size.width - _scrollView.contentSize.width) * 0.5 : 0.0;
  CGFloat offsetY = (_scrollView.bounds.size.height > _scrollView.contentSize.height) ?
                    (_scrollView.bounds.size.height - _scrollView.contentSize.height) * 0.5 : 0.0;
  _contentImageView.center = CGPointMake(_scrollView.contentSize.width * 0.5 + offsetX,
                                        _scrollView.contentSize.height * 0.5 + offsetY);
}

-(void) updateScrollViewMinimumZoomScale
{
  if (_scrollView == nil || _contentImageView == nil)
    return;
  
  CGRect frame = _scrollView.frame;
#if LOAD_FULL_ORIGINAL_IMAGE
  // The following centers the original image in such a way that it occupies the same space as
  // if we loaded the full slide image (fixed at feld aspect ratio) instead
  CGRect mainFeldspace = MezzanineAppContext.currentContext.applicationModel.systemModel.mainFeldspace;
  frame = ProportionalResizeIntoAndCenter(mainFeldspace, frame);
#endif
  CGSize imageSize = _contentImageView.image.size;
  
  // Figure out minimum scale
  CGFloat minimumZoomScale = 1.0;
  CGFloat viewAspectRatio = frame.size.width/frame.size.height;
  CGFloat imageAspectRatio = imageSize.width/imageSize.height;
  if (viewAspectRatio > imageAspectRatio)
    minimumZoomScale = frame.size.height / imageSize.height;
  else
    minimumZoomScale = frame.size.width / imageSize.width;
  
  _scrollView.minimumZoomScale = minimumZoomScale;
}


-(void) viewWillLayoutSubviews
{
  if (_scrollView.zoomScale == _scrollView.minimumZoomScale)
  {
    [self updateScrollViewMinimumZoomScale];
    [_scrollView setZoomScale:_scrollView.minimumZoomScale animated:NO];
  }
  
  [self centerScrollViewContents];
}


-(void) viewDidLayoutSubviews
{
  [super viewDidLayoutSubviews];
  
  _annotationsContainerLayer.frame = _contentImageView.layer.bounds;
}


- (void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
  
  // This call is needed since some layout is done besides loading the image.
  // Initially it's only when the item is set but that's actually happening before the view is load so some outlets are nil yet (placeholders)
  
#warning Maybe it's worthy to re-think the approach
  if (!_contentImageView.image)
    [self beginImageLoad];
}


-(void) viewDidAppear:(BOOL)animated
{
  [super viewDidAppear:animated];
  
  [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
    _toolbar.alpha = kToolbarAlpha;
    _bottomToolbar.alpha = kToolbarAlpha;
  } completion:nil];
  
  // Analytics
  MezzanineAnalyticsManager *analyticsManager = [MezzanineAnalyticsManager sharedInstance];
  [analyticsManager tagScreen:analyticsManager.localPreviewScreen];
}

-(void) viewWillDisappear:(BOOL)animated
{
  [super viewWillDisappear:animated];
  
  [self dismissCurrentPopoverController];
  
  // Analytics
  [[MezzanineAnalyticsManager sharedInstance] tagWorkspaceStatusScreen];
}


-(void) setPlaceholderImage:(UIImage *)newImage
{
  _placeholderImage = newImage;
  
  _placeholderImageView.image = _placeholderImage;
  _placeholderImageView.hidden = NO;
}


-(void) loadContentImage:(UIImage*)image
{
  if (_scrollView == nil)
  {
    // if placeholder is nil frame is 0 and we need to set at least a valid frame for the layout
    CGRect frame = CGRectMake(0, 20, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame) - 20);
    _scrollView = [[UIScrollView alloc] initWithFrame: frame];
    _scrollView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    _scrollView.scrollsToTop = NO;
    _scrollView.decelerationRate = UIScrollViewDecelerationRateFast;
    _scrollView.delegate = self;
    _scrollView.maximumZoomScale = 1.5;
    [self.view insertSubview:_scrollView belowSubview:self.placeholderImageView];

    // Adding swipe gesture on top of scroll view, using technique shown in
    // http://stackoverflow.com/questions/6425785/uigesturerecognizer-over-uiscrollview
    // http://stackoverflow.com/questions/16301286/capture-only-uiview-2-finger-uipangesturerecognizer
    UISwipeGestureRecognizer *threeFingerUpGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(uploadAnnotatedImageGestureRecognized:)];
    threeFingerUpGesture.delegate = self;
    threeFingerUpGesture.numberOfTouchesRequired = 3;
    threeFingerUpGesture.direction = UISwipeGestureRecognizerDirectionUp;
    [_scrollView addGestureRecognizer:threeFingerUpGesture];
    
    _threeFingerDetectionRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(threeFingerPanRecognized:)];
    _threeFingerDetectionRecognizer.minimumNumberOfTouches = 3;
    _threeFingerDetectionRecognizer.maximumNumberOfTouches = 3;
    _threeFingerDetectionRecognizer.delegate = self;
    [_scrollView addGestureRecognizer:_threeFingerDetectionRecognizer];
    
    [_scrollView.panGestureRecognizer requireGestureRecognizerToFail:threeFingerUpGesture];
  }
  
  BOOL updateScrollViewContentSize = NO;
  CGSize imageSize = image.size;
  if (_contentImageView == nil)
  {
    CGRect imageFrame = CGRectMake(0, 0, imageSize.width, imageSize.height);
    _contentImageView = [[UIImageView alloc] initWithFrame:imageFrame];
    _contentImageView.userInteractionEnabled = YES;
    [_scrollView addSubview:_contentImageView];
    
    _annotationsContainerLayer = [AnnotationsContainerLayer layer];
    _annotationsContainerLayer.frame = _contentImageView.bounds;
    _annotationsContainerLayer.masksToBounds = YES;
    [_contentImageView.layer addSublayer:_annotationsContainerLayer];
    
    updateScrollViewContentSize = YES;
  }

  _contentImageView.image = image;

  if (_annotationsContainerLayer.item != _item)
    _annotationsContainerLayer.item = _item;

  if (updateScrollViewContentSize)
  {
    _scrollView.contentSize = imageSize;
    [self updateScrollViewMinimumZoomScale];

    [_scrollView setZoomScale:_scrollView.minimumZoomScale animated:NO];
  }

  // Hide placeholder once we loaded image it's added
  _placeholderImageView.hidden = YES;
}


-(UIView*) viewForZoomingInScrollView:(UIScrollView *)aScrollView
{
  return _contentImageView;
}


-(void) scrollViewDidZoom:(UIScrollView *)aScrollView
{
  if (!_contentImageView.image)
    return;
  
  [self centerScrollViewContents];
}


-(void) setManipulationInterfaceEnabled:(BOOL)enabled
{
  _optionsItem.enabled = enabled;
  _drawOrNavigationModeItem.enabled = enabled;
  _colourPickerButton.enabled = enabled;
  
  if (enabled)
  {
    // If enabled, these buttons should follow the rules based on modal
    [self updateUndoRedoButtons];
  }
  else
  {
    _undoItem.enabled = enabled;
    _redoItem.enabled = enabled;
    _clearItem.enabled = enabled;
  }
}


-(void) setItem:(MZItem *)anItem
{
  if (_item != anItem)
  {
    if (_item)
    {
      for (id observer in _undoManagerObservers)
        [[NSNotificationCenter defaultCenter] removeObserver:observer];
      _undoManagerObservers = nil;
      
      for (id observer in _itemObservers)
        [_item removeObserverWithBlockToken:observer];
      _itemObservers = nil;
    }
    
    _item = anItem;
    
    if (_item)
    {
      if (self.isViewLoaded)
        [self beginImageLoad];
      
      // Undo-ing across multiple openings of the SlideViewContainer seems to be causing problems, so disabling that for now
      [_item.undoManager removeAllActions];
      
      [self updateUndoRedoButtons];
      
      __weak typeof(self) __self = self;
      
      _itemObservers = [[NSMutableArray alloc] init];
      id observer = [_item addObserverForKeyPath:@"annotations" task:^(id obj, NSDictionary *change) {
        [__self updateUndoRedoButtons];
      }];
      [_itemObservers addObject:observer];
      
      observer = [_item addObserverForKeyPath:@"fullURL" task:^(id obj, NSDictionary *change) {
        if (![change[NSKeyValueChangeOldKey] isEqual:[NSNull null]])
        {
          NSString *oldURL = change[NSKeyValueChangeOldKey];
          [__self unloadOldThumbnailURL:oldURL];
        }
        
        [__self beginImageLoad];
      }];
      [_itemObservers addObject:observer];

      observer = [_item addObserverForKeyPath:@"imageAvailable" task:^(id obj, NSDictionary *change) {
        [__self beginImageLoad];
      }];
      [_itemObservers addObject:observer];
    }
  }
}


- (void) unloadOldThumbnailURL:(NSString *)oldURL
{
  NSURL *url = [_appContext.currentCommunicator urlForResource:oldURL];
  if (url == nil)
    return;
  
  [[MZDownloadManager sharedLoader] unloadImageFromMemoryCacheWithURL:url];
  [[MZDownloadManager sharedLoader] cancelLoadingURLForTarget:self];
}


-(void) beginImageLoad
{
  MZAssetCache *cache = [_appContext assetCache];
#if LOAD_FULL_ORIGINAL_IMAGE
  UIImage *image = [cache originalImageForSlide:_item];
#else
  UIImage *image = [cache fullImageForSlide:_item];
#endif
  if (image)
  {
    // This is called in the next runloop cycle to give UIKit a chance
    // to do any layout in case this is called before the view is actually
    // displayed
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
      [self loadContentImage:image];
    }];
    
    [self setManipulationInterfaceEnabled:YES];
    return;
  }

  BOOL showLoadingIndicator = YES;

#if LOAD_FULL_ORIGINAL_IMAGE
  NSString *imagePath = _item.convertedOriginalURL;
#else
  NSString *imagePath = _item.fullURL;
#endif

  if ([_item isKindOfClass:[MZWindshieldItem class]])
  {
    MZWindshieldItem *windshieldItem = (MZWindshieldItem*)_item;
    NSArray *contentSources = [_appContext.applicationModel.systemModel.currentWorkspace.presentation slidesWithContentSource:windshieldItem.assetUid];
    if (contentSources.count > 0)
    {
      MZPortfolioItem *portfolioItem = contentSources[0];
      imagePath = portfolioItem.fullURL;
    }
  }
  else if ([_item isKindOfClass:[MZLiveStream class]])
  {
    showLoadingIndicator = NO;

    // If the live stream is active but the image url is not available,
    // wait for the next time to load it or remove a previously loaded image
    if (!imagePath)
    {
      _videoPlaceholderView.sourceName = [(MZLiveStream *)_item displayName];
      _videoPlaceholderView.sourceOrigin = [(MZLiveStream *)_item remoteMezzName];
      
      _videoPlaceholderView.hidden = NO;
      _contentImageView.hidden = YES;
      _scrollView.hidden = YES;
      [self setManipulationInterfaceEnabled:NO];
      [self dismissCurrentPopoverController];
      return;
    }
    else
    {
      _videoPlaceholderView.hidden = YES;
      _contentImageView.hidden = NO;
      _scrollView.hidden = NO;
    }
  }

  if (showLoadingIndicator)
    _progressView.hidden = NO;
  _isLoadingContent = YES;

  NSURL *url = [_appContext.currentCommunicator urlForResource:imagePath];
  if (url == nil)
  {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Asset Viewer Missing URL Dialog Title", nil)
                                                                             message:NSLocalizedString(@"Asset Viewer Missing URL Dialog Message", nil)
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Generic Button Title Okay", nil)
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * _Nonnull action) {
                                                       [self close];
                                                     }];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
  }

  // If the image from an item that is not a video is still not available,
  // don't try to the download it and avoid the error message
  if (![_item isKindOfClass:[MZLiveStream class]] && !_item.imageAvailable)
    return;

  MZDownloadCacheType cachingType = [_item isKindOfClass:[MZLiveStream class]] ? MZDownloadCacheTypeOnlyInMemory : MZDownloadCacheTypeDefault;

  __weak typeof(self) __self = self;
  [[MZDownloadManager sharedLoader] loadImageWithURL:url
                                            atTarget:self
                                             success:^(UIImage *image) {
                                               [__self loadContentImage:image];
                                               [__self setManipulationInterfaceEnabled:YES];
                                               __self.progressView.hidden = YES;
                                               __self.isLoadingContent = NO;
                                             }
                                               error:^(NSError *error) {
                                                 // if a live stream thumbnail fails to download don't close the annotation
                                                 // and keep showing the previous one if it is available...
                                                 if ([__self.item isKindOfClass:[MZLiveStream class]])
                                                 {
                                                   if (!_contentImageView.image)
                                                   {
                                                     // ... or show the video placeholder otherwise.
                                                     __self.videoPlaceholderView.hidden = NO;
                                                     [__self setManipulationInterfaceEnabled:NO];
                                                     
                                                     [__self dismissCurrentPopoverController];
                                                   }
                                                 }
                                                 else
                                                 {
                                                   UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Asset Viewer Load Error Dialog Title", nil)
                                                                                                                            message:NSLocalizedString(@"Asset Viewer Load Error Dialog Message", nil)
                                                                                                                     preferredStyle:UIAlertControllerStyleAlert];
                                                   UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Generic Button Title Okay", nil)
                                                                                                      style:UIAlertActionStyleDefault
                                                                                                    handler:^(UIAlertAction * _Nonnull action) {
                                                                                                      [__self close];
                                                                                                    }];
                                                   [alertController addAction:okAction];
                                                   [__self presentViewController:alertController animated:YES completion:nil];

                                                   __self.progressView.hidden = YES;
                                                   __self.isLoadingContent = NO;
                                                  }
                                               }
                                            canceled:nil cachingOption:cachingType];
}


-(void) dismissCurrentPopoverController
{
  if (_currentPopoverController)
  {
    [_currentPopoverController dismissPopoverAnimated:YES];
    _currentPopoverController = nil;
  }
}


-(IBAction) showOptions
{
  ButtonMenuViewController *menuViewController = [[ButtonMenuViewController alloc] initWithNibName:nil bundle:nil];
  
  NSMutableArray *menuItems = [NSMutableArray array];
 
  ButtonMenuItem *item = [ButtonMenuItem itemWithTitle:NSLocalizedString(@"Asset Viewer Upload Image Action Message", nil) action:^{
    [self uploadAsNewAssetAndPlaceOnWindshield:nil];
    [_currentPopoverController dismissPopoverAnimated:NO];
    
    [[MezzanineAnalyticsManager sharedInstance] tagEvent:[MezzanineAnalyticsManager sharedInstance].uploadAnnotationsEvent
                                                            attributes:@{[MezzanineAnalyticsManager sharedInstance].destinationKey : [MezzanineAnalyticsManager sharedInstance].workspaceAttribute}];
  }];
  //  item.enabled = (_item.annotations.count > 0);
  [menuItems addObject:item];

  MZSystemModel *systemModel = _appContext.applicationModel.systemModel;

  if (systemModel.featureToggles.disableDownloadsEnabled && !systemModel.isSignedIn)
  {
    NSString *buttonTitle = NSLocalizedString(@"Asset Viewer Sign In To Save To Library Action Message", nil);
    ButtonMenuItem *saveToLibraryButton = [ButtonMenuItem itemWithTitle:buttonTitle action:nil];
    saveToLibraryButton.enabled = NO;
    [menuItems addObject:saveToLibraryButton];
  }
  else
  {
    [menuItems addObject:[ButtonMenuItem itemWithTitle:NSLocalizedString(@"Asset Viewer Save To Library Action Message", nil) action:^{
      [self saveImageToLibrary:nil];
      [_currentPopoverController dismissPopoverAnimated:NO];
      
      [[MezzanineAnalyticsManager sharedInstance] tagEvent:[MezzanineAnalyticsManager sharedInstance].uploadAnnotationsEvent
                                                              attributes:@{[MezzanineAnalyticsManager sharedInstance].destinationKey : [MezzanineAnalyticsManager sharedInstance].libraryAttribute}];
    }]];
  }

  menuViewController.menuItems = menuItems;
  
  _currentPopoverController = [[FPPopoverController alloc] initWithContentViewController:menuViewController delegate:self];
  [_currentPopoverController stylizeAsActionSheet];
  [_currentPopoverController presentPopoverFromRect:_optionsItemPlaceholderView.frame inView:_optionsItemPlaceholderView.superview permittedArrowDirections:FPPopoverArrowDirectionUp animated:YES];
}


-(IBAction) close
{
  // The following frees up references to self in blocks
  [self endAnnotationCreation];
  
  [self setItem:nil];
  
  [CATransaction begin];
  [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
  [_annotationsContainerLayer removeFromSuperlayer];
  [CATransaction commit];
  _annotationsContainerLayer = nil;
  
  [_appContext.mainNavController popViewControllerWithTransitionView:self.view];
}


#pragma mark - Document Interactions

-(void) openDocumentIn
{
  MZAssetCache *cache = [_appContext assetCache];
  NSURL *url = [cache urlForOriginalSlideImage:_item];
  
  UIDocumentInteractionController *diController = [UIDocumentInteractionController interactionControllerWithURL:url];
  diController.delegate = self;
  diController.UTI = @"com.adobe.pdf";
  [diController presentOpenInMenuFromRect:CGRectZero inView:self.view animated:YES];
}

-(void)documentInteractionController:(UIDocumentInteractionController *)controller willBeginSendingToApplication:(NSString *)application
{
}

-(void)documentInteractionController:(UIDocumentInteractionController *)controller didEndSendingToApplication:(NSString *)application
{
}

-(void)documentInteractionControllerDidDismissOpenInMenu:(UIDocumentInteractionController *)controller
{
  
}



#pragma mark - Annotations

-(void) beginAnnotationCreationWithClass:(Class)annotationClass
{
  if (_temporaryAnnotation)
    return;
  
  _temporaryAnnotation = [[annotationClass alloc] init];
  if (!_temporaryAnnotation)
    return;
  
  _temporaryAnnotation.strokeColour = self.currentColor;
  _temporaryAnnotation.fillColour = self.currentColor;
  
  _scrollView.scrollEnabled = NO;
  
  __weak typeof(self) __self = self;
  __weak MZAnnotation *__temporaryAnnotation = _temporaryAnnotation;
  
  [_annotationsContainerLayer addLayerForAnnotation:_temporaryAnnotation];
  
  [_temporaryAnnotation beginCreationModeInView:_contentImageView completion:^{
    if (!__temporaryAnnotation)
      return;
    
    [__self.item addAnnotation:__temporaryAnnotation];
    
    // End annotation creation and cleanup gesture handlers before starting another one
    [__self endAnnotationCreation];
    
    [__self beginAnnotationCreationWithClass:annotationClass];

  } cancelled:^{
    if (!__temporaryAnnotation)
      return;
    
    // End annotation creation and cleanup gesture handlers before starting another one
    [__self endAnnotationCreation];
    
    [__self beginAnnotationCreationWithClass:annotationClass];

  } gestureBegan:^(UIGestureRecognizer *recognizer) {
    _currentDrawingRecognizer = recognizer;
  } gestureEnded:^(UIGestureRecognizer *recognizer) {
    _currentDrawingRecognizer = nil;
  } ];
}

-(void) endAnnotationCreation
{
  if (_temporaryAnnotation)
  {
    [_annotationsContainerLayer removeLayerForAnnotation:_temporaryAnnotation];
    
    [_temporaryAnnotation endCreationMode];
    _temporaryAnnotation = nil;
  }
  
  _scrollView.scrollEnabled = YES;
}


-(IBAction) toggleDrawMode:(id)sender
{
  if (_temporaryAnnotation)
    [self navigationMode];
  else
    [self freehandMode];
}


-(void) navigationMode
{
  [self endAnnotationCreation];

  _drawOrNavigationModeItem.title = NSLocalizedString(@"Asset Viewer Draw Button", nil);

  [UIView animateWithDuration:0.12 animations:^{
    self.colourPickerButton.alpha = 0.0;
  }];
}


-(void) freehandMode
{
  [self beginAnnotationCreationWithClass:[MZFreehandAnnotation class]];

  _drawOrNavigationModeItem.title = NSLocalizedString(@"Asset Viewer Navigation Button", nil);

  [UIView animateWithDuration:0.12 animations:^{
    self.colourPickerButton.alpha = 1.0;
  }];
}


-(IBAction) textMode:(id)sender
{
  // Future
  //[self beginAnnotationCreationWithClass:[MZTextAnnotation class]];
}


-(IBAction) arrowMode:(id)sender
{
  // Future
  //[self beginAnnotationCreationWithClass:[MZArrowAnnotation class]];
}


-(IBAction) showColourPicker:(id)sender
{
  ColourPickerViewController *controller = [[ColourPickerViewController alloc] initWithNibName:nil bundle:nil];
  controller.selectedColour = self.currentColor;
  
  __weak typeof(self) __self = self;
  
  if (self.traitCollection.isRegularWidth && self.traitCollection.isRegularHeight)
  {
    [self dismissCurrentPopoverController];
    
    controller.preferredContentSize = CGSizeMake(320, 452);
    _currentPopoverController = [[FPPopoverController alloc] initWithContentViewController:controller delegate:self];
    [_currentPopoverController stylizeWithNavigationBar:NO];
    
    _currentPopoverController.customTint = [MezzanineStyleSheet sharedStyleSheet].grey50Color;
    
    _currentPopoverController.arrowDirection = UIPopoverArrowDirectionDown;
    //[currentPopoverController presentPopoverFromView:self.bottomToolbar];
    [_currentPopoverController presentPopoverFromRect:_colorPickerPlaceholderView.frame inView:self.bottomToolbar permittedArrowDirections:FPPopoverArrowDirectionDown animated:YES];
    
    controller.selectedColourChanged = ^(UIColor *newColor) {
      __self.currentColor = newColor;
      
      [_currentPopoverController dismissPopoverAnimated:YES];
      _currentPopoverController = nil;
    };
  }
  else
  {
    controller.navigationItem.title = NSLocalizedString(@"Annotation Color Picker Title", nil);
    controller.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Generic Button Title Cancel", nil) style:UIBarButtonItemStylePlain target:self action:@selector(dismissColourPicker:)];
    MezzanineNavigationController *navController = [[MezzanineNavigationController alloc] initWithRootViewController:controller];
    [self presentViewController:navController animated:YES completion:nil];
    
    controller.selectedColourChanged = ^(UIColor *newColor) {
      __self.currentColor = newColor;
      
      [__self dismissViewControllerAnimated:YES completion:nil];
    };
  }
}


-(void) dismissColourPicker:(id)sender
{
  [self dismissViewControllerAnimated:YES completion:nil];
}


-(void) setCurrentColor:(UIColor *)color
{
  _currentColor = color;
  
  if (_currentColor)
  {
    self.colourPickerButton.backgroundColor = _currentColor;
    
    if (_temporaryAnnotation)
    {
      _temporaryAnnotation.fillColour = _currentColor;
      _temporaryAnnotation.strokeColour = _currentColor;
      _temporaryAnnotation.needsRedraw = YES;
    }
    
    [[NSUserDefaults standardUserDefaults] setAnnotationPrimaryColor:_currentColor];
  }
}


-(NSUndoManager*) undoManager
{
  return _item.undoManager;
}


-(void) updateUndoRedoButtons
{
  _undoItem.enabled = _item.undoManager.canUndo;
  _redoItem.enabled = _item.undoManager.canRedo;
  _clearItem.enabled = (_item.annotations.count > 0);
}


-(IBAction) undo:(id)sender
{
  if (self.undoManager.canUndo)
    [self.undoManager undo];
  [self updateUndoRedoButtons];
}


-(IBAction) redo:(id)sender
{
  if (self.undoManager.canRedo)
    [self.undoManager redo];
  [self updateUndoRedoButtons];
}


-(IBAction) clear:(id)sender
{
  if (_item)
  {
    [self.undoManager beginUndoGrouping];
    
    [_item clearAnnotations];
    
    [self.undoManager endUndoGrouping];
  }
}


-(UIImage*) annotatedImage
{
  UIImage *image = _contentImageView.image;
  CGSize imageSize = image.size;
  DLog(@"Drawing annotated image of size %f x %f", imageSize.width, imageSize.height);
  UIGraphicsBeginImageContextWithOptions(imageSize, NO, 1.0);
  
  CGContextRef context = UIGraphicsGetCurrentContext();
  
  // This is only to match the gray slide background that native puts on slides ... necessary?
//  CGContextSetFillColorWithColor(context, [UIColor colorWithWhite:0.5 alpha:1.0].CGColor);
//  CGContextFillRect(context, CGRectMake(0, 0, imageSize.width, imageSize.height));
  
  // Option A: separate rendering
  //[image drawInRect:CGRectMake(0, 0, imageSize.width, imageSize.height)];
  //[annotationsContainerLayer renderInContext:context];
  
  // Option B: Combined layer rendering
  [_contentImageView.layer renderInContext:context];
  
  UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  return outputImage;
}


-(IBAction) uploadAsNewAssetAndPlaceOnWindshield:(id)sender
{
  [self uploadAnnotatedImage:^(NSString *assetUid) {
    MZCommunicator *communicator = _appContext.currentCommunicator;
    MZTransaction *transaction = [communicator.requestor requestWindshieldItemCreateRequest:_workspace.uid contentSource:assetUid surfaceName:@"main" feldRelativeCoords:[FeldRelativeCoords new] feldRelativeSize:[FeldRelativeSize new]];

    transaction.failedBlock = ^(NSError *error) {
      NSString *message = [NSLocalizedString(@"Asset Viewer PlaceOnWindshieldError Title", nil) stringByAppendingFormat:@": %@", error.userInfo[@"description"]];
      [MezzanineAppContext.currentContext.popupInfoView displayMessage:message withTimeout:5.0];
    };
  }];
}


- (void)uploadAnnotatedImage:(void (^)(NSString *assetUid))completion
{
  UIImage *annotatedImage = [self annotatedImage];
  
  MZFileUploadInfo *uploadInfo = [[MZFileUploadInfo alloc] init];
  uploadInfo.fileName = @"Annotated image.png";
  uploadInfo.format = @"png";
  uploadInfo.imageBlock = ^{ return annotatedImage; };
  
  MZCommunicator *communicator = _appContext.currentCommunicator;
  
  void (^success)(void) = ^() {
    [MezzanineAppContext.currentContext.popupInfoView displayMessage:NSLocalizedString(@"Asset Viewer Image Was Uploaded Message", nil) withTimeout:3.0];
    
    NSString *assetUid = uploadInfo.assetUids[0];
    if (completion)
      completion(assetUid);
  };
  
  void (^error)(NSError *) = ^(NSError *error){
    NSString *errorMessage = NSLocalizedString(@"Asset Viewer Uploaded Error Dialog Message", nil);
    if (error.localizedDescription && ![error.localizedDescription isEqualToString:@""])
      errorMessage = error.localizedDescription;
    [MezzanineAppContext.currentContext.popupInfoView displayMessage:errorMessage withTimeout:3.0];
  };

  [communicator.uploader requestFilesUpload:@[uploadInfo]
                               workspaceUid:communicator.systemModel.currentWorkspace.uid
                               limitReached:nil
                      singleUploadCompleted:success
                               errorHandler:error];
  
  [MezzanineAppContext.currentContext.popupInfoView displayMessage:NSLocalizedString(@"Asset Viewer Uploading Image Message", nil) withTimeout:3.0];
}


-(IBAction) saveImageToLibrary:(id)sender
{
  UIImage *image = (_item.annotations.count > 0) ? [self annotatedImage] : _contentImageView.image;
  
  [PhotoManager.sharedInstance authorizationStatus:^(PHAuthorizationStatus status, BOOL promptedAccess) {
    if (status == PHAuthorizationStatusAuthorized)
    {
      [PhotoManager.sharedInstance saveImageToLibrary:image completion:^(BOOL success, NSError * _Nullable error) {
        dispatch_async(dispatch_get_main_queue(), ^{
          if (success)
            _appContext.currentCommunicator.systemModel.popupMessage = NSLocalizedString(@"Asset Viewer Saved To Library Message Success",nil);
          
          else
            _appContext.currentCommunicator.systemModel.popupMessage = [NSString stringWithFormat: NSLocalizedString(@"Asset Viewer Saved To Library Message Error",nil), error.localizedDescription];
          
        });
      }];
    }
    else if (!promptedAccess)
    {
      [self presentErrorWithCode:status];
    }
  }];
}


- (void)presentErrorWithCode:(NSInteger)statusCode
{
  NSString *errorMessage = nil;
  NSString *title = nil;

  switch (statusCode) {
    case PHAuthorizationStatusDenied:
    case PHAuthorizationStatusRestricted:
      title = NSLocalizedString(@"Photos Framework Error Enabling Access Title", nil);
      errorMessage = NSLocalizedString(@"Photos Framework Error Enabling Access Message", nil);
      break;
    default:
      title = NSLocalizedString(@"Photos Framework Error Dialog Title", nil);
      errorMessage = NSLocalizedString(@"Photos Framework Error Default Message", nil);
      break;
  }
  
  UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
  UIAlertAction *settingsAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Generic Title Settings", "") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
  } ];
  
  UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Generic Title OK", "") style:UIAlertActionStyleDefault handler:nil];
  
  [alertController addAction:settingsAction];
  [alertController addAction:okAction];
  
  [self.navigationController presentViewController:alertController animated:YES completion:nil];
}


#pragma mark - Upload Gesture

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
  return YES;
}


- (void)threeFingerPanRecognized:(UIGestureRecognizer*)recognizer
{
  if (recognizer.state == UIGestureRecognizerStateBegan)
  {
    _scrollView.panGestureRecognizer.enabled = NO;
    _scrollView.panGestureRecognizer.enabled = YES;
    
    _currentDrawingRecognizer.enabled = NO;
    _currentDrawingRecognizer.enabled = YES;
  }
}


- (void)uploadAnnotatedImageGestureRecognized:(UISwipeGestureRecognizer*)recognizer
{
  if (recognizer.state == UIGestureRecognizerStateRecognized)
    [self uploadAsNewAssetAndPlaceOnWindshield:nil];
}


#pragma mark UIPopoverControllerDelegate

- (void)popoverControllerDidDismissPopover:(FPPopoverController *)popoverController
{
  if (_currentPopoverController)
  {
    _currentPopoverController = nil;
  }
}

@end
