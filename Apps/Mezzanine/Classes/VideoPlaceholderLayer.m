//
//  VideoPlaceholderView.m
//  Mezzanine
//
//  Created by Zai Chang on 8/20/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import "VideoPlaceholderLayer.h"
#import "MezzanineStyleSheet.h"


@implementation VideoPlaceholderLayer

@synthesize sourceName;
@synthesize sourceOrigin;

@synthesize maxTitleFontSize;
@synthesize minTitleFontSize;
@synthesize minSubtitleFontSize;


-(id) init
{
  self = [super init];
  if (self)
  {
    self.minificationFilter = @"linear";
    
    maxTitleFontSize = 48.0;
    minTitleFontSize = 16.0;
    minSubtitleFontSize = 16.0;
    _containerZoomScale = 1.0;
  }
  return self;
}


-(void) setSourceName:(NSString*)name
{
  if (sourceName != name)
  {
    sourceName = name;
    
    [self setNeedsDisplay];
  }
}


-(void) setSourceOrigin:(NSString*)origin
{
  if (sourceOrigin != origin)
  {
    sourceOrigin = origin;
    
    [self setNeedsDisplay];
  }
}


-(void) setContainerZoomScale:(CGFloat)containerZoomScale
{
  _containerZoomScale = containerZoomScale == 1.0 ? 1.0 : containerZoomScale * 2.0;
}


-(void) drawInContext:(CGContextRef)context
{
  MezzanineStyleSheet *styleSheet = [MezzanineStyleSheet sharedStyleSheet];
  
  UIGraphicsPushContext(context);
  
  CGContextSetAllowsAntialiasing(context, YES);
  CGContextSetAllowsFontSmoothing(context, YES);
  CGContextSetShouldAntialias(context, YES);
  CGContextSetShouldSmoothFonts(context, YES);
  CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
 
  CGContextSetFillColorWithColor(context, styleSheet.videoPlaceholderBackgroundColor.CGColor);
  CGContextFillRect(context, self.bounds);
  
  // Corner brackets
  CGFloat minimumDimension = MIN(self.bounds.size.width, self.bounds.size.height);
  CGFloat margin = minimumDimension / 9.0;
  CGFloat length = minimumDimension / 9.0;
  
  CGFloat fontSize = MAX(MIN(minimumDimension / 6.0, maxTitleFontSize), minTitleFontSize) / _containerZoomScale;
  CGRect bracketsRect = CGRectInset(self.bounds, margin, margin);
  CGFloat lineWidth = MAX(1.0, fontSize / 14.0);
  CGContextSetLineWidth(context, lineWidth);
  
  [styleSheet.videoPlaceholderPrimaryColor setStroke];
  [styleSheet.videoPlaceholderPrimaryColor setFill];
  
  
  CGMutablePathRef path = CGPathCreateMutable();
  // Top left corner
  CGPathMoveToPoint(path, NULL, CGRectGetMinX(bracketsRect) + length, CGRectGetMinY(bracketsRect));
  CGPathAddLineToPoint(path, NULL, CGRectGetMinX(bracketsRect), CGRectGetMinY(bracketsRect));
  CGPathAddLineToPoint(path, NULL, CGRectGetMinX(bracketsRect), CGRectGetMinY(bracketsRect) + length);
  
  // Top right corner
  CGPathMoveToPoint(path, NULL, CGRectGetMaxX(bracketsRect) - length, CGRectGetMinY(bracketsRect));
  CGPathAddLineToPoint(path, NULL, CGRectGetMaxX(bracketsRect), CGRectGetMinY(bracketsRect));
  CGPathAddLineToPoint(path, NULL, CGRectGetMaxX(bracketsRect), CGRectGetMinY(bracketsRect) + length);
  
  // Bottom right corner
  CGPathMoveToPoint(path, NULL, CGRectGetMaxX(bracketsRect) - length, CGRectGetMaxY(bracketsRect));
  CGPathAddLineToPoint(path, NULL, CGRectGetMaxX(bracketsRect), CGRectGetMaxY(bracketsRect));
  CGPathAddLineToPoint(path, NULL, CGRectGetMaxX(bracketsRect), CGRectGetMaxY(bracketsRect) - length);
  
  // Bottom left corner
  CGPathMoveToPoint(path, NULL, CGRectGetMinX(bracketsRect) + length, CGRectGetMaxY(bracketsRect));
  CGPathAddLineToPoint(path, NULL, CGRectGetMinX(bracketsRect), CGRectGetMaxY(bracketsRect));
  CGPathAddLineToPoint(path, NULL, CGRectGetMinX(bracketsRect), CGRectGetMaxY(bracketsRect) - length);
  
  CGContextAddPath(context, path);
  CGContextDrawPath(context, kCGPathStroke);
  CGPathRelease(path);
  

  CGFloat subtitleFontSize = (NSInteger) MAX(fontSize / 1.5, minSubtitleFontSize) / _containerZoomScale;
  CGFloat textMargin = margin / _containerZoomScale;
  CGFloat paddingBetweenLines = (NSInteger) (subtitleFontSize / 8.0) / _containerZoomScale;

  UIFont *font = [UIFont dinOfSize:fontSize];
  NSString *title = sourceName ? sourceName : NSLocalizedString(@"Video Placeholder Video Title", nil);

  if (_isLocalVideo || !sourceOrigin || [sourceOrigin isEqualToString:@""])
  {
    CGFloat offsetY = 0.0;
    CGRect videoLabelRect = CGRectMake(textMargin, (self.bounds.size.height - fontSize*1.3) / 2.0 + offsetY, self.bounds.size.width - 2 * textMargin, fontSize*1.3);

    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByTruncatingTail;
    paragraphStyle.alignment = NSTextAlignmentCenter;

    [title drawInRect:videoLabelRect
       withAttributes:@{NSFontAttributeName:font,
                        NSParagraphStyleAttributeName:paragraphStyle,
                        NSForegroundColorAttributeName: styleSheet.videoPlaceholderPrimaryColor}];
  }
  else
  {
    CGFloat offsetY = CGRectGetMidY(self.frame) - font.pointSize - 4.0 - paddingBetweenLines / 2.0;
    CGRect videoLabelRect = CGRectMake((NSInteger) textMargin, (NSInteger) offsetY, (NSInteger) (self.bounds.size.width - 2.0 * textMargin), (NSInteger) (font.pointSize * 1.3));

    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByTruncatingTail;
    paragraphStyle.alignment = NSTextAlignmentCenter;

    [title drawInRect:videoLabelRect
       withAttributes:@{NSFontAttributeName:font,
                        NSParagraphStyleAttributeName:paragraphStyle,
                        NSForegroundColorAttributeName: styleSheet.videoPlaceholderPrimaryColor}];

    UIFont *font = [UIFont dinOfSize:subtitleFontSize];
    offsetY = CGRectGetMaxY(videoLabelRect) + paddingBetweenLines;

    CGRect rect = CGRectMake((NSInteger) textMargin, (NSInteger)  offsetY, (NSInteger) (self.bounds.size.width - 2.0 * textMargin), (NSInteger) (font.pointSize * 1.3));

    paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByTruncatingTail;
    paragraphStyle.alignment = NSTextAlignmentCenter;

    [sourceOrigin drawInRect:rect
              withAttributes:@{NSFontAttributeName:font,
                               NSParagraphStyleAttributeName:paragraphStyle,
                               NSForegroundColorAttributeName: styleSheet.videoPlaceholderSecondaryColor}];
  }
  
  UIGraphicsPopContext();
}


- (void)drawRect:(CGRect)rect
{
  CGContextRef context = UIGraphicsGetCurrentContext();
  [self drawInContext:context];
}

@end
