//
//  BinCollectionViewInteractor.swift
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 20/02/2017.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import UIKit

class BinCollectionViewInteractor: NSObject {
  weak var presenter: BinCollectionViewController!
  var entity: BinCollectionViewDataSource! {
    didSet {
      self.entity.workspace = systemModel.currentWorkspace
    }
  }
  
  var systemModelObserverArray = Array <String> ()
  var communicatorObserverArray = Array <String> ()
  var workspaceObserverArray = Array <String> ()
  var presentationObserverArray = Array <String> ()
  var liveStreamsObserverArray = Array <String> ()
  
  @objc var communicator: MZCommunicator! {
    willSet(newCommunicator) {
      if self.communicator != newCommunicator {
        endObservingCommunicator()
      }
    }
    
    didSet {
      communicatorObserverArray.removeAll()
      
      if communicator != nil {
        beginObservingCommunicator()
      }
    }
  }
  
  @objc var systemModel: MZSystemModel! {
    willSet(newSystemModel) {
      if self.systemModel != newSystemModel {
        endObservingSystemModel()
      }
    }
    
    didSet {
      systemModelObserverArray.removeAll()
      
      if systemModel != nil {
        presentation = systemModel.currentWorkspace.presentation
        beginObservingSystemModel()
      }
      else {
        presentation = nil
        workspace = nil
      }
    }
  }
  
  // FIXME: Shall it be optional?
  @objc var workspace: MZWorkspace! {
    willSet(newWorkspace) {
      if self.workspace != newWorkspace && self.workspace != nil {
        endObservingWorkspace()
        endObservingLivestreams()
      }
    }
    
    didSet {
      workspaceObserverArray.removeAll()
      liveStreamsObserverArray.removeAll()
      
      if workspace != nil {
        beginObservingWorkspace()
        beginObservingLivestreams()
      }
    }
  }
  
  var presentation: MZPresentation! {
    willSet(newPresentation) {
      if self.presentation != newPresentation && self.presentation != nil{
        endObservingPresentation()
      }
    }
    
    didSet {
      presentationObserverArray.removeAll()
      
      if presentation != nil {
        beginObservingPresentation()
      }
    }
  }
  
  
  init(systemModel: MZSystemModel, communicator: MZCommunicator, entity: BinCollectionViewDataSource) {
    
    self.communicator = communicator
    
    self.systemModel = systemModel
    self.workspace = self.systemModel.currentWorkspace
    self.presentation = self.workspace.presentation
    
    self.entity = entity
    self.entity.communicator = self.communicator
    self.entity.workspace = self.workspace
    
    super.init()
    
    beginObservingCommunicator()
    beginObservingSystemModel()
    beginObservingWorkspace()
    beginObservingPresentation()
    beginObservingLivestreams()
    
  }
  
  required init?(coder aDecoder: NSCoder) {
    
    fatalError("init(coder:) has not been implemented")
  }

  deinit {
    print("BinCollectionViewInteractor deinit")
  }
  
  
  // MARK: Observation
  
  func beginObservingCommunicator() {
    // pendingTransactions
    communicatorObserverArray.append(communicator.addObserver(forKeyPath: "pendingTransactions", options:[.new, .old], on: OperationQueue.main) { ( obj, change: [AnyHashable: Any]!) in
      self.updateTransferStatusMessage()
      })
  }
  
  func endObservingCommunicator() {
    if communicatorObserverArray.count > 0 {
      for token in communicatorObserverArray {
        communicator.removeObserver(withBlockToken: token)
      }
    }
  }
  
  func beginObservingSystemModel() {
    
    // currentWorkspace
    systemModelObserverArray.append(systemModel.addObserver(forKeyPath: "currentWorkspace", options:[.new, .old], on: OperationQueue.main) { ( obj, change: [AnyHashable: Any]!) in
      let systemModel = obj as! MZSystemModel
      if let workspace = systemModel.currentWorkspace {
        self.workspace = workspace
        self.presentation = self.workspace.presentation
      }
      else {
        self.presentation = nil
        self.workspace = nil
      }
      
      self.updateEntity()
      })
    
    // portfolioExport.info.progress
    systemModelObserverArray.append(systemModel.addObserver(forKeyPath: "portfolioExport.info.progress", options:[.new, .old], on: OperationQueue.main) { ( obj, change: [AnyHashable: Any]!) in
      self.updateTransferStatusMessage()
      })
    
    
  }
  
  func endObservingSystemModel() {
    if systemModelObserverArray.count > 0 {
      for token in systemModelObserverArray {
        systemModel.removeObserver(withBlockToken: token)
      }
    }
  }
  
  func beginObservingWorkspace() {
    
    // liveStreams
    workspaceObserverArray.append(workspace.addObserver(forKeyPath: "liveStreams", options:[.new, .old], on: OperationQueue.main) { ( obj, change: [AnyHashable: Any]!) in
      
      self.updateEntity()

      let kind = change[NSKeyValueChangeKey.kindKey] as! Int
      if kind == 2 /*NSKeyValueChangeInsertion*/ {
        let inserted = change[NSKeyValueChangeKey.newKey]
        
        (inserted as AnyObject).enumerateObjects({ (object, index, stop) in
          self.beginObservingLiveStream(object as! MZLiveStream)
        })
      } else if kind == 3 /*NSKeyValueChangeRemoval*/ {
        let deleted = change[NSKeyValueChangeKey.oldKey]
        
        (deleted as AnyObject).enumerateObjects({ (object, index, stop) in
          self.endObservingLiveStream(object as! MZLiveStream)
        })
      }
    })
  }
  
  func endObservingWorkspace() {
    if workspaceObserverArray.count > 0  {
      for token in workspaceObserverArray {
        workspace.removeObserver(withBlockToken: token)
      }
    }
  }
  
  func beginObservingPresentation() {
    
    // active
    presentationObserverArray.append(presentation.addObserver(forKeyPath: "active", options:[.new, .old, .initial], on: OperationQueue.main) { ( obj, change: [AnyHashable: Any]!) in
      self.requestUpdateCurrentSlideView(self.presentation.currentSlideIndex, animated: false, active: self.presentation.active)
      
      self.updateEntity()
      })
    
    // currentSlideIndex
    presentationObserverArray.append(presentation.addObserver(forKeyPath: "currentSlideIndex", options:[.new, .old], on: OperationQueue.main) { ( obj, change: [AnyHashable: Any]!) in
      self.requestUpdateCurrentSlideView(self.presentation.currentSlideIndex, animated: true, active: self.presentation.active)
      
      self.updateEntity()
      })
    
    // slides
    presentationObserverArray.append(presentation.addObserver(forKeyPath: "slides", options:[.new, .old], on: OperationQueue.main) { ( obj, change: [AnyHashable: Any]!) in
      
      let kind = NSKeyValueChange(rawValue: change[NSKeyValueChangeKey.kindKey] as! UInt)!
      let indexSet = change[NSKeyValueChangeKey.indexesKey]
      var cellUpdates = Array<[String: AnyObject]>()
      
      // Insertions come one by one
      // Deletions come all at once...
      
      // If we are in Mezz-In there's only one section hence we are in 0, otherwise 1
      let sectionIndex = self.hasLiveStreamsSection() ? 1 : 0
      if kind == .insertion {
        (indexSet! as AnyObject).enumerate { (index, stop) in
          // This means we will replace the placeholder by the actual slide or the opposite ONLY if there's one change
          if self.entity.workspace.presentation.numberOfSlides == 1 && (indexSet! as AnyObject).count() == 1 {
            cellUpdates.append(["kind": NSKeyValueChange.replacement.rawValue as AnyObject, "indexPath" :IndexPath(item: index, section: sectionIndex) as AnyObject])
          } else {
            cellUpdates.append(["kind": kind.rawValue as AnyObject, "indexPath" :IndexPath(item: index, section: sectionIndex) as AnyObject])
          }
        }
      } else if kind == .removal {
        (indexSet! as AnyObject).enumerate { (index, stop) in
          // This means we will replace the placeholder by the actual slide or the opposite
          if Int((indexSet! as AnyObject).count()) - 1 == index && self.entity.workspace.presentation.numberOfSlides == 0 {
            cellUpdates.append(["kind": NSKeyValueChange.replacement.rawValue as AnyObject, "indexPath" :IndexPath(item: index, section: sectionIndex) as AnyObject])
          } else {
            cellUpdates.append(["kind": kind.rawValue as AnyObject, "indexPath" :IndexPath(item: index, section: sectionIndex) as AnyObject])
          }
        }
      }
      
      self.updateEntity()
      self.requestUpdateCollectionViewItems(cellUpdates)
      })
    
    self.requestReloadUpdateCollectionView()
  }
  
  func endObservingPresentation() {
    if presentationObserverArray.count > 0 {
      for token in presentationObserverArray {
        presentation.removeObserver(withBlockToken: token)
      }
    }
  }
  
  func beginObservingLivestreams() {
    for liveStream in workspace.liveStreams {
      self.beginObservingLiveStream(liveStream as! MZLiveStream)
    }
  }
  
  func beginObservingLiveStream(_ liveStream: MZLiveStream) {
    liveStreamsObserverArray.append(liveStream.addObserver(forKeyPath: "active", options:[.new, .old], on: OperationQueue.main) { ( obj, change: [AnyHashable: Any]!) in
      
      // We receive multiple updates, if there's no change no bother to do anything
      if change[NSKeyValueChangeKey.oldKey] as! Bool  == change[NSKeyValueChangeKey.newKey] as! Bool {
        return
      }

      // Needed to update if now we have livestream section
      self.updateEntity()
      
      let valueChange = liveStream.active == true ? NSKeyValueChange.insertion.rawValue : NSKeyValueChange.removal.rawValue
      let kind = NSKeyValueChange(rawValue: valueChange)!
      
      if kind == .insertion {
        let itemIndex = self.entity.validLiveStreams().index(of: liveStream)
        let indexPath = IndexPath(item: itemIndex!, section: 0)

        if self.entity.numberOfLiveStreamsItemsInCollectionView() > 1 {
          self.requestUpdateCollectionViewItems([["kind": kind.rawValue as AnyObject, "indexPath": indexPath as AnyObject]])
        } else if self.entity.numberOfLiveStreamsItemsInCollectionView() == 1 {
          self.requestUpdateCollectionViewSections([["kind": kind.rawValue as AnyObject, "indexPath": indexPath as AnyObject]])
        }
      } else {
        var itemIndex = -1
        for (index, validLiveStream) in self.workspace.liveStreams.enumerated() {
          if (validLiveStream as AnyObject).uid == liveStream.uid && change[NSKeyValueChangeKey.oldKey] as! Bool == true {
            itemIndex = min(index, self.entity.validLiveStreams().count)
            break
          }
        }
        
        let indexPath = IndexPath(item: itemIndex, section: 0)
        if self.entity.numberOfLiveStreamsItemsInCollectionView() >= 1 {
          self.requestUpdateCollectionViewItems([["kind": kind.rawValue as AnyObject, "indexPath": indexPath as AnyObject]])
        } else {
          self.requestUpdateCollectionViewSections([["kind": kind.rawValue as AnyObject, "indexPath": indexPath as AnyObject]])
        }
      }
      
      self.updateEntity()
      })
  }
  
  func endObservingLiveStream(_ liveStream: MZLiveStream) {
    let index = workspace.liveStreams.index(of: liveStream)
    if index != NSNotFound {
      liveStream.removeObserver(withBlockToken: liveStreamsObserverArray[index])
    }
  }
  
  func endObservingLivestreams() {
    if liveStreamsObserverArray.count > 0 {
      for liveStream in workspace.liveStreams {
        self.endObservingLiveStream(liveStream as! MZLiveStream)
      }
    }
  }
  
  
  // MARK: Entity
  
  func updateEntity() {
    entity.workspace = systemModel.currentWorkspace
  }
  
  
  // MARK: Public
  
  func updateTransferStatusMessage() {
    let pendingTransactions = communicator.pendingTransactions
    let numberOfImageUploads = communicator.numberOfImagesBeingUploaded()
    let numberOfFileUploads = communicator.numberOfFilesBeingUploaded()
    
    var uploading = false
    var statusText = ""
    
    let transactionsWithTitle = pendingTransactions?.filtered(using: NSPredicate(format: "title.length > 0", argumentArray: nil))
    if (transactionsWithTitle?.count)! > 0 {
      statusText = (transactionsWithTitle?.first as! MZTransaction).title
    } else if numberOfImageUploads >= 1 || numberOfFileUploads >= 1 {
      statusText = "Portfolio Uploading Status Partial Message (Uploading)".localized
      uploading = true
      if numberOfImageUploads > 1 {
        statusText = String(format: "Portfolio Uploading Status Partial Message (...# images)".localized, statusText, numberOfImageUploads)
      } else if numberOfImageUploads == 1 {
        statusText = String(format: "Portfolio Uploading Status Partial Message (...image)".localized, statusText)
      }
      
      if numberOfImageUploads > 0 && numberOfFileUploads > 0 {
        statusText = String(format: "Portfolio Uploading Status Partial Message (...and)".localized, statusText)
      }
      
      if numberOfFileUploads > 1 {
        statusText = String(format: "Portfolio Uploading Status Partial Message (...# PDFs)".localized, statusText, numberOfFileUploads)
      } else if (numberOfFileUploads == 1) {
        statusText = String(format: "Portfolio Uploading Status Partial Message (...PDF)".localized, statusText)
      }
    }
    
    let exportRequested = systemModel.portfolioExport.info.state == .requested
    let downloading = systemModel.portfolioExport.info.state == .downloading
    var exportingText = ""
    
    switch systemModel.portfolioExport.info.state {
    case .requested:
      exportingText = uploading ? String(format:"Portfolio Preparing Export Lowercase Status Message".localized) :
        String(format:"Portfolio Preparing Export Uppercase Status Message").localized
      break
    case .downloading:
      exportingText = uploading ? "Portfolio Downloading Export Lowercase Status Message".localized :
        "Portfolio Downloading Export Uppercase Status Message".localized
      break
      
    default:
      exportingText = ""
      break;
    }
    
    let statusLabel = String(format: "%@%@%@", statusText, uploading && (downloading || exportRequested) ? "Portfolio Downloading And Uploading Nexus".localized : "", exportingText)
    requestUpdateTransferStatusMessage(uploading || exportRequested || downloading, message: statusLabel)
  }
  
  
  // MARK: Layout
  
  fileprivate func requestUpdateCurrentSlideView(_ index: Int, animated: Bool, active: Bool) {
    presenter?.updateCurrentSlideFrameView(index, animated: animated, active: active)
  }
  
  fileprivate func requestUpdateCollectionViewItems(_ updates: Array<[String: AnyObject]>) {
    presenter?.updateCollectionViewItems(updates)
  }
  
  fileprivate func requestUpdateCollectionViewSections(_ updates: Array<[String: AnyObject]>) {
    presenter?.updateCollectionViewSections(updates)
    presenter?.updateCurrentSlideFrameView(presentation.currentSlideIndex, animated: true, active: presentation.active)
  }
  
  fileprivate func requestReloadUpdateCollectionView() {
    presenter?.reloadCollectionView()
  }
  
  fileprivate func requestUpdateTransferStatusMessage(_ visible: Bool, message: String) {
    presenter?.updateTransferStatusLabel(visible, message: message)
  }
  
  
  // MARK: Presenter calls
  
  func placeItemOnScreen(_ slide: MZItem) {
    var uid = String()
    if slide is MZLiveStream {
      uid = slide.uid
    } else if slide is MZPortfolioItem {
      uid = slide.contentSource
    }
    
    if workspace != nil {
      communicator.requestor.requestWindshieldItemCreateRequest(workspace.uid, contentSource: uid, surfaceName: "main", feldRelativeCoords: FeldRelativeCoords(), feldRelativeSize: FeldRelativeSize())
    }
  }
  
  func presentOrJumpToItemAtIndex(_ index: Int) {
    if workspace != nil {
      communicator.requestor.requestPresentationStartRequest(workspace.uid, currentIndex: index)
    }
  }
  
  func deleteItem(_ item: MZItem) {
    if workspace != nil {
      communicator.requestor.requestPortfolioItemDeleteRequest(workspace.uid, uid: item.uid)
    }
  }
  
  func grabItem(_ item: MZItem) {
    if workspace != nil {
      if item is MZLiveStream {
        communicator.requestor.requestLiveStreamGrabRequest(workspace.uid, uid: item.uid)
      } else if item is MZPortfolioItem {
        communicator.requestor.requestPortfolioItemGrabRequest(workspace.uid, uid: item.uid)
      }
    }
  }
  
  func relinquishItem(_ item: MZItem) {
    if workspace != nil {
      if item is MZLiveStream {
        communicator.requestor.requestLiveStreamRelinquishRequest(workspace.uid, uid: item.uid)
      } else if item is MZPortfolioItem {
        communicator.requestor.requestPortfolioItemRelinquishRequest(workspace.uid, uid: item.uid)
      }
    }
  }
  
  func insertItem(_ item: MZItem, index: Int) {
    if workspace != nil && item is MZLiveStream {
      communicator.requestor.requestPortfolioItemInsertRequest(workspace.uid, contentSource: item.uid, index: index)
    }
  }
  
  func reorderItem(_ item: MZItem, index: Int) {
    if workspace != nil && item is MZPortfolioItem {
      communicator.requestor.requestPortfolioItemReorderRequest(workspace.uid, uid: item.uid, index: index)
    }
  }
  
  
  // MARK: Presenter helpers
  
  func hasLiveStreamsSection() -> Bool {
    return entity.hasLiveStreamsSection
  }
  
  func currentWorkspace() -> MZWorkspace {
    return workspace
  }
  
  func getLiveStreamItem(_ index: Int) -> MZLiveStream {
    return entity.validLiveStreams()[index] as MZLiveStream
  }
  
  func getPresentationItem(_ index: Int) -> MZPortfolioItem {
    return presentation.slides[index] as! MZPortfolioItem
  }
  
  func getLiveStreamFromContentSource(_ contentSource: String) -> MZLiveStream? {
    if workspace != nil {
      return workspace.liveStream(withUid: contentSource)
    }
    
    return nil
  }
  
  func presentationIsActive() -> Bool {
    if workspace != nil {
      return workspace.presentation.active
    }
    
    return false
  }
  
  func numberOfPresentationSlides() -> Int {
    if workspace != nil {
      return workspace.presentation.numberOfSlides
    }
    
    return NSNotFound
  }
  
  func currentPresentationIndex() -> Int {
    return workspace.presentation.currentSlideIndex
  }
}
