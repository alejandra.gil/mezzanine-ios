//
//  HintsViewController.swift
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 29/03/2017.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import UIKit

class HintsViewController: UIViewController {
  
  @IBOutlet var hintLabel: UILabel!
  @IBOutlet var magnifierImageView: UIImageView!
  
  var currentFocus: AnyObject? {
    didSet {
      if currentFocus == nil && magnifierImageView != nil {
        self.view.alpha = 1.0
      }
      else if currentFocus is MZSurface && magnifierImageView != nil {
        self.view.alpha = 0.0
      }
      else if currentFocus is MZFeld && magnifierImageView != nil {
        self.view.alpha = 0.0
      }
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    setupLabelAttributes()
  }
  
  func setupLabelAttributes() {
    let attributes: NSDictionary = [
      NSAttributedStringKey.font:UIFont.dinLight(ofSize: 14.0),
      NSAttributedStringKey.kern:CGFloat(1.8)
    ]
    
    let hintTitle = NSAttributedString(string: "Double-tap to zoom".localizedUppercase,
                                       attributes:attributes as? [NSAttributedStringKey : AnyObject])
    
    hintLabel.attributedText = hintTitle
    hintLabel.textColor = MezzanineStyleSheet.shared().grey80Color
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
}
