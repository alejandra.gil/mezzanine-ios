//
//  SlidingPanelViewController.swift
//  Mezzanine
//
//  Created by Miguel Sanchez on 25/6/15.
//  Copyright (c) 2015 Oblong Industries. All rights reserved.
//

import UIKit

enum SlidingPanelOrigin {
  case left
  case right
}

@objc open class SlidingPanelViewController: UIViewController, UIGestureRecognizerDelegate
{
  fileprivate var animationTime: TimeInterval = 0.25

  var panelViewsArray: [PanelView] = []
  weak var delegate: SlidingPanelViewControllerDelegate?

  var slidingPanelOrigin: SlidingPanelOrigin

  init (origin: SlidingPanelOrigin)
  {
    slidingPanelOrigin = origin
    super.init(nibName: nil, bundle: nil)
  }

  required public init(coder aDecoder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
  }

  open override func viewDidLoad()
  {
    super.viewDidLoad()

    self.view.backgroundColor = UIColor(white: 0.0, alpha: 0.0)

    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(SlidingPanelViewController.viewTapped(_:)))
    tapGesture.delegate = self
    self.view.addGestureRecognizer(tapGesture)

    let panGesture = UIPanGestureRecognizer(target: self, action: #selector(SlidingPanelViewController.viewPanned(_:)))
    panGesture.delegate = self
    self.view.addGestureRecognizer(panGesture)
  }

  @objc func viewTapped(_ recognizer: UITapGestureRecognizer)
  {
    hideSlidingPanel()
  }


  var panGestureStartX: CGFloat = 0
  var panGestureDeltaX: CGFloat = 0
  var panGestureStartConstraintConstant: CGFloat = 0

  @objc func viewPanned(_ recognizer: UIPanGestureRecognizer)
  {
    let location = recognizer.location(in: recognizer.view)
    let panelView = panelViewsArray.first!

    switch recognizer.state {
    case .began:
      panGestureStartX = location.x
      panGestureStartConstraintConstant = panelView.spaceToPreviousPanelConstraint.constant

    case .changed:
      panGestureDeltaX = location.x - panGestureStartX

      if slidingPanelOrigin == .right && panGestureDeltaX < 0 {
        panGestureDeltaX = 0
      }
      else if slidingPanelOrigin == .left && panGestureDeltaX > 0 {
        panGestureDeltaX = 0
      }
      
      let state = (slidingPanelOrigin == .left) ? (panGestureDeltaX / panelView.frame.size.width) : (1.0 - panGestureDeltaX / panelView.frame.size.width)
      self.view.backgroundColor = backgroundColorForPresentationState(state)
      
      panelView.spaceToPreviousPanelConstraint.constant = panGestureStartConstraintConstant + panGestureDeltaX
      self.view.setNeedsUpdateConstraints()

    case .ended:
      let velocity = recognizer.velocity(in: recognizer.view)
      let minimumVelocity:CGFloat = 20;

      if abs(velocity.x) > minimumVelocity {
        if isPresentDirection(velocity.x) {
          presentSlidingPanel()
        }
        else {
          hideSlidingPanel()
        }
      }
      else {
        let halfWidth = calculateTotalPanelsWidth() / 2.0
        if isPresentDirection(panGestureDeltaX - halfWidth) {
          presentSlidingPanel()
        }
        else {
          hideSlidingPanel()
        }
      }

    default:
      break
    }
  }

  // Returns whether the x direction specified is in the direction of the panel being presented
  func isPresentDirection(_ x: CGFloat) -> Bool {
    if slidingPanelOrigin == .right {
      // Panning towards the right = hide
      return x < 0
    }
    else if slidingPanelOrigin == .left {
      // Panning towards the left = show
      return x > 0
    }
    return false;
  }

  // state is between 0 (panel hidden) and 1 (panel presented)
  func backgroundColorForPresentationState(_ state: CGFloat) -> UIColor {
    let boundedState = min(max(state, 0.0), 1.0)
    return UIColor(white: 0.0, alpha: (0.5 * boundedState))
  }

  // MARK - Panel Presentation methods

  func presentSlidingPanel()
  {
    let panelView = panelViewsArray.first!
    panelView.spaceToPreviousPanelConstraint.constant = slidingPanelOrigin == .right ? -calculateTotalPanelsWidth() : calculateTotalPanelsWidth()
    self.view.setNeedsUpdateConstraints()

    panelView.controller?.viewWillAppear(true)
    UIView.animate(withDuration: animationTime, delay: 0, options: UIViewAnimationOptions(), animations: {
      self.view.backgroundColor = self.backgroundColorForPresentationState(1.0)
      self.view.layoutIfNeeded()
    }, completion: nil)
  }

  func hideSlidingPanel()
  {
    let panelView = panelViewsArray.first!
    panelView.spaceToPreviousPanelConstraint.constant = slidingPanelOrigin == .right ? calculateTotalPanelsWidth() : -calculateTotalPanelsWidth()
    self.view.setNeedsUpdateConstraints()

    panelView.controller?.viewWillDisappear(true)
    delegate?.slidingPanelViewControllerWillDismiss?(self)
    UIView.animate(withDuration: animationTime, delay: 0, options: UIViewAnimationOptions(), animations: {
      self.view.backgroundColor = self.backgroundColorForPresentationState(0.0)
      self.view.layoutIfNeeded()
      }, completion: {(Bool) -> Void in
        self.delegate?.slidingPanelViewControllerDidDismiss?(self)
    })
  }

  func hideLastPanel()
  {
    UIView.animate(withDuration: animationTime, delay: 0, options: UIViewAnimationOptions(), animations: {
      for panelView in self.panelViewsArray {
        panelView.spaceToPreviousPanelConstraint.constant += self.slidingPanelOrigin == .right ? panelView.frame.width : -panelView.frame.width
      }
      self.view.setNeedsUpdateConstraints()
      self.view.layoutIfNeeded()
      }, completion: {(Bool) -> Void in
        self.removePanel(self.panelViewsArray.last!)
    })
  }

  func isViewControllerVisible(_ viewController: UIViewController?) -> Bool
  {
    if viewController == nil {
      return false
    }

    for panelView in self.panelViewsArray {
      if panelView.controller == viewController {
        return true
      }
    }

    return false
  }


  // MARK - Panel Management

  func addNewPanel(_ viewController: UIViewController)
  {
    let panelView = PanelView(frame: CGRect.zero,sideType: slidingPanelOrigin)
    panelViewsArray.append(panelView)

    panelView.controller = viewController

    panelView.translatesAutoresizingMaskIntoConstraints = false
    panelView.backgroundColor = UIColor.red

    self.addChildViewController(viewController)
    self.view.addSubview(panelView)
    viewController.didMove(toParentViewController: self)
    panelView.panelSize = viewController.preferredContentSize

    panelView.widthPanelConstraint = NSLayoutConstraint(item: panelView,
      attribute: .width,
      relatedBy: .equal,
      toItem: nil,
      attribute: .notAnAttribute,
      multiplier: 1.0,
      constant: panelView.panelSize.width)
    self.view.addConstraint(panelView.widthPanelConstraint);

    panelView.spaceToPreviousPanelConstraint = NSLayoutConstraint(item: panelView,
      attribute: slidingPanelOrigin == .right ? .leading : .trailing,
      relatedBy: .equal,
      toItem: getPreviousView(panelView),
      attribute: slidingPanelOrigin == .right ? .trailing : .leading,
      multiplier: 1.0,
      constant: 0.0)
    self.view.addConstraint(panelView.spaceToPreviousPanelConstraint);

    self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[panelView]-0-|", options:NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["panelView":panelView]))

    panelView.controller = viewController

    self.view.layoutIfNeeded()
  }

  func removePanel(_ panelView: PanelView)
  {
    panelView.removeFromSuperview()
    panelView.controller?.removeFromParentViewController()
    panelViewsArray.remove(at: panelViewsArray.index(of: panelView)!)
    self.view.layoutIfNeeded()
  }

  func replaceLastPanel(_ viewController: UIViewController)
  {
    panelViewsArray.last!.controller = viewController
    self.view.layoutIfNeeded()
  }


  // MARK - Helper Methods

  func calculateTotalPanelsWidth() -> CGFloat
  {
    var totalPanelsWidth:CGFloat = 0
    for panelView in panelViewsArray
    {
      totalPanelsWidth += panelView.frame.width
    }
    return totalPanelsWidth
  }

  func getPreviousView(_ aView: UIView) -> UIView
  {
    if aView == panelViewsArray.first
    {
      return self.view
    }
    else
    {
      let ix = panelViewsArray.index(of: aView as! PanelView)
      return panelViewsArray[ix! - 1] as UIView
    }
  }


  // MARK - UIGestureRecognizerDelegate

  open func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool
  {
    for view in panelViewsArray {
      let point = gestureRecognizer.location(in: self.view)
      if view.frame.contains(point) {
        return false
      }
    }
    return true
  }
}

@objc protocol SlidingPanelViewControllerDelegate : NSObjectProtocol
{
  @objc optional func slidingPanelViewControllerWillDismiss(_ slidingPanel: SlidingPanelViewController)
  @objc optional func slidingPanelViewControllerDidDismiss(_ slidingPanel: SlidingPanelViewController)
}

