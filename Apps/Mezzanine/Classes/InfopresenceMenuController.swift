//
//  InfopresenceMenuController.swift
//  Mezzanine
//
//  Created by Miguel Sanchez on 1/7/15.
//  Copyright (c) 2015 Oblong Industries. All rights reserved.
//

import UIKit

@objc class InfopresenceMenuController: NSObject, SlidingPanelViewControllerDelegate
{
  @objc var slidingPanelViewController: SlidingPanelViewController?
  @objc var menuViewController: InfopresenceMenuViewController?
  @objc var inviteMezzaninesViewController: InfopresenceInviteViewController?
  @objc var inviteParticipantsViewController: InfopresenceInviteParticipantsViewController?
  @objc var joinViewController: InfopresenceJoinViewController?

  @objc weak var containerViewController: UIViewController?

  @objc var communicator: MZCommunicator?

  var submenuPresented: Bool = false

  @objc weak var delegate: InfopresenceMenuControllerDelegate?

  convenience init(containerViewController controller: UIViewController) {

    self.init()
    containerViewController = controller
    slidingPanelViewController = SlidingPanelViewController(origin: .right)
    slidingPanelViewController!.delegate = self
    slidingPanelViewController!.view.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    slidingPanelViewController!.view.frame = controller.view.frame
    controller.view.addSubview(slidingPanelViewController!.view)
    controller.addChildViewController(slidingPanelViewController!)
    slidingPanelViewController?.didMove(toParentViewController: controller)
  }

  deinit {

    print("InfopresenceMenuController is being deinitialized")
  }

  // MARK - System Model Observation
  fileprivate var kSystemModelContext = 0
  var systemModel: MZSystemModel? {

    willSet(aNewModel) {
      if self.systemModel != aNewModel {
        endObservingSystemModel()
      }
    }
    didSet {
      if self.systemModel != nil {
        beginObservingSystemModel()
      }
    }
  }

  func beginObservingSystemModel() {

    self.systemModel?.addObserver(self, forKeyPath: "infopresence.status", options: [.new, .old], context: &kSystemModelContext)
  }

  func endObservingSystemModel() {

    self.systemModel?.removeObserver(self, forKeyPath: "infopresence.status")
  }

  override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {

    if context == &kSystemModelContext {

      if keyPath == "infopresence.status" {

        let oldInfopresenceStatus = MZInfopresenceStatus(rawValue: change?[NSKeyValueChangeKey.oldKey] as! UInt)
        if oldInfopresenceStatus != MZInfopresenceStatus.active && systemModel?.infopresence.status == MZInfopresenceStatus.active && // Infopresence transitions from any status to active
           (containerViewController?.navigationController?.topViewController == joinViewController || // compact width is has pushed the view controller
           slidingPanelViewController?.isViewControllerVisible(joinViewController) == true) { // or regular width has presented a submenu as a panel
          hideSubmenu()
        }
      }
    }
    else {

      super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
    }
  }

  // MARK - Menu Management
  func showMenuViewController() {
    guard let systemModel = systemModel else { return }
    guard let communicator = communicator else { return }

    if menuViewController == nil {

      menuViewController = InfopresenceMenuViewController(systemModel: systemModel, communicator: communicator, menuController: self)
    }

    presentViewControllerUsingSlidingPanel(menuViewController!)
  }


  func showInviteMezzaninesMenu() {

    if inviteMezzaninesViewController == nil {

      inviteMezzaninesViewController = InfopresenceInviteViewController(systemModel: systemModel!, communicator: communicator!, infopresenceMenuController:self)
    }

    presentSubmenuWithViewController(inviteMezzaninesViewController!)
  }


  func showInviteParticipantsMenu() {

    if inviteParticipantsViewController == nil {

      inviteParticipantsViewController = InfopresenceInviteParticipantsViewController(systemModel: systemModel!, mailComposer: MezzanineAppContext.current().mailComposer, infopresenceMenuController:self)
    }

    presentSubmenuWithViewController(inviteParticipantsViewController!)
  }


  func showJoinAnotherMezzanineMenu() {

    if joinViewController == nil {

      joinViewController = InfopresenceJoinViewController(systemModel: systemModel!, communicator: communicator!, infopresenceMenuController:self)
    }

    presentSubmenuWithViewController(joinViewController!)
  }


  func presentSubmenuWithViewController(_ controller: UIViewController) {

    if containerViewController!.traitCollection.isRegularWidth() == true && containerViewController!.traitCollection.isRegularHeight() == true {

      if submenuPresented {

        slidingPanelViewController?.replaceLastPanel(controller)
      }
      else {

        presentViewControllerUsingSlidingPanel(controller)
        submenuPresented = true
      }
    }
    else {

      presentViewControllerUsingNavigationController(controller)
    }
  }

  func presentViewControllerUsingSlidingPanel(_ controller: UIViewController) {
    delegate?.infopresenceMenuControllerWillPresentSlidingPanelWithController?(controller)
    slidingPanelViewController?.addNewPanel(controller)
    slidingPanelViewController?.presentSlidingPanel()
  }

  func presentViewControllerUsingNavigationController(_ controller: UIViewController) {
    delegate?.infopresenceMenuControllerWillPushViewController?(controller)
    containerViewController?.navigationController?.pushViewController(controller, animated: true)
  }

  func popViewControllerUsingNavigationController() {
    delegate?.infopresenceMenuControllerWillPopViewController?()
    containerViewController?.navigationController?.popViewController(animated: true)
  }

  func hideInfopresenceMenu() {

    if containerViewController?.navigationController?.topViewController == inviteParticipantsViewController ||
       containerViewController?.navigationController?.topViewController == inviteMezzaninesViewController ||
       containerViewController?.navigationController?.topViewController == joinViewController {

      popViewControllerUsingNavigationController()
    }
    slidingPanelViewController?.hideSlidingPanel()
  }

  func hideSubmenu() {

    if containerViewController!.traitCollection.isRegularWidth() == true && containerViewController!.traitCollection.isRegularHeight() == true {

      slidingPanelViewController?.hideLastPanel()
    }
    else {

      popViewControllerUsingNavigationController()
    }

    submenuPresented = false
  }

  // MARK - SlidingPanelViewControllerDelegate
  func slidingPanelViewControllerWillDismiss(_ slidingPanel: SlidingPanelViewController) {
    delegate?.infopresenceMenuControllerWillDismiss?(self)
  }
  
  func slidingPanelViewControllerDidDismiss(_ slidingPanel: SlidingPanelViewController) {

    joinViewController = nil
    inviteParticipantsViewController = nil
    inviteMezzaninesViewController = nil
    menuViewController = nil

    slidingPanel.willMove(toParentViewController: nil)
    slidingPanel.view.removeFromSuperview()
    slidingPanel.removeFromParentViewController()

    delegate?.infopresenceMenuControllerDidDismiss?(self)
  }
}

@objc protocol InfopresenceMenuControllerDelegate : NSObjectProtocol {
  @objc optional func infopresenceMenuControllerWillPresentSlidingPanelWithController(_ controller: UIViewController)
  @objc optional func infopresenceMenuControllerWillPushViewController(_ controller: UIViewController)
  @objc optional func infopresenceMenuControllerWillPopViewController()
  @objc optional func infopresenceMenuControllerWillDismiss(_ infopresenceMenuController: InfopresenceMenuController)
  @objc optional func infopresenceMenuControllerDidDismiss(_ infopresenceMenuController: InfopresenceMenuController)
}

