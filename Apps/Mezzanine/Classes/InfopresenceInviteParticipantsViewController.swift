//
//  InfopresenceInviteParticipantsViewController.swift
//  Mezzanine
//
//  Created by Miguel Sánchez on 3/7/15.
//  Copyright (c) 2015 Oblong Industries. All rights reserved.
//

import UIKit

class InfopresenceInviteParticipantsViewController: UIViewController, UINavigationBarDelegate
{
  @IBOutlet weak var navigationBar: UINavigationBar!

  @IBOutlet var participantsCanJoinView: UIView!
  @IBOutlet var participantsCanJoinLabel: UILabel!
  @IBOutlet var participantsCanJoinSwitch: UISwitch!
  @IBOutlet var descriptionParticipationOff: UILabel!
  @IBOutlet var serverURLLabel: UILabel!

  @IBOutlet var invitationOptionsView: UIView!
  @IBOutlet var emailURLButton: UIButton!
  @IBOutlet var copyURLButton: UIButton!

  unowned var infopresenceMenuController: InfopresenceMenuController
  fileprivate var kInfopresenceModelContext = 0
  var infopresenceModel: MZInfopresence!
  var systemModel: MZSystemModel!
  var mailComposer: MezzanineMailComposerController!

  func beginObservingInfopresenceModel() {

    infopresenceModel.addObserver(self, forKeyPath: "remoteAccessStatus", options: [.new, .old], context: &kInfopresenceModelContext)
  }

  func endObservingInfopresenceModel() {

    infopresenceModel.removeObserver(self, forKeyPath: "remoteAccessStatus")
  }


  init(systemModel: MZSystemModel, mailComposer: MezzanineMailComposerController, infopresenceMenuController: InfopresenceMenuController) {
    self.systemModel = systemModel
    self.infopresenceModel = self.systemModel.infopresence
    self.infopresenceMenuController = infopresenceMenuController
    self.mailComposer = mailComposer
    super.init(nibName: "InfopresenceInviteParticipantsViewController", bundle: nil)
    beginObservingInfopresenceModel()
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  deinit {
    endObservingInfopresenceModel()
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    
    let isRegular = infopresenceMenuController.containerViewController?.traitCollection.isRegularWidth() == true && infopresenceMenuController.containerViewController?.traitCollection.isRegularHeight() == true
    preferredContentSize = CGSize(width: isRegular ? 320.0 : 300.0, height: CGFloat.greatestFiniteMagnitude);
    
    updateViewStateAnimated(false)

    let styleSheet = MezzanineStyleSheet.shared()
    self.view.backgroundColor = styleSheet?.grey30Color

    navigationBar.delegate = self
    navigationBar.backgroundColor = styleSheet?.grey50Color

    let navBarLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 32))
    navBarLabel.text = "Infopresence Invite Perticipants Panel Header".localized
    navBarLabel.textAlignment = .center
    styleSheet?.stylizeHeaderTitleLabel(navBarLabel)

    // The navigation item has been added at the nib file
    let item = navigationBar.items!.first!
    item.titleView = navBarLabel

    if !isRegular
    {
      let backButton = UIButton(type: .custom)
      backButton.frame = CGRect(x: 0, y: 0, width: 32, height: 22)
      backButton.setImage(UIImage(named: "Arrow"), for: .normal)
      backButton.imageEdgeInsets = UIEdgeInsetsMake(8, 0, 5, 20)
      backButton.addTarget(self, action: #selector(goBack), for: .touchUpInside)
      let barButtonItem = UIBarButtonItem(customView: backButton)
      item.leftBarButtonItem = barButtonItem
    }

    participantsCanJoinView.backgroundColor = styleSheet?.grey40Color;
    styleSheet?.stylize(withTopAndBottomBorders: participantsCanJoinView)

    stylizeButton(emailURLButton)
    stylizeButton(copyURLButton)

    serverURLLabel.font = UIFont.dinBold(ofSize: 16)
    serverURLLabel.textColor = UIColor.white

    descriptionParticipationOff.font = UIFont.din(ofSize: 13)
    descriptionParticipationOff.textColor = styleSheet?.grey110Color

    initAccessibility()
  }
  
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    
    // Analytics
    let analyticsManager = MezzanineAnalyticsManager.sharedInstance
    analyticsManager.tagScreen(analyticsManager.infopresenceInviteParticipantsScreen)
  }
  
  override var prefersStatusBarHidden : Bool {
    return false
  }
  
  override var preferredStatusBarUpdateAnimation : UIStatusBarAnimation {
    return .fade
  }
  
  override var preferredStatusBarStyle : UIStatusBarStyle {
    return .lightContent
  }

  func stylizeButton(_ button: UIButton) {
    button.tintColor = UIColor.darkGray
    button.adjustsImageWhenHighlighted = true
    button.setTitleColor(UIColor.white, for: UIControlState())
    button.setTitleColor(UIColor.lightGray, for: .highlighted)
    button.setTitleColor(UIColor.lightGray, for: .selected)
    
    button.titleLabel!.font = UIFont.dinMedium(ofSize: 13.0)
    button.backgroundColor = MezzanineStyleSheet.shared().grey90Color
    button.layer.cornerRadius = 4.0
  }

  // MARK: View Initializer

  fileprivate func initAccessibility() {
    participantsCanJoinLabel.accessibilityIdentifier = "InviteOusideParticipantsMenu.EnableRemoteParticipationLabel"
    participantsCanJoinSwitch.accessibilityIdentifier = "InviteOusideParticipantsMenu.EnableRemoteParticipationSwitch"
    descriptionParticipationOff.accessibilityIdentifier = "InviteOusideParticipantsMenu.DescriptionCurrentStateLabel"
    serverURLLabel.accessibilityIdentifier = "InviteOusideParticipantsMenu.RemoteParticipationURLLabel"
    emailURLButton.accessibilityIdentifier = "InviteOusideParticipantsMenu.EmailURLButton"
    copyURLButton.accessibilityIdentifier = "InviteOusideParticipantsMenu.CopyURLButton"

    // The navigation item has been added at the nib file
    navigationBar.items!.first!.leftBarButtonItem?.accessibilityIdentifier = "InviteOusideParticipantsMenu.BackButton"
  }

  // MARK: Observation

  override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?)
  {
    if context == &kInfopresenceModelContext
    {
      let oldValue = change![NSKeyValueChangeKey.oldKey]
      let newValue = change![NSKeyValueChangeKey.newKey]
      
      if (oldValue as AnyObject?) === (newValue as AnyObject?) {
        return
      }
      
      updateViewStateAnimated(true)
    }
    else
    {
      super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
    }
  }
  
  func updateViewStateAnimated(_ animated: Bool)
  {
    if animated == false {
      switch self.infopresenceModel.remoteAccessStatus {
      case .active:
        serverURLLabel.text = infopresenceModel.remoteAccessUrl
        descriptionParticipationOff.text = "Outside participation allows participants outside of Mezzanine rooms to join the session using an invitation link."
      case .inactive:
        serverURLLabel.text = ""
        descriptionParticipationOff.text = ""
      case .starting:
        descriptionParticipationOff.text = "Starting outside participation..."
      }
      animateViewUpdates()
    } else {
      if self.infopresenceModel.remoteAccessStatus == .active {
        serverURLLabel.text = infopresenceModel.remoteAccessUrl
        descriptionParticipationOff.text = "Outside participation allows participants outside of Mezzanine rooms to join the session using an invitation link."
      }
      
      if self.infopresenceModel.remoteAccessStatus == .starting {
        descriptionParticipationOff.text = "Starting outside participation..."
      }
      
      UIView.animate(withDuration: 0.3, animations: {
        self.animateViewUpdates()
        }, completion: { (Bool) in
          if self.infopresenceModel.remoteAccessStatus == .inactive {
            self.serverURLLabel.text = ""
            self.descriptionParticipationOff.text = ""
          }
      })
    }
  }
  
  func animateViewUpdates()
  {
    switch self.infopresenceModel.remoteAccessStatus {
    case .active:
      participantsCanJoinSwitch.setOn(true, animated: true)
      descriptionParticipationOff.alpha = 1.0
      invitationOptionsView.alpha = 1.0
      emailURLButton.alpha = 1.0
      copyURLButton.alpha = 1.0
      serverURLLabel.alpha = 1.0
    case .inactive:
      participantsCanJoinSwitch.setOn(false, animated: true)
      descriptionParticipationOff.alpha = 0.0
      invitationOptionsView.alpha = 0.0
      emailURLButton.alpha = 0.0
      copyURLButton.alpha = 0.0
      serverURLLabel.alpha = 0.0
    case .starting:
      participantsCanJoinSwitch.setOn(true, animated: true)
      descriptionParticipationOff.alpha = 1.0
      invitationOptionsView.alpha = 1.0
      emailURLButton.alpha = 0.0
      copyURLButton.alpha = 0.0
      serverURLLabel.alpha = 0.0
    }
  }

  // MARK: Actions

  @IBAction func switchRemoteParticipationState(_ sender: AnyObject)
  {
    let communicator = MezzanineAppContext.current().currentCommunicator
    
    let toggleSwitch = sender as! UISwitch
    let transaction = toggleSwitch.isOn ? communicator?.requestor.requestInfopresenceEnableRemoteAccessRequest() : communicator?.requestor.requestInfopresenceDisableRemoteAccessRequest()

    transaction?.failedBlock = { error in
      self.updateViewStateAnimated(true)
    }
    
    let analyticsManager = MezzanineAnalyticsManager.sharedInstance
    analyticsManager.tagEvent(analyticsManager.mezzInToggleEvent, attributes: [analyticsManager.actionKey : toggleSwitch.isOn ? analyticsManager.startAttribute : analyticsManager.stopAttribute])
  }

  @IBAction func goBack() {
    infopresenceMenuController.popViewControllerUsingNavigationController()
  }

  @IBAction func copyInviteLinkToClipboard(_ sender: AnyObject)
  {
    if infopresenceModel.remoteAccessUrl != nil {
      UIPasteboard.general.string = infopresenceModel.remoteAccessUrl
    }
    
    let analyticsManager = MezzanineAnalyticsManager.sharedInstance
    analyticsManager.tagEvent(analyticsManager.mezzInShareLinkEvent, attributes: [analyticsManager.actionKey :  analyticsManager.copyAttribute])
  }

  @IBAction func sendInviteLinkViaEmail(_ sender: AnyObject)
  {
    if mailComposer.canSendMail() == false {
      let alertController = UIAlertController(title: "Infopresence Invite Error Dialog Title".localized, message: "Infopresence Invite Error Dialog Message".localized, preferredStyle: .alert)
      alertController.addAction(UIAlertAction(title: "Generic Title OK".localized, style: .default, handler: nil))
      self.present(alertController, animated: true, completion:nil)
      return
    }

    let path = Bundle.main.path(forResource: "RPInvitationTemplate", ofType: nil)
    if path == nil {
      showErrorAlertLoadingTemplate()
      return
    }

    var htmlTemplate = String()
    do {
      htmlTemplate = try NSString(contentsOfFile: path!, encoding: String.Encoding.utf8.rawValue) as String
    }
    catch _ {
      showErrorAlertLoadingTemplate()
    }

    htmlTemplate = buildHtmlInviteWithTemplate(htmlTemplate)
    mailComposer.presentMailComposer(buildMailSubject(), message: htmlTemplate, isHTML: true)

    let analyticsManager = MezzanineAnalyticsManager.sharedInstance
    analyticsManager.tagEvent(analyticsManager.mezzInShareLinkEvent, attributes: [analyticsManager.actionKey :  analyticsManager.emailAttribute])
  }


  // MARK: - Invite Mail Helpers

  func showErrorAlertLoadingTemplate() {
    let alertController = UIAlertController(title: "Invite RP Participants Error Title".localized, message: "Invite RP Participants Error Message".localized, preferredStyle: .alert)
    alertController.addAction(UIAlertAction(title: "Generic Title OK".localized, style: .default, handler: nil))
    self.present(alertController, animated: true, completion:nil)
  }
  
  func buildMailSubject() -> String {
    var companyLocationStr = companyLocationString()
    if companyLocationStr == "" {
        companyLocationStr = systemModel.myMezzanine.name
    }
    return String(format: "Infopresence Invite Email Subject Text".localized, companyLocationStr)
  }

  func buildHtmlInviteWithTemplate(_ htmlTemplate: String) -> String {
    var htmlTemplate = htmlTemplate.replacingOccurrences(of: "[subject]", with: buildMailSubject())
    htmlTemplate = htmlTemplate.replacingOccurrences(of: "[RP_link]", with: infopresenceModel.remoteAccessUrl)
    htmlTemplate = htmlTemplate.replacingOccurrences(of: "[company], [location]", with: companyLocationString())
    htmlTemplate = htmlTemplate.replacingOccurrences(of: "[mezzName]", with: systemModel.myMezzanine.name)
    return htmlTemplate
  }

  func companyLocationString() -> String {
    var companyLocationStr = ""
    if (systemModel.myMezzanine.company != nil) {
      if (systemModel.myMezzanine.location != nil) {
        companyLocationStr = systemModel.myMezzanine.company + ", " + systemModel.myMezzanine.location
      } else {
        companyLocationStr = systemModel.myMezzanine.company
      }
    }
    else if (systemModel.myMezzanine.location != nil) {
      companyLocationStr = systemModel.myMezzanine.location
    }
    return companyLocationStr
  }

  
  // MARK: Adaptative layout

  override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
    for (_, viewController) in (navigationController?.viewControllers.enumerated())! {
      if viewController == self {
        navigationController?.popViewController(animated: false)
        break
      }
    }
  }


  // MARK: Navigation bar delegate

  func position(for bar: UIBarPositioning) -> UIBarPosition {
    return .topAttached
  }
}
