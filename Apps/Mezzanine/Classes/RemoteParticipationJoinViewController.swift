//
//  RemoteParticipationJoinViewController.swift
//  Mezzanine
//
//  Created by miguel on 30/6/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

import UIKit


// MARK: - RemoteParticipationJoinViewControllerDelegate
@objc protocol RemoteParticipationJoinViewControllerDelegate {
  // Connection
  func remoteParticipationJoinViewControllerShouldConnect(_ rpVC: RemoteParticipationJoinViewController, name: String, audioMuted: Bool, videoMuted: Bool, audioOnly: Bool)
  func remoteParticipationJoinViewControllerShouldCancelConnection(_ rpVC: RemoteParticipationJoinViewController)
}

// MARK: -
// MARK: - RemoteParticipationJoinViewController

@objc class RemoteParticipationJoinViewController: UIViewController, UITextFieldDelegate {

  @objc weak var delegate:RemoteParticipationJoinViewControllerDelegate?

  @IBOutlet var welcomeLabel: UILabel!
  @IBOutlet var contentView: UIView!
  @IBOutlet var usernameTextField: UITextField!
  @IBOutlet var muteControlContainerView: UIView!
  @IBOutlet var muteAudioButton: MuteButton!
  @IBOutlet var muteVideoButton: MuteButton!
  @IBOutlet var enterSessionButton: UIButton!
  @IBOutlet var cancelButton: UIButton!
  @IBOutlet var audioOnlySwitchContainerView: UIView!
  @IBOutlet var audioOnlySwitch: UISwitch!
  @IBOutlet var audioOnlyLabel: UILabel!
  @IBOutlet var audioOnlyHintLabel: UILabel!
  @IBOutlet var oblongLogoImage: UIImageView!
  @IBOutlet var appVersionLabel: UILabel!
  @IBOutlet var contentViewCenterYConstraint: NSLayoutConstraint!
  @IBOutlet var usernameTextFieldBottomConstraint: NSLayoutConstraint!
  @IBOutlet var muteControlContainerViewTopConstraint: NSLayoutConstraint!
  @IBOutlet var usernameTextFieldBottomToMuteControlContainerTopConstraint: NSLayoutConstraint!
  @IBOutlet var muteControlContainerViewWidthConstraint: NSLayoutConstraint!
  @IBOutlet var oblongLogoImageTopConstraint: NSLayoutConstraint!
  @IBOutlet var audioOnlySwitchContainerViewHeightConstraint: NSLayoutConstraint!

  var audioOnlyMode: Bool = false
  fileprivate var observationTokens = Array<AnyObject>()
  fileprivate var communicatorObservers = Array <String> ()
  internal var isThisViewLoaded: Bool = false
  
  fileprivate var rightTextFieldAccessory: UIView!

  @objc var communicator: MZCommunicator! {
    willSet {
      if (communicator != nil) {
        endObservingCommunicator()
      }
    }
    didSet {
      if (communicator != nil) {
        beginObservingCommunicator()
      }
    }
  }


  // MARK: - View life cycle
  override func viewDidLoad() {
    super.viewDidLoad()
    
    let sharedStyle = MezzanineStyleSheet.shared()

    self.view.backgroundColor = sharedStyle?.grey50Color
    audioOnlySwitchContainerView.backgroundColor = sharedStyle?.grey40Color
    
    // Accessories to keep always centered the text
    // Empiric value which matches the clearButton frame (and perhaps some insets around)
    rightTextFieldAccessory = UIView(frame: CGRect(x: 0, y: 0, width: 27.0, height: 27.0))
    
    usernameTextField.font = UIFont.din(ofSize: 16.0)
    usernameTextField.enablesReturnKeyAutomatically = true
    usernameTextField.clearButtonMode = .whileEditing
    usernameTextField.text = UserDefaults.standard.displayName
    usernameTextField.placeholder = "Remote Participation Welcome Screen Name Textfield Hint".localized
    usernameTextField.leftViewMode = .always
    usernameTextField.rightViewMode = .always
    usernameTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 27.0, height: 27.0))
    usernameTextField.rightView = rightTextFieldAccessory

    welcomeLabel.font = UIFont.dinLight(ofSize: 24)
    welcomeLabel.textColor = sharedStyle?.textNormalColor
    welcomeLabel.text = "Remote Participation Welcome Screen Title".localized

    audioOnlyLabel.font = UIFont.dinMedium(ofSize: 18)
    audioOnlyLabel.textColor = sharedStyle?.textNormalColor
    audioOnlyLabel.text = "Remote Participation Welcome Screen Audio Only Title".localized

    audioOnlyHintLabel.font = UIFont.din(ofSize: 14)
    audioOnlyHintLabel.textColor = sharedStyle?.grey110Color
    audioOnlyHintLabel.text = "Remote Participation Welcome Screen Audio Only Hint".localized

    audioOnlySwitch.setOn(true, animated: false)
    audioOnlyMode = !isOnWiFi()
    updateAudioOnlyModeControlsVisibility(animated: false)

    muteAudioButton.type = .audio
    muteVideoButton.type = .video
    
    sharedStyle?.stylizeCall(toActionButton: enterSessionButton)
    enterSessionButton.layer.cornerRadius = 3.0;
    enterSessionButton.clipsToBounds = true;
    enterSessionButton.setTitle("Remote Participation Welcome Screen Enter Session Button".localized, for: UIControlState())

    cancelButton.titleLabel?.font = UIFont.dinMedium(ofSize: 14)
    cancelButton.setTitle("Remote Participation Welcome Screen Cancel".localized, for: UIControlState())

    oblongLogoImage.image = UIImage(named: "oblong-logo-white-128.png")?.withRenderingMode(.alwaysTemplate)
    oblongLogoImage.tintColor = sharedStyle?.grey90Color
    if UIDevice.current.isIPhone4SOrNewer() && !UIDevice.current.isIPhone5OrNewer() {
      oblongLogoImage.isHidden = true
    }

    appVersionLabel.textColor = sharedStyle?.grey110Color
    appVersionLabel.font = UIFont.din(ofSize: 12)
    appVersionLabel.text = Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) as? String

    enableHideKeyboardOnTapOutsideTextFields()

    initAccessibility()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    if traitCollection.isRegularWidth() {
      audioOnlySwitchContainerView.layer.cornerRadius = 6;
      audioOnlyHintLabel.font = UIFont.dinMedium(ofSize: 12)
    }
    else {
      oblongLogoImageTopConstraint.constant = calculateOblongLogoTopConstraintValue()
    }

    registerObservers()
    updateConnectButton()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    isThisViewLoaded = true

    // Analytics
    let analyticsManager = MezzanineAnalyticsManager.sharedInstance
    analyticsManager.tagScreen(analyticsManager.mezzInJoinViewScreen)
    
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    isThisViewLoaded = false

    unregisterObservers()
  }

  override func viewWillLayoutSubviews() {
    super.viewWillLayoutSubviews()
    updateAudioOnlySwitchVisiblity(animated: isThisViewLoaded)
  }
  
  override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
    if self.traitCollection.isRegularHeight() && self.traitCollection.isRegularWidth() {
      UIView.animate(withDuration: 0.3, delay: 0.3, options: .beginFromCurrentState, animations: {
        self.unregisterObservers()
        self.updateWithKeyboardVisibility(self.usernameTextField.isFirstResponder)
      }) { (Bool) in
        self.registerObservers()
      }
    }
  }

  override var shouldAutorotate : Bool {
    return false
  }

  override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
    return self.traitCollection.isiPad() ? .all : .portrait
  }
  
  override var prefersStatusBarHidden : Bool {
    return false
  }
  
  override var preferredStatusBarUpdateAnimation : UIStatusBarAnimation {
    return .fade
  }
  
  override var preferredStatusBarStyle : UIStatusBarStyle {
    return .lightContent
  }

  // MARK: View Initializer

  fileprivate func initAccessibility() {
    welcomeLabel.accessibilityIdentifier = "MezzInWelcomeScreen.WelcomeLabel"
    usernameTextField.accessibilityIdentifier = "MezzInWelcomeScreen.WelcomeLabel"
    muteAudioButton.accessibilityIdentifier = "MezzInWelcomeScreen.MuteAudioButton"
    muteVideoButton.accessibilityIdentifier = "MezzInWelcomeScreen.MuteVideoButton"
    audioOnlyLabel.accessibilityIdentifier = "MezzInWelcomeScreen.AudioOnlyLabel"
    audioOnlySwitch.accessibilityIdentifier = "MezzInWelcomeScreen.AudioOnlySwitch"
    enterSessionButton.accessibilityIdentifier = "MezzInWelcomeScreen.EnterSessionButton"
    cancelButton.accessibilityIdentifier = "MezzInWelcomeScreen.CancelButton"
  }

  // MARK: - Observers
  
  func registerObservers() {
    observationTokens.append(NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardWillShow, object: nil, queue: OperationQueue.main, using: { [unowned self] (notification) in
      self.updateWithKeyboardVisibility(true)
      }))
    
    observationTokens.append(NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardWillHide, object: nil, queue: OperationQueue.main, using: { [unowned self] (notification) in
      self.updateWithKeyboardVisibility(false)
      }))
    
    observationTokens.append(NotificationCenter.default.addObserver(forName: NSNotification.Name.UITextFieldTextDidChange, object: usernameTextField, queue: OperationQueue.main, using: { [unowned self] (notification) in
      self.updateConnectButton()
      }))
    
    observationTokens.append(NotificationCenter.default.addObserver(forName: NSNotification.Name.UIDeviceOrientationDidChange, object: nil, queue: OperationQueue.main, using: {(notification) in
      if (UIDevice.current.isIOS10OrAbove()) {
        UIApplication.shared.statusBarOrientation = .portrait
      }
    }))
  }
  
  func unregisterObservers() {
    for observer in observationTokens {
      NotificationCenter.default.removeObserver(observer)
    }
    
    observationTokens.removeAll()
  }

  func beginObservingCommunicator() {
    communicatorObservers.append(communicator.addObserver(forKeyPath: "networkConnectionType", options: [.new, .old], on: OperationQueue.main) { ( obj, change: [AnyHashable: Any]!) in
      self.audioOnlyMode = self.isOnWiFi() ? false : true
      self.updateAudioOnlySwitchVisiblity(animated: true)
      self.updateAudioOnlyModeControlsVisibility(animated: true)
      })
  }

  func endObservingCommunicator() {
    for token in communicatorObservers {
      communicator.removeObserver(withBlockToken: token)
    }
    communicatorObservers.removeAll()
  }

  // MARK: - View states
  
  func updateConnectButton() {
    guard let usernameTextFieldText = usernameTextField.text else {
      enterSessionButton.isEnabled = false
      return
    }
    enterSessionButton.isEnabled = usernameTextFieldText.characters.count > 0
  }

  func updateWithKeyboardVisibility(_ visible: Bool) {
    var midY: CGFloat = 0.0
    let oblongLogoTopConstraintValue = calculateOblongLogoTopConstraintValue()
    var logoTop = oblongLogoTopConstraintValue
    var welcomeLabelAlpha: CGFloat = 1.0
    var oblongLogoImageAlpha: CGFloat = 1.0

    if self.traitCollection.isRegularHeight() && self.traitCollection.isRegularWidth() {
      if (UIDeviceOrientationIsLandscape(UIDevice.current.orientation)) {
        midY = visible ? -170.0 : midY
        logoTop = visible ? -oblongLogoTopConstraintValue * 2.0 : oblongLogoTopConstraintValue
        oblongLogoImageAlpha = visible ? 0.0 : 1.0
      }
    } else {
      let offset:CGFloat = UIDevice.current.isIPhone4SOrNewer() && !UIDevice.current.isIPhone5OrNewer() ? -80.0 : -90.0
      midY = visible ? offset : midY
      logoTop = visible ? -oblongLogoTopConstraintValue * 2.0 : oblongLogoTopConstraintValue
      oblongLogoImageAlpha = visible ? 0.0 : 1.0
      welcomeLabelAlpha = visible ? 0.0 : 1.0
    }
    
    contentViewCenterYConstraint.constant = midY
    oblongLogoImageTopConstraint.constant = logoTop
    UIView.animate(withDuration: 0.3, delay: 0, options: .beginFromCurrentState, animations: {
      self.welcomeLabel.alpha = welcomeLabelAlpha
      self.oblongLogoImage.alpha = oblongLogoImageAlpha
      self.view.layoutIfNeeded()
    }, completion: nil)
  }
  
  func updateAudioOnlyModeControlsVisibility(animated: Bool) {
    muteControlContainerViewWidthConstraint.constant = audioOnlyMode ? 60.0 : 135.0
    
    let blockUpdates = {
      self.muteVideoButton.alpha = self.audioOnlyMode ? 0.0 : 1.0
      self.view.setNeedsUpdateConstraints()
      self.view.layoutIfNeeded()
    }
    
    if animated {
      UIView.animate(withDuration: 0.3, animations: {
        blockUpdates()
      }) 
    } else {
      blockUpdates()
    }
  }

  func updateAudioOnlySwitchVisiblity(animated: Bool) {
    let onWiFi = isOnWiFi()
    
    let blockUpdates = {
      self.muteVideoButton.alpha = self.audioOnlyMode ? 0.0 : 1.0
      self.audioOnlySwitchContainerView.alpha = !onWiFi ? 1.0 : 0.0
      self.view.setNeedsUpdateConstraints()
      self.view.layoutIfNeeded()
    }
    
    self.audioOnlySwitchContainerViewHeightConstraint.constant = !onWiFi ? 74.0 : 0.0
    self.muteControlContainerViewTopConstraint.constant = !onWiFi ? 15.0 : 0.0
    
    if animated {
      UIView.animate(withDuration: 0.3, delay: 0, options: .beginFromCurrentState, animations: {
        blockUpdates()
      }) { (Bool) in
        self.audioOnlySwitch.setOn(self.audioOnlyMode, animated: false)
      }
    } else {
      blockUpdates()
      self.audioOnlySwitch.setOn(self.audioOnlyMode, animated: false)
    }
  }
  
  func isOnWiFi() -> Bool {
    return communicator?.networkConnectionType != .cellular
  }

  func calculateOblongLogoTopConstraintValue() -> CGFloat {
    let contentViewTop = contentView.frame.minY - contentViewCenterYConstraint.constant
    let welcomeLabelTop = welcomeLabel.frame.minY
    let oblongLogoHeight = oblongLogoImage.frame.height
    let newPosition = (contentViewTop + welcomeLabelTop - oblongLogoHeight + 20.0) / 2.0
    return min(self.traitCollection.isiPad() ? 45.0 : 38.0, newPosition)
  }
  

  // MARK: - Actions

  @IBAction func enterSession(_ sender: UIButton) {
    connect()
  }

  @IBAction func cancelConnection(_ sender: UIButton) {
    usernameTextField.resignFirstResponder()
    
    // Wait until keyboard finishes animating to avoid
    let delay = DispatchTime.now() + Double(Int64(0.3 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
    DispatchQueue.main.asyncAfter(deadline: delay)  {
      self.delegate?.remoteParticipationJoinViewControllerShouldCancelConnection(self)
    }
  }

  @IBAction func switchAudioOnly(_ sender: UISwitch) {
    audioOnlyMode = !audioOnlyMode
    updateAudioOnlyModeControlsVisibility(animated: true)
  }


  // MARK: - Connection

  func connect() {
    // Analytics
    let analyticsManager = MezzanineAnalyticsManager.sharedInstance
    let audioOnlySwitchValue = audioOnlyMode ? audioOnlySwitch.isOn : false
    analyticsManager.tagEvent(analyticsManager.audioOnlyEvent, attributes: [analyticsManager.stateKey : audioOnlySwitchValue ? analyticsManager.onAttribute : analyticsManager.offAttribute, analyticsManager.joinedKey : analyticsManager.noAttribute])
    
    usernameTextField.resignFirstResponder()
    delegate?.remoteParticipationJoinViewControllerShouldConnect(self, name: usernameTextField.text!, audioMuted: muteAudioButton.isMuted, videoMuted: muteVideoButton.isMuted, audioOnly: audioOnlyMode)
  }

  
  // MARK: - UITextFieldDelegate
  
  func textFieldDidBeginEditing(_ textField: UITextField) {
    if textField.text?.isEmpty == false {
      textField.rightView = nil
    }
  }
  
  func textFieldDidEndEditing(_ textField: UITextField) {
    textField.rightView = rightTextFieldAccessory
  }
  
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    if (string.isEmpty == true && string == "") && textField.text?.characters.count == 1 {
      textField.rightView = rightTextFieldAccessory
    } else if textField.text?.isEmpty == false {
      textField.rightView = nil
    } else {
      textField.rightView = nil
    }
    
    return true
  }
  
  func textFieldShouldClear(_ textField: UITextField) -> Bool {
    textField.rightView = rightTextFieldAccessory
    
    return true
  }
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    connect()
    
    return true
  }
}

