//
//  FPPopoverController+Mezzanine.m
//  Mezzanine
//
//  Created by Zai Chang on 6/17/13.
//  Copyright (c) 2013 Oblong Industries. All rights reserved.
//

#import "FPPopoverController+Mezzanine.h"
#import "MezzanineStyleSheet.h"
#import "NSString+VersionChecking.h"

@implementation FPPopoverController (Mezzanine)

- (void)stylizeBorderlessWithArrowColor:(UIColor *)arrowColor
{
  self.customTint = !arrowColor ? [MezzanineStyleSheet sharedStyleSheet].grey50Color : arrowColor;
  self.contentView.radius = 4.0;
  self.usesGradient = NO;
  self.lineBorder = NO;
  self.border = NO;
  [self setShadowsHidden:YES];
}


- (void)stylizeWithNavigationBar:(BOOL)hasNavigationBar
{
  if (!hasNavigationBar) {
    self.customTint =  [MezzanineStyleSheet sharedStyleSheet].darkestFillColor;    // dark blue
  } else {
    self.customTint = [MezzanineStyleSheet sharedStyleSheet].grey50Color;
  }
  
  self.contentView.radius = 4.0;
  self.usesGradient = NO;
  self.lineBorder = YES;
  self.customLineBorderColor = [MezzanineStyleSheet sharedStyleSheet].defaultHoverColor;
  self.border = NO;
}

- (void)stylizeAsActionSheet
{
  self.contentView.radius = 4.0;
	self.customTint = [UIColor whiteColor];
  self.usesGradient = NO;
  self.lineBorder = NO;
  //self.customLineBorderColor = [MezzanineStyleSheet sharedStyleSheet].defaultHoverColor;
  self.border = NO;
}

@end
