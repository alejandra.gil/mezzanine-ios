//
//  OBOvum+Adorn.h
//  Mezzanine
//
//  Created by Zai Chang on 7/10/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import "OBDragDropManager.h"


// Convenience methods of adding various visual feedback to an OBOvum

@interface OBOvum (Adorners)

-(void) adornWithDeletionMode:(BOOL)deletionMode;
-(void) adornWithMessage:(NSString*)message tintColor:(UIColor*)tintColor;
-(void) clearAdorner;

@end
