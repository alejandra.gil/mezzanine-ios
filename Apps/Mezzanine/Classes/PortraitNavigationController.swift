//
//  PortraitNavigationController.swift
//  Mezzanine
//
//  Created by miguel on 12/12/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

import UIKit

class PortraitNavigationController: MezzanineNavigationController {

  override var shouldAutorotate : Bool {
    return false
  }

  override var preferredInterfaceOrientationForPresentation : UIInterfaceOrientation {
    return UIInterfaceOrientation.portrait
  }

  override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
    return UIInterfaceOrientationMask.portrait
  }
  
  override var preferredStatusBarStyle : UIStatusBarStyle {
    return .lightContent
  }
  
  override var prefersStatusBarHidden : Bool {
    return false
  }
  
  override var preferredStatusBarUpdateAnimation : UIStatusBarAnimation {
    return .fade
  }
}
