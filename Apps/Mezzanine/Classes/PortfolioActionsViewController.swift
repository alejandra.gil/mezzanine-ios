//
//  PortfolioActionsViewController.swift
//  Mezzanine
//
//  Created by miguel on 9/2/17.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import UIKit

enum PortfolioActionsBarState {
  case emptyPortfolio
  case active
  case paused
  case initial
}

class PortfolioActionsViewController: UIViewController, FPPopoverControllerDelegate {

  var interactor: PortfolioActionsInteractor!

  var currentPopover: FPPopoverController?
  var currentAlertController: UIAlertController?

  @IBOutlet var movableContainerView: UIView!
  @IBOutlet var movableContainerTrailingConstraint: NSLayoutConstraint!

  @IBOutlet var presentationSliderView: UIView!
  @IBOutlet var addItemsButton: UIButton!
  @IBOutlet var moreButton: UIButton!
  @IBOutlet var presentButton: UIButton!
  @IBOutlet var presentationSliderSeparatorView: UIView!
  @IBOutlet var leftSeparatorView: UIView!
  @IBOutlet var rightSeparatorView: UIView!

  fileprivate var nextTraitCollection: UITraitCollection!
  fileprivate var isLandscape: Bool = false
  fileprivate var shouldAnimate: Bool = false
  
  var presentationSliderViewController: PresentationSliderViewController!
  var presentationSliderInteractor: PresentationSliderInteractor!
  
  var state: PortfolioActionsBarState = .initial {
    didSet {
      if state == .emptyPortfolio {
        emptyLayout(shouldAnimate)
      } else if state == .active {
        activePresentationLayout(shouldAnimate)
      } else if state == .paused {
        pausedPresentationLayout(shouldAnimate)
      } else {
        emptyLayout(shouldAnimate)
      }
    }
  }


  override func viewDidLoad() {
    super.viewDidLoad()
    
    let styleSheet = MezzanineStyleSheet.shared()
    view.backgroundColor = styleSheet?.grey50Color

    addItemsButton.setTitle("Portfolio Add Action Button".localized, for: UIControlState())
    moreButton.setTitle("Portfolio More Action Button".localized, for: UIControlState())
    presentButton.setTitle("Portfolio Present Action Button".localized, for:UIControlState())

    addItemsButton.accessibilityIdentifier = "PortfolioActionsView.AddButton"
    presentButton.accessibilityIdentifier = "PortfolioActionsView.PresentButton"
    moreButton.accessibilityIdentifier = "PortfolioActionsView.MoreButton"

    leftSeparatorView.backgroundColor = styleSheet?.grey80Color
    rightSeparatorView.backgroundColor = styleSheet?.grey80Color
    presentationSliderSeparatorView.backgroundColor = styleSheet?.grey80Color

    styleSheet?.stylizeActionBarButton(addItemsButton)
    styleSheet?.stylizeActionBarButton(moreButton)
    styleSheet?.stylizeActionBarButton(presentButton)

    let maskView = UIImageView(image: UIImage(named: "portfolio-actions-bar-mask"))
    maskView.frame = self.view.bounds
    self.view.mask = maskView
    
    initializePresentationSlider()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    
    // TMP: Removed animations in favor of US for this purpose
    // shouldAnimate = true
  }

  override func viewWillDisappear(_ animated: Bool) {
    if let currentPopover = currentPopover {
      if currentPopover.isPopoverVisible {
        currentPopover.dismissPopover(animated: true)
      }
    }
  }
  
  deinit {
    print("PortfolioActionsViewController deinit")

    presentationSliderViewController?.interactor.communicator = nil

    presentationSliderViewController?.interactor = nil
    presentationSliderViewController?.delegate = nil

    presentationSliderInteractor?.communicator = nil
    presentationSliderInteractor?.presenter = nil
    presentationSliderInteractor?.systemModel = nil
  }
  
  override func viewDidLayoutSubviews() {
    interactor.defineCurrentState()
    
    // For some reason I don't know, duplicate the call makes everything work
    interactor.defineCurrentState()
  }
  
  // MARK: Presentation slider
  
  func initializePresentationSlider() {
    presentationSliderInteractor = PresentationSliderInteractor()
    presentationSliderInteractor.systemModel = interactor.systemModel
    presentationSliderInteractor.communicator = interactor.communicator

    presentationSliderViewController = PresentationSliderViewController(sliderStyle: .landscape)
    
    presentationSliderViewController.interactor = presentationSliderInteractor
    presentationSliderInteractor.presenter = presentationSliderViewController

    self.addChildViewController(presentationSliderViewController)
    let view = presentationSliderViewController.view
    view?.autoresizingMask = [.flexibleWidth]
    view?.frame = presentationSliderView.frame
    presentationSliderView.addSubview(view!)
    presentationSliderViewController.didMove(toParentViewController: self)
    
    presentationSliderViewController.updateLayout(animate: false)
  }


  // MARK: Layout
  
  override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
    super.viewWillTransition(to: size, with: coordinator)
    
    isLandscape = size.width > size.height ? true : false
  }
  
  override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
    super.traitCollectionDidChange(previousTraitCollection)
    
    guard let view = transitionCoordinator?.containerView ?? self.view?.window else { return }
    isLandscape = view.frame.width > view.frame.height ? true : false
  }

  func emptyLayout(_ animated: Bool) {
    let animationBlock = {
      self.moreButton.alpha = 0.0
      self.presentButton.alpha = 0.0
      self.presentationSliderSeparatorView.alpha = 0.0
      self.presentationSliderView.alpha = 0.0
      self.leftSeparatorView.alpha = 0.0
      self.rightSeparatorView.alpha = 0.0
      self.view.mask!.frame = self.addItemsButton.frame.insetBy(dx: -6, dy: 0).offsetBy(dx: self.moreButton.frame.width - 6, dy: 0)

      self.movableContainerTrailingConstraint.constant = -(self.moreButton.frame.width - 6)
      self.view.layoutIfNeeded()
    }
    
    if animated {
      UIView.animate(withDuration: 0.3, animations: {
        animationBlock()
      }) 
    }else {
      animationBlock()
    }
  }

  func activePresentationLayout(_ animated: Bool) {
    
    UIView.performWithoutAnimation({
      self.presentButton.setTitle("Presentation Stop Button Title".localized, for:UIControlState())
      self.presentButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 25, bottom: 0, right: 0)
    })
    
    let rect: CGRect = isLandscape ? self.view.frame.insetBy(dx: 2.0, dy: 0) : CGRect(x: presentButton.frame.minX, y: 0, width: self.presentButton.frame.width + self.addItemsButton.frame.width + self.moreButton.frame.width, height: self.moreButton.frame.maxY).insetBy(dx: 4.0, dy: 0).offsetBy(dx: 4.0, dy: 0)
    
    let animationBlock = {
      self.presentButton.setImage(UIImage(named:"portfolio-controls-stop"), for: UIControlState())
      self.presentButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
      
      self.moreButton.alpha = 1.0
      self.presentButton.alpha = 1.0
      self.presentationSliderSeparatorView.alpha = self.isLandscape ? 1.0 : 0.0
      self.presentationSliderView.alpha = self.isLandscape ? 1.0 : 0.0
      self.leftSeparatorView.alpha = 1.0
      self.rightSeparatorView.alpha = 1.0
      self.view.mask!.frame = rect
      
      self.movableContainerTrailingConstraint.constant = 0
      self.presentationSliderViewController.updateLayout(animate: false)
      self.view.layoutIfNeeded()
    }
    
    if animated {
      UIView.animate(withDuration: 0.3, animations: {
        animationBlock()
      }) 
    }else {
      animationBlock()
    }
  }
  
  func pausedPresentationLayout(_ animated: Bool) {
    UIView.performWithoutAnimation({
      self.presentButton.setTitle("Portfolio Present Action Button".localized, for:UIControlState())
      self.presentButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 0)
      
      // Set left and right inset to match space left between title and left margin of the view
      self.presentButton.setImage(UIImage(named:"portfolio-controls-present"), for: UIControlState())
      self.presentButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 11, bottom: 0, right: 0)
      
      self.presentationSliderSeparatorView.alpha = 0.0
      self.presentationSliderView.alpha = isLandscape ? 1.0 : 0.0
    })
    

    let rect: CGRect = CGRect(x: presentButton.frame.minX, y: 0, width: presentButton.frame.width + addItemsButton.frame.width + moreButton.frame.width, height: moreButton.frame.maxY).insetBy(dx: -4.0, dy: 0).offsetBy(dx: -4.0, dy: 0)
    
    let animationBlock = {
      // Set left and right inset to maintain same inset as stop button insets
      self.presentButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: -9, bottom: 0, right: 0)
      
      self.moreButton.alpha = 1.0
      self.presentButton.alpha = 1.0
      self.presentationSliderSeparatorView.alpha = 0.0
      self.presentationSliderView.alpha = 0.0
      self.leftSeparatorView.alpha = 1.0
      self.rightSeparatorView.alpha = 1.0
      
      self.view.mask!.frame = rect
      self.movableContainerTrailingConstraint.constant = 0
      self.view.layoutIfNeeded()
    }
    
    if animated {
      UIView.animate(withDuration: 0.3, animations: {
        animationBlock()
      }) 
    }else {
      animationBlock()
    }
  }


  // Actions

  @IBAction func add(_ sender: AnyObject) {
    let addMenuInteractor = PortfolioAddMenuInteractor(systemModel: interactor.systemModel, communicator: interactor.communicator)
    addMenuInteractor.presenter = self
    
    let addMenuViewController = PortfolioAddMenuViewController(interactor: addMenuInteractor)
    addMenuViewController?.fromView = addItemsButton
    
    // TODO: should be weak?
    let popover = FPPopoverController(contentViewController: addMenuViewController, delegate: self)
    popover?.stylizeAsActionSheet()

    let popoverPresentingBlock = { popover?.presentPopover(from: self.addItemsButton.frame, in: self.addItemsButton.superview, permittedArrowDirections: FPPopoverArrowDirectionDown, animated: true) }

    if self.traitCollection.isRegularWidth() {
        popover?.popoverPresentingBlock = popoverPresentingBlock as? () -> Void
    }

    popoverPresentingBlock()
    
    addMenuViewController?.popover = popover
    self.currentPopover = popover
  }

  @IBAction func present(_ sender: AnyObject) {

    let analyticsManager = MezzanineAnalyticsManager.sharedInstance
    if state == .paused {
      analyticsManager.tagEvent(analyticsManager.startPresentationEvent, attributes: [analyticsManager.sourceKey: analyticsManager.presentButtonAttribute])
    }
    else if state == .active {
      analyticsManager.tagEvent(analyticsManager.stopPresentationEvent, attributes: [analyticsManager.sourceKey: analyticsManager.presentButtonAttribute])
    }

    interactor.startOrPausePresentation()
  }

  @IBAction func more(_ sender: AnyObject) {

    let menuViewController = PortfolioMoreMenuViewController()
    menuViewController.systemModel = interactor.systemModel

    let analyticsManager = MezzanineAnalyticsManager.sharedInstance

    menuViewController.requestPortfolioExport = {
      analyticsManager.tagEvent(analyticsManager.downloadPresentionEvent)
      self.interactor.exportPortfolio()
      self.currentPopover?.dismissPopover(animated: false)
    }

    menuViewController.requestPortfolioDeleteAll = {
      self.confirmClearPortfolio()
      self.currentPopover?.dismissPopover(animated: false)
    }

    // TODO: should be weak?
    let popover = FPPopoverController(contentViewController: menuViewController, delegate: self)
    popover?.stylizeAsActionSheet()

    let popoverPresentingBlock = { popover?.presentPopover(from: self.moreButton.frame, in: self.moreButton.superview, permittedArrowDirections: FPPopoverArrowDirectionDown, animated: true) }

    if self.traitCollection.isRegularWidth() {
      popover?.popoverPresentingBlock = popoverPresentingBlock as? () -> Void
    }

    popoverPresentingBlock()

    self.currentPopover = popover
  }


  func confirmClearPortfolio() {

    let alertController = UIAlertController(title: "Portfolio Clear Dialog Title".localized, message: "Portfolio Clear Dialog Message".localized, preferredStyle:.alert)

    let cancelAction = UIAlertAction(title: "Portfolio Clear Dialog Cancel Button".localized, style: .cancel) { (_) in
      self.currentAlertController = nil
    }

    let confirmAction = UIAlertAction(title: "Portfolio Clear Dialog Confirm Button".localized, style: .default) { (_) in
      self.interactor.clearPortfolio()
      self.currentAlertController = nil
    }

    alertController.addAction(cancelAction)
    alertController.addAction(confirmAction)

    self.present(alertController, animated: true, completion: nil)

    self.currentAlertController = alertController
  }

  func showUploadLimitReachedAlert(_ numberOfItems: Int, requestTransaction: MZFileBatchUploadRequestTransaction, uids: Array<String>, continueBlock: @escaping MZFileBatchUploadRequestContinuationBlock) {

    let alertController = UIAlertController(title: "Upload Continuation Dialog Title".localized, message: String(format: "Upload Continuation Dialog Message".localized, uids.count, numberOfItems), preferredStyle:.alert)

    let cancelAction = UIAlertAction(title: "Upload Continuation Dialog Cancel Button".localized, style: .cancel) { (_) in
      continueBlock(requestTransaction, uids, false)
    }

    let confirmAction = UIAlertAction(title: "Upload Continuation Dialog Confirm Button".localized, style: .default) { (_) in
      continueBlock(requestTransaction, uids, true)
    }

    alertController.addAction(cancelAction)
    alertController.addAction(confirmAction)

    self.present(alertController, animated: true, completion: nil)

    self.currentAlertController = alertController
  }

  func showUploadErrordAlert(_ error: NSError) {

    let alertController = UIAlertController(title: "Upload FileTypeNotSupported Dialog Title".localized, message: error.localizedDescription, preferredStyle:.alert)

    let okAction = UIAlertAction(title: "Generic Button Title Okay".localized, style: .default, handler: nil)

    alertController.addAction(okAction)

    self.present(alertController, animated: true, completion: nil)

    self.currentAlertController = alertController
  }

  func showPartialUploadAlertAndContinue(_ errorMessage: String, continueClosure: @escaping () -> ()) {

    let alertController = UIAlertController(title: "Image Upload Error Dialog Title".localized, message: errorMessage, preferredStyle:.alert)

    let okAction = UIAlertAction(title: "Generic Button Title Okay".localized, style: .default) { (_) in
      continueClosure()
    }

    alertController.addAction(okAction)

    self.present(alertController, animated: true, completion: nil)

    self.currentAlertController = alertController
  }
}

// Presenter methods

extension PortfolioActionsViewController {

  func dimissImagePicker() {

    if (presentedViewController != nil) {
      dismiss(animated: true, completion: nil)
    }
  }
}
