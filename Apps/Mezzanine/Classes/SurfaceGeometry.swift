//
//  SurfaceGeometry.swift
//  Mezzanine
//
//  Created by miguel on 21/2/17.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import Foundation

protocol SurfaceGeometryProtocol {

  func roundedFrameForSurface(_ surface: MZSurface) -> CGRect
  func rectForSurface(_ surface: MZSurface) -> CGRect
  func rectForExtendedSurface(_ surface: MZSurface) -> CGRect
  func surfaceForLocation(_ location: CGPoint) -> MZSurface?
  func closestSurfaceForLocation(_ location: CGPoint) -> MZSurface?
  func surfaceForLocationInExtendedArea(_ location: CGPoint) -> MZSurface?
  func surfacesContainedInRect(_ rect: CGRect) -> Array<MZSurface>?
  func surfacesIntersectingWithRect(_ rect: CGRect) -> Array<MZSurface>?
}

// A surface is represented by:
// - Surface Rect: its actual frame
// - Extended Rect: frame including margins

struct SurfaceGeom {
  var surface: MZSurface!
  var surfaceRect: CGRect!
  var extendedRect: CGRect!
}

class SurfaceGeometry: NSObject, SurfaceGeometryProtocol {

  var surfaces: [MZSurface]? {
    didSet {
      surfaceGeoms = calculateSurfaceGeom()
    }
  }

  var containerRect: CGRect? {
    didSet {
      surfaceGeoms = calculateSurfaceGeom()
    }
  }

  var borderHorizontalMargin: CGFloat = 0.0
  var interItemHorizontalMargin: CGFloat = 0.0

  fileprivate var surfaceGeoms: Array<SurfaceGeom>?


  fileprivate func fixMixedGeometrySurfaceBoundingFeld() {

    guard let surfaces = surfaces else { return }

    // Calculate total width needed to represent all surfaces
    for surface in surfaces {

      // Bug 18709.
      // If protein were right we'd rely on doing
      // let surfaceBoundingFeld = surface.boundingFeld
      // let surfacePhysicalSize = surfaceBoundingFeld.physicalSize

      // Bug 20079
      // Continuation of the previous bug that only adjusted the bounding feld
      // This refactor of the patch also sets the right offset for the internal felds

      /// Patch:
      var x: Double = 0.0
      var y: Double = 0.0

      // Store initial bounding feld x position to use it later
      let boundingFeldRectXBefore = surface.boundingFeld.feldRect().minX

      // Fix physical size of surface bounding feld
      for(index, feld) in surface.felds.enumerated() {

        let feld = feld as! MZFeld
        let offset = index >= 1 ? feld.feldRect().minX - (surface.felds[index - 1] as AnyObject).feldRect().maxX : 0.0

        x += Double(feld.feldRect().width + offset)
        y = Double(feld.feldRect().height)
      }

      let surfacePhysicalSize = MZVect.doubleWith(x: x, y: y)
      let surfaceBoundingFeld = surface.boundingFeld
      surfaceBoundingFeld?.physicalSize = surfacePhysicalSize

      // Use new bounding feld x position to use it later to calculate offset of internal felds
      let boundingFeldRectXAfter = surface.boundingFeld.feldRect().minX
      let boundingFeldRectDiffX = abs(boundingFeldRectXBefore - boundingFeldRectXAfter)

      // Shift internal felds to adjust their position according to the origin of the boundingFeld
      for feld in surface.felds {
        let feld = feld as! MZFeld
        let center = feld.center!
        let newCenterX = NSNumber(value: center.x.doubleValue - Double(boundingFeldRectDiffX))
        feld.center = MZVect(x: newCenterX, y: center.y, z: center.z)
      }
    }
  }


  fileprivate func calculateSurfaceGeom() -> Array<SurfaceGeom>? {

    guard let surfaces = surfaces else { return nil }
    guard let containerRect = containerRect else { return nil }
    guard surfaces.count > 0 else { return nil }

    var tmpSurfaceArray = Array<SurfaceGeom>()
    var totalSurfaceWidth: CGFloat = 0.0
    var totalMarginWidth: CGFloat = 0.0

    var maxSurfaceWidth: CGFloat = 0.0
    var minSurfaceWidth: CGFloat = CGFloat(MAXFLOAT)
    var maxFeldWidth: CGFloat = 0.0

    var interItemMarginCount: CGFloat = 0.0

    // Bug 18709 and 20079
    fixMixedGeometrySurfaceBoundingFeld()

    // Calculate total width needed to represent all surfaces
    for surface in surfaces {
      let surfaceBoundingFeld = surface.boundingFeld
      let surfacePhysicalSize = surfaceBoundingFeld?.physicalSize
      let surfaceRect = CGRect(x: 0, y: 0, width: CGFloat((surfacePhysicalSize?.x.doubleValue)!), height: CGFloat((surfacePhysicalSize?.y.doubleValue)!))
      tmpSurfaceArray.append(SurfaceGeom(surface: surface, surfaceRect: surfaceRect, extendedRect: surfaceRect))
      totalSurfaceWidth += surfaceRect.width
      maxSurfaceWidth = max(maxSurfaceWidth, surfaceRect.width)
      minSurfaceWidth = min(minSurfaceWidth, surfaceRect.width)
      maxFeldWidth = max(maxFeldWidth, CGFloat((surface.felds[0] as! MZFeld).physicalSize.x.doubleValue))
      interItemMarginCount += 2
    }

    // This provides enough marging to never need to zoom below 1.0 the scroll view hosting the workspace
    let physicalBorderHorizontalMargin = maxSurfaceWidth
    let physicalInterItemMargin = maxFeldWidth / 20.0

    totalMarginWidth += 2 * physicalBorderHorizontalMargin + physicalInterItemMargin * interItemMarginCount

    let ratioPhysicalVirtualWidth = (totalMarginWidth + totalSurfaceWidth) / containerRect.width

    borderHorizontalMargin = physicalBorderHorizontalMargin / ratioPhysicalVirtualWidth
    interItemHorizontalMargin = physicalInterItemMargin / ratioPhysicalVirtualWidth

    var cumOffset: CGFloat = borderHorizontalMargin
    var finalSurfaceGeomArray = Array<SurfaceGeom>()

    for index in 0...tmpSurfaceArray.count - 1 {

      let surfaceGeom = tmpSurfaceArray[index]

      let surfaceWidth = surfaceGeom.surfaceRect.width / ratioPhysicalVirtualWidth
      let surfaceHeight = surfaceWidth / surfaceGeom.surfaceRect.width * surfaceGeom.surfaceRect.height
      let finalSurfaceRect = CGRect(x: (cumOffset + interItemHorizontalMargin), y: ((containerRect.height - surfaceHeight) / 2.0), width: (surfaceWidth), height: (surfaceHeight))

      let extendedSurfaceX = index == 0 ? 0 : cumOffset
      let extendedSurfaceWidth = surfaceWidth + 2 * interItemHorizontalMargin
      let externalMargin = (index == 0 || index == tmpSurfaceArray.count - 1) ? borderHorizontalMargin : 0

      let extendedSurfaceRect = CGRect(x: extendedSurfaceX, y: 0, width: externalMargin + surfaceWidth + 2 * interItemHorizontalMargin, height: containerRect.height)

      finalSurfaceGeomArray.append(SurfaceGeom(surface: surfaceGeom.surface, surfaceRect: finalSurfaceRect, extendedRect: extendedSurfaceRect))
      cumOffset += extendedSurfaceWidth
    }

    return finalSurfaceGeomArray
  }

  func roundedFrameForSurface(_ surface: MZSurface) -> CGRect {
    return rectForSurface(surface).roundedFrame()
  }

  func rectForSurface(_ surface: MZSurface) -> CGRect {

    let surfaceGeom = surfaceGeoms?.filter{ $0.surface == surface }.first

    if let surfaceGeom = surfaceGeom {
      return surfaceGeom.surfaceRect
    }

    return CGRect.zero
  }

  func rectForExtendedSurface(_ surface: MZSurface) -> CGRect {

    let surfaceGeom = surfaceGeoms?.filter{ $0.surface == surface }.first

    if let surfaceGeom = surfaceGeom {
      return surfaceGeom.extendedRect
    }

    return CGRect.zero
  }

  func surfaceForLocation(_ location: CGPoint) -> MZSurface? {

    let surfaceGeom = surfaceGeoms?.filter{ $0.surfaceRect.contains(location) }.first

    if let surfaceGeom = surfaceGeom {
      return surfaceGeom.surface
    }

    return nil
  }

  func closestSurfaceForLocation(_ location: CGPoint) -> MZSurface? {

    guard let surfaceGeoms = surfaceGeoms else { return nil }

    var minDistance = CGFloat.greatestFiniteMagnitude
    var closestSurface = surfaceForLocation(location)

    if closestSurface != nil {
      return closestSurface
    }

    for surfaceGeom in surfaceGeoms {
      let distance = CGPointDistance(location, CGPoint(x: surfaceGeom.surfaceRect.midX, y: surfaceGeom.surfaceRect.midY))
      if distance < minDistance {
        minDistance = distance
        closestSurface = surfaceGeom.surface
      }
    }
    return closestSurface
  }

  func surfaceForLocationInExtendedArea(_ location: CGPoint) -> MZSurface? {

    let surfaceGeom = surfaceGeoms?.filter{ $0.extendedRect.contains(location) }.first

    if let surfaceGeom = surfaceGeom {
      return surfaceGeom.surface
    }

    return nil
  }

  func surfacesContainedInRect(_ rect: CGRect) -> Array<MZSurface>? {

    return surfaceGeoms?.filter{ rect.contains($0.surfaceRect) }.map{ $0.surface }
  }

  func surfacesIntersectingWithRect(_ rect: CGRect) -> Array<MZSurface>? {

    return surfaceGeoms?.filter{ rect.intersects($0.surfaceRect) }.map{ $0.surface }
  }
}
