//
//  PortfolioViewController.m
//  Mezzanine
//
//  Created by Zai Chang on 1/16/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import "PortfolioViewController.h"
#import "PortfolioViewController_Private.h"
#import "MezzanineMainViewController.h"
#import "MezzanineStyleSheet.h"
#import "FPPopoverController.h"
#import "ButtonMenuViewController.h"
#import "SlideCreationViewController.h"
#import "PortfolioCollectionViewCell.h"
#import "NSObject+BlockObservation.h"
#import "AssetViewController.h"
#import "DocumentUploadInfoViewController.h"
#import "LiveStreamCollectionViewCell.h"
#import "PortfolioMoreMenuViewController.h"
#import "WhiteboardCaptureViewController.h"
#import "WhiteboardInteractor.h"
#import "UIViewController+FPPopover.h"
#import "UIView+OBDropZone.h"

#import <ImageIO/ImageIO.h>
#import <AVFoundation/AVFoundation.h>

#import "NSString+VersionChecking.h"
#import "UITraitCollection+Additions.h"

#import "Mezzanine-Swift.h"

static NSString *kPortfolioItemCellIdentifier = @"PortfolioItemCell";
static NSString *kLiveStreamCellIdentifier = @"LiveStreamCell";

static CGFloat kPortfolioStatusAnimationDuration = 0.2;


@interface PortfolioViewController () <PhotoPickerDelegate>
{
  BOOL isRegularWidth;
}

@end

@implementation PortfolioViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
      // Custom initialization
      _animationCount = 0;
    }
    return self;
}


- (void)dealloc
{
  DLog(@"%@ dealloc", [self class]);
  onceTokenCollectionViewContentOffset = 0;
}


- (void)viewDidLoad
{
  [super viewDidLoad];
  
  [self initAccessibility];

  isRegularWidth = self.traitCollection.isRegularWidth;

  MezzanineStyleSheet *styleSheet = [MezzanineStyleSheet sharedStyleSheet];
  self.view.backgroundColor = styleSheet.grey40Color;
  
  _collectionView.backgroundColor = [UIColor clearColor];

  [_collectionView registerClass:[PortfolioCollectionViewCell class] forCellWithReuseIdentifier:kPortfolioItemCellIdentifier];
  [_collectionView registerClass:[LiveStreamCollectionViewCell class] forCellWithReuseIdentifier:kLiveStreamCellIdentifier];
  _collectionView.alwaysBounceHorizontal = YES;
  _collectionView.scrollsToTop = NO;
  
  _collectionView.dataSource = self;
  _collectionView.delegate = self;

  _collectionView.dropZoneHandler = self;

  _statusLabel.text = @"";
  _statusLabel.accessibilityLabel = [NSString stringWithFormat:@"%@-%@", [self class], @"StatusLabel"];
  [_statusLabel setFont:[UIFont dinOfSize:12.0]];
  _statusView.hidden = YES;

  [_hintLabel setText:NSLocalizedString(@"Portfolio Hint Label Text", nil)];
  [_hintLabel setFont:[UIFont dinOfSize:isRegularWidth ? 14.0 : 11.0]];
  [_hintLabel setTextColor:styleSheet.textSecondaryColor];

  _titleLabelLeftConstraint.constant = isRegularWidth ? 15.0 : 11.0;

  UILongPressGestureRecognizer *longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(titleLabelLongPressed:)];
  _titleLabel.userInteractionEnabled = YES;
  [_titleLabel addGestureRecognizer:longPressRecognizer];

  longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(presentButtonLongPressed:)];
  [_presentButton addGestureRecognizer:longPressRecognizer];

  [_progressPieView setPieColor:styleSheet.textSecondaryColor];
  [self updateStatus];

  // The more button is the anchor point to where an Open In menu for an exported file should be presented
  [[[MezzanineAppContext currentContext] exportManager] setOpenInPopoverAnchorView:_moreButton];

  [_addButton setTitle:NSLocalizedString(@"Portfolio Add Action Button", nil) forState:UIControlStateNormal];
  [_moreButton setTitle:NSLocalizedString(@"Portfolio More Action Button", nil) forState:UIControlStateNormal];
  [_presentButton setTitle:NSLocalizedString(@"Portfolio Present Action Button", nil) forState:UIControlStateNormal];
}


- (void)initAccessibility
{
  self.view.accessibilityIdentifier = @"PortfolioView";
  _collectionView.accessibilityIdentifier = @"PortfolioView.CollectionView";
  
  _addButton.accessibilityIdentifier = @"PortfolioView.AddButton";
  _presentButton.accessibilityIdentifier = @"PortfolioView.PresentButton";
  _moreButton.accessibilityIdentifier = @"PortfolioView.MoreButton";

  _statusView.accessibilityIdentifier = @"PortfolioView.statusView";
  _progressPieView.accessibilityIdentifier = @"PortfolioView.progressPieView";

  _hintLabel.accessibilityIdentifier = @"PortfolioView.hintLabel";
}


- (void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];

  [self applyFontStyles];
  [self updateButtonStates];
  [self updateLayout];
  
  //Bug 15952. Dirty hack to force collection view to content offset x=0
  dispatch_once(&onceTokenCollectionViewContentOffset, ^{
    [_collectionView setContentOffset:CGPointZero];
  });
}


- (void)viewDidAppear:(BOOL)animated
{
  [super viewDidAppear:animated]; // Without this, autolayout seems to not be performed on first load despite the default implementation doing nothing according to Apple
  _viewIsVisible = YES;
}


- (void)viewWillDisappear:(BOOL)animated
{
  [super viewWillDisappear:animated];

  FPPopoverController *currentPopover = self.currentPopover;
  if ([currentPopover isPopoverVisible])
    [currentPopover dismissPopoverAnimated:YES];
}


- (void)viewDidDisappear:(BOOL)animated
{
  [super viewDidDisappear:animated];
  _viewIsVisible = NO;
}


- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
  [self.currentPopover dismissPopoverAnimated:NO];
  [self updateLayout];
}


- (void)updateFontsAndLabels
{
  [_hintLabel setFont:[UIFont dinOfSize:isRegularWidth ? 14.0 : 11.0]];
  _titleLabelLeftConstraint.constant = isRegularWidth ? 15.0 : 11.0;
}


-(void)updateButtonStates
{
  BOOL presentationEnabled = [self.systemModel.currentWorkspace.presentation.slides count] > 0;
  _presentButton.enabled = presentationEnabled;
  MezzanineStyleSheet *styleSheet = [MezzanineStyleSheet sharedStyleSheet];

  _presentButtonImageView.tintColor = presentationEnabled ? [UIColor whiteColor] : styleSheet.mediumFillColor;

  [UIView animateWithDuration:0.15 animations:^{
    _hintLabel.alpha = self.workspace.presentation.numberOfSlides == 0 ? 1.0 : 0.0;
  }];
}


- (void)updateLayout
{
  isRegularWidth = self.traitCollection.isRegularWidth;
  [self updateCollectionViewLayout];
  [self updateCurrentSlideView:self.presentation.currentSlideIndex animated:NO];
  [self updateFontsAndLabels];
  [self performSelector:@selector(updateCurrentSlideViewAfterLayoutIsSet) withObject:nil afterDelay:0.0];
}


- (void)updateCurrentSlideViewAfterLayoutIsSet
{
  [self updateCurrentSlideView:self.presentation.currentSlideIndex animated:NO];
}


- (void)applyFontStyles
{
  MezzanineStyleSheet *styleSheet = [MezzanineStyleSheet sharedStyleSheet];
  [styleSheet stylizeLabel:self.titleLabel withPanelTitle:NSLocalizedString(@"Portfolio Title", nil)];

  [styleSheet stylizeSmallFontButton:self.addButton];
  [styleSheet stylizeSmallFontButton:self.moreButton];
  [styleSheet stylizeSmallFontButton:self.presentButton];

  [self.view setNeedsLayout];
}


#pragma mark - Model

- (void)setAppContext:(MezzanineAppContext *)appContext
{
  if (_appContext != appContext)
  {
    if (_appContext)
    {
      self.systemModel = nil;
    }
    
    _appContext = appContext;
    
    if (_appContext)
    {
      self.systemModel = _appContext.applicationModel.systemModel;
    }
  }
}


- (void)setSystemModel:(MZSystemModel *)systemModel
{
  if (_systemModel != systemModel)
  {
    if (_systemModel)
    {
      for (id observer in _systemModelObservers)
        [_systemModel removeObserverWithBlockToken:observer];

      _systemModelObservers = nil;
    }
    
    _systemModel = systemModel;
    
    if (_systemModel)
    {
      _systemModelObservers = [NSMutableArray array];
      id observer;
      __weak typeof(self) __self = self;
      
      observer = [_systemModel addObserverForKeyPath:@"portfolioExport.info.progress"
                                             options:0
                                             onQueue:[NSOperationQueue mainQueue]
                                                task:^(id obj, NSDictionary *change) {
                                                  [__self updateStatus];
                                                }];
      [_systemModelObservers addObject:observer];

      observer = [_systemModel addObserverForKeyPath:@"infopresence.status"
                                             options:0
                                             onQueue:[NSOperationQueue mainQueue]
                                                task:^(id obj, NSDictionary *change) {
                                                  // Bug 14703 Video items keep the old livestream when calling to another workspace with a video
                                                  if (__self.systemModel.infopresence.status == MZInfopresenceStatusActive)
                                                    [__self updateItemsInVisibleCells];
                                                }];
      [_systemModelObservers addObject:observer];
    }
  }
}

- (void)setWorkspace:(MZWorkspace *)workspace
{
  if (_workspace != workspace)
  {
    if (_workspace)
    {
      for (id observer in _workspaceObservers)
        [_workspace removeObserverWithBlockToken:observer];
      _workspaceObservers = nil;
    }
    
    _workspace = workspace;
    
    if (_workspace)
    {
      _workspaceObservers = [NSMutableArray array];
      id observer;
      __weak typeof(self) __self = self;
      observer = [_workspace addObserverForKeyPath:@"presentation" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) onQueue:[NSOperationQueue mainQueue] task:^(id object, NSDictionary *change) {
        __self.presentation = __self.workspace.presentation;
      }];
      [_workspaceObservers addObject:observer];

    }
    
    self.presentation = _workspace.presentation;
  }
}


- (void)setPresentation:(MZPresentation *)presentation
{
  if (_presentation != presentation)
  {
    if (_presentation)
    {
      for (id observer in _presentationObservers)
        [_presentation removeObserverWithBlockToken:observer];
      _presentationObservers = nil;
    }

    _presentation = presentation;

    if (_presentation)
    {
      _presentationObservers = [NSMutableArray array];
      id observer;
      __weak typeof(self) __self = self;
      observer = [_presentation addObserverForKeyPath:@"slides" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) onQueue:[NSOperationQueue mainQueue] task:^(id object, NSDictionary *change) {
//        DLog(@"PortfolioViewController %x observe change in slides: %@", self, presentation.slides);
        NSNumber *kind = change[NSKeyValueChangeKindKey];
        NSIndexSet *indexSet = change[NSKeyValueChangeIndexesKey];
        
        NSMutableArray *indexPaths = [NSMutableArray array];
        [indexSet enumerateIndexesUsingBlock:^(NSUInteger index, BOOL *stop) {
          [indexPaths addObject:[NSIndexPath indexPathForItem:index inSection:0]];
        }];

        if (indexPaths.count > 0)
        {
          __self.animationCount++;

          // Collection Views have a limit of 31 simultaneous animations, if the 32 takes place
          // the app may crash. By counting the current one we can either accept a new one,
          // or directly reloading the whole collection view.
          //
          // Also need to check if the view is currently visisble, because calling performBatchUpdates:
          // while a modal view is covering it prevents certain update within UICollectionView's internal
          // state it seems, causing future consistency exceptions.
          if (__self.viewIsVisible && __self.animationCount < 32)
          {
            [__self.collectionView performBatchUpdates:^{
              if ([kind integerValue] == NSKeyValueChangeInsertion)  // Rows were added
              {
                [__self.collectionView insertItemsAtIndexPaths:indexPaths];
              }
              else if ([kind integerValue] == NSKeyValueChangeRemoval)  // Rows were removed
              {
                [__self.collectionView deleteItemsAtIndexPaths:indexPaths];
              }
            } completion:^(BOOL finished) {
              __self.animationCount--;
            }];
          }
          else
          {
            [__self.collectionView reloadData];
            __self.animationCount--;
          }

          [__self updateButtonStates];

          if (([kind integerValue] == NSKeyValueChangeRemoval))
          {
            NSArray *removedObjects = change[NSKeyValueChangeOldKey];
            [__self dismissPopoverIfContextMatches:removedObjects];
          }
        }
      }];
      [_presentationObservers addObject:observer];

      observer = [_presentation addObserverForKeyPath:@"active" options:NSKeyValueObservingOptionInitial onQueue:[NSOperationQueue mainQueue] task:^(id obj, NSDictionary *change) {
        [__self setCurrentSlideViewVisible:__self.presentation.active animated:YES];
        [__self updateCurrentSlideView:__self.presentation.currentSlideIndex animated:NO];
      }];
      [_presentationObservers addObject:observer];
      
      observer = [_presentation addObserverForKeyPath:@"currentSlideIndex" task:^(id obj, NSDictionary *change) {
        [__self updateCurrentSlideView:__self.presentation.currentSlideIndex animated:YES];
      }];
      [_presentationObservers addObject:observer];

      [self.collectionView reloadData];
    }
  }
}


- (void)updateItemsInVisibleCells
{
  NSArray *visibleCells = [_collectionView visibleCells];
  NSMutableArray *indexPathsToReload = [NSMutableArray new];
  for (UICollectionViewCell *cell in visibleCells)
  {
    NSIndexPath *ixPath = [_collectionView indexPathForCell:cell];
    NSInteger pos = ixPath.item;
    MZSlide *slide = [_workspace.presentation.slides objectAtIndex:pos];
    if (slide.isVideo)
    {
      if ([cell isKindOfClass:[LiveStreamCollectionViewCell class]])
      {
        [(LiveStreamCollectionViewCell *)cell setLiveStream:[_workspace liveStreamWithUid:slide.contentSource]];
      }
      else
      {
        [indexPathsToReload addObject:ixPath];
      }
    }
    else
    {
      if ([cell isKindOfClass:[PortfolioCollectionViewCell class]])
      {
        [(PortfolioCollectionViewCell *)cell setSlide:(MZPortfolioItem *)slide];
      }
      else
      {
        [indexPathsToReload addObject:ixPath];
      }
    }
  }
  if (indexPathsToReload.count)
    [_collectionView reloadItemsAtIndexPaths:indexPathsToReload];
}



#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
  NSInteger numberOfSlides = _presentation.numberOfSlides;
//  DLog(@"PortfolioViewController %x numberOfSlides: %d", self, numberOfSlides);
  return numberOfSlides;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
  MZSlide *slide = [_workspace.presentation.slides objectAtIndex:indexPath.row];
  
  UICollectionViewCell *cell;
  if (slide.isVideo)
  {
    LiveStreamCollectionViewCell *liveStreamCell = [collectionView dequeueReusableCellWithReuseIdentifier:kLiveStreamCellIdentifier forIndexPath:indexPath];
    liveStreamCell.liveStream = [_workspace liveStreamWithUid:slide.contentSource];
    cell = liveStreamCell;
  }
  else
  {
    PortfolioCollectionViewCell *portfolioItemCell = [collectionView dequeueReusableCellWithReuseIdentifier:kPortfolioItemCellIdentifier forIndexPath:indexPath];
    portfolioItemCell.communicator = self.mezzanineViewController.communicator;  // Has to come before setSlide:
    portfolioItemCell.slide = (MZPortfolioItem *)slide;
    cell = portfolioItemCell;
  }
  
  BOOL cellHasDragDropGestureRecognizer = NO;
  for (UIGestureRecognizer *recognizer in cell.gestureRecognizers)
  {
    if ([recognizer isKindOfClass:[UILongPressGestureRecognizer class]])
      cellHasDragDropGestureRecognizer = YES;
  }
  
  if (!cellHasDragDropGestureRecognizer)
  {
    OBDragDropManager *dragDropManager = [OBDragDropManager sharedManager];
    UILongPressGestureRecognizer *dragDropRecognizer = [dragDropManager createLongPressDragDropGestureRecognizerWithSource:self];
    dragDropRecognizer.delegate = self;
    dragDropRecognizer.minimumPressDuration = 0.12;
    [cell addGestureRecognizer:dragDropRecognizer];
  }

#if DOUBLE_TAP_TO_LOCAL_PREVIEW
//  UITapGestureRecognizer *doubleTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTapGesture:)];
//  doubleTapRecognizer.numberOfTouchesRequired = 2;
//  [cell addGestureRecognizer:doubleTapRecognizer];
#endif

  return cell;
}


// Copied from LiveStreamsViewController. Should consider consolidating code
- (void)updateCollectionViewLayout
{
  // Fix for 17251.
  // We have to check also for the height since iPhone Plus has regular width but behaves as an iPhone (compact width).
  CGFloat margin = isRegularWidth && self.traitCollection.isRegularHeight ? 16.0 : 6.0;
  CGFloat sectionMargin = isRegularWidth && self.traitCollection.isRegularHeight ? 16.0 : 12.0;
  CGFloat itemHeight = (NSInteger) (self.collectionView.frame.size.height - 2 * margin);
  CGFloat itemWidth = (NSInteger) (itemHeight * 16.0 / 9.0);
  
  UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
  layout.itemSize = CGSizeMake(itemWidth, itemHeight);
  layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
  layout.sectionInset = UIEdgeInsetsMake(margin, sectionMargin, margin, sectionMargin);
  layout.minimumLineSpacing = margin;
  layout.minimumInteritemSpacing = margin;
  [self.collectionView setCollectionViewLayout:layout];
}

#if DOUBLE_TAP_TO_LOCAL_PREVIEW
- (void)doubleTapGesture:(UIGestureRecognizer*)recognizer
{
  if (recognizer.state == UIGestureRecognizerStateRecognized)
  {
    if ([recognizer.view isKindOfClass:[PortfolioCollectionViewCell class]])
    {
      PortfolioCollectionViewCell *cell = (PortfolioCollectionViewCell*)recognizer.view;
      [self openAssetViewerForSlide:cell.slide fromCell:cell animated:YES];
    }
  }
}
#endif



#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
  MZSlide *slide = [self.workspace.presentation.slides objectAtIndex:indexPath.row];
  PortfolioCollectionViewCell *cell = (PortfolioCollectionViewCell*)[collectionView cellForItemAtIndexPath:indexPath];
  
  ButtonMenuViewController *menuViewController = [[ButtonMenuViewController alloc] initWithNibName:nil bundle:nil];
  
  NSMutableArray *menuItems = [NSMutableArray array];
  if (slide.isVideo)
  {
    MZLiveStream *liveStream = [_workspace liveStreamWithUid:slide.contentSource];
    if (liveStream.displayName)
      [menuItems addObject:[ButtonMenuHeaderTextItem itemWithText:liveStream.displayName title:nil action:nil]];
  }

  [menuItems addObject:[ButtonMenuItem itemWithTitle:NSLocalizedString(@"Portfolio Item Menu Place", nil) action:^{
    [self placeItemOnScreen:slide];
    
    [self.currentPopover dismissPopoverAnimated:NO];
  }]];

  NSString *presentItemString = _presentation.active ? NSLocalizedString(@"Portfolio Item Jump To Slide", nil) : NSLocalizedString(@"Portfolio Item Present From Slide", nil);
  [menuItems addObject:[ButtonMenuItem itemWithTitle:presentItemString action:^{
    MZCommunicator *communicator = self.mezzanineViewController.communicator;
    [communicator.requestor requestPresentationStartRequest:self.workspace.uid currentIndex:indexPath.row];
    [self.currentPopover dismissPopoverAnimated:NO];
    
    // Analytics
    MezzanineAnalyticsManager *analyticsManager = [MezzanineAnalyticsManager sharedInstance];
    if (!_presentation.active)
      [analyticsManager tagEvent:analyticsManager.startPresentationEvent
                                                attributes:@{analyticsManager.sourceKey : analyticsManager.portfolioItemAttribute}];
    else
      [analyticsManager tagEvent:analyticsManager.scrollPresentationEvent
                                                attributes:@{analyticsManager.sourceKey : analyticsManager.jumpToSlideAttribute}];

  }]];
  
  if (slide.isVideo)
  {
    [menuItems addObject:[ButtonMenuItem itemWithTitle:NSLocalizedString(@"Live Stream Item Menu View", nil) action:^{
      [self openAssetViewerFromCell:cell];
      [self.currentPopover dismissPopoverAnimated:NO];
    }]];
  }
  else
  {
    [menuItems addObject:[ButtonMenuItem itemWithTitle:NSLocalizedString(@"Portfolio Item Menu View", nil) action:^{
      [self openAssetViewerFromCell:cell];
      [self.currentPopover dismissPopoverAnimated:NO];
    }]];
  }
  
  ButtonMenuItem *deleteItem = [ButtonMenuItem itemWithTitle:NSLocalizedString(@"Portfolio Item Menu Delete", nil) action:^{
    [self deleteSlide:slide];
		
		[self.currentPopover dismissPopoverAnimated:NO];
	}];
	deleteItem.buttonColor = [MezzanineStyleSheet sharedStyleSheet].redHighlightColor;
  [menuItems addObject:deleteItem];
  menuViewController.menuItems = menuItems;
  
  // N.B. menuViewController needs its menu items before having a content height
  FPPopoverController *popover = [[FPPopoverController alloc] initWithContentViewController:menuViewController delegate:self];
  popover.context = slide;
  [popover stylizeAsActionSheet];
  [popover presentPopoverFromRect:[cell frame] inView:[cell superview] permittedArrowDirections:FPPopoverArrowDirectionDown animated:YES];
  self.currentPopover = popover;
}


- (void)placeItemOnScreen:(MZSlide*)slide
{
  MZCommunicator *communicator = self.mezzanineViewController.communicator;
  [communicator.requestor requestWindshieldItemCreateRequest:_workspace.uid contentSource:slide.contentSource surfaceName:@"main" feldRelativeCoords:[FeldRelativeCoords new] feldRelativeSize:[FeldRelativeSize new]];
}


- (void)deleteSlide:(MZSlide*)slide
{
  MZCommunicator *communicator = self.mezzanineViewController.communicator;
  [communicator.requestor requestPortfolioItemDeleteRequest:self.workspace.uid uid:slide.uid];
}


- (void)openAssetViewerFromCell:(UICollectionViewCell*)cell
{
  if (!cell)
    return;
  
  MZItem *itemForController = nil;
  UIImage *placeholderImage = nil;
  
  if ([cell isKindOfClass:[PortfolioCollectionViewCell class]])
  {
    placeholderImage = ((PortfolioCollectionViewCell*)cell).imageView.image;
    itemForController = ((PortfolioCollectionViewCell*)cell).slide;
  }
  else
  {
    placeholderImage = ((LiveStreamCollectionViewCell*)cell).liveStream.thumbnail;
    itemForController = ((LiveStreamCollectionViewCell*)cell).liveStream;
  }
  
  AssetViewController *controller = [[AssetViewController alloc] initWithNibName:nil bundle:nil];
  controller.appContext = _appContext;
  controller.workspace = self.workspace;
  controller.placeholderImage = placeholderImage;
  controller.item = itemForController;
  
  [_appContext.mainNavController pushViewController:controller transitionView:cell];
}


#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
  if ([gestureRecognizer isKindOfClass:[OBLongPressDragDropGestureRecognizer class]] && self.mezzanineViewController.currentDragDropRecognizer != nil)
    return NO;
  return YES;
}


#pragma mark - OBOvumSource

-(OBOvum *) createOvumFromView:(UIView*)sourceView
{
  if (![sourceView isKindOfClass:[UICollectionViewCell class]]) {
    return nil;
  }
  
  UICollectionViewCell *cell = (UICollectionViewCell *)sourceView;
  NSIndexPath *indexPath = [_collectionView indexPathForCell:cell];
  MZSlide *slide = [_presentation.slides objectAtIndex:indexPath.item];
  OBOvum *ovum = [[OBOvum alloc] init];
  ovum.dataObject = slide;
  return ovum;
}


-(UIView *) createDragRepresentationOfSourceView:(UIView *)sourceView inWindow:(UIWindow*)window
{
  if ([sourceView isKindOfClass:[PortfolioCollectionViewCell class]])
  {
    PortfolioCollectionViewCell *cell = (PortfolioCollectionViewCell *)sourceView;
    
    CGRect frameInWindow = [cell convertRect:cell.bounds toView:cell.window];
    frameInWindow = [window convertRect:frameInWindow fromWindow:cell.window];
    
    PortfolioCollectionViewCell *dragCell = [[PortfolioCollectionViewCell alloc] initWithFrame:frameInWindow];
    dragCell.slide = cell.slide;
    dragCell.imageView.image = cell.imageView.image;
    return dragCell;
  }
  else if ([sourceView isKindOfClass:[LiveStreamCollectionViewCell class]])
  {
    LiveStreamCollectionViewCell *cell = (LiveStreamCollectionViewCell *)sourceView;
    
    CGRect frameInWindow = [cell convertRect:cell.bounds toView:cell.window];
    frameInWindow = [window convertRect:frameInWindow fromWindow:cell.window];
    
    LiveStreamCollectionViewCell *dragCell = [[LiveStreamCollectionViewCell alloc] initWithFrame:frameInWindow];
    dragCell.liveStream = cell.liveStream;
    dragCell.imageView.image = [[(LiveStreamCollectionViewCell *)sourceView imageView] image];
    return dragCell;
  }

  return nil;
}


-(void) dragViewWillAppear:(UIView *)dragView inWindow:(UIWindow*)window atLocation:(CGPoint)location
{
  dragView.alpha = 1.0;
  
  [UIView animateWithDuration:0.25 animations:^{
    dragView.alpha = 0.75;
  }];

  POPSpringAnimation *animation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
  animation.toValue = [NSValue valueWithCGSize:CGSizeMake(1.25, 1.25)];
  animation.velocity = isRegularWidth ? [NSValue valueWithCGSize:CGSizeMake(0.06, 0.06)] : [NSValue valueWithCGSize:CGSizeMake(0.12, 0.12)] ;
  animation.springBounciness = 16;
  animation.springSpeed = isRegularWidth ? 16 : 12;
  [dragView pop_addAnimation:animation forKey:@"scale"];
  
  animation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewCenter];
  animation.toValue = [NSValue valueWithCGPoint:location];
  animation.velocity = [NSValue valueWithCGSize:CGSizeMake(0.0, 0.0)];
  animation.springBounciness = 6;
  animation.springSpeed = 15;
  [dragView pop_addAnimation:animation forKey:@"center"];
}


-(void) ovumDragWillBegin:(OBOvum*)ovum
{
  DLog(@"PortfolioViewController ovumDragWillBegin");
  
  MZCommunicator *communicator = [MezzanineAppContext currentContext].currentCommunicator;
  if ([ovum.dataObject isKindOfClass:[MZItem class]])
  {
    MZItem *item = (MZItem*)ovum.dataObject;
    [communicator.requestor requestPortfolioItemGrabRequest:self.workspace.uid uid:item.uid];
  }
}


-(void) ovumDragEnded:(OBOvum*)ovum
{
  DLog(@"PortfolioViewController ovumDragEnded");
  
  MZCommunicator *communicator = [MezzanineAppContext currentContext].currentCommunicator;
  if ([ovum.dataObject isKindOfClass:[MZItem class]])
  {
    MZItem *item = (MZItem*)ovum.dataObject;
    [communicator.requestor requestPortfolioItemRelinquishRequest:self.workspace.uid uid:item.uid];
  }
  
  [ovum.dragView pop_removeAllAnimations];
}


#pragma mark - OBDropTarget

- (void)showInsertionMarkerAtIndex:(NSInteger)index
{
  if (_insertionMarker == nil)
  {
    MezzanineStyleSheet *styleSheet = [MezzanineStyleSheet sharedStyleSheet];
    _insertionMarker = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 4.0, self.collectionView.frame.size.height - 24.0)];
    _insertionMarker.backgroundColor = [styleSheet.defaultHighlightColor colorWithAlphaComponent:0.72];
    _insertionMarker.layer.cornerRadius = 1.0;
    
    [_collectionView addSubview:_insertionMarker];
  }

  UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout*)_collectionView.collectionViewLayout;
  _insertionMarker.center = CGPointMake(layout.sectionInset.left + (layout.itemSize.width + layout.minimumInteritemSpacing) * index - layout.minimumInteritemSpacing / 2.0,
                                        _collectionView.bounds.size.height / 2.0);
}


- (void)hideInsertionIndicator
{
  if (!_insertionMarker)
    return;

  [UIView animateWithDuration:0.2 animations:^{
    _insertionMarker.alpha = 0.0;
  } completion:^(BOOL finished) {
    [_insertionMarker removeFromSuperview];
    _insertionMarker = nil;
  }];
}


- (NSInteger)insertionIndexForLocation:(CGPoint)location
{
  UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout*)_collectionView.collectionViewLayout;
  CGFloat layoutWidth = (layout.itemSize.width + layout.minimumInteritemSpacing);
  NSInteger insertionIndex = round((location.x - layout.minimumInteritemSpacing / 2.0) / layoutWidth);
  insertionIndex = MIN(_presentation.slides.count, MAX(insertionIndex, 0));
  return insertionIndex;
}


-(OBDropAction) ovumEntered:(OBOvum*)ovum inView:(UIView*)view atLocation:(CGPoint)location
{
  id dataObject = ovum.dataObject;
  if ([dataObject isKindOfClass:[MZLiveStream class]])
    return OBDropActionCopy;
  else if ([dataObject isKindOfClass:[MZPortfolioItem class]])
    return OBDropActionMove;
  return OBDropActionNone;
}


-(OBDropAction) ovumMoved:(OBOvum*)ovum inView:(UIView*)view atLocation:(CGPoint)location
{
  [self handleEdgeScrollingEventAtLocation:location];

  NSInteger insertionIndex = [self insertionIndexForLocation:location];
  if (insertionIndex != NSNotFound)
  {
    id dataObject = ovum.dataObject;
    if ([dataObject isKindOfClass:[MZPortfolioItem class]])
    {
      MZPortfolioItem *item = (MZPortfolioItem*)dataObject;
      if (insertionIndex < item.index || insertionIndex > (item.index + 1))
        [self showInsertionMarkerAtIndex:insertionIndex];
      else
        [self hideInsertionIndicator];
    }
    else
    {
      [self showInsertionMarkerAtIndex:insertionIndex];
    }
  }

  return ovum.dropAction;
}


-(void) ovumDropped:(OBOvum*)ovum inView:(UIView*)view atLocation:(CGPoint)location
{
  DLog(@"PortfolioViewController ovumDropped: %f, %f", location.x, location.y);
  
  NSInteger insertionIndex = [self insertionIndexForLocation:location];
  if (insertionIndex != NSNotFound)
  {
    id dataObject = ovum.dataObject;
    if ([dataObject isKindOfClass:[MZLiveStream class]])
    {
      MZLiveStream *liveStream = (MZLiveStream*)dataObject;
      [_mezzanineViewController.communicator.requestor requestPortfolioItemInsertRequest:_mezzanineViewController.workspace.uid contentSource:liveStream.uid index:insertionIndex];
    }
    else if ([dataObject isKindOfClass:[MZPortfolioItem class]])
    {
      MZPortfolioItem *item = (MZPortfolioItem*)dataObject;
      if (insertionIndex > item.index)
      {
        // Take into account the fact that if moving an item forward, its place in the list should no longed be counted.
        insertionIndex--;
      }
      
      if (insertionIndex < item.index || insertionIndex > item.index) {
        [_mezzanineViewController.communicator.requestor requestPortfolioItemReorderRequest:_mezzanineViewController.workspace.uid uid:item.uid index:insertionIndex];
      }
    }
  }
  
  [self endEdgeScrolling];
  
  [self hideInsertionIndicator];
}


-(void) ovumExited:(OBOvum*)ovum inView:(UIView*)view atLocation:(CGPoint)location
{
  DLog(@"PortfolioViewController ovumExited: %f, %f", location.x, location.y);

  [self endEdgeScrolling];
  
  [self hideInsertionIndicator];
}



#pragma mark - Edge Scrolling

-(void) beginEdgeScrolling
{
  if (_edgeScrollingTimer)
    return;
  
  _edgeScrollingSpeed = 0.0;
  _edgeScrollingTimer = [NSTimer scheduledTimerWithTimeInterval:0.03 target:self selector:@selector(handleEdgeScrollingTimer:) userInfo:nil repeats:YES];
  
}


-(void) endEdgeScrolling
{
  if (_edgeScrollingTimer)
  {
    [_edgeScrollingTimer invalidate];
    _edgeScrollingTimer = nil;
  }
}


-(void) handleEdgeScrollingTimer:(NSTimer*)timer
{
  CGPoint contentOffset = _collectionView.contentOffset;
  contentOffset.x += _edgeScrollingSpeed;
  
  CGFloat maxContentOffsetX = _collectionView.contentSize.width - _collectionView.bounds.size.width - _collectionView.contentInset.right;
  contentOffset.x = MIN(maxContentOffsetX, contentOffset.x);
  CGFloat minContentOffsetX = 0;
  contentOffset.x = MAX(minContentOffsetX, contentOffset.x);
  
  _collectionView.contentOffset = contentOffset;
}


-(void) handleEdgeScrollingEventAtLocation:(CGPoint)location
{
  CGFloat scrollZone = isRegularWidth ? 120.0 : 64.0;
  CGFloat maxScrollSpeed = isRegularWidth ? 24.0 : 12.0;
  
  // Not actually sure why view location is affected by contentOffset, since this is supposed to be in the UIScrollView's coordinate space ...
  CGPoint contentOffset = _collectionView.contentOffset;
  location.x -= contentOffset.x;
  
  CGFloat viewWidth = _collectionView.frame.size.width;
  
  if (location.x < scrollZone)
  {
    _edgeScrollingSpeed = - (scrollZone - location.x) / scrollZone * maxScrollSpeed;
    [self beginEdgeScrolling];
  }
  else if (location.x > viewWidth - scrollZone)
  {
    _edgeScrollingSpeed = (location.x - (viewWidth - scrollZone)) / scrollZone * maxScrollSpeed;
    [self beginEdgeScrolling];
  }
  else
  {
    _edgeScrollingSpeed = 0.0;
    [self endEdgeScrolling];
  }
}



#pragma mark - Actions

- (IBAction)add:(id)sender
{
  BOOL cameraIsAvailable = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];

  ButtonMenuViewController *menuViewController = [[ButtonMenuViewController alloc] initWithNibName:nil bundle:nil];
  
  NSMutableArray *menuItems = [NSMutableArray array];
  
  [menuItems addObject:[ButtonMenuItem itemWithTitle:NSLocalizedString(@"Upload Text Button Title", nil) action:^{
    [self.currentPopover dismissPopoverAnimated:NO];
    [self showSlideCreationView];
    
    [[MezzanineAnalyticsManager sharedInstance] tagEvent:[MezzanineAnalyticsManager sharedInstance].addPortfolioItemEvent
                                                            attributes:@{[MezzanineAnalyticsManager sharedInstance].sourceKey : [MezzanineAnalyticsManager sharedInstance].textAttribute}];
  }]];

  [menuItems addObject:[ButtonMenuItem itemWithTitle:NSLocalizedString(@"Upload Photo From Library Button Title", nil) action:^{
    [self.currentPopover dismissPopoverAnimated:NO];
    [self showImagePicker:UIImagePickerControllerSourceTypePhotoLibrary];
    [[MezzanineAnalyticsManager sharedInstance] tagEvent:[MezzanineAnalyticsManager sharedInstance].addPortfolioItemEvent
                                                            attributes:@{[MezzanineAnalyticsManager sharedInstance].sourceKey : [MezzanineAnalyticsManager sharedInstance].libraryAttribute}];
  }]];
  
  if (cameraIsAvailable)
    [menuItems addObject:[ButtonMenuItem itemWithTitle:NSLocalizedString(@"Upload New Photo Button Title", nil) action:^{
      [self.currentPopover dismissPopoverAnimated:NO];
      [self showImagePicker:UIImagePickerControllerSourceTypeCamera];
      [[MezzanineAnalyticsManager sharedInstance] tagEvent:[MezzanineAnalyticsManager sharedInstance].addPortfolioItemEvent
                                                              attributes:@{[MezzanineAnalyticsManager sharedInstance].sourceKey : [MezzanineAnalyticsManager sharedInstance].cameraAttribute}];
      
    }]];

//    [menuItems addObject:[ButtonMenuItem itemWithTitle:NSLocalizedString(@"Upload PDF Document Button Title", nil) action:^{
//      [self.currentPopover dismissPopoverAnimated:NO];
//      [self showPDFUploadInfoView];
//    }]];

  BOOL whiteboardIsAvailable = _systemModel.whiteboards.count > 0;
  if (whiteboardIsAvailable)
  {
    BOOL thereAreMoreThanOneWhiteboard = _systemModel.whiteboards.count > 1;

    NSString *title = thereAreMoreThanOneWhiteboard ? NSLocalizedString(@"Whiteboard Capture Multiple Button Title", @"Whiteboard Capture") :
                                                      NSLocalizedString(@"Whiteboard Capture Button Title", @"Whiteboard Capture");

    [menuItems addObject:[ButtonMenuItem itemWithTitle:title action:^{
      
      if (thereAreMoreThanOneWhiteboard)
      {
        [self.currentPopover dismissPopoverAnimated:NO];
        [self showWhiteboardCaptureView];
      }
      else
      {
        [self.currentPopover dismissPopoverAnimated:YES];
        [self captureWhiteboard:_systemModel.whiteboards[0]];
      }
      
      [[MezzanineAnalyticsManager sharedInstance] tagEvent:[MezzanineAnalyticsManager sharedInstance].addPortfolioItemEvent
                                                              attributes:@{[MezzanineAnalyticsManager sharedInstance].sourceKey : [MezzanineAnalyticsManager sharedInstance].whiteboardAttribute}];
    }]];
    

  }
  
  menuViewController.menuItems = menuItems;
  
  FPPopoverController *popover = [[FPPopoverController alloc] initWithContentViewController:menuViewController delegate:self];
  [popover stylizeAsActionSheet];
  __weak typeof (popover) weakPopover = popover;
  void (^popoverPresentingBlock)() = ^{
    [weakPopover presentPopoverFromRect:self.addButton.frame inView:self.addButton.superview permittedArrowDirections:FPPopoverArrowDirectionDown animated:YES];
  };

  if (isRegularWidth)
    popover.popoverPresentingBlock = popoverPresentingBlock;

  popoverPresentingBlock();
  self.currentPopover = popover;
}


- (void)presentFromSlideAtIndex:(NSInteger)slideIndex
{
  MZCommunicator *communicator = self.appContext.currentCommunicator;
  if (self.workspace.presentation.active)
  {
    // Analytics
    NSString *source = self.presentButton.highlighted ? [MezzanineAnalyticsManager sharedInstance].presentButtonAttribute : [MezzanineAnalyticsManager sharedInstance].sliderButtonAttribute;
    [[MezzanineAnalyticsManager sharedInstance] tagEvent:[MezzanineAnalyticsManager sharedInstance].stopPresentationEvent
                                              attributes:@{[MezzanineAnalyticsManager sharedInstance].sourceKey : source}];
    
    [communicator.requestor requestPresentationStopRequest:self.workspace.uid];
    
    // Pre-emptive reaction in local UI?
    self.workspace.presentation.active = NO;
  }
  else
  {
    // Analytics
    [[MezzanineAnalyticsManager sharedInstance] tagEvent:[MezzanineAnalyticsManager sharedInstance].startPresentationEvent
                                              attributes:@{[MezzanineAnalyticsManager sharedInstance].sourceKey : [MezzanineAnalyticsManager sharedInstance].presentButtonAttribute}];
    
    [communicator.requestor requestPresentationStartRequest:self.workspace.uid currentIndex:slideIndex];
    
    // Pre-emptive reaction in local UI?
    self.workspace.presentation.active = YES;
  }
  
  [self.view setNeedsLayout];
}


- (IBAction)present:(id)sender
{
  NSInteger indexToPresent = self.workspace.presentation.currentSlideIndex;
  [self presentFromSlideAtIndex:indexToPresent];
}


- (void)presentButtonLongPressed:(UIGestureRecognizer*)recognizer
{
  if (recognizer.state == UIGestureRecognizerStateRecognized)
    [self presentFromSlideAtIndex:self.workspace.presentation.currentSlideIndex];
}


- (void)updatePresentationModeState
{
  if (self.workspace.presentation.active)
  {
    NSString *title = NSLocalizedString(@"Presentation Stop Button Title", nil);
    [_presentButton setTitle:title forState:UIControlStateNormal];
    UIImage *templatePresentationImage = [[UIImage imageNamed:@"PresentationStop.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [_presentButtonImageView setImage:templatePresentationImage];
    
    if (!isRegularWidth)
      _presentButton.contentEdgeInsets = UIEdgeInsetsMake(0, -20, 0, 0);
  }
  else
  {
    NSString *title = NSLocalizedString(@"Presentation Present Button Title", nil);
    [_presentButton setTitle:title forState:UIControlStateNormal];
    UIImage *templatePresentationImage = [[UIImage imageNamed:@"PresentationPlay.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [_presentButtonImageView setImage:templatePresentationImage];

    if (!isRegularWidth)
      _presentButton.contentEdgeInsets = UIEdgeInsetsMake(0, 6, 0, 0);
  }

  [self updateButtonStates];
}


- (IBAction)more:(id)sender
{
  __weak typeof(self) __self = self;
  PortfolioMoreMenuViewController *menuViewController = [[PortfolioMoreMenuViewController alloc] initWithNibName:nil bundle:nil];
  menuViewController.systemModel = _systemModel;

  menuViewController.requestPortfolioExport = ^{
    [[MezzanineAnalyticsManager sharedInstance] tagEvent:[MezzanineAnalyticsManager sharedInstance].downloadPresentionEvent attributes:@{}];
    [__self exportPortfolio];
    [__self.currentPopover dismissPopoverAnimated:NO];
  };
  
  menuViewController.requestPortfolioDeleteAll = ^{
    [__self clearPortfolio];
    [__self.currentPopover dismissPopoverAnimated:NO];
  };

  FPPopoverController *popover = [[FPPopoverController alloc] initWithContentViewController:menuViewController delegate:self];
  [popover stylizeAsActionSheet];
  __weak typeof (popover) weakPopover = popover;
  void (^popoverPresentingBlock)() = ^{
    [weakPopover presentPopoverFromRect:self.moreButton.frame inView:self.moreButton.superview permittedArrowDirections:FPPopoverArrowDirectionDown animated:YES];
  };

  if (isRegularWidth)
    popover.popoverPresentingBlock = popoverPresentingBlock;

  popoverPresentingBlock();
  self.currentPopover = popover;
}


- (void)titleLabelLongPressed:(UIGestureRecognizer*)recognizer
{
  if (recognizer.state == UIGestureRecognizerStateBegan)
  {
    NSString *title = _presentation.slides.count > 0 ? [NSString stringWithFormat:NSLocalizedString(@"Number Of Items Title", nil), (unsigned long)_presentation.slides.count] : NSLocalizedString(@"No Items Title", nil);
    UIMenuItem *menuItem = [UIMenuItem new];
    [menuItem setTitle:title];
    
    UIMenuController *menuController = [UIMenuController sharedMenuController];
    [menuController setTargetRect:recognizer.view.frame inView:recognizer.view.superview];
    menuController.arrowDirection = UIMenuControllerArrowDown;
    menuController.menuItems = [NSArray arrayWithObject:menuItem];
    [menuController setMenuVisible:YES animated:YES];
  }
}


-(void) showImagePicker:(UIImagePickerControllerSourceType)source
{
  if (source == UIImagePickerControllerSourceTypePhotoLibrary)
  {
    [PhotoManager.sharedInstance authorizationStatus:^(PHAuthorizationStatus status, BOOL promptedAccess) {
      if (status == PHAuthorizationStatusAuthorized)
      {

        // OBImagePickerController

//        _obImagePicker = [OBImagePickerController new];
//        _obImagePicker.delegate = self;
//        _obImagePicker.modalPresentationStyle = UIModalPresentationFormSheet;
//        [self.mezzanineViewController presentViewController:_obImagePicker animated:YES completion:nil];

        // PhotoPickerController

        PhotoPickerController *photoPicker = [[PhotoPickerController alloc] initWithDelegate:self];
        photoPicker.modalPresentationStyle = UIModalPresentationFormSheet;
        [self.mezzanineViewController presentViewController:photoPicker animated:YES completion:nil];
      }
      else if (!promptedAccess)
      {
        [self presentAccessErrorAlert:UIImagePickerControllerSourceTypePhotoLibrary];
      }
    }];
  }
  else if (source == UIImagePickerControllerSourceTypeCamera)
  {
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if (status == AVAuthorizationStatusAuthorized)
    {
      [self presentImagePickerForCamera];
    }
    else
    {
      [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
        if (granted)
        {
          dispatch_async(dispatch_get_main_queue(), ^{
            [self presentImagePickerForCamera];
          });
        }
        else
        {
          dispatch_async(dispatch_get_main_queue(), ^{
            if (status == AVAuthorizationStatusNotDetermined)
            {
              // First time the user was asked permission to access the camera and denied it
            }
            else
            {
              [self presentAccessErrorAlert:UIImagePickerControllerSourceTypeCamera];
            }
          });
        }
      }];
    }
  }
}


- (void)presentImagePickerForCamera
{
  _imagePicker = [[UIImagePickerController alloc] init];
  _imagePicker.delegate = self;
  _imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
  
  _imagePicker.cameraDevice = UIImagePickerControllerCameraDeviceRear;
  _imagePicker.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
  _imagePicker.showsCameraControls = YES;
  
  [self.mezzanineViewController presentViewController:_imagePicker animated:YES completion:nil];
}


- (void)presentAccessErrorAlert:(UIImagePickerControllerSourceType)source
{
  NSString *title = nil;
  NSString *errorMessage = nil;
  
  if (source == UIImagePickerControllerSourceTypeCamera)
  {
    title = NSLocalizedString(@"Camera Error Enabling Access Title", nil);
    errorMessage = NSLocalizedString(@"Camera Error Enabling Access Message", nil);
  }
  else if (source == UIImagePickerControllerSourceTypePhotoLibrary)
  {
    title = NSLocalizedString(@"Photos Framework Error Enabling Access Title", nil);
    errorMessage = NSLocalizedString(@"Photos Framework Error Enabling Access Message", nil);
  }
  
  UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title
                                                                           message:errorMessage
                                                                    preferredStyle:UIAlertControllerStyleAlert];
  
  UIAlertAction *settingsAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Generic Title Settings", "")
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * _Nonnull action) {
                                                           [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                                                         } ];
  
  UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Generic Title OK", "")
                                                     style:UIAlertActionStyleDefault
                                                   handler:nil];
  
  [alertController addAction:settingsAction];
  [alertController addAction:okAction];
  
  [self.mezzanineViewController presentViewController:alertController animated:YES completion:nil];
}


- (void)showSlideCreationView
{
  SlideCreationViewController *controller = [[SlideCreationViewController alloc] initWithNibName:nil bundle:nil];
  
  MezzanineNavigationController *navController = [[MezzanineNavigationController alloc] initWithRootViewController:controller];
  navController.modalPresentationStyle = UIModalPresentationFormSheet;
  [self.mezzanineViewController presentViewController:navController animated:YES completion:nil];
}


- (void)showPDFUploadInfoView
{
  DocumentUploadInfoViewController *controller = [[DocumentUploadInfoViewController alloc] initWithNibName:nil bundle:nil];
  controller.modalPresentationStyle = UIModalPresentationFormSheet;
  [self.mezzanineViewController presentViewController:controller animated:YES completion:nil];
}


- (void)showWhiteboardCaptureView
{
  MZCommunicator *communicator = _appContext.currentCommunicator;
  
  WhiteboardInteractor *interactor = [[WhiteboardInteractor alloc] init];
  interactor.communicator = communicator;
  interactor.systemModel = _systemModel;
  
  WhiteboardCaptureViewController *controller = [[WhiteboardCaptureViewController alloc] initWithNibName:nil bundle:nil];
  controller.systemModel = _systemModel;
  controller.interactor = interactor;
  controller.preferredContentSize = CGSizeMake(240.0, 308.0);
  controller.didRequestWhiteboardCapture = ^(NSString *whiteboardUid) {
    [self.currentPopover dismissPopoverAnimated:YES];
  };
  
  MezzanineNavigationController *navController = [[MezzanineNavigationController alloc] initWithRootViewController:controller];
  
  FPPopoverController *popover = [[FPPopoverController alloc] initWithContentViewController:navController delegate:self];
  [popover stylizeAsActionSheet];
  __weak typeof (popover) weakPopover = popover;
  void (^popoverPresentingBlock)() = ^{
    [weakPopover presentPopoverFromRect:self.addButton.frame inView:self.addButton.superview permittedArrowDirections:FPPopoverArrowDirectionDown animated:YES];
  };

  if (isRegularWidth)
    popover.popoverPresentingBlock = popoverPresentingBlock;
  
  popoverPresentingBlock();
  self.currentPopover = popover;
}


- (void)captureWhiteboard:(NSDictionary *)whiteboard
{
  MZCommunicator *communicator = self.appContext.currentCommunicator;
  NSString *whiteboardUID = whiteboard[@"uid"];
  
  if (whiteboardUID)
    [communicator.requestor requestWhiteboardCaptureRequest:whiteboardUID workspaceUid:self.workspace.uid];
}


- (void)clearPortfolio
{
  UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Portfolio Clear Dialog Title", nil)
                                                                           message:NSLocalizedString(@"Portfolio Clear Dialog Message", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
  UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Portfolio Clear Dialog Cancel Button", nil)
                                                         style:UIAlertActionStyleCancel
                                                       handler:^(UIAlertAction * _Nonnull action) {
                                                         _currentAlertController = nil;
                                                       }];
  UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Portfolio Clear Dialog Confirm Button", nil)
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * _Nonnull action) {
                                                          [self.appContext.currentCommunicator.requestor requestPortfolioClearRequest:self.workspace.uid];
                                                          _currentAlertController = nil;
                                                        }];
  [alertController addAction:cancelAction];
  [alertController addAction:confirmAction];
  [self presentViewController:alertController animated:YES completion:nil];
  _currentAlertController = alertController;
}


- (void)exportPortfolio
{
  // TODO - Update the following for workspace

  MZSystemModel *systemModel = self.mezzanineViewController.systemModel;
  MZCommunicator *communicator = self.appContext.currentCommunicator;
  systemModel.portfolioExport.info.state = MZExportStateRequested;
  
  [communicator.requestor requestPortfolioDownloadRequest:_workspace.uid];
}


#pragma mark - Current Slide View

- (void)setCurrentSlideViewVisible:(BOOL)visible animated:(BOOL)animated
{
  void (^animations)();
  
  if (visible)
  {
    if (_currentSlideView == nil)
    {
      _currentSlideView = [[UIView alloc] initWithFrame:CGRectZero];
      _currentSlideView.alpha = 0.0;
      _currentSlideView.layer.borderColor = [MezzanineStyleSheet sharedStyleSheet].defaultHighlightColor.CGColor;
      _currentSlideView.layer.borderWidth = isRegularWidth ? 4.0 : 2.0;
      _currentSlideView.layer.cornerRadius = 3.0;
      _currentSlideView.userInteractionEnabled = NO;  // Such that the slide underneath and be drag and dropped
      _currentSlideView.accessibilityIdentifier = @"PortfolioView.currentSlideView";
      [_collectionView addSubview:_currentSlideView];
    }

    animations = ^{
      _currentSlideView.alpha = 1.0;
    };
  }
  else
  {
    animations = ^{
      _currentSlideView.alpha = 0.0;
    };
  }
  
  if (animated)
    [UIView animateWithDuration:0.2 animations:animations];
  else
    animations();
}


- (void)updateCurrentSlideView:(NSInteger)slideIndex animated:(BOOL)animated
{
  if (!_currentSlideView)
    return;

  // To avoid setting the current slide view before inserting the items in the collectionView
  // See bug 11970
  NSInteger itemsInCollectionView = [_collectionView numberOfItemsInSection:0];
  if (!itemsInCollectionView || slideIndex > itemsInCollectionView)
    return;
  
  NSIndexPath *indexPath = [NSIndexPath indexPathForItem:slideIndex inSection:0];
  UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout*)_collectionView.collectionViewLayout;
  UICollectionViewLayoutAttributes *layoutAttributes = [layout layoutAttributesForItemAtIndexPath:indexPath];
  CGRect targetFrame = layoutAttributes.frame;
  targetFrame = CGRectInset(targetFrame, -2, -2);
  void (^animations)() = ^{
    _currentSlideView.frame = targetFrame;
  };
  
  if (animated)
    [UIView animateWithDuration:0.2 animations:animations];
  else
    animations();
}



#pragma mark - Image Upload

-(BOOL) canUploadImage:(UIImage *)image
{
  NSData *imageData = [[NSData alloc] initWithData:UIImageJPEGRepresentation((image), 0.9)];
  
  if (!_systemModel.featureToggles.largeImagesEnabled && !_systemModel.featureToggles.immenseImagesEnabled)
    return [self canUploadImageWithPixelDimension:image.size] && [self canUploadImageWithFileSize:imageData.length];
  
  CGSize imagePixelSize = CGSizeMake(CGImageGetWidth(image.CGImage), CGImageGetHeight(image.CGImage));
  return [self canUploadImageWithPixelSize:imagePixelSize] && [self canUploadImageWithFileSize:imageData.length];
}


-(BOOL) canUploadImageWithPixelDimension:(CGSize)pixelSize
{
  MZUploadLimits *uploadLimits = _appContext.applicationModel.systemModel.uploadLimits;
  return ((pixelSize.width <= uploadLimits.maxImageWidth.floatValue &&
           pixelSize.height <= uploadLimits.maxImageHeight.floatValue) ||
          (pixelSize.height <= uploadLimits.maxImageWidth.floatValue &&
           pixelSize.width <= uploadLimits.maxImageHeight.floatValue));
}


// The width and height, in pixels, of the specified bitmap image (or image mask).
-(BOOL) canUploadImageWithPixelSize:(CGSize)imagePixelSize
{
  long long totalPixels = imagePixelSize.width * imagePixelSize.height;
  
  MZUploadLimits *uploadLimits = _appContext.applicationModel.systemModel.uploadLimits;
  long long limitInPixels = uploadLimits.maxImageSizeInMP.longLongValue * 1024 * 1024;
  return (totalPixels < limitInPixels && totalPixels > 0);
}


// File Size in bytes
-(BOOL) canUploadImageWithFileSize:(long long)fileSize
{
  MZUploadLimits *uploadLimits = _appContext.applicationModel.systemModel.uploadLimits;
  long long limitInBytes = uploadLimits.maxImageSizeInMB.longLongValue * 1024 * 1024;
  return (fileSize < limitInBytes && fileSize > 0);
}


-(void) requestUploadImage:(UIImage*)image filename:(NSString*)filename format:(NSString*)format
{
  DLog(@"PortfolioViewController uploading image %@", image);
  
  void (^onError)(NSError*) = ^(NSError *error) {
    // -updateStatus uses the list of pending transactions to determine the state
    // of affairs, and as such needs to be updated after the current transaction
    // has been removed from the pending list
    [NSOperationQueue.mainQueue addOperationWithBlock:^{
      [self updateStatus];
    }];
    
    NSString *message = error.localizedDescription;
    self.mezzanineViewController.systemModel.popupMessage = message;
  };

  MZFileBatchUploadRequestLimitReachedBlock limitReached = ^(MZFileBatchUploadRequestTransaction *transaction, NSArray *assetUids, MZFileBatchUploadRequestContinuationBlock continuationBlock) {
    [NSOperationQueue.mainQueue addOperationWithBlock:^{
      [self updateStatus];
    }];
  };
  
  void (^onSuccess)() = ^{
    // -updateStatus uses the list of pending transactions to determine the state
    // of affairs, and as such needs to be updated after the current transaction
    // has been removed from the pending list
    [NSOperationQueue.mainQueue addOperationWithBlock:^{
      [self updateStatus];
    }];
  };
  

  MZFileUploadInfo *uploadInfo = [[MZFileUploadInfo alloc] init];
  uploadInfo.fileName = filename;
  uploadInfo.format = format;
  uploadInfo.imageBlock = ^{ return image; };
  
  MZCommunicator *communicator = self.appContext.currentCommunicator;
  [communicator.uploader requestFilesUpload:@[uploadInfo]
                               workspaceUid:communicator.systemModel.currentWorkspace.uid
                               limitReached:limitReached
                      singleUploadCompleted:onSuccess
                               errorHandler:onError];

  
  // Create a thumbnail version of the image for progress
  // WARNING: Possibly long operation on main thread
  //  UIImage *thumbnailImage = [image resizeImagePreservingAspectRatio:CGSizeMake(240, 240)];
  //  transaction.thumbnail = thumbnailImage;
  
  [self updateStatus];
}


#pragma mark - PhotoPickerDelegate

- (void)photoPickerControllerDidCancel
{
  // Nothing for now
}


- (void)photoPickerControllerDidSelectPhotos:(NSArray *)photos
{
  if (![photos count])
    return;
  
  if (!_imageUploadQueue)
  {
    _imageUploadQueue = [[NSOperationQueue alloc] init];
    _imageUploadQueue.name = @"imageUploadQueue";
    [_imageUploadQueue setMaxConcurrentOperationCount:1];
  }
  
  _preparingImageBatchFromAssetLibrary = YES;
  [self updateStatus];
  
  __weak typeof(self) __self = self;
  [_imageUploadQueue addOperationWithBlock:^{
    
    // First we need to filter invalid size images in the batch.
    // And prepares the rest as candidates to be uploaded keeping a reference
    // to its ALAssetRepresentation, to avoid overloading the device memory.
    NSMutableArray *uploadInfos = [[NSMutableArray alloc] init];
    __block NSInteger imagesExceedingPixelSize = 0;
    __block NSInteger imagesExceedingFileSize = 0;
    
    for (PhotoAsset *photoAsset in photos)
    {
      // Fetch data and initialize photo metadata
      [[PhotoManager sharedInstance] fetchPhotoAssetData:photoAsset];

      BOOL acceptedFileSize = [__self canUploadImageWithFileSize:photoAsset.filesize];
      BOOL acceptedPixelSize = NO;
      if (!_systemModel.featureToggles.largeImagesEnabled && !_systemModel.featureToggles.immenseImagesEnabled)
        acceptedPixelSize = [__self canUploadImageWithPixelDimension:CGSizeMake(photoAsset.pixelWidth, photoAsset.pixelHeight)];
      else
        acceptedPixelSize = [__self canUploadImageWithPixelSize:CGSizeMake(photoAsset.pixelWidth, photoAsset.pixelHeight)];
      
      if (acceptedPixelSize && acceptedFileSize)
      {
        MZFileUploadInfo *item = [[MZFileUploadInfo alloc] init];
        item.fileName = photoAsset.filename;
        item.imageDataBlock = photoAsset.imageDataBlock;
        item.format = photoAsset.imageType;
        [uploadInfos addObject:item];
      }
      else
      {
        imagesExceedingPixelSize += !acceptedPixelSize ? 1 : 0;
        imagesExceedingFileSize += !acceptedFileSize ? 1 : 0;
      }
    }

    // Done with preparation update status
    _preparingImageBatchFromAssetLibrary = NO;
    [__self updateStatus];
    
    
    // This block starts the transaction requests that handles the batch upload
    // It is informed in case the paramus or deck reaches its limits to
    // ask the user how to proceed (partial upload or canceling)
    // Also every time each image is uploaded it refreshes the status view
    // and handles the any error with the transaction.
    void (^progressiveUploadBlock)() = ^{
      
      NSInteger numberOfItems = uploadInfos.count;
      MZUploader *uploader = __self.appContext.currentCommunicator.uploader;
      [uploader requestFilesUpload:uploadInfos
                      workspaceUid:self.workspace.uid
                      limitReached:^(MZFileBatchUploadRequestTransaction *requestTransaction,
                                     NSArray *uids,
                                     MZFileBatchUploadRequestContinuationBlock continueBlock) {
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Upload Continuation Dialog Title", nil)
                                                                                                 message:[NSString stringWithFormat:NSLocalizedString(@"Upload Continuation Dialog Message", nil), (long)uids.count, (long)numberOfItems]
                                                                                          preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Upload Continuation Dialog Cancel Button", nil)
                                                                               style:UIAlertActionStyleCancel
                                                                             handler:^(UIAlertAction * _Nonnull action) {
                                                                               continueBlock(requestTransaction, uids, false);
                                                                             }];
                        UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Upload Continuation Dialog Confirm Button", nil)
                                                                                style:UIAlertActionStyleDefault
                                                                              handler:^(UIAlertAction * _Nonnull action) {
                                                                                continueBlock(requestTransaction, uids, true);
                                                                              }];
                        [alertController addAction:cancelAction];
                        [alertController addAction:confirmAction];
                        [__self presentViewController:alertController animated:YES completion:nil];
                        [__self updateStatus];
                      }
             singleUploadCompleted:^{
               [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                 [__self updateStatus];
               }];
             }
                      errorHandler:^(NSError *error) {
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Upload FileTypeNotSupported Dialog Title", nil)
                                                                                                 message:[error localizedDescription]
                                                                                          preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Generic Button Title Okay", nil)
                                                                           style:UIAlertActionStyleDefault
                                                                         handler:nil];
                        [alertController addAction:okAction];
                        [__self presentViewController:alertController animated:YES completion:nil];
                      }];
    };
    
    
    // The progressive batch upload is executed directly or after
    // informing about the number of size-invalid images in the batch
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
      
      if (imagesExceedingFileSize || imagesExceedingPixelSize)
      {
        MZUploadLimits *uploadLimits = _appContext.applicationModel.systemModel.uploadLimits;
        NSString *errorString = nil;
        
        if (imagesExceedingFileSize && imagesExceedingPixelSize)
        {
          if (!_systemModel.featureToggles.largeImagesEnabled && !_systemModel.featureToggles.immenseImagesEnabled)
            errorString = [NSString stringWithFormat:NSLocalizedString(@"Multiple Images Upload File And Pixel Size Error Dialog Message", nil),
                           uploadLimits.maxImageSizeInMB, uploadLimits.maxImageWidth, uploadLimits.maxImageHeight];
          else
            errorString = [NSString stringWithFormat:NSLocalizedString(@"Multiple Images Upload File And Exceeded Megapixels Error Dialog Message", nil), uploadLimits.maxImageSizeInMB, uploadLimits.maxImageSizeInMP];
        }
        else if (imagesExceedingFileSize)
        {
          errorString = (imagesExceedingFileSize == 1 ) ?
          [NSString stringWithFormat:NSLocalizedString(@"Single Image Upload File Size Error Dialog Message", nil), uploadLimits.maxImageSizeInMB] :
          [NSString stringWithFormat:NSLocalizedString(@"Multiple Images Upload File Size Error Dialog Message", nil), uploadLimits.maxImageSizeInMB];
        }
        else
        {
          if (!_systemModel.featureToggles.largeImagesEnabled && !_systemModel.featureToggles.immenseImagesEnabled)
          {
            errorString = (imagesExceedingPixelSize == 1) ?
            [NSString stringWithFormat:NSLocalizedString(@"Single Image Upload Pixel Dimension Error Dialog Message", nil), uploadLimits.maxImageWidth, uploadLimits.maxImageHeight] :
            [NSString stringWithFormat:NSLocalizedString(@"Multiple Images Upload Pixel Dimension Error Dialog Message", nil), uploadLimits.maxImageWidth, uploadLimits.maxImageHeight];
          } else {
            errorString = (imagesExceedingPixelSize == 1) ?
            [NSString stringWithFormat:NSLocalizedString(@"Single Image Upload Exceeded Megapixels Error Dialog Message", nil), uploadLimits.maxImageSizeInMP] :
            [NSString stringWithFormat:NSLocalizedString(@"Multiple Images Upload Exceeded Megapixels Error Dialog Message", nil), uploadLimits.maxImageSizeInMP];
          }
        }
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Image Upload Error Dialog Title", nil)
                                                                                 message:errorString
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Generic Button Title Okay", nil)
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * _Nonnull action) {
                                                           if ([uploadInfos count] != 0)
                                                             progressiveUploadBlock();
                                                         }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
      }
      else
      {
        progressiveUploadBlock ();
      }
    }];

  }];
}


#pragma mark - Status View

-(void) setStatusViewVisible:(BOOL)visible
{
  if (visible)
  {
    _statusView.hidden = NO;

    [UIView animateWithDuration:kPortfolioStatusAnimationDuration animations:^{
      _statusView.alpha = 1.0;
    }];
  }
  else
  {
    [UIView animateWithDuration:kPortfolioStatusAnimationDuration animations:^{
      _statusView.alpha = 0.0;
    } completion:^(BOOL finished) {
      _statusView.hidden = YES;
    }];
  }
}


- (void)updateStatus
{
  NSArray *pendingTransactions = _appContext.currentCommunicator.pendingTransactions;
  NSInteger uploadTransactionsCount = _appContext.currentCommunicator.uploader.numberOfUploadTransactions;

//  DLog(@"PortfolioViewController updateStatus for transactions: %@", pendingTransactions);
  NSString *statusText = @"";
  BOOL uploading = NO;

  if (pendingTransactions.count == 0 && uploadTransactionsCount == 0)
  {
    // Needed with large batches, can be ignore if another batch is already being uploaded.
    if (_preparingImageBatchFromAssetLibrary)
    {
      statusText = NSLocalizedString(@"Portfolio Preparing Upload Status Message", nil);
      uploading = YES;

    }
    else
    {
      statusText = @"";
      uploading = NO;
    }
  }
  else
  {
    NSInteger numberOfImageUploads = [_appContext.currentCommunicator numberOfImagesBeingUploaded];
    NSInteger numberOfFileUploads = [_appContext.currentCommunicator numberOfFilesBeingUploaded];
    uploading = YES;

    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"title.length > 0"];
    NSArray *transactionsWithTitle = [pendingTransactions filteredArrayUsingPredicate:predicate];
    if ([transactionsWithTitle count])
    {
      MZTransaction *firstTransaction = transactionsWithTitle[0];
      statusText = firstTransaction.title;
    }
    else if (numberOfImageUploads >= 1 || numberOfFileUploads >= 1)
    {
      statusText = NSLocalizedString(@"Portfolio Uploading Status Partial Message (Uploading)", nil);

      if (numberOfImageUploads > 1)
        statusText = [NSString stringWithFormat:NSLocalizedString(@"Portfolio Uploading Status Partial Message (...# images)", nil), statusText, (long)numberOfImageUploads];
      else if (numberOfImageUploads == 1)
        statusText = [NSString stringWithFormat:NSLocalizedString(@"Portfolio Uploading Status Partial Message (...image)", nil), statusText];

      if (numberOfImageUploads && numberOfFileUploads)
        statusText = [NSString stringWithFormat:NSLocalizedString(@"Portfolio Uploading Status Partial Message (...and)", nil), statusText];

      if (numberOfFileUploads > 1)
        statusText = [NSString stringWithFormat:NSLocalizedString(@"Portfolio Uploading Status Partial Message (...# PDFs)", nil), statusText, (long)numberOfFileUploads];
      else if (numberOfFileUploads == 1)
        statusText = [NSString stringWithFormat:NSLocalizedString(@"Portfolio Uploading Status Partial Message (...PDF)", nil), statusText];
    }
    else
    {
      // Needed with large batches, can be ignore if another batch is already being uploaded.
      if (_preparingImageBatchFromAssetLibrary)
      {
        statusText = NSLocalizedString(@"Portfolio Preparing Upload Status Message", nil);
        uploading = YES;

      }
      else
      {
        statusText = @"";
        uploading = NO;
      }
    }
  }

  BOOL exportRequested = (_systemModel.portfolioExport.info.state == MZExportStateRequested);
  BOOL downloading = (_systemModel.portfolioExport.info.state == MZExportStateDownloading);

  NSString *exportingText;
  switch (_systemModel.portfolioExport.info.state)
  {
    case MZExportStateRequested:
      exportingText = uploading ? NSLocalizedString(@"Portfolio Preparing Export Lowercase Status Message", nil) : NSLocalizedString(@"Portfolio Preparing Export Uppercase Status Message", nil);
      break;
    case MZExportStateDownloading:
      exportingText = uploading ? NSLocalizedString(@"Portfolio Downloading Export Lowercase Status Message", nil) : NSLocalizedString(@"Portfolio Downloading Export Uppercase Status Message", nil);
      break;
    default:
      exportingText = @"";
      break;
  }

  _statusLabel.text = [NSString stringWithFormat:@"%@%@%@", statusText,
                                                            (uploading && (downloading || exportRequested) ? NSLocalizedString(@"Portfolio Downloading And Uploading Nexus", nil) : @""),
                                                            exportingText];

  [self setStatusViewVisible:(uploading || exportRequested || downloading)];

  if (!uploading && !exportRequested && !downloading)
  {
    [_progressPieView setState:ProgressPieStateInvisible];

  }

  if (uploading || (exportRequested && !downloading))
  {
    [_progressPieView setState:ProgressPieStateActivity];
    [_progressPieView setProgress:0];
  }

  if (downloading)
  {
    [_progressPieView setState:ProgressPieStateProgress];
    [_progressPieView setProgress:_systemModel.portfolioExport.info.progress];
  }
}


@end


#pragma mark - UIImagePickerController

@implementation PortfolioViewController (UIImagePickerControllerDelegate)

-(void) dismissImagePicker
{
  if (self.mezzanineViewController.presentedViewController)
    [self.mezzanineViewController dismissViewControllerAnimated:YES completion:nil];
  
  _imagePicker = nil;
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
  [self dismissImagePicker];
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
  UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
  if ([self canUploadImage:image])
  {
    NSURL *referenceURL = [info valueForKey:UIImagePickerControllerReferenceURL];
    NSString *extension = [referenceURL pathExtension];
    
    // Image upload mechanism currently only supports jpeg and png
    // N.B. extension is nil in cases where user takes a photo using the camera, thus a default fallback is necessary.
    NSString *format = @"jpg";
    if ([extension isEqual:@"PNG"])
      format = @"png";

    // As mentioned above referenceURL is nil if this is a photo from the camera, thus need a fallback
    NSString *filename = referenceURL ? [referenceURL lastPathComponent] :
                                        [NSString stringWithFormat:@"Camera Capture %@.jpg", [NSDate date]];
    
    
    [self requestUploadImage:image filename:filename format:format];
  }
  else
  {
    // The iDevice camera is creating images that are larger than Mezzanine allows, we should make something about it.
    NSLog(@"This device takes pictures that are bigger than what Mezzanines allows.");
  }

  // The one case where image picker shouldn't be dismissed automatically after
  // an image is picked is on the iPad and the image was selected from the photo
  // library. This is to allow users to upload multiple images quickly.
  BOOL dismissImagePicker = !(isRegularWidth && (picker.sourceType == UIImagePickerControllerSourceTypePhotoLibrary));
  if (dismissImagePicker)
    [self dismissImagePicker];
}

@end
