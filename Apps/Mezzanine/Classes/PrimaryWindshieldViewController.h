//
//  PrimaryWindshieldViewController.h
//  Mezzanine
//
//  Created by miguel on 6/3/15.
//  Copyright (c) 2015 Oblong Industries. All rights reserved.
//

#import "WindshieldViewController.h"

@interface PrimaryWindshieldViewController : WindshieldViewController

-(void) showPlaceholderView:(BOOL)show animated:(BOOL)animated;
-(void) refreshLayout;
@property (nonatomic, assign) CGRect shielderContainerFrame; // Can be refactored together with PresentationViewController to calculate Feld frames

@end
