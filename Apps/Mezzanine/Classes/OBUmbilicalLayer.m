//
//  OBUmbilicalLayer.m
//  Mezzanine
//
//  Created by Zai Chang on 3/13/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import "OBUmbilicalLayer.h"

@implementation OBUmbilicalLayer

@synthesize sourceLayer;
@synthesize targetLayer;

@synthesize amplitude;
@synthesize phase;


static CGFloat dotRadius = 20.0;


-(void) setupLayer
{
  self.strokeColor = [UIColor colorWithWhite:1.0 alpha:0.33].CGColor;
  self.fillColor = [UIColor clearColor].CGColor;
  self.lineWidth = 4.0;
  self.lineCap = kCALineCapRound;
  self.lineDashPattern = @[@30.0,
                          @20.0];
  self.lineDashPhase = 0.0;
  self.zPosition = 8.0;
  
  originDotLayer = [CAShapeLayer layer];
  CGPathRef path = CGPathCreateWithEllipseInRect(CGRectMake(-dotRadius, -dotRadius, dotRadius * 2, dotRadius * 2), nil);
  originDotLayer.path = path;
  CGPathRelease(path);
  originDotLayer.fillColor = [UIColor colorWithWhite:1.0 alpha:0.33].CGColor;
  [self addSublayer:originDotLayer];
}


-(id) init
{
  self = [super init];
  if (self)
  {
    self.amplitude = 10.0;
    self.phase = 0.0;
    
    [self setupLayer];    
  }
  return self;
}


-(id) initWithLayer:(id)layer
{
  self = [super initWithLayer:layer];
  if (self)
  {
    if([layer isKindOfClass:[OBUmbilicalLayer class]])
    {
      OBUmbilicalLayer *umbilicalLayer = (OBUmbilicalLayer*)layer;
      self.amplitude = umbilicalLayer.amplitude;
      self.phase = umbilicalLayer.phase;      
      [self setupLayer];
    }
  }
  return self;
}




-(void) beginPhaseTimer
{
  phaseTimer = [NSTimer scheduledTimerWithTimeInterval:0.03 target:self selector:@selector(handlePhaseTimer:) userInfo:nil repeats:YES];
}


-(void) stopPhaseTimer
{
  if (phaseTimer)
  {
    [phaseTimer invalidate];
    phaseTimer = nil;
  }
}


-(void) handlePhaseTimer:(NSTimer*)timer
{
  phase += 0.06;
  
  [self setNeedsLayout];
}


+(id) defaultValueForKey:(NSString *)key
{
  if ([key isEqualToString:@"phase"]) 
    return @0.0;
  else if ([key isEqualToString:@"amplitude"]) 
    return @100.0;

  return [super defaultValueForKey:key];
}


+(BOOL) needsDisplayForKey:(NSString*)key 
{
  if ([key isEqualToString:@"phase"])
    return YES;
  else if ([key isEqualToString:@"amplitude"]) 
    return YES;

  return [super needsDisplayForKey:key];
}

+(id<CAAction>) defaultActionForKey:(NSString *)key
{
  if ([key isEqualToString:@"phase"]) 
    return [CABasicAnimation animationWithKeyPath:@"phase"];
  else if ([key isEqualToString:@"amplitude"]) 
    return [CABasicAnimation animationWithKeyPath:@"amplitude"];

  return [super defaultActionForKey:key];
}


-(void) layoutSublayers
{
  [super layoutSublayers];
  
  CGRect sourceFrame = sourceLayer.frame;
  CGPoint sourceCenter = CGPointMake(CGRectGetMidX(sourceFrame), CGRectGetMidY(sourceFrame));
  
  CGRect targetFrame = targetLayer.frame;
  CGPoint targetCenter = CGPointMake(CGRectGetMidX(targetFrame), CGRectGetMidY(targetFrame));
  
  
  // Adjust the dot
  [CATransaction begin];
  [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
  originDotLayer.frame = CGRectMake(sourceCenter.x - CGRectGetMidX(self.frame),
                                    sourceCenter.y - CGRectGetMidY(self.frame),
                                    2 * dotRadius,
                                    2 * dotRadius);
  [CATransaction commit];
    
  
  // Adjust the line
  CGPoint vector = CGPointMake(targetCenter.x - sourceCenter.x, targetCenter.y - sourceCenter.y);
    
  CGMutablePathRef path = CGPathCreateMutable();
  
  CGPathMoveToPoint(path, nil, sourceCenter.x, sourceCenter.y);
  
  CGPoint (^pointAtState)(CGFloat state) = ^(CGFloat state) {
    CGFloat x = sourceCenter.x + vector.x * state;
    CGFloat y = sourceCenter.y - amplitude * sin(state * 2 * M_PI + phase * M_PI_2);
    return CGPointMake(x, y);
  };
  
  CGPoint controlPoint1 = pointAtState(0.33);
  CGPoint controlPoint2 = pointAtState(0.66);
  
  CGPathAddCurveToPoint(path, nil, controlPoint1.x, controlPoint1.y, controlPoint2.x, controlPoint2.y, targetCenter.x, targetCenter.y);
  
  self.path = path;
  
  CGPathRelease(path);
}


@end
