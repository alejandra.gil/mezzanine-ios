//
//  MZSystemModel+InterfaceTesting.m
//  Mezzanine
//
//  Created by Zai Chang on 1/14/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import "MZSystemModel+InterfaceTesting.h"
#import "MezzanineStyleSheet.h"
#import "MZWorkspace.h"


@implementation MZSystemModel (InterfaceTesting)

-(void) loadTestFeldSingle
{
  MZFeld *feld = [MZFeld feldWithName:@"main"];
  feld.pixelSize = [MZVect integerVectWithX:1920 y:1080];
  feld.physicalSize = [MZVect doubleVectWithX:1920 y:1080];
  feld.center = [MZVect doubleVectWithX:960 y:540 z:0];
  feld.over = [MZVect doubleVectWithX:1.0 y:0.0 z:0.0];
  feld.up = [MZVect doubleVectWithX:0.0 y:1.0 z:0.0];
  feld.norm = [MZVect doubleVectWithX:0.0 y:1.0 z:-1.0];

  self.felds = [NSMutableArray arrayWithObjects:feld, nil];

  MZFeld *boundingFeld = [MZFeld feldWithName:@"bounding-feld"];
  boundingFeld.pixelSize = [MZVect integerVectWithX:1920 y:1080];
  boundingFeld.physicalSize = [MZVect doubleVectWithX:1920 y:1080];

  MZSurface *mainSurface = [[MZSurface alloc] initWithName:@"main"];
  mainSurface.felds = self.felds;
  mainSurface.boundingFeld = boundingFeld;

  self.surfaces = @[mainSurface].mutableCopy;
}


-(void) loadTestFeldsDouble
{
  MZFeld *mainFeld = [MZFeld feldWithName:@"main"];
  mainFeld.pixelSize = [MZVect integerVectWithX:1920 y:1080];
  mainFeld.physicalSize = [MZVect doubleVectWithX:1920 y:1080];
  mainFeld.center = [MZVect doubleVectWithX:960 y:540 z:0];
  mainFeld.over = [MZVect doubleVectWithX:1.0 y:0.0 z:0.0];
  mainFeld.up = [MZVect doubleVectWithX:0.0 y:1.0 z:0.0];
  mainFeld.norm = [MZVect doubleVectWithX:0.0 y:1.0 z:-1.0];


  MZFeld *leftFeld = [MZFeld feldWithName:@"left"];
  leftFeld.pixelSize = [MZVect integerVectWithX:1920 y:1080];
  leftFeld.physicalSize = [MZVect doubleVectWithX:1920 y:1080];
  leftFeld.center = [MZVect doubleVectWithX:-980 y:540 z:0];
  leftFeld.over = [MZVect doubleVectWithX:1.0 y:0.0 z:0.0];
  leftFeld.up = [MZVect doubleVectWithX:0.0 y:1.0 z:0.0];
  leftFeld.norm = [MZVect doubleVectWithX:0.0 y:1.0 z:-1.0];

  self.felds = [NSMutableArray arrayWithObjects:leftFeld, mainFeld, nil];

  MZFeld *boundingFeld = [MZFeld feldWithName:@"bounding-feld"];
  boundingFeld.pixelSize = [MZVect integerVectWithX:3860 y:1080];
  boundingFeld.physicalSize = [MZVect doubleVectWithX:3860 y:1080];

  MZSurface *mainSurface = [[MZSurface alloc] initWithName:@"main"];
  mainSurface.felds = self.felds;
  mainSurface.boundingFeld = boundingFeld;

  self.surfaces = @[mainSurface].mutableCopy;
}


-(void) loadTestFeldsTriple
{
  MZFeld *mainFeld = [MZFeld feldWithName:@"main"];
  mainFeld.pixelSize = [MZVect integerVectWithX:1920 y:1080];
  mainFeld.physicalSize = [MZVect doubleVectWithX:1920 y:1080];
  mainFeld.center = [MZVect doubleVectWithX:960 y:540 z:0];
  mainFeld.over = [MZVect doubleVectWithX:1.0 y:0.0 z:0.0];
  mainFeld.up = [MZVect doubleVectWithX:0.0 y:1.0 z:0.0];
  mainFeld.norm = [MZVect doubleVectWithX:0.0 y:1.0 z:-1.0];

  MZFeld *leftFeld = [MZFeld feldWithName:@"left"];
  leftFeld.pixelSize = [MZVect integerVectWithX:1920 y:1080];
  leftFeld.physicalSize = [MZVect doubleVectWithX:1920 y:1080];
  leftFeld.center = [MZVect doubleVectWithX:-980 y:540 z:0];
  leftFeld.over = [MZVect doubleVectWithX:1.0 y:0.0 z:0.0];
  leftFeld.up = [MZVect doubleVectWithX:0.0 y:1.0 z:0.0];
  leftFeld.norm = [MZVect doubleVectWithX:0.0 y:1.0 z:-1.0];

  MZFeld *rightFeld = [MZFeld feldWithName:@"right"];
  rightFeld.pixelSize = [MZVect integerVectWithX:1920 y:1080];
  rightFeld.physicalSize = [MZVect doubleVectWithX:1920 y:1080];
  rightFeld.center = [MZVect doubleVectWithX:2900 y:540 z:0];
  rightFeld.over = [MZVect doubleVectWithX:1.0 y:0.0 z:0.0];
  rightFeld.up = [MZVect doubleVectWithX:0.0 y:1.0 z:0.0];
  rightFeld.norm = [MZVect doubleVectWithX:0.0 y:1.0 z:-1.0];

  self.felds = [NSMutableArray arrayWithObjects:leftFeld, mainFeld, rightFeld, nil];

  MZFeld *boundingFeld = [MZFeld feldWithName:@"bounding-feld"];
  boundingFeld.pixelSize = [MZVect integerVectWithX:5800 y:1080];
  boundingFeld.physicalSize = [MZVect doubleVectWithX:5800 y:1080];

  MZSurface *mainSurface = [[MZSurface alloc] initWithName:@"main"];
  mainSurface.felds = self.felds;
  mainSurface.boundingFeld = boundingFeld;

  self.surfaces = @[mainSurface].mutableCopy;
}

-(void) loadTestFeldsThreeByTwo
{
  MZFeld *topMainFeld = [MZFeld feldWithName:@"top-main"];
  topMainFeld.pixelSize = [MZVect integerVectWithX:1920 y:1080];
  topMainFeld.physicalSize = [MZVect doubleVectWithX:1920 y:1080];
  topMainFeld.center = [MZVect doubleVectWithX:960 y:540 z:0];
  topMainFeld.over = [MZVect doubleVectWithX:1.0 y:0.0 z:0.0];
  topMainFeld.up = [MZVect doubleVectWithX:0.0 y:1.0 z:0.0];
  topMainFeld.norm = [MZVect doubleVectWithX:0.0 y:1.0 z:-1.0];

  MZFeld *topLeftFeld = [MZFeld feldWithName:@"top-left"];
  topLeftFeld.pixelSize = [MZVect integerVectWithX:1920 y:1080];
  topLeftFeld.physicalSize = [MZVect doubleVectWithX:1920 y:1080];
  topLeftFeld.center = [MZVect doubleVectWithX:-980 y:540 z:0];
  topLeftFeld.over = [MZVect doubleVectWithX:1.0 y:0.0 z:0.0];
  topLeftFeld.up = [MZVect doubleVectWithX:0.0 y:1.0 z:0.0];
  topLeftFeld.norm = [MZVect doubleVectWithX:0.0 y:1.0 z:-1.0];

  MZFeld *topRightFeld = [MZFeld feldWithName:@"top-right"];
  topRightFeld.pixelSize = [MZVect integerVectWithX:1920 y:1080];
  topRightFeld.physicalSize = [MZVect doubleVectWithX:1920 y:1080];
  topRightFeld.center = [MZVect doubleVectWithX:2900 y:540 z:0];
  topRightFeld.over = [MZVect doubleVectWithX:1.0 y:0.0 z:0.0];
  topRightFeld.up = [MZVect doubleVectWithX:0.0 y:1.0 z:0.0];
  topRightFeld.norm = [MZVect doubleVectWithX:0.0 y:1.0 z:-1.0];

  MZFeld *mainFeld = [MZFeld feldWithName:@"main"];
  mainFeld.pixelSize = [MZVect integerVectWithX:1920 y:1080];
  mainFeld.physicalSize = [MZVect doubleVectWithX:1920 y:1080];
  mainFeld.center = [MZVect doubleVectWithX:960 y:540 z:0];
  mainFeld.over = [MZVect doubleVectWithX:1.0 y:0.0 z:0.0];
  mainFeld.up = [MZVect doubleVectWithX:0.0 y:1.0 z:0.0];
  mainFeld.norm = [MZVect doubleVectWithX:0.0 y:1.0 z:-1.0];

  MZFeld *leftFeld = [MZFeld feldWithName:@"left"];
  leftFeld.pixelSize = [MZVect integerVectWithX:1920 y:1080];
  leftFeld.physicalSize = [MZVect doubleVectWithX:1920 y:1080];
  leftFeld.center = [MZVect doubleVectWithX:-980 y:540 z:0];
  leftFeld.over = [MZVect doubleVectWithX:1.0 y:0.0 z:0.0];
  leftFeld.up = [MZVect doubleVectWithX:0.0 y:1.0 z:0.0];
  leftFeld.norm = [MZVect doubleVectWithX:0.0 y:1.0 z:-1.0];

  MZFeld *rightFeld = [MZFeld feldWithName:@"right"];
  rightFeld.pixelSize = [MZVect integerVectWithX:1920 y:1080];
  rightFeld.physicalSize = [MZVect doubleVectWithX:1920 y:1080];
  rightFeld.center = [MZVect doubleVectWithX:2900 y:540 z:0];
  rightFeld.over = [MZVect doubleVectWithX:1.0 y:0.0 z:0.0];
  rightFeld.up = [MZVect doubleVectWithX:0.0 y:1.0 z:0.0];
  rightFeld.norm = [MZVect doubleVectWithX:0.0 y:1.0 z:-1.0];

  self.felds = [NSMutableArray arrayWithObjects:topLeftFeld, topMainFeld, topRightFeld, leftFeld, mainFeld, rightFeld, nil];

  MZFeld *boundingFeld = [MZFeld feldWithName:@"bounding-feld"];
  boundingFeld.pixelSize = [MZVect integerVectWithX:5800 y:2180];
  boundingFeld.physicalSize = [MZVect doubleVectWithX:5800 y:2180];

  MZSurface *mainSurface = [[MZSurface alloc] initWithName:@"main"];
  mainSurface.felds = self.felds;
  mainSurface.boundingFeld = boundingFeld;

  self.surfaces = @[mainSurface].mutableCopy;
}


-(void) loadWhiteboards:(NSInteger)numberOfWhiteboards
{
  NSMutableArray *testWhiteboards = [NSMutableArray array];
  for (NSInteger i=0; i<numberOfWhiteboards; i++)
  {
//    MZWhiteboard *whiteboard = [[MZWhiteboard alloc] init];
    NSDictionary *whiteboard = @{@"uid": [NSString stringWithFormat:@"ws-%ld", (long)i],
                                 @"display-name": [NSString stringWithFormat:@"Whiteboard %ld", (long)i]};
    [testWhiteboards addObject:whiteboard];
  }
  self.whiteboards = testWhiteboards;
}


// Load some fake system information
-(void) loadTestCommonBase
{
  self.testModeEnabled = YES;
  
  self.myMezzanine = [[MZMezzanine alloc] init];
  self.myMezzanine.uid = @"123456789";
  self.myMezzanine.name = @"Interface Testing";
  self.myMezzanine.company = @"Oblong";
  self.myMezzanine.location = @"Barcelona";

  self.remoteMezzes = [[NSMutableArray alloc] init];
  MZRemoteMezz *remoteMezz = [[MZRemoteMezz alloc] init];
  remoteMezz.uid = @"Mezz Test 1";
  remoteMezz.name = @"Mezz Test 1";
  remoteMezz.company = @"Oblong";
  remoteMezz.location = @"Barcelona";
  remoteMezz.online = YES;
  remoteMezz.collaborationState = MZRemoteMezzCollaborationStateNotInCollaboration;
  [self.remoteMezzes addObject:remoteMezz];

  remoteMezz = [[MZRemoteMezz alloc] init];
  remoteMezz.uid = @"Mezz Test 2";
  remoteMezz.name = @"Mezz Test 2";
  remoteMezz.company = @"Oblong";
  remoteMezz.location = @"Barcelona";
  remoteMezz.online = NO;
  remoteMezz.collaborationState = MZRemoteMezzCollaborationStateNotInCollaboration;
  [self.remoteMezzes addObject:remoteMezz];
  
  remoteMezz = [[MZRemoteMezz alloc] init];
  remoteMezz.uid = @"Mezz Test 3";
  remoteMezz.name = @"Mezz Test 3";
  remoteMezz.company = @"Oblong";
  remoteMezz.location = @"Los Angeles";
  remoteMezz.online = NO;
  remoteMezz.collaborationState = MZRemoteMezzCollaborationStateNotInCollaboration;
  [self.remoteMezzes addObject:remoteMezz];
  
  remoteMezz = [[MZRemoteMezz alloc] init];
  remoteMezz.uid = @"Mezz Test 4";
  remoteMezz.name = @"Mezz Test 4";
  remoteMezz.company = @"Oblong";
  remoteMezz.location = @"New York City";
  remoteMezz.online = YES;
  remoteMezz.collaborationState = MZRemoteMezzCollaborationStateNotInCollaboration;
  [self.remoteMezzes addObject:remoteMezz];

  self.infopresence.infopresenceInstalled = YES;
  self.infopresence.status = MZInfopresenceStatusActive;

  self.uploadLimits.maxImageSizeInMB = @(50);
  self.uploadLimits.maxImageSizeInMP = @(50);
  self.uploadLimits.maxImageWidth = @(6000);
  self.uploadLimits.maxImageHeight = @(3300);
  self.uploadLimits.maxGlobalSizeInMB = @(500);
  self.uploadLimits.maxPDFSizeInMB = @(500);
}


-(NSArray*) testImagePaths
{
  NSBundle *mainBundle = [NSBundle mainBundle];
  NSArray *imagePaths = [mainBundle pathsForResourcesOfType:@"png" inDirectory:nil];
  return imagePaths;
}


-(void) loadTestWindshield:(MZWindshield*) windshield
{
  CGFloat feldAspectRatio = [(MZFeld*)self.felds[0] aspectRatio];

  NSArray *testImagePaths = [self testImagePaths];
  //NSInteger numberOfImages = testImagePaths.count;
  NSInteger numberOfImages = 5;
  for (NSInteger i=0; i<numberOfImages; i++)
  {
    MZWindshieldItem *item = [[MZWindshieldItem alloc] init];

    item.uid = [NSString stringWithFormat:@"ws-000%ld", (long)i];
    item.assetUid = [NSString stringWithFormat:@"as-000%ld", (long)i];
    NSInteger index = (testImagePaths.count - 1) * drand48();
    item.thumbURL = testImagePaths[index];
    item.fullURL = item.thumbURL;
    item.surface = self.surfaces[0];
    item.feld = self.felds[0];
    
    CGFloat x = (CGFloat)i * 0.1;
    CGFloat y = x * feldAspectRatio;
    CGFloat width = 0.33 / self.felds.count;
    CGFloat height = 1.0 * feldAspectRatio;
    item.frame = CGRectMake(x, y, width, height);
    
    [windshield insertObject:item inItemsAtIndex:i];
  }
}

-(void) loadWorkspaces
{
  for (NSInteger i=0; i<5; i++)
  {
    MZWorkspace *testWorkspace = [[MZWorkspace alloc] init];
    testWorkspace.uid = @"ds-08002";
    testWorkspace.name = @"Test Workspace";
    testWorkspace.modifiedDate = [NSDate date];
    [self.workspaces addObject:testWorkspace];
  }
}


-(void) loadTestWorkspace
{
  [self loadTestCommonBase];
  self.myMezzanine.version = @"3.10";
  self.infopresence.infopresenceInstalled = YES;
  self.infopresence.status = MZInfopresenceStatusActive;
	[MezzanineStyleSheet sharedStyleSheet].version = self.myMezzanine.version;
  
  MZWorkspace *testWorkspace = [[MZWorkspace alloc] init];
  testWorkspace.uid = @"ds-08002";
  testWorkspace.name = @"Test Workspace";
  testWorkspace.modifiedDate = [NSDate date];
  //testWorkspace.saved = NO;
  
  NSArray *testImagePaths = [self testImagePaths];
  NSInteger numberOfSlides = testImagePaths.count;
  NSMutableArray *slides = [NSMutableArray array];
  for (NSInteger i=0; i<numberOfSlides; i++)
  {
    MZPortfolioItem *slide = [[MZPortfolioItem alloc] init];
    slide.uid = [NSString stringWithFormat:@"sl-000%ld", (long)i];
    slide.index = i;

    NSInteger index = (testImagePaths.count - 1) * drand48();
    slide.thumbURL = testImagePaths[index];
    slide.fullURL = slide.thumbURL;
    slide.imageAvailable = YES;

    [slides addObject:slide];
  }
  testWorkspace.presentation.slides = slides;
  [testWorkspace.presentation updateSlideIndexes];
  
  
  [self loadTestWindshield:testWorkspace.windshield];

  NSInteger numberOfStreams = 4;
  for (NSInteger i=0; i<numberOfStreams; i++)
  {
    MZLiveStream *liveStream = [[MZLiveStream alloc] init];
    liveStream.active = YES;  // Otherwise it won't show up in the bin
    liveStream.displayInfo = @{@"display-name": [NSString stringWithFormat:@"Stream %ld", (long)i]};
    [testWorkspace.liveStreams addObject:liveStream];
  }


  [[NSOperationQueue mainQueue] addOperationWithBlock:^{
    self.currentWorkspace = testWorkspace;
    self.state = MZSystemStateWorkspace;
  }];
}


@end
