//
//  DocumentUploadInfoViewController.m
//  Mezzanine
//
//  Created by Zai Chang on 5/14/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import "DocumentUploadInfoViewController.h"

@interface DocumentUploadInfoViewController ()

@end

@implementation DocumentUploadInfoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
  [super viewDidLoad];

  NSString *title = NSLocalizedString(@"PDF Upload Info Title", @"Uploading a PDF");
  self.titleLabel.text = title;

  NSString *description = NSLocalizedString(@"PDF Upload Info Description", @"Uploading a PDF is difficult");
  self.textView.text = description;
}


- (IBAction)close:(id)sender
{
  [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}


@end
