//
//  VideoPlaceholderView.m
//  Mezzanine
//
//  Created by miguel on 17/07/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import "VideoPlaceholderView.h"
#import "MezzanineStyleSheet.h"
#import "CornerBorderView.h"


@interface VideoPlaceholderView ()

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *subtitleLabel;
@property (nonatomic, strong) CornerBorderView *cornerViewTopLeft;
@property (nonatomic, strong) CornerBorderView *cornerViewTopRight;
@property (nonatomic, strong) CornerBorderView *cornerViewBottomLeft;
@property (nonatomic, strong) CornerBorderView *cornerViewBottomRight;

@end

@implementation VideoPlaceholderView

@synthesize sourceName;
@synthesize sourceOrigin;

@synthesize maxTitleFontSize;
@synthesize minTitleFontSize;
@synthesize minSubtitleFontSize;

@synthesize titleLabel;
@synthesize subtitleLabel;
@synthesize cornerViewTopLeft;
@synthesize cornerViewTopRight;
@synthesize cornerViewBottomLeft;
@synthesize cornerViewBottomRight;


- (id)initWithCoder:(NSCoder *)aDecoder
{
  self = [super initWithCoder:aDecoder];
  if (self)
  {
    [self initialize];
  }
  return self;
}


- (id)initWithFrame:(CGRect)frame
{
  self = [super initWithFrame:frame];
  if (self)
  {
    [self initialize];
  }
  return self;
}


- (void) initialize
{
  maxTitleFontSize = 48.0;
  minTitleFontSize = 16.0;
  minSubtitleFontSize = 16.0;

  MezzanineStyleSheet *styleSheet = [MezzanineStyleSheet sharedStyleSheet];

  self.backgroundColor = styleSheet.videoPlaceholderBackgroundColor;

  titleLabel = [UILabel new];
  titleLabel.font = [UIFont dinOfSize:48.0];
  titleLabel.textAlignment = NSTextAlignmentCenter;
  titleLabel.textColor = styleSheet.videoPlaceholderPrimaryColor;
  titleLabel.baselineAdjustment = UIBaselineAdjustmentAlignBaselines;

  subtitleLabel = [UILabel new];
  subtitleLabel.font = [UIFont dinOfSize:48.0];
  subtitleLabel.textAlignment = NSTextAlignmentCenter;
  subtitleLabel.textColor = styleSheet.videoPlaceholderSecondaryColor;
  subtitleLabel.baselineAdjustment = UIBaselineAdjustmentAlignBaselines;

  cornerViewTopLeft = [CornerBorderView new];
  cornerViewTopLeft.cornerType = CornerBorderViewTypeTopLeft;
  cornerViewTopRight = [CornerBorderView new];
  cornerViewTopRight.cornerType = CornerBorderViewTypeTopRight;
  cornerViewBottomLeft = [CornerBorderView new];
  cornerViewBottomLeft.cornerType = CornerBorderViewTypeBottomLeft;
  cornerViewBottomRight = [CornerBorderView new];
  cornerViewBottomRight.cornerType = CornerBorderViewTypeBottomRight;

  [self addSubview:cornerViewTopLeft];
  [self addSubview:cornerViewTopRight];
  [self addSubview:cornerViewBottomLeft];
  [self addSubview:cornerViewBottomRight];

  [self addSubview:titleLabel];
  [self addSubview:subtitleLabel];
}


-(void) setSourceName:(NSString*)name
{
  if (sourceName != name)
  {
    sourceName = name;
    titleLabel.text = sourceName ? sourceName : NSLocalizedString(@"Video Placeholder Video Title", nil);

    [self setNeedsDisplay];
  }
}


-(void) setSourceOrigin:(NSString*)origin
{
  if (sourceOrigin != origin)
  {
    sourceOrigin = origin;
    subtitleLabel.text = origin;

    [self setNeedsDisplay];
  }
}


-(void) layoutSubviews
{
  CGFloat minimumDimension = MIN(MIN(self.bounds.size.width, self.bounds.size.height), 100.0);
  CGFloat margin = (NSInteger) (minimumDimension / 9.0);
  CGFloat length = (NSInteger) (minimumDimension / 9.0);

  cornerViewTopLeft.frame = CGRectMake(margin, margin, length, length);
  cornerViewTopRight.frame = CGRectMake(CGRectGetMaxX(self.frame) - 2.0 * margin, margin, length, length);
  cornerViewBottomLeft.frame = CGRectMake(margin, CGRectGetMaxY(self.frame) - 2.0 * margin, length, length);
  cornerViewBottomRight.frame = CGRectMake(CGRectGetMaxX(self.frame) - 2.0 * margin, CGRectGetMaxY(self.frame) - 2.0 * margin, length, length);

  [cornerViewTopLeft setNeedsDisplay];
  [cornerViewTopRight setNeedsDisplay];
  [cornerViewBottomLeft setNeedsDisplay];
  [cornerViewBottomRight setNeedsDisplay];

  CGFloat fontSize = (NSInteger) MAX(MIN(minimumDimension / 6.0, maxTitleFontSize), minTitleFontSize);

  UIFont *font = titleLabel.font;
  titleLabel.font = [UIFont fontWithName:font.fontName size:fontSize];

  CGFloat subtitleFontSize = (NSInteger) MAX(fontSize / 1.5, minSubtitleFontSize);
  font = subtitleLabel.font;
  subtitleLabel.font = [UIFont fontWithName:font.fontName size:subtitleFontSize];

  if (_isLocalVideo || !sourceOrigin || [sourceOrigin isEqualToString:@""])
  {
    CGRect frame = CGRectInset(self.bounds, margin, margin);
    titleLabel.frame = frame;
  }
  else
  {
    CGFloat paddingBetweenLines = (NSInteger) (subtitleFontSize / 8.0);

    CGFloat yPos = CGRectGetMidY(self.frame) - titleLabel.font.pointSize - 4.0 - paddingBetweenLines / 2.0;
    titleLabel.frame = CGRectMake((NSInteger) margin, (NSInteger) yPos, (NSInteger) (self.frame.size.width - 2.0 * margin), (NSInteger) (titleLabel.font.pointSize * 1.3));
    yPos = CGRectGetMaxY(titleLabel.frame) + paddingBetweenLines;
    subtitleLabel.frame = CGRectMake((NSInteger) margin, (NSInteger) yPos, (NSInteger) (self.frame.size.width - 2.0 * margin), (NSInteger) (subtitleLabel.font.pointSize * 1.3));
  }
}

@end
