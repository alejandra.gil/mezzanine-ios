//
//  AcknowledgementsViewController.m
//  Mezzanine
//
//  Created by Zai Chang on 7/25/13.
//  Copyright (c) 2013 Oblong Industries. All rights reserved.
//

#import "AcknowledgementsViewController.h"
#import "MezzanineStyleSheet.h"


@interface AcknowledgementsViewController ()

@end

@implementation AcknowledgementsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
  [super viewDidLoad];

  MezzanineStyleSheet *styleSheet = [MezzanineStyleSheet sharedStyleSheet];
  self.view.backgroundColor = styleSheet.darkerFillColor;
  
  textView = [[UITextView alloc] initWithFrame:self.view.bounds];
  textView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
  textView.backgroundColor = styleSheet.darkerFillColor;
  textView.textColor = styleSheet.textNormalColor;
  textView.font = [UIFont dinMediumOfSize:13.0];
  textView.editable = NO;
  [self.view addSubview:textView];
  
  NSString *acknowledgementsFile = [[NSBundle mainBundle] pathForResource:@"Acknowledgements" ofType:@"txt"];
  if (acknowledgementsFile)
  {
    NSString *contents = [NSString stringWithContentsOfFile:acknowledgementsFile encoding:NSUTF8StringEncoding error:nil];
    if (contents)
      textView.text = contents;
  }

	self.navigationItem.title = NSLocalizedString(@"Acknowledgements Title", nil) ;
  self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(done:)];
}

- (void) viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
  [textView setContentOffset:CGPointMake(0.0, 0.0) animated:NO];
}


-(BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
  return YES;
}


-(IBAction) done:(id)sender
{
  [self dismissViewControllerAnimated:YES completion:nil];
}

@end
