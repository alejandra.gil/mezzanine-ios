//
//  WorkspaceGeometry.swift
//  Mezzanine
//
//  Created by miguel on 2/3/17.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import UIKit

class WorkspaceGeometry: NSObject {

  @objc var surfaces: [MZSurface]? {
    didSet {
      updateWorkspaceGeometry()
    }
  }

  var containerRect: CGRect? {
    didSet {
      updateWorkspaceGeometry()
    }
  }

  @objc func surfaceBorderHorizontalMargin() -> CGFloat {
    return surfaceGeometry.borderHorizontalMargin
  }

  fileprivate var surfaceGeometry = SurfaceGeometry()
  fileprivate var feldGeometries = Array<FeldGeometry>()

  fileprivate func updateWorkspaceGeometry() {

    guard let surfaces = surfaces else { return }
    surfaceGeometry.containerRect = self.containerRect
    surfaceGeometry.surfaces = surfaces

    for surface in surfaces {

      var feldGeometry = feldGeometries.filter{ $0.surface == surface }.first

      if feldGeometry == nil {
        feldGeometry = FeldGeometry()
        feldGeometry!.surface = surface
        feldGeometries.append(feldGeometry!)
      }

      feldGeometry!.containerRect = surfaceGeometry.rectForSurface(surface)
    }
  }


  // MARK: var setters for Obj-C classes
  @objc func containerRectSetter(_ containerRect: CGRect) {
    self.containerRect = containerRect
  }


  // MARK: Coordinate Conversion Helper Methods

  fileprivate func rectOfSurfaceInWorkspaceCoordinates(_ rect: CGRect, surface: MZSurface) -> CGRect {
    let surfaceRect = rectForSurface(surface)
    return rect.offsetBy(dx: surfaceRect.minX, dy: surfaceRect.minY)
  }

  fileprivate func rectOfWorkspaceInSurfaceCoordinates(_ rect: CGRect, surface: MZSurface) -> CGRect {
    let surfaceRect = rectForSurface(surface)
    return rect.offsetBy(dx: -surfaceRect.minX, dy: -surfaceRect.minY)
  }

  fileprivate func pointOfWorkspaceInSurfaceCoordinates(_ point: CGPoint, surface: MZSurface) -> CGPoint {
    let surfaceRect = rectForSurface(surface)
    return CGPoint(x: point.x - surfaceRect.minX, y: point.y - surfaceRect.minY)
  }

  func roundedFrameForWorkspace() -> CGRect {

    return rectForWorkspace().roundedFrame()
  }

  func rectForWorkspace() -> CGRect {

    guard let surfaces = surfaces else { return CGRect.zero }

    let firstSurfaceRect = surfaceGeometry.rectForSurface(surfaces.first!)
    let lastSurfaceRect = surfaceGeometry.rectForSurface(surfaces.last!)

    let x = firstSurfaceRect.minX
    let y = min(firstSurfaceRect.minY, lastSurfaceRect.minY)
    let width = lastSurfaceRect.maxX - firstSurfaceRect.minX
    let height = max(firstSurfaceRect.maxY, lastSurfaceRect.maxY) - y

    return CGRect(x: x, y: y, width: width, height: height)
  }

  func rectForSmallestFeld() -> CGRect {

    guard let surfaces = surfaces else { return CGRect.zero }
    guard let containerRect = containerRect else { return CGRect.zero }

    var minRect = containerRect

    for surface in surfaces {
      for feld in surface.felds {

        let rectForCurrentFeld: CGRect = rectForFeld(feld as! MZFeld)
        if rectForCurrentFeld.size.width < minRect.size.width && rectForCurrentFeld.size.height < minRect.size.height {
          minRect = rectForCurrentFeld
        }
      }
    }

    return minRect
  }

  func rectForVisibleFelds(felds: Array<MZFeld>, surface: MZSurface) -> CGRect {

    var x: CGFloat = CGFloat.greatestFiniteMagnitude
    var y: CGFloat = CGFloat.greatestFiniteMagnitude
    var w: CGFloat = 0.0
    var h: CGFloat = 0.0

    for feld in felds {
      let feldRect = rectForFeld(feld)
      x = min(x, feldRect.minX)
      y = min(y, feldRect.minY)
      w = w + feldRect.width
      h = feldRect.height
    }

    return CGRect(x: x, y: y, width: w  + CGFloat(felds.count - 1) * feldMarginInSurface(surface), height: h)
  }

  func rectForVisibleToAllFelds(surface: MZSurface) -> CGRect {
    var visibleFelds = Array<MZFeld>()

    for feld in surface.felds {

      let feld = feld as! MZFeld
      if feld.visibility != nil && feld.visibility == "all" {
        visibleFelds.append(feld)
      }
      else if feld.visibility == nil && feld.visibleToAll == true {
        visibleFelds.append(feld)
      }
    }

    return rectForVisibleFelds(felds: visibleFelds, surface: surface)
  }


  func minFeldInscribedInRectangle(_ rectangle: CGSize) -> MZFeld? {

    guard let surfaces = surfaces else { return nil }

    // Another way to calculate the minFeld to investigate

//    let containerRect = CGRect(origin: CGPointZero, size: rectangle)
//    var minArea:CGFloat = CGFloat(MAXFLOAT)
//    var minFeldInscribed: MZFeld?
//
//    for surface in surfaces {
//      for feld in surface.felds {
//        var currentFeldRect = rectForFeld(feld as! MZFeld)
//        currentFeldRect.origin = CGPointZero
//        let feldArea = currentFeldRect.size.width * currentFeldRect.size.height
//        if containerRect.contains(currentFeldRect) && feldArea < minArea  {
//          minArea = feldArea
//          minFeldInscribed = feld as? MZFeld
//        }
//      }
//    }

    var maxScale:CGFloat = 0
    var minFeldInscribed: MZFeld?

    for surface in surfaces {
      for feld in surface.felds {
        let currentFeldRect = rectForFeld(feld as! MZFeld)
        let scale = rectangle.aspectRatio() > currentFeldRect.aspectRatio() ?
          rectangle.height / currentFeldRect.height : rectangle.width / currentFeldRect.width
        if scale > maxScale {
          maxScale = scale
          minFeldInscribed = feld as? MZFeld
        }
      }
    }
    return minFeldInscribed
  }


  func maxFeldInscribedInRectangle(_ rectangle: CGSize) -> MZFeld? {

    guard let surfaces = surfaces else { return nil }

    var minScale:CGFloat = CGFloat(MAXFLOAT)
    var maxFeldInscribed: MZFeld?

    for surface in surfaces {
      for feld in surface.felds {
        let currentFeldRect = rectForFeld(feld as! MZFeld)
        let scale = rectangle.aspectRatio() > currentFeldRect.aspectRatio() ?
          rectangle.height / currentFeldRect.height : rectangle.width / currentFeldRect.width
        if minScale > scale {
          minScale = scale
          maxFeldInscribed = feld as? MZFeld
        }
      }
    }
    return maxFeldInscribed
  }

  func maxSurfaceInscribedInRectangle(_ rectangle: CGSize) -> MZSurface? {

    guard let surfaces = surfaces else { return nil }

    var minScale:CGFloat = CGFloat(MAXFLOAT)
    var maxSurfaceInscribed: MZSurface?

    for surface in surfaces {
      let currentSurfaceRect = rectForSurface(surface)
      let scale = rectangle.aspectRatio() > currentSurfaceRect.aspectRatio() ?
        rectangle.height / currentSurfaceRect.height : rectangle.width / currentSurfaceRect.width
      if minScale > scale {
        minScale = scale
        maxSurfaceInscribed = surface
      }
    }
    return maxSurfaceInscribed
  }
}

extension WorkspaceGeometry: SurfaceGeometryProtocol {

  @objc func roundedFrameForSurface(_ surface: MZSurface) -> CGRect {

    return surfaceGeometry.roundedFrameForSurface(surface)
  }

  @objc func rectForSurface(_ surface: MZSurface) -> CGRect {

    return surfaceGeometry.rectForSurface(surface)
  }

  @objc func rectForExtendedSurface(_ surface: MZSurface) -> CGRect {

    return surfaceGeometry.rectForExtendedSurface(surface)
  }

  @objc func surfaceForLocation(_ location: CGPoint) -> MZSurface? {

    return surfaceGeometry.surfaceForLocation(location)
  }

  @objc func closestSurfaceForLocation(_ location: CGPoint) -> MZSurface? {
    
    return surfaceGeometry.closestSurfaceForLocation(location)
  }


  @objc func surfaceForLocationInExtendedArea(_ location: CGPoint) -> MZSurface? {

    return surfaceGeometry.surfaceForLocationInExtendedArea(location)
  }

  @objc func surfacesContainedInRect(_ rect: CGRect) -> Array<MZSurface>? {

    return surfaceGeometry.surfacesContainedInRect(rect)
  }

  @objc func surfacesIntersectingWithRect(_ rect: CGRect) -> Array<MZSurface>? {

    return surfaceGeometry.surfacesIntersectingWithRect(rect)
  }
}

extension WorkspaceGeometry: FeldGeometryProtocol {

  @objc func roundedFrameForFeld(_ feld: MZFeld) -> CGRect {

    return rectForFeld(feld).roundedFrame()
  }

  @objc func rectForFeld(_ feld: MZFeld) -> CGRect {

    var rect = CGRect.zero
    var surface: MZSurface
    for feldGeometry in feldGeometries {
      rect = feldGeometry.rectForFeld(feld)
      if rect != CGRect.zero {
        surface = feldGeometry.surface!
        return rectOfSurfaceInWorkspaceCoordinates(rect, surface: surface)
      }
    }
    return CGRect.zero
  }

  @objc func feldForLocation(_ location: CGPoint) -> MZFeld? {

    guard let surface = surfaceForLocation(location) else { return nil }
    let feldGeometry = feldGeometries.filter{ $0.surface == surface }.first
    let locationInSurfaceCoordinates = pointOfWorkspaceInSurfaceCoordinates(location,surface: surface)
    return feldGeometry?.feldForLocation(locationInSurfaceCoordinates)
  }

  @objc func closestFeldForLocation(_ location: CGPoint) -> MZFeld? {

    guard let surface = closestSurfaceForLocation(location) else { return nil }
    let feldGeometry = feldGeometries.filter{ $0.surface == surface }.first
    let locationInSurfaceCoordinates = pointOfWorkspaceInSurfaceCoordinates(location,surface: surface)
    return feldGeometry?.closestFeldForLocation(locationInSurfaceCoordinates)
  }

  @objc func feldsContainedInRect(_ rect: CGRect) -> Array<MZFeld>? {

    guard let surfacesVisible = surfacesIntersectingWithRect(rect) else { return nil }

    var feldsContained = Array<MZFeld>()
    for surface in surfacesVisible {

      let rectInSurfaceCoordinates = rectOfWorkspaceInSurfaceCoordinates(rect, surface: surface)
      let feldGeometry = feldGeometries.filter{ $0.surface == surface }.first
      guard let feldsContainedInSurface = feldGeometry?.feldsContainedInRect(rectInSurfaceCoordinates)  else { continue }
      feldsContained.append(contentsOf: feldsContainedInSurface)
    }

    return feldsContained.count > 0 ? feldsContained : nil
  }

  @objc func feldsIntersectingWithRect(_ rect: CGRect) -> Array<MZFeld>? {

    guard let surfacesVisible = surfacesIntersectingWithRect(rect) else { return nil }

    var feldsVisible = Array<MZFeld>()
    for surface in surfacesVisible {

      let rectInSurfaceCoordinates = rectOfWorkspaceInSurfaceCoordinates(rect, surface: surface)
      let feldGeometry = feldGeometries.filter{ $0.surface == surface }.first
      guard let feldsVisibleInSurface = feldGeometry?.feldsIntersectingWithRect(rectInSurfaceCoordinates)  else { continue }
      feldsVisible.append(contentsOf: feldsVisibleInSurface)
    }
    return feldsVisible.count > 0 ? feldsVisible : nil
  }
  
  @objc func feldMarginInSurface(_ surface: MZSurface) -> CGFloat {
    if surface.felds.count == 1 {
      return 0
    }

    let leftFeld = surface.felds[0] as! MZFeld
    let rightFeld = surface.felds[1] as! MZFeld

    return rectForFeld(rightFeld).minX - rectForFeld(leftFeld).maxX

  }
}
