//
//  PresentationViewController_Private.h
//  Mezzanine
//
//  Created by Miguel Sánchez Valdés on 28/10/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

#import "PresentationViewController.h"

@class DeckSliderView;

@interface PresentationViewController ()
{
  BOOL _performWithAnimations;
  NSMutableArray *_systemModelObservers;
  NSMutableArray *_workspaceObservers;
}

@property (nonatomic, strong) NSMutableArray *slideLayers;
@property (nonatomic, strong) CALayer *slidesContainerLayer;
@property (nonatomic, assign) CGFloat slidesContentLayerScale;

@property (nonatomic, strong) UIScrollView *contentScrollView;
@property (nonatomic, strong) DeckSliderView *deckSliderView;


- (void)layoutSlideLayersAnimated:(BOOL)animated;
- (void)updateSlideContainerTransformAnimated:(BOOL)animated;

- (SlideLayer*)layerForSlide:(MZPortfolioItem*)slide;
- (void)insertLayerForSlide:(MZPortfolioItem*)slide atIndex:(NSUInteger)index;
- (BOOL)isSlideLayerVisible:(CALayer*)layer;
- (BOOL)isSlideLayerVisibleOrNextToVisible:(CALayer*)layer;
- (CGRect)frameForFeldAtIndex:(NSInteger)index;

- (void)removeAllSlideLayers;

- (void)loadImageForSlide:(MZItem*)slide inLayer:(SlideLayer*)slideLayer;

- (BOOL)shouldDisplayFullImageForLayer:(SlideLayer*)slideLayer;
- (void)refreshFullSlideImagesWithDelay:(CGFloat)delay;
- (void)loadFullImageForSlideLayer:(SlideLayer*)slideLayer;
- (void)unloadFullImageForSlideLayer:(SlideLayer*)slideLayer;

- (void)updateSlideLayerImages;
- (void)updateSlideContainerTransform;
- (void)updateSlideLayerVisibility:(SlideLayer*)slideLayer;

- (void)beginObservingDocument:(MZPresentation *)aDocument;
- (void)endObservingDocument:(MZPresentation *)aDocument;

- (void)nextSlide;
- (void)previousSlide;

- (CGRect)frameForSlideAtIndex:(NSInteger)index;
- (CGRect)frameForSlidesArea;

- (void)loadAllSlides;
- (void)setPresentationModeActive:(BOOL)active animated:(BOOL)animated;

@end
