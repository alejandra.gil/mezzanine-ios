//
//  PresentationSliderInteractor.swift
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 03/04/2017.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import UIKit

class PresentationSliderInteractor: NSObject {
  var presenter: PresentationSliderViewController?
  
  fileprivate var systemModelObserverArray = Array <String> ()
  
  var systemModel: MZSystemModel! {
    willSet(newSystemModel) {
      if self.systemModel != newSystemModel {
        endObservingSystemModel()
      }
    }
    
    didSet {
      if systemModel != nil {
        presentation = systemModel.currentWorkspace.presentation
        beginObservingSystemModel()
      }
      else {
        presentation = nil
      }
    }
  }
  
  fileprivate var presentationObserverArray = Array <String> ()
  
  fileprivate var presentation: MZPresentation! {
    willSet(newPresentation) {
      if self.presentation != newPresentation {
        endObservingPresentation()
      }
    }
    
    didSet {
      if presentation != nil {
        beginObservingPresentation()
      }
    }
  }
  
  var communicator: MZCommunicator!
  
  deinit {
    print("PresentationSliderInteractor deinit")
  }
  
  // MARK: Observation
  
  func beginObservingSystemModel() {
    
    // currentWorkspace
    systemModelObserverArray.append(systemModel.addObserver(forKeyPath: "currentWorkspace.presentation", options:[.new, .old], on: OperationQueue.main) { [unowned self] ( obj, change: [AnyHashable: Any]!) in
      let systemModel = obj as! MZSystemModel
      if systemModel.currentWorkspace == nil {
        self.presentation = nil
      } else if let presentation = systemModel.currentWorkspace.presentation {
        self.presentation = presentation
      }
      else {
        self.presentation = nil
      }
      })
  }
  
  func endObservingSystemModel() {
    if systemModelObserverArray.count > 0 {
      for token in systemModelObserverArray {
        systemModel.removeObserver(withBlockToken: token)
      }
    }
    systemModelObserverArray.removeAll()
  }

  func beginObservingPresentation() {
    
    // active
    presentationObserverArray.append(presentation.addObserver(forKeyPath: "active", options:[.new, .old], on: OperationQueue.main) { ( obj, change: [AnyHashable: Any]!) in
      let presentation = obj as! MZPresentation
      self.presenter?.updateLayout(animate: true)
      self.presenter?.updateSliderVisibility(presentation.active, animated: true)
      })
    
    // currentSlideIndex
    presentationObserverArray.append(presentation.addObserver(forKeyPath: "currentSlideIndex", options:[.new, .old], on: OperationQueue.main) { ( obj, change: [AnyHashable: Any]!) in
      self.presenter?.updateSliderIndicatorLabel()
      self.presenter?.updateSliderIndicatorPosition(animated: true)
      self.presenter?.updateSliderCurrentIndicatorPosition(animated: true)
      })
    
    // slides
    presentationObserverArray.append(presentation.addObserver(forKeyPath: "slides", options:[.new, .old], on: OperationQueue.main) { ( obj, change: [AnyHashable: Any]!) in
      self.presenter?.updateLayout(animate: true)
      })
    
    presenter?.updateLayout(animate: false)
    presenter?.updateSliderVisibility(presentation.active, animated: true)
  }
  
  func endObservingPresentation() {
    if presentationObserverArray.count > 0 {
      for token in presentationObserverArray {
        presentation.removeObserver(withBlockToken: token)
      }
    }
    presentationObserverArray.removeAll()
  }
  
  func isPresentationActive() -> Bool {
    guard let presentation = presentation else { return false }
    
    return presentation.active
  }
  
  func numberOfSlides() -> Int {
    guard let presentation = presentation else { return 0 }
    
    return presentation.numberOfSlides
  }
  
  func currentSlideIndex() -> Int {
    guard let presentation = presentation else { return 0 }
    
    return presentation.currentSlideIndex
  }
  
  func requestPresentationScrollRequest(_ index: Int) {
    communicator.requestor.requestPresentationScrollRequest(systemModel.currentWorkspace.uid, currentIndex: index)
  }
}
