//
//  AnalyticsViewController.swift
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 04/03/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

import UIKit

class AnalyticsViewController: UIViewController {
  
  @IBOutlet var titleLabel: UILabel!
  @IBOutlet var yesMessageLabel: UILabel!
  @IBOutlet var noMessageLabel: UILabel!
  @IBOutlet var termsButton: UIButton!
  @IBOutlet var enableSwitch: UISwitch!
  @IBOutlet var cell: UIView!
  @IBOutlet var cellLabel: UILabel!
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = MezzanineStyleSheet.shared().grey30Color
    title = "Mezzanine"
    navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Confirm", style: .plain, target: self, action: #selector(AnalyticsViewController.confirmAction))
  }
  
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    setupOutlets()
  }
  

  //MARK: Actions

  @IBAction func showTermsAction(_ sender: AnyObject) {
    navigationController!.pushViewController(AnalyticsTermsViewController(nibName: "AnalyticsTermsViewController", bundle: nil), animated: true)
  }
  
  @IBAction func switchValueChanged(_ sender: AnyObject) {
    if (enableSwitch.isOn) {
      UIView.animate(withDuration: 0.15, animations: { () -> Void in
        self.yesMessageLabel.alpha = 1.0
        self.noMessageLabel.alpha = 0.0
      })
    } else {
      UIView.animate(withDuration: 0.15, animations: { () -> Void in
        self.yesMessageLabel.alpha = 0.0
        self.noMessageLabel.alpha = 1.0
      })
    }
  }
  
  
  //MARK: Private
  
  func setupOutlets () {
    
    let styleSheet = MezzanineStyleSheet.shared()
    
    //titleLabel
    titleLabel.font = UIFont.dinLight(ofSize: 24)
    titleLabel.textColor = UIColor.white
    
    //messageLabel
    yesMessageLabel.font = UIFont.din(ofSize: 13)
    yesMessageLabel.textColor = UIColor.white
    noMessageLabel.font = UIFont.din(ofSize: 13)
    noMessageLabel.textColor = UIColor.white
    
    //termsButton
    termsButton.titleLabel?.font = UIFont.din(ofSize: 13)
    
    let attributes = [NSAttributedStringKey.foregroundColor : UIColor.white, NSAttributedStringKey.underlineStyle : NSUnderlineStyle.styleSingle.rawValue] as? [NSAttributedStringKey : Any]
    let attributedString = NSAttributedString(string: "About Privacy Policy", attributes: attributes)
  
    termsButton.setAttributedTitle(attributedString, for: UIControlState())
    
    //cell
    cell.backgroundColor = styleSheet?.grey50Color
    cell.layer.borderColor = styleSheet?.grey90Color.cgColor
    cell.layer.borderWidth = 1.0
    cellLabel.font = UIFont.dinMedium(ofSize: 16)
  }
  
  
  @objc func confirmAction() {
    
    // Analytics
    let isRemote = MezzanineAppContext.current().currentCommunicator.isARemoteParticipationConnection
    MezzanineAnalyticsManager.sharedInstance.trackShareDataUsageEvent(accepted: enableSwitch.isOn, isRemote:isRemote)
    
    if ((MezzanineAppContext.current().passphraseViewController) != nil) {
      MezzanineAppContext.current().passphraseViewController.view.alpha = 0
      dismiss(animated: true, completion: nil)
      MezzanineAppContext.current().mainNavController.transitionCoordinator?.animate(alongsideTransition: { (UIViewControllerTransitionCoordinatorContext) in
        MezzanineAppContext.current().mainNavController.viewControllers.first!.dismiss(animated: false, completion: nil)
      }, completion:nil)
    } else {
      MezzanineAppContext.current().mainNavController.viewControllers.first!.dismiss(animated: true, completion: nil)
      dismiss(animated: true, completion: nil)
    }
  }
}
