//
//  ParticipantsRosterTableViewCell.swift
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 04/10/2018.
//  Copyright © 2018 Oblong Industries. All rights reserved.
//

import UIKit

class ParticipantsRosterTableViewCell: UITableViewCell {

  @IBOutlet var participantTypeImageView: UIImageView!
  @IBOutlet var participantNameLabel: UILabel!

  let styleSheet = MezzanineStyleSheet.shared()

  var roomType: MZMezzanineRoomType?

  var participant: MZParticipant? {
    didSet {
      updateParticipantTypeImageView()
      updateParticipantNameLabel()
    }
  }

  override func awakeFromNib() {
    super.awakeFromNib()

    participantNameLabel.font = UIFont.dinMedium(ofSize: 17.0)
  }

  func updateParticipantTypeImageView() {
    var rpAvatar: UIImage?

    if roomType == MZMezzanineRoomType.local || roomType == MZMezzanineRoomType.remote {
      participantTypeImageView.accessibilityIdentifier = "participant-inroom"
      rpAvatar =  UIImage(named:"participant-inroom")?.withRenderingMode(.alwaysTemplate)
    } else {
      if participant?.type == "iPhone" || participant?.type == "iPad" {
        participantTypeImageView.accessibilityIdentifier = "participant-mobile"
        rpAvatar =  UIImage(named:"participant-mobile")?.withRenderingMode(.alwaysTemplate)
      } else {
        participantTypeImageView.accessibilityIdentifier = "participant-laptop"
        rpAvatar =  UIImage(named:"participant-laptop")?.withRenderingMode(.alwaysTemplate)
      }
    }

    participantTypeImageView.image = rpAvatar
    participantTypeImageView.tintColor = styleSheet?.grey190Color
  }

  func updateParticipantNameLabel() {
    participantNameLabel.text = participant?.displayName
  }
}

