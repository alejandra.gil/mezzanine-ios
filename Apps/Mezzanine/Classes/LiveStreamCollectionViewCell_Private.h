//
//  LiveStreamCollectionViewCell_Private.h
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 13/10/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

#import "LiveStreamCollectionViewCell.h"
#import "VideoPlaceholderView.h"

@interface LiveStreamCollectionViewCell ()
{
  VideoPlaceholderView *_videoPlaceholderView;
}

@end
