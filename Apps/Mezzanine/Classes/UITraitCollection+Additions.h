//
//  UITraitCollection+Additions.h
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 25/01/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITraitCollection (Additions)

- (BOOL)isiPhone;
- (BOOL)isiPad;

- (BOOL)isCompactWidth;
- (BOOL)isRegularWidth;

- (BOOL)isCompactHeight;
- (BOOL)isRegularHeight;

- (BOOL)isiPhoneCompactHeight;
- (BOOL)isiPhoneRegularHeight;

- (BOOL)isiPhoneCompactWidth;
- (BOOL)isiPhoneRegularWidth;

- (BOOL)isiPadCompactHeight;
- (BOOL)isiPadRegularHeight;

- (BOOL)isiPadCompactWidth;
- (BOOL)isiPadRegularWidth;

@end
