//
//  PassphraseViewController_Private.h
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 28/10/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

#import "PassphraseViewController.h"

@interface PassphraseViewController ()
{
  BOOL _inTextFieldChangeCallback;
}

@property (nonatomic, assign) NSInteger numOfFields;
@property (nonatomic, strong) NSMutableArray *textFieldArray;
@property (nonatomic, strong) NSMutableArray *underlinesArray;

- (void)join;
- (void)startOver;


@end
