//
//  PopupInfoView.m
//  Mezzanine
//
//  Created by Zai Chang on 7/5/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import "PopupInfoView.h"
#import "UIFont+Oblong.h"

static float innerLabelMargin = 10.0;

@interface PopupInfoLabel : UILabel
@end


@implementation PopupInfoLabel

- (void)drawTextInRect:(CGRect)rect {
  UIEdgeInsets insets = {0, innerLabelMargin, 0, innerLabelMargin};
  [super drawTextInRect:UIEdgeInsetsInsetRect(rect, insets)];
}

@end


@interface PopupInfoView () {
  PopupInfoLabel *label;
  NSTimer *dismissTimer;
}

@end


@implementation PopupInfoView

- (instancetype)initWithFrame:(CGRect)frame
{
  if (self = [super initWithFrame:frame]) {
    self.alpha = 0.0;
    self.backgroundColor = [UIColor clearColor];
    
    label = [[PopupInfoLabel alloc] initWithFrame:frame];
    label.translatesAutoresizingMaskIntoConstraints = NO;
    label.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.66];
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont dinBoldOfSize:16.0];
    label.lineBreakMode = NSLineBreakByWordWrapping;
    label.textAlignment = NSTextAlignmentCenter;
    label.numberOfLines = 0;
    label.layer.cornerRadius = 8.0;
    label.layer.masksToBounds = YES;
    
    [self addSubview:label];
    
  }
  
  return self;
}


- (void)setupConstraints
{
  [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|->=8-[label]->=8-|"
                                                               options:0
                                                               metrics:nil
                                                                 views:NSDictionaryOfVariableBindings(label)]];
  
  [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[label]-60-|"
                                                               options:0
                                                               metrics:nil
                                                                 views:NSDictionaryOfVariableBindings(label)]];
  
  [self addConstraint:[NSLayoutConstraint constraintWithItem:label
                                                   attribute:NSLayoutAttributeCenterX
                                                   relatedBy:NSLayoutRelationEqual
                                                      toItem:self
                                                   attribute:NSLayoutAttributeCenterX
                                                  multiplier:1.0
                                                    constant:0]];

  NSString *width = [NSString stringWithFormat:@"[label(==%f)]", [self textWidth] + innerLabelMargin * 2];
  [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:width
                                                              options:0
                                                              metrics:nil
                                                                views:NSDictionaryOfVariableBindings(label)]];
}


- (void)displayMessage:(NSString*)message withTimeout:(NSTimeInterval)timeoutInterval
{
  if (_overlayWindow)
    _overlayWindow.hidden = NO;

  if (message.length == 0)
    return;
  
  label.text = message;
  [self setupConstraints];

  
  [self layoutIfNeeded];
  [UIView animateWithDuration:0.25 animations:^{
    [self layoutIfNeeded];
    self.alpha = 1.0;
  }];
  
  if (dismissTimer)
  {
    [dismissTimer invalidate];
    dismissTimer = nil;
  }
  
  dismissTimer = [NSTimer timerWithTimeInterval:timeoutInterval target:self selector:@selector(dismissTimerFired:) userInfo:nil repeats:NO];
  [[NSRunLoop currentRunLoop] addTimer:dismissTimer forMode:NSRunLoopCommonModes];
}


-(void) dismissTimerFired:(NSTimer*)timer
{
  [UIView animateWithDuration:0.25 animations:^{
    self.alpha = 0.0;
  }];
  
  dismissTimer = nil;

  if (_overlayWindow)
    _overlayWindow.hidden = YES;
}

- (float)textWidth
{
  NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
  paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
  return [label.text boundingRectWithSize:self.frame.size
                                  options:NSStringDrawingUsesLineFragmentOrigin
                               attributes:@{NSFontAttributeName:[UIFont dinBoldOfSize:16.0],
                                            NSParagraphStyleAttributeName:paragraphStyle}
                                  context:nil].size.width + innerLabelMargin;
}

@end
