//
//  SurfaceControlsView.swift
//  Mezzanine
//
//  Created by miguel on 10/3/17.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import UIKit

class SurfaceControlsView: UIView {

  let zoomButton: UIButton!
//  let arrangeButton: UIButton!
//  let clearButton: UIButton!

  var surface: MZSurface?

  override init(frame: CGRect) {

    var bounds = frame
    bounds.origin = CGPointZero
    zoomButton = UIButton(type: UIButtonType.Custom)
    zoomButton.setImage(UIImage(named: "mobile-workspace-zoom-in-icon"), forState: .Normal)
    zoomButton.imageView?.contentMode = .ScaleAspectFit
    zoomButton.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]

    super.init(frame: frame)

    self.addSubview(zoomButton)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
