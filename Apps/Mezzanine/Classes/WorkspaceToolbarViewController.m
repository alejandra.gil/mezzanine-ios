//
//  WorkspaceToolbarViewController.m
//  Mezzanine
//
//  Created by Zai Chang on 1/22/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import "WorkspaceToolbarViewController.h"

#import "WorkspaceEditorViewController.h"
#import "WorkspaceMenuViewController.h"
#import "MezzanineStyleSheet.h"
#import "ButtonMenuViewController.h"

#import "NSObject+BlockObservation.h"
#import "NSString+VersionChecking.h"

#import "UITraitCollection+Additions.h"
#import "UIView+Debug.h"
#import "UIViewController+FPPopover.h"
#import "Mezzanine-Swift.h"

@interface WorkspaceToolbarViewController ()
{
  NSMutableArray *_systemModelObservers;
  NSMutableArray *_appContextObservers;
  NSMutableDictionary *_roomsContextObservers;
}

@property (nonatomic, strong) WorkspaceToolbarLeaveInteractor *leaveInteractor;

@end

@implementation WorkspaceToolbarViewController

- (void)viewDidLoad
{
  [super viewDidLoad];
  [self initAccessibility];
  [self applyFontStyles];

  // update count UI here for the first time
  // `beginObservingParticipantsInRooms` is called before loading
  // the view so it can't be updated there
  [self updateParticipantsCount];
}


-(void) viewWillDisappear:(BOOL)animated
{
  [super viewWillDisappear:animated];

  if ([self.currentPopover isPopoverVisible])
    [self.currentPopover dismissPopoverAnimated:YES];
}


- (void)viewDidLayoutSubviews
{
  [super viewDidLayoutSubviews];

  // Need to update state after applying font style, as some of our font choices are state-dependent
  if (self.systemModel)
  {
    [self updateMezzanineName];

    if (self.systemModel.systemType == MZSystemTypeMezzanine)
      [self updateWorkspaceNameButton];
    else if (self.systemModel.systemType == MZSystemTypeOcelot)
      [self removeWorkspaceNameUI];
  }
}


- (void)initAccessibility
{
  self.view.accessibilityIdentifier = @"WorkspaceToolbar";
  self.workspaceNameButton.accessibilityIdentifier = @"WorkspaceToolbar.WorkspaceButton";
  self.leaveButton.accessibilityIdentifier = @"WorkspaceToolbar.LeaveButtonButton";
  self.participantsButton.accessibilityIdentifier = @"WorkspaceToolbar.ParticipantsButton";
}


- (void)applyFontStyles
{
  MezzanineStyleSheet *styleSheet = [MezzanineStyleSheet sharedStyleSheet];
  [styleSheet stylizeToolbarButton:self.workspaceNameButton];
  [styleSheet stylizeToolbarButton:self.leaveButton];

  self.mezzanineRoomLabel.textColor = [styleSheet lightBlueRoomColor];
  self.mezzanineRoomLabel.font = [UIFont dinOfSize:16.0];

  self.leaveButton.titleLabel.font =  [UIFont dinOfSize:16.0];
  [self.leaveButton setTitleColor:[styleSheet redHighlightColor] forState:UIControlStateNormal];
  [self.leaveButton setTitleColor:[styleSheet redHighlightColorPress] forState:UIControlStateHighlighted];

  self.participantsCountLabel.font = [UIFont dinOfSize:16.0];
}

- (void)removeWorkspaceNameUI
{
  if (_workspaceNameButton == nil)
    return;

  [_workspaceNameButton removeFromSuperview];
  [_workspaceNameDownCaret removeFromSuperview];
  [_separator removeFromSuperview];

  _workspaceNameButton = nil;
  _workspaceNameDownCaret = nil;
  _separator = nil;

  [_mezzanineRoomContainer removeConstraints:_mezzanineRoomLabel.constraints];
  [_mezzanineRoomContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[roomLabel]-0-|" options:0 metrics:nil views:@{@"roomLabel": _mezzanineRoomLabel}]];
  [_mezzanineRoomContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[roomLabel]-0-|" options:0 metrics:nil views:@{@"roomLabel": _mezzanineRoomLabel}]];
}

#pragma mark - Model

- (void)setSystemModel:(MZSystemModel *)aModel
{
  if (_systemModel != aModel)
  {
    if (_systemModelObservers && _systemModel)
    {
      for (id token in _systemModelObservers)
        [_systemModel removeObserverWithBlockToken:token];
      
      _systemModelObservers = nil;

      [self endObservingParticipantsInRooms:_systemModel.infopresence.rooms];
    }
    
    _systemModel = aModel;
    
    if (_systemModel)
    {
      _systemModelObservers = [[NSMutableArray alloc] init];
      id token;
      __weak typeof(self) __self = self;

      if (self.systemModel.systemType == MZSystemTypeMezzanine)
      {
        token = [_systemModel addObserverForKeyPath:@"currentWorkspace.name"
                                            options:(NSKeyValueObservingOptionInitial)
                                            onQueue:[NSOperationQueue mainQueue]
                                               task:^(id obj, NSDictionary *change) {
                                                 if (__self.isViewLoaded) {
                                                   [__self updateWorkspaceNameButton];
                                                 }
                                               }];
        [_systemModelObservers addObject:token];

        token = [_systemModel addObserverForKeyPath:@"currentWorkspace.isSaved"
                                            options:(NSKeyValueObservingOptionInitial)
                                            onQueue:[NSOperationQueue mainQueue]
                                               task:^(id obj, NSDictionary *change) {
                                                 if (__self.isViewLoaded) {
                                                   [__self updateWorkspaceNameButton];
                                                 }
                                               }];
        [_systemModelObservers addObject:token];

        token = [_systemModel addObserverForKeyPath:@"currentWorkspace.isLoading"
                                            options:0
                                            onQueue:[NSOperationQueue mainQueue]
                                               task:^(id obj, NSDictionary *change) {
                                                 if (__self.isViewLoaded) {
                                                   [__self updateWorkspaceNameButton];
                                                 }
                                               }];
        [_systemModelObservers addObject:token];
      }

      token = [_systemModel addObserverForKeyPath:@"infopresence.status"
                                          options:0
                                          onQueue:[NSOperationQueue mainQueue]
                                             task:^(id obj, NSDictionary *change) {
                                               if (__self.isViewLoaded) {
                                                 [__self updateParticipantsCount];
                                               }
                                             }];
      [_systemModelObservers addObject:token];

      token = [_systemModel addObserverForKeyPath:@"infopresence.rooms"
                                          options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld | NSKeyValueObservingOptionInitial)
                                          onQueue:[NSOperationQueue mainQueue]
                                             task:^(id obj, NSDictionary *change) {
                                               if (__self.isViewLoaded) {
                                                 [__self updateRoomsObservers:change];
                                                 [__self updateParticipantsCount];
                                               }
                                             }];
      [_systemModelObservers addObject:token];

      token = [_systemModel addObserverForKeyPath:@"infopresence.remoteParticipants"
                                          options:0
                                          onQueue:[NSOperationQueue mainQueue]
                                             task:^(id obj, NSDictionary *change) {
                                               if (__self.isViewLoaded) {
                                                 [__self updateParticipantsCount];
                                               }
                                             }];
      [_systemModelObservers addObject:token];

      token = [_systemModel addObserverForKeyPath:@"myMezzanine.participants"
                                          options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld | NSKeyValueObservingOptionInitial)
                                          onQueue:[NSOperationQueue mainQueue]
                                             task:^(id obj, NSDictionary *change) {
                                               if (__self.isViewLoaded) {
                                                 [__self updateParticipantsCount];
                                               }
                                             }];
      [_systemModelObservers addObject:token];

      token = [_systemModel addObserverForKeyPath:@"myMezzanine.name"
                                          options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld | NSKeyValueObservingOptionInitial)
                                          onQueue:[NSOperationQueue mainQueue]
                                             task:^(id obj, NSDictionary *change) {
                                               if (__self.isViewLoaded) {
                                                 [__self updateMezzanineName];
                                               }
                                             }];
      [_systemModelObservers addObject:token];

      [self beginObservingParticipantsInRooms:_systemModel.infopresence.rooms];
    }
  }
}


- (void)setAppContext:(MezzanineAppContext *)appContext
{
  if (_appContext != appContext)
  {
    if (_appContextObservers && _appContext)
    {
      for (id token in _appContextObservers)
        [[NSNotificationCenter defaultCenter] removeObserver:token];
      [_appContextObservers removeAllObjects];

      _appContextObservers = nil;
    }

    _appContext = appContext;
  }
}

- (void)beginObservingParticipantsInRooms:(NSArray *)rooms
{
  if (!_roomsContextObservers)
    _roomsContextObservers = [NSMutableDictionary new];

  for (MZMezzanine *mz in rooms)
  {
    NSMutableArray *tokens = _roomsContextObservers[mz.uid];
    if (!tokens)
    {
      tokens = [NSMutableArray new];
      _roomsContextObservers[mz.uid] = tokens;
    }
    
    __weak typeof(self) __self = self;
    id token = [mz addObserverForKeyPath:@"participants"
                                        options:0
                                        onQueue:[NSOperationQueue mainQueue]
                                           task:^(id obj, NSDictionary *change) {
                                             if (__self.isViewLoaded) {
                                               [__self updateParticipantsCount];
                                             }
                                           }];
    [tokens addObject:token];
  }
}

- (void)endObservingParticipantsInRooms:(NSArray *)rooms
{
  for (MZMezzanine *mz in rooms)
  {
    NSMutableArray *tokens = _roomsContextObservers[mz.uid];
    if (tokens)
    {
      for (id token in tokens)
        [mz removeObserverWithBlockToken:token];
    }
    [tokens removeAllObjects];
    [_roomsContextObservers removeObjectForKey:mz.uid];
  }
}

- (void)updateRoomsObservers:(NSDictionary *)change
{
  NSNumber *kind = change[NSKeyValueChangeKindKey];
  if ([kind integerValue] == NSKeyValueChangeInsertion)
  {
    NSArray *inserted = change[NSKeyValueChangeNewKey];
    [self beginObservingParticipantsInRooms:inserted];
  }
  else if ([kind integerValue] == NSKeyValueChangeRemoval)
  {
    NSArray *removed = change[NSKeyValueChangeOldKey];
    [self endObservingParticipantsInRooms:removed];
  }
}


#pragma mark - Actions

- (IBAction)mezzanineDetails:(id)sender
{
  if (!_leaveInteractor) {
    _leaveInteractor = [[WorkspaceToolbarLeaveInteractor alloc] initWithViewController:self];
    _leaveInteractor.communicator = self.communicator;
  }
  [_leaveInteractor leaveFromButton:(UIButton *)sender];
}


- (IBAction)workspaceDetails:(id)sender
{
  WorkspaceMenuViewController *controller = [[WorkspaceMenuViewController alloc] init];
  controller.systemModel = _systemModel;
  controller.workspaceToolbarViewController = self;
  controller.workspace = _systemModel.currentWorkspace;
  
  FPPopoverController *popoverController = [[FPPopoverController alloc] initWithContentViewController:controller];
  [popoverController stylizeAsActionSheet];

  __weak typeof (popoverController) weakPopoverController = popoverController;
  popoverController.popoverPresentingBlock = ^{

    [weakPopoverController presentPopoverFromRect:self.workspaceNameButton.frame inView:self.workspaceNameButton.superview permittedArrowDirections:FPPopoverArrowDirectionUp animated:YES];
  };

  popoverController.popoverPresentingBlock ();
  self.currentPopover = popoverController;
}


- (IBAction)participantsList:(id)sender
{
  [self showParticipantsList];
}


#pragma mark - Private

- (void)updateMezzanineName
{
  if (self.communicator.isARemoteParticipationConnection)
    self.mezzanineRoomLabel.text = NSLocalizedString(@"Mezzanine", nil);
  else
    self.mezzanineRoomLabel.text = self.systemModel.myMezzanine.name;
}


- (void)showParticipantsList
{
  if ([_delegate respondsToSelector:@selector(workspaceToolbarOpenInfopresence)]) {
    [_delegate workspaceToolbarOpenInfopresence];
  }
}


- (void)updateWorkspaceNameButton
{
  CGFloat fontSize = 16.0;
  MZWorkspace *workspace = self.systemModel.currentWorkspace;
  if (workspace.isSaved)
  {
    [self.workspaceNameButton setTitle:workspace.name forState:UIControlStateNormal];
    _workspaceNameButton.accessibilityLabel = workspace.name;
    _workspaceNameButton.titleLabel.font = [UIFont dinOfSize:fontSize];
  }
  else
  {
    NSString *unsavedString = NSLocalizedString(@"Workspace Unsaved Button Title", nil);
    [_workspaceNameButton setTitle:unsavedString forState:UIControlStateNormal];
    _workspaceNameButton.accessibilityLabel = unsavedString;
    _workspaceNameButton.titleLabel.font = [UIFont dinItalicOfSize:fontSize];
  }

  _workspaceNameButton.enabled = !workspace.isLoading;
}


- (void)workspaceRename
{
  if (!_systemModel.currentWorkspace.isSaved)
    return [self workspaceSave];
  
  CGFloat popoverWidth = self.traitCollection.isRegularWidth && self.traitCollection.isRegularHeight ? 320 : 300;
  WorkspaceEditorViewController *controller = [[WorkspaceEditorViewController alloc] initWithWorkspace:_systemModel.currentWorkspace];
  controller.preferredContentSize = CGSizeMake(popoverWidth, 142);
  controller.doneButtonTitle = NSLocalizedString(@"Workspace Rename Button Title", nil);
  controller.autoHighlightNameField = YES;
  
  FPPopoverController *popoverController = [[FPPopoverController alloc] initWithContentViewController:controller];
  [popoverController stylizeAsActionSheet];
  
  __weak typeof(self) __self = self;
  __weak WorkspaceEditorViewController *__controller = controller;
  __weak FPPopoverController *__popoverController = popoverController;
  
  controller.requestSave = ^{
    if (__self.systemModel.currentWorkspace.isSaved)
    {
      [__self.communicator.requestor requestWorkspaceRenameRequest:__self.systemModel.currentWorkspace.uid name:__controller.nameTextField.text];
    }
    else
    {
      NSArray *ownersArray = __self.systemModel.currentUsername ? @[__self.systemModel.currentUsername] : @[];
      [__self.communicator.requestor requestWorkspaceSaveRequest:__self.systemModel.currentWorkspace.uid owners:ownersArray name:__controller.nameTextField.text];
    }
    
    
    [__popoverController dismissPopoverAnimated:YES];
  };
  controller.requestCancel = ^{
    [__popoverController dismissPopoverAnimated:YES];
  };
  
  popoverController.popoverPresentingBlock = ^{
    
    [__popoverController presentPopoverFromRect:self.workspaceNameButton.frame inView:self.workspaceNameButton.superview permittedArrowDirections:FPPopoverArrowDirectionUp animated:YES];
  };
  
  popoverController.popoverPresentingBlock ();
  
  self.currentPopover = popoverController;
}


- (void)workspaceSave
{
  CGFloat popoverWidth = self.traitCollection.isRegularWidth && self.traitCollection.isRegularHeight ? 320 : 300;
  WorkspaceEditorViewController *controller = [[WorkspaceEditorViewController alloc] initWithWorkspace:_systemModel.currentWorkspace];
  controller.preferredContentSize = CGSizeMake(popoverWidth, 188); // TODO - calculate height dynamically
  controller.doneButtonTitle = NSLocalizedString(@"Workspace Save Button Title", nil);
  controller.hintText = (_systemModel.currentUsername != nil) ? [NSString stringWithFormat:NSLocalizedString(@"Worskpace Save As Private With User Message", nil), _systemModel.currentUsername] : NSLocalizedString(@"Worskpace Save As Public Message", nil);
  controller.workspaceNameOverride = [MZWorkspace createDefaultWorkspaceName];
  controller.autoHighlightNameField = YES;
  
  FPPopoverController *popoverController = [[FPPopoverController alloc] initWithContentViewController:controller];
  [popoverController stylizeAsActionSheet];
  
  __weak typeof(self) __self = self;
  __weak WorkspaceEditorViewController *__controller = controller;
  __weak FPPopoverController *__popoverController = popoverController;
  
  controller.requestSave = ^{
    NSArray *ownersArray = __self.systemModel.currentUsername ? @[__self.systemModel.currentUsername] : @[];
    [__self.communicator.requestor requestWorkspaceSaveRequest:__self.systemModel.currentWorkspace.uid owners:ownersArray name:__controller.nameTextField.text];
    
    [__popoverController dismissPopoverAnimated:YES];
  };
  controller.requestCancel = ^{
    [__popoverController dismissPopoverAnimated:YES];
  };
  
  [__popoverController presentPopoverFromRect:self.workspaceNameButton.frame inView:self.workspaceNameButton.superview permittedArrowDirections:FPPopoverArrowDirectionUp animated:NO];
  
  self.currentPopover = popoverController;
}


- (void)workspaceDiscard
{
  __weak typeof(self) weakSelf = self;

  UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Workspace Discard Dialog Title", nil)
                                                                           message:NSLocalizedString(@"Workspace Discard Dialog Message", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
  UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Workspace Discard Dialog Cancel Button Title", nil)
                                                         style:UIAlertActionStyleCancel
                                                       handler:nil];
  UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Workspace Discard Dialog Confirm Button Title", nil)
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * _Nonnull action) {
                                                          MZTransaction *transaction = [self.communicator.requestor requestWorkspaceDiscardRequest:weakSelf.systemModel.currentWorkspace.uid];

                                                          transaction.beganBlock = ^{
                                                            if ([weakSelf.delegate respondsToSelector:@selector(workspaceToolbarDiscardWorkspaceOverlayMessage:animated:)]) {
                                                              [weakSelf.delegate workspaceToolbarDiscardWorkspaceOverlayMessage:NSLocalizedString(@"Workspace Discarding", nil) animated:YES];
                                                            }
                                                          };
                                                          transaction.endedBlock = ^{
                                                            if ([weakSelf.delegate respondsToSelector:@selector(workspaceToolbarHideWorkspaceOverlay:)]) {
                                                              [weakSelf.delegate workspaceToolbarHideWorkspaceOverlay:YES];
                                                            }
                                                          };
                                                        }];
  [alertController addAction:cancelAction];
  [alertController addAction:confirmAction];
  [self presentViewController:alertController animated:YES completion:nil];
}


- (void)workspaceOpen
{
  __weak typeof(self) weakSelf = self;

  if ([weakSelf.delegate respondsToSelector:@selector(workspaceToolbarOpenWorkspaceList)]) {
    [weakSelf.delegate workspaceToolbarOpenWorkspaceList];
  }
}


- (void)workspaceClose
{
  __weak typeof(self) weakSelf = self;

  if (weakSelf.systemModel.currentWorkspace.isSaved)
  {
    if ([weakSelf.delegate respondsToSelector:@selector(workspaceToolbarCloseWorkspace)]) {
      [weakSelf.delegate workspaceToolbarCloseWorkspace];
    }
  }
  else
  {
    [self workspaceDiscard];
  }
}

#pragma mark - Participants counter

- (void)updateParticipantsCount
{
  if (self.systemModel.featureToggles.participantRoster)
    [self updateCountForParticipantsRoster];
  else
    [self updateCountForRoomAndRemotes];
}


- (void)updateCountForParticipantsRoster
{
  NSInteger totalParticipants = self.systemModel.myMezzanine.participants.count;

  for (MZMezzanine *room in self.systemModel.infopresence.rooms)
  {
    if (room.roomType != MZMezzanineRoomTypeCloud && room.participants.count == 0)
      totalParticipants += 1;
    else
      totalParticipants += room.participants.count;
  }

  self.participantsCountLabel.text = [NSString stringWithFormat:@"%ld", (long)totalParticipants];
}


- (void)updateCountForRoomAndRemotes
{
  NSInteger participants = self.systemModel.infopresence.remoteParticipants.count;

  for (MZMezzanine *room in self.systemModel.infopresence.rooms)
  {
    if (room.roomType != MZMezzanineRoomTypeCloud && room.participants.count == 0)
      participants += 1;
  }

  self.participantsCountLabel.text = [NSString stringWithFormat:@"%ld", (long)participants];
}

@end
