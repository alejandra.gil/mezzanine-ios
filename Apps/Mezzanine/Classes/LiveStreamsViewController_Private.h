//
//  LiveStreamsViewController_Private.h
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 09/10/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

#import "LiveStreamsViewController.h"
#import "FPPopoverController.h"

@class NIMutableCollectionViewModel;
@class LiveStreamCollectionViewCell;

@interface LiveStreamsViewController () <FPPopoverControllerDelegate>
{
  NSMutableArray *workspaceObservers;
  CGFloat previousLayoutHeight;
}

@property (nonatomic, strong) NIMutableCollectionViewModel *collectionViewModel;

- (void)beginObservingLiveStream:(MZLiveStream*)liveStream;
- (void)endObservingLiveStream:(MZLiveStream*)liveStream;

- (NSArray *)resetCollectionViewDataSource;
- (BOOL)isValidLiveStream:(MZLiveStream*)liveStream;
- (NSArray*)validStreamsFromModel;
- (void)placeItemOnScreen:(MZLiveStream*)liveStream;
- (void)openAssetViewerFromCell:(LiveStreamCollectionViewCell *)cell;

@end
