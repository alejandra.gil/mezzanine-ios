//
//  FeldGeometry.swift
//  Mezzanine
//
//  Created by miguel on 24/2/17.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import Foundation

protocol FeldGeometryProtocol {

  func roundedFrameForFeld(_ feld: MZFeld) -> CGRect
  func rectForFeld(_ feld: MZFeld) -> CGRect
  func feldForLocation(_ location: CGPoint) -> MZFeld?
  func closestFeldForLocation(_ location: CGPoint) -> MZFeld?
  func feldsContainedInRect(_ rect: CGRect) -> Array<MZFeld>?
  func feldsIntersectingWithRect(_ rect: CGRect) -> Array<MZFeld>?
}

struct FeldGeom {
  var feld: MZFeld!
  var feldRect: CGRect!
}


class FeldGeometry: NSObject, FeldGeometryProtocol {

  var surface: MZSurface? {
    didSet {
      feldGeoms = calculateFeldGeom()
    }
  }

  var containerRect: CGRect? {
    didSet {
      feldGeoms = calculateFeldGeom()
    }
  }

  fileprivate var feldGeoms: Array<FeldGeom>?

  fileprivate func calculateFeldGeom() -> Array<FeldGeom>? {

    guard let surface = surface else { return nil }
    guard let containerRect = containerRect else { return nil }

    // This assumes that the containerRect is based in the boundingFeld of the surface
    let scale = containerRect.width / CGFloat(surface.boundingFeld.physicalSize.x.doubleValue)
    let offsetX = (surface.felds[0] as AnyObject).feldRect().minX
    let offsetY = (surface.felds[0] as AnyObject).feldRect().minY

    var finalFeldGeomArray = Array<FeldGeom>()
    for feld in surface.felds {

      let feld = feld as! MZFeld
      let width = (CGFloat(feld.physicalSize.x.doubleValue) * scale)
      // FIXME: Instead of using aspect ratio, let's shield clients from incorrect screen configurations
      // let height = (width / feld.aspectRatio)
      let height = (CGFloat(feld.physicalSize.y.doubleValue) * scale)
      let x = ((feld.feldRect().origin.x - offsetX) * scale)
      let y = ((feld.feldRect().origin.y - offsetY) * scale)

      let rect = CGRect(x: x, y: y, width: width, height: height)
      finalFeldGeomArray.append(FeldGeom(feld: feld, feldRect: rect))
    }

    return finalFeldGeomArray
  }

  func roundedFrameForFeld(_ feld: MZFeld) -> CGRect {
    return rectForFeld(feld).roundedFrame()
  }

  func rectForFeld(_ feld: MZFeld) -> CGRect {

    let feldGeom = feldGeoms?.filter{ $0.feld == feld }.first

    if let feldGeom = feldGeom {
      return feldGeom.feldRect
    }

    return CGRect.zero
  }

  func feldForLocation(_ location: CGPoint) -> MZFeld? {

    let feldGeom = feldGeoms?.filter{ $0.feldRect.contains(location) }.first

    if let feldGeom = feldGeom {
      return feldGeom.feld
    }
    
    return nil
  }

  func closestFeldForLocation(_ location: CGPoint) -> MZFeld? {

    guard let feldGeoms = feldGeoms else { return nil }

    var minDistance = CGFloat.greatestFiniteMagnitude
    var closestFeld = feldForLocation(location)

    if closestFeld != nil {
      return closestFeld
    }

    for feldGeom in feldGeoms {
      let distance = CGPointDistance(location, CGPoint(x: feldGeom.feldRect.midX, y: feldGeom.feldRect.midY))
      if distance < minDistance {
        minDistance = distance
        closestFeld = feldGeom.feld
      }
    }
    return closestFeld
  }

  func feldsContainedInRect(_ rect: CGRect) -> Array<MZFeld>? {

    return feldGeoms?.filter{ rect.contains($0.feldRect) }.map{ $0.feld }
  }

  func feldsIntersectingWithRect(_ rect: CGRect) -> Array<MZFeld>? {

    return feldGeoms?.filter{ rect.intersects($0.feldRect) }.map{ $0.feld }
  }
}
