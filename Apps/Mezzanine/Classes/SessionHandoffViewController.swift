//
//  SessionHandoffViewController.swift
//  Mezzanine
//
//  Created by miguel on 31/1/17.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import UIKit

class SessionHandoffViewController: UIViewController {

  @IBOutlet var titleLabel: UILabel!
  @IBOutlet var subtitleLabel: UILabel!
  @IBOutlet var disconnectButton: UIButton!
  @IBOutlet var cancelButton: UIButton!

  var communicator: MZCommunicator!

  init(communicator: MZCommunicator!) {
    self.communicator = communicator
    super.init(nibName: "SessionHandoffViewController", bundle: nil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    view.isOpaque = false

    titleLabel.font = UIFont.dinLight(ofSize: 30)
    titleLabel.textColor = UIColor.white
    titleLabel.text = "Handoff Title".localized
    subtitleLabel.font = UIFont.dinMedium(ofSize: 18)
    subtitleLabel.textColor = UIColor.white
    subtitleLabel.text = "Handoff Subtitle".localized

    MezzanineStyleSheet.shared().stylizeDeclineActionButton(disconnectButton)
    disconnectButton.setTitle("Handoff Disconnect Button".localized, for: UIControlState())

    MezzanineStyleSheet.shared().stylizeCancelActionButton(cancelButton)
    cancelButton.setTitle("Handoff Cancel Button".localized, for: UIControlState())

    self.view.setNeedsLayout()
  }

  @IBAction func cancel(_ sender: AnyObject) {
    let analyticsManager = MezzanineAnalyticsManager.sharedInstance
    analyticsManager.tagEvent(analyticsManager.handoffInitEvent, attributes: [analyticsManager.responseKey: analyticsManager.cancelAttribute])

    self.dismiss(animated: true, completion: nil)
  }

  @IBAction func disconnect(_ sender: AnyObject) {
    let analyticsManager = MezzanineAnalyticsManager.sharedInstance
    analyticsManager.tagEvent(analyticsManager.handoffEndedEvent, attributes: [analyticsManager.responseKey: analyticsManager.disconnectAttribute])

    communicator.disconnect()
    self.dismiss(animated: true, completion: nil)
  }
}
