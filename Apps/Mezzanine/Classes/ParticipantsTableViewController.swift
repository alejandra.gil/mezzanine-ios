//
//  ParticipantsTableViewController.swift
//  Mezzanine
//
//  Created by Miguel Sanchez on 29/6/15.
//  Copyright (c) 2015 Oblong Industries. All rights reserved.
//

import UIKit

class ParticipantsTableViewController: ParticipantsBaseTableViewController
{
  var participants = Array<Any>()
  var totalRooms: Int = 0
  var totalPeople: Int = 0

  fileprivate let maxParticipantsBeforeExpansion = 5
  var isShowingAllParticipants = false

  let styleSheet = MezzanineStyleSheet.shared()


  init(systemModel: MZSystemModel, communicator: MZCommunicator)
  {
    super.init(nibName: nil, bundle: nil)
    
    self.systemModel = systemModel
    self.communicator = communicator

    isShowingAllParticipants = traitCollection.isRegularWidth() == true && traitCollection.isRegularHeight() == true
  }

  required init!(coder aDecoder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
  }

  deinit
  {
    endObservingSystemModel()
    participants.removeAll(keepingCapacity: false)
    totalRooms = 0
    totalPeople = 0
  }

  // MARK: - View Cycle

  fileprivate let participantCellIdentifier = "ParticipantsTableViewCell"
  fileprivate let expansionCellIdentifier = "ExpansionTableViewCell"

  var noActiveSessionLabel: UILabel?

  override func viewDidLoad()
  {
    super.viewDidLoad()

    tableView.backgroundColor = UIColor.clear
    tableView.separatorStyle = .none
    tableView.rowHeight = 35.0
    tableView.register(UINib(nibName: participantCellIdentifier, bundle:nil), forCellReuseIdentifier: participantCellIdentifier)
    tableView.register(UITableViewCell.self, forCellReuseIdentifier: expansionCellIdentifier)

    tableView.reloadData()
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)

    tableView.reloadData()
  }

  // MARK: - Model Observation

  fileprivate var kSystemModelContext = 0
  fileprivate let kInfopresenceStatusObservationKeyPath = "infopresence.status"
  fileprivate let kInfopresenceRemoteParticipantsObservationKeyPath = "infopresence.remoteParticipants"
  fileprivate let kInfopresenceRoomsObservationKeyPath = "infopresence.rooms"

  fileprivate var kRemoteMezzContext = 1
  fileprivate var kRemoteMezzNameObservationKeyPath = "name"
  fileprivate var kRemoteMezzLocationObservationKeyPath = "location"
  fileprivate var kRemoteMezzCompanyObservationKeyPath = "company"

  fileprivate var kRemoteParticipantContext = 2
  fileprivate var kRemoteParticipantDisplayNameObservationKeyPath = "displayName"

  override var systemModel: MZSystemModel! {
    willSet(aNewModel)
    {
      if (self.systemModel != nil) && (self.systemModel != aNewModel)
      {
        endObservingSystemModel()
      }
    }
    didSet
    {
      if (self.systemModel != nil)
      {
        beginObservingSystemModel()
        tableView.reloadData()
      }
    }
  }

  func beginObservingSystemModel()
  {
    self.systemModel?.addObserver(self, forKeyPath: kInfopresenceStatusObservationKeyPath, options: .new, context: &kSystemModelContext)
    self.systemModel?.addObserver(self, forKeyPath: kInfopresenceRemoteParticipantsObservationKeyPath, options: [.new, .old], context: &kSystemModelContext)
    self.systemModel?.addObserver(self, forKeyPath: kInfopresenceRoomsObservationKeyPath, options: [.new, .old], context: &kSystemModelContext)

    if let rooms = systemModel?.infopresence.rooms
    {
      for room in rooms {
        if (room is MZRemoteMezz) {
          beginObservingRemoteMezz(room as! MZRemoteMezz)

          // Do not show Cloud mezzes in the list
          guard let room = room as? MZMezzanine else { return }
          if room.roomType != .cloud {
            totalRooms = +1
            participants += [room]
          }
        } else {
          totalRooms = +1
          participants += [room]
        }
      }
    }
    if let pendingInvites = systemModel?.infopresence.pendingInvites
    {
      participants += pendingInvites
    }
    if let remoteParticipants = systemModel?.infopresence.remoteParticipants
    {
      for participant in remoteParticipants {
        beginObservingRemoteParticipant(participant as! MZRemoteParticipant)
      }
      totalPeople = remoteParticipants.count
      participants += remoteParticipants
    }

    participants = participants.sorted(by: participantsSort)
  }

  func endObservingSystemModel()
  {
    for participant in participants {
      if participant is MZRemoteMezz {
        endObservingRemoteMezz(participant as! MZRemoteMezz)
      }
      else if participant is MZRemoteParticipant {
        endObservingRemoteParticipant(participant as! MZRemoteParticipant)
      }
    }

    participants.removeAll(keepingCapacity: false)

    self.systemModel?.removeObserver(self, forKeyPath: kInfopresenceStatusObservationKeyPath)
    self.systemModel?.removeObserver(self, forKeyPath: kInfopresenceRemoteParticipantsObservationKeyPath)
    self.systemModel?.removeObserver(self, forKeyPath: kInfopresenceRoomsObservationKeyPath)
  }

  func beginObservingRemoteMezz(_ remoteMezz: MZRemoteMezz)
  {
    remoteMezz.addObserver(self, forKeyPath: kRemoteMezzNameObservationKeyPath, options: [.new, .old], context: &kRemoteMezzContext)
    remoteMezz.addObserver(self, forKeyPath: kRemoteMezzCompanyObservationKeyPath, options: [.new, .old], context: &kRemoteMezzContext)
    remoteMezz.addObserver(self, forKeyPath: kRemoteMezzLocationObservationKeyPath, options: [.new, .old], context: &kRemoteMezzContext)
  }

  func endObservingRemoteMezz(_ remoteMezz: MZRemoteMezz)
  {
    remoteMezz.removeObserver(self, forKeyPath: kRemoteMezzNameObservationKeyPath, context: &kRemoteMezzContext)
    remoteMezz.removeObserver(self, forKeyPath: kRemoteMezzCompanyObservationKeyPath, context: &kRemoteMezzContext)
    remoteMezz.removeObserver(self, forKeyPath: kRemoteMezzLocationObservationKeyPath, context: &kRemoteMezzContext)
  }

  func beginObservingRemoteParticipant(_ remoteParticipant: MZRemoteParticipant)
  {
    remoteParticipant.addObserver(self, forKeyPath: kRemoteParticipantDisplayNameObservationKeyPath, options: [.new, .old], context: &kRemoteParticipantContext)
  }

  func endObservingRemoteParticipant(_ remoteParticipant: MZRemoteParticipant)
  {
    remoteParticipant.removeObserver(self, forKeyPath: kRemoteParticipantDisplayNameObservationKeyPath, context: &kRemoteParticipantContext)
  }


  override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?)
  {
    switch (keyPath!, context) {
    case (kInfopresenceStatusObservationKeyPath, .some(&kSystemModelContext)):
      tableView.reloadData()

    case (kInfopresenceRoomsObservationKeyPath, .some(&kSystemModelContext)),
         (kInfopresenceRemoteParticipantsObservationKeyPath, .some(&kSystemModelContext)):
      
      if let inserted = change?[NSKeyValueChangeKey.newKey] as! NSArray! {
        for insertedItem in inserted {
          if insertedItem is MZRemoteMezz {
            beginObservingRemoteMezz(insertedItem as! MZRemoteMezz)

            // Do not show Cloud mezzes in the list
            guard let insertedItem = insertedItem as? MZMezzanine else { return }
            if insertedItem.roomType != .cloud {
              totalPeople += 1
              participants += [insertedItem]
            }

          }
          else if insertedItem is MZRemoteParticipant {
            beginObservingRemoteParticipant(insertedItem as! MZRemoteParticipant)

            totalPeople += 1
            participants += [insertedItem]
          }

          participants = participants.sorted(by: participantsSort)
        }
      }
      
      if let deleted = change?[NSKeyValueChangeKey.oldKey] as! NSArray! {
        for deletedItem in deleted {
          let index = (participants as NSArray).index(of: deletedItem)

          if deletedItem is MZRemoteMezz {
            endObservingRemoteMezz(deletedItem as! MZRemoteMezz)

            guard let deletedItem = deletedItem as? MZMezzanine else { return }
            if deletedItem.roomType != .cloud {
              totalPeople -= 1
              participants.remove(at: index)
            }
          }
          else if deletedItem is MZRemoteParticipant {
            endObservingRemoteParticipant(deletedItem as! MZRemoteParticipant)

            totalPeople -= 1
            participants.remove(at: index)
          }
        }
      }

      tableView.reloadData()

    case (kRemoteMezzNameObservationKeyPath, .some(&kRemoteMezzContext)),
         (kRemoteMezzCompanyObservationKeyPath, .some(&kRemoteMezzContext)),
         (kRemoteMezzLocationObservationKeyPath, .some(&kRemoteMezzContext)),
         (kRemoteParticipantDisplayNameObservationKeyPath, .some(&kRemoteParticipantContext)):
      participants = participants.sorted(by: participantsSort)
      tableView.reloadData()

    default:
      super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
    }
  }

  func canInsertOrDeleteARowAtIndex(_ index: Int) -> Bool {
    return isShowingAllParticipants || index < maxParticipantsBeforeExpansion
  }

  // MARK: - Participants Sorting methods

  func stringsAreEqualOrEmpty(_ s1: String?, s2: String?) -> Bool
  {
    return ((s1 ?? "").isEmpty && (s2 ?? "").isEmpty) || (s1 ?? "").localizedCaseInsensitiveCompare((s2 ?? "")) == ComparisonResult.orderedSame
  }

  func remoteMezzSort(_ m1: MZRemoteMezz, m2: MZRemoteMezz) -> Bool
  {
    if stringsAreEqualOrEmpty(m1.name, s2:m2.name) {
      if stringsAreEqualOrEmpty(m1.company, s2:m2.company) {
        if stringsAreEqualOrEmpty(m1.location, s2:m2.location) {
          return Unmanaged.passUnretained(m1).toOpaque() < Unmanaged.passUnretained(m2).toOpaque()
        } else {
          return (m1.location ?? "").compare((m2.location ?? ""), options: [.numeric, .caseInsensitive]) == ComparisonResult.orderedAscending
        }
      } else {
        return (m1.company ?? "").compare((m2.company ?? ""), options: [.numeric, .caseInsensitive]) == ComparisonResult.orderedAscending
      }
    } else {
      return (m1.name ?? "").compare((m2.name ?? ""), options: [.numeric, .caseInsensitive]) == ComparisonResult.orderedAscending
    }
  }

  func remoteParticipantSort(_ p1: MZRemoteParticipant, p2: MZRemoteParticipant) -> Bool
  {
    if stringsAreEqualOrEmpty(p1.displayName, s2:p2.displayName) {
      if stringsAreEqualOrEmpty(p1.uid, s2:p2.uid) {
        return Unmanaged.passUnretained(p1).toOpaque() < Unmanaged.passUnretained(p2).toOpaque()
      } else {
        return (p1.uid ?? "").compare((p2.uid ?? ""), options: [.numeric, .caseInsensitive]) == ComparisonResult.orderedAscending
      }
    } else {
      return (p1.displayName ?? "").compare((p2.displayName ?? ""), options: [.numeric, .caseInsensitive]) == ComparisonResult.orderedAscending
    }
  }

  func participantIsMyMezzanine (_ p: Any) -> Bool {
    return (p is MZMezzanine && !(p is MZRemoteMezz))
  }

  func participantsSort(_ p1: Any, p2: Any) -> Bool
  {
    if participantIsMyMezzanine(p1) {
      return true
    }
    else if participantIsMyMezzanine(p2) {
      return false
    }
    else if (p1 is MZRemoteMezz) && (p2 is MZRemoteMezz) {
      return remoteMezzSort(p1 as! MZRemoteMezz, m2:p2 as! MZRemoteMezz)
    }
    else if (p1 is MZRemoteMezz) {
      return true
    }
    else if (p2 is MZRemoteMezz) {
      return false
    }
    else if (p1 is MZInfopresenceCall) && (p2 is MZInfopresenceCall) {
      return remoteMezzSort(systemModel.remoteMezz(withUid: (p1 as! MZInfopresenceCall).uid!), m2:systemModel.remoteMezz(withUid: (p2 as! MZInfopresenceCall).uid!))
    }
    else if (p1 is MZInfopresenceCall) {
      return true
    }
    else if (p2 is MZInfopresenceCall) {
      return false
    }
    else {
      return remoteParticipantSort(p1 as! MZRemoteParticipant, p2:p2 as! MZRemoteParticipant)
    }
  }


  // MARK: - Table view data source
  override func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }

  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return isShowingExpansionButton() ? maxParticipantsBeforeExpansion + 1 : totalParticipants()
  }

  override func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
    return false
  }

  // Participant roster table has more space between the table view header and cells, let's add a header to achieve the same result
  override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 10.0
  }

  override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    return UIView(frame: CGRect(x: CGFloat(0.0), y: CGFloat(0.0), width: self.tableView.frame.width, height: 10.0))
  }

  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

    if isShowingExpansionButton() && indexPath.row == maxParticipantsBeforeExpansion {
      let cell = tableView.dequeueReusableCell(withIdentifier: expansionCellIdentifier, for: indexPath) 

      var showAllButton: UIButton? = nil
      let buttons = cell.contentView.subviews.filter({$0 is UIButton})

      if buttons.count == 0 {
        showAllButton = UIButton(type: .system)
        showAllButton!.frame = CGRect(x: 0, y: 0, width: 200, height: 40)
        showAllButton!.addTarget(self, action: #selector(ParticipantsTableViewController.showAllParticipants(_:)), for: UIControlEvents.touchUpInside)

        showAllButton!.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
        showAllButton!.translatesAutoresizingMaskIntoConstraints = false

        cell.contentView.addSubview(showAllButton!)

        cell.contentView.backgroundColor = styleSheet?.grey30Color

        cell.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[showAllButton]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["showAllButton":showAllButton!]))
        cell.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-45-[showAllButton]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["showAllButton":showAllButton!]))
      } else {
        showAllButton = buttons.first as? UIButton
      }

      let title = NSAttributedString(string: "Participants Section Show All".localized, attributes: [NSAttributedStringKey.font: UIFont.dinMedium(ofSize: 14.0), NSAttributedStringKey.foregroundColor:UIColor.white])
      showAllButton!.setAttributedTitle(title, for: UIControlState())

      return cell
    }

    let cell = tableView.dequeueReusableCell(withIdentifier: participantCellIdentifier, for: indexPath) as! ParticipantsTableViewCell

    cell.systemModel = systemModel
    cell.communicator = communicator
    cell.participant = participants[indexPath.row]

    return cell
  }

  func totalParticipants() -> NSInteger {
     return participants.count
  }

  func isShowingExpansionButton() -> Bool {
    return !isShowingAllParticipants && totalParticipants() > maxParticipantsBeforeExpansion
  }

  @objc func showAllParticipants(_ sender: AnyObject) {
    isShowingAllParticipants = true
    self.tableView.reloadData()
  }
  
  
  // MARK: - Adaptative layout
  override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
    for (_, viewController) in (navigationController?.viewControllers.enumerated())! {
      if viewController == self {
        navigationController?.popViewController(animated: false)
        break
      }
    }
  }
}
