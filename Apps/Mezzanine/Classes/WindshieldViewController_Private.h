//
//  WindshieldViewController_Private.h
//
//  A WindshieldViewController class extension
//
//  Mezzanine
//
//  Created by Miguel Sánchez Valdés on 09/10/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

#import "WindshieldViewController.h"
#import "WindshieldPlaceholderView.h"

@interface WindshieldViewController ()
{
  BOOL aShielderIsGrabbed;
  NSUInteger previousNumberOfTouchesWhileDragging;
  MZWindshieldItem *droppedShielderWaitingForConfirmation;
  WindshieldPlaceholderView *placeholderView;
}

@property (nonatomic, assign) BOOL firstLoad;

- (void)refreshItemsLayout:(NSArray *)items;

- (void)insertViewForItem:(MZWindshieldItem *)item;
- (void)removeItemView:(AssetView*)itemView animated:(BOOL)animated;
- (void)removeViewForItem:(MZWindshieldItem *)item animated:(BOOL)animated;
- (void)removeAllItemViews;
- (void)loadAllWindshieldItems;
- (AssetView*)viewForItem:(id)item;
- (void)updateCropViewForItem:(MZWindshieldItem *)item;
- (void)addGestureRecognizers:(AssetView *)itemView;

- (CGFloat)windshieldFeldWidth;
- (CGFloat)windshieldWidth;
- (CGFloat)windshieldHeight;
- (CGFloat)surface:(MZSurface *)surface feldXOffset:(MZFeld *)feld;
- (CGFloat)surface:(MZSurface *)surface feldHeight:(MZFeld *)feld;
- (CGFloat)surface:(MZSurface *)surface feldWidth:(MZFeld *)feld;
- (CGPoint)shiftPointFromWindshieldViewToShielderContainer:(CGPoint)pointInWindshieldViewCoordinates;

- (NSUInteger)indexOfFeldAtLocation:(CGPoint)location;
- (MZFeld *)closestFeldAtLocation:(CGPoint)location;
- (CGFloat)scaleOfItemView:(AssetView *)assetView atLocation:(CGPoint)location;
- (CGFloat)zoomScaleOfWindshield;

- (CGRect)rectFromNativeWindshieldCoordinatesForItem:(MZWindshieldItem *)item;
- (CGRect)rectToNativeWindshieldCoordinates:(CGRect)frame inFeld:(MZFeld *)feld;

- (CGFloat)borderWidth;

- (void)windshieldItemTapped:(UIGestureRecognizer*)recognizer;
- (void)openAssetViewerForAssetView:(AssetView *)assetView;

- (BOOL)scaleInFullscreenMargins:(CGFloat)scale;
- (CGRect)placeholderRectForObject:(id)object inOverlayWindowWithLocation:(CGPoint)location;
- (void)createIndicatorForOvum:(OBOvum *)ovum atLocation:(CGPoint)location;
- (void)removeIndicator;
- (void)showPlaceholderView:(BOOL)show animated:(BOOL)animated;
- (void)updatePlaceholderViewForObject:(id)object withLocation:(CGPoint)location andScale:(CGFloat)scale;

- (BOOL)isLocationInCorkboardSpace:(CGPoint)location;

@end
