//
//  UIView+Debug.m
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 03/02/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

#import "UIView+Debug.h"

@implementation UIView (Debug)

- (void)debugWithColor:(UIColor *)color
{
  self.layer.borderColor = !color ? [UIColor redColor].CGColor : color.CGColor;
  self.layer.borderWidth = 0.5;
}

@end
