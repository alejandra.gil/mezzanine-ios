//
//  ProgressPieView
//  Mezzanine
//
//  Created by Miguel Sanchez Valdes on 23/07/13.
//  Copyright (c) 2013 Oblong Industries. All rights reserved.
//

#import "ProgressPieView.h"

@implementation ProgressPieView

@synthesize progress;
@synthesize borderWidth;
@synthesize pieColor;
@synthesize activityIndicatorView;
@synthesize state;

- (id)initWithCoder:(NSCoder *)aDecoder
{
  if ((self = [super initWithCoder:aDecoder]))
  {
    [self initialize];
  }
  return self;
}


-(id)initWithFrame:(CGRect)frame
{
  self = [super initWithFrame:frame];
  if (self)
  {
    [self initialize];
  }
  return self;
}


-(void) initialize
{
  [self setBackgroundColor:[UIColor clearColor]];
  borderWidth = 2.0;
  pieColor = [UIColor whiteColor];
  activityIndicatorView = [[UIActivityIndicatorView alloc] initWithFrame:self.bounds];
  [activityIndicatorView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];
  [self addSubview:activityIndicatorView];
  state = ProgressPieStateInvisible;
}

-(void) drawRect:(CGRect)rect
{
  if (state != ProgressPieStateProgress)
    return;
  
  CGContextRef ctx = UIGraphicsGetCurrentContext();
  [pieColor set];

  // Draw circumference
  CGContextSetLineWidth(ctx, borderWidth);
  CGRect pieCircumferenceRect = CGRectMake(borderWidth / 2.0f, borderWidth / 2.0f,
                                   rect.size.width - borderWidth, rect.size.height - borderWidth);
  CGContextStrokeEllipseInRect(ctx, pieCircumferenceRect);
  
  // Calculate progress pie area
	CGPoint center = CGPointMake(CGRectGetMidX(rect), CGRectGetMidY(rect));
	CGFloat radius = floorf((rect.size.width - borderWidth) / 2.0);
  CGFloat origin =  -90.0 * M_PI / 180; // to radians
  CGFloat end = origin + 360.0f * progress * M_PI / 180;
  
  // Draw portion of pie
  UIBezierPath *portionPath = [UIBezierPath bezierPath];
  [portionPath moveToPoint:center];
  [portionPath addArcWithCenter:center radius:radius startAngle:origin endAngle:end clockwise:YES];
  [portionPath closePath];
  
  [portionPath fill];
}


-(void)setProgress:(CGFloat)newProgress
{
  borderWidth = 2.0;
	progress = fmaxf(0.0f, fminf(1.0f, newProgress));
	[self setNeedsDisplay];
}

-(void)setState:(ProgressPieState)aState
{
  state = aState;
  [activityIndicatorView setHidden:!(state == ProgressPieStateActivity)];
  if (state == ProgressPieStateActivity)
    [activityIndicatorView startAnimating];
  else
    [activityIndicatorView stopAnimating];
}


@end
