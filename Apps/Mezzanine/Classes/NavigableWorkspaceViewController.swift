//
//  NavigableWorkspaceViewController.swift
//  Mezzanine
//
//  Created by miguel on 1/3/17.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import UIKit

struct NavigableWorkspaceConstants {
  static let minimumVerticalMargin: CGFloat = 100.0
  static let horizontalMargin: CGFloat = 12.0
  static let verticalOffsetDueToToolbar: CGFloat = 34.0
}

enum ZoomLevel {
  case workspace
  case surface
  case feld
}

class NavigableWorkspaceViewController: UIViewController, UIScrollViewDelegate {

  @IBOutlet var scrollView: UIScrollView!
  @IBOutlet var miniMapView: UIView!
  @IBOutlet var miniMapViewWidthConstraint: NSLayoutConstraint!
  @IBOutlet var zoomInButton: UIButton!
  @IBOutlet var zoomOutButton: UIButton!

  var mainSurfaceModelObserverArray = Array <String> ()
  var systemModel: MZSystemModel? {
    willSet(newSystemModel) {
      if self.systemModel != newSystemModel {
        endObservingSystemModel()
      }
    }
    
    didSet {
      planarWorkspaceViewController.systemModel = systemModel
      surfaceControlsViewController.systemModel = systemModel
      miniMapViewController.systemModel = systemModel
      presentationSliderInteractor.systemModel = systemModel
      beginObservingSystemModel()
    }
  }

  var communicator: MZCommunicator? {
    didSet {
      planarWorkspaceViewController.communicator = communicator
      surfaceControlsViewController.communicator = communicator
      presentationSliderInteractor.communicator = communicator
    }
  }

  let workspaceGeometry = WorkspaceGeometry()

  // Layout
  fileprivate var nextTraitCollection: UITraitCollection!

  // Zoom
  internal fileprivate(set) var zoomLevel: ZoomLevel = .workspace {
    didSet {
      updateZoomButtonsState()
      planarWorkspaceViewController.zoomLevel = zoomLevel
    }
  }

  // Edge scrolling
  fileprivate var edgeScrollingSpeed: Float!
  fileprivate var edgeScrollingTimer: Timer!

  var currentFocus: Any? {
    didSet {
      miniMapViewController.currentFocus = currentFocus
      
      if currentFocus == nil {
        zoomLevel = .workspace
      }
      else if currentFocus is MZSurface {
        zoomLevel = .surface
      }
      else if currentFocus is MZFeld {
        zoomLevel = .feld
      }
    }
  }
  
  //  State
  var state: WorkspaceViewState = .full {
    didSet {
      if state == .preview {
        miniMapView.alpha = 0.0
        zoomInButton.alpha = 0.0
        zoomOutButton.alpha = 0.0
        scrollView.alpha = 0.0
        surfaceControlsViewController.view.alpha = 0.0
      }
      else if state == .full {
        miniMapView.alpha = 1.0
        zoomInButton.alpha = 1.0
        zoomOutButton.alpha = 1.0
        scrollView.alpha = 1.0
        surfaceControlsViewController.view.alpha = 1.0
      }
    }
  }

  var doubleTapOnWorkspace: UITapGestureRecognizer?
  fileprivate var threeFingerPanGesture: UIPanGestureRecognizer!

  // Controllers
  let miniMapViewController = MiniMapViewController()
  let planarWorkspaceViewController = PlanarWorkspaceViewController()
  let surfaceControlsViewController = SurfaceControlsViewController(nibName: "SurfaceControlsViewController", bundle: nil)

  // Interactors
  let presentationSliderInteractor = PresentationSliderInteractor()

  deinit {
    print("NavigableWorkspaceViewController deinit")
    planarWorkspaceViewController.systemModel = nil
    planarWorkspaceViewController.workspaceGeometry = nil

    surfaceControlsViewController.delegate = nil
    surfaceControlsViewController.contentContainerView = nil
    surfaceControlsViewController.surfaces = nil
    surfaceControlsViewController.workspaceGeometry = nil
    surfaceControlsViewController.presentationSliderInteractor = nil

    presentationSliderInteractor.presenter = nil
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    view.backgroundColor = MezzanineStyleSheet.shared().darkestFillColor
    scrollView.backgroundColor = UIColor.clear

    guard let systemModel = systemModel else { return }

    let maxScreenSize = max(UIScreen.main.bounds.height, UIScreen.main.bounds.width)
    let contentFrame = CGRect(x: 0, y: 0, width: maxScreenSize, height: maxScreenSize)

    workspaceGeometry.surfaces = systemModel.surfaces as NSArray as? [MZSurface]
    workspaceGeometry.containerRect = contentFrame

    scrollView.minimumZoomScale = 1.0
    scrollView.maximumZoomScale = maximumZoomScale()
    scrollView.isScrollEnabled = false
    scrollView.panGestureRecognizer.minimumNumberOfTouches = 3
    scrollView.panGestureRecognizer.maximumNumberOfTouches = 3
    scrollView.panGestureRecognizer.isEnabled = false
    scrollView.delegate = self
    scrollView.scrollsToTop = false

    // Planar
    self.addChildViewController(planarWorkspaceViewController)
    planarWorkspaceViewController.containerScrollView = scrollView
    planarWorkspaceViewController.workspaceGeometry = workspaceGeometry
    planarWorkspaceViewController.systemModel = systemModel
    planarWorkspaceViewController.delegate = self

    let workspaceContentView = planarWorkspaceViewController.view
    workspaceContentView?.translatesAutoresizingMaskIntoConstraints = false

    scrollView.addSubview(workspaceContentView!)

    workspaceContentView?.addConstraint(NSLayoutConstraint(item: workspaceContentView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: maxScreenSize))
    workspaceContentView?.addConstraint(NSLayoutConstraint(item: workspaceContentView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: maxScreenSize))
    scrollView.addConstraint(NSLayoutConstraint(item: workspaceContentView, attribute: .centerX, relatedBy: .equal, toItem: scrollView, attribute: .centerX, multiplier: 1.0, constant: 0.0))
    scrollView.addConstraint(NSLayoutConstraint(item: workspaceContentView, attribute: .centerY, relatedBy: .equal, toItem: scrollView, attribute: .centerY, multiplier: 1.0, constant: 0.0))

    planarWorkspaceViewController.didMove(toParentViewController: self)

    let doubleTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(doubleTapGesture(_:)))
    doubleTapGestureRecognizer.numberOfTapsRequired = 2
    workspaceContentView?.addGestureRecognizer(doubleTapGestureRecognizer)
    doubleTapOnWorkspace = doubleTapGestureRecognizer
    planarWorkspaceViewController.tapGesturesThatShouldBeRequiredToFail = [doubleTapGestureRecognizer]
    
    // Minimap
    self.addChildViewController(miniMapViewController)
    miniMapViewController.workspaceGeometry = workspaceGeometry
    miniMapViewController.systemModel = systemModel
    miniMapViewController.initialZoomLevel = possibleZoomLevels().first
    miniMapViewController.delegate = self
    
    let aView = miniMapViewController.view
    aView?.frame = miniMapView.bounds
    miniMapView.addSubview(aView!)
    miniMapViewController.didMove(toParentViewController: self)

    self.addChildViewController(surfaceControlsViewController)
    surfaceControlsViewController.delegate = self
    surfaceControlsViewController.contentContainerView = planarWorkspaceViewController.view
    
    presentationSliderInteractor.systemModel = systemModel
    presentationSliderInteractor.communicator = communicator!
    surfaceControlsViewController.presentationSliderInteractor = presentationSliderInteractor
    
    surfaceControlsViewController.surfaces = systemModel.surfaces
    surfaceControlsViewController.workspaceGeometry = workspaceGeometry
    surfaceControlsViewController.presentationSliderViewController.delegate = (planarWorkspaceViewController.presentationViewController as! PresentationSliderViewControllerDelegate)
    presentationSliderInteractor.presenter = surfaceControlsViewController.presentationSliderViewController

    let surfaceControlsView = surfaceControlsViewController.view
    surfaceControlsView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    surfaceControlsView?.frame = view.bounds
    view.addSubview(surfaceControlsView!)
    surfaceControlsViewController.didMove(toParentViewController: self)

    // Gesture recognizers
    threeFingerPanGesture = UIPanGestureRecognizer(target: self, action: #selector(self.handleThreeFingerDetection(_:)))
    threeFingerPanGesture.delegate = self
    threeFingerPanGesture.minimumNumberOfTouches = 3
    threeFingerPanGesture.maximumNumberOfTouches = 3
    scrollView.addGestureRecognizer(threeFingerPanGesture)

    zoomOutButton.adjustsImageWhenDisabled = false
    zoomInButton.adjustsImageWhenDisabled = false

    setInitialFocusArea()

    updateNavigationControlsVisibility()
    scrollView.zoom(to: currentSnapRect(), animated: false)
  }

  func setInitialFocusArea() {
    guard let systemModel = systemModel else { return }

    let validZoomLevels = possibleZoomLevels()

    // Always start in the top-most level
    if validZoomLevels.first == .workspace {
      currentFocus = nil
    }
    else if validZoomLevels.first == .surface {
      currentFocus = systemModel.surfaces.firstObject as AnyObject
    }
  }

  func updateZoomButtonsState() {
    let validZoomLevels = possibleZoomLevels()

    switch zoomLevel {
    case .workspace:
      zoomInButton.isEnabled = true
      zoomInButton.setImage(UIImage(named: "zoom-in-enabled"), for: UIControlState())
      zoomOutButton.isEnabled = false
      zoomOutButton.setImage(UIImage(named: "zoom-out-disabled"), for: UIControlState())
    case .surface:
      if validZoomLevels.contains(ZoomLevel.workspace) {
        zoomOutButton.isEnabled = true
        zoomOutButton.setImage(UIImage(named: "zoom-out-enabled"), for: UIControlState())
      }
      else {
        zoomOutButton.isEnabled = false
        zoomOutButton.setImage(UIImage(named: "zoom-out-disabled"), for: UIControlState())
      }
      if validZoomLevels.contains(ZoomLevel.feld) {
        zoomInButton.isEnabled = true
        zoomInButton.setImage(UIImage(named: "zoom-in-enabled"), for: UIControlState())
      }
      else {
        zoomInButton.isEnabled = false
        zoomInButton.setImage(UIImage(named: "zoom-in-disabled"), for: UIControlState())
      }
    case .feld:
      zoomOutButton.isEnabled = true
      zoomInButton.isEnabled = false
      zoomInButton.setImage(UIImage(named: "zoom-in-disabled"), for: UIControlState())
      zoomOutButton.setImage(UIImage(named: "zoom-out-enabled"), for: UIControlState())
    }
  }

  func possibleZoomLevels() -> Array<ZoomLevel> {

    var zoomLevels = Array<ZoomLevel>()

    guard let systemModel = systemModel else { return zoomLevels }

    if systemModel.surfaces.count > 1 && self.traitCollection.isiPad() {
      zoomLevels.append(ZoomLevel.workspace)
    }

    zoomLevels.append(ZoomLevel.surface)

    let moreThanOneFeldSurfaces = systemModel.surfaces.filter{($0 as! MZSurface).felds.count > 1}

    if moreThanOneFeldSurfaces.count > 0 {
      zoomLevels.append(ZoomLevel.feld)
    }
    else if systemModel.surfaces.count > 1 && zoomLevels.count == 1 {
      // This is a special case: all surfaces are single feld
      // Valid zoom levels: workspace / surface
      zoomLevels.insert(ZoomLevel.workspace, at: 0)
    }

    return zoomLevels
  }

  func updateNavigationControlsVisibility() {
    let hidden = systemModel?.surfaces.count == 1 && (systemModel?.surfaces.firstObject as! MZSurface).felds.count == 1
    miniMapView.isHidden = hidden
    zoomInButton.isHidden = hidden
    zoomOutButton.isHidden = hidden
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    OperationQueue.main.addOperation {

      self.scrollView.zoom(to: self.currentSnapRect(), animated: false)
      self.surfaceControlsViewController.updateLayout()
      self.planarWorkspaceViewController.updateWelcomeHintView()
      self.planarWorkspaceViewController.updateSharedScreenView()
      self.miniMapViewController.updateLayout()

      self.disablePinching()
      self.disableScrolling()
    }
  }
  
  // MARK: Observation
  
  func beginObservingSystemModel() {

    guard let systemModel = systemModel else { return }
    // felds in main
    let mainSurface = systemModel.surface(withName: "main")
    mainSurfaceModelObserverArray.append(mainSurface!.addObserver(forKeyPath: "felds", options:[.new, .old], on: OperationQueue.main) { ( obj,
      change: [AnyHashable: Any]!) in
      
      self.workspaceGeometry.surfaces = systemModel.surfaces as NSArray as? [MZSurface]
      
      UIView.performWithoutAnimation({
        // re-do zoom
        self.scrollView.maximumZoomScale = self.maximumZoomScale()
        self.setInitialFocusArea()
        self.zoomToCurrentSnapRect()
        
        // update layout
        self.updateNavigationControlsVisibility()
        self.surfaceControlsViewController.updateLayout()
        self.miniMapViewController.updateLayout()
      })
      })
  }
  
  func endObservingSystemModel() {
    if mainSurfaceModelObserverArray.count > 0 {
      let mainSurface = systemModel!.surface(withName: "main")
      for token in mainSurfaceModelObserverArray {
        mainSurface!.removeObserver(withBlockToken: token)
      }
      mainSurfaceModelObserverArray.removeAll()
    }
  }

  // MARK: View Controller Layout

  override func viewDidLayoutSubviews() {
    self.surfaceControlsViewController.updateLayout()
  }

  override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
    return traitCollection.isiPad() ? .all : .allButUpsideDown
  }

  override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
    super.willTransition(to: newCollection, with: coordinator)

    nextTraitCollection = newCollection

    // We don't want it animatedly
    hideControls()

    coordinator.animate(alongsideTransition: { (UIViewControllerTransitionCoordinatorContext) in
      self.zoomToCurrentSnapRect()
    }, completion: nil)
  }

  override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
    super.viewWillTransition(to: size, with: coordinator)

    // We don't want it animatedly
    hideControls()

    coordinator.animate(alongsideTransition: { UIViewControllerTransitionCoordinatorContext in
      self.zoomToCurrentSnapRect()
    }, completion: nil)
  }

  override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
    super.traitCollectionDidChange(previousTraitCollection)
    nextTraitCollection = nil
  }


  // MARK: UIScrollViewDelegate

  func viewForZooming(in scrollView: UIScrollView) -> UIView? {
    return planarWorkspaceViewController.view
  }


  // MARK: Snapping Helpers

  func snapAreaWithRect(_ visibleRect: CGRect, atScale scale: CGFloat ) -> AnyObject? {
    return snapAreaWithLocation(CGPoint(x:visibleRect.midX, y: visibleRect.midY), atScale: scale)
  }

  func snapAreaWithLocation(_ location: CGPoint, atScale scale: CGFloat ) -> AnyObject? {

    let workspaceZoomScale = zoomScaleForZoomLevel(.workspace)
    let surfaceZoomScale = zoomScaleForZoomLevel(.surface)
    let feldZoomScale = zoomScaleForZoomLevel(.feld)

    if (scale < (workspaceZoomScale + surfaceZoomScale) / 2.0) {
      return nil
    }
    else if (scale < (surfaceZoomScale + feldZoomScale) / 2.0) {
      let surface = workspaceGeometry.closestSurfaceForLocation(location)
      return surface!
    }
    else {
      let feld = workspaceGeometry.closestFeldForLocation(location)
      return feld!
    }
  }

  func zoomToCurrentSnapRect() {

    enablePinching()
    hideControls()

    let finalRect = currentSnapRect()
    
    UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseOut, .beginFromCurrentState], animations: {
      self.scrollView.zoom(to: finalRect, animated: false)
      self.planarWorkspaceViewController.updateWelcomeHintView()
      self.planarWorkspaceViewController.updateSharedScreenView()
    }) { (Bool) -> Void in
      self.surfaceControlsViewController.updateLayout()
      self.showControls()
      self.disablePinching()
    }
  }

  func scrollToCurrentSnapRect() {

    enablePinching()

    let currentRect = currentSnapRect()

    var newContentOffset = self.scrollView.contentOffset
    newContentOffset.x = currentRect.origin.x * scrollView.zoomScale + planarWorkspaceViewController.view.frame.minX
    newContentOffset = newContentOffset.roundedPoint()

    if !scrollView.contentOffset.equalTo(newContentOffset) {
      hideControls()
    }

    // TOOD: This works with linear setups but will fail when 2x3. To keep investigating
    // The calculation below is the same than maintaining the current Y content offset.
//    guard let workspaceContainerRect = workspaceGeometry.containerRect else { return }
//    newContentOffset.y = (workspaceContainerRect.height * scrollView.zoomScale - workspaceContainerRect.height) / 2.0 - scrollView.zoomScale

    UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseOut, .beginFromCurrentState], animations: {
      self.scrollView.setContentOffset(newContentOffset, animated: false)
    }) { (Bool) -> Void in
      self.surfaceControlsViewController.updateLayout()

      self.showControls()
      self.disablePinching()
    }
  }

  func currentSnapRect() -> CGRect {

    var snapRect = CGRect.zero

    switch zoomLevel {
    case .workspace:
      snapRect = workspaceSnapRect()
    case .surface:
      snapRect = surfaceSnapRect(currentFocus as! MZSurface)
    case .feld:
      snapRect = feldSnapRect(currentFocus as! MZFeld)
    }

    return snapRect
  }

  func workspaceSnapRect() -> CGRect {

    let rectWorkspace = workspaceGeometry.rectForWorkspace()

    return pageRectForAreaRect(rectWorkspace, maxRect: rectWorkspace)
  }

  func surfaceSnapRect(_ surface: MZSurface) -> CGRect {

    let containerRect = self.scrollView.bounds
    let rect = workspaceGeometry.rectForSurface(surface)

    let maxSurfaceInscribedInView = workspaceGeometry.maxSurfaceInscribedInRectangle(containerRect.size)
    let rectLargestSurface = workspaceGeometry.rectForSurface(maxSurfaceInscribedInView!)

    return pageRectForAreaRect(rect, maxRect: rectLargestSurface)
  }

  func feldSnapRect(_ feld: MZFeld) -> CGRect {

    let containerRect = self.scrollView.bounds
    let rect = workspaceGeometry.rectForFeld(feld)

    let maxFeldInscribedInView = workspaceGeometry.maxFeldInscribedInRectangle(containerRect.size)
    let rectLargestFeld = workspaceGeometry.rectForFeld(maxFeldInscribedInView!)

    return pageRectForAreaRect(rect, maxRect: rectLargestFeld)
  }

  func pageRectForAreaRect(_ rect: CGRect, maxRect: CGRect) -> CGRect {

    let containerRect = self.scrollView.bounds
    let rect = addMarginsIfNeeded(rect)
    let maxRect = addMarginsIfNeeded(maxRect)

    let zoomScale = maxRect.width > maxRect.height ? containerRect.width / maxRect.width : containerRect.height / maxRect.height
    let pageContentSize = pageSizeAtZoomScale(zoomScale)

    if (zoomScale < 1) {
      print("WARNING: zoomScale should not be < 1")
    }

    let originX = rect.origin.x - (pageContentSize.width - rect.width) / 2.0
    let originY = rect.origin.y - (pageContentSize.height - rect.height) / 2.0

    return CGRect(x: originX, y: originY, width: pageContentSize.width, height: pageContentSize.height)
  }

  func addMarginsIfNeeded(_ snapRect: CGRect) -> CGRect {

    // Set a vertical offset to center the workspace in the screen instead of the view assuming the existence of a top toolbar
    var verticalOffset:CGFloat = NavigableWorkspaceConstants.verticalOffsetDueToToolbar
    if traitCollection.isiPhone() {
      if nextTraitCollection != nil {
        verticalOffset = nextTraitCollection.isCompactHeight() ? NavigableWorkspaceConstants.verticalOffsetDueToToolbar : view.safeAreaInsets.top
      }
      else {
        verticalOffset = traitCollection.isCompactHeight() ? view.safeAreaInsets.top : NavigableWorkspaceConstants.verticalOffsetDueToToolbar
      }
    }

    // Leave some vertical margin to fit the surface control bar above the surface view
    if self.view.frame.aspectRatio() > snapRect.aspectRatio() {
      let scale = self.view.safeAreaLayoutGuide.layoutFrame.height / snapRect.height
      return snapRect.insetBy(dx: -NavigableWorkspaceConstants.horizontalMargin / scale, dy: -NavigableWorkspaceConstants.minimumVerticalMargin / scale).offsetBy(dx: 0, dy: verticalOffset / scale)
    }
    else {
      let scale = self.view.safeAreaLayoutGuide.layoutFrame.width / snapRect.width
      return snapRect.insetBy(dx: -NavigableWorkspaceConstants.horizontalMargin / scale, dy: 0).offsetBy(dx: 0, dy: verticalOffset / scale)
    }
  }

  func pageSizeAtZoomScale(_ scale: CGFloat) -> CGSize {
    var addWidth: CGFloat = 0.0
    var addHeight: CGFloat = 0.0

    if (zoomLevel == .feld && traitCollection.isCompactHeight()) {
        addWidth =  view.safeAreaInsets.left + view.safeAreaInsets.right
        addHeight =  view.safeAreaInsets.top + view.safeAreaInsets.bottom
    }
    
    return CGSize(width: (self.view.safeAreaLayoutGuide.layoutFrame.width + addWidth) / scale, height: (self.view.safeAreaLayoutGuide.layoutFrame.height + addHeight) / scale)
  }

  func maximumZoomScale() -> CGFloat {

    guard let smallerRectInscribed = workspaceGeometry.minFeldInscribedInRectangle(scrollView.bounds.size) else { return 1.0 }
    let smallerFeldRect = workspaceGeometry.rectForFeld(smallerRectInscribed)

    let workspaceRect = workspaceGeometry.rectForWorkspace()

    return max((workspaceRect.width + 2 * workspaceGeometry.surfaceBorderHorizontalMargin()) / smallerFeldRect.width, workspaceRect.height / smallerFeldRect.height) + 10
  }

  // MARK: Surface Controls

  func hideControls() {
      self.surfaceControlsViewController.view.alpha = 0.0
  }

  func showControls() {
    UIView.animate(withDuration: 0.1, animations: {
      self.surfaceControlsViewController.view.alpha = 1.0
    }) 
  }

  func zoomScaleForZoomLevel(_ aZoomLevel: ZoomLevel) -> CGFloat {

    switch aZoomLevel {
    case .workspace:
      return zoomScaleForSize(workspaceGeometry.rectForWorkspace().size)
    case .surface:
      let maxSurfaceInscribedInView = workspaceGeometry.maxSurfaceInscribedInRectangle(self.scrollView.bounds.size)
      let rectLargestSurface = workspaceGeometry.rectForSurface(maxSurfaceInscribedInView!)
      return zoomScaleForSize(rectLargestSurface.size)
    case .feld:
      let maxFeldInscribedInView = workspaceGeometry.maxFeldInscribedInRectangle(self.scrollView.bounds.size)
      let rectLargestFeld = workspaceGeometry.rectForFeld(maxFeldInscribedInView!)
      return zoomScaleForSize(rectLargestFeld.size)
    }
  }

  func zoomScaleForSize(_ size: CGSize) -> CGFloat {

    if self.scrollView.bounds.aspectRatio() > size.aspectRatio() {
      return self.view.safeAreaLayoutGuide.layoutFrame.height / size.height
    }
    else {
      return self.view.safeAreaLayoutGuide.layoutFrame.width / size.width
    }
  }


  // MARK: Gestures

  @objc func doubleTapGesture(_ recognizer: UIGestureRecognizer) {
    if recognizer.state == .recognized {

      let touchLocation = recognizer.location(in: recognizer.view)

      var sameLevelTarget: Any?
      var innerLevelTarget: Any?

      switch zoomLevel {
      case .workspace:
        sameLevelTarget = nil
        innerLevelTarget = workspaceGeometry.surfaceForLocation(touchLocation)
      case .surface:
        sameLevelTarget = workspaceGeometry.surfaceForLocation(touchLocation)
        innerLevelTarget = possibleZoomLevels().contains(ZoomLevel.feld) ? workspaceGeometry.feldForLocation(touchLocation) : nil
      case .feld:
        sameLevelTarget = workspaceGeometry.feldForLocation(touchLocation)
        innerLevelTarget = possibleZoomLevels().contains(ZoomLevel.workspace) ? nil : workspaceGeometry.surfaceForLocation(touchLocation)
      }

      if sameLevelTarget as AnyObject === currentFocus as AnyObject {
        zoomInToTarget(innerLevelTarget)
      }
      else if sameLevelTarget == nil {
        zoomOut()
      }
      else if sameLevelTarget as AnyObject !== currentFocus as AnyObject {
        currentFocus = sameLevelTarget
        scrollToCurrentSnapRect()
      }
    }
  }

  func zoomInToTarget(_ target: Any?) {
    if zoomLevel == .workspace {
      guard let surface = target as? MZSurface else { return }
      currentFocus = surface.felds.count > 1 ? surface : surface.felds.firstObject
    }
    else if zoomLevel == .surface {
      currentFocus = target
    }
    else if zoomLevel == .feld {
      currentFocus = target
    }
    zoomToCurrentSnapRect()
  }

  func zoomOut() {
    if zoomLevel == .surface && possibleZoomLevels().contains(ZoomLevel.workspace) {
      currentFocus = nil
      zoomToCurrentSnapRect()
    }
    else if zoomLevel == .feld {
      let surface = systemModel?.surface(with: currentFocus as! MZFeld)
      currentFocus = surface?.felds.count == 1 && possibleZoomLevels().contains(ZoomLevel.workspace) ? nil : surface
      zoomToCurrentSnapRect()
    }
  }

  @objc func handleThreeFingerDetection(_ gestureRecognizer: UIPanGestureRecognizer) {
    // TODO: Add advance gesture to go to next focus area
  }

  // MARK: Edge scrolling
  fileprivate func beginEdgeScrolling() {
    if edgeScrollingTimer != nil {
      return
    }

    edgeScrollingSpeed = 0.0
    edgeScrollingTimer = Timer.scheduledTimer(timeInterval: 0.03, target: self, selector: #selector(self.handleEdgeScrollingTimer), userInfo: nil, repeats: true)
  }

  fileprivate func endEdgeScrolling() {
    if edgeScrollingTimer != nil {
      edgeScrollingTimer.invalidate()
      edgeScrollingTimer = nil
    }
  }

  func minContentOffsetForCurrentZoomLevel() -> CGFloat {
    var rect: CGRect
    if self.currentFocus is MZFeld {
      let firstSurface = systemModel?.surfaces.firstObject as! MZSurface
      let firstFeld = firstSurface.felds.firstObject as! MZFeld
      rect = feldSnapRect(firstFeld)
    }
    else if self.currentFocus is MZSurface {
      let firstSurface = systemModel?.surfaces.firstObject as! MZSurface
      rect = surfaceSnapRect(firstSurface)
    }
    else {
      rect = workspaceSnapRect()
    }
    return rect.minX
  }

  func maxContentOffsetForCurrentZoomLevel() -> CGFloat {
    var rect: CGRect
    if self.currentFocus is MZFeld {
      let firstSurface = systemModel?.surfaces.lastObject as! MZSurface
      let firstFeld = firstSurface.felds.lastObject as! MZFeld
      rect = feldSnapRect(firstFeld)
    }
    else if self.currentFocus is MZSurface {
      let firstSurface = systemModel?.surfaces.lastObject as! MZSurface
      rect = surfaceSnapRect(firstSurface)
    }
    else {
      rect = workspaceSnapRect()
    }
    return rect.minX
  }

  @objc func handleEdgeScrollingTimer(_ timer: Timer) {
    var contentOffset = scrollView.contentOffset
    contentOffset.x += CGFloat(edgeScrollingSpeed)

    let maxContentOffsetX = maxContentOffsetForCurrentZoomLevel() * scrollView.zoomScale + planarWorkspaceViewController.view.frame.minX
    contentOffset.x = min(maxContentOffsetX, contentOffset.x)

    let minContentOffsetX = minContentOffsetForCurrentZoomLevel() * scrollView.zoomScale + planarWorkspaceViewController.view.frame.minX
    contentOffset.x = max(minContentOffsetX, contentOffset.x)

    // Hide controls only when auto scroll has started
    if !scrollView.contentOffset.equalTo(contentOffset) {
      hideControls()
    }

    scrollView.setContentOffset(contentOffset, animated: false)

    planarWorkspaceViewController.updateOnEdgeScrolling()
  }

  fileprivate func handleEdgeScrollingEventAtLocation(_ location: CGPoint) {

    if zoomLevel == .workspace {
      return
    }

    let scrollZone = UIDevice.current.isIPad() ? CGFloat(120.0) : CGFloat(64.0)
    let maxScrollSpeed = UIDevice.current.isIPad() ? CGFloat(24.0) : CGFloat(12.0)

    // Not actually sure why view location is affected by contentOffset, since this is supposed to be in the UIScrollView's coordinate feld ...
    var loc = location
    loc.x -= scrollView.contentOffset.x

    let viewWidth = scrollView.frame.size.width

    if (loc.x < scrollZone)
    {
      edgeScrollingSpeed = Float(-(scrollZone - loc.x) / scrollZone * maxScrollSpeed)
      beginEdgeScrolling()
    }
    else if (loc.x > viewWidth - scrollZone)
    {
      edgeScrollingSpeed = Float((loc.x - (viewWidth - scrollZone)) / scrollZone * maxScrollSpeed)
      beginEdgeScrolling()
    }
    else
    {
      edgeScrollingSpeed = 0.0
      endEdgeScrolling()
    }
  }

  fileprivate func updateFeldIndexWithLocation(_ location: CGPoint) {
    endEdgeScrolling()

    if zoomLevel == .surface {
      let surface = workspaceGeometry.closestSurfaceForLocation(location)
      if (currentFocus as? MZSurface) != surface {
        currentFocus = surface
      }
      scrollToCurrentSnapRect()
    } else if zoomLevel == .feld {
      let feld = workspaceGeometry.closestFeldForLocation(location)
      if (currentFocus as? MZFeld) != feld {
        currentFocus = feld
      }
      scrollToCurrentSnapRect()
    }
    else {
      showControls()
    }
  }


  // MARK: Button Actions 

  @IBAction func zoomInButtonAction(_ sender: UIButton) {
    var target: Any?
    switch zoomLevel {
    case .workspace:
      target = systemModel?.surfaces.firstObject as Any
    case .surface:
      let surface = currentFocus as! MZSurface
      let felds = surface.felds as! [MZFeld]
      let feldsNamedMain: [MZFeld] = felds.filter{($0.name as String) == "main"} as [MZFeld]
      target = feldsNamedMain.count > 0 ? feldsNamedMain.first : surface.felds.firstObject
    case .feld:
      break
    }
    zoomInToTarget(target)
  }

  @IBAction func zoomOutButtonAction(_ sender: UIButton) {
    zoomOut()
  }
}

extension NavigableWorkspaceViewController: MiniMapViewControllerDelegate {
  
  func miniMapDidTap(_ feld: MZFeld?, surface: MZSurface?) {
    if zoomLevel == .surface {
      if (currentFocus as? MZSurface) != surface {
        currentFocus = surface
        scrollToCurrentSnapRect()
      }
    } else if zoomLevel == .feld {
      if (currentFocus as? MZFeld) != feld {
        currentFocus = feld
        scrollToCurrentSnapRect()
      }
    }
  }

  func miniMapDidSetContentFrame(_ contentFrame: CGRect) {
    miniMapViewWidthConstraint.constant = contentFrame.width
  }
  
  func miniMapDidMagnified(_ isMagnified: Bool) {
    UIView.animate(withDuration: 0.15, animations: {
      self.zoomInButton.alpha = isMagnified ? 0.0 : 1.0
      self.zoomOutButton.alpha = isMagnified ? 0.0 : 1.0
    }) 
  }
}

extension NavigableWorkspaceViewController: PlanarWorkspaceViewControllerDelegate {

  func planarWorkspaceContainerZoomScale() -> CGFloat {
    return scrollView.zoomScale
  }

  func planarWorkspaceContainerScrollView() -> UIScrollView {
    return scrollView
  }

  func planarWorkspaceNavigableContainer() -> NavigableWorkspaceViewController {
    return self
  }
}

extension NavigableWorkspaceViewController: SurfaceControlsViewControllerDelegate {

  func currentFocusArea() -> Any? {
    return currentFocus
  }
}

// MARK: UIGestureRecognizerDelegate
extension NavigableWorkspaceViewController: UIGestureRecognizerDelegate {


  func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
    return true
  }

  func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {

    if gestureRecognizer == threeFingerPanGesture {
      return false
    }

    return true
  }
}

// MARK: WindshieldViewControllerDelegate
extension NavigableWorkspaceViewController: WindshieldViewControllerDelegate {

  func windshieldViewUpdateSpaceIndex(withLocation location: CGPoint) {
    updateFeldIndexWithLocation(location)
  }

  func windshieldViewPresentDragDropDeletionArea(withDeletion allowDeletion: Bool) {
    // show delete area in the container
  }

  func windshieldViewHideDragDropDeletionArea() {
    // hide delete area in the container
  }

  func windshieldViewEndEdgeScrolling() {
    endEdgeScrolling()
  }

  func windshieldViewEdgeScrollingEvent(atLocation location: CGPoint) {
    handleEdgeScrollingEventAtLocation(location)
  }
}

// MARK: Navigation Enablers

extension NavigableWorkspaceViewController {

  func enablePinching() {
    scrollView.minimumZoomScale = 1.0
    scrollView.maximumZoomScale = maximumZoomScale()
  }

  func disablePinching() {
    scrollView.minimumZoomScale = scrollView.zoomScale
    scrollView.maximumZoomScale = scrollView.zoomScale
  }

  func enableScrolling() {
    scrollView.isScrollEnabled = true
  }

  func disableScrolling() {
    scrollView.isScrollEnabled = false
  }
}


extension CGRect {
  
  func aspectRatio() -> CGFloat {
    return self.width / self.height
  }

  func distanceWithRect(_ anotherRect: CGRect) -> CGFloat {
    let center1 = CGPoint(x: self.midX, y: self.midY)
    let center2 = CGPoint(x: anotherRect.midX, y: anotherRect.midY)

    let horizontalDistance = (center2.x - center1.x)
    let verticalDistance = (center2.y - center1.y)

    return sqrt((horizontalDistance * horizontalDistance) + (verticalDistance * verticalDistance))
  }

  func scaleRect(_ scale: CGFloat) -> CGRect {
    return CGRect(x: origin.x * scale, y: origin.y * scale, width: size.width * scale, height: size.height * scale)
  }
}

extension CGSize {

  func aspectRatio() -> CGFloat {
    return self.width / self.height
  }
}
