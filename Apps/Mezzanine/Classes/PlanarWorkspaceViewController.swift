//
//  PlanarWorkspaceViewController.swift
//  Mezzanine
//
//  Created by miguel on 6/3/17.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import UIKit

protocol PlanarWorkspaceViewControllerDelegate: NSObjectProtocol {

  func planarWorkspaceContainerZoomScale() -> CGFloat
  func planarWorkspaceContainerScrollView() -> UIScrollView
  func planarWorkspaceNavigableContainer() -> NavigableWorkspaceViewController
}

class PlanarWorkspaceViewController: UIViewController {

  @objc var containerScrollView: UIScrollView?
  var welcomeHintView: WelcomeHintView!
  let sharedScreenIndicatorView = SharedScreenIndicatorView()
  var zoomLevel: ZoomLevel = .workspace {
    didSet {
      sharedScreenIndicatorView.zoomLevel = zoomLevel
    }
  }
  
  weak var delegate: PlanarWorkspaceViewControllerDelegate?

  var presentationPanGestureRecognizer: UIPanGestureRecognizer?

  var tapGesturesThatShouldBeRequiredToFail = Array<UITapGestureRecognizer>() {
    didSet {
      fullWindshieldViewController?.tapGesturesThatShouldBeRequiredToFail = tapGesturesThatShouldBeRequiredToFail
      presentationViewController?.tapGesturesThatShouldBeRequiredToFail = tapGesturesThatShouldBeRequiredToFail
    }
  }

  var systemModelObserverArray = Array <String> ()
  var mainSurfaceModelObserverArray = Array <String> ()
  var systemModel: MZSystemModel? {
    willSet(newSystemModel) {
      if self.systemModel != newSystemModel {
        endObservingSystemModel()
      }
    }
    
    didSet {
      if systemModel != nil {
        workspace = systemModel!.currentWorkspace
        beginObservingSystemModel()
      }
      else {
        self.workspace = nil
        fullWindshieldViewController?.systemModel = nil
      }
    }
  }

  var workspaceObserverArray = Array <String> ()
  var workspace: MZWorkspace? {
    willSet(newWorkspace) {
      if self.workspace != newWorkspace {
        endObservingWorkspace()
      }
    }
    
    didSet {
      guard let workspace = workspace else {
        presentationViewController?.workspace = nil
        fullWindshieldViewController?.workspace = nil
        fullWindshieldViewController?.windshield = nil
        return
      }
      
      beginObservingWorkspace()
      presentationViewController?.workspace = workspace
      fullWindshieldViewController?.workspace = workspace
      fullWindshieldViewController?.windshield = workspace.windshield
    }
  }

  var communicator: MZCommunicator? {
    didSet {
      presentationViewController?.communicator = communicator
      fullWindshieldViewController?.communicator = communicator
    }
  }

  var workspaceGeometry: WorkspaceGeometry? {
    didSet {
      surfaceViewController.workspaceGeometry = workspaceGeometry
      fullWindshieldViewController?.workspaceGeometry = workspaceGeometry
      presentationViewController?.workspaceGeometry = workspaceGeometry
    }
  }

  // Controllers
  var fullWindshieldViewController: FullWindshieldViewController?
  var surfaceViewController = SurfaceContainerViewController()
  var presentationViewController: TWPresentationViewController?

  deinit {
    print("PlanarWorkspaceViewController deinit")

    fullWindshieldViewController?.systemModel = nil
    fullWindshieldViewController?.fullWindshieldDelegate = nil
    fullWindshieldViewController?.delegate = nil
    fullWindshieldViewController?.panGestureThatShouldBeRequiredToFailByItemPanningGestures = nil

    fullWindshieldViewController?.tapGesturesThatShouldBeRequiredToFail = nil
    presentationViewController?.tapGesturesThatShouldBeRequiredToFail = nil

    presentationViewController?.communicator = nil
    presentationViewController?.systemModel = nil
    presentationViewController?.planarWorkspaceViewController = nil
    presentationViewController?.delegate = nil

    surfaceViewController.containerScrollView = nil
    surfaceViewController.surfaces = nil

    sharedScreenIndicatorView.workspaceGeometry = nil
    sharedScreenIndicatorView.surface = nil
    sharedScreenIndicatorView.containerScrollView = nil

    endObservingSystemModel()
    endObservingWorkspace()
  }

  
  override func viewDidLoad() {
    super.viewDidLoad()

    view.backgroundColor = UIColor.clear

    guard let systemModel = systemModel else { return }
    guard let containerScrollView = containerScrollView else { return }
    guard let workspaceGeometry = workspaceGeometry else { return }
    guard let delegate = delegate else { return }

    self.addChildViewController(surfaceViewController)
    surfaceViewController.workspaceGeometry = workspaceGeometry
    surfaceViewController.containerScrollView = containerScrollView
    let surfacesView = surfaceViewController.view
    surfacesView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    surfacesView?.frame = workspaceGeometry.containerRect!
    view.addSubview(surfacesView!)
    surfaceViewController.didMove(toParentViewController: self)
    surfaceViewController.surfaces = systemModel.surfaces

    let presentationVC = TWPresentationViewController()
    presentationVC.delegate = self
    presentationVC.communicator = communicator
    presentationVC.workspaceGeometry = workspaceGeometry
    presentationVC.planarWorkspaceViewController = self

    self.addChildViewController(presentationVC)
    let presentationView = presentationVC.view
    presentationView?.frame = workspaceGeometry.containerRect!
    view.addSubview(presentationView!)
    presentationVC.didMove(toParentViewController: self)
    presentationViewController = presentationVC

    welcomeHintView = WelcomeHintView()
    view.addSubview(welcomeHintView)
    
    let fullWindshieldVC = FullWindshieldViewController()
    fullWindshieldVC.communicator = communicator
    fullWindshieldVC.workspaceGeometry = workspaceGeometry
    fullWindshieldVC.systemModel = systemModel
    fullWindshieldVC.fullWindshieldDelegate = self
    fullWindshieldVC.delegate = delegate.planarWorkspaceNavigableContainer()

    self.addChildViewController(fullWindshieldVC)
    let fullWindshieldView = fullWindshieldVC.view
    fullWindshieldView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    fullWindshieldView?.frame = workspaceGeometry.containerRect!
    view.addSubview(fullWindshieldView!)
    fullWindshieldVC.didMove(toParentViewController: self)
    fullWindshieldViewController = fullWindshieldVC
    
    sharedScreenIndicatorView.workspaceGeometry = workspaceGeometry
    sharedScreenIndicatorView.surface = systemModel.surface(withName: "main")
    sharedScreenIndicatorView.containerScrollView = containerScrollView
    view.addSubview(sharedScreenIndicatorView)

    let presentationPanGesture = UIPanGestureRecognizer(target: presentationVC, action: #selector(presentationVC.handleDeckPanGesture(_:)))
    presentationPanGesture.minimumNumberOfTouches = 1
    presentationPanGesture.maximumNumberOfTouches = 1
    presentationPanGesture.delegate = self
    view.addGestureRecognizer(presentationPanGesture)
    presentationPanGestureRecognizer = presentationPanGesture

    fullWindshieldVC.panGestureThatShouldBeRequiredToFailByItemPanningGestures = presentationPanGestureRecognizer
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    surfaceViewController.updateSurfacesLayout()

    presentationViewController?.systemModel = systemModel
    presentationViewController?.workspace = workspace
    presentationViewController?.updateLayout()
  }
  
  // MARK: Observation
  
  func beginObservingSystemModel() {

    // infopresence.status
    systemModelObserverArray.append(systemModel!.addObserver(forKeyPath: "infopresence.status", options:[.new, .old], on: OperationQueue.main) { ( obj, change: [AnyHashable: Any]!) in
      self.updateWelcomeHintView()
      self.updateSharedScreenView()
    })

    // currentWorkspace
    systemModelObserverArray.append(systemModel!.addObserver(forKeyPath: "currentWorkspace", options:[.new], on: OperationQueue.main) {(obj, change: [AnyHashable: Any]!) in
      let systemModel = obj as! MZSystemModel
      self.workspace = systemModel.currentWorkspace
      self.updateWelcomeHintView()
    })
    
    // felds in main
    let mainSurface = systemModel?.surface(withName: "main")
    mainSurfaceModelObserverArray.append(mainSurface!.addObserver(forKeyPath: "felds", options:[.new, .old], on: OperationQueue.main) { ( obj,
      change: [AnyHashable: Any]!) in
      self.workspaceGeometry!.surfaces = self.systemModel!.surfaces as NSArray as? [MZSurface]
      
      UIView.performWithoutAnimation({
        self.surfaceViewController.insertSurfaces()
        self.presentationViewController?.updateLayout()
        
        self.updateWelcomeHintView()
        self.updateSharedScreenView()
      })
    })
  }
  
  func endObservingSystemModel() {
    if systemModelObserverArray.count > 0 {
      for token in systemModelObserverArray {
        systemModel!.removeObserver(withBlockToken: token)
      }
      systemModelObserverArray.removeAll()
    }
    if mainSurfaceModelObserverArray.count > 0 {
      guard let mainSurface = systemModel?.surface(withName: "main") else {
        print("Planar Workspace Error: Leaked observer on main surface")
        return
      }
      for token in mainSurfaceModelObserverArray {
        mainSurface.removeObserver(withBlockToken: token)
      }
      mainSurfaceModelObserverArray.removeAll()
    }
  }
  
  func beginObservingWorkspace() {
    workspaceObserverArray.append(workspace!.addObserver(forKeyPath: "presentation.active", options:[.new, .old], on: OperationQueue.main) { ( obj, change: [AnyHashable: Any]!) in
      self.updateWelcomeHintView()
      })
    
    workspaceObserverArray.append(workspace!.addObserver(forKeyPath: "windshield.items", options:[.new, .old], on: OperationQueue.main) { ( obj, change: [AnyHashable: Any]!) in
      self.updateWelcomeHintView()
      })
  }
  
  func endObservingWorkspace() {
    if workspaceObserverArray.count > 0 {
      for token in workspaceObserverArray {
        workspace!.removeObserver(withBlockToken: token)
      }
      
      workspaceObserverArray.removeAll()
    }
  }

  // MARK: Edge Scrolling 

  func updateOnEdgeScrolling() {
    fullWindshieldViewController?.updateOnEdgeScrolling()
  }
  
  // MARK: Shared M2M view
  
  func updateSharedScreenView() {
    if systemModel?.infopresence.status != .active {
      sharedScreenIndicatorView.alpha = 0.0
    } else {
      sharedScreenIndicatorView.alpha = 1.0
      UIView.performWithoutAnimation({
        sharedScreenIndicatorView.updateLayout()
      })
    }
  }
  
  
  // MARK: Welcome placeholder
  // TODO: Move to its class?
  func updateWelcomeHintView() {
    guard let workspaceGeometry = workspaceGeometry else { return }
    guard let fullWindshieldViewController = fullWindshieldViewController else { return }
    guard let windshield = fullWindshieldViewController.windshield else { return }
    guard let containerScrollView = containerScrollView else { return }
    guard let mainSurface = mainSurface() else { return }
    guard let presentation = presentationViewController?.presentation else { return }
    
    var frame = CGRect.zero
    if mainSurface.felds.count == 1 {
      frame = workspaceGeometry.roundedFrameForSurface(mainSurface)
    } else {
      frame = workspaceGeometry.roundedFrameForFeld(mainSurface.felds[1] as! MZFeld)
    }
    
    let zoomScale = max(self.welcomeHintView.zoomScale, containerScrollView.zoomScale)
    welcomeHintView.zoomScale = zoomScale
    let alpha: CGFloat = presentation.active == true || windshield.items.count > 0 ? 0.0 : 1.0
    welcomeHintView.alpha = alpha
    welcomeHintView.frame = frame
    
    let scale = UIScreen.main.scale * zoomScale
    welcomeHintView.titleLabel.layer.contentsScale = scale
    welcomeHintView.titleLabel.layer.setNeedsDisplay()
  }
  
  func mainSurface() -> MZSurface? {
    for surface in systemModel!.surfaces! {
      let surface = surface as! MZSurface
      if surface.name == "main" {
        return surface
      }
    }
    
    return nil
  }
}

extension PlanarWorkspaceViewController: FullWindshieldViewControllerDelegate {

  func windshieldContainerZoomScale() -> CGFloat {

    guard let delegate = delegate else { return 1.0 }
    
    return delegate.planarWorkspaceContainerZoomScale()
  }

  func windshieldContainerScrollView() -> UIScrollView! {

    guard let delegate = delegate else { return nil }

    return delegate.planarWorkspaceContainerScrollView()
  }
}

extension PlanarWorkspaceViewController: PresentationViewControllerDelegate {
  func presentationViewControllerZoomState() -> CGFloat {
    return 1.0
  }
  
  func presentationViewUpdateWorkspaceViewControllerDeckSlider() {
    // Not used in Teamwork UI
  }
}

extension PlanarWorkspaceViewController: UIGestureRecognizerDelegate {

  func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
    return true
  }

  func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {

    return true
  }

  func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRequireFailureOf otherGestureRecognizer: UIGestureRecognizer) -> Bool {
    if otherGestureRecognizer.accessibilityLabel == "WindshieldItemPanGesture" {
      return true
    }
    if otherGestureRecognizer.accessibilityLabel == "WindshieldItemPinchGesture" {
      return true
    }
    return false
  }
}


