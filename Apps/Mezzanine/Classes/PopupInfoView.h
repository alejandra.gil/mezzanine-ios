//
//  PopupInfoView.h
//  Mezzanine
//
//  Created by Zai Chang on 7/5/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>


// A temporary popup view has a timer to self dismiss
// Used for short informational messages that should not be modally displayed
// 
// TODO - Add ability for caller to adjust view style
// TODO - Add the ability for it to become modal if necessary, and add a close button
//
@interface PopupInfoView : UIView

@property (nonatomic, strong) UIWindow *overlayWindow;

- (void)displayMessage:(NSString*)message withTimeout:(NSTimeInterval)timeoutInterval;

@end
