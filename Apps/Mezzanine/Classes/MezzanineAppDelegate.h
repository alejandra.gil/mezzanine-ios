//
//  MezzanineAppDelegate.h
//  Mezzanine
//
//  Created by Zai Chang on 9/17/10.
//  Copyright 2010 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MezzanineAppContext.h"
#import "MezzanineStyleSheet.h"


@interface MezzanineAppDelegate : NSObject <UIApplicationDelegate>
{
  MezzanineAppContext *appContext;
  UIBackgroundTaskIdentifier backgroundTaskIdentifier;
}

@property (strong, nonatomic) UIWindow *window;


@end

