//
//  VideoPlaceholderView.h
//  Mezzanine
//
//  Created by Zai Chang on 8/20/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface VideoPlaceholderLayer : CALayer

@property (nonatomic, strong) NSString *sourceName;
@property (nonatomic, strong) NSString *sourceOrigin;
@property (nonatomic, assign) BOOL isLocalVideo;

@property (nonatomic, assign) CGFloat maxTitleFontSize;
@property (nonatomic, assign) CGFloat minTitleFontSize;
@property (nonatomic, assign) CGFloat minSubtitleFontSize;

@property (nonatomic, assign) CGFloat containerZoomScale;

@end
