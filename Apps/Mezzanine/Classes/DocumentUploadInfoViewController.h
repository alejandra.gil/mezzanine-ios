//
//  DocumentUploadInfoViewController.h
//  Mezzanine
//
//  Created by Zai Chang on 5/14/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DocumentUploadInfoViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UITextView *textView;

- (IBAction)close:(id)sender;

@end
