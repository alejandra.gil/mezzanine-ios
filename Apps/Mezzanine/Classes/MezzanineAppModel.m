//
//  MezzanineAppModel.m
//  Mezzanine
//
//  Created by miguel on 15/10/13.
//  Copyright (c) 2013 Oblong Industries. All rights reserved.
//

#import "MezzanineAppModel.h"

@implementation MezzanineAppModel

@synthesize systemModel;

-(id)init
{
  self = [super init];
  if (self)
  {
    self.systemModel = [[MZSystemModel alloc] init];
  }
  return self;
}

-(void)reset
{
  [self.systemModel reset];
}


@end
