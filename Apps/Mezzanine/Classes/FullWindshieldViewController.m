//
//  FullWindshieldViewController.m
//  Mezzanine
//
//  Created by miguel on 6/3/17.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

#import "FullWindshieldViewController.h"
#import "WindshieldViewController_Private.h"
#import "UIGestureRecognizer+OBDragDrop.h"
#import "Mezzanine-Swift.h"

@interface FullWindshieldViewController ()

- (void)addGestureRecognizers:(AssetView *)itemView;

@end

@implementation FullWindshieldViewController

@synthesize fullWindshieldDelegate;

-(CGFloat) borderWidth
{
  return 0;
}

- (CGFloat)zoomScaleOfWindshield
{
  if ([fullWindshieldDelegate respondsToSelector:@selector(windshieldContainerZoomScale)])
    return [fullWindshieldDelegate windshieldContainerZoomScale];

  return 1.0;
}

- (void)updateCropViewForItem:(MZWindshieldItem *)item
{
  CGRect visibleRect = [_workspaceGeometry roundedFrameForSurface:item.surface];
  AssetView *assetView = [self viewForItem:item];
  [self cropView:assetView rect:visibleRect];
}

- (void)cropView:(UIView *)itemView rect:(CGRect)rect
{
  // Create a mask layer and the frame to determine what will be visible in the view.
  CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];

  CGFloat x = CGRectGetMinX(itemView.frame) < 0 ? fabs(CGRectGetMinX(itemView.frame)) + CGRectGetMinX(rect) : MAX(0, CGRectGetMinX(rect) - CGRectGetMinX(itemView.frame));
  CGFloat y = CGRectGetMinY(itemView.frame) < 0 ? fabs(CGRectGetMinY(itemView.frame)) + CGRectGetMinY(rect) : MAX(0, CGRectGetMinY(rect) - CGRectGetMinY(itemView.frame));
  CGFloat w = MIN(CGRectGetMaxX(itemView.frame),CGRectGetMaxX(rect)) - MAX(MAX(CGRectGetMinX(rect), CGRectGetMinX(itemView.frame)),x);
  w += CGRectGetMinX(itemView.frame) < 0 ? fabs(CGRectGetMinX(itemView.frame)) : 0;
  CGFloat h = MIN(CGRectGetMaxY(itemView.frame),CGRectGetMaxY(rect)) - MAX(MAX(CGRectGetMinY(rect), CGRectGetMinY(itemView.frame)),y);
  h += CGRectGetMinY(itemView.frame) < 0 ? fabs(CGRectGetMinY(itemView.frame)) : 0;

  CGRect maskRect = CGRectMake(x,y,w,h);

  // If no mask is needed, don't set it and remove any previous one.
  if (CGRectEqualToRect(itemView.frame, maskRect))
  {
    itemView.layer.mask = nil;
  }
  else
  {
    CGPathRef path = CGPathCreateWithRect(maskRect, NULL);
    maskLayer.path = path;
    CGPathRelease(path);
    itemView.layer.mask = maskLayer;
  }
}

- (void)loadAllWindshieldItems
{
  [self removeAllItemViews];
  [self.windshield.items enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
    [self insertViewForItem:obj];
  }];
}

- (void)insertViewForItem:(MZWindshieldItem *)item
{
  [super insertViewForItem:item];
  [self updateCropViewForItem:item];
}

- (void)addGestureRecognizers:(AssetView *)itemView
{
  UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(windshieldItemTapped:)];
  tapGestureRecognizer.accessibilityLabel = @"WindshieldItemTapGesture";
  tapGestureRecognizer.delegate = self;
  tapGestureRecognizer.numberOfTapsRequired = 1;
  tapGestureRecognizer.numberOfTouchesRequired = 1;
  [itemView addGestureRecognizer:tapGestureRecognizer];

  OBDragDropManager *dragDropManager = [OBDragDropManager sharedManager];
  UIPanGestureRecognizer * gesture = (UIPanGestureRecognizer *)[dragDropManager createDragDropGestureRecognizerWithClass:[UIPanGestureRecognizer class] source:self];
  gesture.delegate = self;
  gesture.accessibilityLabel = @"WindshieldItemPanGesture";
  gesture.maximumNumberOfTouches = 2;
  [itemView addGestureRecognizer:gesture];

  // Also adding a UIPinchGestureRecognizer such that a user can pinch and immeidately scale a windshield item
  // without having to drag / move it first
  UIPinchGestureRecognizer *pinchGesture = (UIPinchGestureRecognizer *)[dragDropManager createDragDropGestureRecognizerWithClass:[UIPinchGestureRecognizer class] source:self];
  pinchGesture.delegate = self;
  pinchGesture.accessibilityLabel = @"WindshieldItemPinchGesture";
  [itemView addGestureRecognizer:pinchGesture];
}


#pragma mark - 
#pragma mark Setters

- (void)setSystemModel:(MZSystemModel *)systemModel
{
  [super setSystemModel:systemModel];
  
  self.workspace = systemModel.currentWorkspace;
  self.windshield = systemModel.currentWorkspace.windshield;
}


#pragma mark -
#pragma mark Size Helpers

- (CGFloat)windshieldFeldWidth
{
  MZSurface *mainSurface = [_systemModel surfaceWithName:@"main"];
  MZFeld *mainFeld = [mainSurface feldWithName:@"main"];
  return [self surface:mainSurface feldWidth:mainFeld];
}

- (CGFloat)windshieldWidth
{
  MZSurface *mainSurface = [_systemModel surfaceWithName:@"main"];
  return [_workspaceGeometry rectForSurface:mainSurface].size.width;
}

- (CGFloat)windshieldHeight
{
  MZSurface *mainSurface = [_systemModel surfaceWithName:@"main"];
  return [_workspaceGeometry rectForSurface:mainSurface].size.height;
}


#pragma mark -
#pragma mark Coordinates transforms


- (CGFloat)surface:(MZSurface *)surface feldHeight:(MZFeld *)feld
{
  MZVect *feldPhysicalSize = feld.physicalSize;
  MZVect *surfacePhysicalSize = surface.boundingFeld.physicalSize;
  CGFloat proportion = feldPhysicalSize.y.doubleValue / surfacePhysicalSize.y.doubleValue;
  CGRect surfaceRect = [_workspaceGeometry rectForSurface:surface];
  return surfaceRect.size.height * proportion;
}


- (CGFloat)surface:(MZSurface *)surface feldWidth:(MZFeld *)feld
{
  MZVect *feldPhysicalSize = feld.physicalSize;
  MZVect *surfacePhysicalSize = surface.boundingFeld.physicalSize;
  CGFloat proportion = feldPhysicalSize.x.doubleValue / surfacePhysicalSize.x.doubleValue;

  CGRect surfaceRect = [_workspaceGeometry rectForSurface:surface];
  return surfaceRect.size.width * proportion;
}

- (CGFloat)surface:(MZSurface *)surface feldXOffset:(MZFeld *)feld
{
  CGFloat surfacePhysicalWidth = surface.boundingFeld.physicalSize.x.doubleValue;
  CGFloat feldPhysicalWidth = feld.physicalSize.x.doubleValue;
  CGFloat boundingFeldOver = [surface.boundingFeld.center doubleVectDot:surface.boundingFeld.over];
  CGFloat feldOver = [feld.center doubleVectDot:feld.over];
  CGFloat dist = fabs((boundingFeldOver - surfacePhysicalWidth / 2.0) - (feldOver - feldPhysicalWidth / 2.0));
  CGRect surfaceRect = [_workspaceGeometry rectForSurface:surface];
  CGFloat pixelsPerMM = surfaceRect.size.width / surfacePhysicalWidth;
  return dist * pixelsPerMM;
}

-(CGFloat) surface:(MZSurface *)surface feldYOffset:(MZFeld *)feld
{ // Vertical coordinates in native are grow bottom to top, iOS is top to bottom.
  CGFloat surfacePhysicalHeight = surface.boundingFeld.physicalSize.y.doubleValue;
  CGFloat feldPhysicalHeight = feld.physicalSize.y.doubleValue;
  CGFloat boundingFeldUp = [surface.boundingFeld.center doubleVectDot:surface.boundingFeld.up];
  CGFloat feldUp = [feld.center doubleVectDot:feld.up];
  CGFloat dist = (boundingFeldUp + surfacePhysicalHeight / 2.0) - (feldUp + feldPhysicalHeight / 2.0);
  CGRect surfaceRect = [_workspaceGeometry rectForSurface:surface];
  CGFloat pixelsPerMM = surfaceRect.size.height / surfacePhysicalHeight;
  return dist * pixelsPerMM;
}


- (CGRect)convertFrameFromRelativeCoordinatesToWindshieldViewCoordinates:(CGRect)frameInRelativeCoordinates inFeld:(MZFeld *)feld inSurface:(MZSurface *)surface
{
  CGRect rect = CGRectMake([self surface:surface feldXOffset:feld] + ((frameInRelativeCoordinates.origin.x - frameInRelativeCoordinates.size.width / 2.0) + 0.5) * [self surface:surface feldWidth:feld],
                           [self surface:surface feldYOffset:feld] + ((1 - (frameInRelativeCoordinates.origin.y + 0.5)) - frameInRelativeCoordinates.size.height / 2.0) * [self surface:surface feldHeight:feld],
                           frameInRelativeCoordinates.size.width * [self surface:surface feldWidth:feld],
                           frameInRelativeCoordinates.size.height * [self surface:surface feldHeight:feld]);

  return rect;
}


- (CGRect)convertFrameFromWindshieldViewCoordinatesToRelativeCoordinates:(CGRect)frameInWindshieldViewCoordinates inFeld:(MZFeld *)feld inSurface:(MZSurface *)surface
{
  return CGRectMake(-0.5 + (frameInWindshieldViewCoordinates.origin.x - [self surface:surface feldXOffset:feld]) / [self surface:surface feldWidth:feld] + frameInWindshieldViewCoordinates.size.width / [self surface:surface feldWidth:feld] / 2.0,
                    0.5 - ((frameInWindshieldViewCoordinates.origin.y - [self surface:surface feldYOffset:feld]) / [self surface:surface feldHeight:feld]) - frameInWindshieldViewCoordinates.size.height / [self surface:surface feldHeight:feld] / 2.0,
                    frameInWindshieldViewCoordinates.size.width / [self surface:surface feldWidth:feld],
                    frameInWindshieldViewCoordinates.size.height / [self surface:surface feldHeight:feld]);
}


- (CGPoint)shiftPointFromOrigin:(CGPoint)pointInOrigin toFeld:(MZFeld *)feld inSurface:(MZSurface *)surface
{
  CGRect surfaceRect = [_workspaceGeometry rectForSurface:surface];
  return CGPointMake(pointInOrigin.x + surfaceRect.origin.x,
                     pointInOrigin.y + surfaceRect.origin.y);
}


- (CGPoint)shiftPointToOrigin:(CGPoint)pointInOrigin fromFeld:(MZFeld *)feld inSurface:(MZSurface *)surface
{
  CGRect surfaceRect = [_workspaceGeometry rectForSurface:surface];
  return CGPointMake(pointInOrigin.x - surfaceRect.origin.x,
                     pointInOrigin.y - surfaceRect.origin.y);
}


- (CGRect)rectFromNativeWindshieldCoordinatesForItem:(MZWindshieldItem *)item
{
  return [self rectFromNativeWindshieldCoordinates:item.frame inSurface:item.surface inFeld:item.feld];
}


- (CGRect)rectFromNativeWindshieldCoordinates:(CGRect)frame inSurface:(MZSurface *)surface inFeld:(MZFeld *)feld
{
  CGRect absoluteRect = [self convertFrameFromRelativeCoordinatesToWindshieldViewCoordinates:frame inFeld:(MZFeld *)feld inSurface:surface];
  absoluteRect.origin = [self shiftPointFromOrigin:absoluteRect.origin toFeld:feld inSurface:surface];
  return absoluteRect;
}


- (CGRect)rectToNativeWindshieldCoordinates:(CGRect)frame inFeld:(MZFeld *)feld
{
  MZSurface *surface = [self.systemModel surfaceWithFeld:feld];
  frame.origin = [self shiftPointToOrigin:frame.origin fromFeld:feld inSurface:surface];
  frame = [self convertFrameFromWindshieldViewCoordinatesToRelativeCoordinates:frame inFeld:feld inSurface:surface];
  return frame;
}


- (MZFeld *)closestFeldAtLocation:(CGPoint)location
{
  return [_workspaceGeometry closestFeldForLocation:location];
}


- (CGPoint)convertPointFromWindshieldToScrollViewCoordinates:(CGPoint)pointInWindshieldCoordinates
{
  return [[self.fullWindshieldDelegate windshieldContainerScrollView] convertPoint:pointInWindshieldCoordinates fromView:self.view];
}

- (CGPoint)shiftPointFromWindshieldViewToShielderContainer:(CGPoint)pointInWindshieldViewCoordinates
{
  MZSurface *mainSurface = [_systemModel surfaceWithName:@"main"];
  CGRect surfaceRect = [_workspaceGeometry rectForSurface:mainSurface];
  return CGPointMake(pointInWindshieldViewCoordinates.x + surfaceRect.origin.x,
                     pointInWindshieldViewCoordinates.y + surfaceRect.origin.y);
}

- (NSUInteger)indexOfFeldAtLocation:(CGPoint)location
{
  MZFeld *feld = [self closestFeldAtLocation:location];
  MZSurface *mainSurface = [_systemModel surfaceWithName:@"main"];
  return [mainSurface.felds indexOfObject:feld];
}

- (BOOL)isLocationInCorkboardSpace:(CGPoint)location
{
  MZSurface *closestSurface = [_workspaceGeometry closestSurfaceForLocation:location];
  return ![_systemModel.surfacesInPrimaryWindshield containsObject:closestSurface];
}


#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
  return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldBeRequiredToFailByGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
  if (otherGestureRecognizer == _panGestureThatShouldBeRequiredToFailByItemPanningGestures)
    return YES;

  return NO;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRequireFailureOfGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
  if ([gestureRecognizer.accessibilityLabel isEqualToString:@"WindshieldItemTapGesture"] &&
      [_tapGesturesThatShouldBeRequiredToFail containsObject:otherGestureRecognizer])
    return YES;

  return NO;
}


#pragma mark - OBDropZone

-(OBDropAction) ovumEntered:(OBOvum*)ovum inView:(UIView*)view atLocation:(CGPoint)location
{
  [self createIndicatorForOvum:ovum atLocation:location];

  return [super ovumEntered:ovum inView:view atLocation:location];
}

- (void)ovumExited:(OBOvum*)ovum inView:(UIView*)view atLocation:(CGPoint)location
{
  if ([self.delegate respondsToSelector:@selector(windshieldViewEndEdgeScrolling)]) {
    [self.delegate windshieldViewEndEdgeScrolling];
  }

  [super ovumExited:ovum inView:view atLocation:location];
}


- (void)ovumDropped:(OBOvum*)ovum inView:(UIView*)view atLocation:(CGPoint)location
{
  [self removeIndicator];

  [super ovumDropped:ovum inView:view atLocation:location];

  if ([self.delegate respondsToSelector:@selector(windshieldViewUpdateSpaceIndexWithLocation:)]) {
    [self.delegate windshieldViewUpdateSpaceIndexWithLocation:location];
  }
}

- (OBDropAction)ovumMoved:(OBOvum*)ovum inView:(UIView*)view atLocation:(CGPoint)location
{

  if ([ovum.dataObject isKindOfClass:[MZAsset class]] ||
      [ovum.dataObject isKindOfClass:[MZLiveStream class]] ||
      [ovum.dataObject isKindOfClass:[MZPortfolioItem class]])
  {
    CGPoint locationInContentScrollView = [self convertPointFromWindshieldToScrollViewCoordinates:location];

    if ([self.delegate respondsToSelector:@selector(windshieldViewEdgeScrollingEventAtLocation:)]) {
      [self.delegate windshieldViewEdgeScrollingEventAtLocation:locationInContentScrollView];
    }

    CGFloat scale = [self scaleOfItemView:draggedView atLocation:location];
    [self updatePlaceholderViewForObject:ovum.dataObject withLocation:location andScale:scale];
  }
  return [super ovumMoved:ovum inView:view atLocation:location];
}


@end
