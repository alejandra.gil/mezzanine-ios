//
//  PresentationSliderViewController.swift
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 30/03/2017.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import UIKit

enum SliderStyle {
  case portrait
  case landscape
}

struct PresentationConstants {
  var height: CGFloat
  var minimumTapArea: CGFloat
  var minimumIndicatorWidth: CGFloat
  var animationDuration: Double
}

@objc public protocol PresentationSliderViewControllerDelegate {
  
  @objc func presentationSliderMovedToIndex(_ index: Int)
}


@objc class PresentationSliderViewController: UIViewController {
  
  @IBOutlet var portraitSlider: Slider!
  @IBOutlet var landscapeSlider: Slider!
  fileprivate var style: SliderStyle!
  var slider: Slider!
  
  weak var delegate: PresentationSliderViewControllerDelegate?
  var interactor: PresentationSliderInteractor!
  var constants: PresentationConstants!
  
  convenience init(sliderStyle: SliderStyle) {
    self.init(nibName: "PresentationSliderViewController", bundle: nil)
    style = sliderStyle
  }
  
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.view.backgroundColor = UIColor.clear
    addSliderToView()
  }
  
  func addSliderToView() {
    
    constants = PresentationConstants(height: 50.0,
                                      minimumTapArea: 80.0,
                                      minimumIndicatorWidth: 20.0,
                                      animationDuration: 0.2)
    
    if style == .portrait {
      slider = portraitSlider
    } else {
      slider = landscapeSlider
    }
    
    slider.delegate = self
    slider.constants = constants
    updateSliderVisibility(interactor.isPresentationActive(), animated: false)
    view.addSubview(slider)
  }
  
  // MARK: Layout
  
  override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
    super.viewWillTransition(to: size, with: coordinator)
    
    slider.isLandscape = size.width > size.height ? true : false
  }
  
  override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
    super.traitCollectionDidChange(previousTraitCollection)
    
    guard let view = transitionCoordinator?.containerView ?? self.view?.window else { return }
    slider.isLandscape = view.frame.width > view.frame.height ? true : false
  }
  
  // MARK: Slider updates
  
  override func viewDidLayoutSubviews() {
    updateLayout(animate: false)
  }
  
  func updateLayout(animate animated: Bool) {
    updateSliderIndicator()
  }
  
  func updateSliderIndicator() {
    slider.frame.size.width = self.view.frame.width
    slider.numberOfSlides = interactor.numberOfSlides()
    updateSliderIndicatorLabel()
    updateSliderIndicatorWidth()
    updateSliderIndicatorPosition(animated: false)
  }
  
  func updateSliderIndicatorLabel() {
    if slider.isDragging == false {
      slider.updateStringAtIndex(interactor.currentSlideIndex() + 1)
    }
  }
  
  func updateSliderIndicatorWidth() {
    slider.updateIndicatorWidth()
  }
  
  func updateSliderIndicatorPosition(animated: Bool) {
    if slider.isDragging == false {
      slider.updateIndicatorPosition(index: interactor!.currentSlideIndex() + 1, animated: animated)
    }
  }
  
  func updateSliderCurrentIndicatorPosition(animated: Bool) {
    slider.updateCurrentIndicatorPosition(index: interactor.currentSlideIndex() + 1, animated: animated)
  }

  func updateSliderVisibility(_ visible: Bool, animated: Bool) {
    slider.updateVisibility(visible, animated: animated)
  }
}

extension PresentationSliderViewController: SliderDelegate {
  func sliderDraggingToIndex(_ index: Int) {
    delegate?.presentationSliderMovedToIndex(index)
  }
  
  func sliderMovedToIndex(_ index: Int) {
    interactor.requestPresentationScrollRequest(index)
  }
}


// MARK: Slider

protocol SliderDelegate: NSObjectProtocol {
  func sliderDraggingToIndex(_ index: Int)
  func sliderMovedToIndex(_ index: Int)
}

class Slider: UIView {
  
  weak var delegate: SliderDelegate?
  fileprivate var isLandscape: Bool = false
  
  @IBOutlet var trackView: UIView!
  @IBOutlet var indicatorContainerView: UIView!
  @IBOutlet var currentIndicatorView: UIView!
  @IBOutlet var indicatorTrackView: UIView!
  @IBOutlet var panGRView: UIView!
  @IBOutlet var minimumIndicatorView: UIView!
  @IBOutlet var slidesLabel: UILabel!
  
  @IBOutlet var indicatorWidthConstraint: NSLayoutConstraint!
  @IBOutlet var currentIndicatorWidthConstraint: NSLayoutConstraint!
  @IBOutlet var panGRWidthConstraint: NSLayoutConstraint!
  @IBOutlet var leftMarginConstraint: NSLayoutConstraint!
  @IBOutlet var rightMarginConstraint: NSLayoutConstraint!
  
  
  var slidesString: String! = "" {
    didSet {
      // Apply kern to space the letters
      let attributes: NSDictionary = [
        NSAttributedStringKey.kern:CGFloat(1.0)
      ]
      
      let attrText = NSAttributedString(string: slidesString,
                                        attributes:attributes as? [NSAttributedStringKey : AnyObject])
      
      slidesLabel.attributedText = attrText
    }
  }
  
  var numberOfSlides: Int! = 0
  var currentDraggedIndex: Int! = -1
  var isDragging: Bool! = false
  var constants: PresentationConstants!
  
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
    trackView.backgroundColor = MezzanineStyleSheet.shared().grey70Color
    indicatorTrackView.backgroundColor = MezzanineStyleSheet.shared().horizonBlueColorActive
    minimumIndicatorView.backgroundColor = MezzanineStyleSheet.shared().horizonBlueColorActive

    currentIndicatorView.backgroundColor = MezzanineStyleSheet.shared().grey130Color
    
    slidesLabel.font = UIFont.dinMedium(ofSize: 12.0)
    
    let panGR = UIPanGestureRecognizer(target: self, action: #selector(self.sliderPanGesture(_:)))
    panGRView.addGestureRecognizer(panGR)
  }
  
  @IBAction func sliderPanGesture(_ recognizer: UIPanGestureRecognizer) {
    let translation = recognizer.translation(in: self)
    
    switch recognizer.state {
    case .changed:
      isDragging = true
      
      var centerOffsetX: CGFloat = leftMarginConstraint.constant
      if (indicatorContainerView.frame.minX + translation.x) < leftMarginConstraint.constant {
        centerOffsetX = indicatorContainerView.frame.width / 2 + leftMarginConstraint.constant
      } else if (indicatorContainerView.frame.maxX + translation.x) > trackView.frame.maxX {
        centerOffsetX = trackView.frame.maxX - (indicatorContainerView.frame.width / 2)
      } else {
        centerOffsetX = indicatorContainerView.center.x + translation.x
      }
      
      indicatorContainerView.center = CGPoint(x: centerOffsetX, y: indicatorContainerView.center.y)
      minimumIndicatorView.frame.origin.x = self.indicatorContainerView.frame.minX - (offsetForMinimumWidth() * CGFloat(self.getCurrentIndex()))
      panGRView.center = CGPoint(x: centerOffsetX, y: panGRView.center.y)
      
      if !self.isLandscape {
        slidesLabel.center.x = self.minimumIndicatorView.isHidden ? indicatorContainerView.center.x : self.minimumIndicatorView.center.x
      }
      
      recognizer.setTranslation(CGPoint(x: 0,y: 0), in: self)
      
      updateString()
      
      let indexToScroll = getCurrentIndex() - 1
      if currentDraggedIndex != indexToScroll {
        currentDraggedIndex = indexToScroll
        delegate?.sliderDraggingToIndex(indexToScroll)
      }
      
      break
      
    case .ended:
      updateString()
      delegate?.sliderMovedToIndex(getCurrentIndex() - 1)
      snapIndicatorToClosestPosition()
      
      isDragging = false
      
      // Analytics
      let analyticsManager = MezzanineAnalyticsManager.sharedInstance
      analyticsManager.tagEvent(analyticsManager.scrollPresentationEvent, attributes: [analyticsManager.sourceKey : analyticsManager.sliderAttribute])
      
      break
      
    default:
      break
    }
  }

  // MARK: Index updates
  
  func updateString() {
    updateStringAtIndex(getCurrentIndex())
  }
  
  fileprivate func updateStringAtIndex(_ currentIndex: Int) {
    slidesString = String(currentIndex) + "/" + String(numberOfSlides)
  }
  
  fileprivate func getCurrentIndex() -> Int {
    let individualWidth = trackView.frame.width / CGFloat(numberOfSlides)
    let index = ceil((indicatorContainerView.center.x - leftMarginConstraint.constant) / individualWidth)
    
    return index.isNaN ? 0 : Int(index)
  }
  
  // MARK: UI updates
  
  fileprivate func snapIndicatorToClosestPosition() {
    let individualWidth = trackView.frame.width / CGFloat(numberOfSlides)
    let newXCenter = ((CGFloat(getCurrentIndex() - 1) * individualWidth) + indicatorContainerView.frame.width / 2) + leftMarginConstraint.constant
    
    UIView.animate(withDuration: constants.animationDuration, animations: {
      self.indicatorContainerView.center.x = newXCenter
      self.minimumIndicatorView.frame.origin.x = self.indicatorContainerView.frame.minX - (self.offsetForMinimumWidth() * CGFloat(self.getCurrentIndex()))
      self.panGRView.center.x = newXCenter
      
      if !self.isLandscape {
        self.slidesLabel.center.x = self.minimumIndicatorView.isHidden ? newXCenter : self.minimumIndicatorView.center.x
      }
    }) 
  }
  
  func updateIndicatorWidth() {
    if numberOfSlides > 0 {
      let indicatorWidth: CGFloat = trackView.frame.width / CGFloat(numberOfSlides)
      minimumIndicatorView.isHidden = indicatorWidth > constants.minimumIndicatorWidth ? true : false
      
      indicatorWidthConstraint.constant = indicatorWidth
      currentIndicatorWidthConstraint.constant = max(indicatorWidth, constants.minimumIndicatorWidth)
      panGRWidthConstraint.constant = max(indicatorWidth, constants.minimumTapArea)
      layoutIfNeeded()
    }
  }
  
  func updateIndicatorPosition(index: Int, animated: Bool) {
    updateIndicatorWidth()
    let newXCenter = (CGFloat(index) * indicatorWidthConstraint.constant - indicatorContainerView.frame.width / 2) + leftMarginConstraint.constant
    
    let animationBlock = {
      self.indicatorContainerView.center.x = newXCenter
      self.minimumIndicatorView.frame.origin.x = self.indicatorContainerView.frame.minX - (self.offsetForMinimumWidth() * CGFloat(self.getCurrentIndex()))
      self.currentIndicatorView.frame.origin.x = self.minimumIndicatorView.isHidden ? self.indicatorContainerView.frame.minX : self.indicatorContainerView.frame.minX - (self.offsetForMinimumWidth() * CGFloat(self.getCurrentIndex()))
      self.panGRView.center.x = newXCenter
      
      if !self.isLandscape {
        self.slidesLabel.center.x = self.minimumIndicatorView.isHidden ? newXCenter : self.minimumIndicatorView.center.x
      }
    }
    
    if animated {
      UIView.animate(withDuration: constants.animationDuration, animations: {
        animationBlock()
      })
    } else {
      animationBlock()
    }
  }
  
  func updateCurrentIndicatorPosition(index: Int, animated: Bool) {
    let animationBlock = {
      if self.minimumIndicatorView.isHidden {
        self.currentIndicatorView.center.x = (CGFloat(index) * self.indicatorWidthConstraint.constant - self.indicatorContainerView.frame.width / 2) + self.leftMarginConstraint.constant
      } else {
        self.currentIndicatorView.frame.origin.x  = (CGFloat(index) * (self.indicatorWidthConstraint.constant - self.offsetForMinimumWidth()) - self.indicatorWidthConstraint.constant) + self.leftMarginConstraint.constant
      }
    }
    
    if animated {
      UIView.animate(withDuration: constants.animationDuration, animations: {
        animationBlock()
      })
    } else {
      animationBlock()
    }
  }
  
  
  func updateVisibility(_ visible: Bool, animated: Bool) {
    let animationBlock = {
      self.alpha = visible == true ? 1.0 : 0.0
    }
    
    if animated {
      UIView.animate(withDuration: constants.animationDuration, animations: {
        animationBlock()
      })
    } else {
      animationBlock()
    }
  }
  
  func offsetForMinimumWidth() -> CGFloat {
    return (minimumIndicatorView.frame.width - indicatorContainerView.frame.width) / max(CGFloat(numberOfSlides), 1)
  }
}
