//
//  WorkspaceViewController.swift
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 01/07/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

import UIKit

@objc enum WorkspaceViewState: NSInteger {

  case preview = 0
  case full = 1
}

// MARK: - WorkspaceViewControllerDelegate
@objc protocol WorkspaceViewControllerDelegate {
  // Actions
  func workspaceViewControllerDidStopPresentation(_ workspaceVC: WorkspaceViewController)
  func workspaceViewControllerDidToggleZoom(_ workspaceVC: WorkspaceViewController, mode: Bool, animated: Bool)

  // OBDragZone
  func workspaceViewControllerDidDraggingWindshieldItem(_ workspaceVC: WorkspaceViewController)
  func workspaceViewControllerDidStopDraggingWindshieldItem(_ workspaceVC: WorkspaceViewController)
}


// MARK: -
// MARK: - WorkspaceViewController

class WorkspaceViewController: UIViewController {
  
  @objc weak var delegate:WorkspaceViewControllerDelegate?
  
  // Analytics
  fileprivate var analyticsManager = MezzanineAnalyticsManager.sharedInstance
  
  // Gesture recognizers
  fileprivate var presentationPanGesture: OBDirectionalPanGestureRecognizer!
  fileprivate var threeFingerPanGesture: UIPanGestureRecognizer!
  
  // misc values
  fileprivate var currentFeldIndex: Int = 0
  fileprivate var zoomModeEnabled: Bool = false
  fileprivate var firstLoad: Bool = false
  fileprivate var tmpNewSlide: Int!
  fileprivate var currentDragDropRecognizer: UIGestureRecognizer!

  // Edge scrolling
  fileprivate var edgeScrollingSpeed: Float!
  fileprivate var edgeScrollingTimer: Timer!
  
  // Layout
  fileprivate var nextTraitCollection: UITraitCollection!
  
  // IBOutlet
  @IBOutlet var extendedWindshieldContainerView: UIView!
  @IBOutlet var zoomToggleButton: UIButton!
  @IBOutlet var presentationModeToolbar: UIView!
  @IBOutlet var presentationStopButton: UIButton!
  @IBOutlet var feldSwitcherView: FeldSwitcherView!
  @IBOutlet var feldSwitcherViewWidthContraint: NSLayoutConstraint!
  @IBOutlet var extendedWindshieldToggleView: CorkboardToggleView!
  @IBOutlet var extendedWindshieldToggleViewWidthContraint: NSLayoutConstraint!
  @IBOutlet var extendedWindshieldTitleLabel: UILabel!
  @IBOutlet var extendedWindshieldPanelDropZoneView: UIView!
  @IBOutlet var extendedWindshieldBlurredView: UIView!
  @IBOutlet var workspaceOptionsButton: UIButton!

  //TO DO some childViewControllers needs access to this for now...
  @IBOutlet var deckSliderView: DeckSliderView!
  @IBOutlet var contentScrollView: WorkspaceContentScrollView!
  @IBOutlet var workspaceContentView: UIView!
  @objc var welcomeHintView: WelcomeHintView!
  @objc var presentationViewController: PresentationViewController!
  @objc var windshieldViewController: PrimaryWindshieldViewController!
  @objc var currentPopover: FPPopoverController?

  // Controllers
  @objc var extendedWindshieldViewController: ExtendedWindshieldViewController!
  
  // Observers
  fileprivate var systemModelObserverArray: Array <String> = Array <String> ()
  fileprivate var workspaceObserverArray: Array <String> = Array <String> ()

  //  State
  @objc var state: WorkspaceViewState = .full {

    didSet {

      if self.state == .preview {

        workspaceOptionsButton.alpha = 0.0
        extendedWindshieldToggleView.alpha = 0.0
        zoomToggleButton.alpha = 0.0
        extendedWindshieldTitleLabel.alpha = 0.0
        showCorkboardPanelBlurredView(false)
        extendedWindshieldViewController.hideExtendedWindshield()
        feldSwitcherView.alpha = 0.0
        presentationViewController.view.alpha = 0.0
        updateDeckSliderAndStopButtonView()
      }
      else if self.state == .full {

        workspaceOptionsButton.alpha = 1.0
        extendedWindshieldToggleView.alpha = 1.0
        zoomToggleButton.alpha = 1.0
        feldSwitcherView.alpha = self.isZoomedIn() ? 1.0 : 0.0
        presentationViewController.view.alpha = 1.0
        updateDeckSliderAndStopButtonView()
      }
    }
  }

  @objc var communicator: MZCommunicator!
  
  @objc var workspace: MZWorkspace? {
    willSet(newWorkspace) {
      
      if self.workspace != newWorkspace {
        endObservingWorkspace()
      }
    }
    
    didSet {
      presentationViewController.workspace = workspace
      windshieldViewController.workspace = workspace
      windshieldViewController.windshield = workspace != nil ? workspace!.windshield : nil
      extendedWindshieldViewController.workspace = workspace
      extendedWindshieldViewController.windshield = workspace != nil ? workspace!.windshield : nil
      
      if workspace != nil {
        workspaceObserverArray.removeAll()
        beginObservingWorkspace()
      }
    }
  }
  
  func beginObservingWorkspace() {
    workspaceObserverArray.append(workspace!.addObserver(forKeyPath: "presentation.active", options: [.new, .old], on: OperationQueue.main) { ( obj, change: [AnyHashable: Any]!) in
      self.updatePresentationModeAnimated(true)
      self.updateWelcomeHint()
      }
    )
    
    workspaceObserverArray.append(workspace!.addObserver(forKeyPath: "windshield.items", options: [.new, .old], on: OperationQueue.main) { ( obj, change: [AnyHashable: Any]!) in
      self.updateWelcomeHint()
      }
    )
    
    updatePresentationModeAnimated(false)
    updateDeckSlider(deckSliderView)
    updateWelcomeHint()
  }
  
  func endObservingWorkspace() {
    for token in workspaceObserverArray {
      workspace!.removeObserver(withBlockToken: token)
    }
  }
  
  
  @objc var systemModel: MZSystemModel! {
    willSet(newSystemModel) {
      if self.systemModel != newSystemModel {
        endObservingSystemModel()
      }
    }
    
    didSet {
      windshieldViewController.systemModel = systemModel
      extendedWindshieldViewController.systemModel = systemModel
      presentationViewController.systemModel = systemModel
      extendedWindshieldToggleView.systemModel = systemModel
      feldSwitcherView.systemModel = systemModel
      
      if systemModel != nil {
        systemModelObserverArray.removeAll()
        beginObservingSystemModel()
      }
    }
  }
  
  func beginObservingSystemModel() {
    // felds
    systemModelObserverArray.append(systemModel.addObserver(forKeyPath: "felds", options: [.new, .old], on: OperationQueue.main) { ( obj, change: [AnyHashable: Any]!) in
    
      // If we don't have felds, we are on the way to disconnect. See bug 17488
      if change["new"] is NSNull {
        self.endObservingSystemModel()
        return
      }
      
      self.presentationViewController.refreshFeldBoundViews()
      self.feldSwitcherViewWidthContraint.constant = self.feldSwitcherView.calculateSizeForView().width
      self.setZoomMode(false, index: self.systemModel.indexOfCenterFeld(), animated: false)
      self.contentScrollView.updateContentViewLayout(false)
      self.updateWelcomeHint()
      }
    )
    
    // currentWorkspace
    systemModelObserverArray.append(systemModel.addObserver(forKeyPath: "currentWorkspace", options:[.new, .old], on: OperationQueue.main) { ( obj, change: [AnyHashable: Any]!) in
      self.updateWelcomeHint()
      }
    )
    
    // infopresence.status
    systemModelObserverArray.append(systemModel.addObserver(forKeyPath: "infopresence.status", options:[.new, .old], on: OperationQueue.main) { ( obj, change: [AnyHashable: Any]!) in
      // Update only there's a change
      if (change[NSKeyValueChangeKey.oldKey] as! NSNumber) != (change[NSKeyValueChangeKey.newKey] as! NSNumber) {
        self.presentationViewController.updateFeldBoundViews()
        self.updateWelcomeHint()
      }
      }
    )
    
    // extended windshield
    systemModelObserverArray.append(systemModel.addObserver(forKeyPath: "surfaces", options:[.new, .old], on: OperationQueue.main) { ( obj, change: [AnyHashable: Any]!) in
      self.updateCorkboardToggleState()
      }
    )
  }
  
  func endObservingSystemModel() {
    for token in systemModelObserverArray {
      systemModel.removeObserver(withBlockToken: token)
    }
  }
  
  
  // MARK: - Init
  @objc convenience init(systemModel: MZSystemModel, communicator: MZCommunicator, delegate: WorkspaceViewControllerDelegate) {
    self.init(nibName: "WorkspaceViewController", bundle: nil)
    
    self.systemModel = systemModel
    
    /* The willSet and didSet observers of superclass properties are called when a property is set in a subclass initializer, after the superclass initializer has been called. They are not called while a class is setting its own properties, before the superclass initializer has been called.*/
    beginObservingSystemModel()
    
    self.communicator = communicator
    self.delegate = delegate
    
  }
  
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  
  // MARK: - View life cycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.view.clipsToBounds = true
    firstLoad = true
    
    initializePresentation()
    initializeWindshield()
    initializeContentScrollView()
    initializeExtendedWindshield()
    initializeGRs()
    setupChildViewControllers()
    
    setupOutlets()
    defaultContentScrollViewZoomScaleConfiguration()
    
    initAccessibility()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    registerNotifications()
    
    if firstLoad == false {
      contentScrollView.updateContentViewLayout(false)
    }
    
    updateAdaptativeLayout(traitCollection)
    
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    
    updateWelcomeHint()

    extendedWindshieldViewController.perform(#selector(extendedWindshieldViewController.refreshLayout))

    OperationQueue.main.addOperation { 
      self.firstLoad = false
      self.welcomeHintView.alreadyLoaded = true
    }
  }

  override func viewWillDisappear(_ animated: Bool) {
    if currentPopover != nil {
      if currentPopover!.isPopoverVisible {
        currentPopover!.dismissPopover(animated: true)
      }
    }
  }

  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    unregisterNotifications()
    
  }
  
  
  // MARK: Initialization
  fileprivate func initializePresentation() {
    presentationViewController = PresentationViewController(nibName: nil, bundle: nil)
    presentationViewController.delegate = self
    presentationViewController.communicator = communicator ?? nil
    presentationViewController.systemModel = systemModel ?? nil
    presentationViewController.workspaceViewController = self
  }
  
  fileprivate func initializeWindshield() {
    windshieldViewController = PrimaryWindshieldViewController(nibName: nil, bundle: nil)
    windshieldViewController.delegate = self
    windshieldViewController.workspaceViewController = self
    windshieldViewController.systemModel = systemModel ?? nil
    windshieldViewController!.windshield = workspace != nil ? workspace!.windshield : nil
    windshieldViewController!.workspace = workspace ?? nil
  }
  
  fileprivate func initializeGRs() {
    presentationPanGesture = OBDirectionalPanGestureRecognizer(target: presentationViewController, action: #selector(presentationViewController.handleDeckPanGesture(_:)))
    presentationPanGesture.delegate = self
    presentationPanGesture.direction = OBDirectionalPanHorizontal
    presentationPanGesture.angleThreshold = 45.0
    presentationPanGesture.minimumNumberOfTouches = 1
    presentationPanGesture.maximumNumberOfTouches = 1
    workspaceContentView.addGestureRecognizer(presentationPanGesture)
    
    // Gesture recognizers
    threeFingerPanGesture = UIPanGestureRecognizer(target: self, action: #selector(self.handleThreeFingerDetection(_:)))
    threeFingerPanGesture.delegate = self
    threeFingerPanGesture.minimumNumberOfTouches = 3
    threeFingerPanGesture.maximumNumberOfTouches = 3
    contentScrollView.addGestureRecognizer(threeFingerPanGesture)
  }
  
  fileprivate func initializeContentScrollView() {
    // Configure content scroll view and add workspaceContentView
    contentScrollView.scrollsToTop = false
    contentScrollView.minimumZoomScale = 1.0/CGFloat(systemModel.felds.count)
    contentScrollView.maximumZoomScale = 1.0
    
    contentScrollView.delegate = self;
    contentScrollView.dropZoneHandler = self
    // `pinchGestureRecognizer` will return nil when zooming is disabled. 
    // Means minimumZoomScale and maximumZoomScale are the same.
    contentScrollView.pinchGestureRecognizer?.isEnabled = true
    contentScrollView.panGestureRecognizer.minimumNumberOfTouches = 3
    contentScrollView.panGestureRecognizer.maximumNumberOfTouches = 3
    contentScrollView.backgroundColor = MezzanineStyleSheet.shared().grey20Color
    contentScrollView.systemModel = systemModel
    contentScrollView.workspaceViewController = self
    contentScrollView.addSubview(workspaceContentView)
  }
  
  fileprivate func initializeExtendedWindshield() {
    extendedWindshieldViewController = ExtendedWindshieldViewController(nibName: nil, bundle: nil)
    extendedWindshieldViewController!.workspaceViewController = self
    extendedWindshieldViewController!.delegate = self
    extendedWindshieldViewController!.systemModel = systemModel ?? nil
    extendedWindshieldViewController!.workspace = workspace ?? nil
    extendedWindshieldViewController!.windshield = workspace != nil ? workspace!.windshield : nil
  }
  
  fileprivate func setupChildViewControllers() {
    let presentationView = presentationViewController.view
    presentationView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    presentationView?.frame = workspaceContentView.bounds
    workspaceContentView.addSubview(presentationViewController.view)
    self.addChildViewController(presentationViewController)
    presentationViewController.didMove(toParentViewController: self)
    
    let windshieldView = windshieldViewController.view
    windshieldView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    windshieldView?.frame = workspaceContentView.bounds
    workspaceContentView.insertSubview(windshieldViewController.view, aboveSubview: presentationViewController.view)
    self.addChildViewController(windshieldViewController)
    windshieldViewController.didMove(toParentViewController: self)
    
    let extendedWindshieldView = extendedWindshieldViewController.view
    extendedWindshieldView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    extendedWindshieldView?.frame = extendedWindshieldContainerView.bounds
    extendedWindshieldContainerView.addSubview(extendedWindshieldViewController.view)
    self.addChildViewController(extendedWindshieldViewController)
    extendedWindshieldViewController.didMove(toParentViewController: self)
  }
  
  fileprivate func setupOutlets() {
    let styleSheet = MezzanineStyleSheet.shared()
    // Presentation toolbar
    let stopButtonImage = UIImage(named: "mobile-stop-icon")!.withRenderingMode(.alwaysTemplate)
    presentationStopButton.setImage(stopButtonImage, for: UIControlState())
    presentationStopButton.imageView!.tintColor = styleSheet?.mediumFillColor
    presentationStopButton.titleLabel!.font = styleSheet?.buttonFont
    
    // DeckSliderView
    deckSliderView.delegate = self;
    
    // Extended windshield Label
    styleSheet?.stylizeLabel(extendedWindshieldTitleLabel, withPanelTitle: NSLocalizedString("Corkboard Space Title", comment: ""))
    extendedWindshieldTitleLabel.alpha = 0.0;
    
    // Feld Switcher Control
    feldSwitcherView!.systemModel = systemModel
    feldSwitcherView!.delegate = self
    feldSwitcherView!.alpha = 0.0
    feldSwitcherView?.selectedFeldIndex = currentFeldIndex
    
    // Welcome Hint
    welcomeHintView = WelcomeHintView()
    workspaceContentView.insertSubview(welcomeHintView, belowSubview: windshieldViewController.view)
    
    // Extended windshield Toggle Control
    extendedWindshieldToggleView.systemModel = systemModel
    extendedWindshieldToggleView.delegate = self
    extendedWindshieldPanelDropZoneView.dropZoneHandler = self
    updateCorkboardToggleState()
  }
  
  fileprivate func defaultContentScrollViewZoomScaleConfiguration() {
    // Feld management
    currentFeldIndex = systemModel.indexOfCenterFeld() // Default to viewing center feld
    
    var zoomScale = (!zoomModeEnabled) ? contentScrollView.minimumZoomScale : contentScrollView.maximumZoomScale
    
    // Single Feld is always zoomed in
    if (systemModel.felds.count == 1) {
      zoomScale = contentScrollView.maximumZoomScale
    }
    
    contentScrollView.setZoomScale(zoomScale, animated: false)
    
    // Start out with felds zoomed out
    setZoomMode(false, animated: false)
  }
  
  fileprivate func registerNotifications() {
    // Notifications
    NotificationCenter.default.addObserver(self, selector: #selector(self.dragDropManagerWillBeginDrag(_:)), name: NSNotification.Name(rawValue: OBDragDropManagerWillBeginDragNotification), object: nil)
    
    NotificationCenter.default.addObserver(self, selector: #selector(self.dragDropManagerDidEndDrag(_:)), name: NSNotification.Name(rawValue: OBDragDropManagerDidEndDragNotification), object: nil)
  }
  
  fileprivate func unregisterNotifications() {
    // Notifications
    NotificationCenter.default.addObserver(self, selector: #selector(self.dragDropManagerWillBeginDrag(_:)), name: NSNotification.Name(rawValue: OBDragDropManagerWillBeginDragNotification), object: nil)
    
    NotificationCenter.default.addObserver(self, selector: #selector(self.dragDropManagerDidEndDrag(_:)), name: NSNotification.Name(rawValue: OBDragDropManagerDidEndDragNotification), object: nil)
  }
  
  
  //MARK: Layout updates
  @objc func updateWelcomeHint() {
    var alpha: Double
    if presentationViewController.presentation == nil {
      alpha = 0.0
    } else {
      alpha = self.presentationViewController.presentation.active == true || self.windshieldViewController.windshield.items.count > 0 ? 0.0 : 1.0
    }
    
    UIView.animate(withDuration: 0.3, animations: {
      self.welcomeHintView.alpha = CGFloat(alpha)
      let scale = self.contentScrollView.zoomScale * UIScreen.main.scale
      self.welcomeHintView.titleLabel.layer.contentsScale = scale
      self.welcomeHintView.titleLabel.layer.setNeedsDisplay()
    }) 
    
    welcomeHintView.zoomScale = contentScrollView.zoomScale
  }
  
  fileprivate func updateCorkboardLabel() {
    
    let visible: CGFloat = !extendedWindshieldToggleView.selected || !(systemModel.sideWallScreenCount() > 0) ? 0.0 : 1.0
    UIView.animate(withDuration: 0.33, animations: {
      self.extendedWindshieldTitleLabel.alpha = visible
    }) 
  }
  
  fileprivate func updateCorkboardToggleState() {
    extendedWindshieldToggleViewWidthContraint.constant = extendedWindshieldToggleView.calculateSizeForView().width
    extendedWindshieldPanelDropZoneView.isHidden = systemModel.sideWallScreenCount() == 0
    
    // If the corkboard/extended windshield panel is visible but they are gone, hide it.
    if (!extendedWindshieldContainerView.isHidden && systemModel.sideWallScreenCount() == 0) {
      didTapOnCorkboardToggle()
    }
  }
  
  fileprivate func updatePresentationModeAnimated(_ animated: Bool) {

    let animations = {
      self.updateDeckSliderAndStopButtonView()
      self.contentScrollView.updateContentViewLayout(false)
    }
    
    if (animated) {
      UIView.animate(withDuration: 0.2, animations: {
        animations()
      })
    }
    else {
      animations()
    }
  }

  override func viewDidLayoutSubviews() {
    // TODO: Review it to remove this layout refresh
    contentScrollView.updateContentViewLayout(false)
    windshieldViewController.perform(#selector(windshieldViewController.refreshLayout))
    extendedWindshieldViewController.perform(#selector(extendedWindshieldViewController.refreshLayout))
  }

  fileprivate func updateDeckSliderAndStopButtonView() {

    self.presentationStopButton.alpha = workspace!.presentation.active && state == .full ? 1.0 : 0.0
    self.layoutDeckSlider(self.traitCollection)
  }

  override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
    return traitCollection.isiPad() ? .all : .allButUpsideDown
  }
  
  override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
    super.willTransition(to: newCollection, with: coordinator)
    
    nextTraitCollection = newCollection
    
    coordinator.animate(alongsideTransition: { UIViewControllerTransitionCoordinatorContext in
      self.updateAdaptativeLayout(self.nextTraitCollection)
      }, completion: nil)
  }
  
  override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
    super.viewWillTransition(to: size, with: coordinator)
    
    if nextTraitCollection != nil {
      return
    }
    
    coordinator.animate(alongsideTransition: { UIViewControllerTransitionCoordinatorContext in
      self.updateAdaptativeLayout(self.traitCollection)
      }, completion: nil)
  }
  
  override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
    super.traitCollectionDidChange(previousTraitCollection)
    nextTraitCollection = nil
  }
  
  fileprivate func updateAdaptativeLayout(_ traiCollection: UITraitCollection) {
    
    if traitCollection.isCompactHeight() {
      zoomToggleButton.isHidden = true
    } else {
      zoomToggleButton.isHidden = false
    }
    
    layoutDeckSlider(traiCollection)
    contentScrollView.updateContentViewLayout(false)
    extendedWindshieldViewController.perform(#selector(extendedWindshieldViewController.refreshLayout))

    let newZoomMode = traitCollection.isCompactHeight() ? false : zoomModeEnabled
    setZoomMode(newZoomMode, animated: false)
    
    let feldFrame = frameForFeldAtIndex(currentFeldIndex)
    contentScrollView.setContentOffset(feldFrame.origin, animated: false)
    windshieldViewController.perform(#selector(windshieldViewController.refreshLayout))

    if currentPopover != nil {
      if currentPopover!.isPopoverVisible {
        currentPopover!.dismissPopover(animated: true)
      }
    }
  }


  // MARK: Felds and Layout
  // Frame for use within windshieldCotainerWiew, where each feld is essentially a
  // page within the UIScrollView
  fileprivate func frameForFeldAtIndex(_ index: Int) -> CGRect {
    return frameForFeldAtIndex(index, pageSize: contentScrollView.frame.size)
  }
  
  fileprivate func frameForFeldAtIndex(_ index: Int, pageSize: CGSize) -> CGRect {
    var frame: CGRect = CGRect.zero
    
    frame.origin.x = horizontalContentOffsetForFeldIndex(index)
    frame.origin.y = 0.0 // _contentScrollView's y is sometimes non-zero, but within the windshieldCotainerWiew all felds should be pegged to the top edge
    frame.size = pageSize
    
    return frame;
  }
  
  fileprivate func horizontalContentOffsetForFeldIndex(_ feldIndex: Int) -> CGFloat {
    if (!zoomModeEnabled) {
      // Triptych / Uniptych Page
      return 0
    }
    
    let w = presentationViewController.getFeldSize().width
    if (contentScrollView.frame.width > w) {
      // If tryptich pages aren't equally sized...
      return w * CGFloat(feldIndex)
    } else {
      // All Triptych / Uniptych pages are sized equally
      return contentScrollView.frame.size.width * CGFloat(feldIndex)
    }
  }
  
  @objc func feldIndexAtLocation(_ location: CGPoint) -> Int {
    let feldWidth = presentationViewController.getFeldSize().width
    let xLocationIn_contentScrollView = location.x / contentScrollView.zoomScale
    let triptychHorizontalMargin = presentationViewController.view.frame.origin.x
    let triptychWidth = presentationViewController.view.frame.size.width
    
    var index = 0;
    if (xLocationIn_contentScrollView < presentationViewController.view.frame.origin.x) {
      // Should return below first feld, so let's return the closest (first feld)
      index = 0;
    } else if (xLocationIn_contentScrollView < triptychHorizontalMargin + triptychWidth) {
      // Returns the index of a feld in the triptych / uniptych
      index = Int((xLocationIn_contentScrollView - triptychHorizontalMargin) / feldWidth);
    } else if (xLocationIn_contentScrollView < 2.0 * triptychHorizontalMargin + triptychWidth) {
      // Returns the rightest feld of the triptych one.
      index = systemModel.felds.count - 1;
    }
    
    return index;
  }
  
  fileprivate func showFeldAtIndex(_ index: Int, animated: Bool) {
    currentFeldIndex = index
    let feldFrame = frameForFeldAtIndex(currentFeldIndex)
    var zoomScale = (!zoomModeEnabled) ? contentScrollView.minimumZoomScale : contentScrollView.maximumZoomScale
    
    // Single Feld is always zoomed in
    if systemModel.felds.count == 1 {
      zoomScale = contentScrollView.maximumZoomScale
    }
    
    contentScrollView.setZoomScale(zoomScale, animated: animated)
    contentScrollView.setContentOffset(feldFrame.origin, animated: animated)
  }
  
  
  // MARK: Zoom
  @objc func isZoomedIn() -> Bool {
    return zoomModeEnabled
  }

  fileprivate func setZoomMode(_ mode: Bool, animated: Bool) {
    setZoomMode(mode, index: currentFeldIndex, animated: animated)
  }
  
  fileprivate func setZoomMode(_ mode: Bool, index: Int, animated: Bool) {
    zoomModeEnabled = mode
    
    delegate?.workspaceViewControllerDidToggleZoom(self, mode: zoomModeEnabled, animated: firstLoad ? false : animated)
    
    let animations = {
      self.showFeldAtIndex(index, animated: false)
      self.contentScrollView.updateContentViewLayout(false)

      self.feldSwitcherView.alpha = (self.extendedWindshieldContainerView.isHidden && self.zoomModeEnabled && self.systemModel.felds.count > 1 && self.state == .full) ? 1.0 : 0.0
      self.windshieldViewController.perform(#selector(self.windshieldViewController?.refreshLayout))
      self.extendedWindshieldViewController.perform(#selector(self.extendedWindshieldViewController?.refreshLayout))

      // Repeat setting the scrollView content offset for cases where the feld has a smaller width than the screen width
      let feldFrame = self.frameForFeldAtIndex(self.currentFeldIndex)
      self.contentScrollView.setContentOffset(feldFrame.origin, animated: false)
    }
    
    if animated {
      UIView.animate(withDuration: 1.0/3.0, delay: 0, options: .beginFromCurrentState, animations: {
        animations()
        }, completion: nil)
    } else {
      animations()
    }
    
    if zoomModeEnabled {
      if index <= systemModel.felds.count && index != presentationViewController.feldIndex {
        presentationViewController.feldIndex = index
      }
    }
    
    // Update button image
    let imageName = zoomModeEnabled ? "mobile-workspace-zoom-out-icon" : "mobile-workspace-zoom-in-icon"
    zoomToggleButton.setImage(UIImage(named: imageName), for: UIControlState())
    
    // Update selected feld
    feldSwitcherView.selectedFeldIndex = index
    
    // TODO: Check for zoomModeEnabled at the beginning to try to simpligy call 
    // Analytics. It's a bit hacky but a simple way to filter if the zoom is made by the user or by a layout call is to check if it's animated
    if animated == false {
      return
    } else if zoomModeEnabled {
      let source = extendedWindshieldToggleView.selected ? analyticsManager.extendedWindshieldAttribute : analyticsManager.windshieldAttribute
      
      let presentationIsActive = workspace!.presentation.active ? analyticsManager.yesAttribute : analyticsManager.noAttribute
      
      analyticsManager.tagEvent(analyticsManager.startWorkspaceZoomEvent, attributes:[ analyticsManager.sourceKey : source,
        analyticsManager.presentationActiveKey : presentationIsActive])
      
      analyticsManager.tagScreen(analyticsManager.workspaceZoomScreen)
    } else {
      analyticsManager.tagWorkspaceStatusScreen()
    }
  }


  // MARK: Edge scrolling
  fileprivate func beginEdgeScrolling() {
    if edgeScrollingTimer != nil {
      return
    }
    
    edgeScrollingSpeed = 0.0
    edgeScrollingTimer = Timer.scheduledTimer(timeInterval: 0.03, target: self, selector: #selector(self.handleEdgeScrollingTimer), userInfo: nil, repeats: true)
  }
  
  fileprivate func endEdgeScrolling() {
    if edgeScrollingTimer != nil {
      edgeScrollingTimer.invalidate()
      edgeScrollingTimer = nil
    }
  }
  
  @objc func handleEdgeScrollingTimer(_ timer: Timer) {
    var contentOffset = contentScrollView.contentOffset
    contentOffset.x += CGFloat(edgeScrollingSpeed)
    
    let maxContentOffsetX = !zoomModeEnabled ? contentScrollView.contentSize.width - view.frame.size.width + contentScrollView.contentInset.left - WorkspaceContentSizeXPadding
      : workspaceContentView.frame.size.width - view.frame.size.width
    
    contentOffset.x = min(maxContentOffsetX, contentOffset.x)
    
    let minContentOffsetX = !zoomModeEnabled ? -contentScrollView.contentInset.left : 0
    contentOffset.x = max(minContentOffsetX, contentOffset.x)
    
    contentScrollView.contentOffset = contentOffset
    
    windshieldViewController.updateOnEdgeScrolling()
  }
  
  fileprivate func handleEdgeScrollingEventAtLocation(_ location: CGPoint) {
    let scrollZone = UIDevice.current.isIPad() ? CGFloat(120.0) : CGFloat(64.0)
    let maxScrollSpeed = UIDevice.current.isIPad() ? CGFloat(24.0) : CGFloat(12.0)
    
    // Not actually sure why view location is affected by contentOffset, since this is supposed to be in the UIScrollView's coordinate feld ...
    var loc = location
    loc.x -= contentScrollView.contentOffset.x
    
    let viewWidth = contentScrollView.frame.size.width
    
    if (loc.x < scrollZone)
    {
      edgeScrollingSpeed = Float(-(scrollZone - loc.x) / scrollZone * maxScrollSpeed)
      beginEdgeScrolling()
    }
    else if (loc.x > viewWidth - scrollZone)
    {
      edgeScrollingSpeed = Float((loc.x - (viewWidth - scrollZone)) / scrollZone * maxScrollSpeed)
      beginEdgeScrolling()
    }
    else
    {
      edgeScrollingSpeed = 0.0
      endEdgeScrolling()
    }
  }
  
  fileprivate func updateFeldIndexWithLocation(_ location: CGPoint) {
    endEdgeScrolling()
    
    let index = feldIndexAtLocation(location)
    scrollToFeldAtIndex(index)
  }
  
  // TO DO. Only used in the windshield... Remove it from here
  @objc func isLocationInCorkboardSpace(_ location: CGPoint) -> Bool {
    return !extendedWindshieldContainerView.isHidden
  }
  
  
  // MARK: Felds Scrolling
  fileprivate func scrollToFeldAtIndex(_ index: Int) {
    currentFeldIndex = index
    scrollContentViewCenteringToFeld(currentFeldIndex)
    feldSwitcherView.selectedFeldIndex = currentFeldIndex
    presentationViewController.feldIndex = currentFeldIndex
  }
  
  fileprivate func scrollContentViewCenteringToFeld(_ index: Int) {
    var contentOffset = contentScrollView.contentOffset
    contentOffset.x = horizontalContentOffsetForFeldIndex(index)
    contentScrollView.setContentOffset(contentOffset, animated: true)
  }
  
  
  // MARK: IBActions
  @IBAction func toogleZoomMode(_ sender: AnyObject) {
    setZoomMode(!zoomModeEnabled, animated: true)
  }
  
  @IBAction func showWorkspaceOptions(_ sender: AnyObject) {
    let windshieldMenuViewController = WindshieldMenuViewController(nibName: nil, bundle: nil)
    windshieldMenuViewController.windshieldType = extendedWindshieldContainerView.isHidden ? .primary : .extended
    windshieldMenuViewController.communicator = communicator
    windshieldMenuViewController.windshield = workspace?.windshield
    windshieldMenuViewController.workspaceViewController = self

    let popover = FPPopoverController(contentViewController:windshieldMenuViewController)
    popover?.delegate = self
    popover?.stylizeAsActionSheet()
    popover?.presentPopover(from: sender.frame, in: sender.superview, permittedArrowDirections:FPPopoverArrowDirectionUp, animated: true)
    currentPopover = popover
  }
  
  @IBAction func stopPresentation(_ sender: AnyObject) {
    delegate?.workspaceViewControllerDidStopPresentation(self)
  }
  
  // MARK: Notifications
  @objc func dragDropManagerWillBeginDrag(_ notification: Notification) {
    currentDragDropRecognizer = notification.userInfo![OBGestureRecognizerDictionaryKey] as? UIGestureRecognizer
    
    let ovum = notification.userInfo![OBOvumDictionaryKey] as? OBOvum
    if (ovum!.source is WindshieldViewController) {
      return
    }
  }
  
  @objc func dragDropManagerDidEndDrag(_ notification: Notification) {
    self .endEdgeScrolling()
    currentDragDropRecognizer = nil
  }
  
  
  // MARK: Other
  fileprivate func initAccessibility() {
    workspaceOptionsButton.accessibilityIdentifier = "WorkspaceView.workspaceOptionsButton"
    zoomToggleButton.accessibilityIdentifier = "WorkspaceView.ZoomButton"
    
    presentationModeToolbar.accessibilityIdentifier = "WorkspaceView.PresentationModeToolbar"
    deckSliderView.accessibilityIdentifier = "WorkspaceView.DeckSliderView"
  }
}


// MARK: PresentationViewControllerDelegate
extension WorkspaceViewController: PresentationViewControllerDelegate {
  
  func presentationViewControllerZoomState() -> CGFloat {
    let state = 1.0 - (contentScrollView.zoomScale - contentScrollView.minimumZoomScale) / (contentScrollView.maximumZoomScale - contentScrollView.minimumZoomScale)
    return state;
  }
  
  func presentationViewUpdateWorkspaceViewControllerDeckSlider() {
    updateDeckSliderLocationForSlider(deckSliderView)
  }
}


// MARK: WindshieldViewControllerDelegate
extension WorkspaceViewController: WindshieldViewControllerDelegate {
  
  func windshieldViewUpdateSpaceIndex(withLocation location: CGPoint) {
    updateFeldIndexWithLocation(location)
  }
  
  func windshieldViewPresentDragDropDeletionArea(withDeletion allowDeletion: Bool) {
    // show delete area in the container
    delegate?.workspaceViewControllerDidDraggingWindshieldItem(self)
  }
  
  func windshieldViewHideDragDropDeletionArea() {
    // hide delete area in the container
    delegate?.workspaceViewControllerDidStopDraggingWindshieldItem(self)
  }
  
  func windshieldViewEndEdgeScrolling() {
    endEdgeScrolling()
  }
  
  func windshieldViewEdgeScrollingEvent(atLocation location: CGPoint) {
    handleEdgeScrollingEventAtLocation(location)
  }
}


// MARK: DeckSliderViewDelegate
extension WorkspaceViewController: DeckSliderViewDelegate {
  func layoutDeckSlider(_ nextTraitCollection: UITraitCollection?) {
    var trackHeight: CGFloat = 20.0
    var alpha: CGFloat = 1.0
    
    if (nextTraitCollection!.isCompactHeight() || (traitCollection.isCompactHeight() && nextTraitCollection != traitCollection)) {
      trackHeight = 14.0
      alpha = 0.42
    }
    
    deckSliderView.trackHeight = trackHeight
    
    if workspace!.presentation != nil {
      deckSliderView.alpha = workspace!.presentation.active && state == .full ? alpha : 0.0
    } else {
      deckSliderView.alpha = 0.0
    }
  }
  
  func updateDeckSlider(_ deckSlider: DeckSliderView) {
    
    var totalNumberOfSlides: Int = 0
    var currentSlideIndex: Int = 0
    
    if workspace != nil {
      totalNumberOfSlides = workspace!.presentation != nil ? Int(workspace!.presentation.numberOfSlides) : 0
      currentSlideIndex = workspace!.presentation != nil ? workspace!.presentation.currentSlideIndex : 0
    }
    
    deckSlider.updateThumbWidthforSlides(Int(totalNumberOfSlides), andCurrentSlide: currentSlideIndex)
    updateDeckSliderLocationForSlider(deckSlider)
  }
  
  func updateDeckSliderLocationForSlider(_ deckSlider: DeckSliderView) {
    let slideIndex = (deckSlider == deckSliderView) ? presentationViewController.currentSlideIndex : workspace!.presentation.currentSlideIndex
    deckSlider.setSlideIndex(slideIndex, numberOfSlides: Int(workspace!.presentation.numberOfSlides))
  }
  
  func slideIndexForSliderPosition(_ position: Float) -> Int {
    let f = Float(workspace!.presentation.numberOfSlides - 1) * position
    return Int(round(f))
  }
  
  // Called when deck slider's frame is changed, causing a UIView layout pass
  func viewDidLayout(_ view: DeckSliderView!) {
    updateDeckSlider(view)
  }
  
  func didStartDraggingThumbslider() {
    tmpNewSlide = presentationViewController.currentSlideIndex
    presentationViewController.sliderIsBeingDragged = true
  }
  
  func didDragThumbslider(_ position: CGFloat) {
    let newSelectedSlide = slideIndexForSliderPosition(Float(position))
    
    if newSelectedSlide != tmpNewSlide {
      presentationViewController.goToSlide(at: newSelectedSlide)
      tmpNewSlide = newSelectedSlide
    }
  }
  
  func didFinishDraggingThumbslider(atPosition position: CGFloat) {
    presentationViewController.sliderIsBeingDragged = false
    // Such that the slider snaps to actual slide positions
    updateDeckSliderLocationForSlider(deckSliderView)
    let newSelectedSlide = slideIndexForSliderPosition(Float(position))
    presentationViewController.goToSlide(at: newSelectedSlide)
    MezzanineAppContext.current().currentCommunicator.requestor.requestPresentationScrollRequest(workspace!.uid, currentIndex: slideIndexForSliderPosition(Float(position)))
  }
}


// MARK: UIGestureRecognizerDelegate
extension WorkspaceViewController: UIGestureRecognizerDelegate {
  
  func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
    if presentationPanGesture == gestureRecognizer {
      if !workspace!.presentation.active || !extendedWindshieldContainerView.isHidden {
        return false
      }
    }
    return true
  }
  
  func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
    if gestureRecognizer == threeFingerPanGesture {
      return true
    }
    
    return false
  }
  
  @objc func handleThreeFingerDetection(_ gestureRecognizer: UIPanGestureRecognizer) {
    if gestureRecognizer.state == .began {
      // Cancel any drag and drop that's currently running
      if (currentDragDropRecognizer != nil) {
        // Need to capture this gesture recognizer because the .enabled = NO call will lead to
        // the nilifying of the _currentDragDropRecognizer property, thus making the .enabled = YES call useless
        let dragDropRecognizerToCancel = currentDragDropRecognizer!
        
        dragDropRecognizerToCancel.isEnabled = false
        dragDropRecognizerToCancel.isEnabled = true
      }
    }
  }
}


// MARK: UIScrollViewDelegate
extension WorkspaceViewController: UIScrollViewDelegate {
  
  func viewForZooming(in scrollView: UIScrollView) -> UIView? {
    return workspaceContentView
  }
  
  func scrollViewDidZoom(_ scrollView: UIScrollView) {
    if !firstLoad {
      contentScrollView.updateContentViewLayout(false)
      scrollContentViewCenteringToFeld(currentFeldIndex)
    }
  }
  
  func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
    scrollView.pinchGestureRecognizer!.isEnabled = false
  }
  
  func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
    
    var targetFeldIndex = feldIndexAtLocation(targetContentOffset.pointee)
    
    if zoomModeEnabled {
      if currentFeldIndex < targetFeldIndex {
        targetFeldIndex = currentFeldIndex + 1
      } else if currentFeldIndex > targetFeldIndex {
        targetFeldIndex = currentFeldIndex - 1
      }
    }
    
    targetContentOffset.pointee.x = horizontalContentOffsetForFeldIndex(targetFeldIndex)
    
    if zoomModeEnabled {
      if targetFeldIndex <= systemModel.felds.count && targetFeldIndex != presentationViewController.feldIndex {
        presentationViewController.feldIndex = targetFeldIndex
      }
    }
    
    feldSwitcherView.selectedFeldIndex = targetFeldIndex
    currentFeldIndex = targetFeldIndex
  }
}


// MARK: FeldSwitcherViewDelegate
extension WorkspaceViewController: FeldSwitcherViewDelegate {
  
  func didTap(onFeld feldIndex: Int) {
    scrollToFeldAtIndex(feldIndex)
  }
}


// MARK: CorkboardToggleViewDelegate
extension WorkspaceViewController: CorkboardToggleViewDelegate {
  
  func showCorkboardPanelBlurredView(_ show: Bool) {
    UIView.animate(withDuration: 0.33, animations: {
      self.presentationModeToolbar.alpha = !show ? 1.0 : 0.0
      self.feldSwitcherView.alpha = (!show && self.zoomModeEnabled && self.systemModel.felds.count > 1) ? 1.0 : 0.0
      self.extendedWindshieldBlurredView.alpha = show ? 1.0 : 0.0
    }, completion: { (Bool) in
      if !show {
        self.extendedWindshieldContainerView.isHidden = true
        self.extendedWindshieldToggleView.selected = false
      }
    }) 
  }
  
  func didTapOnCorkboardToggle() {
    self.updateCorkboardLabel()
    
    if extendedWindshieldContainerView.isHidden {
      extendedWindshieldContainerView.isHidden = false
      windshieldViewController.showPlaceholderView(false, animated: false)
      showCorkboardPanelBlurredView(true)
      
      extendedWindshieldViewController.presentExtendedWindshield(completion: {
        self.extendedWindshieldToggleView.selected = true
        OperationQueue.main.addOperation({
          self.windshieldViewController.showPlaceholderView(false, animated: false)
        })
      })
    } else {
      showCorkboardPanelBlurredView(false)
      extendedWindshieldViewController.hideExtendedWindshield()
    }
  }
}


// MARK:OBDropZone
extension WorkspaceViewController: OBDropZone {
  func ovumMoved(_ ovum: OBOvum!, in view: UIView!, atLocation location: CGPoint) -> OBDropAction {
    if (view == contentScrollView) {
      handleEdgeScrollingEventAtLocation(location)
      return .none
    }
    
    return ovum.dropAction
  }
  
  func ovumDropped(_ ovum: OBOvum!, in view: UIView!, atLocation location: CGPoint) {
    
  }
  
  func ovumExited(_ ovum: OBOvum!, in view: UIView!, atLocation location: CGPoint) {
    
  }
  
  func ovumEntered(_ ovum: OBOvum!, in view: UIView!, atLocation location: CGPoint) -> OBDropAction {
    if view == contentScrollView {
      return .none
    } else if (view == extendedWindshieldPanelDropZoneView) {
      if (systemModel.sideWallScreenCount() == 0) {
        return .none;
      }
      
      extendedWindshieldToggleView.toggleCorkboard(nil)
      windshieldViewController.showPlaceholderView(false, animated: false)
    }
    
    return .none
  }
  
  
  func handleDropAnimation(for ovum: OBOvum!, withDrag dragView: UIView!, dragDropManager: OBDragDropManager!) {
    // Empty, only to bypass OBDragDropManager's default drop animation (and thus removing the view from existance)
    dragDropManager.animateOvumDrop(ovum, withAnimation: {
      // Do Nothing but let OBDragDropManager hide its overlayWindow
      }, completion: nil)
  }
}

// MARK: FPPopoverControllerDelegate
extension WorkspaceViewController: FPPopoverControllerDelegate {
  func popoverControllerDidDismissPopover(_ popoverController: FPPopoverController) {
    if popoverController == currentPopover {
      let windshieldMenu = popoverController.viewController as! WindshieldMenuViewController
      windshieldMenu.workspaceViewController = nil
      currentPopover = nil
    }
  }
}
