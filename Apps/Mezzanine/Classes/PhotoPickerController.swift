//
//  PhotoPickerController.swift
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 03/01/2017.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import UIKit

@objc class PhotoPickerController: MezzanineNavigationController {
  
  // Delegate is reserved for UINavigationControllerDelegate and can't be overridden
  var pickerDelegate: PhotoPickerDelegate!
  var selectedPhotos = NSMutableArray()
  
  @objc required convenience init(delegate: PhotoPickerDelegate) {
    self.init()
    
    self.pickerDelegate = delegate
    let albums = PhotoManager.sharedInstance.getPhotoAlbums()
    let vc = PhotoAlbumsViewController(containerController: self, albums: albums)
    self.setViewControllers([vc], animated: false)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }

  
  func cancel() {
    dismiss(animated: true) {
      self.pickerDelegate?.photoPickerControllerDidCancel()
    }
  }
  
  func didFinishPickingAssets() {
    dismiss(animated: true) {
      self.pickerDelegate?.photoPickerControllerDidSelectPhotos(self.selectedPhotos)
    }
  }
}


@objc protocol PhotoPickerDelegate: NSObjectProtocol {
  func photoPickerControllerDidSelectPhotos(_ photos: NSArray)
  func photoPickerControllerDidCancel()
}
