//
//  OBOvum+Adorn.m
//  Mezzanine
//
//  Created by Zai Chang on 7/10/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import "OBOvum+Adorners.h"
#import "MezzanineStyleSheet.h"


@implementation OBOvum (Adorners)

static NSInteger kOvumDeletionViewTag = -2212;
static NSInteger kOvumMessageLabelTag = -2213;

//static CGFloat kOvumLabelOffsetY = 56.0;


-(void) adornWithDeletionMode:(BOOL)deletionMode
{
  if (dragView == nil)
    return;
  
  if (deletionMode)
  {
    // Add more obvious feedback to ovum if its about to be deleted
    UIView *ovumDeletionView = [dragView viewWithTag:kOvumDeletionViewTag];
    if (!ovumDeletionView)
    {
      CGRect ovumDeletionViewFrame = dragView.bounds;
      ovumDeletionView = [[UIView alloc] initWithFrame:ovumDeletionViewFrame];
      ovumDeletionView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
      ovumDeletionView.tag = kOvumDeletionViewTag;
      ovumDeletionView.backgroundColor = [UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:0.33];
      ovumDeletionView.userInteractionEnabled = NO;
      [dragView addSubview:ovumDeletionView];
      
//      [self adornWithMessage:@"Delete" tintColor:[UIColor redColor]];
    }
  }
  else
  {
    [[dragView viewWithTag:kOvumDeletionViewTag] removeFromSuperview]; // Remove deletion view if it exists
//    [self adornWithMessage:nil tintColor:nil];
  }
}


-(void) adornWithMessage:(NSString*)message tintColor:(UIColor*)tintColor
{
  if (dragView == nil)
    return;
  
  if (message)
  {
    // Add more obvious feedback to ovum if its about to be deleted
    UILabel *label = (UILabel*) [dragView viewWithTag:kOvumMessageLabelTag];
    
    CGFloat fontSize = 13.0;
    
    if (!label)
    {
      dragView.clipsToBounds = NO;
      
      CGRect frame = CGRectMake(0.0, CGRectGetMidY(dragView.bounds) - 56.0, dragView.bounds.size.width, 56.0);
      label = [[UILabel alloc] initWithFrame:frame];
      label.tag = kOvumMessageLabelTag;
      label.backgroundColor = [MezzanineStyleSheet sharedStyleSheet].darkestFillColor;
      //label.backgroundColor = [MezzanineStyleSheet sharedStyleSheet].textHighlightedColor;
      label.font = [UIFont dinBoldOfSize:fontSize];
      label.textAlignment = NSTextAlignmentCenter;
      label.textColor = [UIColor whiteColor];
      label.shadowColor = [UIColor blackColor];
      label.shadowOffset = CGSizeMake(0, 1);
      label.layer.cornerRadius = 5.0;
      label.layer.borderColor = tintColor.CGColor;
      label.layer.borderWidth = 1.0;
      label.layer.shadowOffset = CGSizeMake(0, 2);
      label.layer.shadowColor = [UIColor blackColor].CGColor;
      label.layer.shadowOpacity = 0.33;

      [dragView addSubview:label];
    }
    
    label.text = message;
    [label sizeToFit];
    label.frame = CGRectInset(label.frame, -12.0, -2.0);
    label.center = CGPointMake(CGRectGetMidX(dragView.bounds), -fontSize/2.0);
  }
  else
  {
    [[dragView viewWithTag:kOvumMessageLabelTag] removeFromSuperview]; // Remove deletion view if it exists
  }
}


-(void) clearAdorner
{
  [self adornWithDeletionMode:NO];
  [self adornWithMessage:nil tintColor:nil];
}

@end
