//
//  VideoPlaceholderView.h
//  Mezzanine
//
//  Created by miguel on 17/07/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoPlaceholderView : UIView

@property (nonatomic, strong) NSString *sourceName;
@property (nonatomic, strong) NSString *sourceOrigin;
@property (nonatomic, assign) BOOL isLocalVideo;

@property (nonatomic, assign) CGFloat maxTitleFontSize;
@property (nonatomic, assign) CGFloat minTitleFontSize;
@property (nonatomic, assign) CGFloat minSubtitleFontSize;

@end
