//
//  UserSignInViewController.h
//  Mezzanine
//
//  Created by miguel on 12/03/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserSignInViewController : UIViewController <UITextFieldDelegate>

@property (nonatomic, strong) IBOutlet UITextField *usernameField;
@property (nonatomic, strong) IBOutlet UITextField *passwordField;
@property (nonatomic, strong) IBOutlet UILabel *signInLabel;
@property (nonatomic, strong) IBOutlet UIButton *signInButton;

@property (nonatomic, strong) IBOutlet NSLayoutConstraint *topConstraint;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *scrollViewBottomConstraint;

@property (nonatomic, copy) void (^signInRequested)(NSString *username, NSString *password);

-(IBAction) signIn:(id)sender;

@end
