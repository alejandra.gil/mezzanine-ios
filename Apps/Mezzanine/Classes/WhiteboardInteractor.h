//
//  WhiteboardInteractor.h
//  Mezzanine
//
//  Created by Zai Chang on 12/4/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>

// Still trying to decide whether this thin layer is really that necessary or useful
// Perhaps MZCommunicator is the interaction layer itself and we don't need another
// layer in between
@interface WhiteboardInteractor : NSObject

@property (nonatomic, retain) MZSystemModel *systemModel;
@property (nonatomic, retain) MZCommunicator *communicator;

-(MZTransaction*) requestWhiteboardCapture:(NSString*)uid;  
-(void) requestWhiteboardCaptureAll;  // Capture all whiteboards

@end
