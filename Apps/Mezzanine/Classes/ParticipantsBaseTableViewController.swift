//
//  ParticipantsBaseTableViewController.swift
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 02/10/2018.
//  Copyright © 2018 Oblong Industries. All rights reserved.
//

import UIKit

class ParticipantsBaseTableViewController: UITableViewController {
  
  var communicator: MZCommunicator?
  var systemModel: MZSystemModel?

  func updateTableView(mezzanines: Array<MZMezzanine>) {
    //To be overridden
  }
}
