//
//  WindshieldViewController.m
//  Mezzanine
//
//  Created by Miguel Sánchez Valdés on 5/10/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import "WindshieldViewController.h"
#import "WindshieldViewController_Private.h"
#import "PortfolioViewController.h"
#import "MezzanineMainViewController.h"
#import "MezzanineStyleSheet.h"
#import "VideoAssetView.h"
#import "OBOvum+Adorners.h"
#import "UIGestureRecognizer+OBDragDrop.h"
#import "CAAnimation+EasingEquations.h"
#import "LiveStreamsViewController.h"
#import "ButtonMenuViewController.h"
#import "AssetViewController.h"
#import "ImageAssetView.h"
#import "UIViewController+FPPopover.h"
#import "AssetView.h"
#import "UIView+OBDropZone.h"
#import "Mezzanine-Swift.h"

#define kWindshieldItemDeletionAnimationDuration 0.25


#define CHECK_SHIELDER_GRAB_RELINQUISH 1

@implementation WindshieldViewController (Debug)

- (void)checkShielderGrab:(MZWindshieldItem*)item
{
#if __DEBUG__ && CHECK_SHIELDER_GRAB_RELINQUISH
  NSAssert((aShielderIsGrabbed == NO), @"A shielder is being grabbed before the previous one is released: %@", item.uid);     // A previous shielder is still grabbed. This shouldn't happen

  DLog(@"Shielder grabbed: %@", item.uid);
  aShielderIsGrabbed = YES;
#endif
}


- (void)checkShielderRelinquish:(MZWindshieldItem*)item
{
#if __DEBUG__ && CHECK_SHIELDER_GRAB_RELINQUISH
  NSAssert((aShielderIsGrabbed == YES), @"A shielder is being relinquished without previously grabbed: %@", item.uid);

  DLog(@"Shielder relinquished: %@", item.uid);
  aShielderIsGrabbed = NO;
#endif
}

@end



@implementation WindshieldViewController

@synthesize draggedView;

-(id) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self)
  {
    _firstLoad = YES;
  }
  return self;
}


-(void) dealloc
{
  DLog(@"%@ dealloc", [self class]);
  [self setDraggedView:nil];

  [insertionIndicators removeAllObjects];
  [self removeAllItemViews];
}


#pragma mark - View lifecycle

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
  [super viewDidLoad];

  self.view.clipsToBounds = YES;  // Prevents windshield items from sprawling over to corkboard spaces

//  [self.view setBackgroundColor:[UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:0.3]]; // Was used for debugging windshield bounds
  
  // Register view as a drop zone that will be handled by its controller
  self.view.dropZoneHandler = self;
  itemViews = [[NSMutableArray alloc] init];
  insertionIndicators = [[NSMutableArray alloc] init];


  if ([NSUserDefaults standardUserDefaults].windshieldParallaxEnabled)
  {
    CGFloat range = 64.0;
    UIInterpolatingMotionEffect *verticalMotionEffect = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.y" type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
    verticalMotionEffect.minimumRelativeValue = @(-range);
    verticalMotionEffect.maximumRelativeValue = @(range);
    UIInterpolatingMotionEffect *horizontalMotionEffect = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.x" type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
    horizontalMotionEffect.minimumRelativeValue = @(-range);
    horizontalMotionEffect.maximumRelativeValue = @(range);
    
    UIMotionEffectGroup *effectGroup = [UIMotionEffectGroup new];
    effectGroup.motionEffects = @[horizontalMotionEffect, verticalMotionEffect];
    [self.view addMotionEffect:effectGroup];
  }
}


- (void)viewDidUnload
{
  [super viewDidUnload];

  [insertionIndicators removeAllObjects];
  [self removeAllItemViews];
}


- (void)viewDidAppear:(BOOL)animated
{
  [super viewDidAppear:animated];
  [[NSOperationQueue mainQueue] addOperationWithBlock:^{
    _firstLoad = NO;
  }];
}


- (void)viewWillDisappear:(BOOL)animated
{
  [super viewWillDisappear:animated];

  FPPopoverController *currentPopover = self.currentPopover;
  if ([currentPopover isPopoverVisible])
    [currentPopover dismissPopoverAnimated:YES];
}


- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
  return UIInterfaceOrientationMaskPortrait;
}


- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
  if (self.currentPopover)
    [self.currentPopover dismissPopoverAnimated:YES];
}

-(void) setDraggedView:(AssetView *)aView
{
  draggedView = aView;

  if (draggedView)
    draggedView.isBeingManipulated = YES;
}


- (void)setWorkspace:(MZWorkspace *)workspace
{
  if (_workspace != workspace)
  {
    if (_workspace.wasDestroyed)
    {
      // Previous workspace was destroyed
      [self removeAllItemViews];
    }
    
    _workspace = workspace;
  }
}

-(void) setWindshield:(MZWindshield *)aWindshield
{
  if (_windshield != aWindshield)
  {
    if (_windshield)
      [self endObservingWindshield:_windshield];

    _windshield = aWindshield;

    if (_windshield)
    {
      [self beginObservingWindshield:_windshield];
      [self loadAllWindshieldItems];
    }
  }
}


#pragma mark - Observation

-(void) beginObservingWindshield:(MZWindshield *)aWindshield
{
  [aWindshield addObserver:self forKeyPath:@"items" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:nil];
  for (MZWindshieldItem *item in aWindshield.items)
  {
    [item addObserver:self forKeyPath:@"frame" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld | NSKeyValueObservingOptionInitial) context:nil];
  }
}

-(void) endObservingWindshield:(MZWindshield *)aWindshield
{
  [aWindshield removeObserver:self forKeyPath:@"items"];
  for (MZWindshieldItem *item in aWindshield.items)
  {
    [item removeObserver:self forKeyPath:@"frame"];
  }
}


#pragma mark - Overridden by subclasses

- (void)loadAllWindshieldItems
{
#if __DEBUG__
  NSAssert(NO, @"This method is expected to be called only from subclasses.");
#endif
}


- (CGFloat)zoomScaleOfWindshield
{
#if __DEBUG__
  NSAssert(NO, @"This method is expected to be called only from subclasses.");
#endif
  
  return 0.0;
}


- (CGFloat)surface:(MZSurface *)surface feldWidth:(MZFeld *)feld
{
#if __DEBUG__
  NSAssert(NO, @"This method is expected to be called only from subclasses.");
#endif
  
  return 0.0;
}


- (CGFloat)surface:(MZSurface *)surface feldHeight:(MZFeld *)feld
{
#if __DEBUG__
  NSAssert(NO, @"This method is expected to be called only from subclasses.");
#endif
  
  return 0.0;
}


- (MZFeld *)closestFeldAtLocation:(CGPoint)location
{
#if __DEBUG__
  NSAssert(NO, @"This method is expected to be called only from subclasses.");
#endif
  return nil;
}


- (CGRect)rectFromNativeWindshieldCoordinatesForItem:(MZWindshieldItem *)item
{
#if __DEBUG__
  NSAssert(NO, @"This method is expected to be called only from subclasses.");
#endif
  
  return CGRectZero;
}


- (CGRect)rectToNativeWindshieldCoordinates:(CGRect)frame inFeld:(MZFeld *)feld
{
#if __DEBUG__
  NSAssert(NO, @"This method is expected to be called only from subclasses.");
#endif
  
  return CGRectZero;
}

- (void)updateCropViewForItem:(MZWindshieldItem *)item
{
#if __DEBUG__
  NSAssert(NO, @"This method is expected to be called only from subclasses.");
#endif
}



#pragma mark - Items Layout


-(void) refreshItemsLayout:(NSArray *)items
{
  for (MZWindshieldItem *windshieldItem in items)
  {
    AssetView *assetView = [self viewForItem:windshieldItem];
    if (assetView == draggedView || [droppedShielderWaitingForConfirmation isEqual:windshieldItem])
    { // Do not layout the item that's being dragged but the frame before the drag.
      frameBeforeDrag = [self rectFromNativeWindshieldCoordinatesForItem:windshieldItem];
      continue;
    }

    void (^animations)() = ^{
      assetView.frame = [self rectFromNativeWindshieldCoordinatesForItem:windshieldItem];
      assetView.layer.borderWidth = [self borderWidth];
    };

    // Doing this non-animatedly seems to help with avoiding an unwanted animation the first load
    if (_firstLoad)
      animations();
    else
      [UIView animateWithDuration:1.0/3.0 animations:animations];
    
    if ([assetView isKindOfClass:[VideoAssetView class]])
      [self updateLabelFontSizeForVideoAssetView:(VideoAssetView *)assetView];
  }
}


#pragma mark -
#pragma mark Item Views Management

-(AssetView*) viewForItem:(id)item
{
  NSUInteger count = [itemViews count];
  
  if ([item isKindOfClass:[MZWindshieldItem class]])
  {
    for (int i=0; i<count; i++)
    {
      AssetView *itemView = itemViews[i];
      if (itemView.asset == item)
        return itemView;
    }
  }

  return nil;
}


-(void) removeItemView:(AssetView*)itemView animated:(BOOL)animated
{
  MZWindshieldItem *windshieldItem = (MZWindshieldItem *)itemView.asset;
  if (windshieldItem && windshieldItem != (id)[NSNull null])
  {
    itemView.asset = nil;
    if ([itemView isKindOfClass:[VideoAssetView class]])
      [(VideoAssetView *)itemView setLiveStream:nil];

    [self dismissPopoverIfContextMatches:@[windshieldItem]];
  }
  
  if (animated)
  {
    [UIView animateWithDuration:kWindshieldItemDeletionAnimationDuration
                     animations:^(){
                       [itemView setAlpha:0.0];

                       CGFloat yTranslation = -[self surface:windshieldItem.surface feldHeight:windshieldItem.feld] * 0.33;

                       CGAffineTransform transform = CGAffineTransformIdentity;
                       transform = CGAffineTransformScale(transform, 0.9, 0.9);
                       transform = CGAffineTransformTranslate(transform, 0, yTranslation);
                       itemView.transform = transform;
                     }
                     completion:^(BOOL finished) {
                       [itemView removeFromSuperview];
                       [itemViews removeObject:itemView];
                     }];
  }
  else
  {
    [itemView removeFromSuperview];
    [itemViews removeObject:itemView];
  }
}

-(void) removeViewForItem:(MZWindshieldItem *)item animated:(BOOL)animated
{
  AssetView *assetView = [self viewForItem:item];
  if (assetView)
  {
    [self removeItemView:assetView animated:animated];
  }
  
  [self dismissPopoverIfContextMatches:@[item]];
}

-(void) removeAllItemViews
{
  NSArray *itemViewsToRemove = [itemViews copy];
  for (AssetView *item in itemViewsToRemove)
    [self removeItemView:item animated:NO];
}

-(AssetView*) assetViewAtViewPosition:(CGPoint)position
{
  UIView *hitView = [self.view hitTest:position withEvent:nil];
  if (hitView && [hitView isKindOfClass:[AssetView class]])
    return (AssetView*)hitView;
  return nil;
}


-(BOOL) itemIsApprovedForConsumption:(MZWindshieldItem *) item
{
  // Check if the frame is valid
  if (!isfinite(item.frame.origin.x) || !isfinite(item.frame.origin.y) ||
      !isfinite(item.frame.size.width) || !isfinite(item.frame.size.height))
    return NO;
  
  return YES;
}


-(void) insertViewForItem:(MZWindshieldItem *) item
{
  if (![self itemIsApprovedForConsumption:item])
    return;
  
  CGRect frame = [self rectFromNativeWindshieldCoordinatesForItem:item];
  
	AssetView *itemView = nil;
  
  MezzanineStyleSheet *styleSheet = [MezzanineStyleSheet sharedStyleSheet];
  if ([item.assetUid hasPrefix:@"as-"])
  {
    itemView = [[ImageAssetView alloc] initWithFrame:frame];
    itemView.contentView.backgroundColor = styleSheet.shielderBackgroundColor;
    [(ImageAssetView *)itemView setImageSourceType:ImageAssetViewLargeThumb];
    [(ImageAssetView *)itemView imageView].backgroundColor = [UIColor clearColor];
    [(ImageAssetView *)itemView imageView].contentMode = UIViewContentModeScaleAspectFill;
  }
  else if ([item.assetUid hasPrefix:@"vi-"])
  {
    VideoAssetView *videoAssetView = [[VideoAssetView alloc] initWithFrame:frame];
    videoAssetView.contentView.backgroundColor = styleSheet.shielderBackgroundColor;

    [videoAssetView setUserInfo:item];
    
    MZLiveStream *liveStream = [self.workspace liveStreamWithUid:item.assetUid];
    if (liveStream)
    {
      // For remote videos, update the name if its missing
      if (!liveStream.local && !liveStream.remoteMezzName)
      {
        NSArray *remoteMezzes = _systemModel.remoteMezzes;
        [_workspace updateLiveStreamRemoteMezzName:liveStream usingRemoteMezzes:remoteMezzes];
      }

      videoAssetView.liveStream = liveStream;
      videoAssetView.placeholderView.isLocalVideo = liveStream.local;
    }

    // On the workspace, items are most likely shrunk as a result of zoom, so we need bigger font sizes to compensate
    [self updateLabelFontSizeForVideoAssetView:videoAssetView];

    itemView = videoAssetView;
  }
  else if ([item.assetUid hasPrefix:@"web-"])
  {
    itemView = [[ImageAssetView alloc] initWithFrame:frame];
    itemView.contentView.backgroundColor = styleSheet.shielderBackgroundColor;
    [(ImageAssetView *)itemView imageView].backgroundColor = [[[MezzanineStyleSheet sharedStyleSheet] grey110Color] colorWithAlphaComponent:0.6];
  }
  else
    return;

  if (![SettingsFeatureToggles sharedInstance].teamworkUI)
    itemView.borderColor = styleSheet.shielderBorderColor;

  [itemView setAlpha:0.0];
  [itemView setMarginForGestures:0.0];
  [itemView setActionsPanelEnabled:NO]; // Disabled because windshield items are often sized arbitrarily and the delete button interaction is inconsistent and in some cases doesn't work well
  [itemView.layer setBorderWidth:[self borderWidth]];

  [self.view addSubview:itemView];
  itemView.accessibilityIdentifier = @"WindshieldViewItem";
  
  [UIView animateWithDuration:0.15 animations:^(){
    [itemView setAlpha:1.0];
  }];
  
  itemView.asset = item;

  [self addGestureRecognizers:itemView];

  [itemViews addObject:itemView];
  
}

-(void) addGestureRecognizers:(AssetView *)itemView
{
  UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(windshieldItemTapped:)];
  [itemView addGestureRecognizer:tapGestureRecognizer];

  OBDragDropManager *dragDropManager = [OBDragDropManager sharedManager];
  UIPanGestureRecognizer * gesture = (UIPanGestureRecognizer *)[dragDropManager createDragDropGestureRecognizerWithClass:[UIPanGestureRecognizer class] source:self];
  gesture.delegate = self;
  gesture.maximumNumberOfTouches = 2;
  gesture.accessibilityLabel = @"WindshieldItemPanGesture";
  [itemView addGestureRecognizer:gesture];

  // Also adding a UIPinchGestureRecognizer such that a user can pinch and immeidately scale a windshield item
  // without having to drag / move it first
  UIPinchGestureRecognizer *pinchGesture = (UIPinchGestureRecognizer *)[dragDropManager createDragDropGestureRecognizerWithClass:[UIPinchGestureRecognizer class] source:self];
  pinchGesture.delegate = self;
  [itemView addGestureRecognizer:pinchGesture];
}

-(CGFloat) borderWidth
{
  if (![SettingsFeatureToggles sharedInstance].teamworkUI)
    return (1.0 / [self zoomScaleOfWindshield]) / [UIScreen mainScreen].scale;  // Exactly one pixel at every zoom scale
  else
    return 0;
}

-(void) updateLabelFontSizeForVideoAssetView:(VideoAssetView *)videoAssetView
{
  CGFloat fontScalingFactor = 1.0 / [self zoomScaleOfWindshield];
  [videoAssetView placeholderView].maxTitleFontSize = [videoAssetView maxTitleFontSize]*fontScalingFactor;
  [videoAssetView placeholderView].minTitleFontSize = [videoAssetView minTitleFontSize]*fontScalingFactor;
  [videoAssetView placeholderView].minSubtitleFontSize = [videoAssetView minSubtitleFontSize]*fontScalingFactor;
  [[videoAssetView placeholderView] setNeedsLayout];
}

#pragma mark -
#pragma mark Insertion Indicator

-(UIView *) showInsertionIndicatorForOvum:(OBOvum*)ovum
{
  CGRect frameOnWindshieldCoordinates = [[OBDragDropManager sharedManager].overlayWindow convertRect:ovum.dragView.frame toView:self.view];

  UIView *indicator = ovum.dragView;
  if ([ovum.dragView isKindOfClass:[VideoAssetView class]])
  {
    [self updateLabelFontSizeForVideoAssetView:(VideoAssetView*)ovum.dragView];

  }
  [indicator setFrame:frameOnWindshieldCoordinates];
  [self.view addSubview:indicator];

  ovum.dragView = nil;
  ovumDragViewAsIndicator = YES;
  return indicator;
}


#pragma mark - Deletion

-(WindshieldItemContentType)analyticsContentType:(MZWindshieldItem *)item
{
  if (item.isImage)
    return WindshieldItemContentTypeImage;
  else if (item.isVideo)
    return WindshieldItemContentTypeVideo;

  return WindshieldItemContentTypeUnknown;
}

-(void) requestDeleteItem:(MZWindshieldItem*)item
{
  if (item == nil)
    return;

  MZTransaction *transaction = [self.communicator.requestor requestWindshieldItemDeleteRequest:self.workspace.uid uid:item.uid];

  MezzanineAnalyticsManager *analyticsManager = [MezzanineAnalyticsManager sharedInstance];
  [analyticsManager trackWindshieldItemDeleteEvent:[self analyticsContentType:item]];

  transaction.successBlock = ^ {
    [self setDraggedView:nil];
  };
  transaction.failedBlock = ^(NSError *error) {
    [self resetDragView];
  };
}



#pragma mark - Drag View

-(void) resetDragView
{
  draggedView.isBeingManipulated = NO;
  draggedView.alpha = 1.0;
  draggedView.frame = frameBeforeDrag;
  [self setDraggedView:nil];
}



#pragma mark - OBDropZone

-(OBDropAction) ovumEntered:(OBOvum*)ovum inView:(UIView*)view atLocation:(CGPoint)location
{
  if (hasExitedFromWindshield)
  {
    hasExitedFromWindshield = NO;
    CGRect frame = [[OBDragDropManager sharedManager].overlayWindow convertRect:ovum.dragView.frame toView:self.view];
    [draggedView setFrame:frame];
    [draggedView setAlpha:1.0];
  }

  if ([ovum.dataObject isKindOfClass:[MZAsset class]] ||
      [ovum.dataObject isKindOfClass:[MZLiveStream class]])
  {
    return OBDropActionCopy;
  }
  
  if ([_workspaceViewController isKindOfClass:[WorkspaceViewController class]] &&
      [ovum.dataObject isKindOfClass:[MZPortfolioItem class]])
  {
    return OBDropActionCopy;
  }
  
  return OBDropActionNone;
}

                                      
-(OBDropAction) ovumMoved:(OBOvum*)ovum inView:(UIView*)view atLocation:(CGPoint)location
{
  if ([ovum.dataObject isKindOfClass:[MZAsset class]] ||
      [ovum.dataObject isKindOfClass:[MZLiveStream class]] ||
      [ovum.dataObject isKindOfClass:[MZPortfolioItem class]])
  {
    if (draggedView)
    {
      [self updateDragDeletionForDraggedView:draggedView];
      CGRect frame = [[OBDragDropManager sharedManager].overlayWindow convertRect:ovum.dragView.frame toView:self.view];
      [draggedView setFrame:frame];
    }
    
    // TODO Experiment with opacity on ovum's Drag View to show in/out shielder's area.
    // ovum.dragView.alpha = 0.7;
    
    // TODO: To be refactored whenever windshieldContinuousDrag is available
    if ([NSUserDefaults standardUserDefaults].windshieldContinuousDrag)
    {
//      if ([ovum.source isKindOfClass:[WindshieldViewController class]]) // Source is WindshieldViewController ergo this is a transformation
//      {
//        MZFeld *feld = [self closestFeldAtLocation:location];
//        
//        CGRect frameOnWindshieldCoordinates = [[OBDragDropManager sharedManager].overlayWindow convertRect:ovum.dragView.frame
//                                                                                                    toView:self.view];
//        
//        CGRect frameOnRelativeCoordinates = [self rectToNativeWindshieldCoordinates:frameOnWindshieldCoordinates
//                                                                             inFeld:feld];
//        
//        FeldRelativeCoords *relativeCoords = [FeldRelativeCoords new];
//        relativeCoords.over = frameOnRelativeCoordinates.origin.x;
//        relativeCoords.up = frameOnRelativeCoordinates.origin.y;
//        relativeCoords.norm = 0.0;
//        relativeCoords.feldId = feld.name;
//        relativeCoords.valid = true;
//        
//        CGFloat aspectRatio = [(MZWindshieldItem *) ovum.dataObject aspectRatio];
//        CGFloat scale = [self scaleOfItemView:(AssetView *)ovum.dragView atLocation:location] / [self zoomScaleOfWindshield];
//        
//        FeldRelativeSize *relativeSize = [FeldRelativeSize new];
//        relativeSize.scale = scale;
//        relativeSize.aspectRatio = aspectRatio;
//        relativeSize.feldId = feld.name;
//        
//        MZSurface *surface = [_systemModel surfaceWithFeld:feld];
//        
//         [self.communicator.requestor requestWindshieldItemTransformRequest:self.workspace.uid uid:[draggedView.asset uid] surfaceName:surface.name feldRelativeCoords:relativeCoords feldRelativeSize:relativeSize];
//      }
    }
    
    return OBDropActionCopy;
  }
  
  return OBDropActionNone;
}


-(void) ovumExited:(OBOvum*)ovum inView:(UIView*)view atLocation:(CGPoint)location
{
  ovum.dragView.layer.borderColor = [UIColor clearColor].CGColor;
  [ovum clearAdorner];

  if (!hasExitedFromWindshield)
  {
    [draggedView setAlpha:0.0];
    hasExitedFromWindshield = YES;
  }
}


-(void) ovumDropped:(OBOvum*)ovum inView:(UIView*)view atLocation:(CGPoint)location
{  
  if (view == self.view)
  {
    // We need to know the location on the Windshield View so that native
    // decides whether it is a correct position or not.
    location = [self.view convertPoint:location toView:self.view];
  }

  __weak AssetView *droppedViewBlock = nil;
  
  if ([ovum.dataObject isKindOfClass:[MZAsset class]] ||
      [ovum.dataObject isKindOfClass:[MZLiveStream class]] ||
      [ovum.dataObject isKindOfClass:[MZPortfolioItem class]])
  {
    MZTransaction *transaction;
    
    if ([ovum.source isKindOfClass:[WindshieldViewController class]]) // Source is WindshieldViewController ergo this is a transformation
    {
      droppedShielderWaitingForConfirmation = ovum.dataObject;

      CGRect frameOnWindshieldCoordinates = [[OBDragDropManager sharedManager].overlayWindow convertRect:ovum.dragView.frame toView:self.view];

      // The item is from the same windshield
      if (ovum.source == self)
      {
        [draggedView setFrame:frameOnWindshieldCoordinates];
        // TODO Needed if experimenting with opacity on ovum's Drag View to show in/out shielder's area.
        //ovum.dragView.alpha = 1.0;

        droppedViewBlock = draggedView;
        [self setDraggedView:nil];
      }

      MZWindshieldItem *item = (MZWindshieldItem *) ovum.dataObject;

      MZFeld *feld = [self closestFeldAtLocation:location];
      MZSurface *surface = [_systemModel surfaceWithFeld:feld];

      CGRect frameOnRelativeCoordinates = [self rectToNativeWindshieldCoordinates:frameOnWindshieldCoordinates inFeld:feld];

      CGFloat aspectRatio = [item aspectRatio];

      CGFloat scale = 1.0;
      if ([self isKindOfClass:[FullWindshieldViewController class]])
      {
        scale = [self scaleOfItemView:(AssetView *)ovum.dragView atLocation:location] / [self zoomScaleOfWindshield];
      }
      else
      {
        // This prevents items that are only relocated to be also resized when switching between surfaces because they are not represented with the same scale
        scale = CGSizeEqualToSize(ovum.dragViewInitialSize, ovum.dragView.frame.size) ? item.scale : [self scaleOfItemView:(AssetView *)ovum.dragView atLocation:location] / [self zoomScaleOfWindshield];
      }

      // If the transformation has finished inside the valid margins to use
      // fullscreen let native handle the position and final size.
      if ([self scaleInFullscreenMargins:scale])
      {
        scale = 1.0;
        frameOnRelativeCoordinates.origin = CGPointMake(0.0, 0.0);
      }

      FeldRelativeCoords *relativeCoords = [FeldRelativeCoords new];
      relativeCoords.over = frameOnRelativeCoordinates.origin.x;
      relativeCoords.up = frameOnRelativeCoordinates.origin.y;
      relativeCoords.norm = 0.0;
      relativeCoords.feldId = feld.name;
      relativeCoords.valid = true;
      
      FeldRelativeSize *relativeSize = [FeldRelativeSize new];
      relativeSize.scale = scale;
      relativeSize.aspectRatio = aspectRatio;
      relativeSize.feldId = feld.name;
      
      if ([NSUserDefaults standardUserDefaults].windshieldContinuousDrag)
      {
        // TODO: To be refactored whenever windshieldContinuousDrag is available
        // transaction = [self.communicator requestWindshieldEndTransformItem:uid withOrigin:frameOnRelativeCoordinates.origin scale:scale aspectRatio:aspectRatio feld:feld.name surface:surface.name workspaceUid:self.workspace.uid];
      }
      else
      {
        transaction = [self.communicator.requestor requestWindshieldItemTransformRequest:self.workspace.uid uid:[item uid] surfaceName:surface.name feldRelativeCoords:relativeCoords feldRelativeSize:relativeSize];

        // Analytics
        MezzanineAnalyticsManager *analyticsManager = [MezzanineAnalyticsManager sharedInstance];
        [analyticsManager trackWindshieldItemTransformEvent:[self analyticsContentType:item]];
      }
      
      if (droppedViewBlock)
      {
        [self checkShielderRelinquish:item];

        transaction.successBlock = ^{
          droppedViewBlock.isBeingManipulated = NO;
        };

        CGRect frameForBlock = frameBeforeDrag;
        transaction.failedBlock = ^(NSError *error) {
          [UIView animateWithDuration:0.15 animations:^{
            droppedViewBlock.alpha = 1.0;
            droppedViewBlock.frame = frameForBlock;
          } completion:^(BOOL finished) {
            droppedViewBlock.isBeingManipulated = NO;
          }];
        };
      }
    }
    else // Source is somewhere else (e.g. LiveStreamsViewController or PortfolioViewController) therefore this is an insertion
    {
      MezzanineAnalyticsManager *analyticsManager = [MezzanineAnalyticsManager sharedInstance];

      NSString *uidString;
      WindshieldItemContentType analyticsContentType = WindshieldItemContentTypeUnknown;
      if ([ovum.dataObject isKindOfClass:[MZAsset class]])
      {
        uidString = [ovum.dataObject uid];
        analyticsContentType = [self analyticsContentType:[self.workspace.windshield itemWithUid:uidString]];
      }
      else if ([ovum.dataObject isKindOfClass:[MZLiveStream class]])
      {
        MZLiveStream *liveStream = ovum.dataObject;
        uidString = liveStream.uid;
        analyticsContentType = WindshieldItemContentTypeVideo;
      }
      else if ([ovum.dataObject isKindOfClass:[MZPortfolioItem class]])
      {
        MZPortfolioItem *slide = ovum.dataObject;
        uidString = slide.contentSource;
        analyticsContentType = slide.isVideo ? WindshieldItemContentTypeVideo : WindshieldItemContentTypeImage;
      }

      MZFeld *feld = [self closestFeldAtLocation:location];
      MZSurface *surface = [_systemModel surfaceWithFeld:feld];

      FeldRelativeCoords *relativeCoords = [FeldRelativeCoords new];
      relativeCoords.over = 0.0;
      relativeCoords.up = 0.0;
      relativeCoords.norm = 0.0;
      relativeCoords.feldId = feld.name;
      relativeCoords.valid = true;
      
      FeldRelativeSize *relativeSize = [FeldRelativeSize new];
      relativeSize.scale = 1.0;
      relativeSize.aspectRatio = 0.0;
      relativeSize.feldId = feld.name;
      
      transaction = [self.communicator.requestor requestWindshieldItemCreateRequest:self.workspace.uid contentSource:uidString surfaceName:surface.name feldRelativeCoords:relativeCoords feldRelativeSize:relativeSize];

      // Analytics
      [analyticsManager trackWindshieldItemCreateEvent:analyticsContentType];
    }
    
    // Add a new insertion indicator for this transaction
    __weak UIView *activeIndicator = [self showInsertionIndicatorForOvum:ovum];

    // Add an animation to indicate that the action is in progress.
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    animation.fromValue = @1.0;
    animation.toValue = @0.5;
    animation.duration = 2.0;
    animation.autoreverses = YES;
    animation.repeatCount = INFINITY;
    [activeIndicator.layer addAnimation:animation forKey:@"opacity"];
    
    [UIView animateWithDuration:0.15
                     animations:^{[droppedViewBlock setAlpha:0.0];}];
    
    [insertionIndicators addObject:activeIndicator];
    
    transaction.endedBlock = ^{
      
      // Some logic to make fade transition from indicator back to the shielder
      // smoother in latency conditions... actually when the indicator is seen.
      // And direct when latency is really small and the indicator animation has not started.
      CGFloat alpha = [(CALayer *) activeIndicator.layer.presentationLayer opacity];
      
      [insertionIndicators removeObject:activeIndicator];
      [activeIndicator.layer removeAllAnimations];
      [activeIndicator removeFromSuperview];
      ovumDragViewAsIndicator = NO;
      
      if (droppedViewBlock)
      {
        if (alpha == 0.0)
        {
          [droppedViewBlock setAlpha:1.0];
        }
        else
        {
          [droppedViewBlock setAlpha:alpha];
          [UIView animateWithDuration:0.15 animations:^{
            [droppedViewBlock setAlpha:1.0];
          } completion:^(BOOL finished) {
          }];
        }
      }

      droppedShielderWaitingForConfirmation = nil;
    };
  }
}


-(void) handleDropAnimationForOvum:(OBOvum*)ovum withDragView:(UIView*)dragView dragDropManager:(OBDragDropManager*)dragDropManager
{
  if (!ovumDragViewAsIndicator)
  {
    if ([ovum.dragView isKindOfClass:[VideoAssetView class]])
      [(VideoAssetView *)ovum.dragView setLiveStream:nil];
  }

  [dragDropManager animateOvumDrop:ovum withAnimation:^{
    // Do Nothing but let OBDragDropManager hide its overlayWindow
  } completion:nil];
}


// This method detects switches between a pan (1 finger) or a pinch gesture (2 fingers)
// And requests hiding or presenting the top deletion area.
-(void) updateDragDeletionForDraggedView:(UIView *)view
{
  // We look for the maxinum number of touches in all its active gestures (UIGestureRecognizerStateChanged)
  NSInteger numberOfTouches = 0;
  for (UIGestureRecognizer *recognizer in [view gestureRecognizers])
  {
    if (recognizer.state == UIGestureRecognizerStateChanged)
    {
      if (recognizer.numberOfTouches >= numberOfTouches)
        numberOfTouches = recognizer.numberOfTouches;
    }
  }

  // It relies in the current number of touches and the previous number to
  // avoid false positives when lifting two fingers at the end of a pinch gesture
  if (previousNumberOfTouchesWhileDragging == 1 && numberOfTouches == 1)
  {
    // We are in a continuous panning state so we present the deletion area
    if ([_delegate respondsToSelector:@selector(windshieldViewPresentDragDropDeletionAreaWithDeletion:)]) {
      [_delegate windshieldViewPresentDragDropDeletionAreaWithDeletion:YES];
    }
  }
  else if (numberOfTouches >= 2)
  {
    // We are in a continuous pinching state so we hide the deletion area
    if ([_delegate respondsToSelector:@selector(windshieldViewHideDragDropDeletionArea)]) {
      [_delegate windshieldViewHideDragDropDeletionArea];
    }
  }

  previousNumberOfTouchesWhileDragging = numberOfTouches;
}


-(CGFloat) scaleOfItemView:(AssetView *)assetView atLocation:(CGPoint)location
{
  MZFeld *feld = [self closestFeldAtLocation:location];
  MZSurface *surfaceAtLocation = [_systemModel surfaceWithFeld:feld];

  MZWindshieldItem *item = (MZWindshieldItem *) assetView.asset;
  CGFloat aspectRatio = [item aspectRatio];
  CGFloat scale = 0;

  if (aspectRatio > feld.aspectRatio)
  {
    scale = (assetView.frame.size.width) / [self surface:surfaceAtLocation feldWidth:feld];
  }
  else
  {
    scale = (assetView.frame.size.height) / [self surface:surfaceAtLocation feldHeight:feld];
  }
  return scale;
}


#pragma mark - OBOvumSource

-(OBOvum *) createOvumFromView:(UIView*)sourceView
{
  hasExitedFromWindshield = NO;
  AssetView *assetView;
  OBOvum *ovum = [[OBOvum alloc] init];
  
  if ([sourceView isKindOfClass:[ImageAssetView class]])
  {
    assetView = (ImageAssetView *)sourceView;
    ovum.dataObject = assetView.asset;
  }
  else if ([sourceView isKindOfClass:[VideoAssetView class]])
  {
    assetView = (VideoAssetView *)sourceView;
    ovum.dataObject = assetView.userInfo; // Using userinfo instead of qmviddle because certain operations need the windshield uid
  }
  else
    return nil;
  
  [self setDraggedView:assetView];
  [self.view bringSubviewToFront:draggedView];
  frameBeforeDrag = draggedView.frame;
  ovum.isCentered = NO;
  ovum.shouldScale = YES;
  return ovum;
  
}


-(UIView *) createDragRepresentationOfSourceView:(UIView *)sourceView inWindow:(UIWindow*)window
{
  if ([sourceView isKindOfClass:[ImageAssetView class]])
  {    
    ImageAssetView *assetView = (ImageAssetView *)sourceView;

    CGRect frameInWindow = [window convertRect:assetView.frame fromView:self.view];

    ImageAssetView *dragImage = [[ImageAssetView alloc] initWithFrame:frameInWindow];
    dragImage.contentView.backgroundColor = [MezzanineStyleSheet sharedStyleSheet].ovumBackgroundColor;
    dragImage.imageView.backgroundColor = [UIColor clearColor];
    dragImage.asset = assetView.asset;
    dragImage.userInteractionEnabled = NO;

    if ([[(MZWindshieldItem *)dragImage.asset assetUid] hasPrefix:@"web-"])
    {
      dragImage.contentView.backgroundColor = [[MezzanineStyleSheet sharedStyleSheet] grey110Color];
    }

    return dragImage;
  }
  else if ([sourceView isKindOfClass:[VideoAssetView class]])
  {    
    VideoAssetView *assetView = (VideoAssetView *)sourceView;

    CGRect frameInWindow = [window convertRect:assetView.frame fromView:self.view];

    VideoAssetView *dragImage = [[VideoAssetView alloc] initWithFrame:frameInWindow];
    dragImage.contentView.backgroundColor = [UIColor clearColor];
    dragImage.asset = assetView.asset;
    dragImage.liveStream = assetView.liveStream;
    dragImage.imageView.backgroundColor = assetView.imageView.backgroundColor;
    dragImage.imageView.image = assetView.imageView.image;
    dragImage.placeholderView.sourceName = assetView.placeholderView.sourceName;
    dragImage.placeholderView.sourceOrigin = assetView.placeholderView.sourceOrigin;
    dragImage.placeholderView.isLocalVideo = assetView.placeholderView.isLocalVideo;
    dragImage.contentMode = UIViewContentModeScaleAspectFill;
    dragImage.userInteractionEnabled = NO;
    return dragImage;
  }
  else
    return nil;
}


-(void) dragViewWillAppear:(UIView *)dragView inWindow:(UIWindow*)window atLocation:(CGPoint)location
{
  dragView.alpha = 1.0;
  
  if ([dragView isKindOfClass:[ImageAssetView class]])
    [(ImageAssetView *)dragView imageView].contentMode = UIViewContentModeScaleAspectFill;
  else
    [(VideoAssetView *)dragView imageView].contentMode = UIViewContentModeScaleAspectFill;
  
  [UIView animateWithDuration:0.4 animations:^{
    dragView.center = location;
  }];
}


-(void) ovumDragWillBegin:(OBOvum*)ovum
{
  DLog(@"WindshieldViewController ovumDragWillBegin: %@", ovum);
  
  if ([ovum.dataObject isKindOfClass:[MZWindshieldItem class]])
  {
    MZWindshieldItem *item = (MZWindshieldItem*)ovum.dataObject;
    
    [self checkShielderGrab:item];
    [self.communicator.requestor requestWindshieldItemGrabRequest:self.workspace.uid uid:item.uid];
  }
}


-(void) ovumDragEnded:(OBOvum*)ovum
{
  DLog(@"WindshieldViewController ovumDragEnded: %@", ovum);

  // Need to convert to view coordinates instead of internal windshield coordinates
  CGPoint frameCenter = CGPointMake(CGRectGetMidX(frameBeforeDrag), CGRectGetMidY(frameBeforeDrag));
  CGPoint centerBeforeDragInScrollViewCoordinates = [_workspaceViewController.contentScrollView convertPoint:frameCenter fromView:self.view];

  if (ovum.dropAction == OBDropActionNone) // When dropping during Spaces mode or outside both self.view and a corkboard 
  {
    if ([ovum.dataObject isKindOfClass:[MZWindshieldItem class]])
    {
      MZWindshieldItem *item = (MZWindshieldItem*)ovum.dataObject;
      
      [self.communicator.requestor requestWindshieldItemRelinquishRequest:self.workspace.uid uid:item.uid];
      [self checkShielderRelinquish:item];
    }
    
    draggedView.frame = frameBeforeDrag;
    [UIView animateWithDuration:0.15 animations:^{
      ovum.dragView.alpha = 0.0;
      [self resetDragView];
    } completion:^(BOOL completition){
      if ([_delegate respondsToSelector:@selector(windshieldViewUpdateSpaceIndexWithLocation:)]) {
        [_delegate windshieldViewUpdateSpaceIndexWithLocation:centerBeforeDragInScrollViewCoordinates];
      }
    }];
  }
  else if (ovum.dropAction == OBDropActionDelete) // Caused by drag and drop delete
  {
    // Don't make draggedView nil here since with the transaction failed case we need to reset the frame
    if ([_delegate respondsToSelector:@selector(windshieldViewUpdateSpaceIndexWithLocation:)]) {
      [_delegate windshieldViewUpdateSpaceIndexWithLocation:centerBeforeDragInScrollViewCoordinates];
    }
    
    [self checkShielderRelinquish:ovum.dataObject];
  }
  else if (ovum.currentDropHandlingView != self.view)// Used when dropping into corkboards
  {
    [UIView animateWithDuration:0.15 animations:^{
      [self resetDragView];
    }];
    
    [self checkShielderRelinquish:ovum.dataObject];
  }
  else if (ovum.dropAction == OBDropActionCopy && ovum.dragView)// Used when gesture has been cancelled
  {
    ovum.dragView.alpha = 0.0;
    
    if ([_delegate respondsToSelector:@selector(windshieldViewUpdateSpaceIndexWithLocation:)]) {
      [_delegate windshieldViewUpdateSpaceIndexWithLocation:centerBeforeDragInScrollViewCoordinates];
    }
    
    [self resetDragView];
  }
}


-(BOOL) shouldCreateOvumFromView:(UIView *)sourceView
{
  // This avoids pinch and pan gestures to be valid at once. Only the first one detected.
  if (sourceView == draggedView)
  {
    return NO;
  }

  return YES;
}


#pragma mark - Observation

-(BOOL) isItemInThisWindshield:(MZWindshieldItem *)item
{
  return (([_systemModel.surfacesInPrimaryWindshield containsObject:item.surface] && [self isKindOfClass:[PrimaryWindshieldViewController class]]) ||
          ([_systemModel.surfacesInExtendedWindshield containsObject:item.surface] && [self isKindOfClass:[ExtendedWindshieldViewController class]]) ||
          [self isKindOfClass:[FullWindshieldViewController class]]);
}

-(void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
  if ([object isKindOfClass:[MZWindshield class]])
  {
    if ([keyPath isEqual:@"items"])
    {
      NSNumber *kind = change[NSKeyValueChangeKindKey];
      if ([kind integerValue] == NSKeyValueChangeInsertion)  // items were added
      {
        NSArray *inserted = change[NSKeyValueChangeNewKey];
        DLog(@"Windshield items were inserted: %@", inserted);
        
        NSUInteger count = [inserted count];

        for (int i=0; i<count; i++)
        {
          MZWindshieldItem *item = inserted[i];
          if ([self isItemInThisWindshield:item])
            [self insertViewForItem:item];

          [item addObserver:self forKeyPath:@"frame" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld | NSKeyValueObservingOptionInitial) context:nil];
        }
      }
      else if ([kind integerValue] == NSKeyValueChangeRemoval)  // items were removed
      {
        NSArray *removed = change[NSKeyValueChangeOldKey];
        DLog(@"Windshield items were deleted: %@", removed);
        
        for (MZWindshieldItem *item in removed)
        {
          if ([self isItemInThisWindshield:item])
            [self removeViewForItem:item animated:YES];
          [item removeObserver:self forKeyPath:@"frame"];
        }
      }
    }
  }
  
  if ([object isKindOfClass:[MZWindshieldItem class]])
  {
    MZWindshieldItem *item = (MZWindshieldItem*)object;

    // Add or remove items that were placed in another surface (Primary vs all Extended)
    if (![self isItemInThisWindshield:item])
    {
      if ([self viewForItem:item])
        [self removeViewForItem:item animated:YES];

      return;
    }
    if (![self viewForItem:item])
      [self insertViewForItem:item];

    
    if ([keyPath isEqual:@"frame"])
    {
      CGRect newFrame = CGRectNull;
      
      AssetView *assetView = (AssetView*)[self viewForItem:item];
      newFrame = [self rectFromNativeWindshieldCoordinatesForItem:item];

      if (draggedView != assetView)
      {
        [self.view bringSubviewToFront:assetView];
        [UIView animateWithDuration:0.15 animations:^(){
          [assetView setFrame:newFrame];
          [self updateCropViewForItem:item];
        } completion:^(BOOL finished) {
          assetView.isBeingManipulated = NO;  // Forces the video asset to refresh its placeholder view. See bug 9546
        }];
      }
      else
      {
        frameBeforeDrag = newFrame;
      }
    }
  }
}


#pragma mark - Asset Viewing

- (void)openAssetViewerForAssetView:(AssetView *)assetView
{
  if (!assetView)
    return;

  AssetViewController *controller = [[AssetViewController alloc] initWithNibName:nil bundle:nil];
  controller.appContext = [MezzanineAppContext currentContext];
  controller.workspace = self.workspace;
  
  if ([assetView isKindOfClass:[ImageAssetView class]])
  {
    controller.placeholderImage = ((ImageAssetView *)assetView).imageView.image;
    controller.item = assetView.asset;
  }
  else
  {
    controller.placeholderImage = ((VideoAssetView *)assetView).liveStream.thumbnail;
    controller.item = ((VideoAssetView *)assetView).liveStream;
  }
  
  [[MezzanineAppContext currentContext].mainNavController pushViewController:controller transitionView:assetView];
}


#pragma mark - UIGestureRecognizer Actions

- (void)windshieldItemTapped:(UIGestureRecognizer*)recognizer
{
  AssetView *assetView = (AssetView*)recognizer.view;
  ButtonMenuViewController *menuViewController = [[ButtonMenuViewController alloc] initWithNibName:nil bundle:nil];
  
  NSMutableArray *menuItems = [NSMutableArray array];

  if ([assetView isKindOfClass:[VideoAssetView class]])
  {
    VideoAssetView *videoAssetView = (VideoAssetView*)assetView;
    [menuItems addObject:[ButtonMenuHeaderTextItem itemWithText:videoAssetView.liveStream.displayName title:nil action:nil]];
  }

  [menuItems addObject:[ButtonMenuItem itemWithTitle:NSLocalizedString(@"Portfolio Item Menu View", nil) action:^{
    [self openAssetViewerForAssetView:assetView];
    [self.currentPopover dismissPopoverAnimated:NO];
  }]];

	ButtonMenuItem *deleteItem = [ButtonMenuItem itemWithTitle:NSLocalizedString(@"Portfolio Item Menu Delete", nil) action:^{
		[self requestDeleteItem:assetView.asset];
		
		[self.currentPopover dismissPopoverAnimated:NO];
	}];
	deleteItem.buttonColor = [MezzanineStyleSheet sharedStyleSheet].redHighlightColor;
	[menuItems addObject:deleteItem];
  menuViewController.menuItems = menuItems;

  // N.B. menuViewController needs its menu items before having a content height
  FPPopoverController *popover = [[FPPopoverController alloc] initWithContentViewController:menuViewController delegate:self];
  popover.context = assetView.asset;
  [popover stylizeAsActionSheet];

  // The following can be simplified when we have an improved FPPopoverController API (bug 11392)
  CGPoint locationInView = [recognizer locationInView:assetView];
  CGPoint locationInWindowTopView = [self.view.window.subviews[0] convertPoint:locationInView fromView:assetView];
  popover.arrowDirection = (FPPopoverArrowDirectionVertical);
  [popover presentPopoverFromPoint:locationInWindowTopView];
  self.currentPopover = popover;
}


#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
  if (otherGestureRecognizer == _workspaceViewController.contentScrollView.panGestureRecognizer)
    return YES;
  return NO;
}

#pragma mark - Indicator View

-(BOOL) scaleInFullscreenMargins:(CGFloat)scale
{
  static CGFloat epsilon = 0.1;
  return (scale > 1.0-epsilon && scale < 1.0+epsilon);
}

-(CGRect) placeholderRectForObject:(id)object inOverlayWindowWithLocation:(CGPoint)location
{
  MZSurface *mainSurface = [_systemModel surfaceWithName:@"main"];
  MZFeld *feld = [self closestFeldAtLocation:location];

  if ([_systemModel surfaceWithFeld:feld] != mainSurface)
    return CGRectZero;

  CGFloat width = 0;
  CGFloat height = 0;
  CGFloat x = [self surface:mainSurface feldXOffset:feld];
  CGFloat y = 0;
  CGRect frameOnWindshield;
  if ([object isKindOfClass:[MZWindshieldItem class]])
  {
    MZWindshieldItem *item = draggedView.asset;
    if (item.aspectRatio > feld.aspectRatio)
    {
      width = [self windshieldFeldWidth];
      height = width / item.aspectRatio;
      y = ([self windshieldHeight] - height) / 2.0;
    }
    else
    {
      height = [self windshieldHeight];
      width = height * item.aspectRatio;
      x += ([self windshieldFeldWidth] - width) / 2.0;
    }

    frameOnWindshield = CGRectMake(x, y, width, height);
  }
  else
  {
    frameOnWindshield = CGRectMake(x, 0, [self windshieldFeldWidth], [self windshieldHeight]);
  }

  frameOnWindshield.origin = [self shiftPointFromWindshieldViewToShielderContainer:frameOnWindshield.origin];

  return [[OBDragDropManager sharedManager].overlayWindow convertRect:frameOnWindshield fromView:self.view];
}


-(void) createIndicatorForOvum:(OBOvum *)ovum atLocation:(CGPoint)location
{
  // This avoid animating the placeholder from its old location
  if (placeholderView)
    [self removeIndicator];

  CGRect frame = [self placeholderRectForObject:ovum.dataObject inOverlayWindowWithLocation:location];
  placeholderView = [[WindshieldPlaceholderView alloc] initWithFrame:frame];
  placeholderView.ovum = ovum;
  placeholderView.alpha = 0.0;
  [placeholderView setUserInteractionEnabled:NO];

  [[OBDragDropManager sharedManager].overlayWindow addSubview:placeholderView];
  placeholderView.layer.zPosition = 100;
}


-(void) removeIndicator
{
  [placeholderView removeFromSuperview];
  placeholderView.ovum = nil;
  placeholderView = nil;
}


-(void) showPlaceholderView:(BOOL)show animated:(BOOL)animated
{
  if (animated)
  {
    [UIView animateWithDuration:0.15 animations:^{
      placeholderView.alpha = show ? 1.0:0.0;
    }];
  }
  else
  {
    placeholderView.alpha = show ? 1.0:0.0;
  }
}


-(void) updatePlaceholderViewForObject:(id)object withLocation:(CGPoint)location andScale:(CGFloat)scale
{
  if ([self isLocationInCorkboardSpace:location])
  {
    [self showPlaceholderView:NO animated:NO];
  }
  else
  {
    CGRect frameOnOvumSpace = [self placeholderRectForObject:(id)object inOverlayWindowWithLocation:location];
    CGFloat duration = CGRectEqualToRect(placeholderView.frame, CGRectZero) ? 0.0 : 0.15;

    [UIView animateWithDuration:duration animations:^{
      [placeholderView setFrame:frameOnOvumSpace];
      [placeholderView setNeedsDisplay];
    } completion:^(BOOL finished) {
      BOOL shouldShow = (draggedView && ![self scaleInFullscreenMargins:scale]) ? NO : YES;
      [self showPlaceholderView:shouldShow animated:YES];
    }];
  }
}


#pragma mark -
#pragma mark Edge Scrolling Updates

-(void) updateOnEdgeScrolling
{
  // If a shielder is being dragged we need to update the location of both its view
  // and, depending on its size, its placeholder while autoscroll is active
  if (draggedView)
  {
    // This is a hack to access to the location of the ovum's dragView of the current draggedView.
    // Before this hack, a delta value (speed of the edge scrolling) was used but it introduced errors that decoupled the locations of both views.
    for (UIGestureRecognizer *recognizer in draggedView.gestureRecognizers)
    {
      if ([recognizer respondsToSelector:@selector(ovum)])
      {
        OBOvum *ovum = recognizer.ovum;
        if (!ovum)
          continue;

        // First update dragged view location
        CGRect frame = [[OBDragDropManager sharedManager].overlayWindow convertRect:ovum.dragView.frame toView:self.view];
        [draggedView setFrame:frame];

        // Then calculate its placeholder new location
        CGPoint location = [recognizer locationInView:self.view];
        CGFloat scale = [self scaleOfItemView:draggedView atLocation:location];
        [self updatePlaceholderViewForObject:ovum.dataObject withLocation:location andScale:scale];
      }
    }
  }
  // Otherwise update just the placeholder if it exists because
  // a portfolio/live stream item is being added to the windshield
  else if (placeholderView)
  {
    CGPoint point = [[OBDragDropManager sharedManager].overlayWindow convertPoint:placeholderView.ovum.dragView.center toView:self.view];
    [self updatePlaceholderViewForObject:placeholderView.ovum.dataObject withLocation:point andScale:1.0];
  }
}

@end

