//
//  BinCollectionViewFlowLayout.swift
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 15/02/2017.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import UIKit

class SeparatorReusableView: UICollectionReusableView {
  override init(frame: CGRect) {
    super.init(frame: frame)
    self.backgroundColor = MezzanineStyleSheet.shared().grey90Color
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}


class BinCollectionViewFlowLayout: UICollectionViewFlowLayout {
  
  fileprivate let separatorDecoratorIdentifier = "separatorDecorator"
  
  override init() {
    super.init()
    self.register(SeparatorReusableView.self, forDecorationViewOfKind: separatorDecoratorIdentifier)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
    var layoutAttributes = super.layoutAttributesForElements(in: rect)
    
    // Separator
    if collectionView?.numberOfSections == 2 {
      let indexPath = IndexPath(row: 0, section: 1)
      if let decorationAttributes = self.layoutAttributesForDecorationView(ofKind: separatorDecoratorIdentifier, at: indexPath) {
        if rect.contains(decorationAttributes.frame) {
          layoutAttributes?.append(decorationAttributes)
        }
      }
    }
    
    return layoutAttributes
  }
  
  override func layoutAttributesForDecorationView(ofKind elementKind: String, at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
    if elementKind == separatorDecoratorIdentifier {
      let attributes = UICollectionViewLayoutAttributes(forDecorationViewOfKind: separatorDecoratorIdentifier, with: indexPath)
      let cellFrame = self.collectionView?.layoutAttributesForItem(at: indexPath)
      
      let width = CGFloat(1.0)
      let originX = (cellFrame?.frame)!.minX - sectionInset.left - (width - width / 2)
      let originY = (cellFrame?.frame)!.minY
      let height = ((self.collectionView?.frame)!.height) - originY
      attributes.frame = CGRect(x: originX, y: originY, width: width, height: height)
      
      return attributes
    }
    
    return nil
  }
  
  override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
    return true
  }
}
