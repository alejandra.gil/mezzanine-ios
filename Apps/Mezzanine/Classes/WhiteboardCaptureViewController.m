//
//  WhiteboardCaptureViewController.m
//  Mezzanine
//
//  Created by Zai Chang on 12/3/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import "WhiteboardCaptureViewController.h"
#import "MezzanineStyleSheet.h"


@interface WhiteboardCaptureViewController ()

@end


@implementation WhiteboardCaptureViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

  BOOL useAutoLayout = NO;
  MezzanineStyleSheet *styleSheet = [MezzanineStyleSheet sharedStyleSheet];
  
  CGFloat buttonHeight = 44.0;
  self.navigationItem.title = NSLocalizedString(@"Whiteboard Capture View Title", nil);
  
  _tableView = [[UITableView alloc] initWithFrame:CGRectZero];
  CGRect tableViewFrame = self.view.bounds;
  tableViewFrame.size.height -= buttonHeight;
  _tableView.frame = tableViewFrame;
  _tableView.backgroundColor = styleSheet.darkestFillColor;
  _tableView.separatorColor = styleSheet.mediumFillColor;
  _tableView.separatorInset = UIEdgeInsetsZero;
  [self.view addSubview:_tableView];

  _dataSource = [[WhiteboardCaptureViewDataSource alloc] init];
  [_dataSource setupTableView:_tableView];
  _dataSource.systemModel = _systemModel;
  _tableView.dataSource = _dataSource;
  _tableView.delegate = self;

  CGRect buttonFrame = CGRectMake(0, self.view.frame.size.height - buttonHeight, self.view.frame.size.width, buttonHeight);
  _captureAllButton = [TTButton buttonWithStyle:@"darkTopBorderedButton:" title:NSLocalizedString(@"Whiteboard Capture All Button Title", nil)];
  _captureAllButton.frame = buttonFrame;
  [_captureAllButton addTarget:self action:@selector(captureAll:) forControlEvents:UIControlEventTouchUpInside];
  [self.view addSubview:_captureAllButton];

  if (useAutoLayout)
  {
    _tableView.translatesAutoresizingMaskIntoConstraints = NO;
    _captureAllButton.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_tableView]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:NSDictionaryOfVariableBindings(_tableView)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_tableView][_captureAllButton(44)]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:NSDictionaryOfVariableBindings(_tableView, _captureAllButton)]];
  }
  else
  {
    _tableView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    _captureAllButton.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin);
  }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)setSystemModel:(MZSystemModel *)systemModel
{
  if (_systemModel != systemModel)
  {
    _systemModel = systemModel;
    
    _dataSource.systemModel = systemModel;
    [_tableView reloadData];
  }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  NSDictionary *whiteboard = [_systemModel.whiteboards objectAtIndex:indexPath.row];
  NSString *whiteboardUid = whiteboard[@"uid"];
  [_interactor requestWhiteboardCapture:whiteboardUid];
  
  if (_didRequestWhiteboardCapture)
    _didRequestWhiteboardCapture(whiteboardUid);
}


- (IBAction)captureAll:(id)sender
{
  [_interactor requestWhiteboardCaptureAll];
  
  if (_didRequestWhiteboardCapture)
    _didRequestWhiteboardCapture(nil);
}

@end



@implementation WhiteboardCaptureViewDataSource

static NSString *cellReuseIdentifier = @"cell";

- (void)setupTableView:(UITableView *)tableView
{
  [tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:cellReuseIdentifier];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  return _systemModel.whiteboards.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellReuseIdentifier];
  cell.backgroundColor = [UIColor clearColor];

  MezzanineStyleSheet *styleSheet = [MezzanineStyleSheet sharedStyleSheet];
  cell.textLabel.font = [UIFont dinMediumOfSize:16.0];
  cell.textLabel.textColor = styleSheet.textNormalColor;
  cell.textLabel.text = [_systemModel.whiteboards objectAtIndex:indexPath.row][@"name"];
  
  UIView *selectedBackgroundView = [[UIView alloc] initWithFrame:CGRectZero];
  selectedBackgroundView.backgroundColor = RGBCOLOR(69, 69, 79);
  cell.selectedBackgroundView = selectedBackgroundView;

  return cell;
}

@end
