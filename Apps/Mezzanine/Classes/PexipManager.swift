//
//  PexipManager.swift
//  Mezzanine
//
//  Created by miguel on 13/6/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

import UIKit
import AVFoundation

@objc public enum PexipConferenceMode: NSInteger {
  case audioVideo
  case audioOnly
}

@objc public enum PexipConferenceState: NSInteger {
  case disconnected
  case connecting
  case connected
}

class PexipManager: NSObject {

/*  private static var __once: () = {
      // Pexip uses the built-in receiver by default instead of the speaker as we want - Bug 16822
      self.updateAudioOutputIfNeeded(self.audioOutputs())
      NotificationCenter.default.addObserver(self, selector: #selector(PexipManager.audioRouteChangeListenerCallback(_:)), name: NSNotification.Name.AVAudioSessionRouteChange, object: nil)

      // Observer for application lifecycle to reconnect - Bug 16785
      PexipManager.appForegroundNotification = NotificationCenter.default.addObserver(forName: NSNotification.Name.UIApplicationWillEnterForeground, object: nil, queue: OperationQueue()) {
        [unowned self] notification in

        // The reconnection needed for Bug 16785 is only for the video feed. Ignore it if we are on an Audio Only conference
        if self.currentConference() == PexipManager.conferenceAudioVideo {
          self.reconnectMedia()
        }
      }
    }()*/

  @objc var infopresence: MZInfopresence?
  @objc dynamic var conferenceMode: PexipConferenceMode = .audioVideo
  @objc dynamic var conferenceState: PexipConferenceState = .disconnected

  fileprivate let conferenceAudioVideo: Conference = Conference()
  fileprivate let conferenceAudioOnly: Conference = Conference()
  var isEscalated: Bool = false
  fileprivate var initialVideoMutedState: Bool = false
  fileprivate var initialAudioMutedState: Bool = false
  fileprivate var appForegroundNotification: NSObjectProtocol?
  fileprivate var shouldCancelJoining: Bool = false
  fileprivate var displayName: String = "Mobile Participant"

  // track our own states since Pexip seems to reset some of then when switching conferences...
  var currentVideoMutedState: Bool = false {
    didSet {
      conferenceAudioOnly.videoMute = currentVideoMutedState
      conferenceAudioVideo.videoMute = currentVideoMutedState
    }
  }

  var currentAudioMutedState: Bool = false {
    didSet {
      conferenceAudioOnly.audioMute = currentAudioMutedState
      conferenceAudioVideo.audioMute = currentAudioMutedState
    }
  }
  
  struct Static {
    static var dispatchOnceAddObserversToken: Int = 0
  }

  func setupVideoViews(_ videoView: PexVideoView, selfVideoView: PexVideoView) {
    self.conferenceAudioVideo.videoView = isCameraAvailable() ? videoView : nil
    self.conferenceAudioVideo.selfVideoView = isCameraAvailable() ? selfVideoView : nil
  }

  func areVideoViewsConfigured() -> Bool {
    return self.conferenceAudioVideo.videoView != nil && self.conferenceAudioVideo.selfVideoView != nil
  }

  func createConferenceURI() -> ConferenceURI? {

    guard let pexipNode = infopresence?.pexipNode else { return nil }

    let conferenceString = (infopresence?.pexipConference)! as String

    return ConferenceURI(conference: conferenceString, hostname: pexipNode)
  }


  func configureSystemObserversIfNeeded() {
//    _ = PexipManager.__once
  }

  func removeSystemObserversIfNeeded() {
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVAudioSessionRouteChange, object: nil)
    if self.appForegroundNotification != nil {
      NotificationCenter.default.removeObserver(self.appForegroundNotification!)
    }
  }

  // MARK: Pexip Connection
  
  func isCameraAvailable() -> Bool {
    let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
    switch cameraAuthorizationStatus {
    case .denied, .restricted:
      return false
      
    case .authorized:
      return true
      
    default:
      return false
    }
  }


  func join(_ conferenceMode: PexipConferenceMode, displayName: String = "Mobile Participant", completion: @escaping (_ result: String) -> Void) {

    if conferenceState == .connecting && conferenceMode == self.conferenceMode {
      return
    }

    shouldCancelJoining = false
    self.displayName = displayName

    guard let _ = createConferenceURI() else { return }

    let isSwitchingConferenceMode = (self.conferenceMode != conferenceMode && self.conferenceState == .connected)
    self.conferenceMode = conferenceMode
    conferenceState = .connecting

    let conference = currentConference()
    conference.useOnlyConferenceIdForRequests = true

    if !isSwitchingConferenceMode && conference.token != nil && conference.token != "" {
      conference.disconnectMedia(completion: { status in
        switch (status) {
        case .ok:
          if self.shouldCancelJoining {
            self.leave()
            completion("Pexip connection interrupted")
            return
          }
          conference.releaseToken(completion: {status in
            switch (status) {
            case .ok:
              self.isEscalated = false
              self.startConnection(conference, isSwitching: isSwitchingConferenceMode, completion: completion)
            default:
              self.conferenceState = .disconnected
              completion("Pexip could not release previous token: \(status)")
            }
          })
        default:
          self.conferenceState = .disconnected
          completion("Pexip could not disconnect previous media: \(status)")
        }
      })
    }
    else {
      startConnection(conference, isSwitching: isSwitchingConferenceMode, completion: completion)
    }
  }

  func startConnection(_ conference: Conference, isSwitching: Bool, completion: @escaping (_ result: String) -> Void) {
    guard let conferenceURI = createConferenceURI() else { return }

    if shouldCancelJoining {
      leave()
      completion("Pexip connection interrupted")
      return
    }

    conference.connect(self.displayName, URI: conferenceURI) { [unowned self] (ok) in
      if !ok {
        self.conferenceState = .disconnected
        completion("Connecting to Pexip server failed")
        return
      }

      self.continueWithConnectionAfterRequestingToken(conference, isSwitching: isSwitching, completion: completion)
    }
  }

  func continueWithConnectionAfterRequestingToken(_ conference: Conference, isSwitching: Bool, completion: @escaping (_ result: String) -> Void) {

    if self.shouldCancelJoining {
      leave()
      completion("Pexip connection interrupted")
      return
    }

    // Setting it in here first to avoid showing self video or sending audio/video for a few seconds when muted
    updateMuteStates(isSwitching)

    DispatchQueue.main.async {
      if self.conferenceMode == .audioVideo {
        // Aspect ratio of this view can be wrong until front camera is turned on, keep it hidden while connecting
        conference.selfVideoView?.alpha = 0.0
      }
    }

    conference.escalateMedia(video: true, resolution: Resolution.p576) { [unowned self] (status) in
      switch (status) {
      case .ok:
        self.completeConnectionAfterEscalating(conference, isSwitching: isSwitching, completion: completion)
      case .alreadyActive:
        self.conferenceState = .connected
        completion("AlreadyActive, Pexip escalateMedia is not needed.")
      default:
        self.conferenceState = .disconnected
        completion("Pexip escalateMedia failed: \(status)")
      }

    }
  }

  func completeConnectionAfterEscalating(_ conference: Conference, isSwitching: Bool, completion: @escaping (_ result: String) -> Void) {

    if shouldCancelJoining {
      leave()
      completion("Pexip connection interrupted")
      return
    }

    isEscalated = true

    configureSystemObserversIfNeeded()

    // Setting values again because escalateMedia disables audio or video mute states
    updateMuteStates(isSwitching)

    if isSwitching {
      let oldConference = previousConference()
      oldConference.disconnectMedia(completion: { status in
        switch (status) {
        case .ok:
          oldConference.releaseToken(completion: { status in
            switch (status) {
            case .ok:
              self.conferenceState = .connected
              completion("Pexip connection complete")
            default:
              self.conferenceState = .disconnected
              completion("Previous pexip conference releaseToken failed: \(status)")
            }
          })
        default:
          self.conferenceState = .disconnected
          completion("Previous pexip conference disconnectMedia failed: \(status)")
        }
      })
    }
    else {
      self.conferenceState = .connected
      completion("Pexip connection complete")
    }
  }

  func leave() {
    shouldCancelJoining = true
    Static.dispatchOnceAddObserversToken = 0
    leaveConference(conferenceAudioOnly)
    leaveConference(conferenceAudioVideo)
  }

  fileprivate func leaveConference(_ conference: Conference) {
    conference.disconnectMedia(completion: { [unowned self] status in
      switch (status) {
      case .ok:
        print("disconnectMedia worked fine")
      default:
        print("disconnectMedia: Got unhandled status of \(status)")
      }
      conference.releaseToken(completion: { [unowned self] status in
        switch (status) {
        case .ok:
          print("Token has been released")
        default:
          print("Token couldn't be released: Got unhandled status of \(status)")
        }
        conference.videoView = nil
        conference.selfVideoView = nil
        self.isEscalated = false
        self.removeSystemObserversIfNeeded()
        self.conferenceState = .disconnected
      })
    })
  }
  
  func reconnectMedia() {

    self.conferenceState = .connecting
    let conference = currentConference()
    let tmpAudioMutedState = currentAudioMutedState
    let tmpVideoMutedState = isCameraAvailable() ? currentVideoMutedState : true
    conference.disconnectMedia(completion: { status in
      // Somehow escalateMedia freezes the UI...
      let priority = DispatchQueue.GlobalQueuePriority.default
      DispatchQueue.global(priority: priority).async {
        conference.escalateMedia(video: true, resolution: Resolution.p576) { status in
          DispatchQueue.main.async {
            // Setting values again because escalateMedia disables audio or video mute states
            self.currentVideoMutedState = tmpVideoMutedState
            self.currentAudioMutedState = tmpAudioMutedState
            self.conferenceState = .connected
          }
        }
      }
    })
  }

  func updateMuteStates(_ isSwitching: Bool) {

    // Setting values again because escalateMedia disables audio or video mute states
    currentAudioMutedState = isSwitching ? currentAudioMutedState : initialAudioMutedState
    if conferenceMode == .audioVideo {
      currentVideoMutedState = isCameraAvailable() ? (isSwitching ? currentVideoMutedState : initialVideoMutedState) : true
    }
  }

  func currentConference() -> Conference {
    if conferenceMode == .audioOnly {
      return conferenceAudioOnly
    }
    else {
      return conferenceAudioVideo
    }
  }

  func previousConference() -> Conference {
    if conferenceMode == .audioOnly {
      return conferenceAudioVideo
    }
    else {
      return conferenceAudioOnly
    }
  }

  // MARK: Audio/Video state control

  func audioToggle() {
    if self.isEscalated {
      currentAudioMutedState = !currentAudioMutedState
    } else {
      initialAudioMutedState = !initialAudioMutedState
    }
  }

  func videoToggle() {
    if conferenceMode == .audioOnly {
      return
    }

    if self.isEscalated {
      currentVideoMutedState = isCameraAvailable() ? !currentVideoMutedState : true
      conferenceAudioVideo.selfVideoView?.alpha = currentVideoMutedState == false ? 1.0 : 0.0
    } else {
      initialVideoMutedState = isCameraAvailable() ? !initialVideoMutedState : true
    }
  }

  @objc func setAudioMuted(_ muted: Bool) {
    if self.isEscalated {
      currentAudioMutedState = muted
    } else {
      initialAudioMutedState = muted
    }
  }

  @objc func setVideoMuted(_ muted: Bool) {
    if conferenceMode == .audioOnly {
      return
    }

    if self.isEscalated {
      currentVideoMutedState = isCameraAvailable() ? muted : true
    } else {
      initialVideoMutedState = isCameraAvailable() ? muted : true
    }
  }

  @objc func isAudioMuted() -> Bool {
    if isEscalated {
      return currentAudioMutedState
    }
    else {
      return initialAudioMutedState
    }
  }

  @objc func isVideoMuted() -> Bool {
    if conferenceMode == .audioOnly {
      return true
    }

    if isEscalated {
      return currentVideoMutedState
    }
    else {
      return initialVideoMutedState
    }
  }

  @objc func cameraFlip() {
    if conferenceMode == .audioOnly {
      return
    }
    conferenceAudioVideo.flipCamera()
  }

  // MARK: Audio Output helper methods

  // Pexip uses the built-in receiver by default instead of the speaker when no audio output is available
  // The following methods permit forcing the use of the speaker by default in that case instead of the 
  // built-in audio receiver (phone calls default audio output) but still allows the user to select it
  // from the audio menu in Control Center

  // Checks when a new audio output device is connected and disconnected and sets the correct audio output
  @objc dynamic func audioRouteChangeListenerCallback(_ notification: Notification) {
    let routeChangeReason = notification.userInfo![AVAudioSessionRouteChangeReasonKey] as! UInt
    switch routeChangeReason {
    case AVAudioSessionRouteChangeReason.newDeviceAvailable.rawValue:
      self.currentConference().audioOutput = .defaultDevice
    case AVAudioSessionRouteChangeReason.oldDeviceUnavailable.rawValue:
      self.currentConference().audioOutput = .speaker
    default:
      break
    }
  }

  func audioOutputs() -> [AVAudioSessionPortDescription] {
    let currentRoute = AVAudioSession.sharedInstance().currentRoute
    return currentRoute.outputs
  }

  func updateAudioOutputIfNeeded(_ currentRouteOutputs: [AVAudioSessionPortDescription]) {
    if currentRouteOutputs.count != 0 {
      for description in currentRouteOutputs {
        if description.portType == AVAudioSessionPortBuiltInSpeaker ||
          description.portType == AVAudioSessionPortBuiltInReceiver {
          // Used when joining without a headphone plugged in
          self.currentConference().audioOutput = .speaker
        } else {
          // Used when joining with a headphone or another audio device plugged in
          self.currentConference().audioOutput = .defaultDevice
        }
      }
    }
  }

}
