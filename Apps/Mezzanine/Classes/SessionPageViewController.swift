//
//  SessionPageViewController.swift
//  Mezzanine
//
//  Created by miguel on 13/10/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

import UIKit

enum SessionPageSwipeDirection {
  case left
  case right
}

class SessionPageViewController: UIViewController {

  @IBOutlet var titleLabel: UILabel!
  @IBOutlet var subtitleLabel: UILabel!
  @IBOutlet var swipeableAreaHeightConstraint: NSLayoutConstraint!
  @IBOutlet var swipeableAreaView: UIView!
  @IBOutlet var controllerContainerView: UIView!
  @IBOutlet var leftPointingChevronsImageView: UIImageView!
  @IBOutlet var rightPointingChevronsImageView: UIImageView!

  weak var controller: UIViewController!

  var swipeAreaTitle = String()
  var swipeAreaSubtitle = String()
  var swipeAreaHeight: CGFloat = 0

  var swipeDirection: SessionPageSwipeDirection

  fileprivate let animationDuration = 0.3
  
  init(controller: UIViewController, swipeAreaHeight: CGFloat, swipeDirection: SessionPageSwipeDirection) {
    self.controller = controller
    self.swipeAreaHeight = swipeAreaHeight
    self.swipeDirection = swipeDirection
    super.init(nibName: "SessionPageViewController", bundle: nil)
  }

  required init?(coder aDecoder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
  }

  deinit {
    print ("SessionPageViewController is being deinitialized")
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    let styleSheet = MezzanineStyleSheet.shared()

    swipeableAreaView.backgroundColor = styleSheet?.superHighlightColor
    self.view.backgroundColor = styleSheet?.darkestFillColor

    addChildViewController(controller)

    let controllerView = controller.view
    controllerView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    controllerView?.frame = controllerContainerView.bounds
    controllerContainerView.addSubview(controllerView!)

    controller.didMove(toParentViewController: self)

    titleLabel.text = swipeAreaTitle
    subtitleLabel.text = swipeAreaSubtitle

    titleLabel.textColor = UIColor.white
    titleLabel.font = UIFont.dinBold(ofSize: 12)

    subtitleLabel.textColor = UIColor.white
    subtitleLabel.font = UIFont.din(ofSize: 12)

    leftPointingChevronsImageView.isHidden = swipeDirection == .right ? true : false
    rightPointingChevronsImageView.isHidden = swipeDirection == .left ? true : false

    updateSwipeAreaHeight(swipeAreaHeight)
  }

  func updateSwipeAreaHeight(_ height: CGFloat) {
    swipeableAreaHeightConstraint?.constant = height
    self.view.setNeedsUpdateConstraints()
    self.view.layoutIfNeeded()
  }

  func setSwipeable(_ canBeSwiped: Bool, animated: Bool) {
    let layoutBlock = {
      self.updateSwipeAreaHeight(canBeSwiped ? self.swipeAreaHeight : 0)
      self.swipeableAreaView.alpha = canBeSwiped ? 1.0 : 0.0
    }
    if animated {
      UIView.animate(withDuration: animationDuration, delay: 0, options: .beginFromCurrentState, animations: {
        layoutBlock()
        }, completion: nil)
    }
    else {
      layoutBlock()
    }
  }
  
  func setSwipeBarEnabled(_ enabled: Bool) {
    UIView.animate(withDuration: animationDuration, delay: 0, options: .beginFromCurrentState, animations: {
      self.swipeableAreaView?.alpha = enabled ? 1.0 : 0.3
      }, completion: nil)
  }
}
