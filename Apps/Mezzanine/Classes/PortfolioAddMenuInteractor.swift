//
//  PortfolioAddMenuInteractor.swift
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 13/04/2017.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//


import UIKit
import Photos

class PortfolioAddMenuInteractor: NSObject {
  
  @objc var presenter: UIViewController?
  @objc var communicator: MZCommunicator!
  let imageUploadQueue = OperationQueue()
  
  fileprivate var systemModelObserverArray = Array <String> ()
  fileprivate var presentationObserverArray = Array <String> ()
  
  @objc var systemModel: MZSystemModel! {
    willSet(newSystemModel) {
      if self.systemModel != newSystemModel {
      }
    }
    
    didSet {
      if systemModel != nil {
        presentation = systemModel.currentWorkspace.presentation
        systemModelObserverArray.removeAll()
      }
      else {
        presentation = nil
      }
    }
  }
  
  @objc var presentation: MZPresentation! {
    willSet(newPresentation) {
      if self.presentation != newPresentation {
      }
    }
    
    didSet {
      if presentation != nil {
        presentationObserverArray.removeAll()
      }
    }
  }
  
  
  init(systemModel: MZSystemModel, communicator: MZCommunicator) {
    
    self.systemModel = systemModel
    self.communicator = communicator
    self.presentation = systemModel.currentWorkspace.presentation
    
    imageUploadQueue.name = "imageUploadQueue"
    imageUploadQueue.maxConcurrentOperationCount = 1
    
    super.init()
  }
  
  required init?(coder aDecoder: NSCoder) {
    
    fatalError("init(coder:) has not been implemented")
  }
  
  @objc func showSlideCreationView() {
    
    let controller = SlideCreationViewController()
    let navController = MezzanineNavigationController(rootViewController: controller)
    navController.modalPresentationStyle = .formSheet
    presenter!.present(navController, animated: true, completion: nil)
  }
  
  @objc func showPhotoLibrary() {
    DispatchQueue.main.async(execute: {
      self.showImagePicker(.photoLibrary)
    })
  }
  
  @objc func showCamera() {
    DispatchQueue.main.async(execute: {
      self.showImagePicker(.camera)
    })
  }
  
  @objc func showImagePicker(_ source: UIImagePickerControllerSourceType) {
    
    if source == .photoLibrary {
      PhotoManager.sharedInstance.authorizationStatus({ (status, promptedAccess) in
        if status == .authorized {
          DispatchQueue.main.async(execute: {
            let photoPicker = PhotoPickerController(delegate: self)
            photoPicker.modalPresentationStyle = .formSheet
            self.presenter!.present(photoPicker, animated: true, completion: nil)
          })
        }
        else if (!promptedAccess) {
          DispatchQueue.main.async(execute: {
            self.presentAccessErrorAlert(.photoLibrary)
          })
        }
      })
      
    }
    else if source == .camera {
      
      let status = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
      if status == .authorized {
        DispatchQueue.main.async(execute: {
          self.presentImagePickerForCamera()
        })
      }
      else {
        AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted) in
          if granted {
            DispatchQueue.main.async(execute: {
              self.presentImagePickerForCamera()
            })
          }
          else {
            DispatchQueue.main.async(execute: {
              if status == .notDetermined {
                // First time the user was asked permission to access the camera and denied it
              }
              else {
                self.presentAccessErrorAlert(.camera)
              }
            })
          }
        })
      }
    }
  }
  
  @objc func captureWhiteboardAtIndex(_ index: Int) {
    
    let whiteboard = systemModel.whiteboards[index] as! Dictionary<String, String>
    if let whiteboardUID = whiteboard["uid"] {
      communicator.requestor.requestWhiteboardCaptureRequest(whiteboardUID, workspaceUid:systemModel.currentWorkspace.uid)
    }
  }
  
  @objc func presentAccessErrorAlert(_ source: UIImagePickerControllerSourceType) {
    
    var title = ""
    var errorMessage = ""
    
    if source == .camera {
      title = "Camera Error Enabling Access Title".localized
      errorMessage = "Camera Error Enabling Access Message".localized
    }
    else if source == .photoLibrary {
      title = "Photos Framework Error Enabling Access Title".localized
      errorMessage = "Photos Framework Error Enabling Access Message".localized
    }
    
    let alertController = UIAlertController(title: title, message: errorMessage, preferredStyle:.alert)
    
    let settingsAction = UIAlertAction(title: "Generic Title Settings".localized, style: .default) { (_) in
      UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
    }
    
    let okAction = UIAlertAction(title: "Generic Title OK".localized, style: .default, handler: nil)
    
    alertController.addAction(settingsAction)
    alertController.addAction(okAction)

    DispatchQueue.main.async(execute: {
      self.presenter!.present(alertController, animated: true, completion: nil)
    })
  }
  
  @objc func presentImagePickerForCamera() {
    
    let imagePicker = UIImagePickerController()
    imagePicker.delegate = self
    imagePicker.sourceType = .camera
    imagePicker.cameraDevice = .rear
    imagePicker.cameraCaptureMode = .photo
    imagePicker.showsCameraControls = true
    DispatchQueue.main.async(execute: {
      self.presenter!.present(imagePicker, animated: true, completion: nil)
    })
  }
  
  @objc func createWhiteboardInteractor() -> WhiteboardInteractor {
    
    let whiteboardInteractor = WhiteboardInteractor()
    whiteboardInteractor.communicator = communicator
    whiteboardInteractor.systemModel = systemModel
    return whiteboardInteractor
  }
  
  @objc func numberOfWhiteboards() -> Int {
    
    return systemModel.whiteboards.count
  }
}

extension PortfolioAddMenuInteractor: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
  
  func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
    if presenter is PortfolioActionsViewController {
      (presenter as! PortfolioActionsViewController).dimissImagePicker()
    } else if presenter is BinCollectionViewController {
      (presenter as! BinCollectionViewController).dimissImagePicker()
    }
  }
  
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
    
    let image = info[UIImagePickerControllerOriginalImage] as! UIImage
    if canUploadImage(image) {
      let referenceURL = info[UIImagePickerControllerReferenceURL] as! URL?
      var filename = "Camera Capture \(Date()).jpg"
      
      if let referenceURL = referenceURL {
        filename = referenceURL.lastPathComponent
      }
      
      // TODO: Check this "jpg"
      requestUploadImage(image, filename: filename, format: "jpg")
    }
    else {
      print("This device takes pictures that are bigger than what Mezzanines allows.")
    }
    
    if presenter is PortfolioActionsViewController {
      (presenter as! PortfolioActionsViewController).dimissImagePicker()
    } else if presenter is BinCollectionViewController {
      (presenter as! BinCollectionViewController).dimissImagePicker()
    }
  }
  
  func requestUploadImage(_ image: UIImage, filename: String, format: String) {
    
    let onError = { (error: Error?) in
      
      // TODO
      // Update upload status as done in PortfolioViewController
      
      guard let error = error else {
        return
      }
      
      let message = error.localizedDescription
      self.systemModel.popupMessage = message
    }
    
    let limitReached: MZFileBatchUploadRequestLimitReachedBlock = {(transaction, assetsUids, continuationBlock) in
      
      // TODO
      // Update upload status as done in PortfolioViewController
      
    }
    
    let onSuccess = {
      
      // TODO
      // Update upload status as done in PortfolioViewController
      
    }
    
    let uploadInfo = MZFileUploadInfo()
    uploadInfo.fileName = filename
    uploadInfo.format = format
    uploadInfo.imageBlock = {
      return image
    }
    
    communicator.uploader.requestFilesUpload([uploadInfo], workspaceUid: systemModel.currentWorkspace.uid, limitReached: limitReached, singleUploadCompleted: onSuccess, errorHandler: onError)
  }
}

extension PortfolioAddMenuInteractor: PhotoPickerDelegate {
  
  func photoPickerControllerDidCancel() {
    
    // Nothing for now
  }
  
  func photoPickerControllerDidSelectPhotos(_ photos: NSArray) {
    
    if photos.count <= 0 {
      return
    }
    
    imageUploadQueue.addOperation { [unowned self]() -> Void in
      
      // Fetch data to initialize photo metadata and download images from iCloud
      for photoAsset in photos as! Array<PhotoAsset> {
        PhotoManager.sharedInstance.fetchPhotoAssetData(photoAsset)
      }
      
      let uploadQueueReady = self.prepareUploadQueue(photos as! Array<PhotoAsset>)
      
      if self.presenter is BinCollectionViewController {
        DispatchQueue.main.async(execute: {
        (self.presenter as! BinCollectionViewController).interactor.updateTransferStatusMessage()
        })
      }
      
      // This block starts the transaction requests that handles the batch upload
      // It is informed in case the paramus or deck reaches its limits to
      // ask the user how to proceed (partial upload or canceling)
      // Also every time each image is uploaded it refreshes the status view
      // and handles the any error with the transaction.
      let progressiveUploadClosure = self.prepareProgressiveUploadClosure(uploadQueueReady.uploadInfoArray)
      
      
      // The progressive batch upload is executed directly or after
      // informing about the number of size-invalid images in the batch
      let imagesExceeding = uploadQueueReady.imagesExceeding
      if imagesExceeding.pixelSize == 0 && imagesExceeding.fileSize == 0 {
        
        OperationQueue.main.addOperation({
          progressiveUploadClosure()
        })
      }
      else {
        
        guard let errorString = self.errorMessageForUploadsExceedingLimits(imagesExceeding) else { return }
        
        OperationQueue.main.addOperation({
          if self.presenter is PortfolioActionsViewController {
            (self.presenter as! PortfolioActionsViewController).showPartialUploadAlertAndContinue(errorString) {
              if uploadQueueReady.uploadInfoArray.count > 0 {
                progressiveUploadClosure()
              }
            }
          } else if self.presenter is BinCollectionViewController {
            (self.presenter as! BinCollectionViewController).showPartialUploadAlertAndContinue(errorString) {
              if uploadQueueReady.uploadInfoArray.count > 0 {
                progressiveUploadClosure()
              }
            }
          }
        })
      }
    }
  }
  
  func prepareUploadQueue(_ photos: Array<PhotoAsset>) -> (uploadInfoArray: Array<MZFileUploadInfo>, imagesExceeding: (pixelSize: Int, fileSize: Int)) {
    
    var uploadInfoArray = Array<MZFileUploadInfo>()
    var imagesExceedingPixelSize: Int = 0
    var imagesExceedingFileSize: Int = 0
    
    for photoAsset in photos {
      let acceptedFileSize = self.canUploadImageWithFileSize(photoAsset.filesize)
      
      var acceptedPixelSize = false
      let photoSize = CGSize(width: CGFloat(photoAsset.pixelWidth), height: CGFloat(photoAsset.pixelHeight))
      if !self.systemModel.featureToggles.largeImagesEnabled && self.systemModel.featureToggles.immenseImagesEnabled {
        acceptedPixelSize = self.canUploadImageWithPixelDimension(photoSize)
      }
      else {
        acceptedPixelSize = self.canUploadImageWithPixelSize(photoSize)
      }
      
      if acceptedPixelSize && acceptedFileSize {
        let uploadInfo = MZFileUploadInfo()
        uploadInfo.fileName = photoAsset.filename
        uploadInfo.imageDataBlock = photoAsset.imageDataBlock
        uploadInfo.format = photoAsset.imageType
        uploadInfoArray.append(uploadInfo)
      }
      else {
        imagesExceedingPixelSize += !acceptedPixelSize ? 1 : 0;
        imagesExceedingFileSize += !acceptedFileSize ? 1 : 0;
      }
    }
    
    return (uploadInfoArray, (imagesExceedingPixelSize, imagesExceedingFileSize))
  }
  
  func prepareProgressiveUploadClosure(_ uploadInfoArray: Array<MZFileUploadInfo>) -> ( () -> () ) {
    
    let closure: () -> () = {
      let numberOfItems = uploadInfoArray.count

      self.communicator.uploader.requestFilesUpload(uploadInfoArray, workspaceUid: self.systemModel.currentWorkspace.uid, limitReached: { (requestTransaction, uids, continueBlock) in
        if self.presenter is PortfolioActionsViewController {
          (self.presenter as! PortfolioActionsViewController).showUploadLimitReachedAlert(numberOfItems, requestTransaction: requestTransaction, uids: uids as! Array<String>, continueBlock: continueBlock)
        } else if self.presenter is BinCollectionViewController {
          (self.presenter as! BinCollectionViewController).showUploadLimitReachedAlert(numberOfItems, requestTransaction: requestTransaction, uids: uids as! Array<String>, continueBlock: continueBlock)
        }

        if self.presenter is BinCollectionViewController {
          DispatchQueue.main.async(execute: {
            (self.presenter as! BinCollectionViewController).interactor.updateTransferStatusMessage()
          })
        }
      }, singleUploadCompleted: { 
        if self.presenter is BinCollectionViewController {
          DispatchQueue.main.async(execute: {
            (self.presenter as! BinCollectionViewController).interactor.updateTransferStatusMessage()
          })
        }
      }, errorHandler: { (error) in

        let error = error as NSError
        if self.presenter is PortfolioActionsViewController {
          (self.presenter as! PortfolioActionsViewController).showUploadErrordAlert(error)
        } else if self.presenter is BinCollectionViewController {
          (self.presenter as! BinCollectionViewController).showUploadErrordAlert(error)
        }
      })
    }
    return closure
  }
  
  func errorMessageForUploadsExceedingLimits(_ imagesExceeding: (pixelSize: Int, fileSize: Int)) -> String? {
    
    let uploadLimits = self.systemModel.uploadLimits
    let largeAndImmenseImageNotEnabled = !self.systemModel.featureToggles.largeImagesEnabled && !self.systemModel.featureToggles.immenseImagesEnabled
    
    var errorString: String?
    
    if imagesExceeding.fileSize > 0 && imagesExceeding.pixelSize > 0 {
      errorString = largeAndImmenseImageNotEnabled ? String(format:"Multiple Images Upload File And Pixel Size Error Dialog Message".localized, (uploadLimits?.maxImageSizeInMB)!, (uploadLimits?.maxImageWidth)!, (uploadLimits?.maxImageHeight)!) : String(format: "Multiple Images Upload File And Exceeded Megapixels Error Dialog Message".localized, (uploadLimits?.maxImageSizeInMB)!, (uploadLimits?.maxImageSizeInMP)!)
    }
    else if imagesExceeding.fileSize > 0 {
      errorString = (imagesExceeding.fileSize == 1 ) ?
        String(format:"Single Image Upload File Size Error Dialog Message".localized, (uploadLimits?.maxImageSizeInMB)!) :
        String(format:"Multiple Images Upload File Size Error Dialog Message".localized, (uploadLimits?.maxImageSizeInMB)!)
    }
    else if imagesExceeding.pixelSize > 0 {
      
      if largeAndImmenseImageNotEnabled {
        errorString = (imagesExceeding.pixelSize == 1 ) ?
          String(format:"Single Image Upload Pixel Dimension Error Dialog Message".localized, (uploadLimits?.maxImageWidth)!, (uploadLimits?.maxImageHeight)!) :
          String(format:"Multiple Images Upload Pixel Dimension Error Dialog Message".localized, (uploadLimits?.maxImageWidth)!, (uploadLimits?.maxImageHeight)!)
      }
      else {
        errorString = (imagesExceeding.pixelSize == 1 ) ?
          String(format:"Single Image Upload Exceeded Megapixels Error Dialog Message".localized, (uploadLimits?.maxImageSizeInMP)!) :
          String(format:"Multiple Images Upload Exceeded Megapixels Error Dialog Message".localized, (uploadLimits?.maxImageSizeInMP)!)
      }
    }
    return errorString
  }
  
}

// Image Upload Limits

extension PortfolioAddMenuInteractor {
  
  func canUploadImage(_ image: UIImage) -> Bool {
    
    let imageData = NSData(data: UIImageJPEGRepresentation(image, 0.9)!) as Data
    
    if !systemModel.featureToggles.largeImagesEnabled && !systemModel.featureToggles.immenseImagesEnabled {
      return canUploadImageWithPixelDimension(image.size) && canUploadImageWithFileSize(imageData.count)
    }
    
    let imagePixelSize = CGSize(width: CGFloat(image.cgImage!.width), height: CGFloat(image.cgImage!.height))
    return canUploadImageWithPixelSize(imagePixelSize) && canUploadImageWithFileSize(imageData.count)
  }
  
  func canUploadImageWithPixelDimension(_ size: CGSize) -> Bool {
    
    let uploadLimits = systemModel.uploadLimits
    
    return ((Float(size.width) <= uploadLimits!.maxImageWidth.floatValue &&
      Float(size.height) <= uploadLimits!.maxImageHeight.floatValue) ||
      (Float(size.height) <= uploadLimits!.maxImageWidth.floatValue &&
        Float(size.width) <= uploadLimits!.maxImageHeight.floatValue)) &&
      Float(size.width) > 0 && Float(size.height) > 0
  }
  
  func canUploadImageWithFileSize(_ size: Int) -> Bool {
    let limitInBytes = systemModel.uploadLimits.maxImageSizeInMB.intValue * 1024 * 1024
    return size < limitInBytes && size > 0
  }
  
  func canUploadImageWithPixelSize(_ size: CGSize) -> Bool{
    let totalPixels = size.width * size.height
    let limitInPixels = systemModel.uploadLimits.maxImageSizeInMP.intValue * 1024 * 1024
    return totalPixels < CGFloat(limitInPixels) && totalPixels > 0
  }
  
}
