//
//  ImageAssetView.h
//  Mezzanine
//
//  Created by Zai Chang on 2/15/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AssetView.h"


enum
{
  ImageAssetViewStandardThumb,
  ImageAssetViewLargeThumb,
  ImageAssetViewFullResolution
};
typedef NSInteger ImageAssetViewSourceType;

@interface ImageAssetView : AssetView <UIGestureRecognizerDelegate>

@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, assign) ImageAssetViewSourceType imageSourceType;

-(void) loadAsset;
-(CGSize) imageSizeAfterScale;

@end
