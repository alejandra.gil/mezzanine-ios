//
//  ExtendedWindshieldViewController_Private.h
//  Mezzanine
//
//  Created by Miguel Sánchez Valdés on 09/10/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

#import "ExtendedWindshieldViewController.h"

@class SurfaceView;

@interface ExtendedWindshieldViewController ()
{
  BOOL isPresenting;  // Active while the Extended Windshield is animating to appear or disappear
  UIView *feldBoundViewsContainer;
  NSMutableArray *separatorLineViews;
  MZWindshieldItem *droppedShielderWaitingForConfirmation;
  NSMutableArray *systemModelObservers;
}

-(CGFloat) borderWidth;
-(SurfaceView *) surfaceViewForSurface:(MZSurface *)surface;

@end
