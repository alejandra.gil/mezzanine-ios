//
//  AssetOvumSourceViewController.h
//  Mezzanine
//
//  Created by Zai Chang on 6/25/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OBDragDropManager.h"

@interface AssetOvumSourceViewController : UIViewController <OBOvumSource>

// Scale to which the ovum's drag view will animate to when it becomes visible
// Default is 1.25
@property (nonatomic, assign) CGFloat dragViewScale;

@end
