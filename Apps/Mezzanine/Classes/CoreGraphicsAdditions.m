//
//  CoreGraphicsAdditions.m
//  Mezzanine
//
//  Created by Zai Chang on 1/16/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import "CoreGraphicsAdditions.h"


CGFloat CGPointDistance(CGPoint pointA, CGPoint pointB)
{
  CGFloat deltaX = pointA.x - pointB.x;
  CGFloat deltaY = pointA.y - pointB.y;
  return sqrt(deltaX*deltaX + deltaY*deltaY);
}
