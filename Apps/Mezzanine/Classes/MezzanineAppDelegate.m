//
//  MezzanineAppDelegate.m
//  Mezzanine
//
//  Created by Zai Chang on 9/17/10.
//  Copyright 2010 Oblong Industries. All rights reserved.
//

#import "MezzanineAppDelegate.h"
#import "MZCommunicator.h"
#import "NSUserDefaults+MezzanineSettings.h"
#import "MezzanineAppContext.h"
#import "NSURL+Additions.h"
#import "NSObject+UIAlertController.h"
#import "MezzanineConnectionViewController_Private.h"

#import "Mezzanine-Swift.h"

// Maximum time, in seconds, that the app will keep connections to a Mezzanine alive
// Since iOS 7 this has been reduced to 3 minutes (180.0 seconds)
#define kBackgroundTaskSleepInterval 3.0
static CGFloat kBackgroundTaskDuration = 180.0 - 2 * kBackgroundTaskSleepInterval;

@implementation MezzanineAppDelegate

#pragma mark -
#pragma mark Application Lifecycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions 
{
  NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleVersionKey];
  [[MezzanineAnalyticsManager sharedInstance] autoIntegrate:launchOptions];
  [[MezzanineAnalyticsManager sharedInstance] initializeGoogleAnalyticsTrackingWithAppVersion:appVersion];
  [[SettingsFeatureToggles sharedInstance] updateSettingsToggles];

  [TTStyleSheet setGlobalStyleSheet:[[MezzanineStyleSheet alloc] init]];
  [MZAssetCache setSharedCache:[[MZAssetCache alloc] init]];
  [NSUserDefaults registerMezzanineSettings];
  
  appContext = [[MezzanineAppContext alloc] init];
  appContext.applicationModel = [[MezzanineAppModel alloc] init];
  appContext.window = _window;
  appContext.mainNavController = (MezzanineNavigationController *)_window.rootViewController;
  appContext.mainNavController.delegate = appContext;
  appContext.assetCache = [MZAssetCache sharedCache];
  [appContext setupDragDropManager];
  [appContext setupInfopresenceOverlay];
  [appContext initPopupInfoView];
  [appContext initProgressView];
  [MezzanineAppContext setCurrentContext:appContext];


  MezzanineConnectionViewController *connectionViewController = (MezzanineConnectionViewController *)[((MezzanineNavigationController *)_window.rootViewController) topViewController];
  connectionViewController.delegate = appContext;

  appContext.connectionViewController = connectionViewController;

  application.statusBarStyle = UIStatusBarStyleLightContent;
  
  NSURL *launchURL = launchOptions[UIApplicationLaunchOptionsURLKey];
  if (launchURL)
  {
    return [self application:application openURL:launchURL options:@{}];
  }
  
  
  if([UIApplicationShortcutItem class]){
    UIApplicationShortcutItem *shortcutItem = launchOptions[UIApplicationLaunchOptionsShortcutItemKey];
    if (shortcutItem) {
      //If the connection can be done we must return NO. Otherwise performActionForShortcutItem will be called.
      return ![self connectToPool:shortcutItem.localizedTitle];
    }
  }
  
  
  return YES;
}


- (void)application:(UIApplication *)application performActionForShortcutItem:(UIApplicationShortcutItem *)shortcutItem completionHandler:(void (^)(BOOL))completionHandler
{
  [self connectToPool:shortcutItem.localizedTitle];
}


- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options
{
  if (url.isFileURL)
  {
    DLog(@"Opening file %@", url);
    
    NSError *error = nil;
    
    if (!appContext.currentCommunicator)
    {
      UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Upload NotConnectedError Dialog Title", nil)
                                                                               message:NSLocalizedString(@"Upload NotConnectedError Dialog Message", nil)
                                                                        preferredStyle:UIAlertControllerStyleAlert];
      UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Generic Button Title Okay", nil) style:UIAlertActionStyleCancel handler:nil];
      [alertController addAction:cancelAction];
      [self showAlertController:alertController animated:YES completion:nil];
      return NO;
    }
    
    if (![appContext.documentInteractionController canUploadDocumentAtURL:url error:&error])
    {
      NSString *errorDescription = [error localizedDescription];
      UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Upload FileTypeNotSupported Dialog Title", nil)
                                                                               message:(errorDescription ? errorDescription : NSLocalizedString(@"Upload FileTypeNotSupported Dialog Message", nil))
                                                                        preferredStyle:UIAlertControllerStyleAlert];
      UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Generic Button Title Okay", nil) style:UIAlertActionStyleCancel handler:nil];
      [alertController addAction:cancelAction];
      [self showAlertController:alertController animated:YES completion:nil];
      return NO;
    }
    
    if (appContext.currentCommunicator.systemModel.passphraseRequested)
    {
      UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Upload SessionLockedError Dialog Title", nil)
                                                                               message:NSLocalizedString(@"Upload SessionLockedError Dialog Message", nil)
                                                                        preferredStyle:UIAlertControllerStyleAlert];
      UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Generic Button Title Okay", nil) style:UIAlertActionStyleCancel handler:nil];
      [alertController addAction:cancelAction];
      [self showAlertController:alertController animated:YES completion:nil];
      return NO;
    }
        
    NSString *filename = [[url path] lastPathComponent];

    // Ask user for confirmation?
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Upload Confirmation Dialog Title", nil), filename]
                                                                             message:NSLocalizedString(@"Upload Confirmation Dialog Message", nil)
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Upload Confirmation Dialog Cancel Button", nil) style:UIAlertActionStyleCancel handler:nil];
    UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Upload Confirmation Dialog Confirm Button", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
      [appContext.documentInteractionController uploadDocumentAtURL:url usingCommunicator:appContext.currentCommunicator];
    }];
    [alertController addAction:cancelAction];
    [alertController addAction:confirmAction];
    [self showAlertController:alertController animated:YES completion:nil];
    return YES;
  }
  
  return [self attempToConnectFromURL: url];
}


- (BOOL)attempToConnectFromURL:(NSURL *)url
{
  // Clean up the URL in case it contains two schemes (i.e. mezzanine+https, mezzanine+wss)
  url = [url sanitizeURLForMezzanineProtocol];
  NSString *host = [url host];
  
  if (host)
  {
    if (appContext.currentCommunicator)
    {
      // If there's an existing session, kill it if the hostname is different?
      // If we are about to connect a RP session host is always the same one (for now) so we need to check for the path as well
      NSURL *currentURL = appContext.currentCommunicator.serverUrl;
      if ([host isEqualToString:[currentURL host]]) {
        if ([appContext.currentCommunicator isARemoteParticipationConnection]
            && ![[url path] isEqualToString: [currentURL path]])
        {
          [appContext.currentCommunicator disconnectWithMessage:[MezzanineAnalyticsManager sharedInstance].newMezzanineURLAttribute];
        } else {
          return YES;
        }
      }
      else {
        [appContext.currentCommunicator disconnectWithMessage:[MezzanineAnalyticsManager sharedInstance].newMezzanineURLAttribute];
      }
    }
    
    [appContext.connectionViewController connectToServerAtURL:[url URLByReplacingSchemeWithHTTPS]
                                                         from:[MezzanineAnalyticsManager sharedInstance].URLAttribute];
    
    return YES;
  }
  
  return NO;
}


-(void) applicationWillResignActive:(UIApplication *)application 
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
  DLog(@"Mezzanine: applicationWillResignActive");
  
  
}

-(void) applicationDidBecomeActive:(UIApplication *)application
{
  /*
   Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
   */
  DLog(@"Mezzanine: applicationDidBecomeActive");
  
  [[[MezzanineAppContext currentContext] exportManager] checkPendingCompletedExports];
  
  
  // Check server timeout when application wakes up or returns from a locked screen,
  // phone call or SMS ...
  // May be redundant because when wifi first resumes native-side proteins are received
  // and the heartbeat check in that protein handler seems to be getting triggererd first
  // But its better to be safe no?
  
  MZCommunicator *communicator = [MezzanineAppContext currentContext].currentCommunicator;
  if (communicator)
  {
    if ((!communicator.isConnecting && !appContext.attemptingToReconnect)
        && ![communicator checkHeartbeatStatus])
    {
      [communicator disconnectWithMessage:[MezzanineAnalyticsManager sharedInstance].heartbeatTimeoutAttribute];
    }
  }
}


-(void) applicationDidEnterBackground:(UIApplication *)application 
{
  DLog(@"Mezzanine: applicationDidEnterBackground");
  
  appContext.inBackground = YES;
  appContext.enteredBackgroundDate = [NSDate date];
  
#define ALLOW_BACKGROUND_CONNETION 1
  
  MZCommunicator *communicator = [MezzanineAppContext currentContext].currentCommunicator;
  if (communicator)
  {
#if ALLOW_BACKGROUND_CONNETION
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
      
      while ([[NSDate date] timeIntervalSinceDate:appContext.enteredBackgroundDate] < kBackgroundTaskDuration &&
             [[UIApplication sharedApplication] backgroundTimeRemaining] > kBackgroundTaskSleepInterval)
      {
        [NSThread sleepForTimeInterval:kBackgroundTaskSleepInterval];
        DLog(@"[[UIApplication sharedApplication] backgroundTimeRemaining] = %f", [[UIApplication sharedApplication] backgroundTimeRemaining]);
      }
      
      if (backgroundTaskIdentifier != UIBackgroundTaskInvalid)
      {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
          [communicator disconnectWithMessage:[MezzanineAnalyticsManager sharedInstance].backgroundTimeoutAttribute];
          
          [application endBackgroundTask:backgroundTaskIdentifier];
          backgroundTaskIdentifier = UIBackgroundTaskInvalid;
        }];
      }
    });
    
    backgroundTaskIdentifier = [application beginBackgroundTaskWithExpirationHandler:^(void) {
      [application endBackgroundTask:backgroundTaskIdentifier];
      backgroundTaskIdentifier = UIBackgroundTaskInvalid;
    }];
#else
    [communicator disconnect];
#endif
  }
  
  // Automatically cleanup inbox
  [appContext cleanupInbox];
}

-(void) applicationWillEnterForeground:(UIApplication *)application
{
  // TODO: Remove when teamwork toggle is deleted
  BOOL teamworkUICurrentToggle = [SettingsFeatureToggles sharedInstance].teamworkUI;
  
  [[SettingsFeatureToggles sharedInstance] updateSettingsToggles];
  [MezzanineAnalyticsManager sharedInstance].amountTimesAppStarted ++;
  

  // TODO: Remove when teamwork toggle is deleted
  // TMP. Teamwork automatic reconnection if toggle changed
  if ([SettingsFeatureToggles sharedInstance].teamworkUI != teamworkUICurrentToggle) {
    MZCommunicator *communicator = [MezzanineAppContext currentContext].currentCommunicator;
    if (communicator)
    {
      NSString *serverName = [communicator.serverUrl host];
      dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [communicator disconnect];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
          [[MezzanineAppContext currentContext].connectionViewController connectTo:serverName];
        });
      });
    }
    
    appContext.enteredBackgroundDate = nil;
    appContext.inBackground = NO;
    [application endBackgroundTask:backgroundTaskIdentifier];
    backgroundTaskIdentifier = UIBackgroundTaskInvalid;
    
    return;
  }
  ///
  
  
  // Try reconnecting to all pools
  DLog(@"Mezzanine: applicationWillEnterForeground");
  
  BOOL tryToReconnect = YES;
  
  if (appContext.enteredBackgroundDate)
  {
    static NSTimeInterval userWouldntHaveRememberedWhatHeWasDoingInMezz = 60 * 15;  // 15 minutes
    NSTimeInterval howLongHasItBeen = [[NSDate date] timeIntervalSinceDate:appContext.enteredBackgroundDate];
    
    if (howLongHasItBeen > userWouldntHaveRememberedWhatHeWasDoingInMezz)
      tryToReconnect = NO;
    
    appContext.enteredBackgroundDate = nil;
  }
  
  appContext.inBackground = NO;
  
  if (backgroundTaskIdentifier != UIBackgroundTaskInvalid)
  {
    [application endBackgroundTask:backgroundTaskIdentifier];
    backgroundTaskIdentifier = UIBackgroundTaskInvalid;
  }

  MZCommunicator *communicator = [MezzanineAppContext currentContext].currentCommunicator;
  if (communicator)
  {
    if (communicator.isConnected)
      return;
    
    if (tryToReconnect)
    {
      // Reconnect to pools
      DLog(@"Mezzanine: Attempting to reconnect to pools");
      
      if (communicator.isConnecting)
      {
        // Still trying the initial connection
        appContext.attemptingToReconnect = NO;
      }
      else
      {
        appContext.attemptingToReconnect = YES;
        
        // Add a activity indication view
        [appContext showProgressViewWithTitle:NSLocalizedString(@"Reconnecting Message", nil) indeterminate:YES];
      }
      
      [communicator connect];
    }
    else
    {
      // This is necessary for cleanup
      [communicator postDidDisconnectNotification:[MezzanineAnalyticsManager sharedInstance].backgroundTimeoutAttribute];
    }
  }
}


-(void)applicationWillTerminate:(UIApplication *)application 
{
    /*
     Called when the application is about to terminate.
     See also applicationDidEnterBackground:.
     */
  
  DLog(@"Mezzanine: applicationWillTerminate");
  
  // At this point, exported documents will be cleaned up because information about the export will be lost
  // (they're not serialized) making the files orphans
  [appContext cleanupExportedDocuments];
  [appContext cleanupInbox];

//  appContext.applicationModel = nil;
}

#pragma mark - Universal links

- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray * _Nullable))restorationHandler
{
  if ([userActivity.activityType isEqualToString:NSUserActivityTypeBrowsingWeb])
  {
    NSURL *url = userActivity.webpageURL;
    
    BOOL proceed = NO;
    for (NSString *pathComponent in url.pathComponents)
    {
      if ([pathComponent isEqualToString:@"m"] || [pathComponent isEqualToString:@"a"])
        proceed = YES;
    }
    
    if (!proceed)
      return NO;
    else
      return [self attempToConnectFromURL: url];
  }
  else if ([userActivity.activityType isEqualToString:[MezzanineUserActivities connected]])
  {
    NSURL *url = userActivity.webpageURL;
    return [self attempToConnectFromURL: url];
  }
  
  return NO;
}


#pragma mark - 3D Touch methods

- (BOOL)connectToPool:(NSString *)poolName
{
  BOOL validAttempt = NO;
  
  if (poolName && appContext)
  {
    if (appContext.currentCommunicator)
    {
      NSString *currentPool = appContext.currentCommunicator.serverUrl.host;
      if (![poolName isEqualToString:currentPool]) {
        [appContext.currentCommunicator disconnectWithMessage:[MezzanineAnalyticsManager sharedInstance].touchAttribute];
        [appContext.connectionViewController connectToServerAtURL:[NSURL httpsSchemeWithHostname:poolName] from:[MezzanineAnalyticsManager sharedInstance].touchAttribute];
      }
    } else {
      [appContext.connectionViewController connectToServerAtURL:[NSURL httpsSchemeWithHostname:poolName] from:[MezzanineAnalyticsManager sharedInstance].touchAttribute];
    }
    
    validAttempt = YES;
  }
  
  return validAttempt;
}

@end

