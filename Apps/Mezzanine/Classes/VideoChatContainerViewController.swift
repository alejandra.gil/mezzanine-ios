//
//  VideoChatContainerViewController.swift
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 23/05/2017.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import UIKit

protocol VideoChatContainerViewControllerDelegate: NSObjectProtocol {
  func videoChatContainerWillTransitionToState(_ state: VideoChatState)
}

class VideoChatContainerViewController: UIViewController {
  
  weak var delegate: VideoChatContainerViewControllerDelegate?
  var videoChatViewController: VideoChatViewController! = nil
  
  @IBOutlet var vtcBubbleContainerView: UIView!
  @IBOutlet var vtcBubbleContainerTopConstraint: NSLayoutConstraint!
  @IBOutlet var vtcBubbleContainerTrailingConstraint: NSLayoutConstraint!
  
  // Video chat
  fileprivate var videoChatOffset = CGPoint(x: 0.0, y: 0.0)
  fileprivate var lastRelativeTrailingOffset: CGFloat = 0.0
  fileprivate var lastRelativeBottomOffset: CGFloat = 0.0
  fileprivate var videoChatInSuperviewConstraints = Array<NSLayoutConstraint>()
  fileprivate var videoChatPanGestureRecognizer = UIPanGestureRecognizer()
  
  var nextTraitCollection: UITraitCollection!
  
  init(pexipManager: PexipManager) {
    videoChatViewController = VideoChatViewController(pexipManager: pexipManager)
    super.init(nibName: "VideoChatContainerViewController", bundle: nil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  deinit {
    print("VideoChatContainerViewController deinit")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    videoChatViewController.delegate = self
    addChildViewController(videoChatViewController)
    videoChatViewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    videoChatViewController.view.frame = vtcBubbleContainerView.bounds
    vtcBubbleContainerView.addSubview(videoChatViewController.view)
    videoChatViewController.didMove(toParentViewController: self)
    
    videoChatInSuperviewConstraints.append(vtcBubbleContainerTopConstraint)
    videoChatInSuperviewConstraints.append(vtcBubbleContainerTrailingConstraint)
    
    videoChatPanGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(self.handleVideoChatPreviewRelocation))
    videoChatPanGestureRecognizer.maximumNumberOfTouches = 1
    vtcBubbleContainerView.addGestureRecognizer(videoChatPanGestureRecognizer)
    
    UIView.performWithoutAnimation { 
      videoChatViewController.updateInterface()
    }
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  // MARK: VideoChatViewController layout and interaction methods
  
  func relocateVideoChatBubble() {
    let margin: CGFloat = traitCollection.isiPad() ? 15.0 : 5.0
    var originalRect: CGRect = vtcBubbleContainerView.frame
    let cmpRect: CGRect = vtcBubbleContainerView.frame.insetBy(dx: -margin, dy: -margin)
    
    let maxX = view.frame.width - view.safeAreaInsets.left - cmpRect.width
    let maxY = view.safeAreaLayoutGuide.layoutFrame.height - cmpRect.height
    
    if cmpRect.origin.x < 0.0 {
      originalRect.origin.x = margin
    } else if cmpRect.origin.x > maxX {
      originalRect.origin.x = maxX + margin
    }
    
    if (cmpRect.origin.y < 0.0) {
      originalRect.origin.y = margin
    } else if (cmpRect.origin.y > maxY) {
      originalRect.origin.y = maxY + margin
    }
    
    videoChatPanGestureRecognizer.isEnabled = false
    UIView.animate(withDuration: 0.15, delay: 0.0, options: .beginFromCurrentState, animations: {
      self.vtcBubbleContainerView.frame = originalRect
    }) { (finished) in
      self.videoChatOffset = CGPoint.zero
      self.setCurrentVideoChatFrameAsAutolayoutConstraints()
      self.videoChatPanGestureRecognizer.isEnabled = true
    }
  }
  
  @objc func handleVideoChatPreviewRelocation(_ recognizer: UIPanGestureRecognizer) {
    if !videoChatViewController.canBeDragged() {
      return
    }
    
    let locationInView = recognizer.location(in: view)
    if recognizer.state == .began {
      videoChatOffset.x = locationInView.x - vtcBubbleContainerView.center.x
      videoChatOffset.y = locationInView.y - vtcBubbleContainerView.center.y
      removeVideoChatViewControllerConstraints()
    } else if recognizer.state == .changed {
      var newCenter = CGPoint.zero
      newCenter.x = locationInView.x - videoChatOffset.x
      newCenter.y = locationInView.y - videoChatOffset.y
      vtcBubbleContainerView.center = newCenter
    } else if recognizer.state == .ended {
      relocateVideoChatBubble()
    }
  }
  
  func removeVideoChatViewControllerConstraints() {
    view.removeConstraints(videoChatInSuperviewConstraints)
    videoChatInSuperviewConstraints.removeAll()
    
    vtcBubbleContainerView.translatesAutoresizingMaskIntoConstraints = true
  }
  
  func setCurrentVideoChatFrameAsAutolayoutConstraints() {
    vtcBubbleContainerView.translatesAutoresizingMaskIntoConstraints = false
    
    if videoChatViewController.state == .preview {
        videoChatInSuperviewConstraints.append(NSLayoutConstraint(item: vtcBubbleContainerView, attribute: .trailing, relatedBy: .greaterThanOrEqual, toItem: view.safeAreaLayoutGuide, attribute: .trailing, multiplier: 1.0, constant: -(view.safeAreaLayoutGuide.layoutFrame.width - vtcBubbleContainerView.frame.maxX) - view.safeAreaInsets.right))
        
        videoChatInSuperviewConstraints.append(NSLayoutConstraint(item: vtcBubbleContainerView, attribute: .leading, relatedBy: .greaterThanOrEqual, toItem: view.safeAreaLayoutGuide, attribute: .leading, multiplier: 1.0, constant: 0.0))
        
        videoChatInSuperviewConstraints.append(NSLayoutConstraint(item: vtcBubbleContainerView, attribute: .bottom, relatedBy: .equal, toItem: view.safeAreaLayoutGuide, attribute: .bottom, multiplier: 1.0, constant: -(view.safeAreaLayoutGuide.layoutFrame.height - vtcBubbleContainerView.frame.maxY)))
    } else {
      videoChatInSuperviewConstraints.append(NSLayoutConstraint(item: vtcBubbleContainerView, attribute: .centerX, relatedBy: .equal, toItem: view.safeAreaLayoutGuide, attribute: .centerX, multiplier: 1.0, constant: 0.0))
      
      videoChatInSuperviewConstraints.append(NSLayoutConstraint(item: vtcBubbleContainerView, attribute: .centerY, relatedBy: .equal, toItem: view.safeAreaLayoutGuide, attribute: .centerY, multiplier: 1.0, constant: 0.0))
      
      videoChatInSuperviewConstraints.append(NSLayoutConstraint(item: vtcBubbleContainerView, attribute: .width, relatedBy: .equal, toItem: view.safeAreaLayoutGuide, attribute: .width, multiplier: 1.0, constant: 0.0))
      
      videoChatInSuperviewConstraints.append(NSLayoutConstraint(item: vtcBubbleContainerView, attribute: .height, relatedBy: .equal, toItem: view.safeAreaLayoutGuide, attribute: .height, multiplier: 1.0, constant: 0.0))
    }
    
    view.addConstraints(videoChatInSuperviewConstraints)
  }
  
  
  //MARK: Adaptivity
  
  override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
    super.willTransition(to: newCollection, with: coordinator)
    nextTraitCollection = newCollection
    updateLayout()
  }
  
  override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
    super.viewWillTransition(to: size, with: coordinator)
    
    if nextTraitCollection == nil {
      updateLayout()
    }
  }
  
  override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
    super.traitCollectionDidChange(previousTraitCollection)
    nextTraitCollection = nil
  }
  
  func updateLayout() {
    guard let videoChatViewController = videoChatViewController else { return }
    if videoChatViewController.state == .fullscreen && traitCollection.isiPhone() {
      removeVideoChatViewControllerConstraints()
      setCurrentVideoChatFrameAsAutolayoutConstraints()
    }
  }
}

extension VideoChatContainerViewController: VideoChatViewControllerDelegate {
  
  func videoChatViewControllerFrameForState(_ state: VideoChatState) -> CGRect {
    if state == .preview {
      let safeWidth = view.safeAreaLayoutGuide.layoutFrame.width
      let safeHeight = view.safeAreaLayoutGuide.layoutFrame.height
        
      let width: CGFloat = traitCollection.isiPad() ? 320.0 : 240.0
      let height: CGFloat = traitCollection.isiPad() ? 180.0 : 135.0
        
      let x: CGFloat = lastRelativeTrailingOffset != 0 ? safeWidth * lastRelativeTrailingOffset : safeWidth - width - 40.0
      let y: CGFloat = lastRelativeBottomOffset != 0 ? safeHeight * lastRelativeBottomOffset : safeHeight - height - 200.0
        
      return CGRect(x: x, y: y, width: width, height: height)
    } else {
      return view.safeAreaLayoutGuide.layoutFrame
    }
  }
  
  func videoChatViewControllerWillTransitionToState(_ videoChatViewController: VideoChatViewController!, state: VideoChatState, animations: @escaping () -> Void) {
    if state == .fullscreen {
      lastRelativeBottomOffset = vtcBubbleContainerView.frame.minY/view.safeAreaLayoutGuide.layoutFrame.height
      lastRelativeTrailingOffset = vtcBubbleContainerView.frame.minX/view.safeAreaLayoutGuide.layoutFrame.width
    }
    
    removeVideoChatViewControllerConstraints()
    let finalFrame: CGRect = videoChatViewControllerFrameForState(state)
    
    // Animations
    delegate?.videoChatContainerWillTransitionToState(state)
    UIView.animate(withDuration: 0.33, delay: 0, options: .beginFromCurrentState, animations: {
      self.vtcBubbleContainerView.frame = finalFrame
      animations()
      self.vtcBubbleContainerView.layoutIfNeeded()
    }) { (finished) in
      self.setCurrentVideoChatFrameAsAutolayoutConstraints()
    }
    
    // Analytics
    let analyticsManager = MezzanineAnalyticsManager.sharedInstance
    if state == .fullscreen {
      analyticsManager.tagScreen(analyticsManager.vtcFullScreen)
    } else {
      analyticsManager.tagWorkspaceStatusScreen()
    }
  }
}
