//
//  FeldSwitcherView.h
//  Mezzanine
//
//  Created by miguel on 30/05/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FeldSwitcherViewDelegate;

@interface FeldSwitcherView : UIView

@property (nonatomic, weak) id <FeldSwitcherViewDelegate> delegate;
@property (nonatomic, strong) MZSystemModel *systemModel;
@property (nonatomic, assign) NSInteger selectedFeldIndex;

- (CGSize)calculateSizeForView;

@end


@protocol FeldSwitcherViewDelegate <NSObject>

- (void)didTapOnFeld:(NSInteger)feldIndex;

@end