//
//  PhotoCollectionViewController.swift
//  Mezzanine
//
//  Created by miguel on 3/1/17.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import UIKit

protocol PhotoCollectionViewControllerDelegate: NSObjectProtocol {
  func photoCollectionViewControllerDidSelectPhotoAsset(_ asset: PhotoAsset)
  func photoCollectionViewControllerDidDeselectPhotoAsset(_ asset: PhotoAsset)
}

class PhotoCollectionViewController: UICollectionViewController {

  weak var delegate: PhotoCollectionViewControllerDelegate?

  fileprivate let reuseIdentifier = "PhotoCell"
  fileprivate let itemsPerRowRegular = 6
  fileprivate let itemsPerRowCompact = 4

  var assets: Array<PhotoAsset>

  init(assets: Array<PhotoAsset>) {
    self.assets = assets
    super.init(collectionViewLayout: PhotoCollectionViewFlowLayout())
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    // Register cell classes
    collectionView?.register(PhotoCollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)

    collectionView?.accessibilityIdentifier = "OBImageCollectionViewController"
    collectionView?.backgroundColor = UIColor.clear
    collectionView?.allowsMultipleSelection = true

  }

  func scrollToBottom() {
    if assets.count > 0 {
      let indexPath = IndexPath(item:assets.count-1, section:0);
      collectionView?.scrollToItem(at: indexPath, at: .bottom, animated: false)
    }
  }

  // MARK: UICollectionViewDataSource

  override func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }

  override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return assets.count
  }

  override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! PhotoCollectionViewCell

    // Configure the cell

    let asset = assets[indexPath.item]
    cell.photoAsset = asset

    return cell
  }

  // MARK: UICollectionViewDelegate

  override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    let asset = assets[indexPath.item]
    updateAssetState(asset)
  }

  override func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
    let asset = assets[indexPath.item]
    updateAssetState(asset)
  }

  func updateAssetState(_ asset: PhotoAsset) {
    if asset.selected == true {
      asset.selected = false
      delegate?.photoCollectionViewControllerDidDeselectPhotoAsset(asset)
    }
    else {
      asset.selected = true
      delegate?.photoCollectionViewControllerDidSelectPhotoAsset(asset)
    }
  }
}

extension PhotoCollectionViewController : UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    let maxItemsPerRow:CGFloat = self.isBeingPresentedInFormSheet() ? CGFloat(itemsPerRowRegular) : CGFloat(itemsPerRowCompact)
    let itemSpacingPerRow = PhotoCollectionViewFlowLayoutConstants.itemSpacing * maxItemsPerRow - 1
    let side = (collectionView.frame.width - 2 * PhotoCollectionViewFlowLayoutConstants.itemSpacing - itemSpacingPerRow) / maxItemsPerRow
    return CGSize(width: side, height: side)
  }
}

extension UIViewController {
  func isBeingPresentedInFormSheet() -> Bool {
    if let presentingViewController = presentingViewController {
      return traitCollection.horizontalSizeClass == .compact && presentingViewController.traitCollection.horizontalSizeClass == .regular
    }
    return false
  }
}
