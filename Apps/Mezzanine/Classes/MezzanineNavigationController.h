//
//  MezzanineNavigationController.h
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 22/03/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MezzanineNavigationController : UINavigationController <UINavigationControllerDelegate, UIGestureRecognizerDelegate>

- (void)pushViewController:(UIViewController *)viewController transitionView:(UIView *)transitionView;
- (void)popViewControllerWithTransitionView:(UIView *)transitionView;

@end
