//
//  LiveStreamCollectionViewCell.h
//  Mezzanine
//
//  Created by Zai Chang on 3/20/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MZLiveStream;

@interface LiveStreamCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) MZLiveStream *liveStream;
@property (strong, nonatomic) UIImageView *imageView;

@end
