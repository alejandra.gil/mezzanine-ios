//
//  BinHeaderViewController.swift
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 14/02/2017.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import UIKit

class BinHeaderView: UIView {
  @IBOutlet var indicator: UIImageView!
  @IBOutlet var title: UILabel!
  @IBOutlet var statusMessage: UILabel!

  @IBOutlet var statusMessageWidthConstraint: NSLayoutConstraint!
  @IBOutlet var indicatorHorizontalSpaceConstraint: NSLayoutConstraint!
}

protocol BinHeaderViewControllerDelegate: NSObjectProtocol {
  func binHeaderViewDidTapLivestreams()
  func binHeaderViewDidTapItems()
}

class BinHeaderViewController: UIViewController, BincollectionViewControllerDelegate {
  
//  private static var __once: () = {
//        itemsHeader.statusMessageWidthConstraint.constant = 0.0
//        itemsHeader.indicatorHorizontalSpaceConstraint.constant = 6.0
//        itemsHeader.frame.size.width = itemsHeader.indicator.frame.maxX
//        updateHeaders(collectionView, scrollDirection: scrollDirection)
//      }()
  
  // When there's no need to show the status message we don't need to re-layout all the time, just one to re-positioned the header.
  // This one time token fits that need
  struct Static {
    static var dispatchOnceReLayoutAfterUploading: Int = 0
  }
  
  weak var delegate: BinHeaderViewControllerDelegate?

  var collectionViewHasLiveStreams: Bool = false
  
  @IBOutlet var liveStreamsHeader: BinHeaderView!
  @IBOutlet var itemsHeader: BinHeaderView!
  @IBOutlet var separatorView: UIView!
  @IBOutlet var livestreamLabelLeftMargin: NSLayoutConstraint!
  
  fileprivate var headerMargin: CGFloat = 24.0
  fileprivate var headersHorizontalSpace: CGFloat = 24.0
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    initializeHeaders()
    separatorView.backgroundColor = MezzanineStyleSheet.shared().grey80Color
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    itemsHeader.statusMessageWidthConstraint.constant = 0.0
    itemsHeader.indicatorHorizontalSpaceConstraint.constant = 6.0
    itemsHeader.frame.size.width = itemsHeader.indicator.frame.maxX
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  func initializeHeaders() {
    // Apply kern to space the letters
    let attributes: NSDictionary = [
      NSAttributedStringKey.font:UIFont.dinMedium(ofSize: 12.0),
      NSAttributedStringKey.kern:CGFloat(1.0)
    ]
    
    // Live streams
    let livestreamsTitle = NSAttributedString(string: "Livestreams".localizedUppercase,
                                              attributes:attributes as? [NSAttributedStringKey : AnyObject])
    
    liveStreamsHeader.title.attributedText = livestreamsTitle
    liveStreamsHeader.title.textColor = MezzanineStyleSheet.shared().horizonBlueColorActive
    livestreamLabelLeftMargin.constant = 0.0

    
    // Portfolio items
    let itemsTitle = NSAttributedString(string: "Portfolio".localizedUppercase,
                                        attributes:attributes as? [NSAttributedStringKey : AnyObject])
    
    itemsHeader.title.attributedText = itemsTitle
    itemsHeader.title.textColor = MezzanineStyleSheet.shared().horizonBlueColorActive
    
    itemsHeader.indicator.transform = CGAffineTransform(rotationAngle: CGFloat(-M_PI));
    
    // Status message
    itemsHeader.statusMessage.font = UIFont.din(ofSize: 12.0)
    itemsHeader.statusMessage.textColor = MezzanineStyleSheet.shared().horizonBlueColorActive
    itemsHeader.statusMessage.alpha = 0.0
    
    // Initial set up
    if collectionViewHasLiveStreams {
      self.view.addSubview(liveStreamsHeader)
      itemsHeader.frame = itemsHeader.frame.offsetBy(dx: liveStreamsHeader.frame.maxX, dy: 0)
      self.view.addSubview(itemsHeader)
    } else {
      self.view.addSubview(itemsHeader)
    }
  }
  
  
  // MARK: Actions
  
  @IBAction func livestreamsTapAction(_ sender: AnyObject) {
    delegate?.binHeaderViewDidTapLivestreams()
  }
  
  @IBAction func itemsTapAction(_ sender: AnyObject) {
    delegate?.binHeaderViewDidTapItems()
  }
  
  
  // MARK: BincollectionViewControllerDelegate
  
  func binCollectionViewCellDidUpdateOrigin(_ collectionView: UICollectionView, scrollDirection: ScrollDirection) {
    addOrRemoveLiveStreamsIfNeeded()
    updateHeaders(collectionView, scrollDirection: scrollDirection)
  }
  
  func binCollectionViewDidUpdateTransferStatusLabel(_ visible: Bool, message: String, collectionView: UICollectionView, scrollDirection: ScrollDirection) {
    if !visible && itemsHeader.statusMessage.alpha == 0.0 {

      //FIXME - fix this issue with __once
      //_ = BinHeaderViewController.__once

      itemsHeader.statusMessageWidthConstraint.constant = 0.0
      itemsHeader.indicatorHorizontalSpaceConstraint.constant = 6.0
      itemsHeader.frame.size.width = itemsHeader.indicator.frame.maxX
      updateHeaders(collectionView, scrollDirection: scrollDirection)

      return
    }
    
    itemsHeader.statusMessage.alpha = visible ? 1.0 : 0.0
    if !visible {
      itemsHeader.statusMessageWidthConstraint.constant = 0.0
      itemsHeader.indicatorHorizontalSpaceConstraint.constant = 6.0
      updateHeaders(collectionView, scrollDirection: scrollDirection)
    } else {
      itemsHeader.statusMessage.text = message
      Static.dispatchOnceReLayoutAfterUploading = 0
      itemsHeader.statusMessage.sizeToFit()
      itemsHeader.indicatorHorizontalSpaceConstraint.constant = itemsHeader.statusMessage.frame.width + 20.0
    }
    
    itemsHeader.frame.size.width = itemsHeader.indicator.frame.maxX
  }
  
  
  // MARK: Private
  
  fileprivate func updateHeaders(_ collectionView: UICollectionView, scrollDirection: ScrollDirection) {
    
    if collectionViewHasLiveStreams {
      let liveStreamAttributes = collectionView.layoutAttributesForItem(at: IndexPath(item: 0, section: 0))
      let liveStreamCellFrame = collectionView.convert(liveStreamAttributes!.frame, to: collectionView.superview)
      updateLiveStreamsHeaderPosition(liveStreamCellFrame, direction: scrollDirection)
      
      let itemAttributes = collectionView.layoutAttributesForItem(at: IndexPath(item: 0, section: 1))
      let itemCellframe = collectionView.convert(itemAttributes!.frame, to: collectionView.superview)
      updateItemsHeaderPosition(itemCellframe, direction: scrollDirection)
    } else {
      let itemAttributes = collectionView.layoutAttributesForItem(at: IndexPath(item: 0, section: 0))
      let itemCellframe = collectionView.convert(itemAttributes!.frame, to: collectionView.superview)
      updateSingleHeaderPosition(itemCellframe, direction: scrollDirection)
    }
  }
  
  
  // MARK: Private
  
  fileprivate func addOrRemoveLiveStreamsIfNeeded() {
    if collectionViewHasLiveStreams {
      if !self.view.subviews.contains(liveStreamsHeader) {
        liveStreamsHeader.alpha = 0.0
        self.view.addSubview(liveStreamsHeader)
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .beginFromCurrentState, animations: {
          self.liveStreamsHeader.alpha = 1.0
          self.itemsHeader.frame = self.itemsHeader.frame.offsetBy(dx: self.liveStreamsHeader.frame.maxX, dy: 0)
          }, completion: nil)
      }
    } else {
      if self.view.subviews.contains(liveStreamsHeader) {
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .beginFromCurrentState, animations: {
          self.liveStreamsHeader.alpha = 0.0
          self.showSeparator(false)
          self.itemsHeader.frame.origin.x = self.headerMargin
          }, completion: { (finished) in
            self.liveStreamsHeader?.removeFromSuperview()
        })
      }
    }
  }
  
  fileprivate func showSeparator(_ show: Bool) {
    let alpha: CGFloat = !show ? 0.0 : 1.0
    if separatorView.alpha == alpha {
      return
    }
    
    separatorView.frame.origin.x = liveStreamsHeader.frame.maxX + headersHorizontalSpace/2 - separatorView.frame.width/2
    
    UIView.animate(withDuration: 0.33, delay: 0.0, options: .beginFromCurrentState, animations: {
      self.separatorView.alpha = alpha
      self.view.layoutIfNeeded()
      }, completion: nil)
  }
  
  fileprivate func showLivestreamIndicator(_ show: Bool) {
    let alpha: CGFloat = !show ? 0.0 : 1.0
    if liveStreamsHeader.indicator.alpha == alpha {
      return
    }
    
    UIView.animate(withDuration: 0.33, delay: 0.0, options: .beginFromCurrentState, animations: {
      self.liveStreamsHeader.indicator.alpha = !show ? 0.0 : 1.0
      self.livestreamLabelLeftMargin.constant = !show ? 0.0 : 12.0
      self.view.layoutIfNeeded()
      }, completion: nil)
    
  }
  
  fileprivate func showItemsIndicator(_ show: Bool) {
    let alpha: CGFloat = !show ? 0.0 : 1.0
    if itemsHeader.indicator.alpha == alpha {
      return
    }
    
    UIView.animate(withDuration: 0.33, delay: 0.0, options: .beginFromCurrentState, animations: {
      self.itemsHeader.indicator.alpha = !show ? 0.0 : 1.0
      self.view.layoutIfNeeded()
      }, completion: nil)
  }
  
  fileprivate func placeItemsHeaderAtMinimum() {
    itemsHeader.frame.origin.x = liveStreamsHeader.frame.maxX + headersHorizontalSpace
    showSeparator(true)
    showLivestreamIndicator(true)
  }
  
  fileprivate func placeItemsHeaderAtMaximum() {
    itemsHeader.frame.origin.x = self.view.frame.width - headerMargin - itemsHeader.frame.width
    showItemsIndicator(true)
  }
  
  fileprivate func updateLiveStreamsHeaderPosition(_ cellFrame: CGRect, direction: ScrollDirection) {
    let diffX = cellFrame.origin.x - liveStreamsHeader.frame.origin.x
    
    // Reached maximum + margin
    if itemsHeader.frame.minX - liveStreamsHeader.frame.maxX <= headersHorizontalSpace * 2
      && direction == .right {
      return
    }
    
    // Snap to first item
    if direction == .right && cellFrame.origin.x >= headerMargin {
      liveStreamsHeader.frame = liveStreamsHeader.frame.offsetBy(dx: diffX, dy: 0)
      return
    }
    
    // Snap to left edge + margins
    if liveStreamsHeader.frame.minX < headerMargin || abs(diffX) > liveStreamsHeader.frame.minX {
      liveStreamsHeader.frame.origin.x = headerMargin
      return
    }
    
    // Normal behaviour, scrolling
    if liveStreamsHeader.frame.minX != headerMargin
      && self.view.frame.intersection(liveStreamsHeader.frame).width == liveStreamsHeader.frame.width
      && liveStreamsHeader.frame.minX > headerMargin {
      if liveStreamsHeader.frame.origin.x < cellFrame.origin.x && direction == .left {
      } else {
        liveStreamsHeader.frame = liveStreamsHeader.frame.offsetBy(dx: diffX, dy: 0)
      }
      showSeparator(false)
    }
  }
  
  fileprivate func updateItemsHeaderPosition(_ cellFrame: CGRect, direction: ScrollDirection) {
    var diff = cellFrame.origin.x - itemsHeader.frame.origin.x
    
    // Initial position
    if direction == .none {
      // Sometimes visibleCells contains non visible cells... so we have to check widths to assure if it's actually shown or not
      if cellFrame.minX > self.view.frame.width {
        placeItemsHeaderAtMaximum()
        return
      }
      
      if cellFrame.minX < liveStreamsHeader.frame.maxX {
        placeItemsHeaderAtMinimum()
        return
      }
      
      itemsHeader.frame.origin.x = cellFrame.origin.x
      showItemsIndicator(false)
      showLivestreamIndicator(false)
      showSeparator(false)
      return
    }
    
    // Reached minimum, next to Livestream header
    if itemsHeader.frame.minX - liveStreamsHeader.frame.maxX <= headersHorizontalSpace && direction == .left {
      placeItemsHeaderAtMinimum()
      return
    }
    
    // Reached maximum, right edge
    if itemsHeader.frame.maxX >= (self.view.frame.width - headerMargin) && direction == .right  {
      placeItemsHeaderAtMaximum()
      return
    }
    
    // At minimum state, wait for cell to pass to snap to it
    if (itemsHeader.frame.maxX > headerMargin || abs(diff) > itemsHeader.frame.maxX)
      && itemsHeader.frame.minX - liveStreamsHeader.frame.maxX <= headersHorizontalSpace
    {
      if diff > 0 {
        itemsHeader.frame.origin.x = cellFrame.origin.x
        showSeparator(false)
        showLivestreamIndicator(false)
      }
      
      return
    }
    
    // Normal behaviour, scrolling
    if itemsHeader.frame.maxX !=  headerMargin
      && self.view.frame.intersection(itemsHeader.frame).width == itemsHeader.frame.width
      && self.view.frame.minX - itemsHeader.frame.maxX <= headerMargin {
      // At maximum state, wait to bounce back to snap it again
      if itemsHeader.frame.origin.x < cellFrame.origin.x && direction == .left {
        
      } else {
        let maxSpace = itemsHeader.frame.minX - liveStreamsHeader.frame.maxX
        diff = max(diff, -maxSpace)
        itemsHeader.frame = itemsHeader.frame.offsetBy(dx: diff, dy: 0)
        showItemsIndicator(false)
      }
      return
    }
  }
  
  func updateSingleHeaderPosition(_ cellFrame: CGRect, direction: ScrollDirection) {
    var headerView: UIView? = nil

    for view in self.view.subviews {
      if view is BinHeaderView {
        headerView = view
      }
    }
    
    if headerView == nil {
      return
    }
    
    let diffX = cellFrame.origin.x - headerView!.frame.origin.x

    // Snap to first item
    if direction == .right && cellFrame.origin.x >= headerMargin {
      headerView!.frame = headerView!.frame.offsetBy(dx: diffX, dy: 0)
      return
    }
    
    // Snap to left edge + margins
    if headerView!.frame.minX < headerMargin || abs(diffX) > headerView!.frame.minX {
      headerView!.frame.origin.x = headerMargin
      return
    }
    
    // Normal behaviour, scrolling
    if headerView!.frame.minX != headerMargin
      && self.view.frame.intersection(headerView!.frame).width == headerView!.frame.width
      && headerView!.frame.minX > headerMargin {
      if headerView!.frame.origin.x < cellFrame.origin.x && direction == .left {
      } else {
        headerView!.frame = headerView!.frame.offsetBy(dx: diffX, dy: 0)
      }
    }
  }
}
