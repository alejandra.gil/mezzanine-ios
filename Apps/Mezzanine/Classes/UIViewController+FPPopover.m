//
//  UIViewController+FPPopover.m
//  Mezzanine
//
//  Created by Zai Chang on 8/13/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import "UIViewController+FPPopover.h"
#import <objc/runtime.h>


@implementation UIViewController (FPPopover)

static char *kUIViewControllerFPPopoverKey = "UIViewControllerFPPopoverKey";


- (void)setCurrentPopover:(FPPopoverController *)popover
{
  FPPopoverController *currentPopover = [self currentPopover];

  objc_setAssociatedObject(self, kUIViewControllerFPPopoverKey, popover, OBJC_ASSOCIATION_RETAIN);

  if (currentPopover)
  {
    // Automatically dismiss previous popover
    [currentPopover dismissPopoverAnimated:NO];
  }
}


- (FPPopoverController*)currentPopover
{
  return objc_getAssociatedObject(self, kUIViewControllerFPPopoverKey);
}


-(void)popoverControllerDidDismissPopover:(FPPopoverController *)popoverController
{
  FPPopoverController *popover = [self currentPopover];
  if (popoverController == popover)
    [self setCurrentPopover:nil];
}


- (void)dismissPopoverIfContextMatches:(NSArray*)objects
{
  FPPopoverController *currentPopover = [self currentPopover];
  if (currentPopover && currentPopover.context)
  {
    for (id object in objects)
    {
      if (object == currentPopover.context)
        [currentPopover dismissPopoverAnimated:YES];
    }
  }
}


@end
