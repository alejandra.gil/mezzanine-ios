//
//  CornerBorderView.h
//  Mezzanine
//
//  Created by miguel on 18/07/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CornerBorderView : UIView

typedef NS_ENUM(NSUInteger, CornerBorderViewType)
{
  CornerBorderViewTypeTopLeft,
  CornerBorderViewTypeTopRight,
  CornerBorderViewTypeBottomLeft,
  CornerBorderViewTypeBottomRight
};

@property (nonatomic, assign) CornerBorderViewType cornerType;

@end
