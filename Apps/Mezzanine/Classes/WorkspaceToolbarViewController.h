//
//  WorkspaceToolbarViewController.h
//  Mezzanine
//
//  Created by Zai Chang on 1/22/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FPPopoverController.h"

@class MezzanineAppContext;

@protocol WorkspaceToolbarViewControllerDelegate <NSObject>

@required
- (void)workspaceToolbarOpenInfopresence;
- (void)workspaceToolbarOpenWorkspaceList;
- (void)workspaceToolbarCloseWorkspace;

// Overlay
- (void)workspaceToolbarDiscardWorkspaceOverlayMessage:(NSString *)message animated:(BOOL)animated;
- (void)workspaceToolbarHideWorkspaceOverlay:(BOOL)animated;

@end


@interface WorkspaceToolbarViewController : UIViewController<FPPopoverControllerDelegate>

@property (nonatomic, weak) id <WorkspaceToolbarViewControllerDelegate> delegate;

// Data Model
@property (nonatomic, strong) MezzanineAppContext *appContext;
@property (nonatomic, strong) MZCommunicator *communicator;
@property (nonatomic, strong) MZSystemModel *systemModel;

// MezzanineRoom
@property (strong, nonatomic) IBOutlet UIView *mezzanineRoomContainer;
@property (strong, nonatomic) IBOutlet UILabel *mezzanineRoomLabel;
@property (strong, nonatomic) IBOutlet UIButton *workspaceNameButton;
@property (strong, nonatomic) IBOutlet UIView *separator;
@property (strong, nonatomic) IBOutlet UIImageView *workspaceNameDownCaret;
- (IBAction)workspaceDetails:(id)sender;

// Leave button
@property (strong, nonatomic) IBOutlet UIButton *leaveButton;
- (IBAction)mezzanineDetails:(id)sender;

// Participants
@property (strong, nonatomic) IBOutlet UILabel *participantsCountLabel;
@property (strong, nonatomic) IBOutlet UIButton *participantsButton;
- (IBAction)participantsList:(id)sender;

@end
