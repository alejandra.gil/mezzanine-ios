//
//  AudioOnlyViewController.swift
//  Mezzanine
//
//  Created by Miguel Sánchez Valdés on 11/10/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

import UIKit

class AudioOnlyViewController: UIViewController, AudioMeterDelegate {

  @IBOutlet var audioOnlyTitleLabel: UILabel!
  @IBOutlet var audioMuteButton: MuteButton!
  @IBOutlet var audioMuteLabel: UILabel!
  @IBOutlet var leaveSessionButton: UIButton!
  @IBOutlet var audioVisualBubble: UIView!

  var communicator: MZCommunicator
  fileprivate var pexipManagerObservers = Array <String> ()
  fileprivate var audioMeter: AudioMeter!
  fileprivate var leaveAlertController: UIAlertController?

  var pexipManager: PexipManager! {
    willSet(newManager) {
      if (pexipManager != nil) {
        endObservingPexipManager()
      }
    }

    didSet {
      if (pexipManager != nil) {
        beginObservingPexipManager()
      }
    }
  }

  init(pexipManager: PexipManager, communicator: MZCommunicator) {
    self.pexipManager = pexipManager
    self.communicator = communicator
    super.init(nibName: "AudioOnlyViewController", bundle: nil)
    beginObservingPexipManager()
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  deinit {
    leaveAlertController?.dismiss(animated: false, completion: nil)
    leaveAlertController = nil
    print("AudioOnlyViewController deinit")
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    let sharedStyle = MezzanineStyleSheet.shared()

    self.view.backgroundColor = sharedStyle?.grey20Color
    
    audioVisualBubble.backgroundColor = MezzanineStyleSheet.shared().grey90Color
    audioVisualBubble.alpha = 0.2
    audioVisualBubble.layer.cornerRadius = audioVisualBubble.bounds.size.width/2
    audioVisualBubble.layer.masksToBounds = true
    
    audioOnlyTitleLabel.font = UIFont.dinLight(ofSize: 24)
    audioOnlyTitleLabel.textColor = sharedStyle?.textNormalColor
    
    audioMuteButton.type = .audioLarge
    audioMuteButton.showsTitle = false

    audioMuteLabel.font = UIFont.din(ofSize: 14)
    audioMuteLabel.textColor = sharedStyle?.grey190Color

    sharedStyle?.stylizeCall(toActionGrayButton: leaveSessionButton)
    leaveSessionButton .setTitle("Audio Only Leave Meeting".localized, for: UIControlState())
    leaveSessionButton.layer.cornerRadius = 3.0;
    leaveSessionButton.clipsToBounds = true;
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    audioMeter = AudioMeter(delegate: self)
    audioVisualBubble.isHidden = true
    updateAudioMuteButton()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    
    // Analytics
    let analyticsManager = MezzanineAnalyticsManager.sharedInstance
    analyticsManager.tagScreen(analyticsManager.audioOnlyViewScreen)
  }
  
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    audioMeter = nil
  }

  func updateAudioMuteButton() {
    audioMuteButton.isMuted = pexipManager.isAudioMuted()
    audioMuteLabel.text = "Audio Only Subitle".localized + (!audioMuteButton.isMuted ? "ON" : "OFF")
    view.bringSubview(toFront: audioMuteButton)
  }


  // MARK: UI Actions

  @IBAction func audioMute(_ sender: UIButton) {
    pexipManager.audioToggle()
    updateAudioMuteButton()
  }

  @IBAction func leaveSession(_ sender: UIButton) {

    var alertTitle: String
    var alertMessage: String
    var cancelButton: String
    var confirmButton: String

    if communicator.systemModel!.portfolioExport.isExporting() {
      alertTitle = "Disconnect While Exporting Dialog Title".localized
      alertMessage = "Disconnect While Exporting Dialog Message".localized
      cancelButton = "Generic Button Title Cancel".localized
      confirmButton = "Connection Lost Dialog Disconnect Action".localized
    }
    else {
      alertTitle = "Leave Meeting Dialog Title".localized
      alertMessage = "Leave Meeting Dialog Message".localized
      cancelButton = "Leave Meeting Dialog Cancel Button Title".localized
      confirmButton = "Leave Meeting Dialog Confirm Button Title".localized
    }

    let alertController = UIAlertController (title: alertTitle, message: alertMessage, preferredStyle: .alert)
    let settingsAction = UIAlertAction(title: confirmButton, style: .destructive) { (_) -> Void in
      self.performDisconnect()
    }

    let cancelAction = UIAlertAction(title: cancelButton, style: .cancel, handler: nil)
    alertController.addAction(settingsAction)
    alertController.addAction(cancelAction)

    self.present(alertController, animated: true, completion: nil)

    leaveAlertController = alertController
  }

  func performDisconnect() {
    leaveAlertController = nil

    if communicator.systemModel!.isSignedIn {
      communicator.requestor.requestClientSignOutRequest()
    }

    if self.communicator.isConnected {
      self.communicator.disconnect(withMessage: MezzanineAnalyticsManager.sharedInstance.audioOnlyLeaveAttribute)
    }
  }

  // MARK: PexipManager Observation

  func beginObservingPexipManager() {
    pexipManagerObservers.append(pexipManager.addObserver(forKeyPath: "conferenceState", options: [.new, .old], on: OperationQueue.main) { ( obj, change: [AnyHashable: Any]!) in

      let newStateRawrawValue = Int(change[NSKeyValueChangeKey.newKey] as! NSNumber)

      switch PexipConferenceState(rawValue:newStateRawrawValue)! {
      case .disconnected:
        // TODO: Show a Disconnected overlay view or similar
        break
      case .connected:
        if self.pexipManager.conferenceMode == .audioOnly {
          self.updateAudioMuteButton()
        }
        break
      default:
        break
      }
    })
  }

  func endObservingPexipManager() {
    for token in pexipManagerObservers {
      pexipManager.removeObserver(withBlockToken: token)
    }
    pexipManagerObservers.removeAll()
  }
  
  
  // MARK: AudioMeterDelegate
  func audioMeterDidUpdateValue(_ value: CGFloat) {
    // Safe checks
    if audioMeter == nil || pexipManager == nil {
      audioVisualBubble.isHidden = true
      return
    }
    
    // If somehow we can't get the session, hide the bubble
    do {
      try self.audioMeter?.audioSession.setActive(true)
      audioVisualBubble.isHidden = false
    } catch {
      audioVisualBubble.isHidden = true
    }

    // If it's muted or we still not in the conference
    if pexipManager.isAudioMuted() || pexipManager.conferenceState != .connected {
      audioVisualBubble.isHidden = true
      return
    }
    
    var animationDuration = 0.05
    
    if  value == 1.0 {
      animationDuration = 0.2
    }
    
    UIView.animate(withDuration: animationDuration, delay: 0, options: .beginFromCurrentState, animations: {
      self.audioVisualBubble.transform = CGAffineTransform(scaleX: value, y: value)
      }, completion: nil)
  }
}

extension UIAlertController {
  open override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
    return self.traitCollection.isiPad() ? .all : .allButUpsideDown
  }

  open override var shouldAutorotate : Bool {
    return true
  }
}
