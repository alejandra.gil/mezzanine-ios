//
//  AssetViewController.h
//  Mezzanine
//
//  Created by Zai Chang on 3/7/11.
//  Copyright 2011 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MezzanineAppContext.h"
#import "AnnotationsContainerLayer.h"
#import "VideoPlaceholderView.h"

@class PresentationViewController;

/// Controller for inspecting an asset from windshield or portfolio

@interface AssetViewController : UIViewController <UIScrollViewDelegate, FPPopoverControllerDelegate, UIDocumentInteractionControllerDelegate, UIGestureRecognizerDelegate>

@property (nonatomic, weak) MezzanineAppContext *appContext;

@property (nonatomic, strong) MZItem *item;
@property (nonatomic, strong) MZWorkspace *workspace;
@property (nonatomic, weak) PresentationViewController *presentationViewController;

@property (nonatomic, strong) IBOutlet UIImageView *placeholderImageView;
@property (nonatomic, strong) IBOutlet VideoPlaceholderView *videoPlaceholderView;
@property (nonatomic, strong) IBOutlet UIToolbar *toolbar;
@property (nonatomic, strong) IBOutlet UIView *progressView;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *closeItem;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *titleItem;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *optionsItem;

@property (nonatomic, strong) IBOutlet UIToolbar *bottomToolbar;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *drawOrNavigationModeItem;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *colourPickerItem;
@property (nonatomic, strong) IBOutlet UIButton *colourPickerButton;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *undoItem;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *redoItem;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *clearItem;

@property (nonatomic, strong) UIImage *placeholderImage;
@property (nonatomic, assign) BOOL isLoadingContent;

@property (nonatomic, strong) UIColor *currentColor;

@property (nonatomic, strong) IBOutlet UILabel *loadingLabel;


-(IBAction) showOptions;
-(IBAction) close;


// Annotation Actions
-(IBAction) toggleDrawMode:(id)sender;
-(IBAction) showColourPicker:(id)sender;
-(IBAction) undo:(id)sender;
-(IBAction) redo:(id)sender;
-(IBAction) clear:(id)sender;

@end
