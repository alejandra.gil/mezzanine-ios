//
//  MZSystemModel+InterfaceTesting.h
//  Mezzanine
//
//  Created by Zai Chang on 1/14/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import "MZSystemModel.h"

@interface MZSystemModel (InterfaceTesting)

// Common test setup
-(void) loadTestFeldSingle;
-(void) loadTestFeldsDouble;
-(void) loadTestFeldsTriple;
-(void) loadTestFeldsThreeByTwo;
-(void) loadWhiteboards:(NSInteger)numberOfWhiteboards;

-(void) loadWorkspaces;
-(void) loadTestWorkspace;

@end
