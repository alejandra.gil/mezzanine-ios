//
//  OBUmbilicalLayer.h
//  Mezzanine
//
//  Created by Zai Chang on 3/13/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OBUmbilicalLayer : CAShapeLayer
{
  CGFloat amplitude;
  CGFloat phase;
  
  CAShapeLayer *originDotLayer;
  NSTimer *phaseTimer;
}

@property (nonatomic, strong) CALayer *sourceLayer;
@property (nonatomic, strong) CALayer *targetLayer;

@property (nonatomic, assign) CGFloat amplitude;
@property (nonatomic, assign) CGFloat phase;

-(void) beginPhaseTimer;
-(void) stopPhaseTimer;

@end
