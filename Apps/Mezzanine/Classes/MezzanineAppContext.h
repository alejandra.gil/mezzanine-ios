//
//  MezzanineAppContext.h
//  Mezzanine
//
//  Created by Zai Chang on 1/26/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import "MezzanineConnectionViewController.h"
#import "UIProgressViewController.h"
#import "MezzanineAppModel.h"
#import "MZCommunicator.h"
#import "MZAssetCache.h"
#import "PassphraseViewController.h"
#import "MezzanineDocumentInteractionController.h"
#import "PopupInfoView.h"
#import "MezzanineExportManager.h"
#import "MezzanineInfopresenceManager.h"
#import "MezzanineNavigationController.h"

// iOS 8 Cache folder Fix
#import "DirectoryWatcher.h"

#define MEZZANINE_MINIMUM_NATIVE_VERSION @"2.0"

#define USE_POPOVERS_IN_IPHONE 0
#define DISCONNECT_IN_BACKGROUND 1

#define DRAG_DROP_WINDSHIELD_HIGHLIGHT_ABOVE 1  // Show above the windshield
#define DRAG_DROP_HIGHLIGHT_MODE_LIGHT 1
#define DRAG_DROP_HIGHLIGHT_MODE_DARK  !DRAG_DROP_HIGHLIGHT_MODE_LIGHT


//#define MezzanineAppContextPassphraseViewPresentedNotification @"PassphraseViewPresented"

@class AnalyticsViewController;
@class PexipManager;
@class RemoteParticipationJoinViewController;
@class MezzanineMailComposerController;
@class SystemUseNotificationViewController;
@class PasskeyCoordinator;

@interface MezzanineAppContext : NSObject <UINavigationControllerDelegate, DirectoryWatcherDelegate>
{
  NSMutableArray *communicatorObservationTokens;
  NSMutableArray *mezzanineObservationTokens;
}

+(MezzanineAppContext*) currentContext;
+(void) setCurrentContext:(MezzanineAppContext*)context;

// Windows
@property (nonatomic, strong) UIWindow *window;

// Application States
@property (nonatomic, assign) BOOL inBackground;
@property (nonatomic, strong) NSDate *enteredBackgroundDate;
@property (nonatomic, assign) BOOL attemptingToReconnect;
@property (nonatomic, assign) BOOL attemptingToConnectWithPassphrase;

// Communicator
@property (nonatomic, strong) MZCommunicator *currentCommunicator;

// Model
@property (nonatomic, strong) MezzanineAppModel *applicationModel;

// Cache
@property (nonatomic, strong) MZAssetCache *assetCache;  // Disk cache for mezzanine assets

// Document interaction
@property (nonatomic, strong) MezzanineDocumentInteractionController *documentInteractionController;
@property (nonatomic, strong) MezzanineExportManager *exportManager;

// View Controllers
@property (nonatomic, strong) MezzanineNavigationController *mainNavController;
@property (nonatomic, strong) MezzanineConnectionViewController *connectionViewController;
@property (nonatomic, strong) UIProgressViewController *progressViewController;
@property (nonatomic, strong) UIWindow *popupInfoWindow;
@property (nonatomic, strong) PopupInfoView *popupInfoView;
@property (nonatomic, strong) UIWindow *progressWindow;
@property (nonatomic, strong) PassphraseViewController *passphraseViewController;
@property (nonatomic, strong) AnalyticsViewController *analyticsViewController;
@property (nonatomic, strong) MezzanineMailComposerController *mailComposer;
@property (nonatomic, strong) SystemUseNotificationViewController *systemUseNotificationViewController;

// Coordinators
@property (nonatomic, strong) PasskeyCoordinator *passkeyCoordinator;

// Infopresence (3.0)
@property (nonatomic, strong) MezzanineInfopresenceManager *infopresenceManager;

// Remote Participation
@property (nonatomic, strong) PexipManager *pexipManager;
@property (nonatomic, strong) RemoteParticipationJoinViewController* remoteParticipationJoinViewController;

-(void) setupDragDropManager;

-(void)setupInfopresenceOverlay;

-(void) initPopupInfoView;

-(void) initProgressView;
-(void) showProgressViewWithTitle:(NSString*)title indeterminate:(BOOL)indeterminate;
-(void) hideProgressView;

-(void) beginCacheCleanupOperation;
-(void) cleanupInbox;

-(void) cleanupExportedDocuments;
-(void) cleanupOrLeaveCacheForURL:(NSURL *)url;


// Offline Testing
-(void) loadTestWorkspaceSingleFeld;
-(void) loadTestWorkspaceDiptych;
-(void) loadTestWorkspaceTriptych;
-(void) loadTestWorkspaceThreeByTwo;
-(void) loadPassphraseView;

@end
