//
//  WorkspaceListCollectionViewLayout.m
//  Mezzanine
//
//  Created by miguel on 10/03/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import "WorkspaceListCollectionViewLayout.h"
#import "UITraitCollection+Additions.h"

@implementation WorkspaceListCollectionViewLayout

-(id)init
{
  self = [super init];
  if (self)
  {
    insertedIndexPaths = [[NSMutableArray alloc] init];
    deletedIndexPaths = [[NSMutableArray alloc] init];
  }
  return self;
}


- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)oldBounds
{
  return YES;
}


- (void)prepareForCollectionViewUpdates:(NSArray *)updates
{
  [super prepareForCollectionViewUpdates:updates];
  for (UICollectionViewUpdateItem* updateItem in updates) {
    if (updateItem.updateAction == UICollectionUpdateActionInsert) {
      [insertedIndexPaths addObject:updateItem.indexPathAfterUpdate];
    }
    else if (updateItem.updateAction == UICollectionUpdateActionDelete) {
      [deletedIndexPaths addObject:updateItem.indexPathBeforeUpdate];
    }
  }
}


- (void)finalizeCollectionViewUpdates
{
  [insertedIndexPaths removeAllObjects];
  [deletedIndexPaths removeAllObjects];
}


- (void)prepareLayout
{
  // Width is dynamically calculated at 'collectionView:layout:sizeForItemAtIndexPath:'
  // Giving a default zero width throws a warning
  self.itemSize = CGSizeMake(1.0, 80.0);
  
  if (self.collectionView.superview.window.traitCollection.isiPadRegularWidth)
  {
  self.sectionInset = UIEdgeInsetsMake(20.0, 20.0, 20.0, 20.0);
  self.minimumLineSpacing = 10.0;
  self.minimumInteritemSpacing = 20.0;
  } else
  {
    self.sectionInset = UIEdgeInsetsMake(10.0, 10.0, 10.0, 10.0);
    self.minimumLineSpacing = 5.0;
    self.minimumInteritemSpacing = 0.0;
  }
}


- (CGPoint)targetContentOffsetForProposedContentOffset:(CGPoint)proposedContentOffset withScrollingVelocity:(CGPoint)velocity
{
  CGFloat offsetAdjustment = MAXFLOAT;

  CGFloat viewWidth = CGRectGetWidth(self.collectionView.bounds);
  CGFloat viewHeight = CGRectGetHeight(self.collectionView.bounds);
  CGFloat contentHeight = self.collectionView.collectionViewLayout.collectionViewContentSize.height;

  // Full top alignment of the collection view
  if (contentHeight < viewHeight)
  {
    return CGPointMake(proposedContentOffset.x, 0);
  }

  // Full bottom alignment of the collection view
  CGFloat highCheckPoint = contentHeight - CGRectGetHeight(self.collectionView.bounds) / 2.0;
  if (proposedContentOffset.y + CGRectGetHeight(self.collectionView.bounds) / 2.0 >= highCheckPoint)
  {
    return CGPointMake(proposedContentOffset.x, highCheckPoint);
  }

  // Rest of the cases
  CGRect targetRect = CGRectMake(proposedContentOffset.x, proposedContentOffset.y, viewWidth, viewHeight);
  NSArray* array = [super layoutAttributesForElementsInRect:targetRect];

  for (UICollectionViewLayoutAttributes* layoutAttributes in array)
  {
    CGFloat itemVerticalOrigin = layoutAttributes.frame.origin.y - self.sectionInset.top + self.minimumLineSpacing;
    if (ABS(itemVerticalOrigin - proposedContentOffset.y) < ABS(offsetAdjustment))
    {
      offsetAdjustment = itemVerticalOrigin - proposedContentOffset.y;
    }
  }
  return CGPointMake(proposedContentOffset.x, proposedContentOffset.y +  + offsetAdjustment);
}

@end
