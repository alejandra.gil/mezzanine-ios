//
//  MezzanineRootViewController.swift
//  Mezzanine
//
//  Created by miguel on 6/10/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

import UIKit

class MezzanineRootViewController: UIViewController, WorkspaceToolbarViewControllerDelegate, InfopresenceMenuControllerDelegate, WorkspaceListViewControllerDelegate, OBDropZone, MezzanineMainViewControllerDelegate, NSUserActivityDelegate, UINavigationBarDelegate {

  struct Constants {
    fileprivate static let kWorkspaceListTransitionAnimationDuration = 0.3
    fileprivate static let kWorkspaceBlurredViewAnimationDuration = 0.5
    fileprivate static let kWorkspaceOverlaysAnimationDuration = 0.2
  }

  // Outlets
  @IBOutlet var navigationBar: UINavigationBar!
  @IBOutlet var sessionContainerView: UIView!
  @IBOutlet var bottomToolbarConstraint: NSLayoutConstraint!

  // Controllers
  @objc let workspaceToolbarViewController = WorkspaceToolbarViewController()
  @objc var mezzanineMainViewController: MezzanineBaseRoomViewController = SettingsFeatureToggles.sharedInstance.teamworkUI == false ? MezzanineMainViewController() : MezzanineRoomViewController()
  @objc var mezzanineSessionViewController: MezzanineSessionViewController?
  @objc var audioOnlyViewController: AudioOnlyViewController?
  @objc var workspaceListViewController: WorkspaceListViewController?

  // Managers
  @objc var infopresenceMenuController: InfopresenceMenuController?

  // Privates
  fileprivate var currentAlertController: UIAlertController?
  fileprivate var toolbarVisible = true
  fileprivate var nextTraitCollection: UITraitCollection?
  internal var dragDropDeletionArea: UIView?
  fileprivate var currentDragDropRecognizer: UIGestureRecognizer?
  fileprivate var showStatusBar = true {
    didSet {
      UIView.animate(withDuration: 0.2, animations: { self.setNeedsStatusBarAppearanceUpdate()}) 
    }
  }
  
  @objc var initAudioOnlySession = false

  @objc var appContext: MezzanineAppContext! {
    didSet {
      workspaceToolbarViewController.appContext = appContext
      mezzanineMainViewController.appContext = appContext
    }
  }

  @objc var communicator: MZCommunicator! {
    didSet {
      workspaceToolbarViewController.communicator = communicator
      mezzanineMainViewController.communicator = communicator
    }
  }

  @objc var systemModel: MZSystemModel! {
    didSet {
      workspaceToolbarViewController.systemModel = systemModel
      mezzanineMainViewController.systemModel = systemModel
    }
  }

  deinit {
    print ("MezzanineRootViewController is being deinitialized")
    workspaceToolbarViewController.delegate = nil
    mezzanineSessionViewController?.communicator = nil
    mezzanineSessionViewController?.pexipManager = nil
    audioOnlyViewController?.pexipManager = nil
    self.userActivity?.invalidate()
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    navigationBar.delegate = self

    // To prevent slides outside the bounds to be drawn during view transitions
    // see bug 2675
    self.view.clipsToBounds = true

    let styleSheet = MezzanineStyleSheet.shared()

    // top and bottom safe areas have different colours
    self.view.backgroundColor = MezzanineStyleSheet.shared().darkestFillColor

    workspaceToolbarViewController.delegate = self
    workspaceToolbarViewController.appContext = appContext // TODO: Refactor this setter on WorkspaceToolbarViewController to set in cascade and avoid the other two setters
    workspaceToolbarViewController.communicator = communicator  // communicator needs to come before systemModel because initial infopresence icon state depends on it
    workspaceToolbarViewController.systemModel = systemModel

    workspaceToolbarViewController.view.layer.backgroundColor = styleSheet?.grey50Color.cgColor
    workspaceToolbarViewController.view.layer.borderColor = UIColor(white:1.0, alpha:0.8).cgColor
//  TOOD:   workspaceToolbarViewController.view.dropZoneHandler = self// Use entire header for expose handling?   <---- TO BE CLARIFIED

    addChildViewController(workspaceToolbarViewController)

    // Dirty trick to add the workspace toolbar, needs a better solution
    let toolbarView = workspaceToolbarViewController.view
    toolbarView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    toolbarView?.frame = navigationBar.bounds
    navigationBar.addSubview(toolbarView!)

    workspaceToolbarViewController.didMove(toParentViewController: self)

    mezzanineMainViewController.appContext = appContext
    mezzanineMainViewController.communicator = communicator
    mezzanineMainViewController.systemModel = systemModel
    mezzanineMainViewController.delegate = self

    if (communicator.isARemoteParticipationConnection) {
      audioOnlyViewController = AudioOnlyViewController(pexipManager: appContext.pexipManager, communicator: communicator)
    }

    mezzanineSessionViewController = MezzanineSessionViewController(pexipManager: appContext.pexipManager, communicator: communicator, mezzanineMainViewController: mezzanineMainViewController, audioOnlyViewController: audioOnlyViewController, initAudioOnlySession:initAudioOnlySession)

    if let mezzanineSessionViewController = mezzanineSessionViewController {
      addChildViewController(mezzanineSessionViewController)
      mezzanineSessionViewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
      mezzanineSessionViewController.view.frame = sessionContainerView.bounds
      sessionContainerView.addSubview(mezzanineSessionViewController.view)
      mezzanineSessionViewController.didMove(toParentViewController: self)
    }

    let connectedActivity = NSUserActivity(activityType: MezzanineUserActivityTypes.connected)
    connectedActivity.title = "Connected"
    connectedActivity.webpageURL = communicator.baseURLForHttpRequests()
    connectedActivity.becomeCurrent()
    connectedActivity.delegate = self
    self.userActivity = connectedActivity
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    if !self.navigationController!.isNavigationBarHidden {
      self.navigationController?.setNavigationBarHidden(true, animated: true)
    }

    updateAdaptativeLayout(self.traitCollection)

    // Feedback for when drag drop is in progress
    NotificationCenter.default.addObserver(self, selector: #selector(dragDropManagerWillBeginDrag(_:)), name: NSNotification.Name(rawValue: OBDragDropManagerWillBeginDragNotification), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(dragDropManagerDidEndDrag(_:)), name: NSNotification.Name(rawValue: OBDragDropManagerDidEndDragNotification), object: nil)
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)

    currentAlertController?.dismiss(animated: true, completion: nil)
    currentAlertController = nil

    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: OBDragDropManagerWillBeginDragNotification), object: nil)
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: OBDragDropManagerDidEndDragNotification), object: nil)
  }
  
  override var preferredStatusBarStyle : UIStatusBarStyle {
    return .lightContent
  }

  override var prefersStatusBarHidden : Bool {
    return !showStatusBar
  }
  
  override var preferredStatusBarUpdateAnimation : UIStatusBarAnimation {
    return .fade
  }
    
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    bottomToolbarConstraint.constant = toolbarVisible ? 0.0 : -navigationBar.frame.size.height
  }


  // MARK: Layout components
  func setToolbarVisible(_ visible: Bool) {
    toolbarVisible = visible
    showStatusBar = toolbarVisible
  }


  // MARK: Adaptative Layout

  override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
    return self.traitCollection.isiPad() ? .all : .allButUpsideDown
  }

  override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
    super.willTransition(to: newCollection, with: coordinator)
    nextTraitCollection = newCollection
    updateAdaptativeLayout(nextTraitCollection!)
    infopresenceMenuController?.hideInfopresenceMenu()
  }

  override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
    super.viewWillTransition(to: size, with: coordinator)

    if nextTraitCollection != nil {
      return
    }

    updateAdaptativeLayout(self.traitCollection)
    infopresenceMenuController?.hideInfopresenceMenu()
  }

  override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
    super.traitCollectionDidChange(previousTraitCollection)
    nextTraitCollection = nil
  }

  func updateAdaptativeLayout(_ traitCollection: UITraitCollection) {
    traitCollection.isCompactHeight() ? setToolbarVisible(false) : setToolbarVisible(true)
  }


  // MARK - WorkspaceToolbarViewControllerDelegate

  func workspaceToolbarOpenInfopresence() {
    if infopresenceMenuController == nil {
      infopresenceMenuController = InfopresenceMenuController(containerViewController: self)
      infopresenceMenuController!.systemModel = systemModel
      infopresenceMenuController!.communicator = communicator
      infopresenceMenuController!.delegate = self
    }
    infopresenceMenuController!.showMenuViewController()
  }

  func workspaceToolbarOpenWorkspaceList() {
    if workspaceListViewController != nil {
      return
    }

    workspaceListViewController = WorkspaceListViewController.init()
    workspaceListViewController!.appContext = appContext
    workspaceListViewController!.communicator = communicator
    workspaceListViewController!.systemModel = systemModel
    workspaceListViewController!.delegate = self
    
    let navigationController = MezzanineNavigationController(rootViewController: workspaceListViewController!)
    if self.traitCollection.isiPad() {
      navigationController.modalPresentationStyle = .formSheet
    }
    self.present(navigationController, animated: true, completion: nil)
  }

  func workspaceToolbarCloseWorkspace() {
    let alertTitle = "Workspace Close Dialog Title".localized
    let alertMessage = "Workspace Close Dialog Message".localized

    let alertController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)

    let confirmAction = UIAlertAction(title: "Workspace Close Dialog Confirm Button Title".localized, style: .default) { [unowned self](_) -> Void in
      self.closeWorkspace()
    }

    let cancelAction = UIAlertAction(title: "Workspace Close Dialog Cancel Button Title".localized, style: .cancel) { [unowned self](_) -> Void in
      self.currentAlertController = nil
    }

    alertController.addAction(confirmAction)
    alertController.addAction(cancelAction)

    self.present(alertController, animated: true, completion: nil)
    currentAlertController = alertController

  }

  func closeWorkspace() {
    let transaction = communicator.requestor.requestWorkspaceCloseRequest(systemModel.currentWorkspace.uid)
    transaction?.beganBlock = {
      self.mezzanineMainViewController.showWorkspaceOverlay(withMessage: "Workspace Closing".localized, animated: true)
    }
    transaction?.endedBlock = {
      self.mezzanineMainViewController.hideWorkspaceOverlayView(animated: true)
    }
    currentAlertController = nil
  }

  func workspaceToolbarDiscardWorkspaceOverlayMessage(_ message: String, animated:Bool) {
    mezzanineMainViewController.showWorkspaceOverlay(withMessage: message, animated:animated)
  }

  func workspaceToolbarHideWorkspaceOverlay(_ animated: Bool) {
    mezzanineMainViewController.hideWorkspaceOverlayView(animated: animated)
  }


  // MARK: InfopresenceMenuControllerDelegate

  // TODO: Clean up if there are no complaints about showing the status bar when infopresence slide panel
  func infopresenceMenuControllerWillPresentSlidingPanelWithController(_ controller: UIViewController) {
    //showStatusBar = false
  }
  
  func infopresenceMenuControllerWillDismiss(_ infopresenceMenuController: InfopresenceMenuController) {
    //showStatusBar = true && toolbarVisible
  }
  
  func infopresenceMenuControllerWillPushViewController(_ controller: UIViewController) {
    //showStatusBar = true
  }
  
  func infopresenceMenuControllerWillPopViewController() {
    //showStatusBar = false
  }
  

  
  func infopresenceMenuControllerDidDismiss(_ controller: InfopresenceMenuController) {
    infopresenceMenuController!.delegate = nil
    infopresenceMenuController!.systemModel = nil
    infopresenceMenuController!.communicator = nil
    infopresenceMenuController = nil
    
    // Analytics
    let analyticsManager = MezzanineAnalyticsManager.sharedInstance
    analyticsManager.tagWorkspaceStatusScreen()
  }


  // MARK: WorkspaceList Delegate

  func dismiss(_ controller: WorkspaceListViewController) {
    controller.dismiss(animated: true) {
      self.workspaceListViewController = nil
    }
  }

  
  // MARK: OBDropZone

  @objc func dragDropManagerWillBeginDrag(_ notification: Notification) {
    let isIPad = UIDevice.current.isIPad()
    let backgroundColor = MezzanineStyleSheet.shared().grey60Color

    if dragDropDeletionArea == nil {

      let textColor = UIColor.red

      var frame = CGRect(x:0, y:0, width:navigationBar.frame.width, height:navigationBar.frame.maxY).insetBy(dx: -1.0, dy: -1.0)
      frame.origin.y -= frame.size.height  // Hidden by default
      frame.size.height -= 1
      dragDropDeletionArea = UIView.init(frame: frame)
      dragDropDeletionArea!.dropZoneHandler = self
      dragDropDeletionArea!.autoresizingMask = [.flexibleWidth, .flexibleBottomMargin]
      dragDropDeletionArea!.backgroundColor = backgroundColor
      dragDropDeletionArea!.layer.borderColor = textColor.cgColor
      dragDropDeletionArea!.layer.borderWidth = 1.0
      dragDropDeletionArea!.layer.zPosition = workspaceToolbarViewController.view.layer.zPosition + 2.0 // To appear above paramus, and more importantly windshield

      let background = UIView.init(frame: CGRect(x:0, y:0, width:navigationBar.frame.width, height:navigationBar.frame.maxY))
      background.autoresizingMask = [.flexibleWidth, .flexibleBottomMargin]
      background.backgroundColor = UIColor.init(red: 0.22, green: 0.12, blue: 0.12, alpha: 1.0)
      dragDropDeletionArea!.addSubview(background)

      let label = UILabel.init(frame: CGRect(x:0, y:navigationBar.frame.origin.y, width:navigationBar.frame.width, height:navigationBar.frame.height))
      label.autoresizingMask = [.flexibleWidth, .flexibleTopMargin]
      label.backgroundColor = UIColor.clear
      label.font = isIPad ? UIFont.dinBold(ofSize: 16.0) : UIFont.dinBold(ofSize: 13.0)
      label.textColor = textColor
      label.textAlignment = .center
      label.text = "Drop Bar Message".localized
      dragDropDeletionArea!.addSubview(label)

      self.view.addSubview(dragDropDeletionArea!)
    }

    let ovum = notification.userInfo![OBOvumDictionaryKey] as! OBOvum
    currentDragDropRecognizer = notification.userInfo![OBGestureRecognizerDictionaryKey] as? UIGestureRecognizer

    let ovumIsFromBins = ovum.source.isKind(of: PortfolioViewController.self) || ovum.source.isKind(of: LiveStreamsViewController.self)
    let ovumIsVideoSource = (ovum.dataObject as AnyObject).isKind(of: MZLiveStream.self)
    let allowDeletion = !(ovumIsFromBins && ovumIsVideoSource)

    presentDragDropDeletionAreaWithDeletion(allowDeletion)
  }

  @objc func dragDropManagerDidEndDrag(_ notification :Notification) {
    hideDragDropDeletionArea()
    currentDragDropRecognizer = nil
  }

  func presentDragDropDeletionAreaWithDeletion(_ allowDeletion: Bool) {
    UIView.animate(withDuration: Constants.kWorkspaceOverlaysAnimationDuration, delay: 0.0, options: [.beginFromCurrentState, .curveEaseOut], animations: {
      if allowDeletion {
        var frame = self.dragDropDeletionArea!.frame
        frame.origin.y = -self.dragDropDeletionArea!.layer.borderWidth
        self.dragDropDeletionArea!.frame = frame
      }
    }, completion: nil)
  }

  func hideDragDropDeletionArea() {
    UIView.animate(withDuration: Constants.kWorkspaceOverlaysAnimationDuration, delay: 0.0, options: [.beginFromCurrentState, .curveEaseIn], animations: {
      var frame = self.dragDropDeletionArea!.frame
      frame.origin.y = -frame.size.height
      self.dragDropDeletionArea!.frame = frame
    }, completion: nil)
  }


  func ovumDropped(_ ovum: OBOvum!, in view: UIView!, atLocation location: CGPoint) {
    if view == dragDropDeletionArea {
      if ovum.dataObject! is MZWindshieldItem {
        if mezzanineMainViewController is MezzanineMainViewController {
          mezzanineMainViewController.workspaceViewController.windshieldViewController.requestDelete(ovum.dataObject! as! MZWindshieldItem)
        } else {
          communicator.requestor.requestWindshieldItemDeleteRequest(systemModel.currentWorkspace.uid, uid: (ovum.dataObject! as! MZWindshieldItem).uid)
        }
      }
      else if ovum.dataObject! is MZPortfolioItem {
        communicator.requestor.requestPortfolioItemDeleteRequest(systemModel.currentWorkspace.uid, uid:(ovum.dataObject! as! MZPortfolioItem).uid)
      }
    }
  }

  func ovumEntered(_ ovum: OBOvum!, in view: UIView!, atLocation location: CGPoint) -> OBDropAction {
    NSObject.cancelPreviousPerformRequests(withTarget: self)

    ovum.adorn(withDeletionMode: view == dragDropDeletionArea)

    if view == dragDropDeletionArea {
      return .delete
    }

    return .none
  }

  func ovumMoved(_ ovum: OBOvum!, in view: UIView!, atLocation location: CGPoint) -> OBDropAction {
    return ovum.dropAction
  }

  func ovumExited(_ ovum: OBOvum!, in view: UIView!, atLocation location: CGPoint) {
    if view == dragDropDeletionArea {
      ovum.adorn(withDeletionMode: false)
    }

    NSObject.cancelPreviousPerformRequests(withTarget: self)
  }

  func handleDropAnimation(for ovum: OBOvum!, withDrag dragView: UIView!, dragDropManager: OBDragDropManager!) {
    if ovum.dropAction == .delete {

      dragDropManager.animateOvumDrop(ovum, withAnimation: { 
        dragView.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
        dragView.alpha = 0.0
        }, completion: nil)
    }
    else {
      // Empty, only to bypass OBDragDropManager's default drop animation (and thus removing the view from existance)
      dragDropManager.animateOvumDrop(ovum, withAnimation: {
        // Do Nothing but let OBDragDropManager hide its overlayWindow
        }, completion: nil)
    }
  }


  // MARK: MezzanineMainViewControllderDelegate

  func mezzanineMainViewControllerDidStartDraggingWindshieldItem(_ mezzanineVC: MezzanineMainViewController!) {
    presentDragDropDeletionAreaWithDeletion(true)
  }

  func mezzanineMainViewControllerDidStopDraggingWindshieldItem(_ mezzanineVC: MezzanineMainViewController!) {
    hideDragDropDeletionArea()
  }


  // MARK: NSUserActivityDelegate

  func userActivityWasContinued(_ userActivity: NSUserActivity) {
    let analyticsManager = MezzanineAnalyticsManager.sharedInstance
    analyticsManager.tagEvent("handoffInit")

    DispatchQueue.main.async {
      let sessionHandoff = SessionHandoffViewController(communicator: self.communicator)
      sessionHandoff.modalPresentationStyle = .overCurrentContext
      self.present(sessionHandoff, animated: false, completion: nil)
    }
  }

  // MARK: Navigation bar delegate

  func position(for bar: UIBarPositioning) -> UIBarPosition {
    return .topAttached
  }
}
