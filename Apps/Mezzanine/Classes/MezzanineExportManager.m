//
//  MezzanineExportManager.m
//  Mezzanine
//
//  Created by Miguel Sánchez on 16/01/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import "MezzanineExportManager.h"
#import "MezzanineExportManager_Private.h"
#import "MezzanineStyleSheet.h"
#import "NSObject+UIAlertController.h"

#define kExportInfoContext @"kExportInfoContext"

@implementation MezzanineExportManager

- (instancetype)init
{
  self = [super init];
  if (self)
  {
    _filesToPreview = [[NSMutableArray alloc] init];
    _exportCompletedInfo = [[NSMutableArray alloc] init];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:UIDeviceOrientationDidChangeNotification object:nil queue:[NSOperationQueue new] usingBlock:^(NSNotification * _Nonnull note) {
      if (_openInDocInteractionController) {
        _openInDocInteractionController = nil;
        [self presentAlertForExportCompleted];
      }
    }];
  }
  
  return self;
}

- (void)checkPendingCompletedExports
{
  if ([_exportCompletedInfo count] > 0)
    [self presentAlertForExportCompleted];
}

#pragma mark - Portfolio ExportInfo Observation

- (void)setPortfolioExportInfo:(MZExportInfo *)portfolioExportInfo
{
  if (_portfolioExportInfo != portfolioExportInfo)
  {
    if (_portfolioExportInfo)
    {
      [_portfolioExportInfo removeObserver:self forKeyPath:@"state"];
    }

    _portfolioExportInfo = portfolioExportInfo;

    if (_portfolioExportInfo)
    {
      [_portfolioExportInfo addObserver:self forKeyPath:@"state" options:NSKeyValueObservingOptionNew context:kExportInfoContext];
    }
  }
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
  if (context == kExportInfoContext)
  {
    MZExportInfo* exportInfo = (MZExportInfo *)object;
    MZExportState newState = [change[NSKeyValueChangeNewKey] integerValue];
    if (newState == MZExportStateReady)
    {
      [self addNewExportedDocument:exportInfo];
      [exportInfo setState:MZExportStateNone];
    }
    else if (newState == MZExportStateFailed)
    {
      NSString *title = [exportInfo errorTitle];
      if (!title)
        title = [NSString stringWithFormat:NSLocalizedString(@"Export Error Dialog Title", nil), [exportInfo sourceString]];

      NSString *message = [exportInfo errorMessage];

      __weak typeof(self) __self = self;

      UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
      UIAlertAction *dismissAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Export Error Dialog Cancel Button", nil)
                                                             style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * _Nonnull action) {
                                                             _exportCompleteAlertController = nil;
                                                             [__self presentAlertForExportCompleted];
                                                           }];
      [alertController addAction:dismissAction];
      [self showAlertController:alertController animated:YES completion:nil];
      _exportCompleteAlertController = alertController;
      [exportInfo setState:MZExportStateNone];
    }
  }
  else
  {
    [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
  }
}


#pragma mark - Open In... Menu

- (void)addNewExportedDocument:(MZExportInfo *)exportInfo
{
  [_exportCompletedInfo addObject:[exportInfo copy]];
  [self presentAlertForExportCompleted];
}

- (void)presentAlertForExportCompleted
{
  // We are already checking or deciding on another document, wait until finish.
  if (_exportCompleteAlertController || _openInDocInteractionController || _preview || ![_exportCompletedInfo count])
    return;

  MZExportInfo *exportInfo = [_exportCompletedInfo firstObject];

  BOOL canPreview = exportInfo.filesizeInBytes <= _maximumFileSizeInBytes;
  DLog(@"filesize of PDF %ld <= max filesize %ld", (long)exportInfo.filesizeInBytes, (long)_maximumFileSizeInBytes);

  UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"PDF Export Complete Dialog Title", nil)
                                                                           message:[NSString stringWithFormat:NSLocalizedString(@"PDF Export Complete Dialog Message", nil), exportInfo.workspaceName]
                                                                    preferredStyle:UIAlertControllerStyleAlert];

  __weak typeof(self) __self = self;
  if (canPreview)
  {
    UIAlertAction *previewAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"PDF Export Dialog Preview Button", nil)
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * _Nonnull action) {
                                                           [_filesToPreview addObject:[exportInfo filepath]];
                                                           [__self loadQuickLookPreview];
                                                         }];
    [alertController addAction:previewAction];
  }

  UIAlertAction *openInAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"PDF Export Dialog Open In Button", nil)
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * _Nonnull action) {
                                                          DLog(@"Will Open in %@", exportInfo.filepath);
                                                          [__self loadOpenInMenuForFile:exportInfo.filepath];
                                                        }];
  [alertController addAction:openInAction];

  UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Export Error Dialog Cancel Button", nil)
                                                         style:UIAlertActionStyleCancel
                                                       handler:^(UIAlertAction * _Nonnull action) {
                                                         _exportCompleteAlertController = nil;
                                                         [__self presentAlertForExportCompleted];
                                                       }];
  [alertController addAction:cancelAction];

  [self showAlertController:alertController animated:YES completion:nil];
  _exportCompleteAlertController = alertController;
  [_exportCompletedInfo removeObject:exportInfo];
}



#pragma mark - Open In and Preview Management

- (void)loadOpenInMenuForFile:(NSString *)filepath
{
  _openInDocInteractionController = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:filepath]];
  _openInDocInteractionController.delegate = self;
  
  if (_openInPopoverAnchorView)
  {
    if (![_openInDocInteractionController presentOpenInMenuFromRect:_openInPopoverAnchorView.superview.frame inView:_openInPopoverAnchorView.superview animated:YES])
    {
      [self presentAlertForNoPDFReaderAppAvailable];
    }
  }
  
  [_filesToPreview removeObject:filepath];
}


- (void)presentAlertForNoPDFReaderAppAvailable
{
  UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"PDF Export Missing PDF App Title", nil)
                                                                           message:NSLocalizedString(@"PDF Export Missing PDF App Message", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
  UIAlertAction *dismissAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Generic Button Title Dismiss", nil)
                                                          style:UIAlertActionStyleDefault
                                                        handler:nil];
  [alertController addAction:dismissAction];
  [self showAlertController:alertController animated:YES completion:nil];
  _openInDocInteractionController = nil;
}


- (void)loadQuickLookPreview
{
  if ([_filesToPreview count] && ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive))
  {
    
    MezzanineNavigationController *mainNavController = [[MezzanineAppContext currentContext] mainNavController];
    
    if (!_preview)
    {
      _preview = [[QLPreviewController alloc] init];
      _preview.delegate = self;
      _preview.dataSource = self;
      _preview.currentPreviewItemIndex = 0;
      UIViewController *topViewController = mainNavController.topViewController;
      UIViewController *visibleViewController = mainNavController.visibleViewController;
      if (topViewController == visibleViewController)
        [topViewController presentViewController:_preview animated:YES completion:nil];
      else
        [topViewController dismissViewControllerAnimated:NO completion:^{
          [topViewController presentViewController:_preview animated:YES completion:nil];
        }];
    }
    else
    {
      [_preview reloadData];
    }
  }
}


#pragma mark -
#pragma mark QLPreviewControllerDataSource methods

- (NSInteger)numberOfPreviewItemsInPreviewController:(QLPreviewController *)controller
{
	return [_filesToPreview count];
}

- (id <QLPreviewItem>)previewController:(QLPreviewController *)controller previewItemAtIndex:(NSInteger)index
{
  return [NSURL fileURLWithPath:[_filesToPreview objectAtIndex:index]];
}


#pragma mark -
#pragma mark QLPreviewControllerDelegate methods

- (void)previewControllerDidDismiss:(QLPreviewController *)controller
{
  _preview = nil;

  DLog(@"Deleting files that have been previewed: %@", _filesToPreview);

  NSFileManager *fileManager = [NSFileManager defaultManager];
  for (NSString *filePath in _filesToPreview)
  {
    NSError *error = nil;
    [fileManager removeItemAtPath:filePath error:&error];
  }

  [_filesToPreview removeAllObjects];

  [self presentAlertForExportCompleted];
}


#pragma mark -
#pragma mark UIDocumentInteractionControllerDelegate methods

- (void)documentInteractionControllerWillPresentOpenInMenu:(UIDocumentInteractionController *)controller
{
  // Bug 17361 - iOS: Close button in Open In pane is blank in iOS 10.
  // We have to force the appearance color.
  UIButton *buttonAppearance = [UIButton appearance];
  [buttonAppearance setTintColor:[MezzanineStyleSheet sharedStyleSheet].defaultSystemBlue];
}


- (void)documentInteractionControllerDidDismissOptionsMenu:(UIDocumentInteractionController *)controller
{
  _openInDocInteractionController = nil;
  [self presentAlertForExportCompleted];
  
  // Put back the UIButton appearance color from the style sheet.
  UIButton *buttonAppearance = [UIButton appearance];
  [buttonAppearance setTintColor:[UIColor whiteColor]];
}


- (void)documentInteractionControllerDidDismissOpenInMenu:(UIDocumentInteractionController *)controller
{
  _openInDocInteractionController = nil;
  [self presentAlertForExportCompleted];
  
  // Put back the UIButton appearance color from the style sheet.
  UIButton *buttonAppearance = [UIButton appearance];
  [buttonAppearance setTintColor:[UIColor whiteColor]];
}


- (void)documentInteractionController:(UIDocumentInteractionController *)controller didEndSendingToApplication:(NSString *)application
{
  _openInDocInteractionController = nil;
}


@end
