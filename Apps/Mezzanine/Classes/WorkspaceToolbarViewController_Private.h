//
//  WorkspaceToolbarViewController_Private.h
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 05/11/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

#import "WorkspaceToolbarViewController.h"

@interface WorkspaceToolbarViewController ()

- (void)updateMezzanineName;
- (void)updateSessionPanel;
- (void)showParticipantsList;

- (void)workspaceDiscard;
- (void)workspaceRename;
- (void)workspaceSave;
- (void)workspaceClose;
- (void)workspaceOpen;

@end
