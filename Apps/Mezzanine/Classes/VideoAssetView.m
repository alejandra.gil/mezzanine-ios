//
//  VideoAssetView.m
//  Mezzanine
//
//  Created by Miguel Sánchez Valdés on 3/12/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import "VideoAssetView.h"
#import "MezzanineStyleSheet.h"
#import "MZDownloadManager.h"
#import "Mezzanine-Swift.h"

typedef NS_ENUM(NSInteger, VideoAssetViewImageMode) {
  VideoAssetViewImageModeSmall,
  VideoAssetViewImageModeLarge
};


@interface VideoAssetView ()
{
  NSMutableArray *liveStreamObservers;
  
  VideoAssetViewImageMode _imageMode;
}

@end



@implementation VideoAssetView

@synthesize imageView;
@synthesize placeholderView;

@synthesize maxTitleFontSize;
@synthesize minTitleFontSize;
@synthesize minSubtitleFontSize;

- (id)initWithFrame:(CGRect)frame
{
  self = [super initWithFrame:frame];
  if (self)
  { // Custom initialization
    CGRect bounds = contentView.bounds;
    contentView.layer.contentsScale = UIScreen.mainScreen.scale;

    placeholderView = [VideoPlaceholderView new];
    placeholderView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    placeholderView.backgroundColor = [MezzanineStyleSheet sharedStyleSheet].videoPlaceholderBackgroundColor;
    placeholderView.frame = bounds;
    [contentView addSubview:placeholderView];

    imageView = [[UIImageView alloc] initWithFrame:bounds];

    if ([SettingsFeatureToggles sharedInstance].teamworkUI)
    {
      imageView.frame = bounds;
    }
    else
    {
      imageView.frame = CGRectInset(bounds, 2.0, 2.0);
    }
    imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    //imageView.backgroundColor = [UIColor colorWithWhite:0.66 alpha:0.56];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    imageView.userInteractionEnabled = NO;  // Important for gesture recognizers
    [contentView addSubview:imageView];

    [self setMaxTitleFontSize:42.0];
    [self setMinTitleFontSize:16.0];
    [self setMinSubtitleFontSize:12.0];

    _imageMode = VideoAssetViewImageModeSmall;
  }
  
  return self;
}

-(void) dealloc
{
  self.liveStream = nil;
}


-(void) layoutSubviews
{
  [super layoutSubviews];
  
  CGRect contentBounds = contentView.bounds;
  imageView.frame = contentBounds;
  placeholderView.frame = contentBounds;

  // Performance become terrible when a windshield asset is scaled to very large
  // so don't render during direct manipulation
  if (!self.isBeingManipulated) {

    // Yet, this isn't quite right to comment the following out, yet it somehow works
    [placeholderView setNeedsDisplay];

    
    [self updateImageMode];
  }
}


-(void) setIsBeingManipulated:(BOOL)isBeingManipulated
{
  [super setIsBeingManipulated:isBeingManipulated];

  if (!isBeingManipulated)
    [placeholderView setNeedsDisplay];
}


-(void) setMaxTitleFontSize:(CGFloat)fontSize
{
  maxTitleFontSize = fontSize;
  placeholderView.maxTitleFontSize = fontSize;
}

-(void) setMinTitleFontSize:(CGFloat)fontSize
{
  minTitleFontSize = fontSize;
  placeholderView.minTitleFontSize = fontSize;
}

-(void) setMinSubtitleFontSize:(CGFloat)fontSize
{
  minSubtitleFontSize = fontSize;
  placeholderView.minSubtitleFontSize = fontSize;
}


- (void)updateImageMode
{
  // Some VideoAssetViews might be contained by a (scroll)view that is in a zoomed out state
  // Those views keep the same frame, but they actually occupy less space in the screen
  // So its zooming factor must be taken into account when deciding the image mode
  UIView *nextSuperview = self.superview;
  CGFloat scaleOnScrollView = 1.0;
  while (nextSuperview)
  {
    if ([nextSuperview isKindOfClass:[UIScrollView class]])
      scaleOnScrollView *= ((UIScrollView *)nextSuperview).zoomScale;
    nextSuperview = nextSuperview.superview;
  }

  CGFloat screenScale = [UIScreen mainScreen].scale;
  CGFloat pixelWidth = self.bounds.size.width * screenScale * scaleOnScrollView;
  CGFloat pixelHeight = self.bounds.size.height * screenScale * scaleOnScrollView;

  if (pixelWidth > 320 || pixelHeight > 320)
    [self setImageMode:VideoAssetViewImageModeLarge];
  else
    [self setImageMode:VideoAssetViewImageModeSmall];
}


- (void)setImageMode:(VideoAssetViewImageMode)mode
{
  if (_imageMode != mode)
  {
    _imageMode = mode;

    if (_liveStream)
      [self updateThumbnailWithURL];
  }
}


#pragma mark - Live Stream

- (void)setLiveStream:(MZLiveStream *)liveStream
{
  if (_liveStream != liveStream)
  {
    if (_liveStream)
    {
      [_liveStream removeObserver:self forKeyPath:@"displayName"];
      [_liveStream removeObserver:self forKeyPath:@"thumbnail"];
      [_liveStream removeObserver:self forKeyPath:@"active"];
      [_liveStream removeObserver:self forKeyPath:@"fullURL"];
      [_liveStream removeObserver:self forKeyPath:@"remoteMezzName"];

      [[MZDownloadManager sharedLoader] cancelLoadingURLForTarget:imageView];
    }
    
    _liveStream = liveStream;
    
    if (_liveStream)
    {
      [_liveStream addObserver:self forKeyPath:@"displayName" options:(NSKeyValueObservingOptionInitial) context:nil];
      [_liveStream addObserver:self forKeyPath:@"thumbnail" options:(NSKeyValueObservingOptionInitial) context:nil];
      [_liveStream addObserver:self forKeyPath:@"active" options:0 context:nil];
      [_liveStream addObserver:self forKeyPath:@"fullURL" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:nil];
      [_liveStream addObserver:self forKeyPath:@"remoteMezzName" options:0 context:nil];

      imageView.image = _liveStream.thumbnail;
    }
  }
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
  if ([keyPath isEqualToString:@"displayName"])
  {
    [self updateSourceNameAndOrigin];
  }
  else if ([keyPath isEqualToString:@"fullURL"])
  {
    if (![change[NSKeyValueChangeOldKey] isEqual:[NSNull null]])
    {
      NSString *oldURL = change[NSKeyValueChangeOldKey];
      [self unloadOldThumbnailURL:oldURL];
    }
    
    [self updateThumbnailWithURL];
  }
  else if ([keyPath isEqualToString:@"remoteMezzName"])
  {
    [self updateSourceNameAndOrigin];
  }
}


- (void)updateSourceNameAndOrigin
{
  if (_liveStream)
  {
    placeholderView.sourceName = _liveStream.displayName;

    NSString *sourceString = _liveStream.remoteMezzName;
    if (sourceString)
      placeholderView.sourceOrigin = [NSString stringWithFormat:NSLocalizedString(@"Video Placeholder Origin Text", nil), sourceString];
    else
      placeholderView.sourceOrigin = nil;
  }
}


- (void) unloadOldThumbnailURL:(NSString *)oldURL
{
  NSURL *url = [[MezzanineAppContext currentContext].currentCommunicator urlForResource:oldURL];
  if (url == nil)
    return;

  [[MZDownloadManager sharedLoader] unloadImageFromMemoryCacheWithURL:url];
  [[MZDownloadManager sharedLoader] cancelLoadingURLForTarget:imageView];
}

- (void)updateThumbnailWithURL
{
  if (_liveStream.active && (_liveStream.fullURL != nil || _liveStream.thumbURL != nil))
  {
    NSURL *thumbUrl = [[MezzanineAppContext currentContext].currentCommunicator urlForResource:_liveStream.thumbURL];
    NSURL *fullUrl = [[MezzanineAppContext currentContext].currentCommunicator urlForResource:_liveStream.fullURL];
    if ((_imageMode == VideoAssetViewImageModeLarge) && (fullUrl != nil))
    {
      [[MZDownloadManager sharedLoader] loadImageWithURL:fullUrl
                                                atTarget:imageView
                                                 success:^(UIImage *image) {
                                                   placeholderView.hidden = YES;
                                                   imageView.image = image;
                                                 }
                                                   error:^(NSError *error) {
                                                     if (thumbUrl == nil)
                                                       return;
                                                     
                                                     [[MZDownloadManager sharedLoader] loadImageWithURL:thumbUrl
                                                                                               atTarget:imageView
                                                                                                success:^(UIImage *image) {
                                                                                                }
                                                                                                  error:^(NSError *error) { }];
                                                   }
                                                canceled:nil cachingOption:MZDownloadCacheTypeOnlyInMemory];
    }
    else if (thumbUrl != nil)
    {
      [[MZDownloadManager sharedLoader] loadImageWithURL:thumbUrl
                                                atTarget:imageView
                                                 success:^(UIImage *image) {
                                                   if (_liveStream.thumbnail != image)
                                                     [_liveStream setThumbnail:image]; // Triggers the thumbnail observer which updates the image

                                                   placeholderView.hidden = YES;
                                                   imageView.image = image;
                                                 }
                                                   error:^(NSError *error) {
                                                   }
                                                canceled:nil cachingOption:MZDownloadCacheTypeOnlyInMemory];

    }
  }
  else
  {
    placeholderView.hidden = NO;
    imageView.image = nil;
  }
}


@end
