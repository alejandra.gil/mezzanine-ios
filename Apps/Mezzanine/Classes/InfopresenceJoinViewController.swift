//
//  InfopresenceJoinViewController.swift
//  Mezzanine
//
//  Created by Zai Chang on 6/26/15.
//  Copyright (c) 2015 Oblong Industries. All rights reserved.
//

import UIKit

@objc class InfopresenceJoinViewController : UIViewController, UITableViewDelegate, UINavigationBarDelegate
{
  @IBOutlet weak var navigationBar: UINavigationBar!
	@IBOutlet weak var tableView: UITableView!

	var noMezzaninesView: UIView?
	@IBOutlet weak var noMezzaninesTitleLabel: UILabel!
	@IBOutlet weak var noMezzaninesDescriptionLabel: UILabel!

  fileprivate var joinAlertController: UIAlertController?

	fileprivate var kSystemModelContext = 0
	var systemModel: MZSystemModel!

	func beginObservingSystemModel() {
		self.systemModel?.addObserver(self, forKeyPath: "remoteMezzes", options: .new, context: &kSystemModelContext)
	}

	func endObservingSystemModel() {
		self.systemModel?.removeObserver(self, forKeyPath: "remoteMezzes")
	}

	var communicator: MZCommunicator
  unowned var infopresenceMenuController: InfopresenceMenuController
	var dataSource: RemoteMezzaninesTableViewDataSource!

	init(systemModel: MZSystemModel, communicator: MZCommunicator, infopresenceMenuController: InfopresenceMenuController) {
		self.systemModel = systemModel;
		self.communicator = communicator
    self.infopresenceMenuController = infopresenceMenuController
		super.init(nibName: "InfopresenceJoinViewController", bundle: nil)
		beginObservingSystemModel()
	}

	required init?(coder aDecoder: NSCoder) {
	    fatalError("init(coder:) has not been implemented")
	}
	
	deinit {
    joinAlertController?.dismiss(animated: true, completion: nil)
    joinAlertController = nil
    endObservingSystemModel()
	}

	override func viewDidLoad() {

    super.viewDidLoad()
    
    let isRegular = infopresenceMenuController.containerViewController?.traitCollection.isRegularWidth() == true && infopresenceMenuController.containerViewController?.traitCollection.isRegularHeight() == true
    preferredContentSize = CGSize(width: isRegular ? 320.0 : 300.0, height: CGFloat.greatestFiniteMagnitude);
    
		dataSource = RemoteMezzaninesTableViewDataSource(systemModel: systemModel, tableView: tableView)

    // This is to avoid the UITableView to be shifted 20 px up on iPhone because of the status bar being present
    self.edgesForExtendedLayout = UIRectEdge()

		tableView.dataSource = dataSource
		tableView.delegate = self
    tableView.backgroundColor = UIColor.clear

		let styleSheet = MezzanineStyleSheet.shared()
		
		let noMezzaninesViewNib = UINib(nibName: "NoMezzaninesAvailableView", bundle: nil)
		noMezzaninesView = noMezzaninesViewNib.instantiate(withOwner: self, options: nil)[0] as? UIView
		noMezzaninesView!.autoresizingMask = [.flexibleWidth, .flexibleHeight]
		noMezzaninesView!.frame = tableView.frame
		noMezzaninesTitleLabel.text = "Infopresence No Mezzanines Label".localized
		noMezzaninesDescriptionLabel.text = "Infopresence No Remote Mezzanine Label".localized
		styleSheet?.stylizeDimInformativeTitleLabel(noMezzaninesTitleLabel)
		styleSheet?.stylizeDimInformativeDescriptionLabel(noMezzaninesDescriptionLabel)
		self.view.addSubview(noMezzaninesView!)
		updateNoMezzaninesView()

    self.view.backgroundColor = styleSheet?.grey30Color

    navigationBar.delegate = self
    navigationBar.backgroundColor = styleSheet?.grey50Color

    let navBarLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 32))
    navBarLabel.text = "Infopresence Join Mezzanines Panel Header".localized
    navBarLabel.textAlignment = .center
		styleSheet?.stylizeHeaderTitleLabel(navBarLabel)

    // The navigation item has been added at the nib file
    let item = navigationBar.items!.first!
    item.titleView = navBarLabel

    if !isRegular
    {
      let backButton = UIButton(type: .custom)
      backButton.frame = CGRect(x: 0, y: 0, width: 32, height: 22)
      backButton.setImage(UIImage(named: "Arrow"), for: .normal)
      backButton.imageEdgeInsets = UIEdgeInsetsMake(8, 0, 5, 20)
      backButton.addTarget(self, action: #selector(goBack), for: .touchUpInside)
      let barButtonItem = UIBarButtonItem(customView: backButton)
      item.leftBarButtonItem = barButtonItem
    }
	}
  
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    
    // Analytics
    let analyticsManager = MezzanineAnalyticsManager.sharedInstance
    analyticsManager.tagScreen(analyticsManager.infopresenceJoinScreen)
  }
  
  override var prefersStatusBarHidden : Bool {
    return false
  }
  
  override var preferredStatusBarUpdateAnimation : UIStatusBarAnimation {
    return .fade
  }
  
  override var preferredStatusBarStyle : UIStatusBarStyle {
    return .lightContent
  }
	
	func updateNoMezzaninesView() {
		if (systemModel.remoteMezzes.count == 0) {
			noMezzaninesView?.isHidden = false
			tableView.isHidden = true
		}
		else {
			noMezzaninesView?.isHidden = true
			tableView.isHidden = false
		}
	}
	
	internal override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?)
	{
		if context == &kSystemModelContext
		{
			if keyPath == "remoteMezzes" {
				updateNoMezzaninesView()
			}
		}
		else
		{
			super.observeValue(forKeyPath: keyPath!, of: object!, change: change, context: context)
		}
	}

  func indexPathIsSelectable(_ indexPath: IndexPath) -> Bool {
    let remoteMezz = dataSource.itemAtIndexPath(indexPath)
    return remoteMezz.online && (remoteMezz.collaborationState == .notInCollaboration)
  }

  func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
    return indexPathIsSelectable(indexPath)
  }

  func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
    if indexPathIsSelectable(indexPath) == true {
      return indexPath
    }
    return nil
  }

	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		startJoiningSelectedMezzanine()
	}

  func startJoiningSelectedMezzanine()
  {
    let currentWorkspace = self.systemModel.currentWorkspace
    let currentWorkspaceIsUnsavedAndHasContent = (!(currentWorkspace?.isSaved)! && (currentWorkspace?.hasContent)!)
    if currentWorkspaceIsUnsavedAndHasContent {
      showWorkspaceIsUnsavedAndHasContentAlert()
    }
    else {
      joinSelectedMezzanine()
    }
  }

  func showWorkspaceIsUnsavedAndHasContentAlert() {
    let alertTitle = "Infopresence Join Dialog Title".localized
    let alertMessage = "Infopresence Join Dialog Message".localized

    let alertController = UIAlertController (title: alertTitle, message: alertMessage, preferredStyle: .alert)

    let confirmAction = UIAlertAction(title: "Infopresence Join Dialog Confirm Button Title".localized, style: .default) { [unowned self](_) -> Void in
      self.joinSelectedMezzanine()
    }

    let cancelAction = UIAlertAction(title: "Infopresence Join Dialog Cancel Button Title".localized, style: .cancel) { [unowned self](_) -> Void in
      self.cancelJoinSelectedMezzanine()
    }
    
    alertController.addAction(confirmAction)
    alertController.addAction(cancelAction)

    self.present(alertController, animated: true, completion: nil)
    joinAlertController = alertController
  }

  func joinSelectedMezzanine() {
    joinAlertController = nil
    var selectedMezzanine: MZRemoteMezz? = nil
    let selectedItems = dataSource.selectedItems()
    if selectedItems.count == 1 {
      selectedMezzanine = selectedItems[0]
    }
    
    if let selectedMezzanineNotNil = selectedMezzanine {
        communicator.requestor.requestInfopresenceOutgoingRequest(selectedMezzanineNotNil.uid, sentUtc: 0)
    }
    
    let isRegular = infopresenceMenuController.containerViewController?.traitCollection.isRegularWidth() == true && infopresenceMenuController.containerViewController?.traitCollection.isRegularHeight() == true
    if !isRegular {
      goBack()
    }
  }

  func cancelJoinSelectedMezzanine() {
    joinAlertController = nil
    var selectedMezzanine: MZRemoteMezz? = nil
    let selectedItems = dataSource.selectedItems()
    if selectedItems.count == 1 {
      selectedMezzanine = selectedItems[0]
      if let indexPath = dataSource.indexPathOfItem(selectedMezzanine!) {
        tableView.deselectRow(at: indexPath, animated: true)
      }
    }
  }

  @IBAction func goBack() {
    infopresenceMenuController.popViewControllerUsingNavigationController()
  }
  
  
  // MARK: Adaptative layout

  override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
    for (_, viewController) in (navigationController?.viewControllers.enumerated())! {
      if viewController == self {
        navigationController?.popViewController(animated: false)
        break
      }
    }
  }

  
  // MARK: Navigation bar delegate

  func position(for bar: UIBarPositioning) -> UIBarPosition {
    return .topAttached
  }
}
