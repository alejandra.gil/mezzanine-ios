//
//  MezzanineViewController.h
//  Mezzanine
//
//  Created by Zai Chang on 9/20/10.
//  Copyright 2010 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MezzanineBaseRoomViewController.h"

// TODO: Move MezzanineBaseRoomViewController stuff here and delete class
@interface MezzanineMainViewController : MezzanineBaseRoomViewController

@end

@protocol MezzanineMainViewControllerDelegate<NSObject>

@required

// OBDragZone
-(void) mezzanineMainViewControllerDidStartDraggingWindshieldItem:(MezzanineMainViewController *)mezzanineVC;
-(void) mezzanineMainViewControllerDidStopDraggingWindshieldItem:(MezzanineMainViewController *)mezzanineVC;

@end


