//
//  PrimaryWindshieldViewController.m
//  Mezzanine
//
//  Created by miguel on 6/3/15.
//  Copyright (c) 2015 Oblong Industries. All rights reserved.
//

#import "PrimaryWindshieldViewController.h"
#import "WindshieldViewController_Private.h"
#import "PrimaryWindshieldViewController_Private.h"
#import "WindshieldPlaceholderView.h"
#import "MezzanineMainViewController.h"
#import "UIGestureRecognizer+OBDragDrop.h"
#import "AssetView.h"
#import "Mezzanine-Swift.h"

@implementation PrimaryWindshieldViewController

@dynamic systemModel;

#pragma mark - View lifecycle

-(void) viewWillDisappear:(BOOL)animated
{
  [super viewWillDisappear:animated];
  [self removeIndicator];
}

#pragma mark -
#pragma mark Item Views Management

-(void) loadAllWindshieldItems
{
  [self removeAllItemViews];

  NSArray *items = self.windshield.itemsInPrimaryWindshield;
  for (NSInteger i=0; i<[items count]; i++)
  {
    [self insertViewForItem:items[i]];
  }
}


- (void)viewDidLayoutSubviews
{
  [super viewDidLayoutSubviews];
  
  if (self.firstLoad)
    [self refreshLayout];
}


-(void) refreshLayout
{
  [self refreshItemsLayout:self.windshield.itemsInPrimaryWindshield];
}

-(void) updateCropViewForItem:(MZWindshieldItem *)item
{
  // Don't do anything here yet.
}

#pragma mark -
#pragma mark Coordinates transforms

-(CGFloat) surface:(MZSurface *)surface feldXOffset:(MZFeld *)feld
{
  NSInteger ix = [_systemModel.felds indexOfObject:feld];
  return [self windshieldFeldWidth] * ix;
}


-(CGFloat) surface:(MZSurface *)surface feldHeight:(MZFeld *)feld
{
  return [self windshieldHeight];
}


-(CGFloat) surface:(MZSurface *)surface feldWidth:(MZFeld *)feld
{
  return [self windshieldFeldWidth];
}


-(CGFloat) windshieldHeight
{
  return _shielderContainerFrame.size.height;
}


-(CGFloat) windshieldWidth
{
  return _shielderContainerFrame.size.width;
}


-(CGFloat) windshieldFeldWidth
{
  NSInteger numberOfFelds = _systemModel.felds.count;
  return [self windshieldWidth] / numberOfFelds;
}


-(CGPoint) convertPointFromWindshieldToScrollViewCoordinates:(CGPoint)pointInWindshieldCoordinates
{
  CGPoint locationInContentScrollView = [self.workspaceViewController.contentScrollView convertPoint:pointInWindshieldCoordinates fromView:self.view];
  return locationInContentScrollView;
}


-(CGRect) convertFrameFromRelativeCoordinatesToWindshieldViewCoordinates:(CGRect)frameInRelativeCoordinates
{
  return CGRectMake(((frameInRelativeCoordinates.origin.x - frameInRelativeCoordinates.size.width / 2.0) + 0.5) * [self windshieldFeldWidth],
                    ((1 - (frameInRelativeCoordinates.origin.y + 0.5)) - frameInRelativeCoordinates.size.height / 2.0) * [self windshieldHeight],
                    frameInRelativeCoordinates.size.width * [self windshieldFeldWidth],
                    frameInRelativeCoordinates.size.height * [self windshieldHeight]);
}


-(CGRect) convertFrameFromWindshieldViewCoordinatesToRelativeCoordinates:(CGRect)frameInWindshieldViewCoordinates
{
  return CGRectMake(-0.5 + (frameInWindshieldViewCoordinates.origin.x) / [self windshieldFeldWidth] + frameInWindshieldViewCoordinates.size.width / [self windshieldFeldWidth] / 2.0,
                    0.5 - (frameInWindshieldViewCoordinates.origin.y / [self windshieldHeight]) - frameInWindshieldViewCoordinates.size.height / [self windshieldHeight] / 2.0,
                    frameInWindshieldViewCoordinates.size.width / [self windshieldFeldWidth],
                    frameInWindshieldViewCoordinates.size.height / [self windshieldHeight]);
}

-(NSInteger)indexOfFeldInPrimarySurface:(MZFeld *)feld
{
  NSInteger ix;
  if ([_systemModel.felds containsObject:feld])
    ix = [_systemModel.felds indexOfObject:feld];
  else
  {
    MZSurface *primarySurface = [_systemModel.surfacesInPrimaryWindshield firstObject];
    NSArray *feldsWithoutMullions = [primarySurface.felds filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"type != %@", @"mullion"]];
    ix = [feldsWithoutMullions indexOfObject:feld];
  }
  return ix;
}

-(CGPoint) shiftPointFromOrigin:(CGPoint)pointInOrigin toFeld:(MZFeld *)feld
{
  return CGPointMake(pointInOrigin.x + [self windshieldFeldWidth] * [self indexOfFeldInPrimarySurface:feld], pointInOrigin.y);
}


-(CGPoint)shiftPointToOrigin:(CGPoint)pointInOrigin fromFeld:(MZFeld *)feld
{
  return CGPointMake(pointInOrigin.x - [self windshieldFeldWidth] * [self indexOfFeldInPrimarySurface:feld], pointInOrigin.y);
}


-(CGPoint)shiftPointFromWindshieldViewToShielderContainer:(CGPoint)pointInWindshieldViewCoordinates
{
  return CGPointMake(pointInWindshieldViewCoordinates.x + _shielderContainerFrame.origin.x,
                     pointInWindshieldViewCoordinates.y + _shielderContainerFrame.origin.y);
}


-(CGPoint)shiftPointFromShielderContainerToWindshieldView:(CGPoint)pointInShielderViewCoordinates
{
  return CGPointMake(pointInShielderViewCoordinates.x - _shielderContainerFrame.origin.x,
                     pointInShielderViewCoordinates.y - _shielderContainerFrame.origin.y);
}


-(CGRect) rectFromNativeWindshieldCoordinatesForItem:(MZWindshieldItem *)item
{
  return [self rectFromNativeWindshieldCoordinates:item.frame inFeld:item.feld];
}


-(CGRect) rectFromNativeWindshieldCoordinates:(CGRect)frame inFeld:(MZFeld *)feld
{
  CGRect absoluteRect = [self convertFrameFromRelativeCoordinatesToWindshieldViewCoordinates:frame];
  absoluteRect.origin = [self shiftPointFromOrigin:absoluteRect.origin toFeld:feld];
  absoluteRect.origin = [self shiftPointFromWindshieldViewToShielderContainer:absoluteRect.origin];
  return absoluteRect;
}


-(CGRect) rectToNativeWindshieldCoordinates:(CGRect)frame inFeld:(MZFeld *)feld
{
  frame.origin = [self shiftPointFromShielderContainerToWindshieldView:frame.origin];
  frame.origin = [self shiftPointToOrigin:frame.origin fromFeld:feld];
  frame = [self convertFrameFromWindshieldViewCoordinatesToRelativeCoordinates:frame];
  return frame;
}


-(MZFeld *)closestFeldAtLocation:(CGPoint)location
{
  CGPoint locationInContentScrollView = [self convertPointFromWindshieldToScrollViewCoordinates:location];
  NSInteger ixFeld = [self.workspaceViewController feldIndexAtLocation:locationInContentScrollView];
  ixFeld = MIN(MAX(ixFeld, 0), _systemModel.felds.count - 1);
  return [_systemModel.felds objectAtIndex:ixFeld];
}

- (CGFloat) zoomScaleOfWindshield
{
  return self.workspaceViewController.contentScrollView.zoomScale;
}

-(NSUInteger)indexOfFeldAtLocation:(CGPoint)location
{
  MZFeld *feld = [self closestFeldAtLocation:location];
  return [_systemModel.felds indexOfObject:feld];
}

- (BOOL)isLocationInCorkboardSpace:(CGPoint)location
{
  return [self.workspaceViewController isLocationInCorkboardSpace:location];
}


#pragma mark - OBDropZone

-(OBDropAction) ovumEntered:(OBOvum*)ovum inView:(UIView*)view atLocation:(CGPoint)location
{
  [self createIndicatorForOvum:ovum atLocation:location];

  return [super ovumEntered:ovum inView:view atLocation:location];
}

-(void) ovumExited:(OBOvum*)ovum inView:(UIView*)view atLocation:(CGPoint)location
{
  if ([self.delegate respondsToSelector:@selector(windshieldViewEndEdgeScrolling)]) {
    [self.delegate windshieldViewEndEdgeScrolling];
  }

  [super ovumExited:ovum inView:view atLocation:location];
  [self showPlaceholderView:NO animated:YES];
}


-(void) ovumDropped:(OBOvum*)ovum inView:(UIView*)view atLocation:(CGPoint)location
{
  [self removeIndicator];

  [super ovumDropped:ovum inView:view atLocation:location];

  CGPoint locationInContentScrollView = [self.workspaceViewController.contentScrollView convertPoint:location fromView:self.view];

  if ([self.delegate respondsToSelector:@selector(windshieldViewUpdateSpaceIndexWithLocation:)]) {
    [self.delegate windshieldViewUpdateSpaceIndexWithLocation:locationInContentScrollView];
  }
  
}

-(OBDropAction) ovumMoved:(OBOvum*)ovum inView:(UIView*)view atLocation:(CGPoint)location
{

  if ([ovum.dataObject isKindOfClass:[MZAsset class]] ||
      [ovum.dataObject isKindOfClass:[MZLiveStream class]] ||
      [ovum.dataObject isKindOfClass:[MZPortfolioItem class]])
  {
    CGPoint locationInContentScrollView = [self convertPointFromWindshieldToScrollViewCoordinates:location];
    
    if ([self.delegate respondsToSelector:@selector(windshieldViewEdgeScrollingEventAtLocation:)]) {
      [self.delegate windshieldViewEdgeScrollingEventAtLocation:locationInContentScrollView];
    }

    CGFloat scale = [self scaleOfItemView:draggedView atLocation:location];
    [self updatePlaceholderViewForObject:ovum.dataObject withLocation:location andScale:scale];
  }
  return [super ovumMoved:ovum inView:view atLocation:location];
}


@end
