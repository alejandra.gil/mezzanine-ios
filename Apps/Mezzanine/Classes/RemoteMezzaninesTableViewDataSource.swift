//
//  RemoteMezzaninesTableViewDataSource.swift
//  Mezzanine
//
//  Created by Zai Chang on 6/26/15.
//  Copyright (c) 2015 Oblong Industries. All rights reserved.
//

import UIKit


// The intent was to use the generic data source class ObservableArrayTableViewDataSource
// something like:
//
// public class RemoteMezzaninesTableViewDataSource: ObservableArrayTableViewDataSource<MZRemoteMezz, RemoteMezzanineTableViewCell>
//
// but ran into issues. See comments in ObservableArrayTableViewDataSource for more information

class RemoteMezzaninesTableViewDataSource: NSObject, UITableViewDataSource
{
	var tableView: UITableView

  var sortedMezzanines = Array<MZRemoteMezz>()

  init(systemModel: MZSystemModel, tableView: UITableView) {
		self.systemModel = systemModel
		self.tableView = tableView
		super.init()
		setupTableView()

		// Because the above assignment doesn't seem to invoke the didSet handler
		beginObservingSystemModel()
    updateSortedMezzanines()
	}
	
	deinit {
		endObservingSystemModel()
	}

	/// Model
	///
	var systemModel: MZSystemModel! {
		willSet(aNewModel) {
			if self.systemModel != aNewModel && self.systemModel != nil
			{
				endObservingSystemModel()
			}
		}
		didSet
		{
      if (self.systemModel != nil)
      {
        beginObservingSystemModel()
      }
		}
	}
	
	func beginObservingSystemModel()
	{
		systemModel?.addObserver(self, forKeyPath: kObservationKeyPath, options: [.new, .old], context: &kModelContext)

    for remoteMezz in (systemModel?.remoteMezzes)! {
      beginObservingRemoteMezz(remoteMezz as! MZRemoteMezz)
    }
	}
	
	func endObservingSystemModel()
	{
		systemModel?.removeObserver(self, forKeyPath: kObservationKeyPath, context: &kModelContext)

    for remoteMezz in (systemModel?.remoteMezzes)! {
      endObservingRemoteMezz(remoteMezz as! MZRemoteMezz)
    }
	}

	func observedArray() -> Array<MZRemoteMezz>
  {
		return systemModel.value(forKeyPath: kObservationKeyPath) as! Array<MZRemoteMezz>
	}

  func beginObservingRemoteMezz(_ remoteMezz: MZRemoteMezz)
  {
    if remoteMezz.roomType == .cloud { return }
    remoteMezz.addObserver(self, forKeyPath: kRemoteMezzNameObservationKeyPath, options: [.new, .old], context: &kRemoteMezzContext)
    remoteMezz.addObserver(self, forKeyPath: kRemoteMezzCompanyObservationKeyPath, options: [.new, .old], context: &kRemoteMezzContext)
    remoteMezz.addObserver(self, forKeyPath: kRemoteMezzLocationObservationKeyPath, options: [.new, .old], context: &kRemoteMezzContext)
  }

  func endObservingRemoteMezz(_ remoteMezz: MZRemoteMezz)
  {
    if remoteMezz.roomType == .cloud { return }
    remoteMezz.removeObserver(self, forKeyPath: kRemoteMezzNameObservationKeyPath, context: &kRemoteMezzContext)
    remoteMezz.removeObserver(self, forKeyPath: kRemoteMezzCompanyObservationKeyPath, context: &kRemoteMezzContext)
    remoteMezz.removeObserver(self, forKeyPath: kRemoteMezzLocationObservationKeyPath, context: &kRemoteMezzContext)
  }


	override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?)
	{
		switch (keyPath!, context) {
		case (kObservationKeyPath, .some(&kModelContext)):
				if let inserted = change?[NSKeyValueChangeKey.newKey] as! Array<MZRemoteMezz>! {
					var indexPaths = Array<IndexPath>()
          updateSortedMezzanines()
					for insertedItem in inserted {
            if insertedItem.roomType == .cloud { continue }
            beginObservingRemoteMezz(insertedItem)
            let indexPath = IndexPath(row: sortedMezzanines.index(of: insertedItem)!, section: 0)
						indexPaths.append(indexPath)
					}

					if indexPaths.count > 0 {
						tableView.insertRows(at: indexPaths, with: UITableViewRowAnimation.automatic)
					}
				}

				if let deleted = change?[NSKeyValueChangeKey.oldKey] as! Array<MZRemoteMezz>! {
					var indexPaths = Array<IndexPath>()

          for deletedItem in deleted {
            if deletedItem.roomType == .cloud { continue }
            endObservingRemoteMezz(deletedItem)
            let indexPath = IndexPath(row: self.sortedMezzanines.index(of: deletedItem)!, section: 0)
            indexPaths.append(indexPath)
          }

          self.updateSortedMezzanines()
					
					if indexPaths.count > 0 {
						tableView.deleteRows(at: indexPaths, with: UITableViewRowAnimation.automatic)
					}
				}
        break
    case (kRemoteMezzNameObservationKeyPath, .some(&kRemoteMezzContext)),
         (kRemoteMezzLocationObservationKeyPath, .some(&kRemoteMezzContext)),
         (kRemoteMezzCompanyObservationKeyPath, .some(&kRemoteMezzContext)):
      updateSortedMezzanines()
      tableView.reloadData()
      break
		default:
			super.observeValue(forKeyPath: keyPath!, of: object!, change: change!, context: context)
		}
	}

	func itemAtIndexPath(_ indexPath: IndexPath) -> MZRemoteMezz
  {
		return sortedMezzanines[indexPath.row]
	}

  func indexPathOfItem(_ item: MZRemoteMezz) -> IndexPath?
  {
    if let index = sortedMezzanines.index(of: item) {
      return IndexPath(row: index, section: 0)
    }
    else {
      return nil
    }
  }

	/// Selection
	///
	func selectedItems() -> Array<MZRemoteMezz>
	{
		var results: Array<MZRemoteMezz> = []
		if let indexPaths = tableView.indexPathsForSelectedRows {
			for indexPath in indexPaths {
				let item = itemAtIndexPath(indexPath )
				results.append(item)
			}
		}
		return results
	}

	
	/// TableView
	///
	func setupTableView()
	{
		let styleSheet = MezzanineStyleSheet.shared()
		
		tableView.rowHeight = 88.0
		tableView.accessibilityIdentifier = "InfopresenceView.TableView"
		tableView.register(UINib(nibName: cellNibName, bundle:nil), forCellReuseIdentifier: cellIdentifier)
		tableView.backgroundColor = styleSheet?.darkestFillColor
		tableView.separatorStyle = .none
	}
	
	
	/// UITableViewDelegate
	///
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
	{
		return sortedMezzanines.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
	{
		let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! RemoteMezzanineTableViewCell

		let remoteMezz = itemAtIndexPath(indexPath)
		cell.remoteMezz = remoteMezz
		
		return cell
	}

  /// Sorted Mezzanines
  ///
  func stringsAreEqualOrEmpty(_ s1: String?, s2: String?) -> Bool
  {
    return ((s1 ?? "").isEmpty && (s2 ?? "").isEmpty) || (s1 ?? "").localizedCaseInsensitiveCompare((s2 ?? "")) == ComparisonResult.orderedSame
  }

  func remoteMezzSort(_ m1: MZRemoteMezz, m2: MZRemoteMezz) -> Bool
  {
    if stringsAreEqualOrEmpty(m1.name, s2:m2.name) {
      if stringsAreEqualOrEmpty(m1.company, s2:m2.company) {
        if stringsAreEqualOrEmpty(m1.location, s2:m2.location) {
          return Unmanaged.passUnretained(m1).toOpaque() < Unmanaged.passUnretained(m2).toOpaque()
        } else {
          return (m1.location ?? "").compare((m2.location ?? ""), options: [.numeric, .caseInsensitive]) == ComparisonResult.orderedAscending
        }
      } else {
        return (m1.company ?? "").compare((m2.company ?? ""), options: [.numeric, .caseInsensitive]) == ComparisonResult.orderedAscending
      }
    } else {
      return (m1.name ?? "").compare((m2.name ?? ""), options: [.numeric, .caseInsensitive]) == ComparisonResult.orderedAscending
    }
  }

  func updateSortedMezzanines()
  {
    let source = observedArray()
    sortedMezzanines = source.filter{$0.roomType != .cloud}.sorted(by: remoteMezzSort)
  }

	
	fileprivate var cellNibName = "RemoteMezzanineTableViewCell"
	fileprivate var cellIdentifier = "RemoteMezzCell"
	
	fileprivate var kModelContext = 0
	fileprivate var kObservationKeyPath = "remoteMezzes"

  fileprivate var kRemoteMezzContext = 1
  fileprivate var kRemoteMezzNameObservationKeyPath = "name"
  fileprivate var kRemoteMezzLocationObservationKeyPath = "location"
  fileprivate var kRemoteMezzCompanyObservationKeyPath = "company"

}
