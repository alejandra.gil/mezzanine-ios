//
//  WorkspaceContentScrollView.m
//  Mezzanine
//
//  Created by miguel on 07/04/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import "WorkspaceContentScrollView.h"
#import "MezzanineStyleSheet.h"

#import "Mezzanine-Swift.h"

@interface WorkspaceContentScrollView ()
{
  UIView *_feldsCorboardsBorderView;
}

@end


CGFloat WorkspaceContentSizeXPadding = 1.0;
static CGFloat marginsToAvoidMainAreaFloatingButtons = 60.0;

@implementation WorkspaceContentScrollView

- (void)layoutSubviews
{
  [super layoutSubviews];
  [self layoutSpacesContentWithPageSize:self.frame.size animated:NO];
}


-(void) layoutSpacesContentWithPageSize:(CGSize)pageSize animated:(BOOL)animated
{
  DLog(@"layoutSpacesContentWithPageSize %@", NSStringFromCGSize(pageSize));

  // Re-layout frames according to zoom scale
  // NB: In UIScrollView that contentSize and zoomScale are interrelated. ie, contentSize
  // is not the size of content at zoomScale 1.0 as one might expect, but size of content
  // relative to the scroll view's frame

  CGSize contentSize = [self calculateContentSize];
  // Don't let contentWidth go below frame width such that the scroll view will always bounce
  contentSize.width += WorkspaceContentSizeXPadding;
  self.contentSize = contentSize;

  [_workspaceViewController.presentationViewController updateFeldBoundViews];

  // Good debug spew to keep around ...
  //  DLog(@"WorkspaceViewController zoomScale=%f", contentScrollView.zoomScale);
  //  DLog(@"WorkspaceViewController contentSize=%@", NSStringFromCGSize(contentScrollView.contentSize));
  //  DLog(@"WorkspaceViewController presentationViewController frame=%@", NSStringFromCGRect(presentationViewController.view.frame));
  //  DLog(@"WorkspaceViewController spacesContentView.frame=%@", NSStringFromCGRect(spacesContentView.frame));
  
  [self updateContentViewLayout:animated];
  [_workspaceViewController updateWelcomeHint];
}


- (void)updateContentViewLayout:(BOOL)animated
{
  CGSize contentSize = [self calculateContentSize];
  CGFloat contentWidth = contentSize.width;

  CGSize triptychAreaSize = [self calculateTriptychAreaSize];
  CGSize extendedTriptychAreaSize = [self calculateExtendedTriptychAreaSize];

  CGFloat scaledContentHeight = self.frame.size.height/self.zoomScale;
  CGFloat xOffset = (extendedTriptychAreaSize.width - triptychAreaSize.width) / 2.0;
  CGFloat yOffset = (scaledContentHeight - triptychAreaSize.height)/2.0;


  void (^animations)() = ^{

    {
      //
      // Spaces Container view layout code
      //

      _workspaceViewController.workspaceContentView.frame = CGRectMake((NSInteger) 0.0,
                                                                    (NSInteger) 0.0,
                                                                    (NSInteger) contentWidth,
                                                                    (NSInteger) self.frame.size.height);
    }


    //
    // Presentation layout code
    //
    CGRect presentationViewFrame = CGRectMake((NSInteger)xOffset,
                                              (NSInteger)0.0,
                                              (NSInteger)triptychAreaSize.width,
                                              (NSInteger)scaledContentHeight);

    _workspaceViewController.presentationViewController.view.frame = presentationViewFrame;

    //
    // Windshield layout code
    //

    _workspaceViewController.windshieldViewController.view.frame = CGRectMake((NSInteger)0.0,
                                                                              (NSInteger)0.0,
                                                                              (NSInteger)extendedTriptychAreaSize.width,
                                                                              (NSInteger)scaledContentHeight);
    _workspaceViewController.windshieldViewController.shielderContainerFrame = CGRectMake((NSInteger)xOffset,
                                                                                          (NSInteger)yOffset,
                                                                                          (NSInteger)triptychAreaSize.width,
                                                                                          (NSInteger)triptychAreaSize.height);

    //
    // WelcomeHintView layout code
    //

    CGFloat welcomeHintOffsetX = xOffset + (_systemModel.felds.count == 3 ? triptychAreaSize.width / _systemModel.felds.count : 0) ;
    CGFloat welcomeHintWidth = triptychAreaSize.width / _systemModel.felds.count;

    _workspaceViewController.welcomeHintView.frame = CGRectMake((NSInteger)welcomeHintOffsetX,
                                                                (NSInteger)yOffset,
                                                                (NSInteger)welcomeHintWidth,
                                                                (NSInteger)triptychAreaSize.height);
  };
  
  if (animated)
    [UIView animateWithDuration:1.0/3.0 animations:animations completion:nil];
  else
    animations();

}


#pragma mark - Workspace Area Sizes

- (CGSize) calculateContentSize
{
  CGSize ts = [self calculateExtendedTriptychAreaSize];
  return CGSizeMake (ts.width*self.zoomScale, ts.height*self.zoomScale);
}


- (CGSize) calculateTriptychAreaSize
{
  CGSize pageSize = self.frame.size;
  pageSize.height -= marginsToAvoidMainAreaFloatingButtons * 2.0;
  NSInteger numberOfFeldsInIptych = _systemModel.felds.count;

  CGFloat iw = pageSize.width * numberOfFeldsInIptych;

  MZFeld *highestFeld = [self highestFeld];
  CGFloat height = iw/numberOfFeldsInIptych / highestFeld.aspectRatio;

  // When height is limiting, we must adapt the width of the feld
  if (height > pageSize.height / self.zoomScale)
  {
    height = pageSize.height / self.zoomScale;
    iw = height * numberOfFeldsInIptych * highestFeld.aspectRatio;
  }

  return CGSizeMake (iw, height);
}

// Extended Triptych Area Size includes margins to the right and left of the triptych
// when a single feld width is not the same as the screen.
- (CGSize) calculateExtendedTriptychAreaSize
{
  CGSize triptychAreaSize = [self calculateTriptychAreaSize];

  CGSize pageSize = [self pageSizeWithMargins];

  CGFloat xOffset = 0.0;
  if (self.frame.size.width > pageSize.width)
  {
    xOffset = (self.frame.size.width - triptychAreaSize.width/_systemModel.felds.count) / 2.0;
  }
  return CGSizeMake (2.0*xOffset + triptychAreaSize.width, triptychAreaSize.height);
}


- (CGSize) pageSizeForSpaceIndex:(NSInteger)index
{
  if (index < _systemModel.felds.count)
    return [self pageSizeWithMargins];
  else
    return self.frame.size;
}


- (CGSize) pageSizeWithMargins
{
  if ([_systemModel.felds count] == 0)
    return (CGSize){0.0, 0.0};
  
  CGSize pageSize = self.frame.size;
  pageSize.height -= (marginsToAvoidMainAreaFloatingButtons * 2.0);

  CGFloat contentScrollAspectRatio = pageSize.width / pageSize.height;
  CGFloat visibleAreaAspectRatio = [_systemModel.felds[0] aspectRatio];

  if (contentScrollAspectRatio < visibleAreaAspectRatio)
  {
    pageSize.width = self.frame.size.width;
  }
  else
  {
    pageSize.width =  [_systemModel.felds[0] aspectRatio] * pageSize.height;
  }
  return pageSize;
}


#pragma mark - Zoom Helpers

- (CGFloat) zoomStateAtZoomScale:(CGFloat)scale
{
  CGFloat state = 1.0 - (scale - self.minimumZoomScale) / (self.maximumZoomScale - self.minimumZoomScale);
  return state;
}

- (CGFloat) zoomState
{
  return [self zoomStateAtZoomScale:self.zoomScale];
}


#pragma mark - Felds Helpers


- (MZFeld *) highestFeld
{
  CGFloat height = 0.001; // Small value instead of zero here, in case of incorrect or corrupt feld information which would result in a divide by zero
  MZFeld *highestFeld = nil;
  for (MZFeld *feld in _systemModel.felds)
  {
    if (feld.feldRect.size.height > height)
    {
      height = feld.feldRect.size.height;
      highestFeld = feld;
    }
  }
  return highestFeld;
}


@end
