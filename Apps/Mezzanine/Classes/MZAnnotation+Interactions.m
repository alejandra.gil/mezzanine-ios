//
//  MZAnnotation+Interactions.m
//  Mezzanine
//
//  Created by Zai Chang on 10/26/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import "MZAnnotation+Interactions.h"
#import <objc/runtime.h>


@interface MZAnnotation ()

@property (nonatomic, copy) void (^creationCompletedBlock)();
@property (nonatomic, copy) void (^creationCancelledBlock)();
@property (nonatomic, copy) void (^creationEndedBlock)();
@property (nonatomic, copy) void (^gestureBeganBlock)(UIGestureRecognizer *);
@property (nonatomic, copy) void (^gestureEndedBlock)(UIGestureRecognizer *);

@end


@implementation MZAnnotation (Interactions)

static char *kMZAnnotationCreationCompletedBlockKey = "MZAnnotationCreationCompletedBlockKey";
static char *kMZAnnotationCreationCancelledBlockKey = "MZAnnotationCreationCancelledBlockKey";
static char *kMZAnnotationCreationEndedBlockKey = "MZAnnotationCreationEndedBlockKey";

static char *kMZAnnotationGestureBeganBlockKey = "MZAnnotationGestureBeganBlockKey";
static char *kMZAnnotationGestureEndedBlockKey = "MZAnnotationGestureEndedBlockKey";


-(void) setCreationCompletedBlock:(void (^)())completionBlock
{
  objc_setAssociatedObject(self, kMZAnnotationCreationCompletedBlockKey, completionBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}


-(void (^)()) creationCompletedBlock
{
  return objc_getAssociatedObject(self, kMZAnnotationCreationCompletedBlockKey);
}


-(void) setCreationCancelledBlock:(void (^)())cancelledBlock
{
  objc_setAssociatedObject(self, kMZAnnotationCreationCancelledBlockKey, cancelledBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}


-(void (^)()) creationCancelledBlock
{
  return objc_getAssociatedObject(self, kMZAnnotationCreationCancelledBlockKey);
}


-(void) setCreationEndedBlock:(void (^)())endedBlock
{
  objc_setAssociatedObject(self, kMZAnnotationCreationEndedBlockKey, endedBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}


-(void (^)()) creationEndedBlock
{
  return objc_getAssociatedObject(self, kMZAnnotationCreationEndedBlockKey);
}


-(void) setGestureBeganBlock:(void (^)())block
{
  objc_setAssociatedObject(self, kMZAnnotationGestureBeganBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}


-(void (^)()) gestureBeganBlock
{
  return objc_getAssociatedObject(self, kMZAnnotationGestureBeganBlockKey);
}


-(void) setGestureEndedBlock:(void (^)())block
{
  objc_setAssociatedObject(self, kMZAnnotationGestureEndedBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}


-(void (^)()) gestureEndedBlock
{
  return objc_getAssociatedObject(self, kMZAnnotationGestureEndedBlockKey);
}


-(void) beginCreationModeInView:(UIView*)view completion:(void (^)())completion cancelled:(void (^)())cancelled gestureBegan:(void (^)(UIGestureRecognizer *))gestureBegan gestureEnded:(void (^)(UIGestureRecognizer *))gestureEnded
{
  [NSException raise:@"NotImplementedException" format:@"MZAnnotation subclass should implement beginCreationModeInView:..."];
}


// To be called for cleanup purposes
-(void) endCreationMode
{
  self.creationCompletedBlock = nil;
  self.creationCancelledBlock = nil;
  self.creationEndedBlock = nil;
  self.gestureBeganBlock = nil;
  self.gestureEndedBlock = nil;
}


-(CGPoint) normalizedLocationForRecognizer:(UIGestureRecognizer*)recognizer
{
  UIView *view = recognizer.view;
  CGPoint location = [recognizer locationInView:view];
  CGPoint scale = CGPointMake(view.transform.a, view.transform.d);
  CGPoint normalizedLocation = CGPointMake(scale.x * location.x / CGRectGetWidth(recognizer.view.frame), scale.y * location.y / CGRectGetHeight(recognizer.view.frame));
  
//  DLog(@"location = %@", NSStringFromCGPoint(location));
//  DLog(@"scale = %@", NSStringFromCGPoint(scale));
//  DLog(@"normalizedLocation = %@", NSStringFromCGPoint(normalizedLocation));
  
  return normalizedLocation;
}


@end



@implementation MZFreehandAnnotation (Interactions)


-(void) beginCreationModeInView:(UIView*)view completion:(void (^)())completion cancelled:(void (^)())cancelled gestureBegan:(void (^)(UIGestureRecognizer *))gestureBegan gestureEnded:(void (^)(UIGestureRecognizer *))gestureEnded
{
  UIPanGestureRecognizer *recognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureRecognized:)];
  recognizer.minimumNumberOfTouches = 1;
  recognizer.maximumNumberOfTouches = 1;
  [view addGestureRecognizer:recognizer];
  
  self.creationCompletedBlock = ^{
    if (completion)
      completion();
  };
  
  self.creationCancelledBlock = ^{
    if (cancelled)
      cancelled();
  };
  
  self.creationEndedBlock = ^{
    [view removeGestureRecognizer:recognizer];    
  };

  self.gestureBeganBlock = gestureBegan;
  self.gestureEndedBlock = gestureEnded;
}


-(void) panGestureRecognized:(UIPanGestureRecognizer*)recognizer
{
  CGPoint normalizedLocation = [self normalizedLocationForRecognizer:recognizer];
  //NSUndoManager *undoManager = recognizer.view.undoManager;

  if (recognizer.state == UIGestureRecognizerStateBegan)
  {
    if (self.gestureBeganBlock)
      self.gestureBeganBlock(recognizer);
    //[undoManager beginUndoGrouping];
    
    [self.points removeAllObjects];
    [self.points addObject:[NSValue valueWithCGPoint:normalizedLocation]];
    self.needsRedraw = YES;
  }
  else if (recognizer.state == UIGestureRecognizerStateChanged)
  {
    [self.points addObject:[NSValue valueWithCGPoint:normalizedLocation]];
    self.needsRedraw = YES;
  }
  else if (recognizer.state == UIGestureRecognizerStateEnded)
  {
    if (self.gestureEndedBlock)
      self.gestureEndedBlock(recognizer);

    self.needsRedraw = YES;
    
    //[undoManager endUndoGrouping];
    
    if (self.creationCompletedBlock)
      self.creationCompletedBlock();
  }
  else if (recognizer.state == UIGestureRecognizerStateCancelled)
  {
    [self.points removeAllObjects];
    self.needsRedraw = YES;
    
    //[undoManager endUndoGrouping];
    
    if (self.creationCancelledBlock)
      self.creationCancelledBlock();
  }
}


-(void) endCreationMode
{
  if (self.creationEndedBlock)
    self.creationEndedBlock();
  
  [super endCreationMode];
}

@end



@implementation MZTextAnnotation (Interactions)

-(void) beginCreationModeInView:(UIView*)view completion:(void (^)())completion cancelled:(void (^)())cancelled
{
  // Todo
}

@end



@implementation MZArrowAnnotation (Interactions)

-(void) beginCreationModeInView:(UIView*)view completion:(void (^)())completion cancelled:(void (^)())cancelled
{
  // Todo
}

@end
