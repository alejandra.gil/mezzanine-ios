//
//  ProgressPieView.h
//  Mezzanine
//
//  Created by Miguel Sanchez Valdes on 23/07/13.
//  Copyright (c) 2013 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>

enum
{
  ProgressPieStateInvisible,
  ProgressPieStateActivity,
  ProgressPieStateProgress
};
typedef NSUInteger ProgressPieState;

@interface ProgressPieView : UIView
{
  BOOL shouldDrawPie;
}

@property (nonatomic, assign) CGFloat progress;
@property (nonatomic, assign) CGFloat borderWidth;
@property (nonatomic, strong) UIColor *pieColor;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicatorView;
@property (nonatomic, assign) ProgressPieState state;

@end
