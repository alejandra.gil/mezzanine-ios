//
//  ColourPickerViewController.m
//  Mezzanine
//
//  Created by Zai Chang on 11/1/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import "ColourPickerViewController.h"


@interface ColourPickerViewController ()

@end


@implementation ColourPickerViewController

@synthesize selectedColour;
@synthesize selectedColourChanged;


-(id) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self)
  {
    numberOfRows = 7;
    numberOfColumns = 5;
    colours = [[NSMutableArray alloc] init];

    for (NSInteger i=0; i<numberOfRows; i++)
    {
      for (NSInteger j=0; j<numberOfColumns; j++)
      {
        CGFloat hue = ABS((i - 1.0) / (CGFloat) (numberOfRows - 1.0));
        CGFloat saturation = (i == 0) ? 0.0 : 1.0;
        CGFloat brightness = 0.1 + 0.9 * (j + 1.0) / (CGFloat)numberOfColumns;
        
        //DLog(@"color at index: %d : %f  %f  %f", [self indexForRow:i column:j], hue, saturation, brightness);
        UIColor *color = [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1.0];
        [colours addObject:color];
      }
    }
  }
  return self;
}




-(NSInteger) indexForRow:(NSInteger)row column:(NSInteger)column
{
  return (row * numberOfColumns) + column;
}


-(void) loadView
{
  [super loadView];
  
  self.view.userInteractionEnabled = YES;
  
  scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
  scrollView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
  scrollView.backgroundColor = [UIColor blackColor];
  [self.view addSubview:scrollView];
  
  contentView = [[UIView alloc] initWithFrame:CGRectZero];
  [scrollView addSubview:contentView];
  
  for (NSInteger i=0; i<numberOfRows; i++)
  {
    for (NSInteger j=0; j<numberOfColumns; j++)
    {
      NSInteger index = [self indexForRow:i column:j];
      UIButton *colourView = [UIButton buttonWithType:UIButtonTypeCustom];
      colourView.tag = index;
      colourView.backgroundColor = colours[index];
      [colourView addTarget:self action:@selector(colourViewTapped:) forControlEvents:UIControlEventTouchUpInside];
      [contentView addSubview:colourView];
    }
  }
}


-(void) viewDidLayoutSubviews
{
  [super viewDidLayoutSubviews];
  
  CGFloat xMargin = 4.0;
  CGFloat yMargin = 4.0;
  CGFloat squareWidth = 60.0;
  CGFloat squareHeight = 60.0;
  
  CGFloat totalWidth = CGRectGetWidth(self.view.bounds);
  CGFloat totalHeight = yMargin + (squareHeight + yMargin) * numberOfRows;
  
  CGFloat contentWidth = squareWidth * numberOfColumns + xMargin * (numberOfColumns - 1);
  
  CGRect contentViewFrame = CGRectMake((totalWidth - contentWidth)/2.0, 0, totalWidth, totalHeight);
  contentView.frame = contentViewFrame;
  scrollView.contentSize = CGSizeMake(contentViewFrame.size.width, MAX(contentViewFrame.size.height, scrollView.frame.size.height + 1.0));
  
  for (NSInteger i=0; i<numberOfRows; i++)
  {
    for (NSInteger j=0; j<numberOfColumns; j++)
    {
      NSInteger index = [self indexForRow:i column:j];
      UIView *colourView = (contentView.subviews)[index];
      CGFloat x = j * (squareWidth + xMargin);
      CGFloat y = yMargin + i * (squareHeight + yMargin);
      colourView.frame = CGRectMake(x, y, squareWidth, squareHeight);
    }
  }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) colourViewTapped:(UIView*)sender
{
  self.selectedColour = sender.backgroundColor;
  
  if (selectedColourChanged)
    selectedColourChanged(self.selectedColour);
}


@end
