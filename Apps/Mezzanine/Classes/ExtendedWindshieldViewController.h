//
//  ExtendedWindshieldViewController.h
//  Mezzanine
//
//  Created by miguel on 2/3/15.
//  Copyright (c) 2015 Oblong Industries. All rights reserved.
//

#import "WindshieldViewController.h"

@interface ExtendedWindshieldViewController : WindshieldViewController

- (void)presentExtendedWindshieldWithCompletion:(void (^)())completionBlock;
- (void)hideExtendedWindshield;
- (void)refreshLayout;

@end
