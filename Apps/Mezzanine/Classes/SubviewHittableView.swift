//
//  SubviewHittableView.swift
//  Mezzanine
//
//  Created by miguel on 13/3/17.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import UIKit

class SubviewHittableView: UIView {

  override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
    let hitView = super.hitTest(point, with: event)
    if hitView == self {
      return nil
    }
    else {
      return hitView
    }
  }
}
