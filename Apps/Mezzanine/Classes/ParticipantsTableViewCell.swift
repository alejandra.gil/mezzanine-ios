//
//  ParticipantsTableViewCell.swift
//  Mezzanine
//
//  Created by Miguel Sanchez on 29/6/15.
//  Copyright (c) 2015 Oblong Industries. All rights reserved.
//

import UIKit

class ParticipantsTableViewCell: UITableViewCell {

  @IBOutlet var participantTypeImageView: UIImageView!
  @IBOutlet var participantTypeModifierImageView: UIImageView!
  @IBOutlet var participantNameLabel: UILabel!
  @IBOutlet var accessoryLabel: UILabel!
  @IBOutlet var cancelInvitationButton: UIButton!

  let styleSheet = MezzanineStyleSheet.shared()

  weak var systemModel: MZSystemModel?
  weak var communicator: MZCommunicator?

  override func awakeFromNib() {

    super.awakeFromNib()

    let styleSheet = MezzanineStyleSheet.shared()
    self.backgroundColor = UIColor.clear

    participantNameLabel.font = UIFont.dinMedium(ofSize: 17.0)
    participantNameLabel.textColor = styleSheet?.grey170Color
  }
  
  deinit
  {
    endObservingParticipant()
  }
  
  func updateParticipantTypeImageView()
  {
    if (participant as? MZRemoteParticipant != nil)
    {
      var rpAvatar: UIImage?
      
      if (participant as! MZRemoteParticipant).uid == communicator?.provenance {
        rpAvatar =  UIImage(named:"mobile-rp-self-icon.png")?.withRenderingMode(.alwaysTemplate)
      } else {
        rpAvatar = UIImage(named:"mobile-rp-icon.png")?.withRenderingMode(.alwaysTemplate)
      }
      
      participantTypeImageView.image = rpAvatar
      participantTypeModifierImageView.image = nil
      participantTypeImageView.tintColor = styleSheet?.grey190Color
    }
    else if (participant as? MZRemoteMezz != nil)
    {
      participantTypeImageView.image = UIImage(named: "mobile-mezz-icon-header.png")?.withRenderingMode(.alwaysTemplate)
      participantTypeModifierImageView.image = nil
      participantTypeImageView.tintColor = styleSheet?.grey190Color
    }
    else if (participant as? MZInfopresenceCall != nil)
    {
      participantTypeImageView.image = UIImage(named: "mobile-mezz-icon-header.png")?.withRenderingMode(.alwaysTemplate)
      participantTypeModifierImageView.image = UIImage(named: "mobile-infopresence-time-icon.png")
      participantTypeImageView.tintColor = styleSheet?.grey90Color
    }
    else if (participant as? MZMezzanine != nil)
    {
      participantTypeImageView.image = UIImage(named: "mobile-this-mezz-icon-header.png")?.withRenderingMode(.alwaysTemplate)
      participantTypeModifierImageView.image = nil
      participantTypeImageView.tintColor = styleSheet?.grey190Color
    }
  }

  func updateParticipantNameLabel()
  {
    if let remoteParticipant = participant as? MZRemoteParticipant
    {
      participantNameLabel.text = remoteParticipant.displayName
      participantNameLabel.textColor = styleSheet?.grey190Color
    }
    if let pendingInviteCall = participant as? MZInfopresenceCall
    {
      let remoteMezz = systemModel!.remoteMezz(withUid: pendingInviteCall.uid)
      participantNameLabel.text = remoteMezz?.name
      participantNameLabel.textColor = styleSheet?.grey90Color
    }
    else if let aMezzanine = participant as? MZMezzanine
    {
      participantNameLabel.text = aMezzanine.name
      participantNameLabel.textColor = styleSheet?.grey190Color
    }
  }


  func updateCancelInvitationButton()
  {
    cancelInvitationButton?.isHidden = (participant as? MZInfopresenceCall != nil) ? false : true
  }


  @IBAction func cancelInvitation(_ sender: AnyObject)
  {
    if let pendingInviteCall = participant as? MZInfopresenceCall {
        communicator?.requestor.requestInfopresenceOutgoingInviteCancelRequest(pendingInviteCall.uid)
    }
  }
  

  var participant: Any! {
    willSet(aParticipant)
    {
      if  self.participant != nil
      {
        endObservingParticipant()
      }
    }

    didSet
    {
      updateParticipantTypeImageView()
      updateParticipantNameLabel()
      updateCancelInvitationButton()

      beginObservingParticipant()
    }
  }

  func beginObservingParticipant()
  {
    if let remoteParticipant = participant as? MZRemoteParticipant
    {
      beginObservingRemoteParticipant(remoteParticipant)
    }
    else if let aMezzanine = participant as? MZRemoteMezz
    {
      beginObservingRemoteMezz(aMezzanine)
    }
    else if let aMezzanine = participant as? MZMezzanine
    {
      beginObservingMezzanine(aMezzanine)
    }
  }

  func endObservingParticipant()
  {
    if let remoteParticipant = participant as? MZRemoteParticipant
    {
      endObservingRemoteParticipant(remoteParticipant)
    }
    else if let aMezzanine = participant as? MZRemoteMezz
    {
      endObservingRemoteMezz(aMezzanine)
    }
    else if let aMezzanine = participant as? MZMezzanine
    {
      endObservingMezzanine(aMezzanine)
    }
  }

  fileprivate var kRemoteMezzContext = 0
  fileprivate var kRemoteParticipantContext = 0
  fileprivate var kNameObservationKeyPath = "name"
  fileprivate var kDisplayNameObservationKeyPath = "displayName"
  fileprivate var kCollaborationStateObservationKeyPath = "collaborationState"


  func beginObservingRemoteMezz(_ participant: MZRemoteMezz) {
    participant.addObserver(self, forKeyPath: kNameObservationKeyPath, options: .new, context: &kRemoteMezzContext)
    participant.addObserver(self, forKeyPath: kCollaborationStateObservationKeyPath, options: .new, context: &kRemoteMezzContext)
  }

  func endObservingRemoteMezz(_ participant: MZRemoteMezz) {
    participant.removeObserver(self, forKeyPath: kNameObservationKeyPath)
    participant.removeObserver(self, forKeyPath: kCollaborationStateObservationKeyPath)
  }

  func beginObservingMezzanine(_ participant: MZMezzanine) {
    participant.addObserver(self, forKeyPath: kNameObservationKeyPath, options: .new, context: &kRemoteMezzContext)
  }

  func endObservingMezzanine(_ remoteMezz: MZMezzanine) {
    remoteMezz.removeObserver(self, forKeyPath: kNameObservationKeyPath)
  }

  func beginObservingRemoteParticipant(_ participant: MZRemoteParticipant) {
    participant.addObserver(self, forKeyPath: kDisplayNameObservationKeyPath, options: .new, context: &kRemoteParticipantContext)
  }

  func endObservingRemoteParticipant(_ participant: MZRemoteParticipant) {
    participant.removeObserver(self, forKeyPath: kDisplayNameObservationKeyPath)
  }

  override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
    if (context == &kRemoteMezzContext) {
      switch keyPath {
        case kNameObservationKeyPath?:
          updateParticipantNameLabel()
        case kCollaborationStateObservationKeyPath?:
          updateParticipantTypeImageView()
          updateCancelInvitationButton()
        default:
          break
      }
    }
    else if context == &kRemoteParticipantContext {
      switch keyPath {
        case kNameObservationKeyPath?:
          updateParticipantNameLabel()
        default:
          break
      }
    }
  }


}
