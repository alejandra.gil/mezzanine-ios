//
//  NSUserDefaults+MezzanineSettings.m
//  Mezzanine
//
//  Created by Zai Chang on 6/17/11.
//  Copyright 2011 Oblong Industries. All rights reserved.
//

#import "NSUserDefaults+MezzanineSettings.h"


#define AppVersionKey @"AppVersion"

#define WindshieldParallaxEnabledKey @"WindshieldParallaxEnabled"
#define WindshieldContinuousDragKey @"WindshieldContinuousDrag"

#define AnnotationPrimaryColourKey @"AnnotationPrimaryColour"
#define LastConnectedMezzKey @"LastConnectedMezzKey"

#define DisplayName @"DisplayName"


@implementation NSUserDefaults (MezzanineSettings)

+(void) registerMezzanineSettings
{
  // Register defaults
  NSDictionary *defaults = @{WindshieldParallaxEnabledKey: @NO,
                             WindshieldContinuousDragKey: @NO,
                             };
  [[NSUserDefaults standardUserDefaults] registerDefaults:defaults];

  NSString *version =[[[NSBundle mainBundle] infoDictionary] valueForKey:@"CFBundleVersion"];
  [[NSUserDefaults standardUserDefaults] setObject:version forKey:AppVersionKey];
  
  [[NSUserDefaults standardUserDefaults] synchronize];
}


-(UIColor *) annotationPrimaryColor
{
  NSString *colorAsString = [self objectForKey:AnnotationPrimaryColourKey];
  if (colorAsString)
  {
    CIColor *color = [CIColor colorWithString:colorAsString];
    return [UIColor colorWithCIColor:color];
  }
  return nil;
}


-(void) setAnnotationPrimaryColor:(UIColor *)annotationPrimaryColor
{
  CIColor *color = [CIColor colorWithCGColor:annotationPrimaryColor.CGColor];
  [self setObject:color.stringRepresentation forKey:AnnotationPrimaryColourKey];
}


-(NSString *) lastConnectedMezz
{
  return [self objectForKey:LastConnectedMezzKey];
}


-(void) setLastConnectedMezz:(NSString *)lastConnectedMezz
{
  [self setObject:lastConnectedMezz forKey:LastConnectedMezzKey];
  [self synchronize];
}


- (NSString *)displayName
{
  return [self objectForKey:DisplayName];
}


- (void)setDisplayName:(NSString *)displayName
{
  [self setObject:displayName forKey:DisplayName];
  [self synchronize];
}


- (BOOL)windshieldParallaxEnabled
{
  return [self boolForKey:WindshieldParallaxEnabledKey];
}


- (void)setWindshieldParallaxEnabled:(BOOL)value
{
  [self setBool:value forKey:WindshieldParallaxEnabledKey];
  [self synchronize];
}


- (BOOL)windshieldContinuousDrag
{
  return [self boolForKey:WindshieldContinuousDragKey];
}


- (void)setWindshieldContinuousDrag:(BOOL)value
{
  [self setBool:value forKey:WindshieldContinuousDragKey];
  [self synchronize];
}

@end
