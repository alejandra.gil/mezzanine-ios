//
//  WorkspaceContentScrollView.h
//  Mezzanine
//
//  Created by miguel on 07/04/14.
//  Copyright (c) 2014 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WorkspaceViewController;

extern CGFloat WorkspaceContentSizeXPadding;

@interface WorkspaceContentScrollView : UIScrollView

@property (nonatomic, strong) MZSystemModel *systemModel;
@property (nonatomic, weak) WorkspaceViewController *workspaceViewController;

- (void) updateContentViewLayout:(BOOL)animated;
- (CGSize) pageSizeForSpaceIndex:(NSInteger)index;

// Workspace Area Sizes
- (CGSize) calculateContentSize;
- (CGSize) calculateTriptychAreaSize;
- (CGSize) calculateExtendedTriptychAreaSize;

@end
