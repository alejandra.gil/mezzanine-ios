//
//  AnnotationsLayer.m
//  Mezzanine
//
//  Created by Zai Chang on 10/22/12.
//  Copyright (c) 2012 Oblong Industries. All rights reserved.
//

#import "AnnotationsContainerLayer.h"


@implementation AnnotationsContainerLayer

-(void) dealloc
{
  self.item = nil;
}


-(void) setItem:(MZItem *)item
{
  if (_item == item)
    return;
  
  [_item removeObserver:self forKeyPath:@"annotations"];
  _item = item;
  [_item addObserver:self forKeyPath:@"annotations" options:(NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew) context:nil];
  
  [self refreshAnnotationLayers];
}


-(void) layoutSublayers
{
  [super layoutSublayers];
  
  for (CALayer *layer in self.sublayers)
    layer.frame = self.bounds;
}


-(void) addLayerForAnnotation:(MZAnnotation*)annotation
{
  Class layerClass = [annotation CALayerClass];
  CALayer *layer = [layerClass layer];
  layer.frame = self.bounds;
  layer.delegate = annotation;
  [annotation updateLayer:layer];
  [annotation addObserver:self forKeyPath:@"needsRedraw" options:0 context:nil];
  
  [self addSublayer:layer];
}


-(CALayer*) layerForAnnotation:(MZAnnotation*)annotation
{
  if (!annotation)
    return nil;
  
  for (CALayer *layer in self.sublayers)
    if (layer.delegate == annotation)
      return layer;
  
  return nil;
}


-(void) removeLayerForAnnotation:(MZAnnotation*)annotation
{
  CALayer *layer = [self layerForAnnotation:annotation];
  if (layer)
  {
    [(MZAnnotation *)layer.delegate removeObserver:self forKeyPath:@"needsRedraw"];
    layer.delegate = nil;
    [layer removeFromSuperlayer];
  }
}


-(void) clearAnnotationLayers
{
  NSArray *sublayers = [self.sublayers copy];
  for (CALayer *layer in sublayers)
  {
    [(MZAnnotation *)layer.delegate removeObserver:self forKeyPath:@"needsRedraw"];
    layer.delegate = nil;
    [layer removeFromSuperlayer];
  }
}


-(void) refreshAnnotationLayers
{
  [self clearAnnotationLayers];

  for (MZAnnotation *annotation in _item.annotations)
  {
    [self addLayerForAnnotation:annotation];
  }
}


-(void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
  if ([object isKindOfClass:[MZItem class]])
  {
    if ([keyPath isEqual:@"annotations"])
    {
      NSArray *deleted = change[NSKeyValueChangeOldKey];
      for (MZAnnotation *annotation in deleted)
        [self removeLayerForAnnotation:annotation];
      
      NSArray *inserted = change[NSKeyValueChangeNewKey];
      for (MZAnnotation *annotation in inserted)
        [self addLayerForAnnotation:annotation];
    }
  }
  
  if ([object isKindOfClass:[MZAnnotation class]])
  {
    MZAnnotation *annotation = (MZAnnotation*)object;
    if ([keyPath isEqual:@"needsRedraw"])
    {
      if (annotation.needsRedraw)
      {
        CALayer *layer = [self layerForAnnotation:annotation];
        [annotation updateLayer:layer];
        [layer needsDisplay];
        annotation.needsRedraw = NO;
      }
    }
  }
}

@end

