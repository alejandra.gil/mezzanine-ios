//
//  MiniMapViewController.swift
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 23/03/2017.
//  Copyright © 2017 Oblong Industries. All rights reserved.
//

import UIKit

protocol MiniMapViewControllerDelegate: NSObjectProtocol {
  func miniMapDidTap(_ feld: MZFeld?, surface: MZSurface?)
  func miniMapDidSetContentFrame(_ contentFrame: CGRect)
  func miniMapDidMagnified(_ isMagnified: Bool)
}

struct MiniMapViewConstants {
  var feldMargin: CGFloat
  var feldSide: CGFloat
  var surfaceMargin: CGFloat
}

class MiniMapViewController: UIViewController {
  
  let feldContainer = UIView()
  
  fileprivate var gestureRecognizer = UILongPressGestureRecognizer()
  fileprivate var constants: MiniMapViewConstants!
  
  weak var delegate: MiniMapViewControllerDelegate?
  var workspaceGeometry = WorkspaceGeometry()
  var systemModel: MZSystemModel?
  
  var currentFocus: Any? {
    didSet {
      updateHighlightedFocus()
    }
  }
  
  var initialZoomLevel: ZoomLevel?

  deinit {
    print("MiniMapViewController deinit")
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    self.view.accessibilityIdentifier = "Minimap"
    
    constants = MiniMapViewConstants(feldMargin: 2.0, feldSide: self.traitCollection.isRegularWidth() ? 30.0 : 22.0, surfaceMargin: 6.0)
    
    currentFocus = nil
    
    let hittableView = SubviewHittableView(frame: self.view.frame)
    hittableView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    view = hittableView
    gestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.panGesture(_:)))
    gestureRecognizer.minimumPressDuration = 0.15
    feldContainer.addGestureRecognizer(gestureRecognizer)

    self.view.addSubview(feldContainer)
    self.view.backgroundColor = UIColor.clear
  }
  
  
  // MARK: UI Updates
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()

    feldContainer.frame.centerViewHorizontallyInView(self.view)
  }
  
  func updateLayout() {
    feldContainer.frame = self.view.bounds
    buildMap()
    updateHighlightedFocus()
  }

  func updateHighlightedFocus() {
    if currentFocus == nil {
      didFocusToWorkspace()
    }
    else if currentFocus is MZSurface {
      didFocusToSurface(currentFocus as! MZSurface)

    }
    else if currentFocus is MZFeld {
      didFocusToFeld(currentFocus as! MZFeld)
    }
  }

  
  // MARK: Initial map
  
  func buildMap() {
    guard let systemModel = systemModel else { return }

    for subview in feldContainer.subviews {
      subview.removeFromSuperview()
    }
    
    var accumulatedWidth: CGFloat = 0.0
    for (surfaceIndex, surface) in (systemModel.surfaces as NSArray).enumerated() {
      for (feldIndex, feld) in ((surface as AnyObject).felds as NSArray).enumerated() {
        let feldSize = getNormalizedSize(workspaceGeometry.rectForFeld(feld as! MZFeld).size)
        let tappableFeldView = TappableFeldView(frame: CGRect(x: accumulatedWidth, y: 0, width: feldSize.width, height: feldContainer.frame.height), constants: constants)
        tappableFeldView.accessibilityIdentifier = "Minimap-button"
        tappableFeldView.accessibilityLabel = String(format:"Minimap-feld-\(surfaceIndex)-\(feldIndex)")
        tappableFeldView.delegate = self
        tappableFeldView.feldSize = feldSize
        tappableFeldView.feld = feld as? MZFeld
        tappableFeldView.surface = surface as? MZSurface
        tappableFeldView.frame.centerViewVerticallyInView(feldContainer)
        feldContainer.addSubview(tappableFeldView)
        
        accumulatedWidth = accumulatedWidth + feldSize.width + constants.feldMargin
      }
      accumulatedWidth = accumulatedWidth + constants.surfaceMargin
    }
    
    feldContainer.frame.size.width = accumulatedWidth - constants.surfaceMargin
    feldContainer.frame.centerViewHorizontallyInView(self.view)
    delegate?.miniMapDidSetContentFrame(feldContainer.frame)
  }
  
  func getNormalizedSize(_ size: CGSize) -> CGSize {
    let ratio = size.width / size.height
    var width: CGFloat = constants.feldSide
    var height: CGFloat = width / ratio
    
    if ratio < 1.0 {
      height = constants.feldSide
      width = height * ratio
    }
    
    width += constants.feldMargin
    return CGSize(width: width, height: height)
  }
  
  
  // MARK: Magnifying map
  
  @objc func panGesture(_ panGesture: UILongPressGestureRecognizer) {
    maximizeFeldContainer()
    
    let locationPoint: CGPoint = panGesture.location(in: self.feldContainer)
    if panGesture.state == .changed || panGesture.state == .began {
      maximizeFeldAtLocation(locationPoint)
    } else if panGesture.state == .ended {
      guard let feldView = feldAtLocation(locationPoint) else { resetFelds(); resetFeldContainer(); return }
      delegate?.miniMapDidTap(feldView.feld, surface: feldView.surface)
      resetFelds()
      resetFeldContainer()
    } else {
      resetFelds()
      resetFeldContainer()
    }
  }
  
  func feldAtLocation(_ location: CGPoint) -> TappableFeldView? {
    var feldView: TappableFeldView?
    
    for feld in feldContainer.subviews {
      if feld.frame.contains(location) {
        feldView = feld as? TappableFeldView
        break
      }
    }
    
    return feldView
  }
  
  func maximizeFeldContainer() {
    if feldContainer.transform.d > 1.0 {
      return
    }
    
    delegate?.miniMapDidMagnified(true)
    
    // FIXME: Temporary solution until the scrollable implementation
    let screenWidth = UIScreen.main.bounds.width
    let convertedFrame = view.convert(feldContainer.frame, to: nil)

    // 15.0 is a margin to not have the view on the left edge of the screen
    let maxZoom = min(3.0, screenWidth / (feldContainer.frame.width + (UIScreen.main.bounds.midX - convertedFrame.midX) + 15.0))
    UIView.animate(withDuration: 0.15, animations: {
      self.feldContainer.transform = CGAffineTransform(scaleX: maxZoom, y: maxZoom)
    })
  }

  func resetFeldContainer() {
    delegate?.miniMapDidMagnified(false)

    UIView.animate(withDuration: 0.15, animations: {
      self.feldContainer.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
    })
  }
  
  func maximizeFeldAtLocation(_ location: CGPoint) {
    for feld in feldContainer.subviews {
      if feld.frame.contains(location) {
        UIView.animate(withDuration: 0.15, animations: {
          (feld as! TappableFeldView).maximizedView.alpha = 0.8
          (feld as! TappableFeldView).maximizedView.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        })
      } else {
        UIView.animate(withDuration: 0.15, animations: {
          (feld as! TappableFeldView).maximizedView.alpha = 0.0
          (feld as! TappableFeldView).maximizedView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
      }
    }
  }
  
  func resetFelds() {
    for feld in feldContainer.subviews {
      UIView.animate(withDuration: 0.15, animations: {
        (feld as! TappableFeldView).maximizedView.alpha = 0.0
        (feld as! TappableFeldView).maximizedView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
      })
    }
  }
  
  
  // MARK: Focus updates
  
  func didFocusToWorkspace() {
    for aFeld in feldContainer.subviews {
      (aFeld as! TappableFeldView).highlighted = false
    }
  }
  
  func didFocusToSurface(_ surface: MZSurface) {
    guard let systemModel = systemModel else { return }

    for aFeld in feldContainer.subviews {
      if (aFeld as! TappableFeldView).surface == surface {
        (aFeld as! TappableFeldView).highlighted = initialZoomLevel == .surface && systemModel.surfaces.count == 1 ? false : true
      } else {
        (aFeld as! TappableFeldView).highlighted = false
      }
    }
  }
  
  func didFocusToFeld(_ feld: MZFeld) {
    for aFeld in feldContainer.subviews {
      if (aFeld as! TappableFeldView).feld == feld {
        (aFeld as! TappableFeldView).highlighted = true
      } else {
        (aFeld as! TappableFeldView).highlighted = false
      }
    }
  }
}


extension MiniMapViewController: FeldViewDelegate {
  func feldViewDidTap(_ feld: MZFeld?, surface: MZSurface?) {
    delegate?.miniMapDidTap(feld, surface: surface)
  }
}


extension CGRect {
  
  mutating func centerViewHorizontallyInView(_ view: UIView) {
    origin.x = (view.frame.width - width) / 2
  }
  
  mutating func centerViewVerticallyInView(_ view: UIView) {
    origin.y = (view.frame.height - height) / 2
  }
}


// MARK: FeldView

protocol FeldViewDelegate: NSObjectProtocol {
  func feldViewDidTap(_ feld: MZFeld?, surface: MZSurface?)
}

class TappableFeldView: UIView {
  
  weak var delegate: FeldViewDelegate?
  var feld: MZFeld?
  var surface: MZSurface?
  
  var button: UIButton!
  var highlighted: Bool = false {
    didSet {
      self.animateStatus()
    }
  }
  
  var feldSize: CGSize! {
    didSet {
      feldView = UIView(frame: CGRect(x: constants.feldMargin / 2, y: self.frame.midY - feldSize.height/2, width: feldSize.width - constants.feldMargin / 2, height: feldSize.height))
      feldView.backgroundColor = UIColor(red: 178.0 / 255.0, green: 215.0 / 255.0, blue: 255.0 / 255.0, alpha: 1.0)
      feldView.layer.cornerRadius = 1.0;
      
      self.alpha = 0.4;
      self.addSubview(feldView)
      self.insertSubview(feldView, belowSubview: button)
      
      maximizedView = UIView(frame: feldView.frame)
      maximizedView.backgroundColor = UIColor(red: 178.0 / 255.0, green: 215.0 / 255.0, blue: 255.0 / 255.0, alpha: 1.0)
      maximizedView.layer.cornerRadius = 1.0;
      maximizedView.alpha = 0.0
      self.insertSubview(maximizedView, aboveSubview: feldView)
    }
  }
  
  var maximizedView: UIView!
  var feldView: UIView!
  var constants: MiniMapViewConstants!
  
  
  required init(frame: CGRect, constants: MiniMapViewConstants) {
    super.init(frame: frame)
   
    self.constants = constants
    
    button = UIButton(frame: CGRect(x: 0, y: 0, width: frame.width, height: frame.height))
    button.addTarget(self, action: #selector(self.feldViewTapAction(_:)), for: .touchUpInside)
    addSubview(button!)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func animateStatus() {
    UIView.animate(withDuration: 0.2, delay: 0, options: .beginFromCurrentState, animations: {
      self.alpha = self.highlighted ? 1.0 : 0.4
      }, completion: nil)
  }
  
  @objc func feldViewTapAction(_ sender: AnyObject) {
    delegate?.feldViewDidTap(self.feld, surface: self.surface)
  }
}
