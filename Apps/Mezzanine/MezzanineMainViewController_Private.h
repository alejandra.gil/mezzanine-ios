//
//  MezzanineMainViewController_Private.h
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 07/07/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

#import "MezzanineMainViewController.h"
#import "WorkspaceOpeningViewController.h"
#import "PortfolioViewController.h"
#import "LiveStreamsViewController.h"

@class VideoChatViewController;

@interface MezzanineMainViewController ()

// IBOutlets
@property (nonatomic, strong) IBOutlet UIView *workspaceContainerView;
@property (nonatomic, strong) IBOutlet UIView *workspaceOpeningViewContainer;
@property (nonatomic, strong) IBOutlet UIView *binsContainerView;
@property (nonatomic, strong) IBOutlet UIView *liveStreamsContainerView;
@property (nonatomic, strong) IBOutlet UIView *portfolioContainerView;
@property (nonatomic, strong) IBOutlet UIView *binsSeparatorView;
@property (nonatomic, strong) IBOutlet UIView *vtcBubbleContainerView;

@property (nonatomic, strong) IBOutlet NSLayoutConstraint *liveStreamsBinHeightConstraint;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *binsContainerHeightConstraint;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *binsSeparatorViewTopConstraint;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *binsBottomConstraint;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *vtcBubbleContainerTopConstraint;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *vtcBubbleContainerTrailingConstraint;

// Controllers
@property (nonatomic, strong) PortfolioViewController *portfolioViewController;
@property (nonatomic, strong) LiveStreamsViewController *liveStreamsViewController;


- (void)initializeWorkspaceViewController;
- (void)initializePortfolioViewController;

@end
