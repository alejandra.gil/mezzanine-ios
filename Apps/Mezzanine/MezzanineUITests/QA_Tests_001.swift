//
//  QA_Tests_001.swift
//  Mezzanine
//
//  Created by Michael Miller on 11/17/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

//PREREQUISITES
//  - Triptych or single screen Mezz
//  - At least one connected live stream
//  - System configured to have a whiteboard
//  - M2M Invites enabled
//  - At least one other Mezzanine available for Infopresence
//  - Portfolio tap enabled
//  - Analytics disabled

import Foundation
import XCTest
import ObjectiveC

@available(iOS 9.0, *)

class QA_Tests_001: XCTestCase {
    
    private let SHORT_TAP_DURATION = 0.1
    private let LONG_TAP_DURATION = 1.0
    
    override func setUp() {
        super.setUp()
        setUpMezzServer()
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        
        super.tearDown()
    }
    
    
    func test_001_AddLiveStreamAndDeleteWindshield() {
        //Adds a live stream to the windshield, and then deletes it
        //Assumes at least one live stream is connected to Mezz, and workspace has no content
        
        let app = XCUIApplication()
        
        connectToServer()
        closeOrDiscardWorkspaceIfNeeded()
        
        app.collectionViews["LiveStreamsView.CollectionView"].cells.childrenMatchingType(.Other).element.childrenMatchingType(.Image).elementAtIndex(0).pressForDuration(LONG_TAP_DURATION, thenDragToElement: XCUIApplication().scrollViews.otherElements.staticTexts["Welcome to Mezzanine"])
        
        XCTAssertFalse(XCUIApplication().scrollViews.otherElements.staticTexts["Welcome to Mezzanine"].hittable)
        
        let liveStream: XCUIElement = app.scrollViews.descendantsMatchingType(.Image).elementAtIndex(0)
        liveStream.pressForDuration(SHORT_TAP_DURATION)
        XCUIApplication().tables.staticTexts["Delete"].tap()
        
        //We deleted the one live stream on the windshield
        //Thus the welcome screen should be visible
        XCTAssertTrue(XCUIApplication().scrollViews.otherElements.staticTexts["Welcome to Mezzanine"].hittable)
        
        disconnectFromServer()
    }

    func test_002_ClearRecentServers() {
        
        let app = XCUIApplication()
        
        //ensure the recent servers list has at least one Mezz in it
        connectToServer()
        disconnectFromServer()
        
        XCTAssertTrue(app.buttons["ConnectionViewRecentConnectionsButton"].hittable)
        
        app.buttons["ConnectionViewRecentConnectionsButton"].tap()
        app.navigationBars["Recent Servers"].buttons["ConnectionHistoryViewClearButton"].tap()
        app.alerts["Clear Recent Servers"].collectionViews.buttons["Okay"].tap()
        
        //make sure the recent servers button has disappeared
        XCTAssertFalse(app.buttons["ConnectionViewRecentConnectionsButton"].hittable)
        
    }
    
    func test_003_AddToRecentServers() {
        let app = XCUIApplication()
        let mezzList: [String] = [mezzServer!] //would be nice to add at least one more Mezz to this list
        
        for server in mezzList {
            connectToServer(server)
            disconnectFromServer()
        }
        
        app.buttons["ConnectionViewRecentConnectionsButton"].tap()
        
        for server in mezzList {
            XCTAssertTrue(app.staticTexts[server].exists)
        }
    }
    
    func test_004_WhiteBoardCapture() {
        _ = XCUIApplication()
        connectToServer()
        closeOrDiscardWorkspaceIfNeeded()
        
        let previousSlideCount = getPortfolioVisibleItemCount()
        takeWhiteboardCapture()
        waitToContinue(2)
        XCTAssertTrue(getPortfolioVisibleItemCount() == (previousSlideCount+1))

        disconnectFromServer()
    }
    
    func test_005_RotateAndCheckUI() {
        let app = XCUIApplication()
        connectToServer()
        stopPresentationIfNeeded()
        
        let deviceType = UIDevice.currentDevice().model
        if deviceType == "iPad" {
            XCUIDevice.sharedDevice().orientation = .LandscapeRight
            XCTAssertTrue(app.buttons["WorkspaceToolbar.ThisMezzanineButton"].hittable)
            XCTAssertTrue(app.buttons["WorkspaceToolbar.WorkspaceButton"].hittable)
            XCTAssertTrue(app.buttons["WorkspaceToolbar.InfopresenceButtonRegular"].hittable)
            XCTAssertTrue(app.buttons.elementAtIndex(0).hittable) //lock button
            XCTAssertTrue(app.buttons["PortfolioView.AddButton"].hittable)
            XCTAssertTrue(app.buttons["MORE"].hittable)
            XCTAssertTrue(app.buttons["PortfolioView.PresentButton"].hittable)
            
            XCUIDevice.sharedDevice().orientation = .PortraitUpsideDown
            XCTAssertTrue(app.buttons["WorkspaceToolbar.ThisMezzanineButton"].hittable)
            XCTAssertTrue(app.buttons["WorkspaceToolbar.WorkspaceButton"].hittable)
            XCTAssertTrue(app.buttons["WorkspaceToolbar.InfopresenceButtonRegular"].hittable)
            XCTAssertTrue(app.buttons.elementAtIndex(0).hittable)
            XCTAssertTrue(app.buttons["PortfolioView.AddButton"].hittable)
            XCTAssertTrue(app.buttons["MORE"].hittable)
            XCTAssertTrue(app.buttons["PortfolioView.PresentButton"].hittable)
            
            XCUIDevice.sharedDevice().orientation = .LandscapeLeft
            XCTAssertTrue(app.buttons["WorkspaceToolbar.ThisMezzanineButton"].hittable)
            XCTAssertTrue(app.buttons["WorkspaceToolbar.WorkspaceButton"].hittable)
            XCTAssertTrue(app.buttons["WorkspaceToolbar.InfopresenceButtonRegular"].hittable)
            XCTAssertTrue(app.buttons.elementAtIndex(0).hittable)
            XCTAssertTrue(app.buttons["PortfolioView.AddButton"].hittable)
            XCTAssertTrue(app.buttons["MORE"].hittable)
            XCTAssertTrue(app.buttons["PortfolioView.PresentButton"].hittable)

            XCUIDevice.sharedDevice().orientation = .Portrait
            XCTAssertTrue(app.buttons["WorkspaceToolbar.ThisMezzanineButton"].hittable)
            XCTAssertTrue(app.buttons["WorkspaceToolbar.WorkspaceButton"].hittable)
            XCTAssertTrue(app.buttons["WorkspaceToolbar.InfopresenceButtonRegular"].hittable)
            XCTAssertTrue(app.buttons.elementAtIndex(0).hittable)
            XCTAssertTrue(app.buttons["PortfolioView.AddButton"].hittable)
            XCTAssertTrue(app.buttons["MORE"].hittable)
            XCTAssertTrue(app.buttons["PortfolioView.PresentButton"].hittable)
        }
        
        else if (deviceType == "iPhone" || deviceType == "iPod touch") {
            //TODO: Figure out button index for lock button on phone
            
            XCUIDevice.sharedDevice().orientation = .LandscapeLeft
            XCTAssertFalse(app.buttons["WorkspaceToolbar.ThisMezzanineButton"].hittable)
            XCTAssertFalse(app.buttons["WorkspaceToolbar.WorkspaceButton"].hittable)
            XCTAssertFalse(app.buttons["WorkspaceToolbar.InfopresenceButtonRegular"].hittable)
            XCTAssertFalse(app.buttons["PortfolioView.AddButton"].hittable)
            XCTAssertFalse(app.buttons["MORE"].hittable)
            XCTAssertFalse(app.buttons["PortfolioView.PresentButton"].hittable)
            
            XCUIDevice.sharedDevice().orientation = .LandscapeRight
            XCTAssertFalse(app.buttons["WorkspaceToolbar.ThisMezzanineButton"].hittable)
            XCTAssertFalse(app.buttons["WorkspaceToolbar.WorkspaceButton"].hittable)
            XCTAssertFalse(app.buttons["WorkspaceToolbar.InfopresenceButtonRegular"].hittable)
            XCTAssertFalse(app.buttons["PortfolioView.AddButton"].hittable)
            XCTAssertFalse(app.buttons["MORE"].hittable)
            XCTAssertFalse(app.buttons["PortfolioView.PresentButton"].hittable)
            
            XCUIDevice.sharedDevice().orientation = .Portrait
            XCTAssertTrue(app.buttons["WorkspaceToolbar.ThisMezzanineButton"].hittable)
            XCTAssertTrue(app.buttons["WorkspaceToolbar.WorkspaceButton"].hittable)
            XCTAssertTrue(app.buttons["WorkspaceToolbar.InfopresenceButtonRegular"].hittable)
            XCTAssertTrue(app.buttons["PortfolioView.AddButton"].hittable)
            XCTAssertTrue(app.buttons["MORE"].hittable)
            XCTAssertTrue(app.buttons["PortfolioView.PresentButton"].hittable)
        }
        
        else
        {
            XCTFail("Am I an Apple TV?")
        }
    }

    func test_006_UnicodeTextSlide() {
        _ = XCUIApplication()
        connectToServer()
        closeOrDiscardWorkspaceIfNeeded()
        
        let previousSlideCount = getPortfolioVisibleItemCount()
        
        createTextSlide("English ABCDEFG\nStandard Emojis: 😍😘😗\nNew Emojis: 👮👮🏻👮🏼👮🏽👮🏾👮🏿\nSimplified Chinese: 简体中文\nCyrillic: русский\nCherokee: ᏣᎳᎩ ᎦᏬᏂᎯᏍᏗ")
        createTextSlide("עברית\nالعَرَبِية‎")
        
        XCTAssertTrue(getPortfolioVisibleItemCount() == (previousSlideCount+2))
        discardWorkspaceIfNeeded()
        disconnectFromServer()
    }
    
    func test_007_InvitesUI() {
        let app = XCUIApplication()
        connectToServer()
        
        app.buttons["WorkspaceToolbar.InfopresenceButtonRegular"].tap()
        XCTAssertTrue(app.scrollViews.otherElements.buttons["Invite other Mezzanines"].hittable)
        app.scrollViews.otherElements.buttons["Invite other Mezzanines"].tap()
        XCTAssertFalse(app.buttons["SELECT MEZZANINES TO INVITE"].enabled)
        
        if(!app.images["mobile-mezz-icon-large"].hittable) { //there is at least one Mezz in the list that can be invited
            app.tables["InfopresenceView.TableView"].staticTexts.elementAtIndex(0).tap() //this is (always?) the first Mezz in the list
            XCTAssertTrue(app.buttons["INVITE 1 MEZZANINE"].enabled, "Couldn't find INVITE 1 MEZZANINE button")
            app.tables["InfopresenceView.TableView"].staticTexts.elementAtIndex(0).tap()
        }
    }
    
    func test_008_PortfolioPopover() {
        let app = XCUIApplication()
        
        connectToServer()
        closeOrDiscardWorkspaceIfNeeded()
        createTextSlide("test_008")
        startPresentationIfNeeded()
        let numFelds = numPresentationFelds()
        
        if (numFelds == 3)
        {
            app.scrollViews.otherElements.otherElements["PresentationView"].childrenMatchingType(.Other).element.childrenMatchingType(.Other).elementBoundByIndex(1).tap()
        }
        
        else if(numFelds == 1) {
            app.scrollViews.childrenMatchingType(.Other).element.childrenMatchingType(.Other).elementBoundByIndex(1).tap()
        }
        
        XCTAssertTrue(app.tables.staticTexts["Local Preview"].hittable)
        XCTAssertTrue(app.tables.staticTexts["Delete"].hittable)
        
        stopPresentationIfNeeded()
    }
}

