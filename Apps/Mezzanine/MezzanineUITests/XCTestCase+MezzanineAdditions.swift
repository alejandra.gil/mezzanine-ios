//
//  XCTestCase+MezzanineAdditions.swift
//  Mezzanine
//
//  Created by miguel on 2/12/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

import Foundation

import XCTest

@available(iOS 9.0, *)
extension XCTestCase {
    
  func addNewSlide() {

    let app = XCUIApplication()

    app.buttons["PortfolioView.AddButton"].tap()
    app.tables.staticTexts["Text"].tap()

    let slidecreateviewcontrollerTextviewTextView = app.textViews["SlideCreateViewController.textView"]
    slidecreateviewcontrollerTextviewTextView.tap()
    slidecreateviewcontrollerTextviewTextView.typeText("Text")
    app.navigationBars["Create Text Slide"].buttons["Create"].tap()
  }

  func createTextSlide(text :String) {
    let app = XCUIApplication()
    app.buttons["PortfolioView.AddButton"].tap()
    app.tables.staticTexts["Text"].tap()
    let slidecreateviewcontrollerTextviewTextView = app.textViews["SlideCreateViewController.textView"]
    slidecreateviewcontrollerTextviewTextView.tap()
    slidecreateviewcontrollerTextviewTextView.typeText(text)
    app.navigationBars["Create Text Slide"].buttons["Create"].tap()
  }
    
  func takeWhiteboardCapture() {
    let app = XCUIApplication()
    app.buttons["PortfolioView.AddButton"].tap()
    app.tables.staticTexts["Whiteboard Capture"].tap()
  }
    
  func addSlidesFromCameraRoll(let numberOfImages: UInt) {

    let app = XCUIApplication()

    app.buttons["PortfolioView.AddButton"].tap()
    app.tables.staticTexts["Photo from Library"].tap()
    app.tables["OBAlbumsController"].staticTexts["Camera Roll"].tap()

    let images = app.collectionViews["OBImageCollectionViewController"].cells
    for (var i:UInt = 0 ; i < numberOfImages ; i++) {
      images.elementAtIndex(i).tap()
    }

    app.navigationBars["OBImageSelectionView"].buttons["Upload"].tap()

    continueAfterElementDissapears("PortfolioView.statusView");
    continueAfterElementDissapears("PortfolioView.progressPieView");
  }

  func getPortfolioVisibleItemCount() -> UInt {
    let app = XCUIApplication()
    return app.collectionViews["PortfolioView.CollectionView"].cells.countForHittables
  }
    
  func deleteAllPortfolioItems() {

    let app = XCUIApplication()

    let items = app.collectionViews["PortfolioView.CollectionView"].cells
    if items.countForHittables > 0
    {
      app.buttons["PortfolioView.MoreButton"].tap()
      app.tables.staticTexts["Delete All"].tap()
      app.alerts["Delete All Portfolio Items"].collectionViews.buttons["Delete All"].tap()
    }
  }

  func deletePortfolioItemAtPosition(let index: UInt) {

    let app = XCUIApplication()
    let items = app.collectionViews["PortfolioView.CollectionView"].cells
    let item = items.elementAtIndex(index)
    item.pressForDuration(0.1)
    app.tables.staticTexts["Delete"].tap()
    XCTAssertFalse(item.hittable)
  }

  func closeOrDiscardWorkspaceIfNeeded() -> Bool {
    let app = XCUIApplication()
    let workspacetoolbarWorkspacebuttonButton = app.buttons["WorkspaceToolbar.WorkspaceButton"]
    let tablesQuery = app.tables

    workspacetoolbarWorkspacebuttonButton.tap()

    if tablesQuery.staticTexts["Close"].exists {
      tablesQuery.staticTexts["Close"].tap()
      if app.alerts["Close Workspace"].exists {
        app.alerts["Close Workspace"].collectionViews.buttons["Close"].tap()
      }

      continueAfterElementDissapears("WorkspaceOpening")
      XCTAssertTrue(app.buttons["WorkspaceToolbar.WorkspaceButton"].label == "Unsaved");
      return true

    } else if tablesQuery.staticTexts["Discard"].exists {
      tablesQuery.staticTexts["Discard"].tap()
      let discardAlert = app.alerts["Discard Workspace"]
      if discardAlert.exists {
        discardAlert.collectionViews.buttons["Discard"].tap()
        continueAfterElementDissapears("WorkspaceOpening")
        XCTAssertTrue(app.buttons["WorkspaceToolbar.WorkspaceButton"].label == "Unsaved");
        return true
      } 
    }

    workspacetoolbarWorkspacebuttonButton.tap()
    return false
  }

  
  func openWorkspaceWithName(workspaceName: String) {
    let app = XCUIApplication()
    let workspacetoolbarWorkspacebuttonButton = app.buttons["WorkspaceToolbar.WorkspaceButton"]
    workspacetoolbarWorkspacebuttonButton.tap()
    app.tables.staticTexts["Open…"].tap()
    checkViewWithAccessibilityIdentifier("WorkspaceList")
    scrollViewToTop()
    
    app.collectionViews.cells[workspaceName].staticTexts["Public"].tap()
    continueAfterElementDissapears("WorkspaceOpening")
  }


  func closeWorkspaceIfNeeded() -> Bool {

    let app = XCUIApplication()
    let workspacetoolbarWorkspacebuttonButton = app.buttons["WorkspaceToolbar.WorkspaceButton"]
    let tablesQuery = app.tables

    workspacetoolbarWorkspacebuttonButton.tap()

    if tablesQuery.staticTexts["Close"].exists {
      tablesQuery.staticTexts["Close"].tap()
      if app.alerts["Close Workspace"].exists {
        app.alerts["Close Workspace"].collectionViews.buttons["Close"].tap()
      }

      continueAfterElementDissapears("WorkspaceOpening")
      XCTAssertTrue(app.buttons["WorkspaceToolbar.WorkspaceButton"].label == "Unsaved");
      return true
    }

    workspacetoolbarWorkspacebuttonButton.tap()
    return false
  }

  func discardWorkspaceIfNeeded() -> Bool  {

    let app = XCUIApplication()
    let workspacetoolbarWorkspacebuttonButton = app.buttons["WorkspaceToolbar.WorkspaceButton"]
    let tablesQuery = app.tables

    workspacetoolbarWorkspacebuttonButton.tap()

    if tablesQuery.staticTexts["Discard"].exists {
      tablesQuery.staticTexts["Discard"].tap()
      let discardAlert = app.alerts["Discard Workspace"]
      if discardAlert.exists {
        discardAlert.collectionViews.buttons["Discard"].tap()
        continueAfterElementDissapears("WorkspaceOpening")
        XCTAssertTrue(app.buttons["WorkspaceToolbar.WorkspaceButton"].label == "Unsaved");
        return true
      } else {
        workspacetoolbarWorkspacebuttonButton.tap()
        return false
      }
    }

    workspacetoolbarWorkspacebuttonButton.tap()
    return false

  }

  func saveWorkspace(let workspaceName:String) {
    
    let app = XCUIApplication()
    app.buttons["WorkspaceToolbar.WorkspaceButton"].tap()
    app.tables.staticTexts["Save"].tap()
    app.textFields["Workspace Name"].typeText(workspaceName)
    app.buttons["Done"].tap()
  }

    
  func renameWorkspace(workspaceName: String) {
    let app = XCUIApplication()
    app.buttons["WorkspaceToolbar.WorkspaceButton"].tap()
    app.tables.staticTexts["Rename"].tap()
    app.buttons["Clear text"].tap()
    app.textFields["Workspace Name"].typeText(workspaceName)
    app.buttons["Rename"].tap()
  }

  func deleteWorkspace(let workspaceName:String) {

    let app = XCUIApplication()
    app.buttons["WorkspaceToolbar.WorkspaceButton"].tap()
    app.tables.staticTexts["Open…"].tap()

    if (app.collectionViews.buttons["WorkspaceListCell.optionsButton-"+workspaceName].hittable)
    {
      app.collectionViews.buttons["WorkspaceListCell.optionsButton-"+workspaceName].tap()
      app.tables.staticTexts["Delete"].tap()

      let deleteButton = app.alerts["Delete Workspace"].collectionViews.buttons["Delete"]
      deleteButton.tap()
      app.buttons["Done"].tap()
    }
  }

  
  func newWorkspaceWithName(workspaceName: String) {
    let app = XCUIApplication()
    app.buttons["WorkspaceToolbar.WorkspaceButton"].tap()
    app.tables.staticTexts["Open…"].tap()
    checkViewWithAccessibilityIdentifier("WorkspaceList")
    
    app.buttons["New"].tap()
    app.textFields["WorkspaceEditorView.textField"].typeText(workspaceName)
    app.buttons["Create"].tap()
    
    continueAfterElementDissapears("WorkspaceOpening")
  }

  func startPresentation() {
    let app = XCUIApplication()
    XCTAssertTrue(app.buttons["PRESENT"].exists)
    app.buttons["PortfolioView.PresentButton"].tap()
  }

  func startPresentationIfNeeded(){
    let app = XCUIApplication()
    if app.buttons["PortfolioView.PresentButton"].label == "PRESENT" {
        app.buttons["PortfolioView.PresentButton"].tap()
    }
  }

  func stopPresentationIfNeeded() {
    let app = XCUIApplication()
    if app.buttons["PortfolioView.PresentButton"].label == "STOP" {
        app.buttons["PortfolioView.PresentButton"].tap()
    }
  }
    
  func moveDeckSliderForwards(let numberOfPositions: Int) {

    let app = XCUIApplication()
    let thumbView = app.descendantsMatchingType(.Any).elementMatchingType(.Any, identifier: "DeckSliderView.ThumbView")
    let coordinateOrigin = thumbView.coordinateWithNormalizedOffset(CGVector(dx: 0, dy: 0))
    let coordinateEnd = thumbView.coordinateWithNormalizedOffset(CGVector(dx: Double(numberOfPositions) + 0.1, dy: 0))
    coordinateOrigin.pressForDuration(0.1, thenDragToCoordinate:coordinateEnd)
  }

  func moveDeckSliderBackwards(let numberOfPositions: Int) {

    let app = XCUIApplication()
    let thumbView = app.descendantsMatchingType(.Any).elementMatchingType(.Any, identifier: "DeckSliderView.ThumbView")
    let coordinateOrigin = thumbView.coordinateWithNormalizedOffset(CGVector(dx: 0, dy: 0))
    let coordinateEnd = thumbView.coordinateWithNormalizedOffset(CGVector(dx: -Double(numberOfPositions), dy: 0))
    coordinateOrigin.pressForDuration(0.1, thenDragToCoordinate:coordinateEnd)
  }

  func dragPresentationToNextSlide() {

    let app = XCUIApplication()
    let presentationView = app.descendantsMatchingType(.Any).elementMatchingType(.Other, identifier: "PresentationView")
    let coordinateOrigin = presentationView.coordinateWithNormalizedOffset(CGVector(dx: 0.4, dy: 0))
    let coordinateEnd = presentationView.coordinateWithNormalizedOffset(CGVector(dx: -0.4, dy: 0))
    coordinateOrigin.pressForDuration(0.1, thenDragToCoordinate:coordinateEnd)
  }

  func dragPresentationToPreviousSlide() {

    let app = XCUIApplication()
    let presentationView = app.descendantsMatchingType(.Any).elementMatchingType(.Other, identifier: "PresentationView")
    let coordinateOrigin = presentationView.coordinateWithNormalizedOffset(CGVector(dx: -0.4, dy: 0))
    let coordinateEnd = presentationView.coordinateWithNormalizedOffset(CGVector(dx: 0.4, dy: 0))
    coordinateOrigin.pressForDuration(0.1, thenDragToCoordinate:coordinateEnd)
  }
    
  func lockMezz() -> String {
    let app = XCUIApplication()
    app.buttons["Off"].tap()
    let switch2 = app.switches["0"]
    switch2.tap()
    app.buttons["WorkspaceToolbar.ThisMezzanineButton"].tap() //dismiss the lock menu
    return app.buttons.elementAtIndex(0).label
  }
    
  func unlockMezz(passkey: String) {
    let app = XCUIApplication()
    app.buttons[passkey].tap()
    
    let switch2 = app.switches["1"]
    switch2.tap()
  }
    
  func numPresentationFelds() -> Int {
    let app = XCUIApplication()
    if (app.scrollViews.otherElements.otherElements["PresentationView"].childrenMatchingType(.Other).element.childrenMatchingType(.Other).elementBoundByIndex(2).exists) {
        return 3
    }
    else if (app.scrollViews.childrenMatchingType(.Other).element.childrenMatchingType(.Other).elementBoundByIndex(1).exists) {
        return 1
    }
    else {
        return -1
    }
        
  }
}