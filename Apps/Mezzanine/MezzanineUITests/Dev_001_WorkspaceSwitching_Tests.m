//
//  Dev_001_WorkspaceSwitching_Tests.m
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 12/11/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "XCTestCase+MezzServer.h"
#import "XCTestCase+UITestingAdditions.h"

@interface Dev_001_WorkspaceSwitching_Tests : XCTestCase

@end

static NSString *Workspace0 = @"000-workspace-test";
static NSString *Workspace1 = @"001-workspace-test";

@implementation Dev_001_WorkspaceSwitching_Tests

- (void)setUp
{
  [super setUp];
  
  // Put setup code here. This method is called before the invocation of each test method in the class.
  
  // In UI tests it is usually best to stop immediately when a failure occurs.
  self.continueAfterFailure = NO;
  // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
  [[[XCUIApplication alloc] init] launch];
  [self setUpMezzServer];
  // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
}


- (void)tearDown
{
  [super tearDown];
}

- (void)disconnect:(XCUIApplication *)app
{
  [app.buttons[@"WorkspaceToolbar.ThisMezzanineButton"] tap];
  [app.buttons[@"MezzanineMenuDisconnectButton"] tap];

  [self checkViewWithAccessibilityIdentifier:@"ConnectionView"];
}

- (void)test_000_SwitchOpenWorkspace
{
  XCUIApplication *app = [[XCUIApplication alloc] init];
  XCUIElement *workspacetoolbarWorkspacebuttonButton = app.buttons[@"WorkspaceToolbar.WorkspaceButton"];
  [self connectToServer];
  
  // Open workspace
  [workspacetoolbarWorkspacebuttonButton tap];
  [app.staticTexts[@"Open…"] tap];
  [self checkViewWithAccessibilityIdentifier:@"WorkspaceList"];
  [self scrollViewToTop];
  NSString *cellElementName = [NSString stringWithFormat:@"WorkspaceListCell-%@", Workspace0];
  [app.collectionViews.cells[cellElementName].staticTexts[@"Public"] tap];
  

  [self continueAfterElementDissapears:@"WorkspaceOpening"];
  XCTAssertTrue([workspacetoolbarWorkspacebuttonButton.label isEqualToString:Workspace0]);
  
  // Open workspace
  [workspacetoolbarWorkspacebuttonButton tap];
  [app.staticTexts[@"Open…"] tap];
  [self checkViewWithAccessibilityIdentifier:@"WorkspaceList"];
  [self scrollViewToTop];
  cellElementName = [NSString stringWithFormat:@"WorkspaceListCell-%@", Workspace1];
  [app.collectionViews.cells[cellElementName].staticTexts[@"Public"] tap];
  
  [self continueAfterElementDissapears:@"WorkspaceOpening"];
  XCTAssertTrue([workspacetoolbarWorkspacebuttonButton.label isEqualToString:Workspace1]);

  [self disconnect:app];
}


- (void)test_001_CloseOpenWorkspace
{
  XCUIApplication *app = [[XCUIApplication alloc] init];
  XCUIElement *workspacetoolbarWorkspacebuttonButton = app.buttons[@"WorkspaceToolbar.WorkspaceButton"];
  [self connectToServer];
  [self closeWorkspaceIfNeeded];
  
  // Open workspace
  [workspacetoolbarWorkspacebuttonButton tap];
  [app.staticTexts[@"Open…"] tap];
  [self checkViewWithAccessibilityIdentifier:@"WorkspaceList"];
  [self scrollViewToTop];
  NSString *cellElementName = [NSString stringWithFormat:@"WorkspaceListCell-%@", Workspace0];
  [app.collectionViews.cells[cellElementName].staticTexts[@"Public"] tap];
  
  [self continueAfterElementDissapears:@"WorkspaceOpening"];
  XCTAssertTrue([workspacetoolbarWorkspacebuttonButton.label isEqualToString:Workspace0]);

  [self disconnect:app];
}


- (void)test_002_DiscardOpenWorkspace
{
  XCUIApplication *app = [[XCUIApplication alloc] init];
  XCUIElementQuery *tablesQuery = app.tables;
  XCUIElement *workspacetoolbarWorkspacebuttonButton = app.buttons[@"WorkspaceToolbar.WorkspaceButton"];
  [self connectToServer];
  [self closeWorkspaceIfNeeded];
  [self addNewSlide];
  
  [app.buttons[@"WorkspaceToolbar.WorkspaceButton"] tap];
  [tablesQuery.staticTexts[@"Discard"] tap];
  [app.alerts[@"Discard Workspace"].collectionViews.buttons[@"Discard"] tap];
  
  [self continueAfterElementDissapears:@"WorkspaceModalOverlay"];
  [workspacetoolbarWorkspacebuttonButton tap];
  
  // Open workspace
  [app.staticTexts[@"Open…"] tap];
  [self scrollViewToTop];
  NSString *cellElementName = [NSString stringWithFormat:@"WorkspaceListCell-%@", Workspace0];
  [app.collectionViews.cells[cellElementName].staticTexts[@"Public"] tap];
  
  [self continueAfterElementDissapears:@"WorkspaceOpening"];
  XCTAssertTrue([workspacetoolbarWorkspacebuttonButton.label isEqualToString:Workspace0]);

  [self disconnect:app];
}


- (void)test_003_SwitchToNewWorkspace
{
  XCUIApplication *app = [[XCUIApplication alloc] init];
  XCUIElement *workspacetoolbarWorkspacebuttonButton = app.buttons[@"WorkspaceToolbar.WorkspaceButton"];
  [self connectToServer];
  
  [workspacetoolbarWorkspacebuttonButton tap];
  [app.tables.staticTexts[@"Open…"] tap];
  [self checkViewWithAccessibilityIdentifier:@"WorkspaceList"];
  
  [app.buttons[@"New"] tap];
  NSString *newWorkspaceName = app.textFields[@"WorkspaceEditorView-textField"].value;
  [app.buttons[@"Create"] tap];
  
  [self continueAfterElementDissapears:@"WorkspaceOpening"];
  XCTAssertTrue([workspacetoolbarWorkspacebuttonButton.label isEqualToString:newWorkspaceName]);

  [self disconnect:app];
}


@end
