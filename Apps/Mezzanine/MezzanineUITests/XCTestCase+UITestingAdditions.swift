//
//  XCTestCase+UITestingAdditions.swift
//  Mezzanine
//
//  Created by Miguel Sánchez Valdés on 30/11/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

import Foundation
import XCTest

private let waitingSecondsDefault:Double = 15.0

@available(iOS 9.0, *)

extension XCTestCase {

  func checkViewWithAccessibilityIdentifier(let identifier:String){
    XCTAssertNotNil(identifier, "identifier can't be nil")

    let app = XCUIApplication()
    let element = app.descendantsMatchingType(.Any).elementMatchingType(.Any, identifier: identifier)
    let existsPredicate = NSPredicate.init(format: "exists == 1")
    expectationForPredicate(existsPredicate, evaluatedWithObject: element, handler: nil)
    waitForExpectationsWithTimeout(waitingSecondsDefault, handler: nil)

    XCTAssert(element.exists)
  }


  func waitToContinue(){
    waitToContinue(waitingSecondsDefault)
  }


  func waitToContinue(let seconds: Double){
    NSRunLoop.currentRunLoop().runUntilDate(NSDate(timeIntervalSinceNow: seconds))
  }


  func continueAfterElementDissapears(let identifier:String){
    XCTAssertNotNil(identifier, "identifier can't be nil");

    // Added a delay, since there are some animations (alphas) going on
    // 1.0 is an empiric value
    waitToContinue(1.0)
    let app = XCUIApplication()
    let element = app.descendantsMatchingType(.Any).elementMatchingType(.Any, identifier: identifier)

    let existsPredicate = NSPredicate.init(format: "hittable == 0")
    expectationForPredicate(existsPredicate, evaluatedWithObject: element, handler: nil)
    waitForExpectationsWithTimeout(waitingSecondsDefault) { error in
      // Added a delay, since there are some animations (alphas) going on
      // 1.0 is an empiric value
      self.waitToContinue(1.0)
    }
  }


  func continueAfterElementDissapearsCompletely(let element:XCUIElement){
    XCTAssertNotNil(element, "element can't be nil");

    // Added a delay, since there are some animations (alphas) going on
    // 1.0 is an empiric value
    waitToContinue(1.0)

    let existsPredicate = NSPredicate.init(format: "hittable == 0 && exists == 0")
    expectationForPredicate(existsPredicate, evaluatedWithObject: element, handler: nil)
    waitForExpectationsWithTimeout(waitingSecondsDefault) { error in
      // Added a delay, since there are some animations (alphas) going on
      // 1.0 is an empiric value
      self.waitToContinue(1.0)
    }
  }
  
  func scrollViewToTop() {
    let app = XCUIApplication()
    app.statusBars.childrenMatchingType(.Other).elementBoundByIndex(1).childrenMatchingType(.Other).elementBoundByIndex(0).tap()
    self.waitToContinue(1.0)
  }
}
