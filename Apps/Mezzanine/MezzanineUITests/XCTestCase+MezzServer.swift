//
//  XCTestCase+MezzServer.swift
//  Mezzanine
//
//  Created by Miguel Sánchez Valdés on 30/11/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

import Foundation
import XCTest
import ObjectiveC

private var mezzServerAssociatedKey: UInt8 = 0

@available(iOS 9.0, *)
extension XCTestCase {

  var mezzServer : String? {
    get {
      return objc_getAssociatedObject(self, &mezzServerAssociatedKey) as? String
    }

    set(server) {
      objc_setAssociatedObject(self, &mezzServerAssociatedKey, server, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }
  }

  func setUpMezzServer() {
    let bundle = NSBundle.init(forClass: Dev_000_Connection_Tests.self)
    let serverConfigPath = bundle.pathForResource("mezz-test-server", ofType: nil)
    do {
      mezzServer = try NSString(contentsOfFile:serverConfigPath!, encoding: NSUTF8StringEncoding) as String!
    } catch {
      XCTFail("UI Testing needs a Mezzanine server to run the tests. Fill mezz-test-server file if running tests locally or configure the bot to modify the file.");
    }

    if (mezzServer == "") {
      XCTFail("UI Testing needs a Mezzanine server to run the tests. Fill mezz-test-server file if running tests locally or configure the bot to modify the file.");
    }
  }

  func connectToServer(address: String) {
        
    let app = XCUIApplication()
    checkViewWithAccessibilityIdentifier("ConnectionView")
    
    let mezzanineExampleComTextField = app.textFields["mezzanine.example.com"]
    mezzanineExampleComTextField.tap()
    if app.buttons["Clear text"].exists {
        app.buttons["Clear text"].tap()
    }
    mezzanineExampleComTextField.typeText(address)
    
    let connectionviewconnectbuttonButton = app.buttons["ConnectionViewConnectButton"]
    connectionviewconnectbuttonButton.tap()
    
    checkViewWithAccessibilityIdentifier("WorkspaceView")
  }

    
  func connectToServer() {
    connectToServer(mezzServer!)
  }

  func disconnectFromServer() {
    let app = XCUIApplication()

    app.buttons["WorkspaceToolbar.ThisMezzanineButton"].tap()
    app.buttons["MezzanineMenuDisconnectButton"].tap()

    checkViewWithAccessibilityIdentifier("ConnectionView")
  }
}