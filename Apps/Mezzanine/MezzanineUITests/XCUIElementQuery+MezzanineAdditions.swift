//
//  XCUIElementQuery+MezzanineAdditions.swift
//  Mezzanine
//
//  Created by Michael Miller on 12/11/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

import Foundation
import XCTest

@available(iOS 9.0, *)

extension XCUIElementQuery {
    var countForHittables: UInt {
    return UInt(allElementsBoundByIndex.filter { $0.hittable }.count)
    }
}