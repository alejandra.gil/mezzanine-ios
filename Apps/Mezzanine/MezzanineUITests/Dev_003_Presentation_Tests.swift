//
//  Dev_003_Presentation_Tests.swift
//  Mezzanine
//
//  Created by miguel on 4/12/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

import XCTest

@available(iOS 9.0, *)

class Dev_003_Presentation_Tests: XCTestCase {
    
  override func setUp() {
    super.setUp()
    setUpMezzServer()
    continueAfterFailure = false
    XCUIApplication().launch()
  }

  override func tearDown() {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    super.tearDown()
  }


  func test_000_DraggingPresentationSlides() {

    connectToServer()
    closeOrDiscardWorkspaceIfNeeded()
    addSlidesFromCameraRoll(4)
    startPresentation()

    // Start on slide 1
    checkCurrentSlideViewIsOverSlide(0)
    checkDeckSliderPosition(1, totalSlides: 4)

    // Get to slide 2
    dragPresentationToNextSlide()
    waitToContinue(0.2)
    checkCurrentSlideViewIsOverSlide(1)
    checkDeckSliderPosition(2, totalSlides: 4)

    // Get to slide 3
    dragPresentationToNextSlide()
    waitToContinue(0.2)
    checkCurrentSlideViewIsOverSlide(2)
    checkDeckSliderPosition(3, totalSlides: 4)

    // Get to slide 4
    dragPresentationToNextSlide()
    waitToContinue(0.2)
    checkCurrentSlideViewIsOverSlide(3)
    checkDeckSliderPosition(4, totalSlides: 4)

    // Try to get to slide 5 that doesn't exist and stays in slide 4 instead (test eslasticity of scroll view)
    dragPresentationToNextSlide()
    waitToContinue(0.2)
    checkCurrentSlideViewIsOverSlide(3)
    checkDeckSliderPosition(4, totalSlides: 4)

    // Go back to slide 3
    dragPresentationToPreviousSlide()
    waitToContinue(0.2)
    checkCurrentSlideViewIsOverSlide(2)
    checkDeckSliderPosition(3, totalSlides: 4)

    // Go back to slide 2
    dragPresentationToPreviousSlide()
    waitToContinue(0.2)
    checkCurrentSlideViewIsOverSlide(1)
    checkDeckSliderPosition(2, totalSlides: 4)

    // Go back to slide 1
    dragPresentationToPreviousSlide()
    waitToContinue(0.2)
    checkCurrentSlideViewIsOverSlide(0)
    checkDeckSliderPosition(1, totalSlides: 4)

    // Try to get to slide 0 that doesn't exist and stays in slide 1 instead (test eslasticity of scroll view)
    dragPresentationToPreviousSlide()
    waitToContinue(0.2)
    checkCurrentSlideViewIsOverSlide(0)
    checkDeckSliderPosition(1, totalSlides: 4)

    discardWorkspaceIfNeeded()
    disconnectFromServer()
  }


  func test_001_PresentAndStopPresentationAndUseDeckSlider() {

    connectToServer()
    closeOrDiscardWorkspaceIfNeeded()

    addNewSlide()
    startPresentation()
    checkDeckSliderPosition(1, totalSlides: 1)

    // Stop presentation with button next to deck slider
    XCUIApplication().buttons["mobile stop icon"].tap()

    addSlidesFromCameraRoll(5)

    // Start on slide 1
    startPresentation()
    checkDeckSliderPosition(1, totalSlides: 6)
    checkCurrentSlideViewIsOverSlide(0)

    // Get to slide 2
    moveDeckSliderForwards(1)
    checkCurrentSlideViewIsOverSlide(1)
    checkDeckSliderPosition(2, totalSlides: 6)

    // Get to slide 3
    moveDeckSliderForwards(1)
    checkCurrentSlideViewIsOverSlide(2)
    checkDeckSliderPosition(3, totalSlides: 6)

    // Get to slide 5
    moveDeckSliderForwards(2)
    checkCurrentSlideViewIsOverSlide(4)
    checkDeckSliderPosition(5, totalSlides: 6)

    // Get to slide 2
    moveDeckSliderBackwards(3)
    checkCurrentSlideViewIsOverSlide(1)

    // Get to slide 1
    moveDeckSliderBackwards(1)
    checkCurrentSlideViewIsOverSlide(0)
    checkDeckSliderPosition(1, totalSlides: 6)

    discardWorkspaceIfNeeded()
    disconnectFromServer()
  }


  func test_002_WelcomeHintStates() {

    let app = XCUIApplication()
    connectToServer()
    closeOrDiscardWorkspaceIfNeeded()

    let welcomeHintLabel = app.descendantsMatchingType(.Any).elementMatchingType(.Any, identifier: "WelcomeHintView.Label")
    let welcomeHintImage = app.descendantsMatchingType(.Any).elementMatchingType(.Any, identifier: "WelcomeHintView.LogoImageView")

    let deviceType = UIDevice.currentDevice().model
    if deviceType == "iPad" {

      XCTAssertTrue(welcomeHintLabel.exists)
      XCTAssertTrue(welcomeHintLabel.hittable)
      XCTAssertTrue(welcomeHintImage.exists)

    } else if deviceType == "iPhone" {

      XCTAssertTrue(welcomeHintLabel.exists)
      XCTAssertTrue(welcomeHintLabel.hittable)
      XCTAssertFalse(welcomeHintImage.exists)
    }

    addNewSlide()
    app.collectionViews["PortfolioView.CollectionView"].cells.elementAtIndex(0).pressForDuration(0.1)
    app.tables.staticTexts["Place on Screen"].tap()

    if deviceType == "iPad" {

      XCTAssertTrue(welcomeHintLabel.exists)
      XCTAssertFalse(welcomeHintLabel.hittable)
      XCTAssertTrue(welcomeHintImage.exists)
      XCTAssertFalse(welcomeHintImage.hittable)

    } else if deviceType == "iPhone" {

      XCTAssertTrue(welcomeHintLabel.exists)
      XCTAssertFalse(welcomeHintLabel.hittable)
      XCTAssertFalse(welcomeHintImage.exists)
    }

    discardWorkspaceIfNeeded()
    disconnectFromServer()
  }


  func checkDeckSliderPosition(let currentSlide: UInt, let totalSlides: UInt) {

    let app = XCUIApplication()
    let slideIndexLabel = app.descendantsMatchingType(.Any).elementMatchingType(.StaticText, identifier: "DeckSliderView.SlideIndexLabel")
    let firstSlideLabel = app.descendantsMatchingType(.Any).elementMatchingType(.StaticText, identifier: "DeckSliderView.FirstSlideLabel")
    let lastSlideLabel = app.descendantsMatchingType(.Any).elementMatchingType(.StaticText, identifier: "DeckSliderView.LastIndexLabel")
    XCTAssertEqual(slideIndexLabel.label, String(currentSlide), "Slider should be on slide #" + String(currentSlide))
    XCTAssertEqual(firstSlideLabel.label, "1", "First index should be 1")
    XCTAssertEqual(lastSlideLabel.label, String(totalSlides), "Last index should be " + String(totalSlides))
  }


  func checkCurrentSlideViewIsOverSlide(let slide: UInt) {

    let app = XCUIApplication()
    let currentSlideView = app.descendantsMatchingType(.Any).elementMatchingType(.Any, identifier: "PortfolioView.currentSlideView")
    var currentSlideCoordinate = currentSlideView.coordinateWithNormalizedOffset(CGVector(dx: 0, dy: 0)).screenPoint
    currentSlideCoordinate.x += 2
    currentSlideCoordinate.y += 2
    let items = app.collectionViews["PortfolioView.CollectionView"].cells
    let itemCoordinate = items.elementAtIndex(slide).coordinateWithNormalizedOffset(CGVector(dx: 0, dy: 0)).screenPoint
    XCTAssertTrue(CGPointEqualToPoint(currentSlideCoordinate, itemCoordinate), "item coordinate should be " + NSStringFromCGPoint(itemCoordinate) + "but it is" + NSStringFromCGPoint(currentSlideCoordinate))
  }
}
