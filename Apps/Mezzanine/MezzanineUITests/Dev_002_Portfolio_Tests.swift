//
//  Dev_002_Portfolio_Tests.swift
//  Mezzanine
//
//  Created by miguel on 1/12/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

import XCTest

@available(iOS 9.0, *)

class Dev_002_Portfolio_Tests: XCTestCase {

  let SHORT_TAP_DURATION = 0.1
  let LONG_TAP_DURATION = 1.0


  override func setUp() {
    super.setUp()
    setUpMezzServer()
    continueAfterFailure = false
    XCUIApplication().launch()
  }

  override func tearDown() {
      // Put teardown code here. This method is called after the invocation of each test method in the class.
      super.tearDown()
  }


  func test_000_UploadPicturesReorderAndDeleteThem() {

    let app = XCUIApplication()
    connectToServer()
    closeOrDiscardWorkspaceIfNeeded()

    let hintLabel = app.descendantsMatchingType(.Any).elementMatchingType(.StaticText, identifier: "PortfolioView.hintLabel")
    XCTAssertTrue(hintLabel.hittable)

    app.buttons["PortfolioView.AddButton"].tap()
    app.tables.staticTexts["Photo from Library"].tap()
    app.tables["OBAlbumsController"].staticTexts["Camera Roll"].tap()

    let images = app.collectionViews["OBImageCollectionViewController"].cells
    images.elementAtIndex(0).tap()
    images.elementAtIndex(1).tap()
    images.elementAtIndex(2).tap()
    images.elementAtIndex(3).tap()
    app.navigationBars["OBImageSelectionView"].buttons["Upload"].tap()

    // Let's wait until all of them have been uploaded
    continueAfterElementDissapears("PortfolioView.progressPieView");

    // Hint is gone when portfolio has items and presentation can be started
    XCTAssertFalse(hintLabel.hittable)

    var items = app.collectionViews["PortfolioView.CollectionView"].cells

    XCTAssertEqual(items.count, 4)
    XCTAssertEqual(items.countForHittables, 4)

    let lastItem = items.elementAtIndex(0)
    let firstItem = items.elementAtIndex(3)
    lastItem.pressForDuration(LONG_TAP_DURATION, thenDragToElement: firstItem)

    // Remove a couple of items individually
    items = app.collectionViews["PortfolioView.CollectionView"].cells
    var item = items.elementAtIndex(3)
    item.pressForDuration(SHORT_TAP_DURATION)
    XCUIApplication().tables.staticTexts["Delete"].tap()
    XCTAssertEqual(items.countForHittables, 3)
    XCTAssertFalse(item.hittable)

    item = items.elementAtIndex(2)
    item.pressForDuration(SHORT_TAP_DURATION)
    XCUIApplication().tables.staticTexts["Delete"].tap()
    XCTAssertEqual(items.countForHittables, 2)
    XCTAssertFalse(item.hittable)

    // Remove the rest
    deleteAllPortfolioItems()
    XCTAssertEqual(items.countForHittables, 0)

    // Hint is back when portfolio is empty and presentation cannot be started
    XCTAssertTrue(hintLabel.hittable)

    disconnectFromServer()
  }


  func test_001_CreateTextSlides() {

    let app = XCUIApplication()
    connectToServer()
    closeOrDiscardWorkspaceIfNeeded()

    let hintLabel = app.descendantsMatchingType(.Any).elementMatchingType(.StaticText, identifier: "PortfolioView.hintLabel")
    XCTAssertTrue(hintLabel.hittable)

    app.buttons["PortfolioView.AddButton"].tap()
    app.tables.staticTexts["Text"].tap()

    let slidecreateviewcontrollerTextviewTextView = app.textViews["SlideCreateViewController.textView"]
    slidecreateviewcontrollerTextviewTextView.tap()
    slidecreateviewcontrollerTextviewTextView.typeText("Text")
    app.navigationBars["Create Text Slide"].buttons["Create"].tap()


    // Let's wait until all of them have been uploaded
    continueAfterElementDissapears("PortfolioView.progressPieView");

    // Hint is gone when portfolio has items and presentation can be started
    XCTAssertFalse(hintLabel.hittable)

    let items = app.collectionViews["PortfolioView.CollectionView"].cells
    XCTAssertEqual(items.count, 1)
    XCTAssertEqual(items.countForHittables, 1)

    // Remove the rest
    app.buttons["PortfolioView.MoreButton"].tap()
    app.tables.staticTexts["Delete All"].tap()
    app.alerts["Delete All Portfolio Items"].collectionViews.buttons["Delete All"].tap()
    XCTAssertEqual(items.countForHittables, 0)

    // Hint is back when portfolio is empty and presentation cannot be started
    XCTAssertTrue(hintLabel.hittable)

    disconnectFromServer()
  }


  func test_002_UploadPicturesAndDeleteIndividually() {

    let app = XCUIApplication()
    connectToServer()
    closeOrDiscardWorkspaceIfNeeded()

    app.buttons["PortfolioView.AddButton"].tap()
    app.tables.staticTexts["Photo from Library"].tap()
    app.tables["OBAlbumsController"].staticTexts["Camera Roll"].tap()

    let images = app.collectionViews["OBImageCollectionViewController"].cells
    images.elementAtIndex(0).tap()
    images.elementAtIndex(1).tap()
    images.elementAtIndex(2).tap()
    images.elementAtIndex(3).tap()
    app.navigationBars["OBImageSelectionView"].buttons["Upload"].tap()

    continueAfterElementDissapears("PortfolioView.statusView");
    continueAfterElementDissapears("PortfolioView.progressPieView");

    let items = app.collectionViews["PortfolioView.CollectionView"].cells

    XCTAssertEqual(items.count, 4)
    XCTAssertEqual(items.countForHittables, 4)

    var item = items.elementAtIndex(3)
    item.pressForDuration(SHORT_TAP_DURATION)
    XCUIApplication().tables.staticTexts["Delete"].tap()
    XCTAssertEqual(items.countForHittables, 3)
    XCTAssertFalse(item.hittable)

    item = items.elementAtIndex(2)
    item.pressForDuration(SHORT_TAP_DURATION)
    XCUIApplication().tables.staticTexts["Delete"].tap()
    XCTAssertEqual(items.countForHittables, 2)
    XCTAssertFalse(item.hittable)

    item = items.elementAtIndex(1)
    item.pressForDuration(SHORT_TAP_DURATION)
    XCUIApplication().tables.staticTexts["Delete"].tap()
    XCTAssertEqual(items.countForHittables, 1)
    XCTAssertFalse(item.hittable)

    item = items.elementAtIndex(0)
    item.pressForDuration(SHORT_TAP_DURATION)
    XCUIApplication().tables.staticTexts["Delete"].tap()
    XCTAssertEqual(items.countForHittables, 0)
    XCTAssertFalse(item.hittable)

    disconnectFromServer()
  }


  func test_003_UploadPicturesAndDeleteThemAll() {

    let app = XCUIApplication()
    connectToServer()
    closeOrDiscardWorkspaceIfNeeded()

    app.buttons["PortfolioView.AddButton"].tap()
    app.tables.staticTexts["Photo from Library"].tap()
    app.tables["OBAlbumsController"].staticTexts["Camera Roll"].tap()

    let images = app.collectionViews["OBImageCollectionViewController"].cells
    images.elementAtIndex(0).tap()
    images.elementAtIndex(1).tap()
    images.elementAtIndex(2).tap()
    images.elementAtIndex(3).tap()
    app.navigationBars["OBImageSelectionView"].buttons["Upload"].tap()

    continueAfterElementDissapears("PortfolioView.statusView");
    continueAfterElementDissapears("PortfolioView.progressPieView");

    let items = app.collectionViews["PortfolioView.CollectionView"].cells

    XCTAssertEqual(items.count, 4)
    XCTAssertEqual(items.countForHittables, 4)

    deleteAllPortfolioItems()
    disconnectFromServer()
  }

  func test_004_PlaceItemOnWindshield() {

    let app = XCUIApplication()
    connectToServer()
    closeOrDiscardWorkspaceIfNeeded()

    let hintLabel = app.descendantsMatchingType(.Any).elementMatchingType(.StaticText, identifier: "PortfolioView.hintLabel")
    XCTAssertTrue(hintLabel.hittable)

    addNewSlide()

    let windshieldItems = app.scrollViews.otherElements.otherElements["WindshieldViewItem"].childrenMatchingType(.Other).elementBoundByIndex(0).childrenMatchingType(.Image)
    XCTAssertTrue(windshieldItems.count == 0, "There should not be any windshield item yet")

    app.collectionViews["PortfolioView.CollectionView"].cells.elementAtIndex(0).pressForDuration(SHORT_TAP_DURATION)
    app.tables.staticTexts["Place on Screen"].tap()

    let existsPredicate = NSPredicate.init(format: "count == 1")
    expectationForPredicate(existsPredicate, evaluatedWithObject: windshieldItems, handler: nil)
    waitForExpectationsWithTimeout(15.0, handler: nil)

    XCTAssertTrue(windshieldItems.count == 1, "There should be 1 windshield item item yet")

    discardWorkspaceIfNeeded()
    disconnectFromServer()
  }


  func test_005_LocalPreviewOneItem() {

    let app = XCUIApplication()
    connectToServer()
    closeOrDiscardWorkspaceIfNeeded()

    let hintLabel = app.descendantsMatchingType(.Any).elementMatchingType(.StaticText, identifier: "PortfolioView.hintLabel")
    XCTAssertTrue(hintLabel.hittable)

    addNewSlide()

    app.collectionViews["PortfolioView.CollectionView"].cells.elementAtIndex(0).pressForDuration(SHORT_TAP_DURATION)
    app.tables.staticTexts["Local Preview"].tap()

    checkViewWithAccessibilityIdentifier("AssetAnnotationView")

    app.toolbars.buttons["Close"].tap()

    continueAfterElementDissapears("AssetAnnotationView")

    discardWorkspaceIfNeeded()
    disconnectFromServer()
  }


  func test_006_WhiteboardCapture() {

    let app = XCUIApplication()
    connectToServer()
    closeOrDiscardWorkspaceIfNeeded()

    let items = app.collectionViews["PortfolioView.CollectionView"].cells
    let hintLabel = app.descendantsMatchingType(.Any).elementMatchingType(.StaticText, identifier: "PortfolioView.hintLabel")
    XCTAssertTrue(hintLabel.hittable)
    XCTAssertTrue(items.count == 0)

    app.buttons["PortfolioView.AddButton"].tap()
    app.tables.staticTexts["Whiteboard Capture"].tap()

    continueAfterElementDissapears("PortfolioView.progressPieView");

    XCTAssertTrue(items.count == 1)

    discardWorkspaceIfNeeded()
    disconnectFromServer()
  }


  func test_007_StartAndStopPresentation() {

    let app = XCUIApplication()
    connectToServer()
    closeOrDiscardWorkspaceIfNeeded()

    addNewSlide()

    continueAfterElementDissapears("PortfolioView.progressPieView");

    XCTAssertTrue(app.buttons["PRESENT"].exists)
    XCTAssertFalse(app.buttons["STOP"].exists)

    app.buttons["PortfolioView.PresentButton"].tap()
    XCTAssertFalse(app.buttons["PRESENT"].exists)
    XCTAssertTrue(app.buttons["STOP"].exists)

    app.buttons["PortfolioView.PresentButton"].tap()
    XCTAssertTrue(app.buttons["PRESENT"].exists)
    XCTAssertFalse(app.buttons["STOP"].exists)

    discardWorkspaceIfNeeded()
    disconnectFromServer()
  }


  func test_008_Export() {

    let app = XCUIApplication()
    connectToServer()
    
    closeOrDiscardWorkspaceIfNeeded()

    let portfolioviewMorebuttonButton = app.buttons["PortfolioView.MoreButton"]
    portfolioviewMorebuttonButton.tap()

    app.tables.staticTexts["Download PDF"].tap()
    // Still hittable because it is disabled so tapping on it doesn't produce any effect
    XCTAssertTrue(app.tables.staticTexts["Download PDF"].hittable)

    // Tap outside to dismiss menu and check the menu is gone
    portfolioviewMorebuttonButton.tap()
    XCTAssertFalse(app.tables.staticTexts["Download PDF"].exists)
    waitToContinue(1.0)

    addNewSlide()

    continueAfterElementDissapears("PortfolioView.progressPieView");

    let workspaceName = "test_007_Export"
    saveWorkspace(workspaceName)

    // Test Previewing the exported document
    portfolioviewMorebuttonButton.tap()
    app.tables.staticTexts["Download PDF"].tap()
    let collectionViewsQuery = app.alerts["PDF Export Complete"].collectionViews
    collectionViewsQuery.buttons["Preview"].tap()
    
    waitToContinue(3.0)
    app.buttons["Done"].tap()
    waitToContinue(1.0)

    // Test Opening in... the exported document
    portfolioviewMorebuttonButton.tap()
    let downloadPdfStaticText = app.tables.staticTexts["Download PDF"]
    downloadPdfStaticText.tap()
    let openInButton = collectionViewsQuery.buttons["Open in..."]
    openInButton.tap()
    if (app.alerts["Missing PDF Reader"].exists) {
      app.alerts["Missing PDF Reader"].collectionViews.buttons["Dismiss"].tap()
    } else {
      waitToContinue(1.0)
      let cancelButton = XCUIApplication().sheets.buttons["Cancel"]
      cancelButton.tap()
    }

    waitToContinue(1.0)

    // Test ignoring the exported document
    portfolioviewMorebuttonButton.tap()
    downloadPdfStaticText.tap()
    collectionViewsQuery.buttons["Cancel"].tap()
    waitToContinue(2.0)

    deleteWorkspace(workspaceName)
    continueAfterElementDissapears("WorkspaceOpening")

    disconnectFromServer()
  }

}
