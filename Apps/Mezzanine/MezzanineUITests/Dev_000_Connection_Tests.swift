//
//  Dev_000_Connection_Tests.swift
//  Mezzanine
//
//  Created by Miguel Sánchez Valdés on 30/11/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

import XCTest

@available(iOS 9.0, *)

class Dev_000_Connection_Tests: XCTestCase {
    
  override func setUp() {

    super.setUp()
    setUpMezzServer()
    continueAfterFailure = false
    XCUIApplication().launch()
  }
  
  override func tearDown() {
      // Put teardown code here. This method is called after the invocation of each test method in the class.
      super.tearDown()
  }

  func test_000_ConnectTest() {

    let app = XCUIApplication()

    checkViewWithAccessibilityIdentifier("ConnectionView")

    let mezzanineExampleComTextField = app.textFields["mezzanine.example.com"]
    mezzanineExampleComTextField.tap()
    mezzanineExampleComTextField.typeText(mezzServer!)

    let connectionviewconnectbuttonButton = app.buttons["ConnectionViewConnectButton"]
    connectionviewconnectbuttonButton.tap()

    checkViewWithAccessibilityIdentifier("WorkspaceView")
    app.buttons["WorkspaceToolbar.ThisMezzanineButton"].tap()
    app.buttons["MezzanineMenuDisconnectButton"].tap()

    checkViewWithAccessibilityIdentifier("ConnectionView")
  }


  func test_001_ConnectClearingPreviousConnection() {

    let app = XCUIApplication()

    checkViewWithAccessibilityIdentifier("ConnectionView")

    let mezzanineExampleComTextField = app.textFields["mezzanine.example.com"]
    mezzanineExampleComTextField.tap()
    app.buttons["Clear text"].tap()
    mezzanineExampleComTextField.typeText(mezzServer!)

    let connectionviewconnectbuttonButton = app.buttons["ConnectionViewConnectButton"]
    connectionviewconnectbuttonButton.tap()

    checkViewWithAccessibilityIdentifier("WorkspaceView")

    app.buttons["WorkspaceToolbar.ThisMezzanineButton"].tap()
    app.buttons["MezzanineMenuDisconnectButton"].tap()

    checkViewWithAccessibilityIdentifier("ConnectionView")
  }

  func test_002_ConnectAndDisconnect() {

    let app = XCUIApplication()
    app.buttons["ConnectionViewConnectButton"].tap()
    app.buttons["WorkspaceToolbar.ThisMezzanineButton"].tap()
    app.buttons["MezzanineMenuDisconnectButton"].tap()

    checkViewWithAccessibilityIdentifier("ConnectionView")
  }

  func test_003_ConnectUsingRecentConnections() {

    let app = XCUIApplication()
    app.buttons["ConnectionViewRecentConnectionsButton"].tap()
    app.tables["ConnectionHistoryView"].staticTexts[mezzServer!].tap()

    checkViewWithAccessibilityIdentifier("WorkspaceView")

    app.buttons["WorkspaceToolbar.ThisMezzanineButton"].tap()
    app.buttons["MezzanineMenuDisconnectButton"].tap()

    checkViewWithAccessibilityIdentifier("ConnectionView")
  }

  func test_004_ClearRecentConnections() {

    let app = XCUIApplication()
    let recentConnectionsButton = app.buttons["ConnectionViewRecentConnectionsButton"]
    recentConnectionsButton.tap()

    app.navigationBars["Recent Servers"].buttons["ConnectionHistoryViewClearButton"].tap()
    app.alerts["Clear Recent Servers"].collectionViews.buttons["Okay"].tap()

    XCTAssertFalse(recentConnectionsButton.hittable);
  }
}
