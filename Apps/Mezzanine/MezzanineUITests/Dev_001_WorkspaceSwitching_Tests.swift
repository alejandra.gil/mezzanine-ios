//
//  Dev_001_WorkspaceSwitching_Tests.swift
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 10/12/15.
//  Copyright © 2015 Oblong Industries. All rights reserved.
//

import XCTest

@available(iOS 9.0, *)

class Dev_001_WorkspaceSwitching_Tests: XCTestCase {
  
  let workspace0 = "000-workspace-test"
  let workspace1 = "001-workspace-test"
  
  override func setUp() {
    
    super.setUp()
    setUpMezzServer()
    continueAfterFailure = false
    XCUIApplication().launch()
  }
  
  
  override func tearDown() {
    super.tearDown()
  }

  
  func test_000_SwitchOpenWorkspace () {
    let app = XCUIApplication()
    let workspacetoolbarWorkspacebuttonButton = app.buttons["WorkspaceToolbar.WorkspaceButton"]
    var workspaceName = "WorkspaceListCell-" + workspace0

    connectToServer()
    
    openWorkspaceWithName(workspaceName)
    XCTAssertTrue(workspacetoolbarWorkspacebuttonButton.label == workspace0)
    
    // docker seems to load workspaces a bit slower...
    waitToContinue(2.0)
    
    workspaceName = "WorkspaceListCell-" + workspace1
    openWorkspaceWithName(workspaceName)
    XCTAssertTrue(workspacetoolbarWorkspacebuttonButton.label == workspace1);
    
    disconnectFromServer()
  }
  
  
  func test_001_CloseOpenWorkspace() {
    let app = XCUIApplication()
    let workspacetoolbarWorkspacebuttonButton = app.buttons["WorkspaceToolbar.WorkspaceButton"]
    let workspaceName = "WorkspaceListCell-" + workspace0

    connectToServer()
    discardWorkspaceIfNeeded()
    openWorkspaceWithName(workspaceName)
    
    XCTAssertTrue(workspacetoolbarWorkspacebuttonButton.label == workspace0)
    
    disconnectFromServer()
  }
  
  
  func test_002_DiscardOpenWorkspace() {
    let app = XCUIApplication()
    let workspacetoolbarWorkspacebuttonButton = app.buttons["WorkspaceToolbar.WorkspaceButton"]
    let workspaceName = "WorkspaceListCell-" + workspace0

    connectToServer()
    discardWorkspaceIfNeeded()
    addNewSlide()
    discardWorkspaceIfNeeded()
    openWorkspaceWithName(workspaceName)

    XCTAssertTrue(workspacetoolbarWorkspacebuttonButton.label == workspace0)
    
    disconnectFromServer()
  }
  
  
  func test_003_SwitchToNewWorkspace() {
    let app = XCUIApplication()
    let workspacetoolbarWorkspacebuttonButton = app.buttons["WorkspaceToolbar.WorkspaceButton"]
    let newWorkspaceName = "test_003_workspace"
    connectToServer()
    
    newWorkspaceWithName(newWorkspaceName)
    XCTAssertTrue(newWorkspaceName == workspacetoolbarWorkspacebuttonButton.label)
    deleteWorkspace(newWorkspaceName)
    
    disconnectFromServer()
  }

}
