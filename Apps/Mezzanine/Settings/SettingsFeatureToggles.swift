//
//  SettingsFeatureToggles.swift
//  Mezzanine
//
//  Created by Ivan Bella Lopez on 08/03/16.
//  Copyright © 2016 Oblong Industries. All rights reserved.
//

import UIKit

class SettingsFeatureToggles : NSObject {
  
  @objc static let sharedInstance = SettingsFeatureToggles()
  
  // Tap presentations items toggle
  let kSettingsTogglePresentationItemTap = "settings.toggle.presentationItemTap"
  @objc var presentationItemTap: Bool
  
  // Teamwork UI toggle
  let kSettingsToggleTeamworkUI = "settings.toggle.teamworkUI"
  @objc var teamworkUI: Bool

  //This prevents others from using the default '()' initializer for this class
  fileprivate override init() {

    // Added a custom KV TargetName. To distinguish between 'Mezzanine' and 'Mezzanine In-House' it's the only way.
    // The previous approach to check against the 'CFBundleDisplayName' is not valid because both have 'Mezzanine'.
    let targetName = Bundle.main.infoDictionary!["TargetName"] as! String

    if targetName == "Mezzanine" {
      // Mezzanine (release) starts with all feature toggles set to false
      presentationItemTap = false
      teamworkUI = true
    }
    else if targetName == "Mezzanine In-House" {
      // Mezzanine In-House may be have feature toggles set to true for internal testing with sales
      presentationItemTap = false
      teamworkUI = true
    }
    else {
      // Mezzanine-alpha starts with all feature toggles set to true for experimentation
      presentationItemTap = true
      teamworkUI = true
    }
  }
  
  @objc func updateSettingsToggles() {
    // presentationItemTap
    let presentationItemTap = UserDefaults.standard.value(forKey: kSettingsTogglePresentationItemTap)
    if presentationItemTap != nil {
      self.presentationItemTap = presentationItemTap as! Bool
    }
    
    // teamworkUI
    let teamworkUI = UserDefaults.standard.value(forKey: kSettingsToggleTeamworkUI)
    if teamworkUI != nil {
      self.teamworkUI = teamworkUI as! Bool
    }
  }
}
