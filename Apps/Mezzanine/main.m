//
//  main.m
//  Mezzanine
//
//  Created by Zai Chang on 9/17/10.
//  Copyright 2010 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MezzanineAppDelegate.h"

int main(int argc, char *argv[])
{
  @autoreleasepool
  {
    return UIApplicationMain(argc, argv, nil, NSStringFromClass([MezzanineAppDelegate class]));
  }
}
