#!/bin/bash

# References:
# http://digitalflapjack.com/blog/2012/may/09/buildnumbers/
# http://www.egeek.me/2013/02/09/xcode-insert-git-build-info-into-ios-app/

# Project info.plist for debug
#info_plist="Mezzanine-Info.plist"
info_plist="${BUILT_PRODUCTS_DIR}/${EXECUTABLE_FOLDER_PATH}/Info.plist"

shortVersionName=`git describe | awk '{split($0,a,"-"); print a[2]}'`
shortVersionString=""
fullVersionString=""
commitCount=`git rev-list HEAD --count`

index=0
shortVerionStringComponentsCount=0

for element in ${shortVersionName//./ } ; do
if [ $index -eq 0 ] ; then
	shortVerionStringComponentsCount=1
	shortVersionString=$element
	fullVersionString=$element
elif [ $index -eq 1 ] ; then
	shortVerionStringComponentsCount=2
	shortVersionString=$shortVersionString"."$element
	fullVersionString=$fullVersionString"."$element
elif [ $index -eq 2 ] ; then
	if [ $element -gt 0 ] ; then
		shortVerionStringComponentsCount=3
		shortVersionString=$shortVersionString"."$element
	fi
fi
	let "index=$index + 1"
done


# If the short version code only has two components, such as 1.0
# we need to append the Z number for the long version such that
# next number can be the build number
if [ $shortVerionStringComponentsCount -eq 2 ] ; then
	fullVersionString=$fullVersionString".0"
fi

fullVersionString=$fullVersionString"."$commitCount

echo "Setting CFBundleShortVersionString to ${shortVersionString}"
/usr/libexec/PlistBuddy -c "Set :CFBundleShortVersionString '${shortVersionString}'" "${info_plist}"

echo "Setting CFBundleVersion to ${fullVersionString}"
/usr/libexec/PlistBuddy -c "Set :CFBundleVersion '${fullVersionString}'" "${info_plist}"
