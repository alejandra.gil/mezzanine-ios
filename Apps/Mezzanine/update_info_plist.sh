#!/bin/bash

# References:
# http://digitalflapjack.com/blog/2012/may/09/buildnumbers/
# http://www.egeek.me/2013/02/09/xcode-insert-git-build-info-into-ios-app/

shortVersionName=`git describe --long | awk '{split($0,a,"-"); print a[2]}'`
patch=`git describe --long | awk '{split($0,a,"-"); print a[3]}'`

OLD_IFS="$IFS"
IFS="."
STR_ARRAY=( $shortVersionName )
IFS="$OLD_IFS"

# Full version name like: X.Y.Z.patch

if ((${#STR_ARRAY[@]} < 3))
then
  shortVersionName=$shortVersionName".0"
fi

fullVersionName=$shortVersionName"."$patch

info_plist="${TARGET_BUILD_DIR}/${EXECUTABLE_FOLDER_PATH}/Info.plist"

echo "Update ${info_plist} to CFBundleShortVersionString=${shortVersionName} and CFBundleVersion=${fullVersionName}"

/usr/libexec/PlistBuddy -c "Set :CFBundleShortVersionString '${shortVersionName}'" "${info_plist}"
/usr/libexec/PlistBuddy -c "Set :CFBundleVersion '${fullVersionName}'" "${info_plist}"
