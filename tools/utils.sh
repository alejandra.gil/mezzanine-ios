#!/bin/bash

: ${VERSION:=latest}
MEZZ_IMAGE="gitlab.oblong.com:4567/mezzanine/mezzanine-image:$VERSION-application-16.04"
: ${CONTAINER:=mezzanine}

function pull_latest_image()
{
  if ! docker pull $MEZZ_IMAGE; then
    echo "Failed to pull mezzanine image"
    echo "Try \`docker login gitlab.oblong.com:4567\` first."
  fi
}

# Regenerate certificate with a valid IP and FQDN that can be verified by iOS
# Otherwise test infrastructure will be stuck with an invalid FQDN or IP.
# TODO: Replace manual username&password introduction with a token or any system that allow automation.
function regen_cert()
{
  read -p 'Username: ' uservar
  read -sp 'Password: ' passvar
  CA_REPO="https://$uservar:$passvar@gitlab.oblong.com/mezzanine/mezzanine-ca"
  cmd="git clone $CA_REPO && cd mezzanine-ca && git checkout debug && sudo FQDN=localhost IP_ADDR=127.0.0.1 ./setup_ca.sh install"
  docker exec $CONTAINER bash -c "${cmd}"
}

# Regenerate mip.conf so the certificate 'issued_to' field is wiped out and
# Screencast can fallback to saving IP address. Otherwise test infrastructure
# will be stuck with an unresolvable FQDN.
function regen_mip()
{
  cmd='. /etc/oblong/startup-commons && $G_SPEAK_DIR/share/oblong/mip/tools/create_server.py /etc/oblong/mip.conf'
  docker exec $CONTAINER bash -c "${cmd}"
}

function clean_run_opts()
{
  printf "\e[32mCleaning opts\n"
  RUN_OPTS="$(echo ${RUN_OPTS##*()})"
  printf "Running with the following options:\e[0m "
  echo "${RUN_OPTS}"
}

function publish_web_ports()
{
  echo "-------------------------------"
  printf "\e[32mpublishing ports 80 & 443 for use on mac / windows\e[0m\n"
  echo "-------------------------------"
  RUN_OPTS="${RUN_OPTS} -p 80:80 -p 443:443 -p 65456:65456"
}

function run_sed_replace()
{
  local key=$1
  local val=$2
  local path=$3
  echo "Setting ${key} to ${val}"
  cmd="sed -i --follow-symlinks -e \"s/^\\([^#]*${key}:\\)\\(.*\\)$/\\1 ${val}/\" ${path}"
  docker exec $CONTAINER bash -c "${cmd}"
}

function run_container()
{
  docker run \
    --detach \
    --privileged \
    --name=$CONTAINER \
    --hostname=$CONTAINER \
    ${RUN_OPTS} \
    $MEZZ_IMAGE "$@" --headless
}

function wait_for_service_to_be_active()
{
  local service=$1
  timeout=0
  until [ "`docker exec ${CONTAINER} systemctl is-active ${service}`" = "active" ]; do
    timeout=$((timeout + 1))
    if [ "$timeout" -eq 3600 ]; then
      echo "Timeout waiting for $service service to be active"
      exit 1
    fi
    sleep 0.1
  done
  printf "\e[32m$service is now ready\e[0m\n"

}

function wait_for_configuration()
{
  if ! docker exec $CONTAINER /resources/ready config; then
    echo
    echo "ERROR: configuration did not complete as expected; aborting..."
    exit 1
  fi
  wait_for_service_to_be_active "mezzanine"
  echo "-------------------------------"
  printf "\e[32mRestarting services\e[0m\n"
  run_sed_replace "remote-access-enabled" "true" "/etc/oblong/app-settings.protein"
  regen_cert
  regen_mip
  docker exec $CONTAINER systemctl restart mezzanine
  docker exec $CONTAINER systemctl restart nginx

  sleep 2 # wait for mezzanine service to become inactive
  wait_for_service_to_be_active "mezzanine"
  wait_for_service_to_be_active "nginx"
  wait_for_service_to_be_active "screenshare-provisioner"
  wait_for_service_to_be_active "siemcy"
}

function finalize_container_creation()
{
  local format ipaddress
  format='{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}'
  ipaddress="$(docker inspect --format="$format" "$CONTAINER")"

  echo
  echo "Started container: $CONTAINER"
  echo
  echo "   ip address: $ipaddress"
  echo "     hostname: $CONTAINER"
  echo
  echo "To login to the container use:"
  echo
  echo "    docker exec -ti $CONTAINER bash"
  echo
  echo "To remove the container use:"
  echo
  echo "    docker rm -f $CONTAINER"
  echo
  echo "To access the web app on windows / mac:"
  echo
  echo "    https://localhost"
  echo
  echo "To access the web app on linux:"
  echo
  echo "    https://$ipaddress"
  echo
}

function save_ip()
{
  local format ipaddress
  format='{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}'
  ipaddress="$(docker inspect --format="$format" "$CONTAINER")"
  echo $ipaddress > ${CONTAINER}-IP
}

function usage()
{
  echo "Usage:"
  echo "-p: publish ports 80 and 443 for use on mac / windows"
  echo
  echo '------------------------------------------------------------------------'
  echo "This script creates a mezzanine container using the latest application"
  echo "image. Run with -p to publish ports 80 and 443 to access the container"
  echo "localhost on mac and windows. Add CONTAINER=<container_name> to set the"
  echo "name of the container otherwise it will default to mezzanine."
  exit 1
}

function die()
{
  printf "\e[31mERROR: $*. Aborting.\e[0m" >&2
  exit 1
}

