#!/bin/bash

shortVersionName=`git describe --long | awk '{split($0,a,"-"); print a[2]}'`
major=`echo ${shortVersionName} | awk '{split($0,a,"."); print a[1]}'`
minor=`echo ${shortVersionName} | awk '{split($0,a,"."); print a[2]}'`
micro=`echo ${shortVersionName} | awk '{split($0,a,"."); print a[3]}'`

if [ -z "$micro" ]
then 
  micro="0"
fi

nextMicro=$((micro+1))
nextShortVersionName="${major}.${minor}.${nextMicro}"
finalTag="rel-${shortVersionName}-final"
nextVersionTag="rel-${nextShortVersionName}"

echo "Tag final version tag ${finalTag}"
git tag ${finalTag}

echo "Empty commit to start ${nextVersionTag}"
git commit --allow-empty -m "Empty commit to hang new tag: ${nextVersionTag}"

echo "Tag final new version ${nextVersionTag}"
git tag -a ${nextVersionTag} -m "${nextVersionTag}"

echo ""
echo "Tagging completed"
echo ""
read -p "Do you want to push the changes? [y]" RESPONSE
if [ "$RESPONSE" = "" ]; then RESPONSE="y"; fi
if [ "$RESPONSE" = "Y" ]; then RESPONSE="y"; fi
if [ "$RESPONSE" = "Yes" ]; then RESPONSE="y"; fi
if [ "$RESPONSE" = "yes" ]; then RESPONSE="y"; fi
if [ "$RESPONSE" = "YES" ]; then RESPONSE="y"; fi
if [ "$RESPONSE" = "y" ]; then
  echo "pushing changes"
  git push origin ${finalTag}
  git push origin master
  git push origin ${nextVersionTag}
fi

