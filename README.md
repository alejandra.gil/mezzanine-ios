# Where to start?

git clone this project and update all its submodules with:

```
cd mezzanine-ios
git submodule update --recursive --init
```

Then open the Xcode project at:

`mezzanine-ios/Apps/Mezzanine/Mezzanine.xcodeproj`

And start hacking :)

# Versioning (Miguel)

Mezzanine iOS has followed the same versioning pattern than the whole Mezzanine ecosystem. The rest of projects (mezzanine, mezzanine-web, admin-web-mezzanine, …) are updated by the automation team but mezzanine-ios versioning is maintained by us.

Typically after a release we increment the version number the client will use for the next release. We use a script located under the tools folder. They increment the minor or micro versions that are used for each release or hot fix, respectively. Those scripts will tag the final commit of the previous release, the initial one of the new release and create an empty commit to start that new build. Then they will be pushed to the repo after confirmation that all values are ok.

`tools/bump-minor-version.sh`

`tools/bump-micro-version.sh`

# Localytics (Ivan)
Mezzanine iOS Client is using Google Analytics as well (as a company wide option for data usage and analytics). Before this was adopted by all platforms, Mezzanine iOS Client has been using Localytics. Localytics is a more focused mobile option, that’s why we preferred this option.

Credentials: Credentials and keys are here. Look for the Localytics entry at Mezzanine folder.

List of tracked events:
https://docs.google.com/spreadsheets/d/1s7MGhAYWsZIB-NHXD5euK3zF52sjLkHxtIXlNa2uHXk/edit#gid=0

We track events (actions done by user) and screens. This allow us to inspect the user’s navigation flow in the dashboard to understand their usage behaviour.

# Releasing
See the `./Docs/RELEASING.md` for a lot more detail on this subject.

# More about this project

Check the shared drive folder for iOS:
https://drive.google.com/drive/u/0/folders/1T1_pU6uoxaAvJiVlRjXbT2L1Ba7XptKf