//
//  NSObject+BlockObservation.h
//  Version 1.0
//
//  Andy Matuschak
//  andy@andymatuschak.org
//  Public domain because I love you. Let me know how you use it.
//
//  Version 1.1 
//
//  Zai Chang
//  zehao.chang@gmail.com
//  iOS-compatibility, and feature additions


#import <Foundation/Foundation.h>

typedef NSString AMBlockToken;
typedef void (^AMBlockTask)(id obj, NSDictionary *change);

@interface NSObject (AMBlockObservation)
- (AMBlockToken *)addObserverForKeyPath:(NSString *)keyPath task:(AMBlockTask)task;
- (AMBlockToken *)addObserverForKeyPath:(NSString *)keyPath onQueue:(NSOperationQueue *)queue task:(AMBlockTask)task;
- (AMBlockToken *)addObserverForKeyPath:(NSString *)keyPath options:(NSKeyValueObservingOptions)options onQueue:(NSOperationQueue *)queue task:(AMBlockTask)task;

- (void)removeObserverWithBlockToken:(AMBlockToken *)token;
@end
