//
//  Conference.swift
//  ObPexKit
//
//  Created by oblong on 15/12/17.
//  Copyright © 2017 oblong. All rights reserved.
//

import Foundation
import WebRTC

public enum AudioOutputDevice {
  case speaker
  case defaultDevice
}

public enum ServiceError {
  case ok, pinRequired, guestOnly, invalidPin, badRequest, error, alreadyActive, dataError
}

public class Conference: NSObject {
  
  var uri: ConferenceURI?
  var displayName: String?
  var call: Call?
  var dyingCall: Call?
  var queue = DispatchQueue(label: "com.oblong.obpexkit")
  var refreshTimer: Timer?
  var myParticipantUUID: UUID?
  public var useOnlyConferenceIdForRequests:Bool = false
  public var token: String?
  var tokenResp: [String : AnyObject]?
  public var videoView: PexVideoView?
  public var selfVideoView: PexVideoView?
  var queryTimeout: TimeInterval = 7
  var callQueryTimeout: TimeInterval = 62
  
  public override init() {}
  
  public var audioOutput: AudioOutputDevice = .defaultDevice
  public var audioMute: Bool = false {
    didSet {
      updateAudioMute (mute: audioMute)
    }
  }
  public var videoMute: Bool = false {
    didSet {
      updateVideoMute (mute: videoMute)
    }
  }
  
  public func requestToken(completion: @escaping (ServiceError) -> Void) {
    
    // Documentation https://docs.pexip.com/api_client/api_rest.htm#request_token
    
    // For proper operation, you should perform an SRV lookup as defined here:
    // https://docs.pexip.com/end_user/guide_for_admins/configuring_dns_pexip_app.htm
    // Then connect to the host that is returned from the lookup rather than the domain
    // in the URI
    
    guard let requestTokenAPICall = requestTokenAPICall(),
          let url = URL(string: requestTokenAPICall)
    else {
      print("failed to build request token URL")
      completion(ServiceError.error)
      return
    }
    
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.timeoutInterval = self.queryTimeout
    request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringCacheData
    let task = URLSession.shared.dataTask(with: request) { data, response, error in
      do {
        guard error == nil else {
          print("error with request: \(String(describing: error?.localizedDescription))")
          completion(ServiceError.error)
          return
        }
        guard let data = data else {
          print("no data in response")
          completion(ServiceError.dataError)
          return
        }
        guard let jsonTockenResponse = try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject] else {
          print("error trying to convert data to JSON")
          completion(ServiceError.dataError)
          return
        }
        
        if jsonTockenResponse["status"] as! String == "success" {
          if let _ = jsonTockenResponse["result"]?["pin"] as? String {
            
            // Looks like we need PIN's - check if guests and / or hosts require them
            // See https://docs.pexip.com/admin/pins_hosts_guests.htm for more information
            
            print("MISSING PIN IMPLEMENTATION")
            completion(ServiceError.pinRequired)
          }
          else {
            print("OK, we're in")
            
            self.tokenResp = jsonTockenResponse["result"] as! [String : AnyObject]?
            self.token = (self.tokenResp?["token"] as? String)!
            
            // At this point you could store the token information for later usage.
            // For now let's stash the participant_uuid (needed later for participant operations)
            
            let uuidString = self.tokenResp?["participant_uuid"] as! String
            self.myParticipantUUID = UUID(uuidString: uuidString)
            
            // Let's setup a timer to refresh the token
            // Documentation https://docs.pexip.com/api_client/api_rest.htm#refresh_token
            if let expires = self.tokenResp?["expires"] as? String,
              let exp = Int(expires) {
              DispatchQueue.main.async {
                self.refreshTimer = Timer.scheduledTimer(timeInterval: TimeInterval(exp/4), target: self, selector: .refreshSelector, userInfo: nil, repeats: true);
              }
            } else {
              print("Failed to create timer")
            }
            
            completion(ServiceError.ok)
          }
        } else {
          print("failed")
          if let result = jsonTockenResponse["result"] as? String {
            if result.lowercased() == "invalid pin" {
              completion(ServiceError.invalidPin)
            } else {
              completion(ServiceError.error)
            }
            
          }
        }
        
      } catch {
        print("Got error with request")
        completion (ServiceError.badRequest)
      }
    }
    task.resume()
  }
  
  @objc func refreshToken(timer: Timer) {
    
    // Documentation https://docs.pexip.com/api_client/api_rest.htm#refresh_token
    
    guard let refreshTockenApiCall = refreshTockenApiCall(),
          let url = URL(string: refreshTockenApiCall)
    else {
      print("failed to build refresh token URL")
      return
    }
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.timeoutInterval = self.queryTimeout
    request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringCacheData
    if let token = self.token {
      request.addValue(token, forHTTPHeaderField: "token")
    }
    let task = URLSession.shared.dataTask(with: request) { data, response, error in
      do {
        guard error == nil else {
          print("error with request: \(String(describing: error?.localizedDescription))")
          return
        }
        guard let data = data else {
          print("no data in response")
          return
        }
        guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject] else {
          print("error trying to convert data to JSON")
          return
        }
        if let status = json["status"] as? String {
          if status.lowercased() == "success" {
            if let result = json["result"] as? [String: AnyObject] {
              self.token = result["token"] as? String
            }
          }
        }
        print(" -- token refreshed --")
      } catch {
        print("Got error with refresh")
      }
    }
    task.resume()
  }
  
  public func connect(_ displayName: String, URI: ConferenceURI, completion: @escaping (_ isOk: Bool) -> Void) {
    self.displayName = displayName
    self.uri = URI
    
    self.requestToken { status in
      completion (status == .ok)
    }
  }
  
  public func escalateMedia(video: Bool, resolution: Resolution, completion: @escaping (ServiceError) -> Void) {
    guard let uri = self.uri else {
      print("empty URI")
      return
    }
    
    self.call = Call(uri: uri, videoView: video ? self.videoView : nil, selfVideoView: video ? self.selfVideoView : nil, resolution: resolution) { sdp in
      
      print("Call object said our SDP is: \(sdp)")
      
      // Let's offer our SDP to the MCU using the participant function "call" on the API
      // https://docs.pexip.com/api_client/api_rest.htm?#calls
      
      let callOffer = [
        "call_type": "WEBRTC",
        "sdp": sdp.sdp
      ]
      let jsonBody = try? JSONSerialization.data(withJSONObject: callOffer, options: [])
      guard let callsAPICall = self.callsAPICall(),
            let url = URL(string: callsAPICall)
      else {
        print("failed to build escalate URL")
        return
      }
      var request = URLRequest(url: url)
      request.httpMethod = "POST"
      // The timeout here is greater as it can take up to 60s for the MCU
      // to timeout the outbound call if it doesn't reach a participant so
      // we need to give it time to return a failure response to us rather
      // than timing out the request ourselves
      // You could use something like:
      //   let action = url.lastPathComponent?.characters.split{$0 == "?"}.map(String.init)
      //   let timeout = action![0] == "calls" ? self.callsQueryTimeout : self.queryTimeout
      // in a generic "request" function for all calls to differentiate
      // between call and non-call API queries
      request.timeoutInterval = self.callQueryTimeout
      request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringCacheData
      request.addValue("application/json", forHTTPHeaderField: "Content-type")
      request.httpBody = jsonBody
      if let token = self.token {
        request.addValue(token, forHTTPHeaderField: "token")
      }
      let task = URLSession.shared.dataTask(with: request) { data, response, error in
        do {
          guard error == nil else {
            print("error with request: \(String(describing: error?.localizedDescription))")
            return
          }
          guard let data = data else {
            print("no data in response")
            return
          }
          guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject] else {
            print("error trying to convert data to JSON")
            return
          }
          if let status = json["status"] as? String {
            if status.lowercased() == "success" {
              if let result = json["result"] as? [String: AnyObject] {
                
                // We should now have SDP and a new call uuid
                
                let uuidString = result["call_uuid"] as! String
                self.call?.uuid = UUID(uuidString: uuidString)
                let remoteSdp = result["sdp"] as! String
                print("Their SDP is: \(remoteSdp)")
                
                print("Setting remote SDP on call object")
                // Let's send their SDP back into the machine
                self.call?.setRemoteSdp(sdp: RTCSessionDescription(type: RTCSdpType.answer, sdp: remoteSdp)) { status in
                  
                  // check status and ACK the request to start the media flow
                  // See https://docs.pexip.com/api_client/api_rest.htm?#ack
                  
                  print("status for remote SDP was \(String(describing: status))")
                  self.ack()
                  
                  completion(.ok)
                }
              }
            }
          }
        } catch {
          print("Got error with call SDP")
        }
      }
      task.resume()
      
    }
    
  }
  
  func ack() {
    
    // Ack the call to start media
    // Documentation: https://docs.pexip.com/api_client/api_rest.htm?#ack
    
    guard let ackString = ackString(),
          let url = URL(string: ackString)
    else {
      print("failed to build ack URL")
      return
    }
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringCacheData
    if let token = self.token {
      request.addValue(token, forHTTPHeaderField: "token")
    }
    let task = URLSession.shared.dataTask(with: request) { data, response, error in
      print("doing")
      guard error == nil else {
        print("error with request: \(String(describing: error?.localizedDescription))")
        return
      }
      print("Call \(self.call!.uuid!.uuidString.lowercased()) acknowledged")
    }
    task.resume()
  }

  
  func disconnectMedia(completion: @escaping (ServiceError) -> Void) {
    
    print("Tearing down call and media")
    
    // Bring down the call
    // Documentation: https://docs.pexip.com/api_client/api_rest.htm#call_disconnect
    
    guard let disconnectString = disconnectString(),
          let url = URL(string: disconnectString)
    else {
      print("failed to build disconnect URL")
      return
    }

    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringCacheData
    if let token = self.token {
      request.addValue(token, forHTTPHeaderField: "token")
    }
    let task = URLSession.shared.dataTask(with: request) { data, response, error in
      guard error == nil else {
        print("error with request: \(String(describing: error?.localizedDescription))")
        return
      }
      // ignoring status here, we could parse it if we wanted to
      print("call disconnected")
      
      DispatchQueue.main.async {
        self.call?.peerConnection?.remove((self.call?.mediaStream)!)
        self.call?.mediaStream = nil
        self.call?.audioTrack = nil
        self.call?.videoTrack = nil
        
        self.call?.peerConnection?.close()
        self.call?.peerConnection = nil
        self.call?.videoView = nil
        RTCCleanupSSL()
      }
      completion(.ok)
    }
    task.resume()
    
  }
  
  public func releaseToken(completion: @escaping (ServiceError) -> Void) {
 
    // Quit the conference by releasing the token
    // Documentation: https://docs.pexip.com/api_client/api_rest.htm?#release_token
    
    if self.call != nil {
      self.disconnectMedia { status in
        print("Disconnected media stack")
      }
    }
    
    // Cancel the refresh timer, we're done
    DispatchQueue.main.async {
      print("Cancelled token refresh timer")
      self.refreshTimer?.invalidate()
    }
    
    guard let releaseTokenApiCall = releaseTokenApiCall(),
          let url = URL(string: releaseTokenApiCall)
    else {
      print("failed to build release URL")
      return
    }
    
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringCacheData
    if let token = self.token {
      request.addValue(token, forHTTPHeaderField: "token")
    }
    let task = URLSession.shared.dataTask(with: request) { data, response, error in
      guard error == nil else {
        print("error with request: \(String(describing: error?.localizedDescription))")
        return
      }
      print("Token released, we're outta here")
      completion(.ok)
    }
    task.resume()
 }
  
  
  public func flipCamera() {
    guard let call = call else { return }
    call.swapCamera()
  }
  
  private func updateAudioMute (mute: Bool) {
// This would mute audio IN
//    guard let call = call,
//          let audioTrack = call.audioTrack else { return }
//
//    audioTrack.isEnabled = mute
    
    guard let call = call,
          let mediaStream = call.mediaStream,
          let audioStreamOut = mediaStream.audioTracks.first else { return }
    audioStreamOut.isEnabled = !mute
  }

  private func updateVideoMute (mute: Bool) {
// This would mute video IN
//    guard let call = call,
//      let videoTrack = call.videoTrack else { return }
//
//    videoTrack.isEnabled = mute
    
    guard let call = call,
          let mediaStream = call.mediaStream,
          let videoStreamOut = mediaStream.videoTracks.first else { return }
    videoStreamOut.isEnabled = !mute
  }
}

public class PexVideoView: RTCEAGLVideoView {
  
}

public struct ConferenceURI {
  
  func lookup (hostname: String) -> String? {
    
    let host = CFHostCreateWithName(nil, hostname as CFString).takeRetainedValue()
    CFHostStartInfoResolution(host, .addresses, nil)
    var success: DarwinBoolean = false
    if let addresses = CFHostGetAddressing(host, &success)?.takeUnretainedValue () as NSArray?,
      let theAddress = addresses.firstObject as? NSData {
      var hostname = [CChar](repeating:0, count: Int(NI_MAXHOST))
      if getnameinfo(theAddress.bytes.assumingMemoryBound(to: sockaddr.self), socklen_t(theAddress.length), &hostname, socklen_t(hostname.count),nil, 0, NI_NUMERICHOST) == 0 {
        let numAddress = String(cString:hostname)
        print (numAddress)
        return numAddress
      }
    }
    return nil
  }
  
  var host: String?
  var conference: String?
  var hostname: String?
  
  public init (conference: String, hostname: String) {
    
    self.conference = conference
    self.hostname = hostname
    self.host = hostname //lookup(hostname: hostname)
  }
}

private extension Selector {
  static let refreshSelector =
    #selector(Conference.refreshToken(timer:))
}

// Pexip API Calls
private extension Conference {
  
  var baseRequest: String? {
    get {
      guard let uri = self.uri,
            let host = uri.host,
            let conference = uri.conference else { return nil }
      return "https://\(host)/api/client/v2/conferences/\(conference)"
    }
  }
  
  func requestTokenAPICall() -> String? {
    guard let baseRequest = self.baseRequest,
          let displayName = self.displayName else { return nil }
    guard let escapedDisplayName = displayName.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) else { return nil }
    return baseRequest+"/request_token?display_name=\(escapedDisplayName)"
  }
  
  func refreshTockenApiCall() -> String? {
    guard let baseRequest = self.baseRequest else { return nil }
    return baseRequest+"/refresh_token"
  }
  
  func releaseTokenApiCall() -> String? {
    guard let baseRequest = self.baseRequest else { return nil }
    return baseRequest+"/release_token"
  }
  
  func callsAPICall() -> String? {
    guard let baseRequest = self.baseRequest,
          let myParticipantUUID = self.myParticipantUUID else { return nil }
    let participantUUID = myParticipantUUID.uuidString.lowercased()
    return baseRequest+"/participants/\(participantUUID)/calls"
  }
  
  func disconnectString() -> String? {
    guard let baseRequest = self.baseRequest,
          let myParticipantUUID = self.myParticipantUUID,
          let call = self.call,
          let uuid = call.uuid else { return nil }
    let participantUUID = myParticipantUUID.uuidString.lowercased()
    let callUUID = uuid.uuidString.lowercased()
    return baseRequest+"/participants/\(participantUUID)/calls/\(callUUID)/disconnect"
  }
  
  func ackString() -> String? {
    guard let baseRequest = self.baseRequest,
      let myParticipantUUID = self.myParticipantUUID,
      let call = self.call,
      let uuid = call.uuid else { return nil }
    let participantUUID = myParticipantUUID.uuidString.lowercased()
    let callUUID = uuid.uuidString.lowercased()
    return baseRequest+"/participants/\(participantUUID)/calls/\(callUUID)/ack"
  }
  
}

