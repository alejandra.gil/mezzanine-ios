# Releasing

This document explains the process for releasing a new version of the Mezzanine iOS app onto the App Store.

This process takes a few days since an actual person at Apple needs to test the app. Because our app doesn't do much when not in a meeting, we need a session that they can mezz into. In the past this was done by exposing a physical Mezzanine's IP to the public. Because this is insecure and impractical, we now instead keep a mezz-in session alive so we no longer tie-up an actual mezzanine.

## Things To Know

### Certificates

Apple uses certificates for signing app builds and releases. Make sure you have an Oblong Apple Developer account with a role of App Manager or higher. Contact IT if you do not.

Certificates can be viewed at http://developer.apple.com -> `Certificates, IDs, & Profiles`. If you do not already have one, create and install an `Apple Development` certificate. Apple's website will walk you through the process.

Once certificates are properly installed, you should be able to run the mezzanine-ios app on a physical device for testing.

### Meeting IDs and Aliases

When you start a mezzanine meeting, there are two IDs- a meeting ID, and a meeting alias; The meeting ID points to the specific running meeting instance, while the alias exists to allow things like joining a scheduled meeting that may not have actually started yet.

For example, given a meeting with alias `123456789` and id `987654321`, we can access the meeting via

1) `https://mezz-in.com/a/123456789` (typical)
1) `https://mezz-in.com/m/987654321` (direct)

### Sessions

When a meeting is started, a session is created in the `meow` cloud container. When a meeting is ended, this session gets automatically destroyed. A large portion of this document is dedicated to explaining the creation and keep-alive of this session.

# Creating a Session

First we want to create a Mezz-In enabled Mezzanine meeting session and get its meeting ID (not alias). To do this, we will launch a mezzanine docker container locally.

1. Clone the `git@gitlab.oblong.com:mezzanine/mezzanine-image` repo
1. Follow its instructions for launching a containerized mezzanine (tldr `./run`)
1. Setup the containerized mezzanine to work with mezz-in
    1. Login to the container's web admin
    1. Infopresence -> Remote Participation
        * Set: `On`
        * Set Server URL: `https://api.mezz-in.com` (**https prefix is important**)
        * Save
    1. System -> Restart Mezzanine
1. Start the meeting through the mezzanine web interface
1. Invite Others to get the mezz in alias link and continue with step 2 in `Creating our Session`.
1. Take note of the meeting ID (not alias) by...
    * While running the mezzanine iOS app from XCode, joining the mezz-in fully, and watching the console for something like
        ```
            "video-chat" =     {
                pexip =         {
                    conference = 987654321;
                    node = "vtc.mezz-in.com";
                };
                service = pexip;
            };
        ```
    * In the desktop mezzanine app, the `Dial in from a Room` number is the meeting ID
    * Peeking proteins on the Mezzanine

1. Leave the meeting running until you've completed the steps in [Stayin' Alive](https://www.youtube.com/watch?v=Vmb1tqYqyII)


# Stayin' Alive

Let's keep the session we started alive so the Apple reviewer can test when they get around to it.

1. Git clone the [meow repository](https://gitlab.oblong.com/mezzanine/meow), read that repository’s readme for additional setup instructions. You will likely need your Google account added for full functionality. Upon setup completion, remain in the repo's directory.

1. Source the meowrc file:

    ```source .meowrc```

1. Enter the meow ops environment for production

    `meow ops meow-prod-1`

1. Touch a keepalive file for the meeting ID received (987654321)

    `kubectl exec $(kubectl get pods -l meeting-id=987654321 -o jsonpath={..metadata.name}) -- touch /.hk-keepalive`

5. Remove keepalive file **once the review is done**

    `kubectl exec $(kubectl get pods -l meeting-id=987654321 -o jsonpath={..metadata.name}) -- rm /.hk-keepalive`

You can now verify that the session has been kept alive by ending your mezzanine meeting and attempting to join `https://mezz-in.com/m/9876545321` through the iOS app. If the session was kept alive, you should still be able to join.

# Packaging the App

Now that we have a working session, we can package the app for upload to the app store. This can be done one of two ways-

## XCode

The app release process can happen entirely through XCode. After opening the project,

1. Set the schema to `Mezzanine Release-AppStore`
2. Build the project `Product -> Build`
3. Archive the project `Product -> Archive`

After archival, the organizer window should popup with your latest archive and the options to `Distribute` or `Validate` your app.

## CI/CD Pipeline
See https://gitlab.oblong.com/mezzanine/mezzanine-ios-ci for more details.

# Submitting the App

This is a standard process well documented by Apple.
https://help.apple.com/app-store-connect/#/dev301cb2b3e

1. In the XCode organizer window with the archive you've just created, select `Distribute` and work your way through the steps- you shouldn't need to change any of the default settings.

1.  Navigate to [AppStore Connect](https://appstoreconnect.apple.com/) ->
My Apps -> Mezzanine

1. Make sure TestFlight is setup so you can test your version

1. Create a new iOS Version (`+ Version or Platform`)

    1. Describe your changes in the `Version Information` section
    1. Select the build you uploaded earlier through XCode
    1. Update the `App Review Information` section to reflect the meeting ID of your kept alive session created earlier, along with your contact information.
    1. Save
    1. When prompted, we do not use an advertising identifier

1. Wait for Review, and put out the fires as they come up

# Releasing an Approved App

Once the app is approved, the new person in charge will receive a notification about it will be asked to manually release it if it was set so.

## Accounts

- App Store:

    Used to generate certificates for external distribution and access to the App Store and TestFlight (dev/test internal and external distribution).

- Enterprise:

    Used to generate certificates for internal and ad-hoc distribution (AirWatch). Please, never use this certificates to sign apps distributed to customers otherwise Apple could put us in their black list, revoke all of our certificates, and so on.