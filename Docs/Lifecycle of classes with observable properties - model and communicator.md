#Problems we want to solve:

- We don't have control of:
	- initialization
	- observation ends
	- first load of views
	- properties: didSet not being called


# Lifecycle of classes with potentially observable properties: model and communicator



## Initizalization

Empty:

```
func init()
```

SystemModel/Communicators are set after init its called
They are optional properties


## Observation

```
var systemModelObservers = [String]()
var systemModel: MZSystemModel? {

	willSet(newSystemModel) {
		endObservingSystemModel()
	}
	
	didSet {
		beginObservingSystemModel()
	}
}

func beginObservingSystemModel() {
	guard let systemModel = systemModel else { return }
	
	// Observer whatever you need
	systemModelObservers.append(...)
}

func endObservingSystemModel() {
	guard let systemModel = systemModel else { return }

	for token in systemModelObservers {
		systemModel.removeObserver(token)
	}
	systemModelObservers.removeAll()
}
```

### Observing properties of another property

We prefer to be as restrictive as possible and pass the less amount of properties as possible, so if a view controller requires observing properties of the communicator, systemModel and the currentworkspace we would only pass the communicator from the initializator class and the receiver would handle setting its own properties.


```
var workspace: MZWorkspace? {
	// Its own observation if requiered
	... 
}

var systemModel: MZSystemModel? {

   ...
	
	didSet {
	
		...
		
		workspace = systemModel != nil ?? systemModel.currentWorkspace : nil
	}
}
```

### Setting properties of instance objets

1. JIT

```
override func viewDidLoad() {
	super.viewDidLoad()
	
	subViewController.systemModel = systemModel 
}
```

2. While setting properties

```
var systemModel: MZSystemModel? {

   ...
	
	didSet {
		...
		
		subViewController.systemModel = systemModel
	}
}
```

## Deinitialization
The class that sets the property also unsets it.

This is wrong or unnecessary because the previous observation process should have make the owner of this class to set its systemModel to nil and at that moment stopped observing it:

```
deinit() {
	endObservingSystemModel()
}

```

