//
//  MezzCommunicatorTestAppDelegate.h
//  MezzCommunicatorTest
//
//  Created by Zai Chang on 11/29/10.
//  Copyright 2010 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MezzCommunicatorTestViewController;

@interface MezzCommunicatorTestAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    MezzCommunicatorTestViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet MezzCommunicatorTestViewController *viewController;

@end

