//
//  MezzCommunicatorTestViewController.m
//  MezzCommunicatorTest
//
//  Created by Zai Chang on 11/29/10.
//  Copyright 2010 Oblong Industries. All rights reserved.
//

#import "MezzCommunicatorTestViewController.h"

@implementation MezzCommunicatorTestViewController



/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
*/

- (void)loadView 
{
  [super loadView];
  
  timeInterval = 0.1;
  timeIntervalSlider.value = 0.5;
  [self timeIntervalChanged];
  
  outputTextView.text = @"Test me";
  
  NSURL *serverURL = [NSURL URLWithString:@"tcp://chang01.local"];
  communicator = [[MezzanineCommunicator alloc] initWithServerUrl:serverURL];
  
  [[NSNotificationCenter defaultCenter] addObserver:self 
                                           selector:@selector(communicatorDidConnect:) 
                                               name:MezzanineCommunicatorDidConnectToAllPoolsNotification 
                                             object:communicator];
}

-(void) communicatorDidConnect:(NSNotification*)notification
{
  [[NSNotificationCenter defaultCenter] addObserver:self 
                                           selector:@selector(receivedProteinFromDocumentSyncPool:) 
                                               name:OBPoolConnectorReceivedProteinNotification 
                                             object:communicator.documentSyncPool];
  
  [communicator stopHeartbeat];
}

-(void) beginSendTimer
{
  sendTimer = [NSTimer scheduledTimerWithTimeInterval:timeInterval 
                                               target:self
                                             selector:@selector(timerFired:) 
                                             userInfo:nil repeats:YES];
}

-(IBAction) begin
{
  if ([sendTimer isValid])
  {
    [sendTimer invalidate];
    sendTimer = nil;
    
    [beginButton setTitle:@"Begin" forState:UIControlStateNormal];
    return;
  }
  
  [beginButton setTitle:@"Stop" forState:UIControlStateNormal];
  
  // Don't want heartbeats messing things up
  [communicator stopHeartbeat];
  
  beginDate = [[NSDate date] retain];
  NSLog(@"Begin Date: %@", beginDate);
  
  [requestStack release];
  requestStack = [[NSMutableArray alloc] init];
  
  numberOfRequests = 0;
  numberOfResponses = 0;
  totalResponseTime = 0;
  averageResponseTime = 0;
  
  [self beginSendTimer];
}

-(IBAction) timeIntervalChanged
{
  timeInterval = 1/pow(10, timeIntervalSlider.value*2);
  
  timeIntervalTextField.text = [NSString stringWithFormat:@"%.2f Hz", 1/timeInterval];
  
  
  if ([sendTimer isValid])
  {
    [sendTimer invalidate];
    [self beginSendTimer];
  }
}

-(void) updateDisplay
{
  NSTimeInterval elapsedTime = [[NSDate date] timeIntervalSinceDate:beginDate];
  NSMutableString *output = [NSMutableString stringWithFormat:@"Elapsed Time: %f\n", elapsedTime];
  [output appendString:@"\n"];
  [output appendFormat:@"Requests: %d\n", numberOfRequests];
  [output appendFormat:@"... in Queue: %d\n", [communicator.documentSyncPool numberOfQueuedProteins]];
  [output appendString:@"\n"];
  [output appendFormat:@"Responses: %d\n", numberOfResponses];
  [output appendString:@"\n"];
  [output appendFormat:@"Last Time: %f\n", lastResponseTime];
  [output appendFormat:@"Average Time: %f\n", averageResponseTime];
  [output appendString:@"\n"];
  
  outputTextView.text = output;
}

-(void) timerFired:(NSTimer*) timer
{  
  numberOfRequests++;
  [requestStack addObject:[NSDate date]];
  [communicator sendRequestStatusProtein];
  [self updateDisplay];
}

-(void) receivedProteinFromDocumentSyncPool:(NSNotification*)notification
{
  OBProtein *p = [[notification userInfo] objectForKey:@"protein"];
  
  if (![communicator isProteinFromNativeApp:p])
    return;
  if (![p containsDescrip:@"deck-status"])
    return;
  
  numberOfResponses++;
  
  // Pop the stack
  if ([requestStack count] > 0)
  {
    NSDate *lastRequestDate = [requestStack objectAtIndex:0];
    NSTimeInterval responseTime = [[NSDate date] timeIntervalSinceDate:lastRequestDate];
    lastResponseTime = responseTime;
    [requestStack removeObjectAtIndex:0];
    
    totalResponseTime += lastResponseTime;
    averageResponseTime = totalResponseTime/numberOfResponses;
  }
  
  [self updateDisplay];
}

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}
*/


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

@end
