//
//  MezzCommunicatorTestViewController.h
//  MezzCommunicatorTest
//
//  Created by Zai Chang on 11/29/10.
//  Copyright 2010 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MezzanineCommunicator.h"

@interface MezzCommunicatorTestViewController : UIViewController 
{
  MezzanineCommunicator *communicator;
  
  IBOutlet UIButton *beginButton;
  IBOutlet UITextView *outputTextView;
  IBOutlet UISlider *timeIntervalSlider;
  IBOutlet UILabel *timeIntervalTextField;
  
  NSDate *beginDate;
  NSTimer *sendTimer;
  NSTimeInterval timeInterval;
  
  NSMutableArray *requestStack;
  int numberOfRequests;
  int numberOfResponses;
  float lastResponseTime;
  float totalResponseTime;
  float averageResponseTime;
}

-(IBAction) begin;
-(IBAction) timeIntervalChanged;

@end

