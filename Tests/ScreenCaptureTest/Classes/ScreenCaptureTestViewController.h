//
//  ScreenCaptureTestViewController.h
//  ScreenCaptureTest
//
//  Created by Zai Chang on 12/7/10.
//  Copyright 2010 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScreenCaptureTestViewController : UIViewController 
{
}

@property (nonatomic,retain) IBOutlet UIImageView *imageView;

@end

