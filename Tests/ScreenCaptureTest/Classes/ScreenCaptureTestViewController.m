//
//  ScreenCaptureTestViewController.m
//  ScreenCaptureTest
//
//  Created by Zai Chang on 12/7/10.
//  Copyright 2010 Oblong Industries. All rights reserved.
//

#import "ScreenCaptureTestViewController.h"
#import <QuartzCore/QuartzCore.h>


// To bypass compiler warning
CGImageRef UIGetScreenImage(void);

@implementation ScreenCaptureTestViewController

@synthesize imageView;

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
-(void) viewDidLoad 
{
  [super viewDidLoad];
  
  [NSTimer scheduledTimerWithTimeInterval:0.1
                                   target:self 
                                 selector:@selector(takeScreenshot:)
                                 userInfo:nil 
                                  repeats:YES];
   
}

-(void) takeScreenshot:(NSTimer*)timer
{
  static int count = 0;
  NSLog(@"Taking Screenshot %d", count++);
  
  CGImageRef screenImageRef = UIGetScreenImage();
  
  UIImage* screenImage = [UIImage imageWithCGImage:screenImageRef];
  
//  UIScreen *screen = [UIScreen mainScreen];
//  UIGraphicsBeginImageContext(screen.bounds.size);
//	CGContextRef context = UIGraphicsGetCurrentContext();
//  CGContextScaleCTM(context, 0.5, -0.5);
//  CGContextTranslateCTM(context, screen.bounds.size.width, screen.bounds.size.height);
//  
//  CGContextDrawImage(context, CGRectMake(0, 0, screen.bounds.size.width, screen.bounds.size.height), screenImageRef);
//  UIImage *screenImage = UIGraphicsGetImageFromCurrentImageContext();
//  UIGraphicsGetCurrentContext();
  
  CGImageRelease(screenImageRef);
  
  imageView.image = screenImage;
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

@end
