//
//  ScreenCaptureTestAppDelegate.h
//  ScreenCaptureTest
//
//  Created by Zai Chang on 12/7/10.
//  Copyright 2010 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ScreenCaptureTestViewController;

@interface ScreenCaptureTestAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    ScreenCaptureTestViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet ScreenCaptureTestViewController *viewController;

@end

