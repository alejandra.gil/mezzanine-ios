#/bin/bash

cp removePlasmaDependency.patch ../MezzanineTests/.
cd ../MezzanineTests
patch -p1 < removePlasmaDependency.patch
rm removePlasmaDependency.patch
