$LOAD_PATH << File.dirname(__FILE__) + "/../MezzanineTests/"
require 'rubygems'
require 'utils/puppets.rb'


# The following methods are just an rewritten version of the originals in
# MezzanineTests ruby utils to include the option for deleting old files.
# In here while waiting for native team to check changes on their own repo.
module Mezzanine
  module Utils

    def self.copy(from, to, delete = false)
      if from.is_a? String and to.is_a? String
        fail unless File.exists?(from)
        $log.debug "copying (cp) from: #{from} to: #{to}"
        `cp -a #{from} #{to}`
      elsif from.is_a? String and not to.is_a? String
        fail unless File.exists?(from)
        fail unless to.is_a?(Hash) and to.key?(:path)
        to = Mezzanine::Utils::remote_hash(to)
        if File.directory?(from)
        to_path = File.dirname(to[:path])
        else
        to_path = to[:path]
      end
        $log.debug "copying (rsync) from: #{from} to: #{to.inspect}"
        cmd = "rsync --rsync-path=\"sudo rsync\" -avz --copy-links -e \"ssh -o UserKnownHostsFile=/dev/null -o GlobalKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i #{to[:keyfile]} -l #{to[:username]}\" #{from} #{to[:host]}:#{to_path}"
        cmd = delete ? cmd + " --delete" : cmd
        $log.debug "executing: #{cmd}"
        `#{cmd}`
      elsif not from.is_a? String and to.is_a? String
        fail unless from.is_a?(Hash) and from.key?(:path)
        from = Mezzanine::Utils::remote_hash(from)
        to = to.chomp("/")
        `mkdir -p #{File.dirname(to)}`
        $log.debug "copying (rync) from: #{from.inspect} to: #{to}"
        cmd = "rsync --rsync-path=\"sudo rsync\" -avz --copy-links -e \"ssh -o UserKnownHostsFile=/dev/null -o GlobalKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i #{from[:keyfile]} -l #{from[:username]}\" #{from[:host]}:#{from[:path]} #{to}"
        cmd = delete ? cmd + " --delete" : cmd
        $log.debug "executing: #{cmd}"
        `#{cmd}`
      else
        fail unless from.is_a?(Hash) and from.key?(:path)
        fail unless to.is_a?(Hash) and to.key?(:path)
        from = Mezzanine::Utils::remote_hash(from)
        to = Mezzanine::Utils::remote_hash(to)
        $log.debug "copying (rsync) from: #{from.inspect} to: #{to.inspect}"
        Net::SSH.start(to[:host], to[:username], {:port => to[:port], :keys_only => true, :keys => [to[:keyfile]]}) do |ssh|
          from_identity_file = ssh.exec!("mktemp").strip.gsub(/\s+/,"")
          ssh.scp.upload!(from[:keyfile], from_identity_file)
          cmd = "sudo rsync --rsync-path=\"sudo rsync\" -avz -e \"ssh -o UserKnownHostsFile=/dev/null -o GlobalKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i #{from_identity_file} -l #{from[:username]}\" #{from[:host]}:#{from[:path]} #{to[:path]}"
          cmd = delete ? cmd + " --delete" : cmd
          $log.debug "running cmd: #{cmd}"
          ssh.exec!(cmd)
          ssh.exec!("rm -f #{from_identity_file}")
        end
      end
    end


  class Remote

    def upload(from, to, replace = false)
      fail unless to.is_a? String
      $log.debug "uploading from #{from.inspect} to #{@host}:#{to} [replacing content:#{replace.to_s}]"
      Mezzanine::Utils::copy(from, to_hash.merge!({:path => to}), replace)
    end

    def replace_mezzateria(name, swap = false)
      # There is some issue with DNS and docker so replacing "apricot" with its IP "10.10.192.24" -- Miguel
      upload({:host => "10.10.192.24", :keyfile => File.expand_path("~/.ssh/id_dockeroblong"), :path => "/chattel/#{name}/"}, "/chattel/mezzateria", swap)
    end

  end

end # module Utils
end # module Mezzanine


puts "Replacing mezzateria and removing the old one"
$puppets[0].remote.replace_mezzateria("mezzateria-ios-tests", true) #true means deleting the old mezzateria and leaving an exact copy of what is copied

puts "Configure Mezzanine as triptych"
$puppets[0].replace_config("fake-triptych")

puts "Restarting mezzanine"
$puppets[0].remote.service_restart(:mezzanine, [])

mz_project_folder = "../../Apps/Mezzanine/"

#set mezzanine server on its config file
File.open(mz_project_folder + "MezzanineUITests/mezz-test-server", 'w') { |file| file.write($puppets[0].host) }

#if run locally execute ui tests
if ARGV[0].eql?("local")

  system "xcrun simctl uninstall \"iPhone 6s\" com.oblong.Mezzanine-Alpha"

  system "MEZZ_TEST_SERVER=" + $puppets[0].host + " xcodebuild \
  -project " + mz_project_folder + "Mezzanine.xcodeproj \
  -scheme MezzanineUITests \
  -sdk iphonesimulator \
  -configuration Debug \
  -enableCodeCoverage YES \
  -destination 'platform=iOS Simulator,name=iPhone 6s,OS=9.2' \
  MEZZ_TEST_SERVER=" + $puppets[0].host + " \
  test"

else

  if File.exist?('/usr/local/bin/ios-deploy')
    #get all devices physically plugged to the bot server
    devices = %x[/usr/local/bin/ios-deploy -B -c -W -t 1 | grep USB | awk '{print $(NF-3)}' | tr -d '()']

    devices.each_line do |device|
      device = device.chomp.chomp(' ')
      puts "/usr/local/bin/ios-deploy -i #{device} -t 1 --uninstall_only --bundle_id com.oblong.Mezzanine-Alpha"
      system "/usr/local/bin/ios-deploy -i #{device} -t 1 --uninstall_only --bundle_id com.oblong.Mezzanine-Alpha"
      puts ""
    end
  else
    puts "ios-deploy needs to be install"
  end

  #get all different simulators supported by Xcode
  simdevices = %x[xcrun simctl list devicetypes | tail -n +2 | sed 's/([^)]*)//g']

  #and shut down, erase and boot all simulators
  simdevices.each_line do |simulator|
    simulator = simulator.chomp
    simulator = simulator.chomp(' ')

    puts "xcrun simctl uninstall \"#{simulator}\" com.oblong.Mezzanine-Alpha"
    system "xcrun simctl uninstall \"#{simulator}\" com.oblong.Mezzanine-Alpha"

    puts ""
  end

end


