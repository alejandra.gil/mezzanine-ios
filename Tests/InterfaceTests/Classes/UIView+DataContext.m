//
//  UIView+DataContext.m
//  InterfaceTests
//
//  Created by Zai Chang on 11/16/10.
//  Copyright 2010 Oblong Industries. All rights reserved.
//

#import "UIView+DataContext.h"
#import <objc/runtime.h>


@implementation UIView(DataContext)

static char dataContextKey;

-(id) dataContext
{
  return objc_getAssociatedObject(self, &dataContextKey);
}

-(void) setDataContext:(id)dataContext
{
  objc_setAssociatedObject(self, &dataContextKey, dataContext, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end
