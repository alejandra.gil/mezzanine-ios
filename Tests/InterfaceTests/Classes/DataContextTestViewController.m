//
//  DataContextTestViewController.m
//  InterfaceTests
//
//  Created by Zai Chang on 11/16/10.
//  Copyright 2010 Oblong Industries. All rights reserved.
//

#import "DataContextTestViewController.h"
#import "UIView+DataContext.h"
#import <objc/runtime.h>


@implementation DataContextTestViewController

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

- (void)viewDidLoad 
{
  [super viewDidLoad];
  
  
  NSLog(@"Testing data context");
  
  view1.dataContext = @"Data Context 1";
  view2.dataContext = [NSArray arrayWithObjects:@"1", @"2", @"3", nil];
  view3.dataContext = [NSDictionary dictionaryWithObjectsAndKeys:
                       @"Value 1", @"Key 1",
                       @"Value 2", @"Key 2",
                       @"Value 3", @"Key 3",
                       nil];
  
  NSLog(@"view1.dataContext = %@", view1.dataContext);
  NSLog(@"view2.dataContext = %@", view2.dataContext);
  NSLog(@"view3.dataContext = %@", view3.dataContext);
  
  NSLog(@"Clearing data contexts");
  
  view1.dataContext = nil;
  view2.dataContext = [NSNull null];
  objc_removeAssociatedObjects(view3);
  
  NSLog(@"view1.dataContext = %@", view1.dataContext);
  NSLog(@"view2.dataContext = %@", view2.dataContext);
  NSLog(@"view3.dataContext = %@", view3.dataContext);
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
