//
//  InterfaceTestsAppDelegate.h
//  InterfaceTests
//
//  Created by Zai Chang on 10/18/10.
//  Copyright 2010 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>

@class InterfaceTestsViewController;

@interface InterfaceTestsAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    InterfaceTestsViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet InterfaceTestsViewController *viewController;

@end

