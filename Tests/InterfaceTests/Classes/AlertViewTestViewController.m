//
//  AlertViewTestViewController.m
//  InterfaceTests
//
//  Created by Zai Chang on 11/16/10.
//  Copyright 2010 Oblong Industries. All rights reserved.
//

#import "AlertViewTestViewController.h"


@implementation AlertViewTestViewController

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}
*/

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

-(IBAction) showAlertView
{
  UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"An Alert View" 
                                                      message:@"Do your worst" 
                                                     delegate:self 
                                            cancelButtonTitle:@"Cancel" 
                                            otherButtonTitles:@"Button 1", @"Button 2", nil];
  [alertView show];
  [alertView release];
}

-(IBAction) showActionSheet
{
  UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"An Action Sheet" 
                                                           delegate:self 
                                                  cancelButtonTitle:@"Cancel" 
                                             destructiveButtonTitle:@"Destructive" 
                                                  otherButtonTitles:@"Button 1", @"Button 2", nil];
  [actionSheet showInView:self.view];
  [actionSheet release];
}


#pragma mark -
#pragma mark UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
  NSLog(@"AlertView clickedButtonAtIndex: %d", buttonIndex);
}

// Called when we cancel a view (eg. the user clicks the Home button). This is not called when the user clicks the cancel button.
// If not defined in the delegate, we simulate a click in the cancel button
- (void)alertViewCancel:(UIAlertView *)alertView
{
  NSLog(@"AlertView alertViewCancel");
}

- (void)willPresentAlertView:(UIAlertView *)alertView
{
  NSLog(@"AlertView willPresentAlertView");
}

- (void)didPresentAlertView:(UIAlertView *)alertView
{
  NSLog(@"AlertView didPresentAlertView");
}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex // before animation and hiding view
{
  NSLog(@"AlertView willDismissWithButtonIndex: %d", buttonIndex);
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex  // after animation
{
  NSLog(@"AlertView didDismissWithButtonIndex: %d", buttonIndex);
}


#pragma mark -
#pragma mark UIActionSheetDelegate

// Called when a button is clicked. The view will be automatically dismissed after this call returns
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
  NSLog(@"ActionSheet clickedButtonAtIndex: %d", buttonIndex);
}

// Called when we cancel a view (eg. the user clicks the Home button). This is not called when the user clicks the cancel button.
// If not defined in the delegate, we simulate a click in the cancel button
- (void)actionSheetCancel:(UIActionSheet *)actionSheet
{
  NSLog(@"ActionSheet actionSheetCancel");
}

- (void)willPresentActionSheet:(UIActionSheet *)actionSheet  // before animation and showing view
{
  NSLog(@"ActionSheet willPresentActionSheet");
}

- (void)didPresentActionSheet:(UIActionSheet *)actionSheet  // after animation
{
  NSLog(@"ActionSheet didPresentActionSheet");
}

- (void)actionSheet:(UIActionSheet *)actionSheet willDismissWithButtonIndex:(NSInteger)buttonIndex // before animation and hiding view
{
  NSLog(@"ActionSheet willDismissWithButtonIndex: %d", buttonIndex);
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex  // after animation
{
  NSLog(@"ActionSheet didDismissWithButtonIndex: %d", buttonIndex);
}

@end


