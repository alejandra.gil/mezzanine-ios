//
//  InterfaceTestsViewController.m
//  InterfaceTests
//
//  Created by Zai Chang on 10/18/10.
//  Copyright 2010 Oblong Industries. All rights reserved.
//

#import "InterfaceTestsViewController.h"



@implementation InterfaceTestsViewController



/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/


/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}
*/


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

-(void) testControllerNamed:(NSString*)controllerName nibName:(NSString*)nib
{
  Class controllerClass = NSClassFromString(controllerName);
  if (controllerClass)
  {
    id controller = nib ? [[controllerClass alloc] initWithNibName:nib bundle:nil] : [[controllerClass alloc] init];
    if (controller)
    {
      UIButton *closeButton = [[[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44.0, 44.0)] autorelease];
      [closeButton setTitle:@"X" forState:UIControlStateNormal];
      [closeButton setBackgroundColor:[UIColor blackColor]];
      [closeButton addTarget:self action:@selector(closeTest) forControlEvents:UIControlEventTouchUpInside];
      [[controller view] addSubview:closeButton];
      
      [self presentModalViewController:controller animated:YES];
    }
  }
}

-(void) testControllerNamed:(NSString*)controllerName
{
  [self testControllerNamed:controllerName nibName:nil];
}

-(IBAction) closeTest
{
  [self dismissModalViewControllerAnimated:YES];
}

-(IBAction) testControls
{
  [self testControllerNamed:@"ControlsTestViewController"];
}

-(IBAction) testLoginView
{
  [self testControllerNamed:@"OBConnectionViewController"];
}

-(IBAction) testFaceTimeView
{
  [self testControllerNamed:@"OBFaceTimeViewController"];
}

-(IBAction) testImageUploadView
{
  [self testControllerNamed:@"OBImageUploadViewController"];
}

-(IBAction) testAlertView
{
  [self testControllerNamed:@"AlertViewTestViewController"];
}

-(IBAction) testDataContext
{
  [self testControllerNamed:@"DataContextTestViewController"];
}

@end
