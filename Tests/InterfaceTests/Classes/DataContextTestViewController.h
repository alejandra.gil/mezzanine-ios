//
//  DataContextTestViewController.h
//  InterfaceTests
//
//  Created by Zai Chang on 11/16/10.
//  Copyright 2010 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface DataContextTestViewController : UIViewController 
{
  IBOutlet UIView *view1;
  IBOutlet UIView *view2;
  IBOutlet UIView *view3;
}

@end
