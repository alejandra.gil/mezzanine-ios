//
//  InterfaceTestsViewController.h
//  InterfaceTests
//
//  Created by Zai Chang on 10/18/10.
//  Copyright 2010 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InterfaceTestsViewController : UIViewController 
{

}

-(IBAction) testControls;
-(IBAction) testLoginView;
-(IBAction) testFaceTimeView;
-(IBAction) testImageUploadView;
-(IBAction) testAlertView;
-(IBAction) testDataContext;

@end

