//
//  UIView+DataContext.h
//  InterfaceTests
//
//  Created by Zai Chang on 11/16/10.
//  Copyright 2010 Oblong Industries. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface UIView (DataContext)

@property (nonatomic,retain) id dataContext;

@end
