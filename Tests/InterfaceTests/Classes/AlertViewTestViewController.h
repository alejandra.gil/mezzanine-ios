//
//  AlertViewTestViewController.h
//  InterfaceTests
//
//  Created by Zai Chang on 11/16/10.
//  Copyright 2010 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AlertViewTestViewController : UIViewController <UIAlertViewDelegate, UIActionSheetDelegate>
{

}

-(IBAction) showAlertView;
-(IBAction) showActionSheet;

@end
