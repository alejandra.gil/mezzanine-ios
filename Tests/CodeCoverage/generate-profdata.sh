#!/bin/bash

xcodebuild test -project ../../MezzKit/MezzKit.xcodeproj -scheme MezzKit -sdk iphonesimulator -configuration Debug -enableCodeCoverage YES -destination 'platform=iOS Simulator,name=iPhone 7,OS=10.2'
xcodebuild test -project ../../Apps/Mezzanine/Mezzanine.xcodeproj -scheme Mezzanine -sdk iphonesimulator -configuration Debug -enableCodeCoverage YES -destination 'platform=iOS Simulator,name=iPhone 7,OS=10.2'
