Mezzanine Code Coverage

Simply run those two scripts in the following order to obtained test results and then analyze the code coverage:

#1 generate-profdata.sh: executes the tests on Mezzanine and MezzKit to generate data to analyze by...)
#2 code-coverage-analysys.sh: outputs a summary of the current state of tests of Mezzanine and MezzKit
