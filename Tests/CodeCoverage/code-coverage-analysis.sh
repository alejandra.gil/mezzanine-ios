#!/bin/bash

# based on http://mgrebenets.github.io/mobile%20ci/2015/09/21/code-coverage-for-ios-xcode-7/

totalLinesAll=0
totalMissingLinesAll=0

calculate_percentage()
{
  PROJECT_NAME=$1
  PROJECT_PATH=$2
  SCHEME=$3
  PROFDATA_SEARH_PATH=$4
  BINARY_SEARH_PATH=$5
  CLASSES_PATH=$6

  echo "$PROJECT_NAME"
  echo "========================================"
  echo ""

  BUILD_SETTINGS=build-settings-$PROJECT_NAME.txt
  xcodebuild -project $PROJECT_PATH -scheme $SCHEME -sdk iphonesimulator -configuration Debug -showBuildSettings > ${BUILD_SETTINGS}

  PROJECT_TEMP_ROOT=$(grep -m1 PROJECT_TEMP_ROOT ${BUILD_SETTINGS} | cut -d= -f2 | xargs)
  PROFDATA=$(find ${PROJECT_TEMP_ROOT} -path "*$PROFDATA_SEARH_PATH" | head -n 1)
  BINARY=$(find ${PROJECT_TEMP_ROOT} -path "*$BINARY_SEARH_PATH")

  totalLines=0;
  totalMissingLines=0;
  for f in $CLASSES_PATH/*.m
  do
    f=$(echo "$(cd "$(dirname "$f")"; pwd)/$(basename "$f")")
    report=$(echo -e `xcrun llvm-cov report -instr-profile ${PROFDATA} ${BINARY} $f 2>/dev/null | tail -n 1`)
    lines=$(echo $report | awk '{print $5}')
    missingLines=$(echo $report | awk '{print $6}')
    totalLines=$((totalLines + lines))
    totalMissingLines=$((totalMissingLines + missingLines))
  done

  totalCoveredLines=$(( totalLines - totalMissingLines ))
  codeCoveragePercentage=$(awk "BEGIN {printf \"%.2f\",${totalCoveredLines}/${totalLines}*100}")

  echo "Total Lines in Project    $totalLines"
  echo "- Not Covered Lines       $totalMissingLines"
  echo "- Covered Lines           $totalCoveredLines"
  echo ""
  echo "Code Coverage             $codeCoveragePercentage%"
  echo ""
  echo ""

  rm $BUILD_SETTINGS

  totalLinesAll=$((totalLines + totalLinesAll));
  totalMissingLinesAll=$((totalMissingLines + totalMissingLinesAll));
}

echo ""
echo ""

calculate_percentage "MEZZANINE" "../../Apps/Mezzanine/Mezzanine.xcodeproj" "Mezzanine" "Coverage.profdata" "Mezzanine.app/Mezzanine" "../../Apps/Mezzanine/Classes"
calculate_percentage "MEZZKIT" "../../MezzKit/MezzKit.xcodeproj" "MezzKit" "Coverage.profdata" "MezzKitTests.xctest/MezzKitTests" "../../MezzKit/MezzKit"

echo "MEZZANINE + MEZZKIT"
echo "========================================"

totalCoveredLinesAll=$(( totalLinesAll - totalMissingLinesAll ))
codeCoveragePercentageAll=$(awk "BEGIN {printf \"%.2f\",${totalCoveredLinesAll}/${totalLinesAll}*100}")

echo "Total Lines in Project    $totalLinesAll"
echo "- Not Covered Lines       $totalMissingLinesAll"
echo "- Covered Lines           $totalCoveredLinesAll"
echo ""
echo "Code Coverage             $codeCoveragePercentageAll%"
echo ""
echo ""
