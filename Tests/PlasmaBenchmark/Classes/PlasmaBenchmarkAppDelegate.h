//
//  PlasmaBenchmarkAppDelegate.h
//  PlasmaBenchmark
//
//  Created by Zai Chang on 12/2/10.
//  Copyright 2010 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PlasmaBenchmarkViewController;

@interface PlasmaBenchmarkAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    PlasmaBenchmarkViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet PlasmaBenchmarkViewController *viewController;

@end

