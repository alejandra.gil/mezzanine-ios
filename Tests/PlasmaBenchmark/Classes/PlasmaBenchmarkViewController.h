//
//  PlasmaBenchmarkViewController.h
//  PlasmaBenchmark
//
//  Created by Zai Chang on 12/2/10.
//  Copyright 2010 Oblong Industries. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlasmaBenchmarkViewController : UIViewController 
{
  NSString *pool;
  NSInteger numberOfBatches;
  NSInteger numberOfProteinsPerBatch;  
}

-(IBAction) runReadTest;

-(IBAction) runWriteTest;

-(IBAction) runPingPongTest;

@end

