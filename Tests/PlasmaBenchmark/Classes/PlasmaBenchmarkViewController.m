//
//  PlasmaBenchmarkViewController.m
//  PlasmaBenchmark
//
//  Created by Zai Chang on 12/2/10.
//  Copyright 2010 Oblong Industries. All rights reserved.
//

#import "PlasmaBenchmarkViewController.h"

#import <libPlasma/bench/Writer.h>
#import <libPlasma/bench/Reader.h>
#import <libPlasma/bench/PingPonger.h>


namespace bench = ::oblong::plasma::bench;


@implementation PlasmaBenchmarkViewController


- (void)viewDidLoad 
{
  [super viewDidLoad];
  
  pool = [@"tcp://chang01.local/bench" retain]; // Leak away, wanker
  numberOfBatches = 25;
  numberOfProteinsPerBatch = 100;  
}

-(IBAction) runReadTest
{
  // Read test
  bench::Reader::Config readerConfig;
  readerConfig.pool = [pool UTF8String];
  readerConfig.batches = numberOfBatches;
  readerConfig.batch_size = numberOfProteinsPerBatch;
  readerConfig.ModeFromString("random");
  bench::Reader reader (readerConfig);
  reader.Run();
}

-(IBAction) runWriteTest
{
  // Write test
  bench::Writer::Config config;
  config.pool = [pool UTF8String];
  config.batches = numberOfBatches;
  config.batch_size = numberOfProteinsPerBatch;
  config.AddProteins("42");
  config.skip = false;
  bench::Writer writer (config);
  writer.Run();
}

-(IBAction) runPingPongTest
{
  bench::PingPonger::Config pingConfig;
  pingConfig.pool = [pool UTF8String];
  pingConfig.batches = numberOfBatches;
  pingConfig.batch_size = numberOfProteinsPerBatch;
  bench::AddProteins (pingConfig.proteins, "42");
  pingConfig.timeout = 10;
  
  pingConfig.ping = true;
  pingConfig.out = "ping";
  
  bench::PingPonger::Config pongConfig (pingConfig);
  pongConfig.out = "pong";
  pongConfig.ping = false;
  
  bench::PingPonger ping (pingConfig);
  bench::PingPonger pong (pongConfig);
  
  if (!pong.Fork () || !ping.Fork ()) return;
  if (!ping.Wait () || !pong.Wait ()) return;
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

@end
